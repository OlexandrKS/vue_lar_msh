<?php

Route::group(['prefix' => 'auth', 'as' => 'auth.'], function () {
    Route::post('login', 'BackendApi\AuthController@login')->name('login');
    Route::post('logout', 'BackendApi\AuthController@logout')->name('logout');
    Route::post('refresh', 'BackendApi\AuthController@refresh')->name('refresh');
    Route::get('user', 'BackendApi\AuthController@user')->name('user');
});

Route::group(['middleware' => 'auth'], function () {
    Route::put('profile', 'BackendApi\ProfileController@update');

    Route::apiResource('constructor', 'BackendApi\Constructor\PageController');
    Route::post('constructor/component/{component}/{action}', 'BackendApi\Constructor\PageController@component')
        ->name('constructor.component.action');
    Route::post('constructor/page/{type}/{action}', 'BackendApi\Constructor\PageController@page')
        ->name('constructor.page.action');

    Route::apiResource('svg-icon', 'BackendApi\SvgIconController');
    Route::apiResource('menu-item', 'BackendApi\MenuItemController');
    Route::apiResource('banner', 'BackendApi\BannerController');

    Route::apiResource('backend-file', 'BackendApi\FileController');
    Route::post('backend-file/delete','BackendApi\FileController@deleteItems' );
});
