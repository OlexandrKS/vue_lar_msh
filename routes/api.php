<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// PopUp
Route::get('popup/{popupIds}', 'Api\PopupController@index')->name('popup.index');
Route::post('send', 'Api\PopupController@send')->name('popup.send');

// Cart
Route::get('cart', 'Api\Accounting\CartController@index')->name('cart.index');
Route::post('cart', 'Api\Accounting\CartController@store')->name('cart.store');
Route::put('cart', 'Api\Accounting\CartController@update')->name('cart.update');
Route::delete('cart', 'Api\Accounting\CartController@destroy')->name('cart.destroy');
Route::post('cart/sync', 'Api\Accounting\CartController@sync')->name('cart.sync');

// Order
Route::get('orders', 'Api\Accounting\OrderController@index')->name('order.index');
Route::post('order/calculate', 'Api\Accounting\OrderController@calculate')->name('order.calculate');
Route::post('order/check', 'Api\Accounting\OrderController@checkUserExists')->name('order.user');

Route::post('checkout/attempt', 'Api\Accounting\OrderController@attemptUserCreation')->name('order.attempt');
Route::post('checkout', 'Api\Accounting\OrderController@store')->name('order.store');

Route::group(['middleware' => 'throttle:60'], function () {
    // authentication
    Route::post('auth/login', 'Api\Auth\AuthController@signIn')->name('auth.login');
    Route::post('auth/logout', 'Api\Auth\AuthController@logout')->name('auth.logout');

    // registration
    Route::post('registration/attempt', 'Api\Auth\RegistrationController@attempt')
        ->name('registration.attempt');
    Route::post('registration/validation', 'Api\Auth\RegistrationController@validateCode')
        ->name('registration.validation');
    Route::post('registration', 'Api\Auth\RegistrationController@register')
        ->name('registration.handler');

    // password recovery
    Route::post('password-recovery/attempt', 'Api\Auth\PasswordRecoveryController@attempt')
        ->name('password-recovery.attempt');
    Route::post('password-recovery/validation', 'Api\Auth\PasswordRecoveryController@validateCode')
        ->name('password-recovery.validation');
    Route::post('password-recovery', 'Api\Auth\PasswordRecoveryController@recovery')
        ->name('password-recovery.handler');

    // card activation
    Route::post('card-activation/attempt', 'Api\Auth\CardActivationController@attempt')
        ->name('card-activation.attempt');
    Route::post('card-activation/activate', 'Api\Auth\CardActivationController@activate')
        ->name('card-activation.activate');
});

// Favorite lists
Route::post('favorite-list', 'Api\Accounting\FavoriteListController@store')->name('favorite-list.store');
Route::delete('favorite-list/{offer}', 'Api\Accounting\FavoriteListController@remove')->name('favorite-list.destroy');
Route::get('favorite-list', 'Api\Accounting\FavoriteListController@index')->name('favorite-list.index');

// Account
Route::group(['middleware' => 'auth', 'prefix' => 'account', 'as' => 'account.'], function () {
    // Personal data
    Route::get('profile', 'Api\Account\ProfileController@index')->name('profile.index');
    Route::post('profile', 'Api\Account\ProfileController@store')->name('profile.store');
    Route::post('change-password', 'Api\Account\ProfileController@changePassword')->name('profile.change-password');

    // Bonuses
    Route::get('bonus-event', 'Api\Account\BonusController@index')->name('bonus-event.index');

    // PromoCode
    Route::get('promo-code', 'Api\Account\PromoCodeController@index')->name('promo-code.index');
    Route::get('promo-code/available', 'Api\Account\PromoCodeController@available')->name('promo-code.available');
    Route::post('promo-code', 'Api\Account\PromoCodeController@store')->name('promo-code.store');

    //Social
    Route::get('social', 'Api\Account\SocialController@index')->name('social.index');
    Route::delete('social', 'Api\Account\SocialController@destroy')->name('social.destroy');

    //Order
    Route::get('order', 'Api\Account\UserOrderController@index')->name('order.index');

    //TODO
    //ProdBoard
    Route::get('prod-board', 'Api\ProdBoardController@signInProdBoard');
});

// ProdBoard
Route::group(['prefix' => 'prodboard', 'as' => 'prodboard.'], function () {
    Route::post('signin', 'Api\ProdBoardController@signin');
    Route::post('send-basket', 'Api\ProdBoardController@sendBasket');
});

Route::group(['prefix' => 'bpm', 'as' => 'bpm.'], function () {
    Route::post('callibri-feedback', 'Api\BPMController@callibriFeedback')->name('callibri-feedback');
    Route::post('left-checkout', 'Api\BPMController@leftCheckout')->name('left-checkout');
});

Route::group(['prefix' => 'catalog', 'as' => 'catalog.'], function () {
    Route::post('review', 'Api\Catalog\ReviewController@store')->name('review.store');
    Route::post('review/{id}/like', 'Api\Catalog\ReviewController@onLike')->name('review.like');
    Route::post('review/{id}/dislike', 'Api\Catalog\ReviewController@onDislike')->name('review.dislike');

    Route::get('product/together-cheaper/{id}', 'Api\Catalog\ProductController@togetherCheaper')->name('product.togetherCheaper');
});

Route::post('vacancies', 'Api\EmployeeController@store');
Route::get('vacancies/{city_id}', 'Api\EmployeeController@index');

Route::get('banner', 'Api\BannerController@index')->name('banner.index');
Route::get('banner/{url}', 'Api\BannerController@show')->name('banner.show');

Route::get('test', function () {
    throw new Illuminate\Auth\AuthenticationException();
});
