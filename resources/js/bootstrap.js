import Vue from 'vue'
import 'vue-template-compiler'
import keys from 'lodash/keys'

// Env
const isProduction = process.env.NODE_ENV === 'production'

Vue.config.productionTip = false
Vue.config.devtools = !isProduction

// Plugins
import './plugins'

// Native event listeners
import './nativeListeners'

// Listeners
import './listeners'

// Global components
import components from './components'
keys(components).map(key => Vue.component(key, components[key]))

// router
Promise.all([
    import(/* webpackChunkName: `router` */ '@/plugins/router'),
    import(/* webpackChunkName: `routes` */ './routes')
]).then(modules => {
    const routerFactory = modules[0].default
    const routes = modules[1].default

    const router = routerFactory(routes)
    window.router = router

    router.resolve(window.location.pathname)
})
