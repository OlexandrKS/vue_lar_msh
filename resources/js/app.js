import './bootstrap'
import Vue from 'vue'
import store from './store'

const app = new Vue({
    el: '#modern-app',
    store,
    created () {
        require('./afterBoot')
    },
    mounted () {
        if (this.$store.getters['user/isAuthenticated']) {
            const syncCartPromise = this.$store.dispatch('cart/syncBackend')
            const syncFavoriteListPromise = this.$store.dispatch('favoriteList/syncBackend')

            // const syncPromoCode = this.$store.dispatch('checkout/promoCode')
        }

        this.$store.dispatch('promotions/parseFreeDelivery30plusFromJs')
    }
})

window.Application = app

export default app
