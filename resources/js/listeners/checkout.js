import { EventBus } from '../plugins/eventBus'
import app from '@/app'

EventBus.$on('checkout.created', order => {
    app.$store.commit('cart/erase')
    app.$store.commit('user/setUser', {
        user: order.user
    })

    window.location.replace('/thanks')
})
