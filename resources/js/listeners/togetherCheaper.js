import { EventBus } from '../plugins/eventBus'
import { Bus as ModalBus } from '@/plugins/vuModal'

EventBus.$on('together-cheaper.modal', (offers) => {
    import(/* webpackChunkName: "together-cheaper-modal" */ '@/components/Product/TogetherCheaper/MsTogetherCheaperModal.vue')
        .then(resource => {
            ModalBus.$emit('new', {
                component: resource.default,
                props: {
                    offers: offers,
                },
            })
        })
})
