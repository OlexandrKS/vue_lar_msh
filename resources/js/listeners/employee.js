import {EventBus} from '../plugins/eventBus'
import {Bus as ModalBus} from '@/plugins/vuModal'

EventBus.$on('employee.modal', (city_id, vacancy_id) => {
    import(/* webpackChunkName: "employee-modal" */ '@/components/Employee/MsEmployeeModal.vue')
        .then(resource => {
            ModalBus.$emit('new', {
                component: resource.default,
                props: {
                    city_id: city_id,
                    vacancy_id: vacancy_id
                }
            })
        })
})