import { EventBus } from '../plugins/eventBus'
import { Bus as ModalBus } from '@/plugins/vuModal'
import app from '@/app'

const showModal = (customTitle = {}) => {
    if (app.$store.getters['user/isAuthenticated']) {
        return
    }

    import(/* webpackChunkName: "auth-modal" */ '@/components/Auth/MsAuthModal.vue')
        .then(resource => {
            ModalBus.$emit('new', {
                component: resource.default,
                props: {
                    customTitles: customTitle
                }
            })
        })
}

EventBus.$on('auth.modal', showModal)

document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('.js-show-auth-modal').forEach(element => {
        element.addEventListener('click', event => {
            event.preventDefault()

            showModal()
        })
    })
})

