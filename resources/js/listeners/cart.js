import EventBus from '@/plugins/eventBus'
import { Bus as ModalBus } from '@/plugins/vuModal'
import Criteo from '@/plugins/ecommerce/criteo'
import RatingMailRu from '@/plugins/ecommerce/ratingMailRu'
import GoogleTagManager from '@/plugins/ecommerce/googleTagManager'
import debounce from 'lodash/debounce'
import app from '@/app'

EventBus.$on('cart.pickOffer', debounce(async ({ offerId, withoutCart = false }) => {
    await app.$store.dispatch('cart/pickOffer', { offerId, withoutCart })

    GoogleTagManager.pushCartFormEvent(app.$store.state.cart.items)
}, 300, {
    leading: true,
    trailing: false,
}))

EventBus.$on('cart.modal', () => {
    import(/* webpackChunkName: "cart-modal" */ '@/components/Cart/MsCartModal.vue')
        .then(resource => {
            ModalBus.$emit('new', {
                component: resource.default,
            })

            Criteo.pushCartEvent(
                Criteo.collectProducts(
                    app.$store.state.cart.items,
                ))

            RatingMailRu.pushCartEvent(RatingMailRu.collectProductsNids(
                app.$store.state.cart.items),
                app.$store.getters['cart/totalPrice'])
        })
})

const prodBoardCallback = async () => {
    const href = await app.$store.dispatch('user/prodBoard')

    if (href) {
        location.href = href
    }
}

EventBus.$on('cart.prodBoard', async () => {
    if (! app.$store.getters['user/isAuthenticated']) {
        app.$store.commit('user/addAfterSuccessAuthCallback', async () => {
            await prodBoardCallback()

            return
        })

        return
    }

    await prodBoardCallback()
})
