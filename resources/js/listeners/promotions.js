import EventBus from '@/plugins/eventBus'
import app from '@/app'
import Cookies from 'js-cookie'

EventBus.$on('cart.changed', () => {
    const cookieName = 'fd_cal'
    const cookieValue = Cookies.get(cookieName)

    if (app.$store.getters['cart/totalPrice'] >= app.$store.state.promotions.free_delivery_30_plus.fromPrice &&
        cookieValue === undefined
    ) {
        const now = new Date()
        const expired = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1)
        const state = {
            expired: expired.getTime() / 1000,
            used: false
        }

        Cookies.set(cookieName, JSON.stringify(state), { expires: 10, path: '' })

        app.$store.commit('promotions/updateFreeDelivery', state)
        app.$store.commit('headBanner/showFreeDelivery')
    }
})
