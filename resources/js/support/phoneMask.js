export default {
    directives: {
        phoneMask: {
            bind (el) {
                el.oninput = function (e) {
                    if (!e.isTrusted) {
                        return;
                    }
                    if (this.value.startsWith('+7')) {
                        let x = this.value.replace(/\D/g, '').match(/(7?)(\d{0,3})(\d{0,3})(\d{0,2})(\d{0,2})/);
                        this.value = !x[3] ? '+' + x[1] + ' (' + x[2] : '+' + x[1] + ' (' + x[2] + ') ' + x[3] + (x[4] ? ' ' + x[4] : '') + (x[5] ? ' ' + x[5] : '');
                    } else {
                        this.value = '+7 (';
                    }
                    el.dispatchEvent(new Event('input'));
                }
            }
        }
    },
}
