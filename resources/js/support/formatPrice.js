export default {
    methods: {
        formatPrice (value) {
            if (value === undefined) {
                return '';
            }

            return parseInt(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        },
    }
}
