import forEach from 'lodash/forEach'

export const getValidationErrors = errors => {
    const response = []

    forEach(errors, (messages, field) => {
        forEach(messages, msg => {
            response.push({ field, msg })
        })
    })


    return response
}

export const pushValidationErrors = (errors, bag) => {
    forEach(getValidationErrors(errors),error => bag.add(error))

}