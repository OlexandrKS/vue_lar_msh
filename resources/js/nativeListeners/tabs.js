const activeClass = 'active'
const attribute = 'data-tab'

const clickEvent = event => {
    let tab = event.target

    if (! tab.classList.contains('nav-tab')) {
        tab = tab.closest('.nav-tab')
    }

    const tabName = tab.getAttribute('data-tab')
    const tabs = document.querySelectorAll('.tabs .nav-tabs .nav-tab')
    const tabContents = document.querySelectorAll('.tabs .content-tabs .content-tab')

    for (let i = 0; i < tabContents.length; i++) {
        tabs[i].classList.remove(activeClass)
        tabs[i].getAttribute(attribute) === tabName ? tabs[i].classList.add(activeClass) : null
        tabContents[i].classList.remove(activeClass)
        tabContents[i].getAttribute(attribute) === tabName ? tabContents[i].classList.add(activeClass) : null
    }
}

document.addEventListener('DOMContentLoaded', () => {
    const tabs = document.querySelectorAll('.tabs .nav-tabs .nav-tab')

    for (let tab of tabs) {
        tab.addEventListener('click', clickEvent)
    }
})
