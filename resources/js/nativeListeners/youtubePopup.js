import { Bus as ModalBus } from '@/plugins/vuModal'

document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('.youtube-modal').forEach(button => {
        button.addEventListener('click', () => {
            import(/* webpackChunkName: "youtube-modal" */ '@/components/Modals/MsYoutubeModal')
                .then(resource => {
                    ModalBus.$emit('new', {
                        component: resource.default,
                        props: {
                            code: button.dataset.code,
                            title: button.dataset.title
                        }
                    })
                })
        })
    })
})
