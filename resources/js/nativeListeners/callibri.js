import { ApiClient } from '@/plugins/axios'
import { EventBus } from '@/plugins/eventBus'

const initFeedbackSendListener = () => {
    let timeout = null

    // Callibri send to bpm feedback request
    window.callibri_onSendFeedback = () => {
        if (! timeout) {
            timeout = setTimeout(() => {
                timeout = null;
            }, 5000);

            ApiClient.$post('/bpm/callibri-feedback')
        }
    }
}

const initSendOrderListener = () => {
    let timerId = setInterval (function () {
        let sendButton = document.getElementById('callibri_callback_button_request')
        if (sendButton) {
            clearInterval(timerId)
            sendButton.addEventListener('click', () => {
                EventBus.$emit('bpm.sendOrder')
            })
        }
    }, 1000)
}



document.addEventListener('DOMContentLoaded', () => {
    initFeedbackSendListener()
    initSendOrderListener()
})
