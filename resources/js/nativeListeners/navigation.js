import get from 'lodash/get'
import EventBus from '@/plugins/eventBus'

class Navigation {
    constructor () {
        this.menu = document.querySelector('.side-menu')

        if (this.menu) {
            this.submenu = this.menu.querySelector('.menu-sub-list')
            this.timeout = 0
            this.activeClass = 'active'
            this.currentActive = null
            this.siteContent = document.querySelector('.content-overlay')

            this.initSubCategory()
            this.initToggleMenu()
        }
    }

    initSubCategory () {
        this.menu.querySelectorAll('li a').forEach(element => {
            element.addEventListener('mouseover', () => {
                if (this.currentActive) {
                    this.currentActive.classList.remove('hover')
                }

                this.currentActive = element.parentNode
                this.currentActive.classList.add('hover')

                const categoryId = element.dataset.catId

                if (this.timeout === 0 && categoryId !== undefined) {
                    this.showSubMenu(categoryId)
                }
            })

            element.addEventListener('mouseleave', () => {
                this.deleteTimeOut()
            })
        })
    }

    initToggleMenu () {
        this.menu.addEventListener('mouseover', () => {
            if (this.menu.classList.contains('mini')) {
                this.menu.classList.add('opened')
            }
        })

        this.menu.addEventListener('mouseleave', () => {
            this.hideSubMenu()

            if (this.menu.classList.contains('mini')) {
                this.menu.classList.remove('opened')
            }
        })

        const wrap = this.menu.querySelector('.menu-list-wrap')

        wrap.addEventListener('mousemove', e => {
            if (e.target.className === 'menu-list-wrap') {
                this.hideSubMenu()
            }
        })
    }

    deleteTimeOut () {
        if (this.timeout !== 0) {
            clearTimeout(this.timeout)
            this.timeout = 0
        }
    }

    showSubMenu (id) {
        this.timeout = setTimeout(() => {
            this.menu.classList.add(this.activeClass)
            this.submenu.classList.add(this.activeClass)
            this.siteContent.classList.add('disabled-darck')
            EventBus.$emit('navigation.show', id)
        }, 400)
    }

    hideSubMenu () {
        this.deleteTimeOut()
        this.menu.classList.remove(this.activeClass)
        this.submenu.classList.remove(this.activeClass)

        if (this.siteContent) {
            this.siteContent.classList.remove('disabled-darck')
        }

        if (this.currentActive) {
            this.currentActive.classList.remove('hover')
        }
    }
}

const initTopBanner = () => {
    const menu = document.querySelector('.side-menu')

    const calc = () => {
        const height = get(document.querySelector('#header'), 'clientHeight', 0)

        if (window.scrollY <= height) {
            menu.style.top = (height - window.scrollY) + 'px'
        }

        if (window.scrollY > height) {
            menu.style.top = 0
        }
    }

    if (menu) {
        setTimeout(() => {
            menu.classList.remove('animate')
        }, 2000)

        window.onscroll = calc
        calc()

        EventBus.$on('navigation.topCalc', calc)
    }
}

const initMenuHamburgerListener = menu => {
    const hamburger = document.querySelector('.hamburger-btn')

    if (hamburger) {
        hamburger.addEventListener('click', () => {
            EventBus.$emit('navigation.toggle')
        })
    }
}

document.addEventListener('DOMContentLoaded', () => {
    const menu = new Navigation()

    initTopBanner()
    initMenuHamburgerListener(menu)
})
