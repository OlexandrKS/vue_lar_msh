import Criteo from '@/plugins/ecommerce/criteo'
import RatingMailRu from '@/plugins/ecommerce/ratingMailRu'
import GoogleTagManager from '@/plugins/ecommerce/googleTagManager'

document.addEventListener('DOMContentLoaded', () => {
    Criteo.init(Application.$store.getters['user/email'])
})
