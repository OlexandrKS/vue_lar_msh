const initSelect = function () {

    const selects = document.querySelectorAll('.js-select')

    for (let i = 0; i < selects.length; i++) {
        selects[i].addEventListener('click', function (e) {
            selects[i].classList.toggle('active')

            const options = this.querySelectorAll('.dropdown-list-item')
            let selected = this.querySelector('.selected')

            for (let option of options) {
                option.addEventListener('click', function () {
                    const element = option.getElementsByTagName('a')[0]
                    if (element !== undefined) {
                        selected.innerHTML = element.innerHTML
                    } else {
                        selected.innerHTML = option.innerHTML
                    }
                })
            }
        })
    }

}

window.addEventListener('load', () => {
    initSelect()
})
