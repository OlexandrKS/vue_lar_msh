document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('.hiding-block-toggle-btn').forEach(element => {
        element.addEventListener('click', () => {
            element.closest('.hiding-block').classList.toggle('active')
        })
    })
})
