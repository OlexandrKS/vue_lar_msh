import axios from 'axios'

document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('img.img-svg')
        .forEach(element => {
            axios.get(element.src).then(response => {
                const svg = new DOMParser().parseFromString(response.data, 'image/svg+xml')

                svg.classList.add('replaced-svg')

                element.replaceWith(svg)
            })
        })
})

// Replace all SVG images with inline SVG (navigation)
// $('img.img-svg').each(function(){
//     var $img = $(this);
//     var imgClass = $img.attr('class');
//     var imgURL = $img.attr('src');
//
//     $.get(imgURL, function(data) {
        // Get the SVG tag, ignore the rest
        // var $svg = $(data).find('svg');

        // Add replaced image's classes to the new SVG
        // if(typeof imgClass !== 'undefined') {
        //     $svg = $svg.attr('class', imgClass+' replaced-svg');
        // }

        // Remove any invalid XML tags as per http://validator.w3.org
        // $svg = $svg.removeAttr('xmlns:a');

        // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
        // if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
        //     $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
        // }

        // Replace image with new SVG
        // $img.replaceWith($svg);
    // }, 'xml');
// });
