import app from '@/app'

window.oSPl = () => {
    if (app.$store.getters['user/isAuthenticated'] && window.hasOwnProperty('oSpP')) {
        oSpP.push('Name', app.$store.getters['user/name'])
        oSpP.push('Email', app.$store.getters['user/email'])
    }
}
