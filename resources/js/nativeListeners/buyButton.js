import EventBus from '@/plugins/eventBus'

document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('.buy-offer-button').forEach(element => {
        EventBus.$emit('cart.pickOffer', {
            offerId: element.dataset.offer,
            withoutCart: !!element.dataset.withoutCart
        })
    })
})
