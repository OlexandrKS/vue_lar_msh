const initExpandBlock = (selector = null) => {
    const expands = document.querySelectorAll(selector || '.expand-block')
    const onClick = event => {
        let element = event.target

        if (! element.classList.contains('.expand-block-header')) {
            element = element.closest('.expand-block-header')
        }

        if (element.dataset.closeAll !== undefined) {
            expands.forEach(element => element.classList.remove('active'))
        }

        element.closest('.expand-block').classList.toggle('active')
    }

    expands.forEach(expand => {
        const btn = expand.querySelector('.js-ex-btn')

        btn.removeEventListener('click', onClick)
        btn.addEventListener('click', onClick)
    })
}

document.addEventListener('DOMContentLoaded', () => {
    initExpandBlock()
})

export default initExpandBlock
