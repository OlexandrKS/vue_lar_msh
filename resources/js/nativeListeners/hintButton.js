document.addEventListener('DOMContentLoaded', () => {
    const hintButtons = document.querySelectorAll('.info-hint')

    for (let hint of hintButtons) {
        hint.addEventListener('click', () => {
            if(hint.classList.contains('active')){
                hint.classList.remove('active')
            } else {
                hint.classList.add('active')
            }
        })
    }
})
