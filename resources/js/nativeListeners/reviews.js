import { Bus as ModalBus } from '@/plugins/vuModal'

document.addEventListener('DOMContentLoaded', () => {
    const writeReview = document.querySelectorAll('.write-review')

    for (let button of writeReview) {
        button.addEventListener('click', event => {
            const productId = event.target.dataset.product

            import(/* webpackChunkName: "write-review" */ '@/components/Review/MsReviewModal')
                .then(resource => {
                    ModalBus.$emit('new', {
                        component: resource.default,
                        props: {
                            product: productId
                        }
                    })
                })
        })
    }
})
