import appear from '@/plugins/appear'
import GoogleTagManager from '@/plugins/ecommerce/googleTagManager'

document.addEventListener('DOMContentLoaded', () => {
    appear({
        elements () {
            return document.getElementsByClassName('product-grid-item') //todo
        },
        appear (el) {
            var ecom = el.querySelector('.ecommerce_info')
            ecom ? GoogleTagManager.pushAppearProductEvent(ecom.dataset.ecommerceInfo) : ''
        }
    })
})
