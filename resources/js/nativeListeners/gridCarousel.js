import Swiper from 'swiper'

const initGridCarousel = function () {

    let swiper = new Swiper('.category-nav .swiper-container', {
        slidesPerView: 'auto',
        spaceBetween: 4,
        loop: false,
        freeMode: false
    });
}

document.addEventListener('DOMContentLoaded', () => {
    initGridCarousel()
})