import ymapsLoader from 'ymaps'

const firstDataLoad = async (ymaps, map) => {
    const elements = document.querySelector('.expand-block-content')
        .querySelectorAll('.shop-list-item')

    await setPlaceMarks(ymaps, map, elements)
}

const setPlaceMarks = async (ymaps, map, elements) => {
    const geoCollection = new ymaps.GeoObjectCollection()

    elements.forEach(element => {
        const address =
            `<address>
                <strong>${element.dataset.shopName}</strong><br/>
                Адрес: ${element.dataset.shopAddress}<br/>
                тел: ${element.dataset.shopPhone}<br/>
                <br/>
                ${element.dataset.shopMode1}<br/>
                ${element.dataset.shopMode2}
             </address>`

        geoCollection.add(new ymaps.Placemark([
            parseFloat(element.dataset.shopLatitude), parseFloat(element.dataset.shopLongitude)
        ], {
            hintContent: element.dataset.shopName,
            // iconContent: element.dataset.shopNumber,
            balloonContentBody: address,
        }, {
            preset: 'islands#darkGreenIcon',
            iconColor: parseInt(element.dataset.shopDiscountCenter) === 1 ? '#ffce00': '#ff5021'
        }))

    })

    map.geoObjects.removeAll()
    map.geoObjects.add(geoCollection)
    map.setBounds(geoCollection.getBounds())

    if (geoCollection.getLength() === 1) {
        map.setZoom(16)
    }
}

export default async () => {
    const ymaps = await ymapsLoader.load()

    const map = new ymaps.Map(document.querySelector('.map-container'), {
        center: [56.326944, 44.0075],
        zoom: 12,
        controls: ['zoomControl', 'typeSelector'],
    })

    await firstDataLoad(ymaps, map)

    document.querySelectorAll('.expand-block .js-ex-btn').forEach(expand => {
        expand.addEventListener('click', async () => {
            const elements = expand.closest('.expand-block')
                .querySelectorAll('.shop-list-item')

            await setPlaceMarks(ymaps, map, elements)
        })
    })

    const shops = document.querySelectorAll('.shop-list-item')

    shops.forEach(element => {
        element.addEventListener('click', () => {
            shops.forEach(element => element.classList.remove('active'))

            element.classList.add('active')

            map.setCenter([
                parseFloat(element.dataset.shopLatitude), parseFloat(element.dataset.shopLongitude)
            ])
            map.setZoom(16)
        })
    })

    document.querySelectorAll('.city-name').forEach(element => {
        element.addEventListener('click', async () => {
            const elements = element.closest('.city-shops')
                .querySelectorAll('.shop-list-item')

            await setPlaceMarks(ymaps, map, elements)
        })
    })
}
