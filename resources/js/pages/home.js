import Swiper from 'swiper'
import Criteo from '@/plugins/ecommerce/criteo'


export default () => {
    const mainSlider = new Swiper('#main-slider', {
        slidesPerView: 1,
        loop: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,

        navigation: {
            nextEl: '.sw-button-next',
            prevEl: '.sw-button-prev',
        },

        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    })

    setTimeout(Criteo.pushHomeEvent(), 1000);

}

