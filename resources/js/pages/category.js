import Swiper from 'swiper'
import Criteo from '@/plugins/ecommerce/criteo'

export default () => {
    const categoryMenu = new Swiper('.category-nav', {
        loop: true,
        freeMode: true,
        slidesPerView: 3,
        spaceBetween: 30,
    })

    Criteo.pushCategoryEvent(Criteo.gatherCategoryProductIds())
}
