import GoogleTagManager from '@/plugins/ecommerce/googleTagManager'
import app from '@/app'

export default () => {
    GoogleTagManager.pushProductCheckoutEvent(app.$store.state.cart.items)

    window.addEventListener('beforeunload', async () => {
        if (! app.$store.state.checkout.isFirstStep) {
            await app.$store.dispatch('checkout/uncompleted')
        }

        return null
    })
}
