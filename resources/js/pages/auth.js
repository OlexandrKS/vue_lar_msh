import app from '@/app'

export default () => {
    app.$store.commit('user/addAfterSuccessAuthCallback', () => {
        window.location.replace('/account')

        return true
    })
}
