import app from '@/app'

export default () => {
    const promoCode = window.location.pathname.split('/')[2]

    app.$store.commit('user/addAfterSuccessAuthCallback', () => {
        window.location.replace('/account')

        return true
    })
}
