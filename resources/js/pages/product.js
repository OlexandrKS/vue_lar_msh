import Swiper from 'swiper'
import Criteo from '@/plugins/ecommerce/criteo'
import RatingMailRu from '@/plugins/ecommerce/ratingMailRu'
import GoogleTagManager from '@/plugins/ecommerce/googleTagManager'
import EventBus from '@/plugins/eventBus'
import YouTubeIframeLoader from 'youtube-iframe'
import MsImageDetailModal from '@/components/Modals/MsImagesDetailModal/MsImagesDetailModal'
import { Bus as ModalBus } from '@/plugins/vuModal'
import get from 'lodash/get'

export default () => {

    const galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 4,
        slidesPerView: 9,
        loop: false,
        freeMode: true,
        loopedSlides: 5, //looped slides should be the same
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        centerInsufficientSlides: true,

        // Responsive breakpoints
        breakpoints: {
            // when window width is <= 1440px
            1439: {
                slidesPerView: 7,
            },
        }
    })

    const galleryTop = new Swiper('.gallery-top', {
        loop: true,
        loopedSlides: 5, //looped slides should be the same
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        navigation: {
            nextEl: '.sw-button-next',
            prevEl: '.sw-button-prev',
        },
        thumbs: {
            swiper: galleryThumbs,
        },
        'onSlideChangeStart': function(galleryTop) {
            // stop video on change
            var isVideo = galleryTop.slides[galleryTop.previousIndex].querySelector('.video-container');
            if (isVideo) {
                window.YT.get(isVideo.querySelector('iframe').id).stopVideo()
            }
        },
    })

    galleryTop.on('click', (e) => {

        if (e.target.className === 'sw-button-prev' || e.target.className === 'sw-button-next') return null

        let images = null
        window.innerWidth >= 992 ? images = document.querySelectorAll('.product-detail-gallery .gallery-thumbs .swiper-slide:not(.video) img') : null

        if (images.length) {
            const modalBus = Promise.resolve('Success').then(function (value) {
                ModalBus.$emit('new', {
                    component: MsImageDetailModal,
                    props: {
                        images
                    }
                })
            })
            modalBus.then(function () {

                const galleryPopupThumbs = new Swiper('.gallery-popup-slider-nav', {
                    spaceBetween: 8,
                    direction: 'vertical',
                    slideToClickedSlide: true,
                    slidesPerView: 'auto'
                });

                const galleryPopupSlider = new Swiper('.gallery-popup-slider', {
                    autoHeight: true, //enable auto height
                    pagination: {
                        el: '.swiper-pagination',
                        clickable: true,
                    },
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    },
                    thumbs: {
                        swiper: galleryPopupThumbs
                    }
                })

            })
        }
    })

    YouTubeIframeLoader.load(() => {})

    var initPlayer = function(element) {
        var player = element.querySelector('.video-iframe');
        var button = element.querySelector('.video-play');
        var ytplayer = new window.YT.Player(player, {
            playerVars: {
                'autoplay': 0,
                'modestbranding': 1,
                'controls': 1,
                'rel': 0,
            },
            videoId: element.dataset.id
        });

        button.addEventListener('click', function() {
            if (ytplayer.getPlayerState() === 1) {
                ytplayer.stopVideo();
            } else {
                ytplayer.playVideo();
            }
        })
    }

    window.onYouTubePlayerAPIReady = function() {
        var container = document.querySelectorAll('.video-container');
        for (var i = 0; i < container.length; i++) {
            initPlayer(container[i])
        }
    }



    Criteo.pushProductEvent(document.getElementsByClassName('ecommerce_info')[0].dataset.numericId)
    RatingMailRu.pushProductEvent(document.getElementById('ecommerce_info'))
    GoogleTagManager.pushProductClickEvent(document.getElementsByClassName('ecommerce_info')[0].dataset.ecommerceInfo)
    GoogleTagManager.pushProductDetailImpressionsEvent(document.getElementsByClassName('ecommerce_info')[0].dataset.ecommerceInfo)

    // buy buttons
    const sideBuyButtonClass = '.product-buy'
    const mainBuyButtonClass = '.buy-info-buttons'
    const buyButtonHandler = element => {
        EventBus.$emit('cart.pickOffer', {
            offerId: element.dataset.offer,
        })
    }

    for (let className of [sideBuyButtonClass, mainBuyButtonClass]) {
        const button = document.querySelector(className)

        if (button) {
            button.addEventListener('click', event => {
                let element = event.target

                if (!element.classList.contains(className)) {
                    element = element.closest(className)
                }

                buyButtonHandler(element)
            })
        }
    }

    window.addEventListener('scroll', () => {
        const stickyBlock = document.querySelector('.product-info-wrap')
        const offset = document.querySelector('.product-description').getBoundingClientRect().y

        if (offset <= 0) {
            stickyBlock.classList.remove('hidden')
        } else {
            stickyBlock.classList.add('hidden')
        }
    })

    document.querySelectorAll('.prod-board-btn').forEach(element => {
        element.addEventListener('click', () => {
            EventBus.$emit('cart.prodBoard')
        })
    })

    // tooltop (popover)
    const links = document.querySelectorAll('a')
    const tooltipClass = 'tooltip'

    const createTooltip = (element, text) => {

        let tooltip = document.createElement('div')

        tooltip.classList.add(tooltipClass)
        tooltip.innerHTML = text
        element.appendChild(tooltip)
    }

    const removeTooltip = element => {

        let tooltip = element.querySelector('.tooltip')

        tooltip.remove()
    }

    for (let link of links){

        link.addEventListener('mouseover', e => {
            let element = e.target
            element.getAttribute( 'data-toggle' ) == 'popover' ? createTooltip(element, element.getAttribute('data-content')) : null
        })

        link.addEventListener('mouseleave', e => {
            let element = e.target
            removeTooltip(element)
        })
    }

    // resize video slide
    const setVideoHeight = () => document.querySelector('.video-iframe').height = document.querySelector('.gallery-top .swiper-slide').clientHeight
    window.addEventListener('resize', setVideoHeight)
    window.addEventListener('load', setVideoHeight)
}
