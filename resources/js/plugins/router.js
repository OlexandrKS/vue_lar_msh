import Navigo from 'navigo'

export default (routes = {}) => {
    const router = new Navigo(null, true)

    router.on(routes)

    return router
}
