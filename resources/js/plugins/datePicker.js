import Datepicker from 'vuejs-datepicker'
import ru from 'vuejs-datepicker/dist/locale/translations/ru'

export default {
    name: 'DatePicker',
    functional: true,
    render: (h, {props, listeners}) =>
        h(Datepicker, {
            props: {
                ...props,
                language: ru,
            },
            on: listeners
        })
}
