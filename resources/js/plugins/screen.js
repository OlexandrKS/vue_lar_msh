import Vue from 'vue'

const screen = new Vue({
    data: () => ({
        object: window.screen
    }),

    computed: {
        isMobile () {
            return this.object.width <= 768
        }
    },

    methods: {
        onResizeListener () {
            this.object = window.screen
        }
    },

    created () {
        window.addEventListener('resize', this.onResizeListener)
    },

    destroyed () {
        window.removeEventListener('resize', this.onResizeListener)
    }
})

Vue.prototype.$screen = screen

export default screen
