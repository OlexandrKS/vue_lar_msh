import ApiClient from './axios'
import app from '../app'

ApiClient.onResponseError(error => {
    if (error.response.status === 401 && app.$store.getters['user/isAuthenticated']) {
        app.$store.dispatch('user/logout', {
            withoutRequest: true,
            redirectTo: null
        })
    }

    return Promise.reject(error)
})
