import Vue from 'vue'
import { Validator, install as VeeValidate } from 'vee-validate/dist/vee-validate.minimal.esm.js'
import ru from 'vee-validate/dist/locale/ru'
import { required, regex, digits, is, min, max, length, email, numeric } from 'vee-validate/dist/rules.esm.js'

Vue.use(VeeValidate)

const fullName = {
    getMessage: field => 'Введите имя и фамилию',
    validate: value => /^([0-9A-Za-zА-Яа-яЁё]+)(.*)(\s)([0-9A-Za-zА-Яа-яЁё]+\s?)$/ui.test(value)
}

const phone = {
    getMessage: field => 'Неверный формат данных',
    validate: value => /^(\+7\s\([0-9]{3}\)\s([0-9]{3})\s([0-9]{2})\s([0-9]{2}))$/.test(value)
}

Validator.extend('required', required)
Validator.extend('regex', regex)
Validator.extend('digits', digits)
Validator.extend('is', is)
Validator.extend('min', min)
Validator.extend('max', max)
Validator.extend('length', length)
Validator.extend('email', email)
Validator.extend('full_name', fullName)
Validator.extend('phone', phone)
Validator.extend('numeric', numeric)

Validator.localize('ru', ru)
Validator.localize({
    ru: {
        messages: {
            required: 'Поле обязательно для заполнения.',
            email: 'Поле должно быть действительным электронным адресом.',
            phone: 'Поле должно быть действительным номером телефона.'
        }
    }
})
