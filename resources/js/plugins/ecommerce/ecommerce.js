export default class Ecommerce
{
    collectProducts(cartItems){
        let products = []
        for(let i = 0; i < cartItems.length; i++){
            products.push({
                id: cartItems[i].offer.primary.nid.numeric_id,
                count: cartItems[i].quantity,
                price: cartItems[i].offer.active_price.value
            })
        }
        return products;
    }

    collectProductsNids(cartItems){
        let products = []
        for(let i = 0; i < cartItems.length; i++){
            products.push(cartItems[i].offer.primary.nid.numeric_id)
        }
        return products;
    }
}