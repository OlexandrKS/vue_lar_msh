import Ecommerce from '@/plugins/ecommerce/ecommerce'

class RatingMailRu extends Ecommerce
{
    pushProductEvent(el){
        window._tmr = window._tmr || (window._tmr = []);
        _tmr.push({
            type: 'itemView',
            pagetype: 'product',
            list: 1,
            productid:  el.dataset.numericId,
            totalvalue:  el.dataset.activePrice
        });
    }

    pushThankPageEvent(){
        window._tmr = window._tmr || (window._tmr = []);
        _tmr.push({
            type: 'itemView',
            pagetype: 'purchase',
            list: 1,
            productid: productNids,
            totalvalue: totalPrice
        });
    }

    pushCartEvent(productNids, totalPrice){
        window._tmr = window._tmr || (window._tmr = []);
        _tmr.push({
            type: 'itemView',
            pagetype: 'cart',
            list: 1,
            productid: productNids,
            totalvalue: totalPrice
        });
    }
}

const mailru = new RatingMailRu()

export default mailru