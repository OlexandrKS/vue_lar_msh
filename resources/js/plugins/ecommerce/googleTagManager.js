import Ecommerce from '@/plugins/ecommerce/ecommerce'

class GoogleTagManager extends Ecommerce {
    mapCheckoutProducts (products) {
        let mappedProducts = []
        for (let i = 0; i < products.length; i++) {
            mappedProducts.push({
                'id': products[i].offer.primary.id,
                'name': products[i].offer.primary.name + products[i].offer.primary.subname,
                'category': products[i].offer.primary.category.name
                    + (products[i].offer.primary.category.name ? '/'
                        + products[i].offer.primary.category.name : ''),
                'quantity': products[i].quantity,
            })
        }
        return mappedProducts
    }

    mapProducts (products) {
        let mappedProducts = []
        products.forEach(function(element) {
            const data = JSON.parse(element.dataset.ecommerceInfo)

            mappedProducts.push({
                'id': data.id,
                'price': data.price,
                'name': data.name,
                'category': data.category,
                'variant': data.variant,
            })
        })
        return mappedProducts
    }

    pushThankPageEvent (products) {
        if (products) {
            window.dataLayer = window.dataLayer || []
            dataLayer.push({
                'ecommerce': {
                    'checkout': {
                        'actionField': { 'step': 6 },
                        'products': this.mapCheckoutProducts(products),
                    },
                },
                'event': 'gtm-ee-event',
                'gtm-ee-event-category': 'Enhanced Ecommerce',
                'gtm-ee-event-action': 'Checkout Step 6',
                'gtm-ee-event-non-interaction': 'False',
            })
        }
    }

    pushProductDetailImpressionsEvent (product) {
        if (product) {
            window.dataLayer = window.dataLayer || []
            dataLayer.push({
                'ecommerce': {
                    'detail': {
                        'products': JSON.parse(product),
                    },
                },
                'event': 'gtm-ee-event',
                'gtm-ee-event-category': 'Enhanced Ecommerce',
                'gtm-ee-event-action': 'Product Details',
                'gtm-ee-event-non-interaction': 'True',
            })
        }
    }

    pushProductClickEvent (product) {
        if (product) {
            window.dataLayer = window.dataLayer || []
            dataLayer.push({
                'ecommerce': {
                    'click': {
                        'products': JSON.parse(product),
                    },
                },
                'event': 'gtm-ee-event',
                'gtm-ee-event-category': 'Enhanced Ecommerce',
                'gtm-ee-event-action': 'Product Clicks',
                'gtm-ee-event-non-interaction': 'False',
            })
        }
    }

    pushCartFormEvent (products) {
        window.dataLayer = window.dataLayer || []
        dataLayer.push({
            'ecommerce': {
                'add': {
                    'products': this.mapCheckoutProducts(products),
                },
            },
            'event': 'gtm-ee-event',
            'gtm-ee-event-category': 'Enhanced Ecommerce',
            'gtm-ee-event-action': 'Adding a Product to a Shopping Cart',
            'gtm-ee-event-non-interaction': 'False',
        })
    }

    pushRemoveCartFormEvent (product) {
        try {
            window.dataLayer = window.dataLayer || []
            dataLayer.push({
                'ecommerce': {
                    'remove': {
                        'products': [{
                            'id': product.offer.primary.id,
                            'name': product.offer.primary.name,
                            'category': product.offer.primary.category.name
                            + product.offer.primary.category.name ? '/'
                                + product.offer.primary.category.name : '',
                            'variant': product.offer.primary.subname,
                            'price': product.offer.active_price.value,
                            'list': 'catalog',
                        }],
                    },
                },
                'event': 'gtm-ee-event',
                'gtm-ee-event-category': 'Enhanced Ecommerce',
                'gtm-ee-event-action': 'Removing a Product from a Shopping Cart',
                'gtm-ee-event-non-interaction': 'False',
            })
        } catch ( e ) {
        }
    }

    pushProductChooserEvent (product) {
        try {
            // Ecommerce event for GTM. Product Clicks
            window.dataLayer = window.dataLayer || []
            dataLayer.push({
                'event': 'addToWishlist',
                'id': product.id,
                'name': product.name + ' ' + product.subname,
                'price': product.activePrice,
                'category': product.category.name + (product.subcategory.name ? '/' + product.subcategory.name : ''),
                'variant': product.subname,
            })
        } catch ( e ) {
        }
    }

    pushLandingCheckoutEvent (products) { //TODO вот тут провірити чи це ідентичний івент з картом
        // Ecommerce event for GTM. The product checkout data
        window.dataLayer = window.dataLayer || []
        dataLayer.push({
            'ecommerce': {
                'checkout': {
                    'products': this.mapCheckoutProducts(products),
                },
            },
            'event': 'gtm-ee-event',
            'gtm-ee-event-category': 'Enhanced Ecommerce',
            'gtm-ee-event-action': 'Checkout Step 1',
            'gtm-ee-event-non-interaction': 'False',
        })
    }

    pushLandingCheckoutInlineEvent (products) { //вот тут ваще хуй зна, нема меток гугловських
        // Ecommerce event for GTM. The product checkout data
        window.dataLayer = window.dataLayer || []
        dataLayer.push({
            'event': 'checkout',
            'ecommerce': {
                'checkout': {
                    'products': this.mapCheckoutProducts(products),
                },
            },
        })
    }

    pushProductCheckoutEvent (products) {
        // Ecommerce event for GTM. The product checkout data
        window.dataLayer = window.dataLayer || []
        dataLayer.push({
            'ecommerce': {
                'checkout': {
                    'actionField': { 'step': 1 },
                    'products': this.mapCheckoutProducts(products),
                },
            },
            'event': 'gtm-ee-event',
            'gtm-ee-event-category': 'Enhanced Ecommerce',
            'gtm-ee-event-action': 'Checkout Step 1',
            'gtm-ee-event-non-interaction': 'False',
        })
    }

    //ці штуки вішаються на клік наступного пункту
    GTMCartStep2 () {
        dataLayer.push({
            'ecommerce': {
                'checkout': {
                    'actionField': { 'step': 2 },
                },
            },
            'event': 'gtm-ee-event',
            'gtm-ee-event-category': 'Enhanced Ecommerce',
            'gtm-ee-event-action': 'Checkout Step 2',
            'gtm-ee-event-non-interaction': 'False',
        })
    }

    GTMCartStep3 () {
        dataLayer.push({
            'ecommerce': {
                'checkout': {
                    'actionField': { 'step': 3 },
                },
            },
            'event': 'gtm-ee-event',
            'gtm-ee-event-category': 'Enhanced Ecommerce',
            'gtm-ee-event-action': 'Checkout Step 3',
            'gtm-ee-event-non-interaction': 'False',
        })
    }

    GTMCartStep4 () {
        dataLayer.push({
            'ecommerce': {
                'checkout': {
                    'actionField': { 'step': 4 },
                },
            },
            'event': 'gtm-ee-event',
            'gtm-ee-event-category': 'Enhanced Ecommerce',
            'gtm-ee-event-action': 'Checkout Step 4',
            'gtm-ee-event-non-interaction': 'False',
        })
    }

    GTMCartStep5 () {
        dataLayer.push({
            'ecommerce': {
                'checkout': {
                    'actionField': { 'step': 5 },
                },
            },
            'event': 'gtm-ee-event',
            'gtm-ee-event-category': 'Enhanced Ecommerce',
            'gtm-ee-event-action': 'Checkout Step 5',
            'gtm-ee-event-non-interaction': 'False',
        })
    }

    //універсальна функція для всіх цих шагів
    pushStepCartEvent (step, products) {
        // Ecommerce event for GTM. The product checkout data
        try {
            window.dataLayer = window.dataLayer || []
            dataLayer.push({
                'ecommerce': {
                    'checkout': {
                        'actionField': { 'step': step },
                        'products': products,
                    },
                },
                'event': 'gtm-ee-event',
                'gtm-ee-event-category': 'Enhanced Ecommerce',
                'gtm-ee-event-action': 'Checkout Step ' + step,
                'gtm-ee-event-non-interaction': 'False',
            })
        } catch ( e ) {
        }
    }

    pushPromotionClickEvent () {
        try {
            // Ecommerce event for GTM. Promotion Clicks
            window.dataLayer = window.dataLayer || []
            dataLayer.push({
                'ecommerce': {
                    'promoClick': {
                        'promotions': [
                            {
                                'name': 'Together Cheaper: Buy Button',
                            },
                        ],
                    },
                },
                'event': 'gtm-ee-event',
                'gtm-ee-event-category': 'Enhanced Ecommerce',
                'gtm-ee-event-action': 'Promotion Clicks',
                'gtm-ee-event-non-interaction': 'False',
            })
        } catch ( e ) {
        }
    }

    pushAppearProductEvent (el) {
        try {
            // Ecommerce event for GTM. Product Impressions
            window.dataLayer = window.dataLayer || []
            dataLayer.push({
                'ecommerce': {
                    'impressions': [
                        JSON.parse(el),
                    ],
                },
                'event': 'gtm-ee-event',
                'gtm-ee-event-category': 'Enhanced Ecommerce',
                'gtm-ee-event-action': 'Product Impressions',
                'gtm-ee-event-non-interaction': 'True',
            })
        } catch ( e ) {
        }
    }

    pushPromotionClickEvent () {  //todo
        try {
            // Ecommerce event for GTM. Promotion Clicks
            window.dataLayer = window.dataLayer || []
            dataLayer.push({
                'ecommerce': {
                    'promoClick': {
                        'promotions': [
                            {
                                'name': 'Together Cheaper: Select Button',
                            },
                        ],
                    },
                },
                'event': 'gtm-ee-event',
                'gtm-ee-event-category': 'Enhanced Ecommerce',
                'gtm-ee-event-action': 'Promotion Clicks',
                'gtm-ee-event-non-interaction': 'False',
            })
        } catch ( e ) {
        }
    }

    pushPromotionImpressionsTabShowEvent () { //todo
        try {
            window.dataLayer = window.dataLayer || []
            dataLayer.push({
                'ecommerce': {
                    'promoView': {
                        'promotions': [
                            {
                                'name': 'Together Cheaper',
                            },
                        ],
                    },
                },
                'event': 'gtm-ee-event',
                'gtm-ee-event-category': 'Enhanced Ecommerce',
                'gtm-ee-event-action': 'Promotion Clicks',
                'gtm-ee-event-non-interaction': 'False',
            })
        } catch ( e ) {
        }
    }
}

const googleTagManager = new GoogleTagManager()

export default googleTagManager
