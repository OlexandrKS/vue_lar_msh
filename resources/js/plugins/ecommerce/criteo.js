import Ecommerce from '@/plugins/ecommerce/ecommerce'

class Criteo extends Ecommerce
{
    init (userMail) {
        window.criteo_q = window.criteo_q || [];
        var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/
            .test(navigator.userAgent) ? "m" : "d";
        window.criteo_q.push(
            { event: "setAccount", account: 56185 },
            { event: "setSiteType", type: deviceType },
            { event: "setEmail", email: userMail }
        )
    }

    generateRandomOrderId(){
        window.criteo_q = window.criteo_q || [];
        let n = Math.floor(Math.random() * 11);
        let k = Math.floor(Math.random() * 1000000);
        return String.fromCharCode(n) + k;
    }

    gatherCategoryProductIds(){
        window.criteo_q = window.criteo_q || [];
        let productCollection = document.getElementsByClassName('ecommerce_info');
        let productIds = [];
        for(let item of productCollection){
            productIds.push(item.dataset.numericId);
        }
        return productIds;
    }

    pushCartEvent(criteoProducts){
        window.criteo_q = window.criteo_q || [];
        window.criteo_q.push({ event: "viewBasket", item: criteoProducts });
    }

    pushThankPageEvent(criteoProducts){
        window.criteo_q = window.criteo_q || [];
        window.criteo_q.push({ event: "trackTransaction", id: this.generateRandomOrderId(), item: criteoProducts });
    }

    pushHomeEvent(){
        window.criteo_q = window.criteo_q || [];
        window.criteo_q.push({ event: "viewHome" });
    }

    pushProductEvent(productId){
        window.criteo_q = window.criteo_q || [];
        window.criteo_q.push({ event: "viewItem", item: productId });
    }

    pushCategoryEvent(productIds){
        window.criteo_q = window.criteo_q || [];
        window.criteo_q.push({ event: "viewList", item: productIds });
    }
}

const criteo = new Criteo()

export default criteo
