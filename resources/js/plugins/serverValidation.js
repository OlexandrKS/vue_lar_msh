import Vue from 'vue'

Vue.prototype.$serverValidation = function (errorResponse) {
    // only allow this function to be run if the validator exists
    if (!this.hasOwnProperty('$validator')) {
        return
    }

    // clear errors
    this.$validator.errors.clear()

    // check if errors exist
    if (!errorResponse.hasOwnProperty('errors')) {
        return
    }

    const errorFields = Object.keys(errorResponse.errors)

    // insert laravel errors
    for (let i = 0; i < errorFields.length; i++) {
        const field = errorFields[i]
        const errorString = errorResponse.errors[field].join(', ')

        this.$validator.errors.add({ field: field, msg: errorString })
    }
};
