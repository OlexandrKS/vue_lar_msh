import Vue from 'vue'
import VuModal, { ModalWrapper, Modal } from './vuModal'

Vue.use(VuModal)

Vue.component('vu-modal-wrapper', ModalWrapper)
Vue.component('vu-modal', Modal)
