import Vue from 'vue'

export const EventBus = new Vue()

Vue.prototype.$bus = EventBus

export default EventBus
