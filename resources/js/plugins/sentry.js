import Vue from 'vue'
import app from '@/app'

export let Sentry = {}

class SentryFallback {
    constructor () {
        this.events = []
    }

    captureException (exception) {
        this.events.push({
            type: 'captureException',
            args: [exception]
        })
    }

    captureMessage (message, level) {
        this.events.push({
            type: 'captureMessage',
            args: [message, level]
        })
    }

    captureEvent (event) {
        this.events.push({
            type: 'captureEvent',
            args: [event]
        })
    }

    configureScope (callback) {
        this.events.push({
            type: 'configureScope',
            args: [callback]
        })
    }

    withScope (callback) {
        this.events.push({
            type: 'withScope',
            args: [callback]
        })
    }

    addBreadcrumb (breadcrumb) {
        this.events.push({
            type: 'addBreadcrumb',
            args: [breadcrumb]
        })
    }

    process (instance) {
        let item

        while ((item = this.events.shift()) !== undefined) {
            instance[item.type].apply(instance, item.args)
        }
    }
}

const initSentry = instanse => {
    const oldValue = Sentry

    Sentry = instanse
    window.Sentry = instanse
    Vue.prototype.$sentry = instanse

    return oldValue
}

if (process.env.SENTRY_ENABLED === 'true' && process.env.SENTRY_FRONTEND_ENABLED === 'true') {
    Promise.all([
        import(/* webpackChunkName: "sentry" */ '@sentry/browser'),
        import(/* webpackChunkName: "sentry-integrations" */ '@sentry/integrations')
    ]).then(imports => {
        const Sentry = imports[0]
        const Integrations = imports[1]

        Sentry.init({
            dsn: process.env.SENTRY_FRONTEND_DSN,
            release: app.$store.state.version,
            environment: process.env.APP_ENV,
            integrations: [
                new Sentry.Integrations.UserAgent(),
                new Sentry.Integrations.GlobalHandlers({
                  onerror: true,
                  onunhandledrejection: true
                }),
                new Integrations.Vue({
                    Vue,
                    attachProps: true
                })
            ]
        })

        Sentry.configureScope(scope => {
            scope.setUser(app.$store.getters['user/sentry'])
        })

        const fallback = initSentry(Sentry)
        fallback.process(Sentry)
    })
}

initSentry(new SentryFallback())
