export default {
    '/catalog/:category/:product': () => import(/* webpackChunkName: `page-product` */ './pages/product')
        .then(page => page.default()),

    '/catalog/:slug': () => import(/* webpackChunkName: `page-category` */ './pages/category')
        .then(page => page.default()),

    '/checkout': () => import(/* webpackChunkName: `checkout` */ './pages/checkout')
        .then(page => page.default()),

    '/authentication': () => import(/* webpackChunkName: `auth-page` */ './pages/auth')
        .then(page => page.default()),

    '/contacts': () => import(/* webpackChunkName: `contacts-page` */ './pages/contacts')
        .then(page => page.default()),

    '/': () => import(/* webpackChunkName: `home-page` */ './pages/home')
        .then(page => page.default()),
}
