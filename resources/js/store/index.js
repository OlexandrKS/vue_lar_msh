import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist/dist/umd'
import createMutationsSharer from 'vuex-shared-mutations'

Vue.use(Vuex)

import cart from './cart'
import user from './user'
import promotions from './promotions'
import favoriteList from './favoriteList'
import popup from './popup'
import headBanner from './headBanner'

const modules = {
    cart,
    user,
    promotions,
    favoriteList,
    popup,
    headBanner,
}

const localState = new VuexPersistence({
    filter: mutation => typeof mutation.payload !== 'function',
})

export default new Vuex.Store({
    strict: process.env.NODE_ENV !== 'production',
    state: {
        version: 'modern'
    },
    mutations: {},
    actions: {},
    modules,
    plugins: [
        localState.plugin,
        createMutationsSharer({
            predicate: [
                'user/setUser',
                'favoriteList/sync',
                'favoriteList/add',
                'favoriteList/remove',
                'cart/sync',
                'cart/add',
                'cart/remove',
                'cart/increment',
                'cart/decrement',
            ]
        })
    ]
})
