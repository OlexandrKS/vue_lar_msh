export default {
    isShowBuyTodayDiscount (state) {
        return Math.floor(Date.now() / 1000) <= state.buy_today_discount.expired && ! state.buy_today_discount.used
    },

    isShowFreeDelivery30Plus (state, getters, rootState, rootGetters) {
        return Math.floor(Date.now() / 1000) <= state.free_delivery_30_plus.expired &&
            ! state.free_delivery_30_plus.used &&
            rootGetters['cart/totalPrice'] >= state.free_delivery_30_plus.fromPrice
    },

    active (state, getters) {
        if (getters.isShowFreeDelivery30Plus) {
            return {
                free_delivery_30_plus: true
            }
        }

        return {}
    }
}
