import Cookies from 'js-cookie'

export default {
    parseBuyTodayDiscountFromJs ({ commit }) {
        const cookieName = 'fb_cal'
        const cookieValue = Cookies.get(cookieName)

        let data =  {
            expired: 1,
            used: true
        }

        if (cookieValue !== undefined) {
            data = JSON.parse(cookieValue)
        }

        commit('updateBuyTodayDiscount', data)
    },

    parseFreeDelivery30plusFromJs ({ commit, getters, dispatch }) {
        const cookieName = 'fd_cal'
        const cookieValue = Cookies.get(cookieName)

        let data =  {
            expired: 1,
            used: true
        }

        if (cookieValue !== undefined) {
            data = JSON.parse(cookieValue)
        }

        commit('updateFreeDelivery', data)

        if (!getters.isShowFreeDelivery30Plus) {
            dispatch('headBanner/showStaticBanner', {}, { root: true })
        }
    }
}
