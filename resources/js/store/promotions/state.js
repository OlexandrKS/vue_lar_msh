export default () => ({
    buy_today_discount: {
        expired: null,
        used: null
    },

    free_delivery_30_plus: {
        expired: null,
        used: null,
        fromPrice: 30000,
    },
})
