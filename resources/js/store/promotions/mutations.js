export default {
    updateBuyTodayDiscount(state, { expired, used }) {
        state.buy_today_discount.expired = expired
        state.buy_today_discount.used = used
    },

    updateFreeDelivery(state, { expired, used }) {
        state.free_delivery_30_plus.expired = expired
        state.free_delivery_30_plus.used = used
    }
}
