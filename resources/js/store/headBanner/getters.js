import HeadBannerStatic from '@/components/Banner/HeadBannerStatic'
import FreeDeliveryHeadBanner from '@/components/Banner/FreeDeliveryHeadBanner'

const components = {
    staticBanner: HeadBannerStatic,
    freeDeliveryTimer: FreeDeliveryHeadBanner
}

export default {
    component (state) {
        return components[state.type]
    }
}
