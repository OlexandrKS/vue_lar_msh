import Vue from 'vue'

export default {
    showStaticBanner (state, banner) {
        Vue.set(state, 'banner', banner)
        state.isShow = true
    },

    showFreeDelivery (state) {
        state.isShow = true
        state.type = 'freeDeliveryTimer'
        state.banner = {}
    },

    setType (state, type) {
        state.type = type
    }
}
