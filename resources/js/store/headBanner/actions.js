import ApiClient from '@/plugins/axios'

export default {
    async getStaticBanner ({ state, commit }) {
        if (state.type === 'staticBanner') {
            const url = btoa(window.location.pathname)
            const response = await ApiClient.$get(`banner/${url}`)

            commit('showStaticBanner', response.data)
        }
    },

    showStaticBanner ({ commit, dispatch }) {
        commit('setType', 'staticBanner')
        dispatch('getStaticBanner')
    }
}
