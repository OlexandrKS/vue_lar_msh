import { ApiClient } from '@/plugins/axios'
import { Sentry } from '@/plugins/sentry'

export default {
    async syncBackend ({ commit }) {
        try {
            const response = await ApiClient.$get('/favorite-list')

            commit('sync', response.data)
        } catch (e) {
            Sentry.captureException(e)
        }
    },

    async pickFavoriteItem ({ commit }, { offerId }) {
        try {
            const response = await ApiClient.$post('/favorite-list', { offerId })
            const favoriteListItem = response.data

            commit('add', { favoriteListItem })
        } catch (e) {
            Sentry.captureException(e)
        }
    },

    async removeFavoriteItem ({ commit, getters }, { offerId } ) {
        try {
            await ApiClient.$delete(`/favorite-list/${offerId}`)
            let index = getters.indexOfOfferId(offerId)

            commit('remove', index)
        } catch (e) {
            Sentry.captureException(e)
        }
    },

    cleanFavoriteListLocally ({ commit }) {
        commit()
    }
}

