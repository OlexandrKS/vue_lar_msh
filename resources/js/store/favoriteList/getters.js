export default {
    countItems (state) {
        if (state.items.length) {
            return state.items.length;
        }
        return 0;
    },
    indexOfOfferId (state) {
        return offerId =>
            state.items.findIndex(item => item.offer.id === offerId)
    }
}
