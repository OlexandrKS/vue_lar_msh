import Vue from 'vue'

export default {
    add (state, { favoriteListItem }) {
        return state.items.push(favoriteListItem) - 1
    },

    remove (state, index) {
        state.items.splice(index, 1)
    },

    sync (state, items = []) {
        Vue.set(state, 'items', items)
    },

    clean (state) {
        Vue.set(state, 'items', [])
    }
}
