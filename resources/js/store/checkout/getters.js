import trim from 'lodash/trim'

export default {
    isValidUserDate (state) {
        return trim(state.userData.phone).length > 0
    },

    calculationData (state, getters, rootState, rootGetters) {
        return {
            items: rootState.cart.items,
            paymentMethod: state.paymentData.activeMethod,
            assembling: state.isAssembling,
            deliveryType: state.deliveryData.type,
            promotions: {
                ...state.promotions,
                ...rootGetters['promotions/active']
            },
        }
    },

    paymentData (state, getters) {
        const method = state.paymentData.activeMethod

        if (method === 'invoice') {
            return {
                method,
                ...getters.invoiceData
            }
        }

        return {
            method,
            ...state.paymentData[method]
        }
    },

    invoiceData (state) {
        const address = state.invoiceUseDeliveryAddress ? state.addressSeparated : {}

        return {
            ...state.paymentData.invoice,
            ...address
        }
    }
}
