import { ApiClient } from '@/plugins/axios'
import { Sentry } from '@/plugins/sentry'
import { EventBus } from '@/plugins/eventBus'
import debounce from 'lodash/debounce'

export default {
    calculate: debounce(async function ({ commit, getters }) {
        try {
            const response = await ApiClient.$post('/order/calculate', getters.calculationData)

            commit('persistCalculation', response.data)
        } catch ( e ) {
            Sentry.captureException(e)
        }
    }, 500),

    async create ({ state, getters, rootState }) {
        try {
            const response = await ApiClient.$post('/checkout', {
                promotions: state.promotions,
                account: state.userData,
                items: rootState.cart.items,
                paymentData: getters.paymentData,
                deliveryData: state.deliveryData,
                comment: state.comment
            })

            EventBus.$emit('checkout.created', response.data)
        } catch ( e ) {
            Sentry.captureException(e)
        }
    },

    async uncompleted ({ state, rootState }) {
        await ApiClient.$post('/bpm/left-checkout', {
            items: rootState.cart.items,
            profile: state.userData,
        })
    }
}
