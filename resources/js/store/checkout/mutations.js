export default {
    persistUserData (state, { name, phone, email }) {
        state.userData.name = name
        state.userData.phone = phone
        state.userData.email = email
    },

    presetCalculationFromCart (state, { total, delivery }) {
        state.calculated.total = total
        state.calculated.delivery = delivery
        state.calculated.amount = total + delivery
    },

    persistCalculation (state, calculation) {
        state.calculated.amount = calculation.amount
        state.calculated.total = calculation.total
        state.calculated.withoutPromotions = calculation.withoutPromotions
        state.calculated.promoDiscounts = calculation.promoDiscounts
        state.calculated.assembling = calculation.assembling
        state.calculated.delivery = calculation.delivery
    },

    changeAssembling (state) {
        state.isAssembling = ! state.isAssembling
    },

    persistDeliveryType (state, type) {
        state.deliveryData.type = type
    },

    persistDeliveryData (state, { type, address, city, street, house, apartment }) {
        const addressValue = address || `${city} ${street} ${house} ${apartment}`

        state.deliveryData.type = type
        state.deliveryData.address = addressValue

        state.addressSeparated.city = city
        state.addressSeparated.street = street
        state.addressSeparated.house = house
        state.addressSeparated.apartment = apartment
    },

    persistPaymentMethod (state, method) {
        state.paymentData.activeMethod = method
    },

    persistCreditData (state, data) {
        state.paymentData.credit.firstInstallment = data.firstInstallment
        state.paymentData.credit.loanTerms = data.loanTerms
        state.paymentData.credit.fullName = data.fullName
        state.paymentData.credit.surname = data.surname
        state.paymentData.credit.phone = data.phone
        state.paymentData.credit.passport = data.passport
    },

    persistCartData (state, prePayment) {
        state.paymentData.card.prePayment = prePayment
    },

    persistInvoiceUseDeliveryAddress (state, value) {
        state.invoiceUseDeliveryAddress = !!value
    },

    persistInvoiceData (state, invoiceData) {
        state.paymentData.invoice.bankName = invoiceData.bankName
        state.paymentData.invoice.bik = invoiceData.bik
        state.paymentData.invoice.inn = invoiceData.inn
        state.paymentData.invoice.city = invoiceData.city
        state.paymentData.invoice.street = invoiceData.street
        state.paymentData.invoice.house = invoiceData.house
        state.paymentData.invoice.apartment = invoiceData.apartment
        state.paymentData.invoice.authPerson = invoiceData.authPerson
        state.paymentData.invoice.deliveryPerson = invoiceData.deliveryPerson
    },

    persistComment (state, value) {
        state.comment = value
    },

    persistUserBonuses (state, value) {
        state.promotions.user_bonuses = parseInt(value)
        state.promotions.promo_code = null
    },

    persistPromoCode (state, value) {
        state.promotions.promo_code = value.toString()
        state.promotions.user_bonuses = 0
    },

    setFirstStep(state, value) {
        state.isFirstStep = value
    }
}
