export default () => ({
    userData: {
        name: '',
        phone: '+7 (',
        email: ''
    },
    deliveryData: {
        type: 0,
        address: '',
    },
    paymentData: {
        activeMethod: 'card',
        credit: {
            firstInstallment: 0,
            loanTerms: 6,
            fullName: '',
            surname: '',
            phone: '+7 (',
            passport: ''
        },
        card: {
            prePayment: 0,
        },
        invoice: {
            bankName: '',
            bik: '',
            inn: '',
            city: '',
            street: '',
            house: '',
            apartment: '',
            authPerson: '',
            deliveryPerson: '',
        }
    },
    promotions: {
        promo_code: null,
        user_bonuses: 0,
    },
    comment: '',

    calculated: {
        amount: 0,
        total: 0,
        withoutPromotions: 0,
        promoDiscounts: 0,
        assembling: 0,
        delivery: 0,
    },
    addressSeparated: {
        city: null,
        street: null,
        house: null,
        apartment: null,
    },
    isAssembling: false,
    invoiceUseDeliveryAddress: false,
    isFirstStep: true
})
