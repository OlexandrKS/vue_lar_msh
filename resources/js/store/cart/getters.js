export default {
    countItems (state) {
        if (state.items.length) {
            let quantity = state.items.map(item => {
                return parseInt(item.quantity)
            });
            return quantity.reduce((a, b) => a + b, 0);
        }

        return 0;
    },

    totalPrice (state) {
        let totalPrice = state.items.map(item => {
            return parseInt(item.quantity * item.offer.active_price.value)
        });

        return totalPrice.reduce((a, b) => a + b, 0);
    },

    deliveryCost (state) {
        let deliveryCost = state.items.map(item => {
            let costDeliveryIndex = item.offer.prices.findIndex(price => price.type === 'delivery')
            return parseInt(item.quantity) * parseInt(item.offer.prices[costDeliveryIndex].value)
        });

        return deliveryCost.reduce((a, b) => a + b, 0);
    },

    costAssembling (state) {
        let costAssembling = state.items.map(item => {
            let costAssemblingIndex = item.offer.prices.findIndex(price => price.type === 'assembling')
            return parseInt(item.quantity) * parseInt(item.offer.prices[costAssemblingIndex].value)
        });

        return costAssembling.reduce((a, b) => a + b, 0);
    },

    indexOfCartItem (state) {
        return cartItem =>
            state.items.findIndex(item =>
                item.id === cartItem.id || item.offer.id === cartItem.offer.id)
    },

    indexOfOfferId (state) {
        return offerId =>
            state.items.findIndex(item => item.offer.id === offerId)
    }
}
