import { ApiClient } from '@/plugins/axios'
import { Sentry } from '@/plugins/sentry'
import { EventBus } from '@/plugins/eventBus'

export default {
    async syncBackend ({ commit, state }) {
        try {
            const response = await ApiClient.$post('/cart/sync', {
                items: state.items
            })

            commit('sync', response.data)

            EventBus.$emit('chat.changed')
        } catch (e) {
            Sentry.captureException(e)
        }
    },

    async pickOffer ({ commit, getters }, { offerId, withoutCart = false }) {
        try {
            const response = await ApiClient.$post('/cart', {
                offer: {
                    id: offerId
                }
            })
            const cartItem = response.data

            let index = getters.indexOfCartItem(cartItem)

            if (index === -1) {
                commit('add', { cartItem })
            } else {
                commit('increment', { offerId: cartItem.offer.id })
            }

            EventBus.$emit('chat.changed')

            if (! withoutCart) {
                EventBus.$emit('cart.modal')
            }
        } catch (e) {
            Sentry.captureException(e)
        }
    },

    async updateOffer({ commit }, { cartItem, operation }) {
        try {
            commit(operation, { offerId: cartItem.offer.id })

            await ApiClient.$put('/cart', cartItem)

            EventBus.$emit('chat.changed')
        } catch (e) {
            Sentry.captureException(e)
        }
    },

    async removeOffer ({ commit, getters }, { cartItem } ) {
        try {
            await ApiClient.$delete('/cart', { data: { id: cartItem.id }})

            let index = getters.indexOfCartItem(cartItem)

            commit('remove', index)

            EventBus.$emit('chat.changed')
        } catch (e) {
            Sentry.captureException(e)
        }
    }
}
