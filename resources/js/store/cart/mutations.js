import Vue from 'vue'
import getters from './getters'
import { EventBus } from '@/plugins/eventBus'
import difference from 'lodash/difference'

export default {
    add (state, { cartItem }) {
        const index = state.items.push(cartItem) - 1

        EventBus.$emit('cart.changed')

        return index
    },

    remove (state, index) {
        state.items.splice(index, 1)

        EventBus.$emit('cart.changed')
    },

    increment (state, { offerId }) {
        const index = getters.indexOfOfferId(state)(offerId)

        state.items[index].quantity++

        EventBus.$emit('cart.changed')
    },

    decrement (state, { offerId }) {
        const index = getters.indexOfOfferId(state)(offerId)

        state.items[index].quantity--

        EventBus.$emit('cart.changed')
    },

    sync (state, items = []) {
        const mapCallback = cartItem => cartItem.id
        const currentIds = state.items.map(mapCallback)
        const newIds = items.map(mapCallback)
        const diff = difference(currentIds, newIds)

        Vue.set(state, 'items', items)

        if (diff.length > 0) {
            EventBus.$emit('cart.changed')
        }
    },

    erase (state) {
        Vue.set(state, 'items', [])
    }
}
