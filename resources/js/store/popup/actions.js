import { ApiClient } from '@/plugins/axios'
import { Sentry } from '@/plugins/sentry'

export default {
    async fetch ({ commit, state }) {
        try {
            const popupIds = btoa(sessionStorage.getItem('closePagePopup'))

            const response = await ApiClient.$get(`/popup/${popupIds}`)

            commit('setPopup', { popup: response.data ? response.data : null })
        } catch ( e ) {
            Sentry.captureException(e)
        }
    },

    async sendPromoCode ({ state }, { email }) {
        try {
            return await ApiClient.$post('/send', { email: email, id: state.popup.id })
        } catch ( e ) {
            Sentry.captureException(e)
        }
    },
}
