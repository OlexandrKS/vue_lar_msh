import get from 'lodash/get'

export default {
    id(state) {
        return get(state, 'popup.id', '')
    },
    image(state) {
        return get(state, 'popup.image_path', '')
    },
    imageUrl(state) {
        return get(state, 'popup.image_url', '')
    },
    message(state) {
        return get(state, 'popup.message', '')
    },
    variantFooter(state) {
        return get(state, 'popup.variant_footer_popup', '')
    },
    leftBtnId(state) {
        return get(state, 'popup.left_button_id', '')
    },
    leftBtnColor(state) {
        return get(state, 'popup.left_button_color', '')
    },
    leftBtnLink(state) {
        return get(state, 'popup.left_button_link', '')
    },
    leftBtnText(state) {
        return get(state, 'popup.left_button_text', '')
    },
    rightBtnId(state) {
        return get(state, 'popup.right_button_id', '')
    },
    rightBtnColor(state) {
        return get(state, 'popup.right_button_color', '')
    },
    rightBtnText(state) {
        return get(state, 'popup.right_button_text', '')
    },
    textSubscribe(state) {
        return get(state, 'popup.text_subscribe', '')
    },
    type(state) {
        return get(state, 'popup.type', '')
    },
    timeout(state) {
        return get(state, 'popup.timeout', '')
    },
}
