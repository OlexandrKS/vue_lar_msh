import get from 'lodash/get'

export default {
    isAuthenticated (state) {
        return !!state.user
    },

    sentry (state, getters) {
        if (!getters.isAuthenticated) {
            return {}
        }

        return {
            id: state.user.id,
            fullName: getters.fullName,
            email: getters.email,
            phone: getters.phone,
        }
    },

    checkoutData (state, getters) {
        return {
            name: getters.fullName,
            email: getters.email,
            phone: getters.phone,
        }
    },

    name (state) {
        return get(state, 'user.name', '')
    },

    lastName (state) {
        return get(state, 'user.last_name', '')
    },

    email (state) {
        return get(state, 'user.email', '')
    },

    phone (state) {
        return get(state, 'user.phone', '')
    },

    fullName (state, getters) {
        return `${getters.name} ${getters.lastName}`
    },

    surname (state) {
        return get(state, 'user.other_name', '')
    },

    cardCode (state) {
        return get(state, 'user.card_code', '')
    },

    gender (state) {
        return get(state, 'user.gender_id', '')
    },

    birthDay (state) {
        return get(state, 'user.birth_date', '')
    },

    activatedBonuses (state) {
        return get(state, 'user.bonuses.activated', 0)
    },

    accruedBonuses (state) {
        return get(state, 'user.bonuses.accrued', 0)
    },

    chargedBonuses (state) {
        return get(state, 'user.bonuses.charged', 0)
    },

    deactivatedBonuses (state) {
        return get(state, 'user.bonuses.deactivated', 0)
    },

    promoCodes (state) {
        return get(state, 'promoCodes', null)
    },

    promoCodesAvailable (state) {
        return get(state, 'promoCodesAvailable', null)
    },

    bonusesEvent (state) {
        return get(state, 'bonusesEvent', null)
    },
    socialGoogle(state) {
        return state.social.find(item => item.provider_id === 'google')
    },
    socialFacebook(state) {
        return state.social.find(item => item.provider_id === 'facebook')
    },
    socialVk(state) {
        return state.social.find(item => item.provider_id === 'vkontakte')
    },
    orders (state) {
        return get(state, 'orders', [])
    }
}
