export default () => ({
    user: null,
    social: [],
    afterSuccessAuthCallbacks: [],
    promo_codes: null,
    orders: []
})
