import { ApiClient } from '@/plugins/axios'
import { Sentry } from '@/plugins/sentry'

export default {
    async authenticate ({ commit }, { login, password }) {
        try {
            const response = await ApiClient.$post('/auth/login', {
                login,
                password,
            })

            commit('setUser', {
                user: response.data,
            })
        } catch ( e ) {
            throw e.response.data
        }
    },

    async logout ({ commit }, { withoutRequest = false, redirectTo = '/' }) {
        try {
            if (withoutRequest) {
                await ApiClient.$post('/auth/logout')
            }

            commit('setUser', { user: null })
            commit('favoriteList/clean', null, { root: true })

            if (redirectTo) {
                window.location.replace(redirectTo)
            }
        } catch ( e ) {
            throw e.response.data
        }
    },

    async emitSuccessAuth ({ state, commit }) {
        let callback

        while ((callback = state.afterSuccessAuthCallbacks.shift()) !== undefined) {
            if (typeof callback !== 'function') {
                continue
            }

            const breakPoint = await callback(state.user)

            if (breakPoint) {
                break
            }
        }

        commit('clearAfterSuccessAuthCallbacks')
    },

    async promoCode ({ commit, rootGetters }) {
        if (!rootGetters['user/isAuthenticated']) {
            return
        }

        try {
            const response = await ApiClient.$get('/account/promo-code')
            commit('setPromoCodes', {
                promoCodes: response.data,
            })
        } catch ( e ) {
            Sentry.captureException(e)
        }
    },

    async promoCodeAvailable ({ commit, rootGetters }) {
        if (!rootGetters['user/isAuthenticated']) {
            return
        }

        try {
            const response = await ApiClient.$get('/account/promo-code/available')

            commit('setPromoCodesAvailable', {
                promoCodes: response.data,
            })
        } catch ( e ) {
            Sentry.captureException(e)
        }
    },

    async bonusesEvent ({ commit, rootGetters }) {
        if (!rootGetters['user/isAuthenticated']) {
            return
        }

        try {
            const response = await ApiClient.$get('/account/bonus-event')

            commit('setBonusesEvent', {
                bonusesEvent: response.data,
            })
        } catch ( e ) {
            Sentry.captureException(e)
        }
    },

    async info ({ commit }) {
        try {
            const response = await ApiClient.$get('/account/profile')

            commit('setUser', {
                user: response.data,
            })
        } catch ( e ) {
            Sentry.captureException(e)
        }
    },

    async edit ({ commit }, { data }) {
        try {
            const response = await ApiClient.$post('/account/profile', data)

            commit('setUser', {
                user: response.data,
            })
        } catch ( e ) {
            Sentry.captureException(e)
        }
    },

    async changePassword ({ commit }, { credentials }) {
        try {
            await ApiClient.$post('/account/change-password', credentials)
        } catch ( e ) {
            Sentry.captureException(e)
        }
    },

    async social ({ commit, rootGetters }) {
        if (!rootGetters['user/isAuthenticated']) {
            return
        }

        try {
            const response = await ApiClient.$get('/account/social')
            commit('setSocial', {
                social: response.data,
            })
        } catch ( e ) {
            Sentry.captureException(e)
        }
    },

    async socialDestroy ({ commit, state, rootGetters }, { socialId }) {
        if (!rootGetters['user/isAuthenticated']) {
            return
        }

        try {
            await ApiClient.$delete('/account/social', { data: { id: socialId } })

            let socialIndex = state.social.findIndex(item => item.id === socialId)

            commit('removeSocial', { index: socialIndex })
        } catch ( e ) {
            Sentry.captureException(e)
        }
    },

    async prodBoard ({ rootGetters }) {
        if (!rootGetters['user/isAuthenticated']) {
            return
        }

        try {
            return await ApiClient.$get('/account/prod-board')
        } catch ( e ) {
            Sentry.captureException(e)
        }
    },

    async order ({ commit, rootGetters }) {
        if (!rootGetters['user/isAuthenticated']) {
            return
        }
        try {
            const response = await ApiClient.$get('/account/order')

            commit('setOrders', { orders: response.data })
        } catch ( e ) {
            Sentry.captureException(e)
        }
    },
}
