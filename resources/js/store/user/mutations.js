import Vue from 'vue'

export default {
    setUser (state, { user }) {
        state.user = user
    },

    addAfterSuccessAuthCallback (state, callback) {
        state.afterSuccessAuthCallbacks.push(callback)
    },

    clearAfterSuccessAuthCallbacks (state) {
        Vue.set(state, 'afterSuccessAuthCallbacks', [])
    },

    setPromoCodes(state, { promoCodes }) {
        state.promoCodes = promoCodes
    },

    setPromoCodesAvailable(state, { promoCodes }) {
        state.promoCodesAvailable = promoCodes
    },

    setBonusesEvent(state, { bonusesEvent }) {
        state.bonusesEvent = bonusesEvent
    },
    setSocial(state, { social }) {
        state.social = social
    },
    removeSocial (state, index) {
        state.social.splice(index, 1)
    },
    setOrders (state, {orders}) {
        state.orders = orders
    },
}
