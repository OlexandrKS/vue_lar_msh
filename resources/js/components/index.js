import { MsCartButton, MsBuyButton, MsProdBoardButton } from './Cart'
import MsAuthButton from './Auth/MsAuthButton'
import { MsSearch } from './Search'
import {MsFavoriteListButton, MsFavoriteButton} from './FavoriteList'
import { MsMobileMenu, MsNavigationMenu } from './Navigation'
import MsHeadBanner from '@/components/Banner/MsHeadBanner'
import MsPopup from './PopUp/MsPopup'
import MsCountdown from './Countdown'
import { MsReviewLikes } from './Review'

export default {
    MsAuthPage: () => import(/* webpackChunkName: "auth-page" */'./Auth/MsAuthPage'),
    MsCartButton,
    MsAuthButton,
    MsSearch,
    MsCheckout: () => import(/* webpackChunkName: "checkout-page" */'./Checkout'),
    MsFavoriteListButton,
    MsFavoriteButton,
    MsBuyButton,
    MsProdBoardButton,
    MsTogetherCheaperWrap: () => import(/* webpackChunkName: "together-cheaper-wrap" */'./Product/TogetherCheaper/MsTogetherCheaperWrap'),
    MsProfile: () => import(/* webpackChunkName: "profile-page" */'./Account/Profile/MsProfile'),
    MsPromoCodes: () => import(/* webpackChunkName: "promo-codes-page" */'./Account/PromoCodes/MsPromoCodes'),
    MsBonuses: () => import(/* webpackChunkName: "bonuses-page" */'./Account/Bonuses/MsBonuses'),
    MsOrderHistory: () => import(/* webpackChunkName: "order-history" */'./Account/OrderHistory/MsOrderHistory'),
    MsNavigationMenu,
    MsMobileMenu,
    MsEmployeeCities: () => import(/* webpackChunkName: "vacancy-cities" */'./Employee/MsEmployeeCities'),
    MsEmployeeModal: () => import(/* webpackChunkName: "review-form" */'./Employee/MsEmployeeModal'),
    MsTogetherCheaperModel: () => import(/* webpackChunkName: "together-cheaper-modal" */'./Product/TogetherCheaper/MsTogetherCheaperModal'),
    MsReviewModal: () => import(/* webpackChunkName: "review-form" */'./Review/MsReviewModal'),
    MsInstallmentCountdown: () => import(/* webpackChunkName: "instalments-page" */'./Installment/MsInstallmentCountdown'),
    MsHeadBanner,
    MsPopup,
    MsPromoCodeActivation: () => import(/* webpackChunkName: "promo-code-activation-page" */'./PromoCodeActivation/MsPromoCodeActivation'),
    MsCountdown,
    MsReviewLikes
}
