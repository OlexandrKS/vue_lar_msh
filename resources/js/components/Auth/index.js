import MsAuthWrapper from './MsAuthWrapper.vue'
import MsAuthPage from './MsAuthPage.vue'
import MsAuthModal from './MsAuthModal.vue'
import MsAuthButton from './MsAuthButton.vue'

export {
    MsAuthWrapper,
    MsAuthPage,
    MsAuthModal,
    MsAuthButton
}

export default MsAuthPage
