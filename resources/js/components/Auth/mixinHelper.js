export default {
    methods: {
        onBlur(activeClass, field) {
            this[activeClass] = this[field].length > 0 ? "active" : ""
        }
    },
    directives: {
        numbersOnly: {
            bind(el) {
                el.oninput = function (e) {
                    if (!e.isTrusted) {
                        return;
                    }
                    let x = this.value.replace(/\D/g, '');
                    this.value = x;
                    el.dispatchEvent(new Event('input'));
                }
            }
        },
        directives:{
            focus: {
                // directive definition
                inserted: function (el) {
                    el.focus()
                }
            }
        },
    }

}