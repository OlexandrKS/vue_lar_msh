import MsFavoriteListButton from './MsFavoriteListButton'
import MsFavoriteButton from './MsFavoriteButton'
export {
    MsFavoriteListButton,
    MsFavoriteButton
}

export default MsFavoriteListButton
