import MsNavigationMenu from './MsNavigationMenu'
import MsMobileMenu from './MsMobileMenu'

export {
    MsNavigationMenu,
    MsMobileMenu
}

export default MsNavigationMenu
