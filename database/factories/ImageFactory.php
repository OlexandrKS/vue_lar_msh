<?php

use Faker\Generator as Faker;
use LuckyWeb\MS\Models\Image;

/* @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Image::class, function (Faker $faker) {
    return [
        'remote_name'     => $faker->words(14, true),
        'local_name'      => $faker->words(12, true),
        'remote_time_mdt' => $faker->date(),
        'remote_size'     => $faker->randomNumber(),
        'short_descr'     => 'desc',
        'assignment'      => 0,
    ];
});
