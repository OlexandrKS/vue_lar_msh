<?php

use App\Domain\Entities\Accounting\CartItem;
use App\Domain\Entities\Catalog\Offer;
use Faker\Generator as Faker;
use LuckyWeb\User\Models\User;

/* @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(CartItem::class, function (Faker $faker) {
    /** @var Offer $offer */
    $offer = factory(Offer::class)->create();
    $offer->products()->sync([
        $offer->primary_id => [
            'quantity' => 1,
        ],
    ]);

    return [
        'offer_tid'  => $offer->getKey(),
        'offer_type' => $offer->type,
        'quantity'   => 1,
        'promotions' => [],
        'user_id'    => factory(User::class)->create()->getKey(),
    ];
});
