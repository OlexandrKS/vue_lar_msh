<?php

use Faker\Generator as Faker;
use LuckyWeb\User\Models\User;

/* @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(User::class, function (Faker $faker) {
    return [
        'name'                  => $faker->firstName,
        'last_name'             => $faker->lastName,
        'email'                 => $email = $faker->safeEmail,
        'password'              => 'password',
        'password_confirmation' => 'password',
        'card_code'             => $faker->numberBetween(0, 9999999),
        'card_pin'              => $faker->numberBetween(1111, 9999),
        'is_activated'          => true,
        'username'              => $email,
    ];
});
