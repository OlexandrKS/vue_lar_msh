<?php

use Faker\Generator as Faker;
use LuckyWeb\MS\Models\Category;

/* @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Category::class, function (Faker $faker) {
    return [
        'name'       => $faker->colorName,
        'price_from' => $faker->randomFloat(2, 10000, 90000),
        'slug'       => $faker->colorName,
        'active'     => 1,
    ];
});
