<?php

use App\Domain\Entities\Catalog\Offer;
use App\Domain\Entities\Catalog\OfferProperties\SingleProperties;
use App\Domain\Entities\Catalog\Price;
use App\Domain\Enums\Catalog\OfferType;
use App\Domain\Enums\Catalog\PriceType;
use Faker\Generator as Faker;
use LuckyWeb\MS\Models\Product;
use October\Rain\Database\Collection;

/* @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Offer::class, function (Faker $faker) {
    /** @var Product $product */
    $product = factory(Product::class)->create();

    return [
        'primary_id' => $product->getKey(),
        'type'       => OfferType::SINGLE,
        'properties' => new SingleProperties(),
        'prices'     => function () use ($product) {
            $collection = new Collection();

            foreach ([null, $product->getKey()] as $productId) {
                $collection->push(new Price([
                    'type'    => PriceType::TOP,
                    'value'   => (float) $product->price_top_b,
                    'product' => $productId,
                ]));

                $collection->push(new Price([
                    'type'    => PriceType::LOWER,
                    'value'   => (float) $product->price_top_b,
                    'product' => $productId,
                ]));

                $collection->push(new Price([
                    'type'    => PriceType::ASSEMBLING,
                    'value'   => (float) $product->getAssemblingCost(),
                    'product' => $productId,
                ]));

                $collection->push(new Price([
                    'type'    => PriceType::DELIVERY,
                    'value'   => (float) $product->getDeliveryCost(),
                    'product' => $productId,
                ]));
            }

            return $collection;
        },
    ];
});
