<?php

use Carbon\Carbon;
use Faker\Generator as Faker;
use LuckyWeb\User\Models\PromoCode;

/* @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(PromoCode::class, function (Faker $faker) {
    return [
        'name'           => $faker->name,
        'code'           => $faker->words(5, true),
        'type'           => PromoCode::TYPE_SALE,
        'amount'         => $faker->randomFloat(0, 1000, 3000),
        'available_from' => $faker->randomFloat(0, 15000, 25000),
        'status'         => PromoCode::STATUS_ENABLED,
        'start_at'       => Carbon::now()->startOfDay(),
        'expired_at'     => Carbon::now()->addMonth(),
    ];
});
