<?php

use Faker\Generator as Faker;
use LuckyWeb\MS\Models\SiteGoodType;

/* @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(SiteGoodType::class, function (Faker $faker) {
    return [
        'type_id'   => $faker->numberBetween(0, 9999999),
        'type_name' => $faker->firstName,
        'width'     => $faker->numberBetween(270, 400),
//        'height' => $faker->numberBetween(120, 350),
    ];
});
