<?php

use Faker\Generator as Faker;
use LuckyWeb\User\Models\PromoCode;
use LuckyWeb\User\Models\PromoCodeEvent;
use LuckyWeb\User\Models\User;

/* @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(PromoCodeEvent::class, function (Faker $faker) {
    /** @var User $user */
    $user = factory(User::class)->create();
    /** @var PromoCode $promoCode */
    $promoCode = factory(PromoCode::class)->create();

    return [
        'promo_code' => $promoCode,
        'user'       => $user,
        'code'       => $promoCode->code,
    ];
});
