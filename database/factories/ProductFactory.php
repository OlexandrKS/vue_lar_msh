<?php

use Faker\Generator as Faker;
use LuckyWeb\MS\Models\Category;
use LuckyWeb\MS\Models\Image;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\SiteGoodType;

/* @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Product::class, function (Faker $faker) {
    $color = $faker->numberBetween(1, 10000);
    $good = $faker->numberBetween(1, 100000);

    $top = $faker->numberBetween(10000, 50000);
    $discount = 40;
    $lower = $top / 100 * 40;

    /** @var Image $image */
    $image = factory(Image::class)->make([
        'good_id'  => $good,
        'color_id' => $color,
    ]);
    $image->updateProductId();
    $image->save();

    return [
        'id'              => "$color.$good.",
        'category_id'     => factory(Category::class)->create()->id,
        'site_type_id'    => factory(SiteGoodType::class)->create()->type_id,
        'color_id'        => $color,
        'good_id'         => $good,
        'name'            => $faker->safeColorName,
        'subname'         => $faker->colorName,
        'price_top_b'     => $top,
        'price_lower_b'   => $lower,
        'discount_b'      => $discount,
        'slug'            => $faker->slug,
        'delivery_cost'   => 400,
        'assembling_cost' => 500,
    ];
});
