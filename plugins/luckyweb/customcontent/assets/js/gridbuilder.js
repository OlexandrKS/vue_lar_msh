$(document).ready(function(){
//Add product
    $('.page-content-wrap').on('click','.grid-builder .product-contrlors-item .add-element', function(e){
        e.preventDefault();
        $('body').addClass('show-popup');

        var _this = $(this);
        var elementsInRow = _this.closest('.grid-builder').find('.product-view-row-content-elem').length;
        var maxElementCounts = 5;
        var addPossibleCounts = maxElementCounts - elementsInRow;

        $.ajax({
            url:    '/backend/luckyweb/customcontent/pages/get_products',
            type:   'GET',
            error:  function(jqXHR){
                console.error(jqXHR.responseText)
                $('body').removeClass('show-popup');
            },
            success:function(response){
                console.log(response);
                $('.product-view-popup-wrap:not(.custom) .popup-content').empty().attr('data-items-add-count',addPossibleCounts).attr('data-items-max-count',maxElementCounts);
                //Create popup preview elements
                for(var i in response){
                    var popupContent = '';
                    if(!$.isEmptyObject(response[i].items)) {
                        
                        popupContent += '<ul>' +
                                            '<li class="category">' +
                                                '<span>' + response[i].title + '<i class="icon-caret-right"></i></span>'+
                                                '<ul class="items-preview">';

                        for(var item in response[i].items){
                            popupContent +=         '<li class="item" data-id="'+response[i].items[item].id+'">' +
                                                        '<img src="'+response[i].items[item].img+'" alt="">' +
                                                        '<div class="item-title">';

                            if(response[i].items[item].subcategory.length > 0){
                                popupContent +=             response[i].items[item].subcategory+' &rarr; ';
                            }

                            popupContent +=                 response[i].items[item].title+' '+response[i].items[item].subtitle+
                                                        '</div>' +
                                                        '<div class="item-price">Цена: '+response[i].items[item].price+'</div>'
                                                    '</li>';
                        }

                        popupContent += '</ul></li></ul>';
                    }


                    $('.product-view-popup-wrap:not(.custom) .popup-content').append(popupContent);
                }

                var addFilesPreggres = '<div class="showProgress">'+
                    '<span>Можно выбрать максимум <span class="showProgressVal">'+addPossibleCounts+'</span></span>'+
                '</div>';

                $('.product-view-popup-wrap:not(.custom) .popup-content').append(addFilesPreggres);

                //View popup
                $('.product-view-popup-wrap:not(.custom)').show();
                //Current component block
                var container = _this.closest('.product-view-rows-wrap');

                $('.product-view-popup-wrap:not(.custom)').off('click', 'button[name=apply]').on('click', 'button[name=apply]', function () {

                    var contentElem = '';

                    var selectedElements = $('.product-view-popup-wrap:not(.custom) .popup-content li.item.active');
                    var elemWidth = +((100/ (selectedElements.length + elementsInRow) )).toFixed(0);

                    container.find('.product-view-row-content-elem').css('width',elemWidth+'%').attr('data-width',elemWidth)
                    container.find('.product-view-row-content-elem select').val(elemWidth);

                    selectedElements.each(function(index,item){
                        if(typeof $(this).attr('data-id') != 'undefined'){

                            //build option to select
                            var gbOptionsList = '';
                            gbSelectOptionList.map(function(item){
                                var gbOptionSelected = item == elemWidth ? 'selected' : '';
                                gbOptionsList+='<option value="'+item+'" '+gbOptionSelected+'>'+item+'%</option>';
                            })

                            contentElem +=
                            '<div class="product-view-row-content-elem" data-id="'+$(this).attr('data-id')+'" style="width:'+elemWidth+'%" data-width="'+elemWidth+'">'+
                                '<div class="content">'+
                                    $(this).html()+
                                    '<div class="drop-button-wrap"><i class="icon-times"></i></div>'+
                                '</div>'+
                                '<div class="item-controls">'+
                                    '<div class="control-position">'+
                                        '<div class="control-elem left"><i class="icon-arrow-left"></i></div>' +
                                        '<div class="control-elem right"><i class="icon-arrow-right"></i></div>'+
                                    '</div>'+
                                    '<div class="control-width">'+
                                        '<span>Размер елемента</span>'+
                                        '<select name="select" data-last-value="'+elemWidth+'">'+
                                                gbOptionsList +
                                            '</select>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';
                        }
                    });
                    container.find('.product-view-row .product-view-background').append(contentElem);
                    $('.product-view-popup-wrap:not(.custom)').hide();

                    // new code
                    setCountPositionInRow();

                    $('body').removeClass('show-popup');
                });
            }
        })
    });
    // //Drop product
    // $('.page-content-wrap').on('click', '.product-view-background .drop-button-wrap', function(){
    //     var res = confirm('Вы действительно хотите удалить данный продукт?');
    //     if(res){
    //         $(this).closest('.product-view-row-content-elem').remove();
    //         setCountPositionInRow();
    //     }
    // });

    //Popup content-> open li
    $('.product-view-popup-wrap:not(.custom)').on('click','.category span', function(){
        if($(this).children('i').hasClass('icon-caret-right')){
            $(this).children('i').removeClass('icon-caret-right').addClass('icon-caret-down');
            $(this).closest('li').find('.items-preview').css({'display': 'flex'});
        }else{
            $(this).children('i').removeClass('icon-caret-down').addClass('icon-caret-right');
            $(this).closest('li').find('.items-preview').css({'display': 'none'});
        }
    });

    //Popup case element
    $('.product-view-popup-wrap:not(.custom)').on('click', 'li.item', function(e){
        var target = $(e.currentTarget);
        var content = $(e.currentTarget).closest('.popup-content');
        var maxCount = parseFloat(content.attr('data-items-add-count'));

        var progressValHolder = content.find('.showProgress .showProgressVal');
        var selectedElem = content.find('li.item.active').length;

        if(target.hasClass('active')){
            target.removeClass('active')
            //progressValHolder.text(selectedElem+1);
        }else{
            if(selectedElem<maxCount){
                target.addClass('active');
                //progressValHolder.text(selectedElem+1);
            }
        }

        progressValHolder.text();
    });

    //Popup close
    $('.product-view-popup-wrap:not(.custom)').on('click','.close-popup', function () {
        $(this).closest('.product-view-popup-wrap:not(.custom)').hide();
        $('body').removeClass('show-popup');
    });

    //Expand
    $('.page-content-wrap').on('click','.product-view-background .control-elem.expand', function(){
        var blockFullWidth = Math.floor(parseFloat($(this).closest('.product-view-background').css('width').replace('/\s|px/g','')));
        var blockPartWidth = blockFullWidth / 4;
        var width = Math.floor(parseFloat($(this).closest('.product-view-row-content-elem').css('width').replace('/\s|px/g','')));
        var mult = parseInt($(this).closest('.product-view-row-content-elem').attr('data-mult')) + 1;
        if(mult <= 4){
            $(this).closest('.product-view-row-content-elem').css({'width': width + blockPartWidth});
            $(this).closest('.product-view-row-content-elem').attr('data-mult', mult);
        }
        if(mult > 1){
            $(this).closest('.product-view-row-content-elem').find('.content .ignore-checkbox').show();
        }
    });
    //Compress
    $('.page-content-wrap').on('click','.product-view-background .control-elem.compress', function(){
        var blockFullWidth = Math.floor(parseFloat($(this).closest('.product-view-background').css('width').replace('/\s|px/g','')));
        var blockPartWidth = blockFullWidth / 4;
        var width = Math.floor(parseFloat($(this).closest('.product-view-row-content-elem').css('width').replace('/\s|px/g','')));
        var mult = parseInt($(this).closest('.product-view-row-content-elem').attr('data-mult')) - 1;
        if(mult >= 1){
            $(this).closest('.product-view-row-content-elem').css({'width': width - blockPartWidth});
            $(this).closest('.product-view-row-content-elem').attr('data-mult', mult);
        }
        if(mult == 1){
            $(this).closest('.product-view-row-content-elem').find('.content .ignore-checkbox input[type=checkbox]').prop('checked', false);
            $(this).closest('.product-view-row-content-elem').find('.content .ignore-checkbox input[type=checkbox]').removeAttr('checked');
            $(this).closest('.product-view-row-content-elem').find('.content .ignore-checkbox').hide();
        }
    });
    // //Move right
    // $('.page-content-wrap').on('click','.product-view-background .control-elem.right', function(){
    //     if($(this).closest('.product-view-row-content-elem').next('.product-view-row-content-elem').length > 0){
    //         var nextElem = $(this).closest('.product-view-row-content-elem').next('.product-view-row-content-elem').clone();
    //         $(this).closest('.product-view-row-content-elem').next('.product-view-row-content-elem').remove();
    //         $(this).closest('.product-view-row-content-elem').before(nextElem)
    //     }
    // });
    // //Move left
    // $('.page-content-wrap').on('click','.product-view-background .control-elem.left', function(){
    //     if($(this).closest('.product-view-row-content-elem').prev('.product-view-row-content-elem').length > 0){
    //         var prevElem = $(this).closest('.product-view-row-content-elem').prev('.product-view-row-content-elem').clone();
    //         $(this).closest('.product-view-row-content-elem').prev('.product-view-row-content-elem').remove();
    //         $(this).closest('.product-view-row-content-elem').after(prevElem)
    //     }
    // });
    $('.page-content-wrap').on('change','.type_selector[type=radio]', function(){
        if($(this).val()=='text'){
            $(this).closest('.page-component-box').find('.txt_btn_container').css('display','block');
            $(this).closest('.page-component-wrap').find('.page-component-box[data-type=button] .href_input').css('display','none');
        }else{
            $(this).closest('.page-component-box').find('.txt_btn_container').css('display','none');
            $(this).closest('.page-component-wrap').find('.page-component-box[data-type=button] .href_input').css('display','block');
        }
    });












    var gbSelectOptionList = [20,25,33,40,50,60,66,75,80,100];

    //  controls in right/left side row
    $(document).on('click','.grid-builder .btn.remove-row',function(e){
        //remove row
        e.preventDefault();
        $(this).closest('.grid-builder').remove();
        updateGridRowIndex();
    })

    function updateGridRowIndex() {
        $('[data-component]').each(function(i,elemComponent){
            $(elemComponent).find('.grid-builder').each(function(j,item){
                $(item).attr('data-row-index',j);
            })
        })
    }

    //click on top/bottom move row
    $(document).on('click','.grid-builder .page-position-controls i',function(e){
        var item = $(e.currentTarget).closest('.grid-builder');

        var position = $(e.currentTarget).hasClass('top') ? 'top' : 'bottom';

        var needSiblingItem = position === 'top' ? item.prev() : item.next();

        if(needSiblingItem.length){
            var cutedItem = item.detach();
            position === 'top' ? needSiblingItem.before(cutedItem) : needSiblingItem.after(cutedItem);
        }

        updateGridRowIndex();
    })


    $(document).on('change','.grid-builder .controls-type input',function(e){
        var value = e.currentTarget.value;
        var holder = $(e.currentTarget).closest('.grid-builder');
        var controlLocation = holder.find('.product-contrlors-item.controls-location');

        holder.attr('data-items-type',value);

        if(value==='dinamic'){
            holder.attr('data-items-location','left');
            controlLocation.addClass('hide')
            controlLocation.find('input[value="left"]').prop("checked", true);
            changeItemsLocationInRow(holder,'left');
            reselectItemWidthToDinamic(holder)
        }else if (value==='static'){
            controlLocation.removeClass('hide')
        }
    })

    $(document).on('change','.grid-builder .controls-location input',function(e){
        var value = e.currentTarget.value;
        var holder = $(e.currentTarget).closest('.grid-builder');
        holder.attr('data-items-location',value);
        changeItemsLocationInRow(holder,value)
    })

    $(document).on('change','.grid-builder .controls-show-mobile input',function(e){
        var value = e.currentTarget.checked;
        var holder = $(e.currentTarget).closest('.grid-builder');
        holder.attr('data-show-mobile',value);
    })

    function changeItemsLocationInRow(holder,value) {
        var wraper = holder.find('.product-view-background');
        switch(value){
            case 'center':
                wraper.removeClass('right left').addClass('center')
                break
            case 'right':
                wraper.removeClass('center left').addClass('right')
                break
            default:
                wraper.removeClass('center right').addClass('left');
        }
    }

    function reselectItemWidthToDinamic(holder) {
        var items = holder.find('.product-view-row-content-elem');
        var itemsCount = items.length;

        var newElemWidth = +((100/itemsCount)).toFixed(0);
        items.attr('data-width',newElemWidth).css('width',newElemWidth+'%');
        items.find('.control-width select').val(newElemWidth)
    }

    //  /controls in right side row


    //  controls in 1 item

    //delete 1 item
    $(document).on('click','.grid-builder .product-view-row-content-elem .drop-button-wrap:not(.edit)',function(e){
        var res = confirm('Вы действительно хотите удалить данный продукт?');
        if(res){
            var item = $(e.currentTarget).closest('.product-view-row-content-elem');
            item.remove();
            setCountPositionInRow();
        }
    })

    //change 1 item position (left/right)
    $(document).on('click','.grid-builder .product-view-row-content-elem .control-position .control-elem',function(e){
        var item = $(e.currentTarget).closest('.product-view-row-content-elem');

        var position = $(e.currentTarget).hasClass('left') ? 'left' : 'right';

        var needSiblingItem = position === 'left' ? item.prev() : item.next();

        if(needSiblingItem.length){
            var cutedItem = item.detach();
            position === 'left' ? needSiblingItem.before(cutedItem) : needSiblingItem.after(cutedItem);
        }

        setCountPositionInRow();
    })

    function setCountPositionInRow() {
        $('.grid-builder .product-view-background').each(function(index,itemsHolders){
            $(itemsHolders).children().each(function(index,item){
                $(item).attr('data-item-index',index)
            })
        })
    }

    //change select size 1 item
    $(document).on('change','.grid-builder .control-width select',function(e){

        var select = $(e.currentTarget);
        var value = select.val();
        var lastValue = $(e.currentTarget).attr('data-last-value');
        var item = $(e.currentTarget).closest('.product-view-row-content-elem');
        var desicion = validateChangeItemSize(parseFloat(value),item);

        if(desicion){
            item.css('width',value+'%').attr('data-width',value);
            select.attr('data-last-value',value);
            cleatGridBuildeerError(select.closest('.grid-builder'))
        }else {
            select.val(lastValue);
            setGridBuilderError('itemSize',select.closest('.grid-builder'))
        }

    })

    function validateChangeItemSize(value,item) {
        var mainWrapper = item.closest('.grid-builder');
        var itemsWrapper = item.closest('.product-view-background');
        var itemsSiblings = item.siblings();
        var maxValue = 100;
        var siblingWidthCounts = 0;
        itemsSiblings.each(function(index,item){
           var width = parseFloat($(item).attr('data-width'))
           siblingWidthCounts+=width;
        });
        if(siblingWidthCounts + value > maxValue){
            return false;
        }
        return true;
    }

    //  /controls in 1 item

    function setGridBuilderError(typeError,holder) {
        var validTextHolder = holder.find('.product-info-validation');
        var errrorText = '';

        switch(typeError){
            default:
                //error in change size on 1 item
                errrorText = 'Ошибка в изменении размера елемента - суммарный размер всех елементов в ряду не должен превышать 100%';
                validTextHolder.text(errrorText).addClass('error');
        }
    }

    function cleatGridBuildeerError(holder) {
        var wrapper = holder.find('.product-info-validation');

        if(wrapper.hasClass('error')){
            wrapper.text('').removeClass('error');
        }

        if(typeof holder === 'undefined'){
            //clear all error
            $('.product-info-validation').text('').removeClass('error');
        }
    }


    //add new empty row to gridBuilder Component
    $(document).on('click','.add-gridBuilder-row',function(e){
        e.preventDefault();
        var rowCounts = $(e.currentTarget).closest('.page-component-wrap').find('.grid-builder').length;
        var rowHolder = $(e.currentTarget).closest('.page-component-wrap').find('.grid-builder-list');

        var markup =
            '<div class="page-component-box grid-builder" data-type="title" data-row-index="'+rowCounts+'" data-items-type="dinamic" data-items-location="left" >'+
                '<div class="page-position-controls">' +
                    '<i class="icon-sort-asc top" ></i>' +
                    '<i class="icon-sort-desc bottom" ></i>' +
                '</div>'+
                '<div class="product-view-rows-wrap">'+
                    '<div class="product-view-row">'+
                        '<div class="product-view-background">'+
                        '</div>'+
                        '<div class="product-bottom-info">'+
                            '<div class="product-info-validation"></div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="product-controls-row-elements">'+
                        '<div class="product-contrlors-item buttons">'+
                            '<button class="btn btn-primary add-element" >Добавить Елемент</button>'+
                            '<button class="btn btn-default remove-row" >Удалить ряд</button>'+
                        '</div>'+
                        '<div class="product-contrlors-item controls-type">'+
                            '<label>'+
                                '<input type="radio" data-control="type" name="control-type-'+rowCounts+'" value="dinamic" checked>'+
                                '<span>Динамичный</span>'+
                            '</label>'+
                            '<label>'+
                                '<input type="radio" data-control="type" name="control-type-'+rowCounts+'" value="static">'+
                                '<span>Статический</span>'+
                            '</label>'+
                        '</div>'+
                        '<div class="product-contrlors-item controls-location hide">'+
                            '<label>'+
                                '<input type="radio" data-control="location" name="item-location-'+rowCounts+'" value="left" checked>'+
                                '<span>По левому</span>'+
                            '</label>'+
                            '<label>'+
                                '<input type="radio" data-control="location" name="item-location-'+rowCounts+'" value="center">'+
                                '<span>По центру</span>'+
                            '</label>'+
                            '<label>'+
                                '<input type="radio" data-control="location" name="item-location-'+rowCounts+'" value="right">'+
                                '<span>По правому</span>'+
                            '</label>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>';

        rowHolder.append(markup);
    })




    //handle event fire when func buildComponentView() done
    $(document).on("ComponentViewCreate", function(e){
        //setCountPositionInRow();

    });

});


