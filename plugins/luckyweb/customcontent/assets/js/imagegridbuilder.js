$(function(){

    $(document).ready(function(){
        сustomPopupFunc();
        editImageGridBuilderItem();
    })

    var max_images = 10;

    function сustomPopupFunc() {
        $(document).on('click','[data-popup]',function(e){
            e.preventDefault();
            var id = $(this).attr('data-popup');
            var hiddenBlock = $('.hidden_block');

            if(hiddenBlock.length){
                var popupHolder = hiddenBlock.find(id);

                try{
                    dynamicCKEreplace();
                }catch(e){}


                var rowIndex = $(this).closest('.grid-builder').attr('data-row-index');
                var globalComponentName = $(this).closest('[data-component]').attr('data-component');
                var globalComponentIndex = $(this).closest('[data-component="'+globalComponentName+'"]').index();
                if(typeof globalComponentIndex !== 'undefined'){
                    popupHolder.attr({
                        "data-row-index": rowIndex,
                        "data-component-index": globalComponentIndex
                    });
                }

                switch(id){
                    case '#img-grid-builder-popup':
                            
                            // opened popup image grid builder component
                            var rowHolder = $(this).closest('.grid-builder');

                            var imageGridItems = rowHolder.find('.product-view-row-content-elem');

                            //detect if user add more then
                            if(imageGridItems.length > max_images - 1){
                                alert('Ошибка! В ряду может быть максимум ' + max_images + ' элементов');
                                resetPopup(popupHolder);
                                return false;
                            }

                            //detact if all element width > 100%
                            var widthAllItems = 0;

                            imageGridItems.each(function(i,item){
                                widthAllItems += parseFloat($(item).attr('data-width'))
                            })

                            if(widthAllItems > 100 - (100 / max_images)){
                                //min width new item
                                alert('Ошибка! Минимальная ширина елемента - ' + (100 / max_images) + '%');
                                resetPopup(popupHolder);
                                return false;
                            }

                        break;
                    default:
                }

                hiddenBlock.addClass('show-popup');
                $('body').addClass('show-popup');

                popupHolder.show();

            }
        })

        $(document).on('click','.product-view-popup-wrap.custom .close-popup',function(e){
            e.preventDefault();
            $('.hidden_block').removeClass('.show-popup')
            var popup = $(this).closest('.product-view-popup-wrap.custom');

            resetPopup(popup)

            popup.removeClass('show-popup').hide();
            $('body').removeClass('show-popup');
        })
    }

    function resetPopup(popup) {
        try{
            //clear popup data
            var editor = popup.find('.textform.needCKE');
            var editorName = editor.attr('name');
            if(!$.isEmptyObject(CKEDITOR.instances[editorName])){
                CKEDITOR.instances[editorName].setData('')
            }

            popup.find('input:not([data-without-reset])').val('');
            popup.find('.image-preview').empty()

            popup.attr({"data-row-index": '', "data-component-index": '','data-item-index':''});

        }catch(e){}
    }

    //img-grid-builder-popup
    $(document).on('click','.hidden_block .img-grid-builder-popup .done-button button',function(e){
        var popup = $(this).closest('.img-grid-builder-popup.custom');

        var imgSrc = popup.find('.image-preview img').attr('src') || '';
        var link = popup.find('.img-grid-builder-link').val() || '';
        var editor = popup.find('.textform.needCKE');
        var editorName = editor.attr('name');
        var text = CKEDITOR.instances[editorName].getData() || '';
        var textVerticalAlign = popup.find('[data-type="textAlign"] input[type="radio"]:checked').val();
        var textColor = popup.find('.img-grid-builder-text-color').val();

        var ImgGridBuilderObj = {
            image:imgSrc,
            link:link,
            text:text,
            textColor:textColor,
            textVerticalAlign:textVerticalAlign
        }

        var option = {
            rowIndex:popup.attr('data-row-index'),
            componentIndex:popup.attr('data-component-index'),
            itemIndex:popup.attr('data-item-index') || ''
        }

        setGridBuilderItem(ImgGridBuilderObj,option);

        resetPopup(popup);

        popup.removeClass('show-popup').hide();
        $('body').removeClass('show-popup');
    })

    //add new empty row to gridBuilder Component
    $(document).on('click','.add-image-GridBuilder-row',function(e){
        e.preventDefault();
        var rowCounts = $(e.currentTarget).closest('.page-component-wrap').find('.grid-builder').length;

        var builderCountGlobal = $('.imagegrid-builder-list .grid-builder').length;

        var rowHolder = $(e.currentTarget).closest('.page-component-wrap').find('.imagegrid-builder-list');

        var markup =
            '<div class="page-component-box grid-builder" data-type="title" data-row-index="'+rowCounts+'" data-items-type="dinamic" data-items-location="left" data-show-mobile="true">'+
                '<div class="page-position-controls">' +
                    '<i class="icon-sort-asc top" ></i>' +
                    '<i class="icon-sort-desc bottom" ></i>' +
                '</div>'+
                '<div class="product-view-rows-wrap">'+
                    '<div class="product-view-row">'+
                        '<div class="product-view-background">'+
                        '</div>'+
                        '<div class="product-bottom-info">'+
                            '<div class="product-info-validation"></div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="product-controls-row-elements">'+
                        '<div class="product-contrlors-item buttons">'+
                            '<button class="btn btn-primary add-img-element" data-popup="#img-grid-builder-popup" >Добавить Елемент</button>'+
                            '<button class="btn btn-default remove-row" >Удалить ряд</button>'+
                        '</div>'+
                        '<div class="product-contrlors-item controls-type">'+
                            '<label>'+
                                '<input type="radio" data-control="type" name="imagegrid-control-type-'+builderCountGlobal+'" value="dinamic" checked>'+
                                '<span>Динамичный</span>'+
                            '</label>'+
                            '<label>'+
                                '<input type="radio" data-control="type" name="imagegrid-control-type-'+builderCountGlobal+'" value="static">'+
                                '<span>Статический</span>'+
                            '</label>'+
                        '</div>'+
                        '<div class="product-contrlors-item controls-location hide">'+
                            '<label>'+
                                '<input type="radio" data-control="location" name="imagegrid-item-location-'+builderCountGlobal+'" value="left" checked>'+
                                '<span>По левому</span>'+
                            '</label>'+
                            '<label>'+
                                '<input type="radio" data-control="location" name="imagegrid-item-location-'+builderCountGlobal+'" value="center">'+
                                '<span>По центру</span>'+
                            '</label>'+
                            '<label>'+
                                '<input type="radio" data-control="location" name="imagegrid-item-location-'+builderCountGlobal+'" value="right">'+
                                '<span>По правому</span>'+
                            '</label>'+
                        '</div>'+
                        '<div class="product-contrlors-item controls-show-mobile">'+
                            '<label>'+
                                '<input type="checkbox" data-control="mobile"  name="imagegrid-item-show-mobile-'+builderCountGlobal+'" checked="true" >'+
                                '<span>Отображать на мобильном ?</span>'+
                            '</label>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>';

        rowHolder.append(markup);
    })


    function setGridBuilderItem(obj,option) {

        var gbSelectOptionList = [10,20,25,33,40,50,60,66,75,80,100];
        var elemWidth = 10;
        //build option to select
        var gbOptionsList = '';
        gbSelectOptionList.map(function(item){
            var gbOptionSelected = item == elemWidth ? 'selected' : '';
            gbOptionsList+='<option value="'+item+'" '+gbOptionSelected+'>'+item+'%</option>';
        })

        var componentIndex = parseFloat(option.componentIndex);
        var rowIndex = parseFloat(option.rowIndex);
        var itemIndex = option.itemIndex;

        var itemRow = $($('.page-components-list-wrap[data-component]')[componentIndex]).find('[data-row-index="'+rowIndex+'"]');

        if(!itemRow.length){
            alert('Ошибка при добавлении елемента! Перезагрузите страницу и попробуйте снова.')
            return false;
        }

        var parentHolder = itemRow.find('.product-view-background');


        var itemText = $(obj.text).text().substring(0,20)+'...';

        if(itemIndex){
            //if reduction existings item
            var reductionItem = parentHolder.find('.product-view-row-content-elem').eq(itemIndex);

            reductionItem.attr({
                "data-item-image": obj.image,
                'data-item-link':obj.link,
                "data-item-text-color": obj.textColor,
                "data-item-text-vertical-align": obj.textVerticalAlign
            });

            reductionItem.find('.data-item-hidden-text').empty().html(obj.text);
            reductionItem.find('.content>img').attr('src',obj.image.length ? obj.image : '')
            reductionItem.find('.item-title').text(itemText);

        }else{
            var elemsCount = parentHolder.find('.product-view-row-content-elem').length;

            var gridBuilderMarkup =
            '<div class="product-view-row-content-elem image-preview" style="width:' + 100/max_images + '%" data-width="' + 100/max_images + '" data-item-index="'+elemsCount+'" data-item-image="'+obj.image+'" data-item-link="'+obj.link+'" data-item-text-color="'+obj.textColor+'" data-item-text-vertical-align="'+obj.textVerticalAlign+'">'+
                '<div class="content">'+
                    '<img src="'+(obj.image || '')+'" alt="" data-type="upload">'+
                    '<div class="item-title">'+itemText+'</div>'+
                    '<div class="drop-button-wrap"><i class="icon-times"></i></div>'+
                    '<div class="drop-button-wrap edit"><i class="icon-edit"></i></div>'+
                '</div>'+
                '<div class="item-controls">'+
                    '<div class="control-position">'+
                        '<div class="control-elem left"><i class="icon-arrow-left"></i></div>' +
                        '<div class="control-elem right"><i class="icon-arrow-right"></i></div>'+
                    '</div>'+
                    '<div class="control-width">'+
                        '<span>Размер елемента</span>'+
                        '<select name="select" data-last-value="10">'+
                            gbOptionsList+
                        '</select>'+
                    '</div>'+
                '</div>'+
                '<div class="data-item-hidden-text">'+obj.text+'</div>'+
            '</div>';

            parentHolder.append(gridBuilderMarkup);
        }

    }


    function editImageGridBuilderItem() {

        $(document).on('click','[data-component="ImageGridBuilder"] .drop-button-wrap.edit',function(e){
            var item = $(this).closest('.product-view-row-content-elem');
            var ImgGridBuilderObj = {
                image:item.attr('data-item-image'),
                link:item.attr('data-item-link'),
                text:item.find('.data-item-hidden-text').html(),
                textColor: item.attr('data-item-text-color'),
                textVerticalAlign: item.attr('data-item-text-vertical-align') || 'center'
            }
            var itemIndexPosition = {
                componentIndex:item.closest('[data-component="ImageGridBuilder"]').index(),
                rowIndex:item.closest('.grid-builder').attr('data-row-index'),
                itemIndex:item.index()
            }

            var popup = $('#img-grid-builder-popup');
            resetPopup(popup)

            popup.attr({
                "data-row-index": itemIndexPosition.rowIndex,
                "data-component-index": itemIndexPosition.componentIndex,
                "data-item-index":itemIndexPosition.itemIndex
            });

            popup.find('.image-preview').append('<img src="'+ImgGridBuilderObj.image+'" alt="" />')

            popup.find('.img-grid-builder-link').val(ImgGridBuilderObj.link);

            popup.find('.img-grid-builder-text-color').val(ImgGridBuilderObj.textColor);

            popup.find('[data-type="textAlign"] input').prop( "checked", false ).end().find('[data-type="textAlign"] input[value="'+ImgGridBuilderObj.textVerticalAlign+'"]').prop( "checked", true );

            var editor = popup.find('.textform.needCKE');
            var editorName = editor.attr('name');

            setTimeout(function(){
                CKEDITOR.instances[editorName].setData(ImgGridBuilderObj.text);
            }, 20);

            $('body').addClass('show-popup');
            popup.addClass('show-popup').show();

        })

    }




})