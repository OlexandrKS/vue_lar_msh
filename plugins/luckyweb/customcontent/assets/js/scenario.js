//Color picker initialize
function colorPickerInitialize(){
    $('.page-component-box .color-picker').spectrum({
        showPaletteOnly: true,
        togglePaletteOnly: true,
        togglePaletteMoreText: 'more',
        togglePaletteLessText: 'less',
        palette: [
            ["#000","#333","#666","#999","#ccc","#eee","#f3f3f3","#fff"],
            ["#ed4f20","#f90","#ff0","#63c33d","#0ff","#00f","#90f","#f0f"],
            ["#f4cccc","#fce5cd","#fff2cc","#d9ead3","#d0e0e3","#cfe2f3","#d9d2e9","#ead1dc"],
            ["#ea9999","#e4d2be","#ffe599","#b6d7a8","#a2c4c9","#9fc5e8","#b4a7d6","#d5a6bd"],
            ["#e06666","#f6b26b","#ffd966","#93c47d","#76a5af","#6fa8dc","#8e7cc3","#c27ba0"],
            ["#c00","#e69138","#f1c232","#6aa84f","#45818e","#3d85c6","#674ea7","#a64d79"],
            ["#900","#b45f06","#bf9000","#38761d","#134f5c","#0b5394","#351c75","#741b47"],
            ["#600","#783f04","#7f6000","#274e13","#0c343d","#073763","#20124d","#4c1130"]
        ]
    });
}

//Search images that was updated to page
/**
 * @returns {Array} of objects {
 *      src - image base64_encoded resource
 *      loc - location of image block
 * }
 */
function searchUploadedImages(){
    var pageObj = $('form').find('.page-content-wrap');
    return pageObj.find('img[data-type=upload]');
}

/**
 * Function save images one-by-one, than run callback
 * @param imagesArr => [src, loc]
 * @param i - image iterrator
 * @param callback
 */
function imagesUpload(imagesArr, i, callback){
    console.log(imagesArr);
    if(imagesArr.length > 0){
        $.ajax({
            url:	'/backend/luckyweb/customcontent/pages/save_image',
            type:	'POST',
            headers:{'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
            data:	{
                image: $(imagesArr[i]).attr('src')
            },
            error:	function(jqXHR){
                console.error(jqXHR.responseText)
            },
            success: function(response){
                if(response.message == 'success'){
                    // $(imagesArr[i]).attr('src','/storage/images/'+response.file);
                    $(imagesArr[i]).attr('src', response.url);
                }else if(response.message == 'error'){
                    validationError(response.text);
                }else{
                    console.error(response)
                }
            }
        }).done(function(){
            i++;
            if(i == imagesArr.length){
                callback();
            }else{
                imagesUpload(imagesArr, i, callback);
            }
        });
    }else{
        callback();
    }
}

//Call validation error pop-up
function validationError(text){
    $('body').append('<p class="flash-message fade error in" style="display:none">'+text+'<button type="button" class="close" aria-hidden="true">×</button></p>');
    $('body>p.flash-message').fadeIn(500).delay(4000).fadeOut(500);
    setTimeout(function(){
        $('body>p.flash-message').remove();
    }, 5500);
}

//Call success Pop-up
function validationSuccess(text, leave){
    $('body').append('<p class="flash-message fade success in" style="display:none">'+text+'<button type="button" class="close" aria-hidden="true">×</button></p>');
    $('body>p.flash-message').fadeIn(500).delay(4000).fadeOut(500);
    setTimeout(function(){
        $('body>p.flash-message').remove();
    }, 5500);
}

/**
 * Function collect page main data
 * @returns {
 *      {
 *      title: string,
 *      slug: string,
 *      description: string,
 *      meta_title: string,
 *      meta_description: string,
 *      custom_content: Array
 *      }
 *  }
 */
function gatherPageData(){
    return {
        title:          $('form input[name="Page[title]"]').val().trim(),
        slug:           $('form input[name="Page[slug]"]').val().trim(),
        description:    $('form textarea[name="Page[description]"]').val(),
        meta_title:     $('form input[name="Page[meta_title]"]').val(),
        meta_description:   $('form textarea[name="Page[meta_description]"]').val(),
        emails:     $('form textarea[name="Page[emails]"]').val(),
        breadcrumbs: $('form input[name="Page[breadcrumbs]"]').val(),
        custom_content: []
    };
}

/**
 * Function collect custom content and send save-data ajax request
 * @param data
 */
function savePageData(data){
    $('ul.page-content-wrap li.page-components-list-wrap').each(function(){
        var temp = {
            id:     (typeof $(this).attr('data-id') != 'undefined')
                        ? $(this).attr('data-id')
                        : 0,
            component:  $(this).attr('data-component'),
            //Default values
            content: {
                marginBottom: ($(this).find('.page-component-box[data-type=marginBottom] input[name=marginBottom]').length > 0)
                    ? $(this).find('.page-component-box[data-type=marginBottom] input[name=marginBottom]').val()
                    : 20,
                marginTop:($(this).find('.page-component-box[data-type=marginTop] input[name=marginTop]').length > 0)
                    ? $(this).find('.page-component-box[data-type=marginTop] input[name=marginTop]').val()
                    : 0,
                anchor: ($(this).find('.page-component-box[data-type=anchor] input[name=anchor]').length > 0)
                    ? $(this).find('.page-component-box[data-type=anchor] input[name=anchor]').val()
                    : 'default',
            }
        };
        switch($(this).attr('data-component')){
            case 'ImageGridBuilder':
                var items = [];
                $(this).find('.page-component-box.grid-builder').each(function(){
                    var tmp=[];
                    var config={
                        type:$(this).attr('data-items-type'),
                        location:$(this).attr('data-items-location'),
                        productsWidth:[]
                    }
                    var products=[];
                    $(this).find('.product-view-row-content-elem').each(function(){
                        config['productsWidth'].push($(this).attr('data-width'));
                        products.push({
                            img:$(this).find('img').attr('src'),
                            text:$(this).find('.data-item-hidden-text').html(),
                            color:$(this).attr('data-item-text-color'),
                            href:$(this).attr('data-item-link'),
                            textVerticalAlign:$(this).attr('data-item-text-vertical-align')
                        });
                    });
                    tmp={
                        in_mobile:$(this).attr('data-show-mobile'),
                        config:config,
                        products:products
                    };
                    items.push(tmp);
                });
                temp.content.items = items;
                console.log(temp.content.items);
                break;
            break;
        
            /**
             * All colors saves as rgb value (exmpl 51,51,51)
             */
            case 'SmallBanner':
                //Component title
                temp.content.title = {
                    tag:    ($(this).find('.page-component-box[data-type=title] input[name=captionTag]').val().trim().length > 0)
                        ? $(this).find('.page-component-box[data-type=title] input[name=captionTag]').val().trim()
                        : 'p',
                        val:    $(this).find('.page-component-box[data-type=title] input[name=title]').val(),
                        color:  $(this).find('.page-component-box[data-type=title] .sp-preview-inner').css('background-color').replace(/rgb|\)|\(|\s/g,''),
                        size: $(this).find('.page-component-box[data-type=title] input[name=titleSize]').val(),
                        Align: ($(this).find('input[name^="titleAlign_"]:checked').length > 0)
                                            ? $(this).find('input[name^="titleAlign_"]:checked').val()
                                            : 'left',
                        LeftPadding: $(this).find('.page-component-box[data-type=title] input[name=titleLeftPadding]').val(),
                        RightPadding: $(this).find('.page-component-box[data-type=title] input[name=titleRightPadding]').val()
                };
                //Component text
                var textareaName = $(this).find('.page-component-box[data-type=text] textarea[name^=text_]').attr('name');
                temp.content.text = {
                    val:    CKEDITOR.instances[textareaName].getData(),
                    color:  $(this).find('.page-component-box[data-type=text] .sp-preview-inner').css('background-color').replace(/rgb|\)|\(|\s/g,''),
                    Align: ($(this).find('input[name^="contentAlign_"]:checked').length > 0)
                                            ? $(this).find('input[name^="contentAlign_"]:checked').val()
                                            : 'left',
                        LeftPadding: $(this).find('.page-component-box[data-type=text] input[name=contentLeftPadding]').val(),
                        RightPadding: $(this).find('.page-component-box[data-type=text] input[name=contentRightPadding]').val()
                };
                textareaName = $(this).find('.page-component-box[data-type=typeSelector] textarea[name^=textTypeButton_]').attr('name');
                //Component button
                temp.content.button = {
                    textTypeButton:{
                        val:    CKEDITOR.instances[textareaName].getData()
                    },
                    type_selector_text:($(this).find('.page-component-box[data-type=typeSelector] input[name^="type_selector_"]:checked').length > 0)
                                            ? $(this).find('.page-component-box[data-type=typeSelector] input[name^="type_selector_"]:checked').val()
                                            : 'href',
                    buttonColor:$(this).find('.page-component-box[data-type=button] input[name=buttonColor]')
                        .next('.sp-replacer')
                        .find('.sp-preview-inner')
                        .css('background-color').replace(/rgb|\)|\(|\s/g,''),
                    hoverColor: $(this).find('.page-component-box[data-type=button] input[name=hoverColor]')
                        .next('.sp-replacer')
                        .find('.sp-preview-inner')
                        .css('background-color').replace(/rgb|\)|\(|\s/g,''),
                    val:        $(this).find('.page-component-box[data-type=button] input[name=buttonCaption]').val(),
                    color:      $(this).find('.page-component-box[data-type=button] input[name=titleColor]')
                        .next('.sp-replacer')
                        .find('.sp-preview-inner')
                        .css('background-color').replace(/rgb|\)|\(|\s/g,''),
                    link:       $(this).find('.page-component-box[data-type=button] input[name=buttonLink]').val()
                };

                temp.content.img = {
                        width : ($(this).find('.page-component-box[data-type=videoOrImage] input[name=imgWidth]').length > 0)
                                ? $(this).find('.page-component-box[data-type=videoOrImage] input[name=imgWidth]').val()
                                : null,
                        height : ($(this).find('.page-component-box[data-type=videoOrImage] input[name=imgHeight]').length > 0)
                                ? $(this).find('.page-component-box[data-type=videoOrImage] input[name=imgHeight]').val()
                                : null
                    };
                //Component additional content
                temp.content.videoOrImage = {
                    type: $(this).find('.page-component-box[data-type=videoOrImage] input[name^=videoOrImage]:checked').val()
                };
                if(temp.content.videoOrImage.type == 'image'){
                    temp.content.videoOrImage.val = ($(this).find('.page-component-box[data-type=videoOrImage] .image-preview img').length > 0)
                                                        ? $(this).find('.page-component-box[data-type=videoOrImage] .image-preview img').attr('src')
                                                        : '';
                }else if(temp.content.videoOrImage.type == 'video'){
                    temp.content.videoOrImage.val = $(this).find('.page-component-box[data-type=videoOrImage] textarea[name^=videoFrame]').val()
                }else{
                    temp.content.videoOrImage.val = '';
                }

                //Component composition
                temp.content.textAlign = ($(this).find('.page-component-box[data-type*="textAlign"] input[name^="textAlign"]:checked').length > 0)
                                            ? $(this).find('.page-component-box[data-type*="textAlign"] input[name^="textAlign"]:checked').val()
                                            : 'left';

                temp.content.show_text = ($(this).find('.page-component-box[data-type=title] input[name^="show"]:checked').length > 0)
                                            ? $(this).find('.page-component-box[data-type=title] input[name^="show"]:checked').val()
                                            : '0';
                temp.content.show_btn = ($(this).find('.page-component-box[data-type=button] input[name^="show"]:checked').length > 0)
                                            ? $(this).find('.page-component-box[data-type=button] input[name^="show"]:checked').val()
                                            : '0';
                temp.content.show_img = ($(this).find('.page-component-box[data-type=videoOrImage] input[name^="show"]:checked').length > 0)

                                            ? $(this).find('.page-component-box[data-type=videoOrImage] input[name^="show"]:checked').val()
                                            : '0';
                break;

            case 'BannerWithBackgroundImage':
                var desktop=$(this).find('.page-component-tabs-item[data-device=desktop]');
                var mobile=$(this).find('.page-component-tabs-item[data-device=mobile]');
                temp.content={
                    desktop:{},
                    mobile:{}
                };
                //Title
                    //Desktop
                    temp.content.desktop.title = {
                        tag:    ($(desktop).find('.page-component-box[data-type=title] input[name=captionTag]').val().trim().length > 0)
                            ? $(desktop).find('.page-component-box[data-type=title] input[name=captionTag]').val().trim()
                            : 'p',
                        val:    $(desktop).find('.page-component-box[data-type=title] input[name=title]').val(),
                        color:  $(desktop).find('.page-component-box[data-type=title] .sp-preview-inner').css('background-color').replace(/rgb|\)|\(|\s/g,''),
                        size: $(desktop).find('.page-component-box[data-type=title] input[name=titleSize]').val(),
                        Align: ($(desktop).find('input[name^="titleAlign_"]:checked').length > 0)
                                            ? $(desktop).find('input[name^="titleAlign_"]:checked').val()
                                            : 'left',
                        LeftPadding: $(desktop).find('.page-component-box[data-type=title] input[name=titleLeftPadding]').val(),
                        RightPadding: $(desktop).find('.page-component-box[data-type=title] input[name=titleRightPadding]').val()
                        
                    };
                    //END Desctop
                    //Mobile
                    temp.content.mobile.title = {
                        tag:    ($(mobile).find('.page-component-box[data-type=title] input[name=captionTag]').val().trim().length > 0)
                            ? $(mobile).find('.page-component-box[data-type=title] input[name=captionTag]').val().trim()
                            : 'p',
                        val:    $(mobile).find('.page-component-box[data-type=title] input[name=title]').val(),
                        color:  $(mobile).find('.page-component-box[data-type=title] .sp-preview-inner').css('background-color').replace(/rgb|\)|\(|\s/g,''),
                        size: $(mobile).find('.page-component-box[data-type=title] input[name=titleSize]').val(),
                        Align: ($(mobile).find('input[name^="titleAlignMobile_"]:checked').length > 0)
                                            ? $(mobile).find('input[name^="titleAlignMobile_"]:checked').val()
                                            : 'left',
                        LeftPadding: $(mobile).find('.page-component-box[data-type=title] input[name=titleLeftPadding]').val(),
                        RightPadding: $(mobile).find('.page-component-box[data-type=title] input[name=titleRightPadding]').val()
                    };
                    //END Mobile
                //END Title
                
                //Text
                    //Desktop
                    var textareaName = $(desktop).find('.page-component-box[data-type=text] textarea[name^=ckeditor_]').attr('name');
                    temp.content.desktop.text = {
                        val:    CKEDITOR.instances[textareaName].getData(),
                        color:  $(desktop).find('.page-component-box[data-type=text] .sp-preview-inner').css('background-color').replace(/rgb|\)|\(|\s/g,''),
                        Align: ($(desktop).find('input[name^="contentAlign_"]:checked').length > 0)
                                            ? $(desktop).find('input[name^="contentAlign_"]:checked').val()
                                            : 'left',
                        LeftPadding: $(desktop).find('.page-component-box[data-type=text] input[name=contentLeftPadding]').val(),
                        RightPadding: $(desktop).find('.page-component-box[data-type=text] input[name=contentRightPadding]').val()
                    };
                    //END Desctop
                    //Mobile
                    var textareaName = $(mobile).find('.page-component-box[data-type=text] textarea[name^=ckeditorMobile_]').attr('name');
                    temp.content.mobile.text = {
                        val:    CKEDITOR.instances[textareaName].getData(),
                        color:  $(mobile).find('.page-component-box[data-type=text] .sp-preview-inner').css('background-color').replace(/rgb|\)|\(|\s/g,''),
                        Align: ($(mobile).find('input[name^="contentAlignMobile_"]:checked').length > 0)
                                            ? $(mobile).find('input[name^="contentAlignMobile_"]:checked').val()
                                            : 'left',
                        LeftPadding: $(mobile).find('.page-component-box[data-type=text] input[name=contentLeftPadding]').val(),
                        RightPadding: $(mobile).find('.page-component-box[data-type=text] input[name=contentRightPadding]').val()
                    };
                    //END Mobile
                    
                //END Text
                
                //Button
                    //Desktop
                    textareaName = $(desktop).find('.page-component-box[data-type=typeSelector] textarea[name^=btn_text_]').attr('name');
                    temp.content.desktop.button = {
                        textTypeButton:{
                            val:    CKEDITOR.instances[textareaName].getData()
                        },
                        type_selector_text:($(desktop).find('.page-component-box[data-type*=typeSelector] input[name^="type_selector_"]:checked').length > 0)
                                                ? $(desktop).find('.page-component-box[data-type*=typeSelector] input[name^="type_selector_"]:checked').val()
                                                : 'href',
                        buttonColor:$(desktop).find('.page-component-box[data-type=button] input[name=buttonColor]')
                            .next('.sp-replacer')
                            .find('.sp-preview-inner')
                            .css('background-color').replace(/rgb|\)|\(|\s/g,''),
                        hoverColor: $(desktop).find('.page-component-box[data-type=button] input[name=hoverColor]')
                            .next('.sp-replacer')
                            .find('.sp-preview-inner')
                            .css('background-color').replace(/rgb|\)|\(|\s/g,''),
                        val:        $(desktop).find('.page-component-box[data-type=button] input[name=buttonCaption]').val(),
                        color:      $(desktop).find('.page-component-box[data-type=button] input[name=titleColor]')
                            .next('.sp-replacer')
                            .find('.sp-preview-inner')
                            .css('background-color').replace(/rgb|\)|\(|\s/g,''),
                        link:       $(desktop).find('.page-component-box[data-type=button] input[name=buttonLink]').val(),
                        buttonLeftPadding: $(desktop).find('.page-component-box[data-type=button] input[name=buttonLeftPadding]').val(),
                        buttonRightPadding: $(desktop).find('.page-component-box[data-type=button] input[name=buttonRightPadding]').val()
                    };
                    //END Desktop
                    //Mobile
                    textareaName = $(mobile).find('.page-component-box[data-type=typeSelector] textarea[name^=btn_textMobile_]').attr('name');
                    temp.content.mobile.button = {
                        textTypeButton:{
                            val:    CKEDITOR.instances[textareaName].getData()
                        },
                        type_selector_text:($(mobile).find('.page-component-box[data-type*=typeSelector] input[name^="type_selectorMobile_"]:checked').length > 0)
                                                ? $(mobile).find('.page-component-box[data-type*=typeSelector] input[name^="type_selectorMobile_"]:checked').val()
                                                : 'href',
                        buttonColor:$(mobile).find('.page-component-box[data-type=button] input[name=buttonColor]')
                            .next('.sp-replacer')
                            .find('.sp-preview-inner')
                            .css('background-color').replace(/rgb|\)|\(|\s/g,''),
                        hoverColor: $(mobile).find('.page-component-box[data-type=button] input[name=hoverColor]')
                            .next('.sp-replacer')
                            .find('.sp-preview-inner')
                            .css('background-color').replace(/rgb|\)|\(|\s/g,''),
                        val:        $(mobile).find('.page-component-box[data-type=button] input[name=buttonCaption]').val(),
                        color:      $(mobile).find('.page-component-box[data-type=button] input[name=titleColor]')
                            .next('.sp-replacer')
                            .find('.sp-preview-inner')
                            .css('background-color').replace(/rgb|\)|\(|\s/g,''),
                        link:       $(mobile).find('.page-component-box[data-type=button] input[name=buttonLink]').val(),
                        buttonLeftPadding: $(mobile).find('.page-component-box[data-type=button] input[name=buttonLeftPadding]').val(),
                        buttonRightPadding: $(mobile).find('.page-component-box[data-type=button] input[name=buttonRightPadding]').val()
                    };
                    //END Mobile
                //END Button
                
                //background
                    //Desktop
                    temp.content.desktop.image = ($(desktop).find('.page-component-box[data-type=background] .image-preview img').length > 0)
                                        ? $(desktop).find('.page-component-box[data-type=background] .image-preview img').attr('src')
                                        : null;
                    temp.content.desktop.img = {
                        width : ($(desktop).find('.page-component-box[data-type=background] input[name=imgWidth]').length > 0)
                                ? $(desktop).find('.page-component-box[data-type=background] input[name=imgWidth]').val()
                                : null,
                        height : ($(desktop).find('.page-component-box[data-type=background] input[name=imgHeight]').length > 0)
                                ? $(desktop).find('.page-component-box[data-type=background] input[name=imgHeight]').val()
                                : null
                    };
                    //END Desktop
                    //Mobile
                    temp.content.mobile.image = ($(mobile).find('.page-component-box[data-type=background] .image-preview img').length > 0)
                                        ? $(mobile).find('.page-component-box[data-type=background] .image-preview img').attr('src')
                                        : null;
                    temp.content.mobile.img = {
                        width : ($(mobile).find('.page-component-box[data-type=background] input[name=imgWidth]').length > 0)
                                ? $(mobile).find('.page-component-box[data-type=background] input[name=imgWidth]').val()
                                : null,
                        height : ($(mobile).find('.page-component-box[data-type=background] input[name=imgHeight]').length > 0)
                                ? $(mobile).find('.page-component-box[data-type=background] input[name=imgHeight]').val()
                                : null
                    };
                    //END Mobile
                //END Background
                
                //textAlign
                    //Desktop
                    temp.content.desktop.textAlign = ($(desktop).find('.page-component-box[data-type*="textAlign"] input[name^="textAlign_"]:checked').length > 0)
                                            ? $(desktop).find('.page-component-box[data-type*="textAlign"] input[name^="textAlign_"]:checked').val()
                                            : 'left';
                    //END Desktop
                    //Mobile
                    temp.content.mobile.textAlign = ($(mobile).find('.page-component-box[data-type*="textAlign"] input[name^="textAlignMobile_"]:checked').length > 0)
                                            ? $(mobile).find('.page-component-box[data-type*="textAlign"] input[name^="textAlignMobile_"]:checked').val()
                                            : 'left';
                    //END Mobile
                //END textAlign
                
                //Basic
                    //Desktop
                    temp.content.desktop.marginBottom=($(desktop).find('.page-component-box[data-type=marginBottom] input[name=marginBottom]').length > 0)
                        ? $(desktop).find('.page-component-box[data-type=marginBottom] input[name=marginBottom]').val()
                        : 20;
                    temp.content.desktop.marginTop=($(desktop).find('.page-component-box[data-type=marginTop] input[name=marginTop]').length > 0)
                        ? $(desktop).find('.page-component-box[data-type=marginTop] input[name=marginTop]').val()
                        : 0;
                     temp.content.desktop.anchor=($(desktop).find('.page-component-box[data-type=anchor] input[name=anchor]').length > 0)
                        ? $(desktop).find('.page-component-box[data-type=anchor] input[name=anchor]').val()
                        : 'default';
                    //END Desktop
                    //Mobile
                    temp.content.mobile.marginBottom=($(mobile).find('.page-component-box[data-type=marginBottom] input[name=marginBottom]').length > 0)
                        ? $(mobile).find('.page-component-box[data-type=marginBottom] input[name=marginBottom]').val()
                        : 20;
                    temp.content.mobile.marginTop=($(mobile).find('.page-component-box[data-type=marginTop] input[name=marginTop]').length > 0)
                        ? $(mobile).find('.page-component-box[data-type=marginTop] input[name=marginTop]').val()
                        : 0;
                     temp.content.mobile.anchor=($(mobile).find('.page-component-box[data-type=anchor] input[name=anchor]').length > 0)
                        ? $(mobile).find('.page-component-box[data-type=anchor] input[name=anchor]').val()
                        : 'default';
                    //END Mobile
                //END Basic
                break;

            case 'FastRequestForm':
                //Component title
                temp.content.title = {
                    tag:    ($(this).find('.page-component-box[data-type=title] input[name=captionTag]').val().trim().length > 0)
                        ? $(this).find('.page-component-box[data-type=title] input[name=captionTag]').val().trim()
                        : 'p',
                    val:    $(this).find('.page-component-box[data-type=title] input[name=title]').val(),
                    color:  $(this).find('.page-component-box[data-type=title] .sp-preview-inner').css('background-color').replace(/rgb|\)|\(|\s/g,''),
                    size: $(this).find('.page-component-box[data-type=title] input[name=titleSize]').val()
                };
                //Component button
                temp.content.button = {
                    buttonColor:$(this).find('.page-component-box[data-type=button] input[name=buttonColor]')
                        .next('.sp-replacer')
                        .find('.sp-preview-inner')
                        .css('background-color').replace(/rgb|\)|\(|\s/g,''),
                    hoverColor: $(this).find('.page-component-box[data-type=button] input[name=hoverColor]')
                        .next('.sp-replacer')
                        .find('.sp-preview-inner')
                        .css('background-color').replace(/rgb|\)|\(|\s/g,''),
                    val:        $(this).find('.page-component-box[data-type=button] input[name=buttonCaption]').val(),
                    color:      $(this).find('.page-component-box[data-type=button] input[name=titleColor]')
                        .next('.sp-replacer')
                        .find('.sp-preview-inner')
                        .css('background-color').replace(/rgb|\)|\(|\s/g,''),
                    link:       $(this).find('.page-component-box[data-type=button] input[name=buttonLink]').val()
                };
                //Component text
                var textareaName = $(this).find('.page-component-box[data-type=text] textarea[name^=text_]').attr('name');
                temp.content.text = {
                    val:    CKEDITOR.instances[textareaName].getData(),
                    color:  $(this).find('.page-component-box[data-type=text] .sp-preview-inner').css('background-color').replace(/rgb|\)|\(|\s/g,'')
                };
                //Background Image
                temp.content.image = ($(this).find('.page-component-box[data-type=background] .image-preview img').length > 0)
                                        ? $(this).find('.page-component-box[data-type=background] .image-preview img').attr('src')
                                        : null;
                //Component composition
                temp.content.textAlign = ($(this).find('input[name*="textAlign"]:checked').length > 0)
                                            ? $(this).find('input[name*="textAlign"]:checked').val()
                                            : 'left';

                break;
            case 'GridBuilder':
                var items = [];
                $(this).find('.page-component-box.grid-builder').each(function(){
                    var tmp=[];
                    var config={
                        type:$(this).attr('data-items-type'),
                        location:$(this).attr('data-items-location'),
                        productsWidth:[]
                    }
                    var products=[];
                    $(this).find('.product-view-row-content-elem').each(function(){
                        config['productsWidth'].push($(this).attr('data-width'));
                        products.push({
                            id:$(this).attr('data-id'),
                            img:$(this).find('.content img').attr('src'),
                            title:$(this).find('.item-title').html()
                        });
                    });
                    tmp={
                        config:config,
                        products:products
                    };
                    items.push(tmp);
                });
                temp.content.items = items;
                break;

            case 'RequestForm':
                //Component title
                temp.content.title = {
                    tag:    ($(this).find('.page-component-box[data-type=title] input[name=captionTag]').val().trim().length > 0)
                        ? $(this).find('.page-component-box[data-type=title] input[name=captionTag]').val().trim()
                        : 'p',
                    val:    $(this).find('.page-component-box[data-type=title] input[name=title]').val(),
                    color:  $(this).find('.page-component-box[data-type=title] .sp-preview-inner').css('background-color').replace(/rgb|\)|\(|\s/g,''),
                    size: $(this).find('.page-component-box[data-type=title] input[name=titleSize]').val()
                };
                //Component button
                temp.content.button = {
                    buttonColor:$(this).find('.page-component-box[data-type=button] input[name=buttonColor]')
                        .next('.sp-replacer')
                        .find('.sp-preview-inner')
                        .css('background-color').replace(/rgb|\)|\(|\s/g,''),
                    hoverColor: $(this).find('.page-component-box[data-type=button] input[name=hoverColor]')
                        .next('.sp-replacer')
                        .find('.sp-preview-inner')
                        .css('background-color').replace(/rgb|\)|\(|\s/g,''),
                    val:        $(this).find('.page-component-box[data-type=button] input[name=buttonCaption]').val(),
                    color:      $(this).find('.page-component-box[data-type=button] input[name=titleColor]')
                        .next('.sp-replacer')
                        .find('.sp-preview-inner')
                        .css('background-color').replace(/rgb|\)|\(|\s/g,''),
                    link:       $(this).find('.page-component-box[data-type=button] input[name=buttonLink]').val()
                };
                //Component background color
                temp.content.background = $(this).find('.page-component-box[data-type=backgroundColor] input[name=backgroundColor]')
                                            .next('.sp-replacer')
                                            .find('.sp-preview-inner')
                                            .css('background-color').replace(/rgb|\)|\(|\s/g,'');
                break;
                case 'TextBlock':
                //Component title
                temp.content.title = {
                    tag:    ($(this).find('.page-component-box[data-type=title] input[name=captionTag]').val().trim().length > 0)
                        ? $(this).find('.page-component-box[data-type=title] input[name=captionTag]').val().trim()
                        : 'p',
                    val:    $(this).find('.page-component-box[data-type=title] input[name=title]').val(),
                    color:  $(this).find('.page-component-box[data-type=title] .sp-preview-inner').css('background-color').replace(/rgb|\)|\(|\s/g,''),
                    size: $(this).find('.page-component-box[data-type=title] input[name=titleSize]').val()
                };

                //Component text
                var textareaName = $(this).find('.page-component-box[data-type=text] textarea[name^=text_]').attr('name');
                var subTextName = $(this).find('.page-component-box[data-type=btn_render] textarea[name^=ckeditor]').attr('name');
                temp.content.text = {
                    val:    CKEDITOR.instances[textareaName].getData(),
                    color:  $(this).find('.page-component-box[data-type=text] .sp-preview-inner').css('background-color').replace(/rgb|\)|\(|\s/g,'')
                };
                temp.content.sub_text = {
                    val:    CKEDITOR.instances[subTextName].getData(),
                    color:  temp.content.text.color
                };
                //Component composition
                temp.content.textAlign = ($(this).find('.page-component-box[data-type*="textAlign"] input[name^="textAlign"]:checked').length > 0)
                                            ? $(this).find('.page-component-box[data-type*="textAlign"] input[name^="textAlign"]:checked').val()
                                            : 'left';

                temp.content.btn_render=($(this).find('.page-component-box[data-type=btn_render] input[name^="btn_render"]:checked').length > 0)
                                                ? $(this).find('.page-component-box[data-type=btn_render] input[name^="btn_render"]:checked').val()
                                                : '0';
                break;
        }
        data.custom_content.push(temp);
    });
    var action = $('form').attr('action').split('/');
    var url = ($.isNumeric(action[action.length -1]))
        ? '/backend/luckyweb/customcontent/pages/'+action[action.length -1]+'/edit'
        : '/backend/luckyweb/customcontent/pages/store';

    $.ajax({
        url:    url,
        type:   'POST',
        headers:{'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
        data:   {
            page: data
        },
        error:  function(jqXHR){
            console.error(jqXHR.responseText)
            lockMainSaveButtom(false);
        },
        success:function(response){
            if(response.message == 'success'){
                location = response.redirect;
            }
        }
    });
}


/**
 * Function returns html template of saved component
 * @param obj - component template
 */
function buildComponentView(obj){
    var componentBody =
    '<li class="page-components-list-wrap" data-component="'+obj.component_slug+'" data-id="'+obj.id+'">'+
        '<div class="page-position-controls">' +
            '<i class="icon-sort-asc" data-direction="up"></i>' +
            '<i class="icon-sort-desc" data-direction="down"></i>' +
        '</div>'+
        '<div class="page-component-wrap">'+
            '<div class="page-content-title">'+
                obj.name+
                '<ul>' +
                    '<li data-action="drop">Удалить</li>' +
                    /*'<li>|</li>' +
                    '<li data-action="drop">Удалить</li>' +*/
                '</ul>'+
            '</div>';

    switch(obj.component_slug){
        case 'BannerWithBackgroundImage':
            console.log(obj.content);
            componentBody+=get_BannerWithBackgroundImage(obj.content,obj.id);
            break;
            case 'PageCode':
                componentBody+='<div class="page-component-box" data-type="page-code">'+
                '<p class="page-component-subtitle">HTML</p>'+
                '<pre class="html_builder" style="width: 100%; padding: 10px; min-height: 200px; max-height: 350px; background-color: #fff;" contenteditable="true"></pre>'+
                '<button name="show_code">Показать код</button>'+
                '<button name="show_code_mobile">Показать код Mobile</button>'+
            '</div>';
            break;

        case 'FastRequestForm':

            var imageTag = (obj.content.image.length > 0)
                ? {img: '<img src="'+obj.content.image+'" alt="" data-type="file">', display: 'inline-block'}
                : {img: '', display: 'none'};

            var alignChecked = {left: '', center: '', right: ''};
            alignChecked[obj.content.textAlign] = 'checked="checked"';
            
            var title_size=14;
            if(typeof obj.content.title.size!='undefined'){
                title_size=obj.content.title.size;
            }
            
            componentBody +=
            '<div class="page-component-box" data-type="title">'+
                '<p class="page-component-subtitle">Заглавие</p>'+
                '<div class="label">'+
                    '<div class="page-flex-wrap">'+
                        '<div class="pseudo-selector">'+
                            '<select name="captionTagFake" class="pseudo-select-tag" placeholder="Тег&hellip;">'+
                                '<option value="p">&lt;p&gt;</option>'+
                                '<option value="h1">&lt;h1&gt;</option>'+
                                '<option value="h2">&lt;h2&gt;</option>'+
                                '<option value="h3">&lt;h3&gt;</option>'+
                                '<option value="h4">&lt;h4&gt;</option>'+
                                '<option value="h5">&lt;h5&gt;</option>'+
                                '<option value="h6">&lt;h6&gt;</option>'+
                                '<option value="span">&lt;span&gt;</option>'+
                                '<option value="strong">&lt;strong&gt;</option>'+
                            '</select>'+
                            '<input name="captionTag" class="pseudo-input-tag" type="text" placeholder="Тег&hellip;" style="width: 85px;" value="'+obj.content.title.tag+'">'+
                        '</div>'+
                        '<input name="title" class="input-text" type="text" placeholder="Текст заглавия&hellip;" value="'+obj.content.title.val+'">'+
                        '<input name="titleColor" class="color-picker" type="text" value="rgb('+obj.content.title.color+')">'+
                        '<span>Текст и цвет заглавия</span>'+
                    '</div>'+
                    '<input name="titleSize" class="input-text" type="number" style="min-width: 40px; width: 40px; padding: 8px 3px;" value="'+title_size+'">'+
                    '<span>Размер шрифта</span>'+
                '</div>'+
            '</div>'+
            '<div class="page-component-box" data-type="text">'+
                '<p class="page-component-subtitle">Текст</p>'+
                '<textarea class="textform needCKE" name="text_'+obj.id+'">'+obj.content.text.val+'</textarea>'+
                '<label>'+
                    '<input type="text" name="titleColor" class="color-picker" value="rgb('+obj.content.text.color+')">'+
                    '<span>Цвет текста</span>'+
                '</label>'+
            '</div>'+
            '<div class="page-component-box" data-type="button">'+
                '<p class="page-component-subtitle">Кнопка</p>'+
                '<label>'+
                    '<input name="buttonCaption" class="input-text" type="text" placeholder="Текст кнопки&hellip;" value="'+obj.content.button.val+'">'+
                    '<input type="text" name="titleColor" class="color-picker" value="rgb('+obj.content.button.color+')">'+
                    '<span>Текст и его цвет</span>'+
                '</label>'+
                '<label>'+
                    '<input name="buttonLink" class="input-text" type="text" placeholder="Ссылка&hellip;" value="'+obj.content.button.link+'">'+
                    '<span>Ссылка</span>'+
                '</label>'+
                '<label>'+
                    '<input type="text" name="buttonColor" class="color-picker" value="rgb('+obj.content.button.buttonColor+')">'+
                    '<span>Цвет кнопки</span>'+
                '</label>'+
                '<label>'+
                    '<input type="text" name="hoverColor" class="color-picker" value="rgb('+obj.content.button.hoverColor+')">'+
                    '<span>Цвет ховера</span>'+
                '</label>'+
            '</div>'+
            '<div class="page-component-box" data-type="background">'+
                '<p class="page-component-subtitle">Изображение бэкграунда</p>'+
                '<div>'+
                    '<div class="image-preview">'+imageTag.img+'</div>'+
                    '<div class="image-control">'+
                        '<input name="backgroundImage" type="file" style="display: none;">'+
                        '<button class="btn btn-primary" type="button" name="fakeOverview">Обзор&hellip;</button>'+
                        '<button class="btn btn-primary" type="button" name="imageClear" style="display: '+imageTag.display+';">Очистить&hellip;</button>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="page-component-box" data-type="textAlign">'+
                '<p class="page-component-subtitle">Размещение блока относительно бэкграунда</p>'+
                '<label>'+
                    '<input type="radio" name="textAlign_'+obj.id+'" value="left" '+alignChecked.left+'>'+
                    '<span>Слева</span>'+
                '</label>'+
                '<label>'+
                    '<input type="radio" name="textAlign_'+obj.id+'" value="center" '+alignChecked.center+'>'+
                    '<span>По центру</span>'+
                '</label>'+
                '<label>'+
                    '<input type="radio" name="textAlign_'+obj.id+'" value="right" '+alignChecked.right+'>'+
                    '<span>Справа</span>'+
                '</label>'+
            '</div>'+
            '<div class="page-component-box" data-type="marginBottom">'+
                '<p class="page-component-subtitle">Отступ снизу</p>'+
                '<label>'+
                    '<input class="input-text" type="number" name="marginBottom" value="'+obj.content.marginBottom+'" step="1" style="min-width: 100px;">'+
                    '<span>Отступ в пикселях</span>'+
                '</label>'+
            '</div>'+
            '<div class="page-component-box" data-type="marginTop">'+
                '<p class="page-component-subtitle">Отступ сверху</p>'+
                '<label>'+
                    '<input class="input-text" type="number" name="marginTop" value="'+obj.content.marginTop+'" step="1" style="min-width: 100px;">'+
                    '<span>Отступ в пикселях</span>'+
                '</label>'+
            '</div>'+
            '<div class="page-component-box" data-type="anchor">'+
                '<p class="page-component-subtitle">Якорь</p>'+
                '<label>'+
                    '<input class="input-text" type="number" name="anchor" value="'+obj.content.anchor+'" step="1" style="min-width: 100px;">'+
                '</label>'+
            '</div>'+
            '<div class="page-component-box" data-type="anchor">'+
                '<p class="page-component-subtitle">Якорь</p>'+
                '<label>'+
                    '<input class="input-text" type="text" name="anchor" value="'+obj.content.anchor+'" step="1" style="min-width: 100px;">'+
                '</label>'+
            '</div>';
            break;

        case 'GridBuilder':

            var textContent = {
                content:{
                    gridList: obj.content.items

                }
            }

            var gbSelectOptionList = [20,25,33,40,50,60,66,75,80,100];
            var gridList = textContent.content.gridList;

            componentBody += '<div class="grid-builder-list">';
            for(var gr in gridList){
                if(typeof gridList[gr]['config'] !='undefined'){
                var gridRow = gridList[gr];

                var rowLocation = gridRow['config']['location'];
                var rowType = gridRow['config']['type'];

                var itemsLocationCss = rowType === 'static' ? rowLocation : '';

                componentBody +=
                '<div class="page-component-box grid-builder" data-type="title" data-row-index="'+gr+'" data-items-type="'+rowType+'" data-items-location="'+rowLocation+'" >'+
                    '<div class="page-position-controls">' +
                        '<i class="icon-sort-asc top" ></i>' +
                        '<i class="icon-sort-desc bottom"></i>' +
                    '</div>'+
                    '<div class="product-view-rows-wrap">'+
                        '<div class="product-view-row">'+
                            '<div class="product-view-background '+itemsLocationCss+'">';

                for(var i in gridRow.products){

                    var elem = gridRow.products[i];
                    var elemsCount = gridRow.products.length
                    //console.info("elem", elem)
                    var elemWidth = null;

                    if(typeof gridRow['config']['productsWidth'] !== 'undefined'){
                        elemWidth = gridRow['config']['productsWidth'][i];
                    }else {
                        var elemWidth = +((100/elemsCount)).toFixed(0);
                    }

                    //build option to select
                    var gbOptionsList = '';
                    gbSelectOptionList.map(function(item){
                        var gbOptionSelected = item == elemWidth ? 'selected' : '';
                        gbOptionsList+='<option value="'+item+'" '+gbOptionSelected+'>'+item+'%</option>';
                    })

                    componentBody +=
                    '<div class="product-view-row-content-elem" data-id="'+elem.id+'" style="width:'+elemWidth+'%" data-width="'+elemWidth+'" data-item-index="'+i+'">'+
                        '<div class="content">'+
                            '<img src="'+elem.img+'" alt="">'+
                            '<div class="item-title">'+elem.title+'</div>'+
                            '<div class="drop-button-wrap"><i class="icon-times"></i></div>'+
                        '</div>'+
                        '<div class="item-controls">'+
                            '<div class="control-position">'+
                                '<div class="control-elem left"><i class="icon-arrow-left"></i></div>' +
                                '<div class="control-elem right"><i class="icon-arrow-right"></i></div>'+
                            '</div>'+
                            '<div class="control-width">'+
                                '<span>Размер елемента</span>'+
                                '<select name="select" data-last-value="'+elemWidth+'">'+
                                    gbOptionsList+
                                '</select>'+
                            '</div>'+
                        '</div>'+
                    '</div>';
                }

                var typeCheckedStatic = rowType === 'static' ? 'checked' : '';
                var typeCheckedDinamic = rowType === 'dinamic' ? 'checked' : '';
                var showLocationControls = typeCheckedStatic.length ? '' : 'hide';

                var locationCheckedLeft = rowLocation === 'left' ? 'checked' : '';
                var locationCheckedCenter = rowLocation === 'center' ? 'checked' : '';
                var locationCheckedRight = rowLocation === 'right' ? 'checked' : '';

                componentBody +=
                            '</div>'+
                            '<div class="product-bottom-info">'+
                                '<div class="product-info-validation"></div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="product-controls-row-elements">'+
                            '<div class="product-contrlors-item buttons">'+
                                '<button class="btn btn-primary add-element" >Добавить Елемент</button>'+
                                '<button class="btn btn-default remove-row" >Удалить ряд</button>'+
                            '</div>'+
                            '<div class="product-contrlors-item controls-type">'+
                                '<label>'+
                                    '<input type="radio" data-control="type" name="control-type-'+gr+'-'+obj.id+'" value="dinamic" '+typeCheckedDinamic+'>'+
                                    '<span>Динамичный</span>'+
                                '</label>'+
                                '<label>'+
                                    '<input type="radio" data-control="type" name="control-type-'+gr+'-'+obj.id+'" value="static" '+typeCheckedStatic+'>'+
                                    '<span>Статический</span>'+
                                '</label>'+
                            '</div>'+
                            '<div class="product-contrlors-item controls-location '+showLocationControls+'">'+
                                '<label>'+
                                    '<input type="radio" data-control="location" name="item-location-'+gr+'-'+obj.id+'" value="left" '+locationCheckedLeft+'>'+
                                    '<span>По левому</span>'+
                                '</label>'+
                                '<label>'+
                                    '<input type="radio" data-control="location" name="item-location-'+gr+'-'+obj.id+'" value="center" '+locationCheckedCenter+'>'+
                                    '<span>По центру</span>'+
                                '</label>'+
                                '<label>'+
                                    '<input type="radio" data-control="location" name="item-location-'+gr+'-'+obj.id+'" value="right" '+locationCheckedRight+'>'+
                                    '<span>По правому</span>'+
                                '</label>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'
                }
            }
            componentBody +='</div>'+
            '<div class="product-info-add-more">'+
                '<button class="btn btn-primary add-gridBuilder-row" >Добавить</button>'+
            '</div>'+
            '<div class="page-component-box" data-type="marginBottom">'+
                '<p class="page-component-subtitle">Отступ снизу</p>'+
                '<label>'+
                    '<input class="input-text" type="number" name="marginBottom" value="'+obj.content.marginBottom+'" step="1" style="min-width: 100px;">'+
                    '<span>Отступ в пикселях</span>'+
                '</label>'+
            '</div>'+
            '<div class="page-component-box" data-type="marginTop">'+
                '<p class="page-component-subtitle">Отступ сверху</p>'+
                '<label>'+
                    '<input class="input-text" type="number" name="marginTop" value="'+obj.content.marginTop+'" step="1" style="min-width: 100px;">'+
                    '<span>Отступ в пикселях</span>'+
                '</label>'+
            '</div>';
            break;

        case 'RequestForm':
            var title_size=14;
            if(typeof obj.content.title.size!='undefined'){
                title_size=obj.content.title.size;
            }
            componentBody+=
            '<div class="page-component-box" data-type="title">'+
                '<p class="page-component-subtitle">Заглавие</p>'+
                '<div class="label">'+
                    '<div class="page-flex-wrap">'+
                        '<div class="pseudo-selector">'+
                            '<select name="captionTagFake" class="pseudo-select-tag" placeholder="Тег&hellip;">'+
                                '<option value="p">&lt;p&gt;</option>'+
                                '<option value="h1">&lt;h1&gt;</option>'+
                                '<option value="h2">&lt;h2&gt;</option>'+
                                '<option value="h3">&lt;h3&gt;</option>'+
                                '<option value="h4">&lt;h4&gt;</option>'+
                                '<option value="h5">&lt;h5&gt;</option>'+
                                '<option value="h6">&lt;h6&gt;</option>'+
                                '<option value="span">&lt;span&gt;</option>'+
                                '<option value="strong">&lt;strong&gt;</option>'+
                            '</select>'+
                            '<input name="captionTag" class="pseudo-input-tag" type="text" placeholder="Тег&hellip;" style="width: 85px;" value="'+obj.content.title.tag+'">'+
                        '</div>'+
                        '<input name="title" class="input-text" type="text" placeholder="Текст заглавия&hellip;" value="'+obj.content.title.val+'">'+
                        '<input name="titleColor" class="color-picker" type="text" value="rgb('+obj.content.title.color+')">'+
                        '<span>Текст и цвет заглавия</span>'+
                    '</div>'+
                    '<input name="titleSize" class="input-text" type="number" style="min-width: 40px; width: 40px; padding: 8px 3px;" value="'+title_size+'">'+
                    '<span>Размер шрифта</span>'+
                '</div>'+
            '</div>'+
            '<div class="page-component-box" data-type="button">'+
                '<p class="page-component-subtitle">Кнопка</p>'+
                '<label>'+
                    '<input name="buttonCaption" class="input-text" type="text" placeholder="Текст кнопки&hellip;" value="'+obj.content.button.val+'">'+
                    '<input type="text" name="titleColor" class="color-picker" value="rgb('+obj.content.button.color+')">'+
                    '<span>Текст и его цвет</span>'+
                '</label>'+
                '<label>'+
                    '<input name="buttonLink" class="input-text" type="text" placeholder="Ссылка&hellip;" value="'+obj.content.button.link+'">'+
                    '<span>Ссылка</span>'+
                '</label>'+
                '<label>'+
                    '<input type="text" name="buttonColor" class="color-picker" value="rgb('+obj.content.button.buttonColor+')">'+
                    '<span>Цвет кнопки</span>'+
                '</label>'+
                '<label>'+
                    '<input type="text" name="hoverColor" class="color-picker" value="rgb('+obj.content.button.hoverColor+')">'+
                    '<span>Цвет ховера</span>'+
                '</label>'+
            '</div>'+
            '<div class="page-component-box" data-type="backgroundColor">'+
                '<p class="page-component-subtitle">Цвет фона</p>'+
                '<label>'+
                    '<input type="text" name="backgroundColor" class="color-picker" value="rgb('+obj.content.background+')">'+
                    '<span>Выбирите цвет</span>'+
                '</label>'+
            '</div>'+
            '<div class="page-component-box" data-type="marginBottom">'+
                '<p class="page-component-subtitle">Отступ снизу</p>'+
                '<label>'+
                    '<input class="input-text" type="number" name="marginBottom" value="'+obj.content.marginBottom+'" step="1" style="min-width: 100px;">'+
                    '<span>Отступ в пикселях</span>'+
                '</label>'+
            '</div>'+
            '<div class="page-component-box" data-type="anchor">'+
                '<p class="page-component-subtitle">Якорь</p>'+
                '<label>'+
                    '<input class="input-text" type="text" name="anchor" value="'+obj.content.anchor+'" step="1" style="min-width: 100px;">'+
                '</label>'+
            '</div>';
            break;

        case 'SmallBanner':
            var desktop=obj.content;
            //Размер картинки 
                //Desctop
                var imgDesctop = {width: 0, height: 0};
                if(typeof desktop.img != "undefined" && typeof desktop.img['height'] != "undefined"){
                    imgDesctop['width']=desktop.img['width'];
                    imgDesctop['height']=desktop.img['height'];
                }
            //END Размер картинки
           
            // Центровка Текста
                //desctop
            var alignTextChecked = {left: '', center: '', right: ''};
            if(typeof desktop.text.Align != "undefined"){
                alignTextChecked[desktop.text.Align] = 'checked="checked"';   
            }else{
                alignTextChecked['left'] = 'checked="checked"';   
            }
            //END центровка Текста
    
            //Отступы Текста
                //desctop
                var textLeftPadding=0;
                if(typeof desktop.text.LeftPadding != "undefined"){
                    textLeftPadding=desktop.text.LeftPadding
                }
                 var textRightPadding=0;
                if(typeof desktop.text.RightPadding != "undefined"){
                    textRightPadding=desktop.text.RightPadding
                }
            //END Отступы Текста
    
    
            
            // Центровка заголовка
                //desctop
            var alignTitleChecked = {left: '', center: '', right: ''};
            if(typeof desktop.title.Align != "undefined"){
                alignTitleChecked[desktop.title.Align] = 'checked="checked"';   
            }else{
                alignTitleChecked['left'] = 'checked="checked"';   
            }
            //END центровка заголовка
    
    
            //Отступы заголовка
                //desctop
                var titleLeftPadding=0;
                if(typeof desktop.title.LeftPadding != "undefined"){
                    titleLeftPadding=desktop.title.LeftPadding
                }
                 var titleRightPadding=0;
                if(typeof desktop.title.RightPadding != "undefined"){
                    titleRightPadding=desktop.title.RightPadding
                }
            //END Отступы заголовка
            
            var videoOrImage = (obj.content.videoOrImage.type == 'image')
                ? {
                    image:          (obj.content.videoOrImage.val.length > 0)
                            ?'<img src="'+obj.content.videoOrImage.val+'" data-type="file" alt="">'
                            : '',
                    imgChk:         'checked="checked"',
                    imgDisplay:     (obj.content.videoOrImage.val.length > 0)? 'inline-block': 'none',
                    imgBlockDisplay:'block',
                    video:          '',
                    videoChk:       '',
                    videoBlockDisplay: 'none'
                }
                : {
                    image:          '',
                    imgChk:         '',
                    imgDisplay:     'none',
                    imgBlockDisplay:'none',
                    video:          obj.content.videoOrImage.val,
                    videoChk:       'checked="checked"',
                    videoBlockDisplay: 'block'
                };

            var alignChecked = {left: '', center: '', right: ''};
            alignChecked[obj.content.textAlign] = 'checked="checked"';
            var show_text ='';
            //console.log(obj.content);
            if(obj.content.show_text=='1') show_text='checked="checked"';
            var show_btn ='';
            if(obj.content.show_btn=='1') show_btn='checked="checked"';
            var show_img ='';
            if(obj.content.show_img=='1') show_img='checked="checked"';
            
            var title_size=14;
            if(typeof obj.content.title.size!='undefined'){
                title_size=obj.content.title.size;
            }

            componentBody +=
            '<div class="page-component-box" data-type="title" data-show-inner="1">'+
                '<p class="page-component-subtitle">Заглавие</p>'+
                '<div class="show_chk show_text">'+
                    '<label>'+
                        '<input type="checkbox" name="show_'+obj.id+'" value="1" '+show_text+' data-show-inner-checkbox >'+
                        '<span>Отображать</span>'+
                    '</label>'+
                '</div>'+
                '<div class="label">'+
                    '<div class="page-flex-wrap">'+
                        '<div class="pseudo-selector">'+
                            '<select name="captionTagFake" class="pseudo-select-tag" placeholder="Тег&hellip;">'+
                                '<option value="h1">&lt;h1&gt;</option>'+
                                '<option value="h2">&lt;h2&gt;</option>'+
                                '<option value="h3">&lt;h3&gt;</option>'+
                                '<option value="h4">&lt;h4&gt;</option>'+
                                '<option value="h5">&lt;h5&gt;</option>'+
                                '<option value="h6">&lt;h6&gt;</option>'+
                            '</select>'+
                            '<input name="captionTag" class="pseudo-input-tag" type="text" placeholder="Тег&hellip;" style="width: 85px;" value="'+obj.content.title.tag+'">'+
                        '</div>'+
                        '<input name="title" class="input-text" type="text" placeholder="Текст заглавия&hellip;" value="'+obj.content.title.val+'">'+
                        '<input name="titleColor" class="color-picker" type="text" value="rgb('+obj.content.title.color+')">'+
                        '<span>Текст и цвет заглавия</span>'+
                    '</div>'+
                    '<input name="titleSize" class="input-text" type="number" style="min-width: 40px; width: 40px; padding: 8px 3px;" value="'+title_size+'">'+
                    '<span>Размер шрифта</span>'+
                '</div>'+
                '<div>'+
                    '<p class="page-component-subtitle">Выравнивание</p>'+
                    '<label>'+
                        '<input type="radio" name="titleAlign_'+obj.id+'" value="left" '+alignTitleChecked['left']+'>'+
                        '<span>Слева</span>'+
                    '</label>'+
                    '<label>'+
                        '<input type="radio" name="titleAlign_'+obj.id+'" value="center" '+alignTitleChecked['center']+'>'+
                        '<span>По центру</span>'+
                    '</label>'+
                    '<label>'+
                        '<input type="radio" name="titleAlign_'+obj.id+'" value="right" '+alignTitleChecked['right']+'>'+
                        '<span>Справа</span>'+
                    '</label>'+
                '</div>'+
                '<div>'+
                    '<input name="titleLeftPadding" class="input-text" type="number" style="min-width: 40px; width: 40px; padding: 8px 3px;" value="'+titleLeftPadding+'">'+
                    '<span>Отступ слева</span>'+
                    '<input name="titleRightPadding" class="input-text" type="number" style="min-width: 40px; width: 40px; padding: 8px 3px;" value="'+titleRightPadding+'">'+
                    '<span>Отступ справа</span>'+
                '</div>'+
            '</div>'+
            '<div class="page-component-box" data-type="text" data-show-inner="1">'+
                '<hr>'+
                '<p class="page-component-subtitle">Текст</p>'+
                '<textarea class="textform needCKE" name="text_'+obj.id+'">'+obj.content.text.val+'</textarea>'+
                '<label>'+
                    '<input type="text" name="titleColor" class="color-picker" value="rgb('+obj.content.text.color+')">'+
                    '<span>Цвет текста</span>'+
                '</label>'+
                '<div>'+
                    '<p class="page-component-subtitle">Выравнивание</p>'+
                    '<label>'+
                        '<input type="radio" name="contentAlign_'+obj.id+'" value="left" '+alignTextChecked['left']+'>'+
                        '<span>Слева</span>'+
                    '</label>'+
                    '<label>'+
                        '<input type="radio" name="contentAlign_'+obj.id+'" value="center" '+alignTextChecked['center']+'>'+
                        '<span>По центру</span>'+
                    '</label>'+
                    '<label>'+
                        '<input type="radio" name="contentAlign_'+obj.id+'" value="right" '+alignTextChecked['right']+'>'+
                        '<span>Справа</span>'+
                    '</label>'+
                '</div>'+
                '<div>'+
                    '<input name="contentLeftPadding" class="input-text" type="number" style="min-width: 40px; width: 40px; padding: 8px 3px;" value="'+textLeftPadding+'">'+
                    '<span>Отступ слева</span>'+
                    '<input name="contentRightPadding" class="input-text" type="number" style="min-width: 40px; width: 40px; padding: 8px 3px;" value="'+textRightPadding+'">'+
                    '<span>Отступ справа</span>'+
                '</div>'+
                '<hr>'+
            '</div>'+

            '<div class="page-component-box" data-type="typeSelector" >'+
                '<p class="page-component-subtitle">Выбор типа кнопки</p>';

            if(typeof obj.content.button.textTypeButton!='undefined' && typeof obj.content.button.type_selector_text!='undefined' ){
                var type_selector_text = {text: '', href: ''};
                type_selector_text[obj.content.button.type_selector_text] = 'checked="checked"';
                componentBody +=
                    '<label>'+
                        '<input type="radio" class="type_selector" name="type_selector_'+obj.id+'" value="href" '+type_selector_text.href+'>'+
                        '<span>Ссылка</span>'+
                    '</label>'+
                    '<label>'+
                        '<input type="radio" class="type_selector" name="type_selector_'+obj.id+'" value="text" '+type_selector_text.text+'>'+
                        '<span>Текст</span>'+
                    '</label>';

            }else{
                componentBody +=
                    '<label>'+
                        '<input type="radio" class="type_selector" name="type_selector_'+obj.id+'" value="href" checked="checked">'+
                        '<span>Ссылка</span>'+
                    '</label>'+
                    '<label>'+
                        '<input type="radio" class="type_selector" name="type_selector_'+obj.id+'" value="text">'+
                        '<span>Текст</span>'+
                    '</label>';
            }
            if(typeof obj.content.button.textTypeButton !='undefined' && typeof obj.content.button.type_selector_text!='undefined'){
                if(obj.content.button.type_selector_text!='text'){
                    componentBody +='<div class="txt_btn_container" style="display:none;"><p class="page-component-subtitle">Текст подробнее для кнопки</p>';
                }else{
                    componentBody +='<div class="txt_btn_container"><p class="page-component-subtitle">Текст подробнее для кнопки</p>';
                }
            }else{
                componentBody +='<div class="txt_btn_container" style="display:none;"><p class="page-component-subtitle">Текст подробнее для кнопки</p>';
            }

            if(typeof obj.content.button.textTypeButton !='undefined' && typeof obj.content.button.type_selector_text!='undefined'){
                componentBody +=
                        '<textarea class="textform needCKE" name="textTypeButton_'+obj.id+'">'+obj.content.button.textTypeButton.val+'</textarea>';
            }else{
                componentBody +=
                        '<textarea class="textform needCKE" style="dispaly:none;" name="textTypeButton_'+obj.id+'"></textarea>';
            }
            componentBody +='</div>';
            componentBody +=
                '</div>'+
                '<div class="page-component-box" data-type="button" data-show-inner="2">'+
                    '<p class="page-component-subtitle">Кнопка</p>'+
                    '<div class="show_chk show_btn">'+
                        '<label>'+
                            '<input type="checkbox" name="show_'+obj.id+'" value="1" '+show_btn+' data-show-inner-checkbox >'+
                            '<span>Отображать</span>'+
                        '</label>'+
                    '</div>'+
                    '<label>'+
                        '<input name="buttonCaption" class="input-text" type="text" placeholder="Текст кнопки&hellip;" value="'+obj.content.button.val+'">'+
                        '<input type="text" name="titleColor" class="color-picker" value="rgb('+obj.content.button.color+')">'+
                        '<span>Текст и его цвет</span>'+
                    '</label>';
                    if(typeof obj.content.button.textTypeButton !='undefined' && typeof obj.content.button.type_selector_text!='undefined'){
                            if(obj.content.button.type_selector_text!='text'){
                                componentBody +=
                                        '<label class="href_input">'+
                                            '<input name="buttonLink" class="input-text" type="text" placeholder="Ссылка&hellip;" value="'+obj.content.button.link+'">'+
                                            '<span>Ссылка</span>'+
                                        '</label>';
                            }else{
                                componentBody +=
                                        '<label class="href_input" style="display:none;">'+
                                            '<input name="buttonLink" class="input-text" type="text" placeholder="Ссылка&hellip;" value="'+obj.content.button.link+'">'+
                                            '<span>Ссылка</span>'+
                                        '</label>';
                            }
                        }else{
                            componentBody +=
                                    '<label class="href_input">'+
                                        '<input name="buttonLink" class="input-text" type="text" placeholder="Ссылка&hellip;" value="'+obj.content.button.link+'">'+
                                        '<span>Ссылка</span>'+
                                    '</label>';
                        }
                        componentBody +=
                    '<label>'+
                        '<input type="text" name="buttonColor" class="color-picker" value="rgb('+obj.content.button.buttonColor+')">'+
                        '<span>Цвет кнопки</span>'+
                    '</label>'+
                    '<label>'+
                        '<input type="text" name="hoverColor" class="color-picker" value="rgb('+obj.content.button.hoverColor+')">'+
                        '<span>Цвет ховера</span>'+
                    '</label>'+
                '</div>'+
                '<div class="page-component-box" data-type="videoOrImage" data-show-inner="3">'+
                    '<p class="page-component-subtitle">Дополнительный контент баннера</p>'+
                    '<div class="show_chk show_img">'+
                        '<label>'+
                            '<input type="checkbox" name="show_'+obj.id+'" value="1" '+show_img+' data-show-inner-checkbox >'+
                            '<span>Отображать</span>'+
                        '</label>'+
                    '</div>'+
                    '<div>'+
                        '<label style="display: inline">'+
                            '<input name="videoOrImage_'+obj.id+'" type="radio" value="image" '+videoOrImage.imgChk+'>'+
                            '<span>Изображение</span>'+
                        '</label>'+
                        '<label style="display: inline">'+
                            '<input name="videoOrImage_'+obj.id+'" type="radio" value="video" '+videoOrImage.videoChk+'>'+
                            '<span>Видео</span>'+
                        '</label>'+
                        '<div data-type="image" style="display: '+videoOrImage.imgBlockDisplay+'">'+
                            '<div class="image-preview">'+videoOrImage.image+'</div>'+
                            '<div class="image-control">'+
                                '<input name="backgroundImage" type="file" style="display: none;">'+
                                '<button class="btn btn-primary" type="button" name="fakeOverview">Обзор&hellip;</button>'+
                                '<button class="btn btn-primary" type="button" name="imageClear" style="display: '+videoOrImage.imgDisplay+'">Очистить&hellip;</button>'+
                            '</div>'+
                        '</div>'+
                        '<div data-type="video" style="display: '+videoOrImage.videoBlockDisplay+'">'+
                            '<textarea class="textform" name="videoFrame_'+obj.id+'" placeholder="Вставьте сюда iframe видео&hellip;">'+videoOrImage.video+'</textarea>'+
                        '</div>'+
                    '</div>'+
                    '<p class="page-component-subtitle">Размеры картинки/видео</p>'+
                    '<div>'+
                        '<input name="imgWidth" class="input-text" type="number" style="min-width: 80px; width: 80px; padding: 8px 3px;" value="'+imgDesctop['width']+'">'+
                        '<span>Ширина</span>'+
                        '<input name="imgHeight" class="input-text" type="number" style="min-width: 80px; width: 80px; padding: 8px 3px;" value="'+imgDesctop['height']+'">'+
                        '<span>Высота</span>'+
                    '</div>'+
                '</div>'+
                '<div class="page-component-box" data-type="textAlign">'+
                    '<p class="page-component-subtitle">Размещение изображения(видео) баннера относительно текста</p>'+
                    '<label>'+
                        '<input type="radio" name="textAlign_'+obj.id+'" value="left" '+alignChecked.left+'>'+
                        '<span>Слева</span>'+
                    '</label>'+
                    '<label>'+
                        '<input type="radio" name="textAlign_'+obj.id+'" value="center" '+alignChecked.center+'>'+
                        '<span>Снизу</span>'+
                    '</label>'+
                    '<label>'+
                        '<input type="radio" name="textAlign_'+obj.id+'" value="right" '+alignChecked.right+'>'+
                        '<span>Справа</span>'+
                    '</label>'+
                '</div>'+
                '<div class="page-component-box" data-type="marginBottom">'+
                    '<p class="page-component-subtitle">Отступ снизу</p>'+
                    '<label>'+
                        '<input class="input-text" type="number" name="marginBottom" value="'+obj.content.marginBottom+'" step="1" style="min-width: 100px;">'+
                        '<span>Отступ в пикселях</span>'+
                    '</label>'+
                '</div>'+
                '<div class="page-component-box" data-type="marginTop">'+
                    '<p class="page-component-subtitle">Отступ сверху</p>'+
                    '<label>'+
                        '<input class="input-text" type="number" name="marginTop" value="'+obj.content.marginTop+'" step="1" style="min-width: 100px;">'+
                        '<span>Отступ в пикселях</span>'+
                    '</label>'+
                '</div>'+
                '<div class="page-component-box" data-type="anchor">'+
                    '<p class="page-component-subtitle">Якорь</p>'+
                    '<label>'+
                        '<input class="input-text" type="text" name="anchor" value="'+obj.content.anchor+'" step="1" style="min-width: 100px;">'+
                    '</label>'+
                '</div>';
            break;

        case 'TextBlock':
            var alignChecked = {left: '', center: '', right: ''};
            var btn_render='';
            alignChecked[obj.content.textAlign] = 'checked="checked"';
            if(obj.content.btn_render=='1'){
                btn_render='checked="checked"';
            }
            
            var title_size=14;
            if(typeof obj.content.title.size!='undefined'){
                title_size=obj.content.title.size;
            }
            
            componentBody +=
            '<div class="page-component-box" data-type="title">'+

                '<p class="page-component-subtitle">Заглавие</p>'+
                '<div class="label">'+
                    '<div class="page-flex-wrap">'+
                        '<div class="pseudo-selector">'+
                            '<select name="captionTagFake" class="pseudo-select-tag" placeholder="Тег&hellip;">'+

                                '<option value="h1">&lt;h1&gt;</option>'+
                                '<option value="h2">&lt;h2&gt;</option>'+
                                '<option value="h3">&lt;h3&gt;</option>'+
                                '<option value="h4">&lt;h4&gt;</option>'+
                                '<option value="h5">&lt;h5&gt;</option>'+
                                '<option value="h6">&lt;h6&gt;</option>'+
                            '</select>'+
                            '<input name="captionTag" class="pseudo-input-tag" type="text" placeholder="Тег&hellip;" style="width: 85px;" value="'+obj.content.title.tag+'">'+
                        '</div>'+
                        '<input name="title" class="input-text" type="text" placeholder="Текст заглавия&hellip;" value="'+obj.content.title.val+'">'+
                        '<input name="titleColor" class="color-picker" type="text" value="rgb('+obj.content.title.color+')">'+
                        '<span>Текст и цвет заглавия</span>'+
                    '</div>'+
                    '<input name="titleSize" class="input-text" type="number" style="min-width: 40px; width: 40px; padding: 8px 3px;" value="'+title_size+'">'+
                    '<span>Размер шрифта</span>'+
                '</div>'+
            '</div>'+
            '<div class="page-component-box" data-type="text">'+
                '<p class="page-component-subtitle">Текст</p>'+
                '<textarea class="textform needCKE" name="text_'+obj.id+'">'+obj.content.text.val+'</textarea>'+
                '<label>'+
                    '<input type="text" name="titleColor" class="color-picker" value="rgb('+obj.content.text.color+')">'+
                    '<span>Цвет текста</span>'+
                '</label>'+
            '</div>'+
            '<div class="page-component-box" data-type="textAlign">'+
                '<p class="page-component-subtitle">Размещение блока относительно бэкграунда</p>'+
                '<label>'+
                    '<input type="radio" name="textAlign_'+obj.id+'" value="left" '+alignChecked.left+'>'+
                    '<span>Слева</span>'+
                '</label>'+
                '<label>'+
                    '<input type="radio" name="textAlign_'+obj.id+'" value="center" '+alignChecked.center+'>'+
                    '<span>По центру</span>'+
                '</label>'+
                '<label>'+
                    '<input type="radio" name="textAlign_'+obj.id+'" value="right" '+alignChecked.right+'>'+
                    '<span>Справа</span>'+
                '</label>'+
            '</div>'+
            '<div class="page-component-box" data-type="btn_render">'+
                '<p class="page-component-subtitle">Отображение кнопки разворота/сворота текста</p>'+
                '<label>'+
                    '<input type="checkbox" name="btn_render" value="1" '+btn_render+' >'+
                    '<span>Отображать</span>'+
                '</label>'+
                '<div class="close_text_cke" style="display:none">'+
                    '<p class="page-component-subtitle">Текст</p>'+
                    '<textarea class="textform needCKE" name="ckeditor_"'+obj.id+'>'+obj.content.sub_text.val+'</textarea>'+
                '</div>'+
            '</div>'+
            '<div class="page-component-box" data-type="marginBottom">'+
                '<p class="page-component-subtitle">Отступ снизу</p>'+
                '<label>'+
                    '<input class="input-text" type="number" name="marginBottom" value="'+obj.content.marginBottom+'" step="1" style="min-width: 100px;">'+
                    '<span>Отступ в пикселях</span>'+
                '</label>'+
            '</div>'+
            '<div class="page-component-box" data-type="marginTop">'+
                '<p class="page-component-subtitle">Отступ сверху</p>'+
                '<label>'+
                    '<input class="input-text" type="number" name="marginTop" value="'+obj.content.marginTop+'" step="1" style="min-width: 100px;">'+
                    '<span>Отступ в пикселях</span>'+
                '</label>'+
            '</div>'+
            '<div class="page-component-box" data-type="anchor">'+
                '<p class="page-component-subtitle">Якорь</p>'+
                '<label>'+
                    '<input class="input-text" type="text" name="anchor" value="'+obj.content.anchor+'" step="1" style="min-width: 100px;">'+
                '</label>'+
            '</div>';
        break;

        case 'ImageGridBuilder':

            var gbSelectOptionList = [10,20,25,33,40,50,60,66,75,80,100];
            if(obj.content === null){
                return false;
            }
            var imageGridList = obj.content.items;

            componentBody += '<div class="imagegrid-builder-list">';
            for(var gr in imageGridList){

                if(typeof imageGridList[gr]['config'] !='undefined'){
                var gridRow = imageGridList[gr];

                var rowLocation = gridRow['config']['location'];
                var rowType = gridRow['config']['type'];

                var itemsLocationCss = rowType === 'static' ? rowLocation : '';

                componentBody +=
                '<div class="page-component-box grid-builder" data-type="title" data-row-index="'+gr+'" data-items-type="'+rowType+'" data-items-location="'+rowLocation+'" data-show-mobile="true">'+
                    '<div class="page-position-controls">' +
                        '<i class="icon-sort-asc top" ></i>' +
                        '<i class="icon-sort-desc bottom"></i>' +
                    '</div>'+
                    '<div class="product-view-rows-wrap">'+
                        '<div class="product-view-row">'+
                            '<div class="product-view-background '+itemsLocationCss+'">';

                for(var i in gridRow.products){

                    var elem = gridRow.products[i];
                    //console.info("elem", elem)
                    var elemsCount = gridRow.products.length
                    //console.info("elem", elem)
                    var elemWidth = null;

                    if(typeof gridRow['config']['productsWidth'] !== 'undefined'){
                        elemWidth = gridRow['config']['productsWidth'][i];
                    }else {
                        var elemWidth = +((100/elemsCount)).toFixed(0);
                    }

                    //build option to select
                    var gbOptionsList = '';
                    gbSelectOptionList.map(function(item){
                        var gbOptionSelected = item == elemWidth ? 'selected' : '';
                        gbOptionsList+='<option value="'+item+'" '+gbOptionSelected+'>'+item+'%</option>';
                    })

                    var itemText = $(elem.text).text().substring(0,20)+'...';

                    var textVerticalAlign = elem.textVerticalAlign || 'center';

                    componentBody +=
                    '<div class="product-view-row-content-elem image-preview" style="width:'+elemWidth+'%" data-width="'+elemWidth+'" data-item-index="'+i+'" data-item-image="'+elem.img+'" data-item-link="'+elem.href+'" data-item-text-color="'+elem.color+'" data-item-text-vertical-align="'+textVerticalAlign+'">'+
                        '<div class="content">'+
                            '<img src="'+elem.img+'" alt="" data-type="file">'+
                            '<div class="item-title">'+itemText+'</div>'+
                            '<div class="drop-button-wrap"><i class="icon-times"></i></div>'+
                            '<div class="drop-button-wrap edit"><i class="icon-edit"></i></div>'+
                        '</div>'+
                        '<div class="item-controls">'+
                            '<div class="control-position">'+
                                '<div class="control-elem left"><i class="icon-arrow-left"></i></div>' +
                                '<div class="control-elem right"><i class="icon-arrow-right"></i></div>'+
                            '</div>'+
                            '<div class="control-width">'+
                                '<span>Размер елемента</span>'+
                                '<select name="select" data-last-value="'+elemWidth+'">'+
                                    gbOptionsList+
                                '</select>'+
                            '</div>'+
                        '</div>'+
                        '<div class="data-item-hidden-text">'+
                           elem.text +
                        '</div>'+
                    '</div>';

                }


                var typeCheckedStatic = rowType === 'static' ? 'checked' : '';
                var typeCheckedDinamic = rowType === 'dinamic' ? 'checked' : '';
                var showLocationControls = typeCheckedStatic.length ? '' : 'hide';

                var locationCheckedLeft = rowLocation === 'left' ? 'checked' : '';
                var locationCheckedCenter = rowLocation === 'center' ? 'checked' : '';
                var locationCheckedRight = rowLocation === 'right' ? 'checked' : '';

                componentBody +=
                            '</div>'+
                            '<div class="product-bottom-info">'+
                                '<div class="product-info-validation"></div>'+
                        '</div>'+
                        '</div>'+
                        '<div class="product-controls-row-elements">'+
                            '<div class="product-contrlors-item buttons">'+
                                '<button class="btn btn-primary add-img-element" data-popup="#img-grid-builder-popup" >Добавить Елемент</button>'+
                                '<button class="btn btn-default remove-row" >Удалить ряд</button>'+
                            '</div>'+
                            '<div class="product-contrlors-item controls-type">'+
                                '<label>'+
                                    '<input type="radio" data-control="type" name="control-type-'+gr+'-'+obj.id+'" value="dinamic" '+typeCheckedDinamic+'>'+
                                    '<span>Динамичный</span>'+
                                '</label>'+
                                '<label>'+
                                    '<input type="radio" data-control="type" name="control-type-'+gr+'-'+obj.id+'" value="static" '+typeCheckedStatic+'>'+
                                    '<span>Статический</span>'+
                                '</label>'+
                            '</div>'+
                            '<div class="product-contrlors-item controls-location '+showLocationControls+'">'+
                                '<label>'+
                                    '<input type="radio" data-control="location" name="item-location-'+gr+'-'+obj.id+'" value="left" '+locationCheckedLeft+'>'+
                                    '<span>По левому</span>'+
                                '</label>'+
                                '<label>'+
                                    '<input type="radio" data-control="location" name="item-location-'+gr+'-'+obj.id+'" value="center" '+locationCheckedCenter+'>'+
                                    '<span>По центру</span>'+
                                '</label>'+
                                '<label>'+
                                    '<input type="radio" data-control="location" name="item-location-'+gr+'-'+obj.id+'" value="right" '+locationCheckedRight+'>'+
                                    '<span>По правому</span>'+
                                '</label>'+
                            '</div>'+
                            '<div class="product-contrlors-item controls-show-mobile">'+
                                '<label>'+
                                    '<input type="checkbox" data-control="mobile"  name="imagegrid-item-show-mobile-'+gr+'-'+obj.id+'" checked="true" >'+
                                    '<span>Отображать на мобильном ?</span>'+
                                '</label>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'
                }
            }

            componentBody +='</div>'+
            '<div class="product-info-add-more">'+
                '<button class="btn btn-primary add-image-GridBuilder-row" >Добавить</button>'+
            '</div>'+
            '<div class="page-component-box" data-type="marginBottom">'+
                '<p class="page-component-subtitle">Отступ снизу</p>'+
                '<label>'+
                    '<input class="input-text" type="number" name="marginBottom" value="'+obj.content.marginBottom+'" step="1" style="min-width: 100px;">'+
                    '<span>Отступ в пикселях</span>'+
                '</label>'+
            '</div>'+
            '<div class="page-component-box" data-type="marginTop">'+
                '<p class="page-component-subtitle">Отступ сверху</p>'+
                '<label>'+
                    '<input class="input-text" type="number" name="marginTop" value="'+obj.content.marginTop+'" step="1" style="min-width: 100px;">'+
                    '<span>Отступ в пикселях</span>'+
                '</label>'+
            '</div>' +
            '<div class="page-component-box" data-type="anchor">'+
                '<p class="page-component-subtitle">Якорь</p>'+
                '<label>'+
                    '<input class="input-text" type="text" name="anchor" value="'+obj.content.anchor+'" step="1" style="min-width: 100px;">'+
                '</label>'+
            '</div>';
        break;

    }

    componentBody +=
        '</div>'+
    '</li>';
    $('.layout-row[data-rel=page-content] .components-details-wrap .page-content-wrap').append(componentBody);

    dynamicCKEreplace();

    $.event.trigger({
        type: "ComponentViewCreate"
    });
}

/**
 * Override textareas
 */
function dynamicCKEreplace(){
    $('.page-content-wrap .needCKE').each(function(){
        var textareaName = $(this).attr('name');
        if($.isEmptyObject(CKEDITOR.instances[textareaName])){
            CKEDITOR.replace(textareaName);
        }
    });
}


// Отображение кнопки разворота/сворота текста в компоненте "TextBlock"
function showMoreInTextBlockComponent() {

    $(document).bind('ComponentViewCreate', function (e) {
        $('[data-component="TextBlock"]').each(function(i,item){
            var checkbox = $(item).find('[data-type="btn_render"] input[name="btn_render"]');
            if(checkbox[0].checked){
                var holder = checkbox.closest('[data-type="btn_render"]');
                holder.find('.close_text_cke').show();
            }
        })
    });

    $(document).on('change','[data-type="btn_render"] input[name="btn_render"]',function(e){
        var holder = $(e.target).closest('[data-type="btn_render"]');
        var holderTextArea = holder.find('.close_text_cke');
        var showTextArea = e.target.checked;
        showTextArea ? holderTextArea.show() : holderTextArea.hide();
    })
}

//Функционал показывания/скрытия  розметки по  чекбоксу
function showHideMarkupInComponentsInner() {

    $(document).bind('ComponentViewCreate', function (e) {

        showHideMarkup();

        showHideMarkupOnInit();
    })

    showHideMarkup();

    function showHideMarkup() {
        $('.page-components-list-wrap').each(function(i,item){
           var showInner = $(item).find('[data-show-inner]');
           var id = showInner.attr('data-show-inner');

           showInner.each(function(i,showInnerItem){

                var checkbox = $(showInnerItem).find('[data-show-inner-checkbox]');

                checkbox.unbind('change').change(function(e){
                    var showTextArea = e.target.checked;

                    var buttonHolder = $(e.target).closest('.show_chk');
                    showTextArea ? buttonHolder.nextAll().slideDown() : buttonHolder.nextAll().slideUp();

                    if($(e.target).closest(showInner).siblings('[data-show-inner="'+id+'"]')){
                        var showInnerSibling = $(e.target).closest(showInner).siblings('[data-show-inner="'+id+'"]')

                        if(!showInnerSibling.find('.show_chk').length){
                             showTextArea ? showInnerSibling.slideDown() : showInnerSibling.slideUp();
                        }
                    }

                })
           })
        })
    }

    function showHideMarkupOnInit() {
        $('.page-components-list-wrap').each(function(i,item){
            var showInner = $(item).find('[data-show-inner]');

            var id = showInner.attr('data-show-inner');

            showInner.each(function(i,showInnerItem){
                var checkbox = $(showInnerItem).find('[data-show-inner-checkbox]');

                if(checkbox.length){
                    var checkboxIsChecked = checkbox[0].checked;

                    if(!checkboxIsChecked){
                        var buttonHolder = checkbox.closest('.show_chk');
                        buttonHolder.nextAll().hide();

                        if(checkbox.closest(showInner).siblings('[data-show-inner="'+id+'"]')){
                            var showInnerSibling = checkbox.closest(showInner).siblings('[data-show-inner="'+id+'"]')

                            if(!showInnerSibling.find('.show_chk').length){
                                showInnerSibling.hide()
                            }
                        }
                    }
                }
            })
        })
    }

}

function lockMainSaveButtom(lock) {
    var button = $('button[name=saveAndExit],button[name=save]');
    var spinner = $('.hidden_block .loader-spinner');
    lock ? button.attr('disabled',true) : button.attr('disabled',false);
    lock ? spinner.addClass('show') : spinner.removeClass('show');
}


function componentTabsFunc() {
    $(document).on('click','.tabs-head-elem',function(e){
        var tabHead = $(e.currentTarget);
        var tabHolder = tabHead.closest('.page-component-tabs');
        var index = tabHead.index();
        var tabItems = tabHolder.find('.page-component-tabs-item');

        tabHolder.find('.tabs-head-elem').removeClass('active')
        tabHead.addClass('active');

        tabItems.removeClass('active').eq(index).addClass('active');
    })
}

function componentTabsDublInputNamesFix() {
    $(document).on('change', 'li[data-component] input',function(e){
        console.log(e)
        if(e.currentTarget.type === 'radio'){
            var input = $(e.currentTarget);
            console.info("input", input)
            var inputName = e.currentTarget.name;
            console.info("inputName", inputName)
            var component = input.closest('[data-component]');
            console.info("component", component)

            if(component.find('input[name="'+inputName+'"]').length > 1 ){
console.info("component.find('input[name=\"'+inputName+'\"]')", component.find('input[name="'+inputName+'"]'))
                input.prop('checked',true);
            }
        }

    })
}



$(document).ready(function(){
    $(document).on('click','button[name=show_code]',function(e){
        if($('a.goto-page').length>0){
            $.ajax({
                url:    '/backend/luckyweb/customcontent/pages/get_html'+$('a.goto-page').attr('data-slug'),
                type:   'GET',
                error:  function(jqXHR){
                    console.error(jqXHR.responseText)
                },
                success:function(response){
                    $('.html_builder').html(response);
                }
            });
        }
        e.preventDefault();
        return false;
    });
    $(document).on('click','button[name=show_code_mobile]',function(e){
        if($('a.goto-page').length>0){
            $.ajax({
                url:    '/backend/luckyweb/customcontent/pages/get_html_mobile'+$('a.goto-page').attr('data-slug'),
                type:   'GET',
                error:  function(jqXHR){
                    console.error(jqXHR.responseText)
                },
                success:function(response){
                    $('.html_builder').html(response);
                }
            });
        }
        e.preventDefault();
        return false;
    });
    componentTabsFunc();
    componentTabsDublInputNamesFix();

    //Tabs
    $('.tabs-wrap .tab-element').click(function(){
        var rel = $(this).attr('data-rel');
        $(this).closest('.tabs-wrap').find('.tab-element').removeClass('active');
        $(this).addClass('active');

        $('form.layout .layout-row[data-rel]').hide();
        $('form.layout .layout-row[data-rel='+rel+']').show();
    });

    //Close validation error pop-up
    $(document).on('click','p.flash-message .close', function(){
        $(this).closest('p.flash-message').remove();
    });

    //Pseudo-selector
    $('.page-content-wrap').on('change', '.pseudo-select-tag', function(){
        $(this).closest('.pseudo-selector').find('.pseudo-input-tag').val($(this).val())
    });

    //Image overview click
    $('.page-content-wrap').on('click', 'button[name=fakeOverview]', function(){
        $(this).closest('.image-control').find('input[name=backgroundImage]').trigger('click');
    });

    //Upload Image
    $('.page-content-wrap').on('change', 'input[type=file]', function(){
        var reader = new FileReader();
        var _this = $(this);
        reader.onload = function(e){
            _this.closest('.page-component-box').find('.image-preview').empty().append('<img src="'+e.target.result+'" alt="" data-type="upload">')
            _this.closest('.page-component-box').find('.image-control button[name=imageClear]').show();
        };
        reader.readAsDataURL(_this.prop('files')[0]);
    });

    //Image clear click
    $('.page-content-wrap').on('click', 'button[name=imageClear]', function(){
        $(this).closest('.page-component-box').find('.image-preview img').remove();
        $(this).closest('.page-component-box').find('input[type=file]').val(null);
    });

    //Video or image block change radio-button
    $('.page-content-wrap').on('change', 'input[name^=videoOrImage]', function(){
        var type = $(this).val();
        $(this).closest('.page-component-box').find('div[data-type]').hide();
        $(this).closest('.page-component-box').find('div[data-type='+type+']').show();
    });

    //Get components list
    if($('form.layout').length > 0){
        //Get form action
        var pageAction = $('form.layout').attr('action').split('/');
        //If there is update action -> get id
        var pageID = ($.isNumeric(pageAction[pageAction.length-1]))
            ? '/'+pageAction[pageAction.length-1]
            : '';
        $.ajax({
            url:    '/backend/luckyweb/customcontent/pages/get_components'+pageID,
            type:   'GET',
            error:  function(jqXHR){
                console.error(jqXHR.responseText)
            },
            success:function(response){
                if(response.message == 'success'){
                    var componentBody = '';
                    for(var component in response.components){
                        componentBody +=
                        '<div class="layout-row">'+
                            '<div class="layout-cell" data-control="dragcomponent" data-component="">'+
                                '<div class="layout-relative">'+
                                    '<span class="name">'+response.components[component].details.name+'</span>'+
                                    '<span class="description">'+response.components[component].details.description+'</span>'+
                                    '<span class="alias oc-icon-code">'+component+'</span>'+
                                    '<input name="component_properties[]" data-inspector-values="" value="{&quot;oc.alias&quot;:&quot;--alias--&quot;}" type="hidden">'+
                                    '<input data-inspector-config="" value="[{&quot;property&quot;:&quot;oc.alias&quot;,&quot;title&quot;:&quot;\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c&quot;,&quot;description&quot;:&quot;\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c \u043a\u043e\u043c\u043f\u043e\u043d\u0435\u043d\u0442\u0430 \u043e\u043f\u0440\u0435\u0434\u0435\u043b\u044f\u0435\u0442 \u0435\u0433\u043e \u0438\u043c\u044f, \u043f\u043e\u0434 \u043a\u043e\u0442\u043e\u0440\u044b\u043c \u043e\u043d \u0434\u043e\u0441\u0442\u0443\u043f\u0435\u043d \u0432 \u043a\u043e\u0434\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u0438\u043b\u0438 \u0448\u0430\u0431\u043b\u043e\u043d\u0430.&quot;,&quot;type&quot;:&quot;string&quot;,&quot;validationPattern&quot;:&quot;^[a-zA-Z]+[0-9a-z\\_]*$&quot;,&quot;validationMessage&quot;:&quot;\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c\u044b \u043e\u0431\u044f\u0437\u0430\u0442\u0435\u043b\u044c\u043d\u044b \u0438 \u043c\u043e\u0433\u0443\u0442 \u0441\u043e\u0434\u0435\u0440\u0436\u0430\u0442\u044c \u0442\u043e\u043b\u044c\u043a\u043e \u043b\u0430\u0442\u0438\u043d\u0441\u043a\u0438\u0435 \u0431\u0443\u043a\u0432\u044b, \u0446\u0438\u0444\u0440\u044b \u0438 \u0437\u043d\u0430\u043a\u0438 \u043f\u043e\u0434\u0447\u0435\u0440\u043a\u0438\u0432\u0430\u043d\u0438\u044f. \u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c\u044b \u0434\u043e\u043b\u0436\u043d\u044b \u043d\u0430\u0447\u0438\u043d\u0430\u0442\u044c\u0441\u044f \u0441 \u043b\u0430\u0442\u0438\u043d\u0441\u043a\u043e\u0439 \u0431\u0443\u043a\u0432\u044b.&quot;,&quot;required&quot;:true,&quot;showExternalParam&quot;:false}]" type="hidden">'+
                                    '<input data-inspector-class="" value="'+response.components[component].model+'" type="hidden">'+
                                    '<input data-component-icon="" value="oc-icon-leaf" type="hidden">'+
                                    '<input data-component-default-alias="" value="'+component+'" type="hidden">'+
                                    '<input data-component-name="" value="'+component+'" type="hidden">'+
                                    '<input name="component_names[]" value="" type="hidden">'+
                                    '<input name="component_aliases[]" value="" type="hidden">'+
                                    '<a href="#" class="remove">×</a>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
                    }
                    if(response.content.length > 0){
                        for(var i in response.content){
                            buildComponentView(response.content[i]);
                            colorPickerInitialize();
                        }
                    }
                }

                $('.custom-content-layout-wrap .components .layout').empty().append(componentBody);
            }
        });
    }


    //Add component
    $('.custom-content-layout-wrap .layout').on('dblclick', '.layout-cell[data-control=dragcomponent]', function(){
        var component = $(this).find('span.alias').text();
        var name = $(this).find('span.name').text();

        $.ajax({
            url:    '/backend/luckyweb/customcontent/pages/component_template/'+component,
            type:   'GET',
            error:  function(jqXHR){
                console.error(jqXHR.responseText)
            },
            success:function(response){
                console.log(response);
                if(response.message == 'success'){
                    $('.layout-row[data-rel=page-content] .components-details-wrap .page-content-wrap').append(
                        '<li class="page-components-list-wrap" data-component="'+component+'">'+
                            '<div class="page-position-controls">' +
                                '<i class="icon-sort-asc" data-direction="up"></i>' +
                                '<i class="icon-sort-desc" data-direction="down"></i>' +
                            '</div>'+
                            '<div class="page-component-wrap">'+
                                '<div class="page-content-title">' +
                                    name+
                                    '<ul>' +
                                        '<li data-action="drop">Удалить</li>' +
                                        /*'<li>|</li>' +
                                        '<li data-action="drop">Удалить</li>' +*/
                                    '</ul>'+
                                '</div>'+
                                response.template+
                            '</div>'+
                        '</li>'
                    );
                    $('.layout-row[data-rel=page-content] .components-details-wrap .page-content-wrap')
                        .find('.page-components-list-wrap:last input[type=checkbox]:checked').prop('checked', true);
                    colorPickerInitialize();
                    dynamicCKEreplace();

                    showHideMarkupInComponentsInner();

                }
                $('.layout-row[data-rel=page-content] .components-details-wrap .page-content-wrap')
                    .closest('.page-component-subtitle').each(function(){
                        $(this).find('input[name^=videoOrImage]').attr('name','videoOrImage_'+Math.floor(Math.random()* 1000000));
                });
            }
        })
    });

    //Drop component
    $('.page-content-wrap').on('click','li[data-action=drop]', function(){
        var res = confirm('Вы действительно хотите удалить данный компонент?');
        if(res){
            $(this).closest('li.page-components-list-wrap').remove();
        }
    });

    //Move component up/down
    $('ul.page-content-wrap').on('click','i.icon-sort-asc, i.icon-sort-desc', function(){
        if(typeof $(this).attr('data-direction') != 'undefined'){
            var direction = $(this).attr('data-direction');

            if(direction == 'up'){
                if($(this).closest('.page-components-list-wrap').prev().length >0){
                    var prevElem = $(this).closest('.page-components-list-wrap').prev().clone();
                    $(this).closest('.page-components-list-wrap').prev().remove();
                    $(this).closest('.page-components-list-wrap').after(prevElem);
                }
            }else{
                if($(this).closest('.page-components-list-wrap').next().length >0){
                    var nextElem = $(this).closest('.page-components-list-wrap').next().clone();
                    $(this).closest('.page-components-list-wrap').next().remove();
                    $(this).closest('.page-components-list-wrap').before(nextElem);
                }
            }
        }
    });

    //Save page
    $('form button[name=save]').click(function(e){
        e.preventDefault();
        lockMainSaveButtom(true)

        var uploadImages = searchUploadedImages();
        console.log(uploadImages);
        //Validate title
        var title = $('form input[name="Page[title]"]').val().trim();
        if(title.length < 1){
            validationError('Поле "Заголовок" не заполнено.');
            return false;
        }
        //Validate slug
        var slug = $('form input[name="Page[slug]"]').val().trim();
        if(slug.length < 1){
            validationError('Поле "Ссылка" не заполнено.');
            return false;
        }

        //Save images than save content
        imagesUpload(uploadImages, 0, function(){
            var data = gatherPageData();
            savePageData(data);
        });
    });
    $(document).on('click','.image-preview',function(){
        console.log($(this).index());
    });
    //Save page -> than close panel
    $('form button[name=saveAndExit]').click(function(e){
        e.preventDefault();
        lockMainSaveButtom(true);

        var uploadImages = searchUploadedImages();
        //Validate title
        var title = $('form input[name="Page[title]"]').val().trim();
        if(title.length < 1){
            validationError('Поле "Заголовок" не заполнено.');
            return false;
        }
        //Validate slug
        var slug = $('form input[name="Page[slug]"]').val().trim();
        if(slug.length < 1){
            validationError('Поле "Ссылка" не заполнено.');
            return false;
        }
        //Save images than save content
        imagesUpload(uploadImages, 0, function(){
            var data = gatherPageData();
            data.leave = '1';
            savePageData(data);
        });
    });

    showMoreInTextBlockComponent();

    showHideMarkupInComponentsInner();

});
function get_BannerWithBackgroundImage(content,id){
    var desktop=content.desktop;
    var mobile = content.mobile;
    var imageTag = (desktop.image.length > 0)
                ? {img: '<img src="'+desktop.image+'" alt="" data-type="file">', display: 'inline-block'}
                : {img: '', display: 'none'};
    var imageTagMobile =(mobile.image.length > 0)
                ? {img: '<img src="'+mobile.image+'" alt="" data-type="file">', display: 'inline-block'}
                : {img: '', display: 'none'};
    var alignChecked = {left: '', center: '', right: ''};
            alignChecked[desktop.textAlign] = 'checked="checked"';
    var alignCheckedMobile = {left: '', center: '', right: ''};
            alignCheckedMobile[mobile.textAlign] = 'checked="checked"';
           
    //Размер картинки 
        //Desctop
        var imgDesctop = {width: 0, height: 0};
        if(typeof desktop.img != "undefined" && typeof desktop.img['height'] != "undefined"){
            imgDesctop['width']=desktop.img['width'];
            imgDesctop['height']=desktop.img['height'];
        }
        //Mobile
        var imgMobile = { width: 0, height: 0 };
        if(typeof mobile.img != "undefined" && typeof mobile.img['height'] != "undefined"){
            imgMobile['width']=mobile.img['width'];
            imgMobile['height']=mobile.img['height'];
        }
    //END Размер картинки
           
    // Центровка Текста
        //desctop
    var alignTextChecked = {left: '', center: '', right: ''};
    if(typeof desktop.text.Align != "undefined"){
        alignTextChecked[desktop.text.Align] = 'checked="checked"';   
    }else{
        alignTextChecked['left'] = 'checked="checked"';   
    }
        //mobile
    var alignTextCheckedMobile = {left: '', center: '', right: ''};
    if(typeof mobile.text.Align != "undefined"){
        alignTextCheckedMobile[mobile.text.Align] = 'checked="checked"';   
    }else{
        alignTextCheckedMobile['left'] = 'checked="checked"';   
    }
    //END центровка Текста
    
    //Отступы Текста
        //desctop
        var textLeftPaddingDesctop=0;
        if(typeof desktop.text.LeftPadding != "undefined"){
            textLeftPaddingDesctop=desktop.text.LeftPadding
        }
         var textRightPaddingDesctop=0;
        if(typeof desktop.text.RightPadding != "undefined"){
            textRightPaddingDesctop=desktop.text.RightPadding
        }
        //mobile
        var textLeftPaddingMobile=0;
        if(typeof mobile.text.LeftPadding != "undefined"){
            textLeftPaddingMobile=mobile.text.LeftPadding
        }
         var textRightPaddingMobile=0;
        if(typeof mobile.text.RightPadding != "undefined"){
            textRightPaddingMobile=mobile.text.RightPadding
        }
    //END Отступы Текста
    
    
            
    // Центровка заголовка
        //desctop
    var alignTitleChecked = {left: '', center: '', right: ''};
    if(typeof desktop.title.Align != "undefined"){
        alignTitleChecked[desktop.title.Align] = 'checked="checked"';   
    }else{
        alignTitleChecked['left'] = 'checked="checked"';   
    }
        //mobile
    var alignTitleCheckedMobile = {left: '', center: '', right: ''};
    if(typeof mobile.title.Align != "undefined"){
        alignTitleCheckedMobile[mobile.title.Align] = 'checked="checked"';   
    }else{
        alignTitleCheckedMobile['left'] = 'checked="checked"';   
    }
    //END центровка заголовка
    
    // Button
        //Desctop
        
        var DesctopButtonLeftPadding = 0;
        if(typeof desktop.button.buttonLeftPadding != "undefined"){
            DesctopButtonLeftPadding = desktop.button.buttonLeftPadding;
        }
        var DesctopButtonRightPadding = 0;
        if(typeof desktop.button.buttonRightPadding != "undefined"){
            DesctopButtonRightPadding = desktop.button.buttonRightPadding;
        }
    
        //Mobile
        var MobileButtonLeftPadding = 0;
        if(typeof mobile.button.buttonLeftPadding != "undefined"){
            MobileButtonLeftPadding = mobile.button.buttonLeftPadding;
        }
        var MobileButtonRightPadding = 0;
        if(typeof mobile.button.buttonRightPadding != "undefined"){
            MobileButtonRightPadding = mobile.button.buttonRightPadding;
        }
        
    //END Button
    
    //Отступы заголовка
        //desctop
        var titleLeftPaddingDesctop=0;
        if(typeof desktop.title.LeftPadding != "undefined"){
            titleLeftPaddingDesctop=desktop.title.LeftPadding
        }
         var titleRightPaddingDesctop=0;
        if(typeof desktop.title.RightPadding != "undefined"){
            titleRightPaddingDesctop=desktop.title.RightPadding
        }
        //mobile
        var titleLeftPaddingMobile=0;
        if(typeof mobile.title.LeftPadding != "undefined"){
            titleLeftPaddingMobile=mobile.title.LeftPadding
        }
         var titleRightPaddingMobile=0;
        if(typeof mobile.title.RightPadding != "undefined"){
            titleRightPaddingMobile=mobile.title.RightPadding
        }
    //END Отступы заголовка
            
    var desctop_title_size=14;
    var mobile_title_size=14;
    if(typeof desktop.title.size!='undefined'){
        desctop_title_size=desktop.title.size;
    }
    if(typeof mobile.title.size!='undefined'){
        mobile_title_size=mobile.title.size;
    }
    
    var result='<div class="page-component-tabs">'+
    '<div class="page-component-tabs-head">'+
        '<div class="tabs-head-elem active">Настольная версия</div>'+
        '<div class="tabs-head-elem">Мобильная версия</div>'+
    '</div>'+
    '<div class="page-component-tabs-holder">'+
        '<div class="page-component-tabs-item active" data-device="desktop">'+
            '<!-- desctop -->'+
            '<div class="page-component-box" data-type="title">'+
                '<p class="page-component-subtitle">Заглавие</p>'+
                '<div class="label">'+
                   ' <div class="page-flex-wrap">'+
                        '<div class="pseudo-selector">'+
                            '<select name="captionTagFake" class="pseudo-select-tag" placeholder="Тег&hellip;">'+
                                '<option value="p">&lt;p&gt;</option>'+
                                '<option value="h1">&lt;h1&gt;</option>'+
                                '<option value="h2">&lt;h2&gt;</option>'+
                                '<option value="h3">&lt;h3&gt;</option>'+
                                '<option value="h4">&lt;h4&gt;</option>'+
                                '<option value="h5">&lt;h5&gt;</option>'+
                                '<option value="h6">&lt;h6&gt;</option>'+
                                '<!--<option value="span">&lt;span&gt;</option>'+
                                '<option value="strong">&lt;strong&gt;</option>-->'+
                            '</select>'+
                            '<input name="captionTag" class="pseudo-input-tag" type="text" placeholder="Тег&hellip;" style="width: 85px;" value="'+desktop.title.tag+'">'+
                        '</div>'+

                        '<input name="title" class="input-text" type="text" placeholder="Текст заглавия&hellip;" value="'+desktop.title.val+'">'+
                        '<input name="titleColor" class="color-picker" type="text" value="rgb('+desktop.title.color+')">'+
                        '<span>Текст и цвет заглавия</span>'+
                    '</div>'+
                    '<input name="titleSize" class="input-text" type="number" style="min-width: 40px; width: 40px; padding: 8px 3px;" value="'+desctop_title_size+'">'+
                    '<span>Размер шрифта</span>'+
                '</div>'+
                '<div>'+
                    '<p class="page-component-subtitle">Выравнивание</p>'+
                    '<label>'+
                        '<input type="radio" name="titleAlign_'+id+'" value="left" '+alignTitleChecked['left']+'>'+
                        '<span>Слева</span>'+
                    '</label>'+
                    '<label>'+
                        '<input type="radio" name="titleAlign_'+id+'" value="center" '+alignTitleChecked['center']+'>'+
                        '<span>По центру</span>'+
                    '</label>'+
                    '<label>'+
                        '<input type="radio" name="titleAlign_'+id+'" value="right" '+alignTitleChecked['right']+'>'+
                        '<span>Справа</span>'+
                    '</label>'+
                '</div>'+
                '<div>'+
                    '<input name="titleLeftPadding" class="input-text" type="number" style="min-width: 40px; width: 40px; padding: 8px 3px;" value="'+titleLeftPaddingDesctop+'">'+
                    '<span>Отступ слева</span>'+
                    '<input name="titleRightPadding" class="input-text" type="number" style="min-width: 40px; width: 40px; padding: 8px 3px;" value="'+titleRightPaddingDesctop+'">'+
                    '<span>Отступ справа</span>'+
                '</div>'+
            '</div>'+

            '<div class="page-component-box" data-type="text">'+
                '<hr>'+
                '<p class="page-component-subtitle">Текст</p>'+
                '<textarea class="textform needCKE" name="ckeditor_'+id+'">'+desktop.text.val+'</textarea>'+
                '<label>'+
                    '<input type="text" name="titleColor" class="color-picker" value="rgb('+desktop.text.color+')">'+
                    '<span>Цвет текста</span>'+
                '</label>'+
                '<div>'+
                    '<p class="page-component-subtitle">Выравнивание</p>'+
                    '<label>'+
                        '<input type="radio" name="contentAlign_'+id+'" value="left" '+alignTextChecked['left']+'>'+
                        '<span>Слева</span>'+
                    '</label>'+
                    '<label>'+
                        '<input type="radio" name="contentAlign_'+id+'" value="center" '+alignTextChecked['center']+'>'+
                        '<span>По центру</span>'+
                    '</label>'+
                    '<label>'+
                        '<input type="radio" name="contentAlign_'+id+'" value="right" '+alignTextChecked['right']+'>'+
                        '<span>Справа</span>'+
                    '</label>'+
                '</div>'+
                '<div>'+
                    '<input name="contentLeftPadding" class="input-text" type="number" style="min-width: 40px; width: 40px; padding: 8px 3px;" value="'+textLeftPaddingDesctop+'">'+
                    '<span>Отступ слева</span>'+
                    '<input name="contentRightPadding" class="input-text" type="number" style="min-width: 40px; width: 40px; padding: 8px 3px;" value="'+textRightPaddingDesctop+'">'+
                    '<span>Отступ справа</span>'+
                '</div>'+
                '<hr>'+
            '</div>'+
            '<div class="page-component-box" data-type="typeSelector">'+
                '<p class="page-component-subtitle">Выбор типа кнопки</p>';
        if(typeof desktop.button.textTypeButton!='undefined' && typeof desktop.button.type_selector_text!='undefined' ){
            var type_selector_text = {text: '', href: ''};
            type_selector_text[desktop.button.type_selector_text] = 'checked="checked"';
            
            result+='<label>'+
                    '<input type="radio" class="type_selector" name="type_selector_'+id+'" value="href" '+type_selector_text.href+'>'+
                    '<span>Ссылка</span>'+
                '</label>'+
                '<label>'+
                    '<input type="radio" class="type_selector" name="type_selector_'+id+'" value="text" '+type_selector_text.text+'>'+
                    '<span>Текст</span>'+
                '</label>';
    }else{
            result+='<label>'+
                    '<input type="radio" class="type_selector" name="type_selector_'+id+'" value="href" checked="checked">'+
                    '<span>Ссылка</span>'+
                '</label>'+
                '<label>'+
                    '<input type="radio" class="type_selector" name="type_selector_'+id+'" value="text">'+
                    '<span>Текст</span>'+
                '</label>';
    }
                if(typeof desktop.button.textTypeButton !='undefined' && typeof desktop.button.type_selector_text!='undefined'){
                    if(desktop.button.type_selector_text!='text'){
                        result+='<div class="txt_btn_container" style="display:none;">'+
                                '<p class="page-component-subtitle">Текст подробнее для кнопки</p>';
                    }else{
                        result+='<div class="txt_btn_container">'+
                                '<p class="page-component-subtitle">Текст подробнее для кнопки</p>';
                    }
                }else{
                    result+='<div class="txt_btn_container" style="display:none;">'+
                    '<p class="page-component-subtitle">Текст подробнее для кнопки</p>';
                }
                if(typeof desktop.button.textTypeButton !='undefined' && typeof desktop.button.type_selector_text!='undefined'){
                    result+='<textarea class="textform needCKE" style="dispaly:none;" name="btn_text_'+id+'">'+desktop.button.textTypeButton.val+'</textarea>';
                }else{
                    result+='<textarea class="textform needCKE" style="dispaly:none;" name="btn_text_'+id+'"></textarea>';
                }
                result+='</div>'+
            '</div>'+
            '<div class="page-component-box" data-type="button">'+
                '<p class="page-component-subtitle">Кнопка</p>'+
                '<label>'+
                    '<input name="buttonCaption" class="input-text" type="text" placeholder="Текст кнопки&hellip;" value="'+desktop.button.val+'">'+
                    '<input type="text" name="titleColor" class="color-picker" value="rgb('+desktop.button.color+')">'+
                    '<span>Текст и его цвет</span>'+
                '</label>'+
                '<div>'+
                    '<input name="buttonLeftPadding" class="input-text" type="number" style="min-width: 40px; width: 40px; padding: 8px 3px;" value="'+DesctopButtonLeftPadding+'">'+
                    '<span>Отступ слева</span>'+
                    '<input name="buttonRightPadding" class="input-text" type="number" style="min-width: 40px; width: 40px; padding: 8px 3px;" value="'+DesctopButtonRightPadding+'">'+
                    '<span>Отступ справа</span>'+
                '</div>';
                if(typeof desktop.button.textTypeButton !='undefined' && typeof desktop.button.type_selector_text!='undefined'){
                    if(desktop.button.type_selector_text!='text'){
                            result +=
                                    '<label class="href_input">'+
                                        '<input name="buttonLink" class="input-text" type="text" placeholder="Ссылка&hellip;" value="'+desktop.button.link+'">'+
                                        '<span>Ссылка</span>'+
                                    '</label>';
                        }else{
                            result +=
                                    '<label class="href_input" style="display:none;">'+
                                        '<input name="buttonLink" class="input-text" type="text" placeholder="Ссылка&hellip;" value="'+desktop.button.link+'">'+
                                        '<span>Ссылка</span>'+
                                    '</label>';
                        }
                }else{
                    result+='<label class="href_input" style="display: none;">'+
                        '<input name="buttonLink" class="input-text" type="text" placeholder="Ссылка&hellip;" value="'+desktop.button.link+'">'+
                        '<span>Ссылка</span>'+
                    '</label>';
                }
                
        
                result+='<label>'+
                    '<input type="text" name="buttonColor" class="color-picker" value="rgb('+desktop.button.buttonColor+')">'+
                    '<span>Цвет кнопки</span>'+
                '</label>'+
                '<label>'+
                    '<input type="text" name="hoverColor" class="color-picker" value="rgb('+desktop.button.hoverColor+')">'+
                    '<span>Цвет ховера</span>'+
                '</label>'+
            '</div>'+

            '<div class="page-component-box" data-type="background">'+
                '<p class="page-component-subtitle">Изображение бэкграунда</p>'+
                '<div>'+
                    '<div class="image-preview">'+imageTag.img+'</div>'+
                    '<div class="image-control">'+
                        '<input name="backgroundImage" type="file" style="display: none;">'+
                        '<button class="btn btn-primary" type="button" name="fakeOverview">Обзор&hellip;</button>'+
                        '<button class="btn btn-primary" type="button" name="imageClear" style="display: '+imageTag.display+';">Очистить&hellip;</button>'+
                    '</div>'+
               ' </div>'+
               '<p class="page-component-subtitle">Размеры картинки</p>'+
                '<div>'+
                    '<input name="imgWidth" class="input-text" type="number" style="min-width: 80px; width: 80px; padding: 8px 3px;" value="'+imgDesctop['width']+'">'+
                    '<span>Ширина</span>'+
                    '<input name="imgHeight" class="input-text" type="number" style="min-width: 80px; width: 80px; padding: 8px 3px;" value="'+imgDesctop['height']+'">'+
                    '<span>Высота</span>'+
                '</div>'+
           ' </div>'+

            '<div class="page-component-box" data-type="textAlign">'+
                '<p class="page-component-subtitle">Размещение блока относительно бэкграунда</p>'+
                '<label>'+
                    '<input type="radio" name="textAlign_'+id+'" value="left" '+alignChecked.left+'>'+
                    '<span>Слева</span>'+
                '</label>'+
                '<label>'+
                   ' <input type="radio" name="textAlign_'+id+'" value="center" '+alignChecked.center+'>'+
                    '<span>По центру</span>'+
                '</label>'+
                '<label>'+
                    '<input type="radio" name="textAlign_'+id+'" value="right" '+alignChecked.right+'>'+
                    '<span>Справа</span>'+
                '</label>'+
            '</div>'+

            '<div class="page-component-box" data-type="marginBottom">'+
                '<p class="page-component-subtitle">Отступ снизу</p>'+
                '<label>'+
                    '<input class="input-text" type="number" name="marginBottom" value="'+desktop.marginBottom+'" step="1" style="min-width: 100px;">'+
                    '<span>Отступ в пикселях</span>'+
                '</label>'+
            '</div>'+
            '<div class="page-component-box" data-type="marginTop">'+
                '<p class="page-component-subtitle">Отступ сверху</p>'+
                '<label>'+
                   ' <input class="input-text" type="number" name="marginTop" value="'+desktop.marginTop+'" step="1" style="min-width: 100px;">'+
                   ' <span>Отступ в пикселях</span>'+
                '</label>'+
            '</div>'+
            '<div class="page-component-box" data-type="anchor">'+
                '<p class="page-component-subtitle">Якорь</p>'+
                '<label>'+
                    '<input class="input-text" type="text" name="anchor" value="'+desktop.anchor+'" step="1" style="min-width: 100px;">'+
               ' </label>'+
           ' </div>'+
        '</div>'+
        '<div class="page-component-tabs-item" data-device="mobile">'+
            '<!-- mobile -->'+
            '<div class="page-component-box" data-type="title">'+
                '<p class="page-component-subtitle">Заглавие</p>'+
                '<div class="label">'+
                   ' <div class="page-flex-wrap">'+
                        '<div class="pseudo-selector">'+
                            '<select name="captionTagFake" class="pseudo-select-tag" placeholder="Тег&hellip;">'+
                                '<option value="p">&lt;p&gt;</option>'+
                                '<option value="h1">&lt;h1&gt;</option>'+
                                '<option value="h2">&lt;h2&gt;</option>'+
                                '<option value="h3">&lt;h3&gt;</option>'+
                                '<option value="h4">&lt;h4&gt;</option>'+
                                '<option value="h5">&lt;h5&gt;</option>'+
                                '<option value="h6">&lt;h6&gt;</option>'+
                                '<!--<option value="span">&lt;span&gt;</option>'+
                                '<option value="strong">&lt;strong&gt;</option>-->'+
                            '</select>'+
                            '<input name="captionTag" class="pseudo-input-tag" type="text" placeholder="Тег&hellip;" style="width: 85px;" value="'+mobile.title.tag+'">'+
                        '</div>'+

                        '<input name="title" class="input-text" type="text" placeholder="Текст заглавия&hellip;" value="'+mobile.title.val+'">'+
                        '<input name="titleColor" class="color-picker" type="text" value="rgb('+mobile.title.color+')">'+
                        '<span>Текст и цвет заглавия</span>'+
                    '</div>'+
                    '<input name="titleSize" class="input-text" type="number" style="min-width: 40px; width: 40px; padding: 8px 3px;" value="'+mobile_title_size+'">'+
                    '<span>Размер шрифта</span>'+
                '</div>'+
                '<div>'+
                    '<p class="page-component-subtitle">Выравнивание</p>'+
                    '<label>'+
                        '<input type="radio" name="titleAlignMobile_'+id+'" value="left" '+alignTitleCheckedMobile['left']+'>'+
                        '<span>Слева</span>'+
                    '</label>'+
                    '<label>'+
                        '<input type="radio" name="titleAlignMobile_'+id+'" value="center" '+alignTitleCheckedMobile['center']+'>'+
                        '<span>По центру</span>'+
                    '</label>'+
                    '<label>'+
                        '<input type="radio" name="titleAlignMobile_'+id+'" value="right" '+alignTitleCheckedMobile['right']+'>'+
                        '<span>Справа</span>'+
                    '</label>'+
                '</div>'+
                '<div>'+
                    '<input name="titleLeftPadding" class="input-text" type="number" style="min-width: 40px; width: 40px; padding: 8px 3px;" value="'+titleLeftPaddingMobile+'">'+
                    '<span>Отступ слева</span>'+
                    '<input name="titleRightPadding" class="input-text" type="number" style="min-width: 40px; width: 40px; padding: 8px 3px;" value="'+titleRightPaddingMobile+'">'+
                    '<span>Отступ справа</span>'+
                '</div>'+
            '</div>'+

            '<div class="page-component-box" data-type="text">'+
                '<hr>'+
                '<p class="page-component-subtitle">Текст</p>'+
                '<textarea class="textform needCKE" name="ckeditorMobile_'+id+'">'+mobile.text.val+'</textarea>'+
                '<label>'+
                    '<input type="text" name="titleColor" class="color-picker" value="rgb('+mobile.text.color+')">'+
                    '<span>Цвет текста</span>'+
                '</label>'+
                '<div>'+
                    '<p class="page-component-subtitle">Выравнивание</p>'+
                    '<label>'+
                        '<input type="radio" name="contentAlignMobile_'+id+'" value="left" '+alignTextCheckedMobile['left']+'>'+
                        '<span>Слева</span>'+
                    '</label>'+
                    '<label>'+
                        '<input type="radio" name="contentAlignMobile_'+id+'" value="center" '+alignTextCheckedMobile['center']+'>'+
                        '<span>По центру</span>'+
                    '</label>'+
                    '<label>'+
                        '<input type="radio" name="contentAlignMobile_'+id+'" value="right" '+alignTextCheckedMobile['right']+'>'+
                        '<span>Справа</span>'+
                    '</label>'+
                '</div>'+
                '<div>'+
                    '<input name="contentLeftPadding" class="input-text" type="number" style="min-width: 40px; width: 40px; padding: 8px 3px;" value="'+textLeftPaddingMobile+'">'+
                    '<span>Отступ слева</span>'+
                    '<input name="contentRightPadding" class="input-text" type="number" style="min-width: 40px; width: 40px; padding: 8px 3px;" value="'+textRightPaddingMobile+'">'+
                    '<span>Отступ справа</span>'+
                '</div>'+
                '<hr>'+
            '</div>'+
            '<div class="page-component-box" data-type="typeSelector">'+
                '<p class="page-component-subtitle">Выбор типа кнопки</p>';
        if(typeof mobile.button.textTypeButton!='undefined' && typeof mobile.button.type_selector_text!='undefined' ){
            var type_selector_text = {text: '', href: ''};
            type_selector_text[mobile.button.type_selector_text] = 'checked="checked"';
            
            result+='<label>'+
                    '<input type="radio" class="type_selector" name="type_selectorMobile_'+id+'" value="href" '+type_selector_text.href+'>'+
                    '<span>Ссылка</span>'+
                '</label>'+
                '<label>'+
                    '<input type="radio" class="type_selector" name="type_selectorMobile_'+id+'" value="text" '+type_selector_text.text+'>'+
                    '<span>Текст</span>'+
                '</label>';
    }else{
            result+='<label>'+
                    '<input type="radio" class="type_selector" name="type_selectorMobile_'+id+'" value="href" checked="checked">'+
                    '<span>Ссылка</span>'+
                '</label>'+
                '<label>'+
                    '<input type="radio" class="type_selector" name="type_selectorMobile_'+id+'" value="text" >'+
                    '<span>Текст</span>'+
                '</label>';
    }
                if(typeof mobile.button.textTypeButton !='undefined' && typeof mobile.button.type_selector_text!='undefined'){
                    if(mobile.button.type_selector_text!='text'){
                        result+='<div class="txt_btn_container" style="display:none;">'+
                                '<p class="page-component-subtitle">Текст подробнее для кнопки</p>';
                    }else{
                        result+='<div class="txt_btn_container">'+
                                '<p class="page-component-subtitle">Текст подробнее для кнопки</p>';
                    }
                }else{
                    result+='<div class="txt_btn_container" style="display:none;">'+
                    '<p class="page-component-subtitle">Текст подробнее для кнопки</p>';
                }
                if(typeof mobile.button.textTypeButton !='undefined' && typeof mobile.button.type_selector_text!='undefined'){
                    result+='<textarea class="textform needCKE" style="dispaly:none;" name="btn_textMobile_'+id+'">'+mobile.button.textTypeButton.val+'</textarea>';
                }else{
                    result+='<textarea class="textform needCKE" style="dispaly:none;" name="btn_textMobile_'+id+'"></textarea>';
                }
                result+='</div>'+
            '</div>'+
            '<div class="page-component-box" data-type="button">'+
                '<p class="page-component-subtitle">Кнопка</p>'+
                '<label>'+
                    '<input name="buttonCaption" class="input-text" type="text" placeholder="Текст кнопки&hellip;" value="'+mobile.button.val+'">'+
                    '<input type="text" name="titleColor" class="color-picker" value="rgb('+mobile.button.color+')">'+
                    '<span>Текст и его цвет</span>'+
                '</label>'+
                '<div>'+
                    '<input name="buttonLeftPadding" class="input-text" type="number" style="min-width: 40px; width: 40px; padding: 8px 3px;" value="'+MobileButtonLeftPadding+'">'+
                    '<span>Отступ слева</span>'+
                    '<input name="buttonRightPadding" class="input-text" type="number" style="min-width: 40px; width: 40px; padding: 8px 3px;" value="'+MobileButtonRightPadding+'">'+
                    '<span>Отступ справа</span>'+
                '</div>';

                if(typeof mobile.button.textTypeButton !='undefined' && typeof mobile.button.type_selector_text!='undefined'){
                    if(mobile.button.type_selector_text!='text'){
                            result +=
                                    '<label class="href_input">'+
                                        '<input name="buttonLink" class="input-text" type="text" placeholder="Ссылка&hellip;" value="'+mobile.button.link+'">'+
                                        '<span>Ссылка</span>'+
                                    '</label>';
                        }else{
                            result +=
                                    '<label class="href_input" style="display:none;">'+
                                        '<input name="buttonLink" class="input-text" type="text" placeholder="Ссылка&hellip;" value="'+mobile.button.link+'">'+
                                        '<span>Ссылка</span>'+
                                    '</label>';
                        }
                }else{
                    result+='<label class="href_input" style="display: none;">'+
                        '<input name="buttonLink" class="input-text" type="text" placeholder="Ссылка&hellip;" value="'+mobile.button.link+'">'+
                        '<span>Ссылка</span>'+
                    '</label>';
                }
                
        
                result+='<label>'+
                    '<input type="text" name="buttonColor" class="color-picker" value="rgb('+mobile.button.buttonColor+')">'+
                    '<span>Цвет кнопки</span>'+
                '</label>'+
                '<label>'+
                    '<input type="text" name="hoverColor" class="color-picker" value="rgb('+mobile.button.hoverColor+')">'+
                    '<span>Цвет ховера</span>'+
                '</label>'+
            '</div>'+

            '<div class="page-component-box" data-type="background">'+
                '<p class="page-component-subtitle">Изображение бэкграунда</p>'+
                '<div>'+
                    '<div class="image-preview">'+imageTagMobile.img+'</div>'+
                    '<div class="image-control">'+
                        '<input name="backgroundImage" type="file" style="display: none;">'+
                        '<button class="btn btn-primary" type="button" name="fakeOverview">Обзор&hellip;</button>'+
                        '<button class="btn btn-primary" type="button" name="imageClear" style="display: '+imageTagMobile.display+';">Очистить&hellip;</button>'+
                    '</div>'+
               ' </div>'+
               '<p class="page-component-subtitle">Размеры картинки</p>'+
                '<div>'+
                    '<input name="imgWidth" class="input-text" type="number" style="min-width: 80px; width: 80px; padding: 8px 3px;" value="'+imgMobile['width']+'">'+
                    '<span>Ширина</span>'+
                    '<input name="imgHeight" class="input-text" type="number" style="min-width: 80px; width: 80px; padding: 8px 3px;" value="'+imgMobile['height']+'">'+
                    '<span>Высота</span>'+
                '</div>'+
           ' </div>'+

            '<div class="page-component-box" data-type="textAlign">'+
                '<p class="page-component-subtitle">Размещение блока относительно бэкграунда</p>'+
                '<label>'+
                    '<input type="radio" name="textAlignMobile_'+id+'" value="left" '+alignCheckedMobile.left+'>'+
                    '<span>Слева</span>'+
                '</label>'+
                '<label>'+
                   ' <input type="radio" name="textAlignMobile_'+id+'" value="center" '+alignCheckedMobile.center+'>'+
                    '<span>По центру</span>'+
                '</label>'+
                '<label>'+
                    '<input type="radio" name="textAlignMobile_'+id+'" value="right" '+alignCheckedMobile.right+'>'+
                    '<span>Справа</span>'+
                '</label>'+
            '</div>'+

            '<div class="page-component-box" data-type="marginBottom">'+
                '<p class="page-component-subtitle">Отступ снизу</p>'+
                '<label>'+
                    '<input class="input-text" type="number" name="marginBottom" value="'+mobile.marginBottom+'" step="1" style="min-width: 100px;">'+
                    '<span>Отступ в пикселях</span>'+
                '</label>'+
            '</div>'+
            '<div class="page-component-box" data-type="marginTop">'+
                '<p class="page-component-subtitle">Отступ сверху</p>'+
                '<label>'+
                   ' <input class="input-text" type="number" name="marginTop" value="'+mobile.marginTop+'" step="1" style="min-width: 100px;">'+
                   ' <span>Отступ в пикселях</span>'+
                '</label>'+
            '</div>'+
            '<div class="page-component-box" data-type="anchor">'+
                '<p class="page-component-subtitle">Якорь</p>'+
                '<label>'+
                    '<input class="input-text" type="text" name="anchor" value="'+mobile.anchor+'" step="1" style="min-width: 100px;">'+
               ' </label>'+
           ' </div>'+
        '</div>'+
    '</div>'+
'</div>';
    return result;
}
