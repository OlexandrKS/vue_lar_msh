<?php namespace Luckyweb\CustomContent;

use Backend;
use System\Classes\PluginBase;

/**
 * CustomContent Plugin Information File
 */
class Plugin extends PluginBase
{
    public $require = [
        'LuckyWeb.MS'
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Контент Страниц',
            'description' => 'Плагин позовляет создавать страницы и управлять их контентом...',
            'author'      => 'Kirill@sheepfish',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Luckyweb\CustomContent\Components\BannerWithBackgroundImage' => 'CustomContentBannerWithBackgroundImage',
            'Luckyweb\CustomContent\Components\SmallBanner' => 'CustomContentSmallBanner',
            'Luckyweb\CustomContent\Components\FastRequestForm' => 'CustomContentFastRequestForm',
            'Luckyweb\CustomContent\Components\RequestForm' => 'CustomContentRequestForm',
            'Luckyweb\CustomContent\Components\ContentManager' => 'ContentManager',
            'Luckyweb\CustomContent\Components\TextBlock' => 'CustomContentTextBlockView',
            'Luckyweb\CustomContent\Components\GridBuilder' => 'CustomGridBuilder',
            'Luckyweb\CustomContent\Components\ImageGridBuilder' => 'CustomImageGridBuilder',
            'Luckyweb\CustomContent\Components\PageCode' => 'CustomPageCode',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        $tab = 'Управление конструктором сраниц.';
        $order = 0;
        return [
            'luckyweb.customcontent.pages' => ['label' => 'Управление страницами', 'tab' => $tab, 'order'=>$order++],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'customcontent' => [
                'label'       => 'Контент Страниц',
                'icon'        => 'icon-leaf',
                'url'         => Backend::url('luckyweb/customcontent/pages'),
                'permissions' => ['luckyweb.customcontent.pages'],
                'order'       => 6,
                'sideMenu' => [
                    'pagemetas' => [
                        'label'       => 'Контент Страниц',
                        'icon'        => 'icon-leaf',
                        'url'         => Backend::url('luckyweb/customcontent/pages'),
                        'permissions' => ['luckyweb.customcontent.pages'],
                    ],
                ],
            ],
        ];
    }
}
