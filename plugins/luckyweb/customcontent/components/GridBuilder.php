<?php namespace LuckyWeb\CustomContent\Components;

use Cms\Classes\ComponentBase;

class GridBuilder extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Построение сетки',
            'description' => 'Вывод товаров по сеткке'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
