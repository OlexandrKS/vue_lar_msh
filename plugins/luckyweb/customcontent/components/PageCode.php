<?php namespace LuckyWeb\CustomContent\Components;

use Cms\Classes\ComponentBase;
use Luckyweb\CustomContent\Models\Page;
use LuckyWeb\CustomContent\Models\PageContent;

class PageCode extends ComponentBase
{
    public $component_content;
    
    public function componentDetails()
    {
        return [
            'name'        => 'Код HTML',
            'description' => 'Вывод кода HTML текущих компонент'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
    public function onRender(){
        $page_url = '/'.str_replace('.htm','',$this->page->attributes['fileName']);

        $page_data = Page::select('id')->where('slug', '=', $page_url)->first();

        $component = PageContent::select('content')
            ->where('page_id', '=', $page_data->id)
            ->where('component_name', '=', $this->alias)
            ->orderBy('position', 'asc')
            ->first();
        $content = json_decode($component->content, true);
        $content['button']['buttonColor'] = explode(',', $content['button']['buttonColor']);
        $content['button']['hoverColor'] = explode(',', $content['button']['hoverColor']);

        $this->component_content = $content;
    }
}