<?php namespace LuckyWeb\CustomContent\Components;
use Jenssegers\Agent\Facades\Agent;
use Cms\Classes\ComponentBase;
use Luckyweb\Customcontent\Models\Page;
use LuckyWeb\CustomContent\Models\PageContent;
use LuckyWeb\MS\Models\Product;
use Illuminate\Support\Facades\View;
use LuckyWeb\MS\Models\Promotion;
use Backend;

use LuckyWeb\MS\Models\Category;
use LuckyWeb\MS\Models\SubCategory;

class ContentManager extends ComponentBase
{
    /**
     * Current mixed promotion
     * @var \LuckyWeb\MS\Models\Promotion
     */
    public $mixedPromotion;

    public function componentDetails()
    {
        return [
            'name'        => 'ContentManager Component',
            'description' => 'No description provided yet...'
        ];
    }

    public static function isMobile(){
        $useragent=$_SERVER['HTTP_USER_AGENT'];

        if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
            return TRUE;
        }
        return false;
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => 'Page slug',
                'description' => 'If not set - get current pae slug',
                'default'     => '{{ :slug }}',
                'type'        => 'string'
            ]
        ];
    }

    /**
     * @param string $path
     * @return string
     */
    private function imagePathToStorage($path)
    {
        if(preg_match('/\/?storage\/images\/([a-z0-9]+\.[a-z0-9]{2,4})/ui', $path, $match)) {
            $image = array_get($match, 1);

            return \Storage::disk('images')
                ->url($image);
        }

        return $path;
    }

    public function onRender()
    {
        $slug = '/'.trim($this->property('slug', array_get($this->controller->vars,'this.param.slug')), '/');
        $page = Page::where('slug', '=', $slug)->first();

        $component_content = '';
        if(!empty($page)){
            if($this->page->id == 'constructor') {
                $this->controller->vars['this']['page']->settings['title'] = $page->title;
                $this->controller->vars['this']['page']->settings['description'] = $page->description;
                $this->controller->vars['this']['page']->settings['meta_title'] = $page->meta_title;
                $this->controller->vars['this']['page']->settings['meta_description'] = $page->meta_description;
            }

            $page_components = PageContent::select('component_slug','content')
                ->where('page_id', '=', $page->id)
                ->orderBy('position','asc')
                ->get();

            $templates_folder = 'plugins/luckyweb/customcontent/components/';

            foreach($page_components as $component){
                $content = json_decode($component->content);
                /*Default settings*/
                //Title
                $font_size='';
                if(!empty($content->title->size)){
                    $font_size=' font-size:'.$content->title->size.'px; ';
                }
                $component_settings['componentTitle'] = (!empty($content->title->val))
                    ?   '<'.$content->title->tag.' style="color: rgb('.$content->title->color.'); '.$font_size.'">'.
                    $content->title->val.
                    '</'.$content->title->tag.'>'
                    : '';

                //Margin-bottom
                if(isset($content->marginBottom)){
                    $component_settings['marginBottom'] = $content->marginBottom;
                }else{
                    $component_settings['marginBottom']='0';
                }

                if(isset($content->marginTop)){
                    $component_settings['marginTop'] = $content->marginTop;
                }else{
                    $component_settings['marginTop']='0';
                }

                if(isset($content->anchor) && $content->anchor!=''){
                    $component_settings['anchor'] = $content->anchor;
                }else{
                    $component_settings['anchor']= 'uniq_'.uniqid();
                }

                switch($component->component_slug){
                    case 'BannerWithBackgroundImage':
                        $res_content=$content->desktop;
                        if(self::isMobile()){
                            $res_content=$content->mobile;
                        }
                        $component_settings['marginBottom'] = $res_content->marginBottom;
                        $component_settings['marginTop'] = $res_content->marginTop;
                        if($res_content->anchor!=''){
                            $component_settings['anchor'] = $res_content->anchor;
                        }else{
                            $component_settings['anchor']= 'uniq_'.uniqid();
                        }

                        //Wrapper class
                        if($res_content->textAlign == 'center'){
                            $component_settings['mainWrap'] = 'banner-wrap center TextBlock_container';
                            $component_settings['secondaryWrap'] = 'banner-desc close_text_btn_wrap';
                            $component_settings['secondaryWrapStyle'] = '';
                        }elseif($res_content->textAlign=='left'){
                            $component_settings['mainWrap'] = 'banner-wrap TextBlock_container';
                            $component_settings['secondaryWrap'] = 'banner-desc left_wrap close_text_btn_wrap';
                            $component_settings['secondaryWrapStyle'] = $res_content->textAlign;
                        }else{
                            $component_settings['mainWrap'] = 'banner-wrap TextBlock_container';
                            $component_settings['secondaryWrap'] = 'banner-desc close_text_btn_wrap';
                            $component_settings['secondaryWrapStyle'] = $res_content->textAlign;
                        }

                        //Background-image
                        $component_settings['no_img_class']=(isset($res_content->image) && !empty($res_content->image))
                            ? ''
                            : 'no_img';
                        $imageWidth='';
                        if(!empty($res_content->img)){
                            if(intval($res_content->img->width)>0){
                                $imageWidth=' width="'.$res_content->img->width.'" ';
                            }
                        }
                        $imageHeight='';
                        if(!empty($res_content->img)){
                            if(intval($res_content->img->height)>0){
                                $imageHeight=' height="'.$res_content->img->height.'" ';
                            }
                        }
                        $component_settings['backgroundImage'] = (isset($res_content->image) && !empty($res_content->image))
                            ? '<img src="'.$this->imagePathToStorage($res_content->image).'" '.$imageWidth.$imageHeight.' alt="">'
                            : '';


                        //Title
                        $font_size='';
                            if(!empty($res_content->title->size)){
                                $font_size=' font-size:'.$res_content->title->size.'px; ';
                            }
                        $titleAlign='';
                            if(!empty($res_content->title->Align)){
                                $titleAlign='text-align: '.$res_content->title->Align.'; ';
                            }
                        $titlePaddingLeft='';
                            if(!empty($res_content->title->LeftPadding)){
                                $titlePaddingLeft='padding-left: '.$res_content->title->LeftPadding.'px; ';
                            }
                        $titlePaddingRight='';
                            if(!empty($res_content->title->RightPadding)){
                                $titlePaddingRight='padding-right: '.$res_content->title->RightPadding.'px; ';
                            }

                        $component_settings['componentTitle']='<'.$res_content->title->tag.' style="color: rgb('.$res_content->title->color.'); '.$font_size.$titleAlign.$titlePaddingLeft.$titlePaddingRight.'">'.$res_content->title->val.'</'.$res_content->title->tag.'>';

                        //Text
                        $contentAlign='';
                            if(!empty($res_content->text->Align)){
                                $contentAlign='text-align: '.$res_content->text->Align.'; ';
                            }
                        $contentPaddingLeft='';
                            if(!empty($res_content->text->LeftPadding)){
                                $contentPaddingLeft='padding-left: '.$res_content->text->LeftPadding.'px; ';
                            }
                        $contentPaddingRight='';
                            if(!empty($res_content->text->RightPadding)){
                                $contentPaddingRight='padding-right: '.$res_content->text->RightPadding.'px; ';
                            }

                        if($res_content->button->type_selector_text=='href'){

                        $component_settings['componentText'] = (!empty($res_content->text->val))
                            ? '<div style="color: rgb('.$res_content->text->color.'); '.$contentAlign.$contentPaddingLeft.$contentPaddingRight.'">'.$res_content->text->val.'</div>'
                            : '';
                        }else{
                            $component_settings['componentText'] = (!empty($res_content->text->val))
                            ? '<div style="color: rgb('.$res_content->text->color.'); '.$contentAlign.$contentPaddingLeft.$contentPaddingRight.'">'.$res_content->text->val.
                                    '<div class="more_text text_block_container_hidden_text" style="'.$contentAlign.$contentPaddingLeft.$contentPaddingRight.'">'.
                                        $res_content->button->textTypeButton->val.
                                    '</div>'.
                                '</div>'
                            : '';
                        }
                        //Button
                        $leftMargin='';
                        if(isset($res_content->button->buttonLeftPadding)){
                            if($res_content->button->buttonLeftPadding != 0){
                                $leftMargin='margin-left:'.$res_content->button->buttonLeftPadding.'px;';
                            }
                        }
                        $rightMargin='';
                        if(isset($res_content->button->buttonRightPadding)){
                            if($res_content->button->buttonRightPadding != 0){
                                $rightMargin='margin-right:'.$res_content->button->buttonRightPadding.'px;';
                            }
                        }
                        if($res_content->button->type_selector_text=='href'){
                            $component_settings['componentButton'] = (!empty($res_content->button->val) && !empty($res_content->button->link))
                            ?   '<a class="el-button" href="'.$res_content->button->link.'" '.
                                        'style="background-color: rgb('.$res_content->button->buttonColor.'); '.
                                        $leftMargin.$rightMargin.
                                        'box-shadow: 0px 8px 12.48px 0.52px rgba('.$res_content->button->buttonColor.',0.35)">'.
                                    '<span style="z-index: 999; color: rgb('.$res_content->button->color.');">'.
                                        $res_content->button->val.
                                    '</span>'.
                                    '<span class="bg"'.
                                        'style="background-color: rgb('.$res_content->button->hoverColor.'); '.
                                        'box-shadow: 0px 8px 12.48px 0.52px rgba('.$res_content->button->hoverColor.', 0.35)">'.
                                    '</span>'.
                                '</a>'
                            : '';
                        }else{
                            $component_settings['componentButton'] = (!empty($res_content->button->val))
                            ?   '<a class="el-button more_type close_text_btn close_text_only" href="#more" '.
                                        'style="background-color: rgb('.$res_content->button->buttonColor.'); '.
                                        $leftMargin.$rightMargin.
                                        'box-shadow: 0px 8px 12.48px 0.52px rgba('.$res_content->button->buttonColor.',0.35)">'.
                                    '<span style="z-index: 999; color: rgb('.$res_content->button->color.');">'.
                                        $res_content->button->val.
                                    '</span>'.
                                    '<span class="bg"'.
                                        'style="background-color: rgb('.$res_content->button->hoverColor.'); '.
                                        'box-shadow: 0px 8px 12.48px 0.52px rgba('.$res_content->button->hoverColor.', 0.35)">'.
                                    '</span>'.
                                '</a>'
                            : '';
                        }
                        break;

                    case 'SmallBanner':

                        $font_size='';
                        if(!empty($content->title->size)){
                            $font_size=' font-size:'.$content->title->size.'px; ';
                        }
                        $titleAlign='';
                            if(!empty($content->title->Align)){
                                $titleAlign='text-align: '.$content->title->Align.'; ';
                            }
                        $titlePaddingLeft='';
                            if(!empty($content->title->LeftPadding)){
                                $titlePaddingLeft='padding-left: '.$content->title->LeftPadding.'px; ';
                            }
                        $titlePaddingRight='';
                            if(!empty($content->title->RightPadding)){
                                $titlePaddingRight='padding-right: '.$content->title->RightPadding.'px; ';
                            }
                        $component_settings['componentTitle'] = (!empty($content->title->val))
                            ?   '<'.$content->title->tag.' style="color: rgb('.$content->title->color.'); '.$font_size.$titleAlign.$titlePaddingLeft.$titlePaddingRight.' ">'.
                            $content->title->val.
                            '</'.$content->title->tag.'>'
                            : '';


                        //Wrappers float
                        switch($content->textAlign){
                            case 'left':
                                $component_settings['wrapper_class']= 'align_left';
                                $component_settings['productFloat'] = '';
                                $component_settings['videoOrImageFloat'] = '';
                                $component_settings['videoOrImageWrap'] = '';
                                break;

                            case 'right':
                                $component_settings['wrapper_class']= 'align_right';
                                $component_settings['productFloat'] = '';
                                $component_settings['videoOrImageFloat'] = '';
                                $component_settings['videoOrImageWrap'] = 'align_right';
                                break;
                            case 'center':
                                $component_settings['wrapper_class']= 'align_center';
                                $component_settings['productFloat'] = '';
                                $component_settings['videoOrImageFloat'] = '';
                                $component_settings['videoOrImageWrap'] = '';
                                break;
                            default:
                                $component_settings['wrapper_class']= 'align_center';
                                $component_settings['productFloat'] = '';
                                $component_settings['videoOrImageWrap'] = 'col-md-12 col-sm-6 col-12 cover-img';
                                $component_settings['videoOrImageFloat'] = '';
                        }
                        if($content->show_text=='1'){
                            $contentAlign='';
                                if(!empty($content->text->Align)){
                                    $contentAlign='text-align: '.$content->text->Align.'; ';
                                }
                            $contentPaddingLeft='';
                                if(!empty($content->text->LeftPadding)){
                                    $contentPaddingLeft='padding-left: '.$content->text->LeftPadding.'px; ';
                                }
                            $contentPaddingRight='';
                                if(!empty($content->text->RightPadding)){
                                    $contentPaddingRight='padding-right: '.$content->text->RightPadding.'px; ';
                                }
                            if($content->button->type_selector_text=='href'){

                            //Text
                            $component_settings['componentText'] = (!empty($content->text->val))
                                ? '<div style="color: rgb('.$content->text->color.'); '.$contentAlign.$contentPaddingLeft.$contentPaddingRight.'">'.$content->text->val.'</div>'
                                : '';
                            }else{
                                $component_settings['componentText'] = (!empty($content->text->val))
                                ? '<div style="color: rgb('.$content->text->color.'); '.$contentAlign.$contentPaddingLeft.$contentPaddingRight.'">'.$content->text->val.
                                        '<div class="more_text">'.
                                            $content->button->textTypeButton->val.
                                        '</div>'.
                                        '</div>'
                                : '';
                            }
                        }else{
                            $component_settings['componentText']='';
                        }
                        if($content->show_btn=='1'){
                            //Button
                            if($content->button->type_selector_text=='href'){
                                $component_settings['componentButton'] = (!empty($content->button->val) && !empty($content->button->link))
                                ?   '<a class="el-button" href="'.$content->button->link.'" '.
                                            'style="background-color: rgb('.$content->button->buttonColor.'); '.
                                            'box-shadow: 0px 8px 12.48px 0.52px rgba('.$content->button->buttonColor.',0.35)">'.
                                        '<span style="z-index: 999; color: rgb('.$content->button->color.');">'.
                                            $content->button->val.
                                        '</span>'.
                                        '<span class="bg"'.
                                            'style="background-color: rgb('.$content->button->hoverColor.'); '.
                                            'box-shadow: 0px 8px 12.48px 0.52px rgba('.$content->button->hoverColor.', 0.35)">'.
                                        '</span>'.
                                    '</a>'
                                : '';
                            }else{
                                $component_settings['componentButton'] = (!empty($content->button->val))
                                ?   '<a class="el-button more_type" href="#more" '.
                                            'style="background-color: rgb('.$content->button->buttonColor.'); '.
                                            'box-shadow: 0px 8px 12.48px 0.52px rgba('.$content->button->buttonColor.',0.35)">'.
                                        '<span style="z-index: 999; color: rgb('.$content->button->color.');">'.
                                            $content->button->val.
                                        '</span>'.
                                        '<span class="bg"'.
                                            'style="background-color: rgb('.$content->button->hoverColor.'); '.
                                            'box-shadow: 0px 8px 12.48px 0.52px rgba('.$content->button->hoverColor.', 0.35)">'.
                                        '</span>'.
                                    '</a>'
                                : '';
                            }
                        }else{
                            $component_settings['componentButton']='';
                        }

                        $imageWidth='';
                        if(!empty($content->img)){
                            if(intval($content->img->width)>0){
                                $imageWidth=' width="'.$content->img->width.'" ';
                            }
                        }
                        $imageHeight='';
                        if(!empty($content->img)){
                            if(intval($content->img->height)>0){
                                $imageHeight=' height="'.$content->img->height.'" ';
                            }
                        }

                        if($content->show_img){
                            //Video or image tag
                            if(!empty($content->videoOrImage->val)){
                                $component_settings['videoOrImageContent'] = ($content->videoOrImage->type == 'image')
                                    ? '<img src="'.$this->imagePathToStorage($content->videoOrImage->val).'" '.$imageWidth.$imageHeight.' alt="">'
                                    : $content->videoOrImage->val;
                            }else{
                                $component_settings['videoOrImageContent'] = '';
                            }
                        }else{
                            $component_settings['videoOrImageContent']='';
                            $component_settings['videoOrImageContent'];
                        }
                        break;

                    case 'FastRequestForm':
                        //Text
                        $component_settings['componentText'] = (!empty($content->text->val))
                            ? '<div style="color: rgb('.$content->text->color.');">'.$content->text->val.'</div>'
                            : '';
                        //Background-image
                        $component_settings['backgroundImage'] = (isset($content->image) && !empty($content->image))
                            ? 'background: url('.$content->image.') no-repeat;'
                            : '';
                        //Wrapper float
                        switch($content->textAlign){
                            case 'left': $component_settings['mainWrap'] = ''; break;

                            case 'center': $component_settings['mainWrap'] = 'style="margin: 0 auto; float: none;"'; break;

                            case 'right': $component_settings['mainWrap'] = 'style="float: right;"'; break;
                        }
                        //Button
                        $component_settings['componentButton'] = (!empty($content->button->val))
                            ?   '<form action="'.Backend::url('luckyweb/customcontent/send_email').'" class="callback-form">'.
                                '<input name="form" value="Форма быстрого заказа" type="hidden">'.
                                '<input name="emails" value="'.$page->slug.'" type="hidden">'.
                                    '<div class="form-field"><input name="name" class="el-input" placeholder="Имя" required="required" type="text"></div>'.
                                    '<div class="form-field"><input name="tel" class="el-input" placeholder="Телефон" required="required" type="text"></div>'.
                                    '<button type="submit"'.
                                            'class="el-button" '.
                                            'style="background-color: rgb('.$content->button->buttonColor.'); '.
                                                    'box-shadow: 0px 8px 12.48px 0.52px rgba('.$content->button->buttonColor.', 0.35)">'.
                                        '<span style="z-index: 999; color: rgb('.$content->button->color.');">'.$content->button->val.'</span>'.
                                        '<span class="bg" style="background-color: rgb('.$content->button->hoverColor.'); '.
                                            'box-shadow: 0px 8px 12.48px 0.52px rgba('.$content->button->hoverColor.',0.35)"></span>'.
                                    '</button>'.
                                '</form>'
                            : '';
                        break;

                    case 'RequestForm':
                        //Button
                        $component_settings['componentButton'] = (!empty($content->button->val))
                            ?   '<form action="'.Backend::url('luckyweb/customcontent/send_email').'" class="callback-form">'.
                                    '<input name="form" value="Форма заказа" type="hidden">'.
                                    '<input name="emails" value="'.$page->slug.'" type="hidden">'.
                                    '<div class="form-field"><input name="name" class="el-input" placeholder="Имя" required="required" type="text"></div>'.
                                    '<div class="form-field"><input name="tel" class="el-input" placeholder="Телефон" required="required" type="text"></div>'.
                                    '<div class="form-field"><input name="email" class="el-input" placeholder="Email" required="required" type="text"></div>'.
                                    '<div class="form-field">'.
                                        '<button type="submit"'.
                                                'class="el-button" '.
                                                'style="background-color: rgb('.$content->button->buttonColor.'); '.
                                                    'box-shadow: 0px 8px 12.48px 0.52px rgba('.$content->button->buttonColor.', 0.35)">'.
                                            '<span style="z-index: 999; color: rgb('.$content->button->color.');">'.$content->button->val.'</span>'.
                                            '<span class="bg" style="background-color: rgb('.$content->button->hoverColor.'); '.
                                                'box-shadow: 0px 8px 12.48px 0.52px rgba('.$content->button->hoverColor.',0.35)"></span>'.
                                        '</button>'.
                                    '</div>'.
                                '</form>'
                            : '';
                        //Background color
                        $component_settings['backgroundColor'] = $content->background;
                        break;

                    case 'ProductsView':
                        //Create array of row grid
                        $rows = [];
                        //Element width multiplication
                        $mult_count = 0;
                        //Create array of column grid
                        $items = [];
//                        echo '<pre>';
//                        print_r($content->items);
//                        echo '</pre>';
                        foreach($content->items as $i => $item){
                            //Get product data
                            $product = Product::find($item->id);

                            $category = $product->category()->select('slug')->first();

                            $item->slug = $product->slug;
                            $item->name = $product->name;
                            $item->subname = $product->subname;
                            $item->priceSplit = explode('.', number_format($product->getActivePrice(), 0, '', '.'));
                            $item->isWeekPromo = $product->isWeekPromo();
                            $item->mixed_discount = $product->getMixedDisount();
                            $item->units = $product->units;
                            $item->category = (!empty($category))
                                ? $category->slug.'/'
                                : '';
                            //Add items for row
                            $items[] = $item;

                            //Increase width multiplicatior
                            $mult_count += $item->mult;

                            //If there are 4 column for row
                            if($mult_count == 4){
                                $rows[] = [
                                    'items' => $items
                                ];
                                //Clear width multiplicatior and column grid
                                $items = [];
                                $mult_count = 0;
                            }
                        }

                        $component_settings['productsArray'] = '';
                        foreach($rows as $row){
                            if(!empty($row['items'])){
                                //Open Row
                                $component_settings['productsArray'] .= '<div class="row">';

                                switch(count($row['items'])){
                                    case 1: $item_width = '100%'; break;

                                    case 2: $item_width = '50%'; break;

                                    case 3: $item_width = '33%'; break;

                                    default: $item_width = '270px';
                                }

                                foreach($row['items'] as $item){
                                    //If isset image
                                    if(!empty($item->img)){
                                        $image = '<img style="" src="'.$this->imagePathToStorage($item->img).' " alt="'.$item->name.' '.$item->subname.'">';
                                    }

                                    //If there is week promo
                                    if($item->isWeekPromo){
                                        $component_settings['productsArray'] .=
                                        '<div class="week-promo">'.
                                            '<img src="'.asset('themes/shara/assets/images/mixed-discount-yellow.png').'">'.
                                            '<span class="discount">-'.$item->mixed_discount.'</span>'.
                                            '<span class="promo-percent">%</span>'.
                                        '</div>';

                                        $price_promo_class = ' week-promo-price';
                                    }else{
                                        $price_promo_class = '';
                                    }

                                    $component_settings['productsArray'] .=
                                    '<div class="product" style="width: '.$item_width.'">'.
                                        '<a href="'.asset('/catalog/'.$item->category.$item->slug).'" title="'.$item->name.' '.$item->subname.'">'.
                                            $image.
                                            '<div class="product-title">'.
                                                '<span class="product-name">'.$item->name.'</span>'.
                                                '<div class="price'.$price_promo_class.'">'.
                                                    '<span class="price-big">'.$item->priceSplit[0].'</span>.';

                                            unset($item->priceSplit[0]);
                                    //If price is quoted in millions
                                    $component_settings['productsArray'] .= implode('.',$item->priceSplit).
                                                    '<i class="fa fa-rub"></i>'.
                                                '</div>';
                                    //If there is price units
                                    if(!empty($item->units)){
                                        $component_settings['productsArray'] .= '<div class="units">цена за '.$item->units.'</div>';
                                    }
                                    //Shoping cart icon
                                    $component_settings['productsArray'] .=
                                                '<a href="#" class="js-order_form_button order-button" data-id="'.$item->id.'">'.
                                                    '<img src="'.asset('themes/shara/assets/images/lending/icons/cart.png').'" alt="Заказать">'.
                                                '</a>';

                                    $component_settings['productsArray'] .=
                                            '</div>'.
                                        '</a>'.
                                    '</div>';
                                }
                                //Close Row
                                $component_settings['productsArray'] .= '</div>';
                            }
                        }
                        break;
                        case 'TextBlock':
                        //Wrapper class
                        if($content->textAlign == 'center'){
                            $component_settings['mainWrap'] = 'center TextBlock';
                            $component_settings['secondaryWrap'] = 'center_container TextBlock_container';
                            $component_settings['secondaryWrapStyle'] = '';
                        }elseif($content->textAlign=='left'){
                            $component_settings['mainWrap'] = 'left TextBlock';
                            $component_settings['secondaryWrap'] = 'left_container TextBlock_container';
                            $component_settings['secondaryWrapStyle'] = $content->textAlign;
                        }else{
                            $component_settings['mainWrap'] = 'right TextBlock';
                            $component_settings['secondaryWrap'] = 'right_container TextBlock_container';
                            $component_settings['secondaryWrapStyle'] = $content->textAlign;
                        }

                            $component_settings['componentTitle'] = '<!--seo_page_text_start-->'.$component_settings['componentTitle'];
                        //Text
                        $component_settings['componentText'] = (!empty($content->text->val))
                            ? '<div style="color: rgb('.$content->text->color.');">'.$content->text->val.'</div>'
                            : '';
                        if($content->btn_render=='1'){
                            $component_settings['componentText'].='<div class="text_block_container_hidden_text" style="color: rgb('.$content->sub_text->color.');">'.$content->sub_text->val.'</div>';
                            $component_settings['componentText'].='<div class="close_text_btn_wrap"><a href="#" class="close_text_btn">Развернуть текст</a></div>';
                        }
                        $component_settings['componentText'].='<!--seo_page_text_end-->';
                        break;
                        case 'GridBuilder':
                            //Create array of row grid
                        $rows = [];
                            $this->mixedPromotion = Promotion::mixedPromotions()->current()->first();
                        foreach($content->items as $i => $item){
                            $prod_tmp=[];
                            if(isset($item->products)){
                                foreach($item->products as $ii => $product){
                                    //Get product data
                                    $product_obj = Product::find($product->id);
                                    if(is_object($product_obj)){
                                        $category = $product_obj->category()->select('slug')->first();

                                        //print_r($product_obj);
                                        $tmp=[
                                            'product_obj'=>$product_obj,
                                            'category'=>(!empty($category))? $category->slug.'/': '',
                                            'width'=>$item->config->productsWidth[$ii],

                                        ];

                                        $prod_tmp[]=$tmp;
                                    }
                                }
                            }

                            $obj=[
                                'config'=>[
                                    'location'=>$item->config->location,
                                    'productsWidth'=>isset($item->config->productsWidth)?$item->config->productsWidth:'',
                                    'type'=>$item->config->type
                                ],
                                'products'=>$prod_tmp,
                                'count'=>count($prod_tmp)
                            ];
                            $rows[]=$obj;
                        }
                        $component_settings['productsArray'] = View('luckyweb.customcontent::gridbuilder', ['elements'=>$rows,'is_mobile'=>self::isMobile()]);
                        break;

                        case 'ImageGridBuilder':
                            $rows = [];
                            if(isset($content->items)){
                                foreach($content->items as $i => $item){
                                    $prod_tmp=[];
                                    if(isset($item->products)){
                                        foreach($item->products as $ii => $product){
                                                //print_r($product_obj);
                                            $alig_class='';
                                                switch ($product->textVerticalAlign){
                                                    case 'top': $alig_class=' text-vertical-align-top';
                                                    break;
                                                    case 'bottom': $alig_class=' text-vertical-align-bottom';
                                                    break;
                                                }
                                                $tmp=[
                                                    'color'=>$product->color,
                                                    'text'=>$product->text,
                                                    'width'=>$item->config->productsWidth[$ii],
                                                    'img'=>$product->img,
                                                    'href'=>$product->href,
                                                    'textVerticalAlign'=>$alig_class
                                                ];

                                                $prod_tmp[]=$tmp;
                                        }
                                    }

                                    $obj=[
                                        'in_mobile'=>$item->in_mobile,
                                        'config'=>[
                                            'location'=>$item->config->location,
                                            'productsWidth'=>isset($item->config->productsWidth)?$item->config->productsWidth:'',
                                            'type'=>$item->config->type
                                        ],
                                        'products'=>$prod_tmp
                                    ];
                                    $rows[]=$obj;
                                }
                            }else{
                                $component_settings['imageArray']='';
                            }
                            $component_settings['imageArray'] = '';
                            foreach($rows as $row){
                                if((self::isMobile() && $row['in_mobile']=='true') || !self::isMobile()){
                                    $component_settings['imageArray'].='<div class="product_row ImageGridBuilderWrapper '.$row['config']['location'].' type_row_'.$row['config']['type'].'">'.
                                        '<div class="product_row_wrap">';
                                    foreach ($row['products'] as $element){
                                        $image='';
                                        if(!empty($element['img'])){
                                            $image = '<img style="" src="'.$this->imagePathToStorage($element['img']).'" alt="'.$element['img'].'">';
                                        }

                                        $component_settings['imageArray'] .=
                                        '<div class="product width_'.$element['width'].$element['textVerticalAlign'].' ">'.
                                            '<a href="'.$element['href'].'">'.
                                                    $image.
                                                '<div class="imgGridText" style="color:'.$element['color'].'">'.$element['text'].'</div>'.
                                            '</a>'.
                                        '</div>';
                                    }
                                    $component_settings['imageArray'] .='</div>';
                                    $component_settings['imageArray'] .= '</div>';
                                }
                            }

                        break;
                }

                //Get component template
                $template = file_get_contents($templates_folder.strtolower($component->component_slug).'/default.htm');

                foreach($component_settings as $key => $value){
                    $template = str_replace('{{ '.$key.' }}', $value, $template);
                }

                $component_content .= $template;
            }
            $this->page['component_content'] = $component_content;
        }
//        $this->addCss('/themes/shara/assets/css/sheepfish/style.css');
//        $this->addJs('/themes/shara/assets/js/sheepfish/scenario.js');
//        if(self::isMobile()){
//            $this->addCss('/themes/shara/assets/css/sheepfish/responsive.css');
//        }


    }
}
