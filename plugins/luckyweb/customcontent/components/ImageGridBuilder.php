<?php namespace LuckyWeb\CustomContent\Components;

use Cms\Classes\ComponentBase;

class ImageGridBuilder extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Построение сетки картинок',
            'description' => 'Вывод картинок по сеткке'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
