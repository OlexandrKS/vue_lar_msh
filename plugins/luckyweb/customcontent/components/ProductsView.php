<?php namespace LuckyWeb\CustomContent\Components;

use Cms\Classes\ComponentBase;

class ProductsView extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Вывод продуктов',
            'description' => 'Вывод ряда продуктов или целой категории'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
