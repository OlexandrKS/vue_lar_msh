<?php namespace Luckyweb\Customcontent\Models;

use Model;

/**
 * Page Model
 */
class Page extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_customcontent_pages';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'title',
        'slug',
        'file',
        'description',
        'meta_title',
        'meta_description',
        'emails',
        'breadcrumbs'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'contents' => 'LuckyWeb\CustomContent\Models\PageContent'
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    //Get templates list
    public function getTemplateOptions(){
        //templates directory
        $files = scandir('themes/shara/layouts');
        //Default template
        $result = ['-- без шаблона --'];

        foreach($files as $file){
            if(($file != '.') && ($file != '..')){
                $result[substr($file, 0, -4)] = substr($file, 0, -4);
            }
        }

        return $result;
    }
}
