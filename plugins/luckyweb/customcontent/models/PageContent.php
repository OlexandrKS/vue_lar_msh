<?php namespace LuckyWeb\CustomContent\Models;

use Model;

/**
 * PageContent Model
 */
class PageContent extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_customcontent_page_contents';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'page_id',
        'position',
        'component_slug',
        'component_name',
        'content'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'page' => 'LuckyWeb\CustomContent\Models\Page'
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
