<?php namespace Luckyweb\Customcontent\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdatePagesTable extends Migration
{
    public function up()
    {
        Schema::table('luckyweb_customcontent_pages', function (Blueprint $table) {
            $table->string('emails')->nullable();
        });
    }

    public function down()
    {
        Schema::table('luckyweb_customcontent_pages', function (Blueprint $table) {
            $table->dropColumn('emails');
        });
    }
}
