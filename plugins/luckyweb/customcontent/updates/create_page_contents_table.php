<?php namespace LuckyWeb\CustomContent\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePageContentsTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_customcontent_page_contents', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('page_id')->unsigned();
            $table->integer('position')->unsigned();
            $table->string('component_slug');
            $table->string('component_name');
            $table->text('content')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_customcontent_page_contents');
    }
}
