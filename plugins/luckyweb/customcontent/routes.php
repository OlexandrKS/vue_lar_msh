<?php
//Get components list
//if isset $page_id -> get page content
Route::get(
    'backend/luckyweb/customcontent/pages/get_components/{page_id?}',
    'LuckyWeb\CustomContent\Controllers\Pages@getComponentsList'
);
//Get template html content
Route::get(
    'backend/luckyweb/customcontent/pages/component_template/{slug}',
    'LuckyWeb\CustomContent\Controllers\Pages@getComponentTemplate'
);
//Store image
Route::post(
    'backend/luckyweb/customcontent/pages/save_image',
    'LuckyWeb\CustomContent\Controllers\Pages@saveImage'
);
//Store page
Route::post(
    'backend/luckyweb/customcontent/pages/store',
    'LuckyWeb\CustomContent\Controllers\Pages@create_onSave'
);
//Update page
Route::post(
    '/backend/luckyweb/customcontent/pages/{id}/edit',
    'LuckyWeb\CustomContent\Controllers\Pages@update_onSave'
);
//Get products
//if isset $ids -> get by ids array
Route::get(
    '/backend/luckyweb/customcontent/pages/get_products/{ids?}',
    'LuckyWeb\CustomContent\Controllers\Pages@getProducts'
);

//Email send page
Route::post(
    '/backend/luckyweb/customcontent/send_email',
    'LuckyWeb\CustomContent\Controllers\Pages@send_email'
);
Route::get(
    'backend/luckyweb/customcontent/pages/get_html/{slug}',
    'LuckyWeb\CustomContent\Controllers\Pages@get_html'
);
Route::get(
    'backend/luckyweb/customcontent/pages/get_html_mobile/{slug}',
    'LuckyWeb\CustomContent\Controllers\Pages@get_html_mobile'
);