<?php

namespace LuckyWeb\Importer\Updates;

use App\ServiceLayer\Importer\ImportManager;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class CreateImportTables extends Migration
{
    private $tablePrefix = ImportManager::DB_TABLE_PREFIX;

    public function up()
    {
        $this->down();

        // Categories
        Schema::create("{$this->tablePrefix}_categories", function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->integer('price_from');
            $table->integer(snake_case('NumberOfPhotos'));
            $table->integer('order')->default(0);
            $table->string('slug');
            $table->string(snake_case('PriceDescription'), 1000)->nullable();
            $table->integer(snake_case('OwnedSite'))->nullable()->default(0);
            $table->text('banner_week_1')->nullable();
            $table->text('banner_week_2')->nullable();

            // Localisation
            $table->string('name_eng')->nullable();
            $table->string(snake_case('PriceDescription_eng'), 1000)->nullable();
            $table->text('banner_week_1_eng')->nullable();
            $table->text('banner_week_2_eng')->nullable();
        });

        // Sub Categories
        Schema::create("{$this->tablePrefix}_sub_categories", function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->integer('category_id')->nullable();
            $table->integer(snake_case('NumberOfPhotos'));
            $table->integer('order')->default(0);
            $table->string('slug');
            $table->integer(snake_case('OwnedSite'))->nullable()->default(0);

            // Localisation
            $table->string('name_eng')->nullable();
        });

        // Goods
        Schema::create("{$this->tablePrefix}_goods", function(Blueprint $table)
        {
            $table->bigIncrements('id')->unsigned();
            $table->mediumText(snake_case('Description'))->nullable();
            $table->integer('description_type_id')->unsigned()->nullable();
            $table->mediumText('description1')->nullable();
            $table->mediumText('description2')->nullable();
            $table->string(snake_case('GoodsName_1c'))->nullable();
            $table->integer(snake_case('FurnisherId'))->unsigned()->nullable();
            $table->integer(snake_case('good_type_id'))->unsigned()->nullable();
            $table->string(snake_case('OverallDimensions'))->nullable();
            $table->decimal(snake_case('PriceTopB'), 10,2)->default(0);
            $table->decimal(snake_case('PriceLowerB'), 10,2)->default(0);
            $table->integer(snake_case('DiscountB'))->nullable();
            $table->decimal(snake_case('PriceSpecial'), 10,2)->default(0);
            $table->integer(snake_case('DiscountSpecial'))->nullable();
            $table->string(snake_case('ConditionForDiscount'))->nullable();
            $table->decimal(snake_case('PricePromo'), 10,2)->default(0);
            $table->integer(snake_case('MixedPromo'))->nullable();
            $table->integer(snake_case('DiscountPromo'))->nullable();
            $table->boolean(snake_case('OnlyForSetOfPackages'))->nullable();
            $table->string(snake_case('category_id'))->nullable();
            $table->string(snake_case('SubCategoryId'))->nullable();
            $table->string(snake_case('PriceDescription'), 1000)->nullable();
            $table->string(snake_case('WeekPromo'))->nullable();
            $table->integer(snake_case('OwnedSite'))->nullable()->default(0);
            $table->string('set_id')->nullable();
            $table->integer('max_order_item')->nullable();
            $table->decimal('delivery_cost', 10, 2)->nullable();
            $table->decimal('assembling_cost', 10, 2)->nullable();
            $table->string('type_name_for_page')->nullable();
            $table->integer('width')->nullable();
            $table->integer('depth')->nullable();
            $table->integer('height')->nullable();
            $table->string('site_type_id')->nullable();
            $table->string('site_width')->nullable();

            // Localisation
            $table->mediumText(snake_case('Description_eng'))->nullable();
        });

        // Colors
        Schema::create("{$this->tablePrefix}_colors", function(Blueprint $table)
        {
            $table->bigIncrements('id')->unsigned();
            $table->string('name');
        });

        // ColorsOfGoods
        Schema::create("{$this->tablePrefix}_goods_colors", function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->bigInteger('good_id')->unsigned();
            $table->bigInteger('color_id')->unsigned();
            $table->string('title')->nullable();
            $table->string('color')->nullable();
            $table->integer('number_of_photots')->nullable();
            $table->integer(snake_case('PositionInCategory'))->index();
            $table->integer(snake_case('PositionInCategory2'))->index();
            $table->integer(snake_case('PositionInMixedPromo'))->index();
            $table->text(snake_case('LinkToVideo'))->nullable();
            $table->decimal(snake_case('PriceTopSale'), 10,2)->nullable();
            $table->decimal(snake_case('PriceLowerSale'), 10,2)->nullable();
            $table->decimal(snake_case('DiscountSale'), 10,2)->nullable();
            $table->string(snake_case('AlsoInSpecList'))->nullable();
            $table->string('slug');
            $table->integer('residual_indicator')->nullable();
            $table->boolean('category_invisible')->default(false);
            $table->string('variant_of_design')->nullable()->defaul(null);

            // Localisation
            $table->string('title_eng')->nullable();
            $table->string('color_eng')->nullable();
            $table->string('variant_of_design_eng')->nullable();
        });

        // Furnishers
        Schema::create("{$this->tablePrefix}_furnishers", function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->string('name');
        });

        // TypesOfGoods
        Schema::create("{$this->tablePrefix}_good_types", function(Blueprint $table)
        {
            $table->bigIncrements('id')->unsigned();
            $table->string('name');
            $table->string('full_name');
            $table->string('category_id')->index();
            $table->string('sub_category_id')->nullable()->index();
        });

        // SetOfPackages
        Schema::create("{$this->tablePrefix}_packages", function(Blueprint $table)
        {
            $table->increments('id');
            $table->mediumText(snake_case('Description'))->nullable();
            $table->integer('description_type_id')->unsigned()->nullable();
            $table->string('description1')->nullable();
            $table->string('description2')->nullable();
            $table->string(snake_case('GoodsName_1c'))->nullable();
            $table->integer(snake_case('FurnisherId'))->unsigned()->nullable();
            $table->integer(snake_case('good_type_id'))->unsigned()->nullable();
            $table->string(snake_case('OverallDimensions'))->nullable();
            $table->decimal(snake_case('PriceTopB'), 10,2)->default(0);
            $table->decimal(snake_case('PriceLowerB'), 10,2)->default(0);
            $table->integer(snake_case('DiscountB'))->nullable();
            $table->decimal(snake_case('PriceSpecial'), 10,2)->default(0);
            $table->integer(snake_case('DiscountSpecial'))->nullable();
            $table->string(snake_case('ConditionForDiscount'))->nullable();
            $table->decimal(snake_case('PricePromo'), 10,2)->default(0);
            $table->integer(snake_case('MixedPromo'))->nullable();
            $table->integer(snake_case('DiscountPromo'))->nullable();
            $table->string('category_id')->nullable();
            $table->string(snake_case('SubCategoryId'))->nullable();
            $table->string(snake_case('PriceDescription'), 1000)->nullable();
            $table->string(snake_case('WeekPromo'))->nullable();
            $table->integer(snake_case('OwnedSite'))->nullable()->default(0);
            $table->string('set_id')->nullable();
            $table->integer('max_order_item')->nullable();
            $table->decimal('delivery_cost', 10, 2)->nullable();
            $table->decimal('assembling_cost', 10, 2)->nullable();
            $table->string('type_name_for_page')->nullable();
            $table->integer('width')->nullable();
            $table->integer('depth')->nullable();
            $table->integer('height')->nullable();
            $table->string('site_type_id')->nullable();
            $table->string('site_width')->nullable();

            // Localisation
            $table->mediumText(snake_case('Description_eng'))->nullable();
        });

        // ColorsOfSetOfPackages
        Schema::create("{$this->tablePrefix}_packages_colors", function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->bigInteger('package_id')->unsigned();
            $table->bigInteger('color_id')->unsigned();
            $table->string('title')->nullable();
            $table->string('color')->nullable();
            $table->integer('number_of_photots')->nullable();
            $table->integer(snake_case('PositionInCategory'))->index();
            $table->integer(snake_case('PositionInCategory2'))->index();
            $table->integer(snake_case('PositionInMixedPromo'))->index();
            $table->text(snake_case('LinkToVideo'))->nullable();
            $table->decimal(snake_case('PriceTopSale'), 10,2)->nullable();
            $table->decimal(snake_case('PriceLowerSale'), 10,2)->nullable();
            $table->decimal(snake_case('DiscountSale'), 10,2)->nullable();
            $table->string(snake_case('AlsoInSpecList'))->nullable();
            $table->string('slug');
            $table->integer('residual_indicator')->nullable();
            $table->boolean('category_invisible')->default(false);
            $table->string('variant_of_design')->nullable()->defaul(null);

            // Localisation
            $table->string('title_eng')->nullable();
            $table->string('color_eng')->nullable();
            $table->string('variant_of_design_eng')->nullable();
        });

        // GoodsOfSetOfPackages
        Schema::create("{$this->tablePrefix}_goods_packages", function(Blueprint $table)
        {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('good_id')->unsigned();
            $table->bigInteger('package_id')->unsigned();
            $table->bigInteger('good_color_id')->unsigned();
            $table->bigInteger('package_color_id')->unsigned();
            $table->integer('quantity')->nullable();
            //$table->primary(['good_id', 'package_id']);
        });

        // TypesOtherDescription
        Schema::create("{$this->tablePrefix}_description_types", function(Blueprint $table)
        {
            $table->bigIncrements('id')->unsigned();
            $table->integer(snake_case('NumberOfDesc'));
            $table->string('desc_name_1');
            $table->string('desc_name_2');
        });

        // DiscountsOfMonths
        Schema::create("{$this->tablePrefix}_promotions", function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('date')->nullable();
            $table->integer('number_of_photots')->nullable();
            $table->string('banner1')->nullable();
            $table->string('banner2')->nullable();
            $table->string('category_id')->nullable();
            $table->text('video_title')->nullable();
            $table->text('link_to_video')->nullable();

            // Localisation
            $table->string('name_eng')->nullable();
            $table->string('banner1_eng')->nullable();
            $table->string('banner2_eng')->nullable();
            $table->text('video_title_eng')->nullable();
        });

        Schema::create("{$this->tablePrefix}_shops", function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('region')->nullable()->index();
            $table->string('city')->nullable()->index();
            $table->integer('map_number')->nullable();
            $table->string('mall_name')->nullable();
            $table->string('street_address')->nullable();
            $table->string('longlat')->nullable();
            $table->string('mode_1')->nullable();
            $table->string('mode_2')->nullable();
            $table->string('phone')->nullable();
            $table->boolean('discount_center')->nullable();
            //region;city;map_number;mall_name;street_address;longlat;mode_1;mode_2;phone
        });

        // DescriptionOfFiles
        Schema::create("{$this->tablePrefix}_files", function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->string(snake_case('File'))->nullable();
            $table->string(snake_case('Description'))->nullable();
            $table->string(snake_case('FileName'))->nullable();
            $table->string(snake_case('ShortDescr'))->nullable();
            $table->integer(snake_case('OwnedSite'))->nullable()->default(0);
            $table->integer('assignment')->nullable();

            // Localisation
            $table->string('description_eng')->nullable();
            $table->string(snake_case('ShortDescr_eng'))->nullable();
        });

        // Sets
        Schema::create("{$this->tablePrefix}_sets", function(Blueprint $table)
        {
            $table->integer('id')->nullable();
            $table->string('name')->nullable();
        });

        // PositionsInSpecList
        Schema::create("{$this->tablePrefix}_spec_positions", function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('package_id')->nullable();
            $table->integer('package_color_id')->nullable();
            $table->integer('good_id')->nullable();
            $table->integer('good_color_id')->nullable();
            $table->integer('position1')->nullable();
            $table->integer('position2')->nullable();
        });

        // Specifications
        Schema::create("{$this->tablePrefix}_specifications", function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer(snake_case('SpecId'))->nullable()->defaul(null);
            $table->string(snake_case('SpecName'))->nullable()->defaul(null);
            $table->integer(snake_case('Sort'))->nullable()->defaul(null);
            $table->integer('popup')->nullable()->defaul(null);

            // Localisation
            $table->string(snake_case('SpecName_eng'))->nullable();
        });

        // ValuesOfSpecifications
        Schema::create("{$this->tablePrefix}_specification_values", function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer(snake_case('ValueId'))->nullable()->defaul(null);
            $table->string(snake_case('ValueName'))->nullable()->defaul(null);
            $table->integer(snake_case('Sort'))->nullable()->defaul(null);

            // Localisation
            $table->string(snake_case('ValueName_eng'))->nullable();
        });

        // ValuesOfSpecificationsByCategories
        Schema::create("{$this->tablePrefix}_specification_values_categories", function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer(snake_case('CategoryId'))->nullable()->defaul(null);
            $table->integer(snake_case('SpecId'))->nullable()->defaul(null);
            $table->integer(snake_case('ValueId'))->nullable()->defaul(null);
        });

        // ValuesOfSpecificationsBySets
        Schema::create("{$this->tablePrefix}_specification_values_sets", function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer(snake_case('ValueId'))->nullable()->defaul(null);
            $table->integer(snake_case('CategoryId'))->nullable()->defaul(null);
            $table->integer(snake_case('SetId'))->nullable()->defaul(null);
        });

        // TogetherCheaper
        Schema::create("{$this->tablePrefix}_together_cheeper", function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('form_good_id')->nullable()->defaul(null);
            $table->integer('form_package_id')->nullable()->defaul(null);
            $table->integer('supp_good_id')->nullable()->defaul(null);
            $table->integer('supp_package_id')->nullable()->defaul(null);
            $table->integer(snake_case('Saving'))->nullable()->defaul(null);
            $table->integer(snake_case('TheBest'))->nullable()->defaul(null);
        });

        // TypesOfGoodsForSite
        Schema::create("{$this->tablePrefix}_site_good_types", function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('type_id')->nullable()->defaul(null);
            $table->string('type_name')->nullable()->defaul(null);
            $table->integer('width')->nullable()->defaul(null);

            // Localisation
            $table->string('type_name_eng')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists("{$this->tablePrefix}_categories");
        Schema::dropIfExists("{$this->tablePrefix}_sub_categories");
        Schema::dropIfExists("{$this->tablePrefix}_goods");
        Schema::dropIfExists("{$this->tablePrefix}_colors");
        Schema::dropIfExists("{$this->tablePrefix}_goods_colors");
        Schema::dropIfExists("{$this->tablePrefix}_furnishers");
        Schema::dropIfExists("{$this->tablePrefix}_good_types");
        Schema::dropIfExists("{$this->tablePrefix}_packages");
        Schema::dropIfExists("{$this->tablePrefix}_packages_colors");
        Schema::dropIfExists("{$this->tablePrefix}_goods_packages");
        Schema::dropIfExists("{$this->tablePrefix}_description_types");
        Schema::dropIfExists("{$this->tablePrefix}_promotions");
        Schema::dropIfExists("{$this->tablePrefix}_shops");
        Schema::dropIfExists("{$this->tablePrefix}_files");
        Schema::dropIfExists("{$this->tablePrefix}_sets");
        Schema::dropIfExists("{$this->tablePrefix}_spec_positions");
        Schema::dropIfExists("{$this->tablePrefix}_specifications");
        Schema::dropIfExists("{$this->tablePrefix}_specification_values");
        Schema::dropIfExists("{$this->tablePrefix}_specification_values_categories");
        Schema::dropIfExists("{$this->tablePrefix}_specification_values_sets");
        Schema::dropIfExists("{$this->tablePrefix}_together_cheeper");
        Schema::dropIfExists("{$this->tablePrefix}_site_good_types");
    }
}
