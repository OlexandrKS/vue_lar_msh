<?php

namespace LuckyWeb\Importer\Updates;

use App\ServiceLayer\Importer\ImportManager;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class UpdateInstallments20190206 extends Migration
{
    public function up()
    {
        Schema::table(ImportManager::DB_TABLE_PREFIX . '_goods', function (Blueprint $table) {
            $table->boolean('installments')->default(0);
        });

        Schema::table(ImportManager::DB_TABLE_PREFIX . '_packages', function (Blueprint $table) {
            $table->boolean('installments')->default(0);
        });
    }

    public function down()
    {
        Schema::table(ImportManager::DB_TABLE_PREFIX . '_goods', function (Blueprint $table) {
            $table->dropColumn(['installments']);
        });

        Schema::table(ImportManager::DB_TABLE_PREFIX . '_packages', function (Blueprint $table) {
            $table->dropColumn(['installments']);
        });
    }
}
