<?php

namespace LuckyWeb\Importer\Updates;

use App\ServiceLayer\Importer\ImportManager;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class UpdateImportItemNumber20190213 extends Migration
{
    public function up()
    {
        Schema::table(ImportManager::DB_TABLE_PREFIX . '_goods', function (Blueprint $table) {
            $table->string('item_number')->nullable();
        });

        Schema::table(ImportManager::DB_TABLE_PREFIX . '_packages', function (Blueprint $table) {
            $table->string('item_number')->nullable();
        });
    }

    public function down()
    {
        Schema::table(ImportManager::DB_TABLE_PREFIX . '_goods', function (Blueprint $table) {
            $table->dropColumn('item_number');
        });

        Schema::table(ImportManager::DB_TABLE_PREFIX . '_packages', function (Blueprint $table) {
            $table->dropColumn('item_number');
        });
    }
}
