<?php

namespace LuckyWeb\Importer\Updates;

use App\ServiceLayer\Importer\ImportManager;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class UpdateImportTablesLocale20190206 extends Migration
{
    public function up()
    {
        Schema::table(ImportManager::DB_TABLE_PREFIX . '_goods', function (Blueprint $table) {
            $table->mediumText('description1_eng')->nullable();
            $table->mediumText('description2_eng')->nullable();
        });

        Schema::table(ImportManager::DB_TABLE_PREFIX . '_packages', function (Blueprint $table) {
            $table->mediumText('description1_eng')->nullable();
            $table->mediumText('description2_eng')->nullable();
        });
    }

    public function down()
    {
        Schema::table(ImportManager::DB_TABLE_PREFIX . '_goods', function (Blueprint $table) {
            $table->dropColumn(['description1_eng', 'description2_eng']);
        });

        Schema::table(ImportManager::DB_TABLE_PREFIX . '_packages', function (Blueprint $table) {
            $table->dropColumn(['description1_eng', 'description2_eng']);
        });
    }
}
