<?php

namespace LuckyWeb\Importer\Updates;

use App\ServiceLayer\Importer\ImportManager;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class UpdateSiteTypeHeight20190206 extends Migration
{
    public function up()
    {
        Schema::table(ImportManager::DB_TABLE_PREFIX . '_site_good_types', function (Blueprint $table) {
            $table->string('height')->default(0)->after('width');
        });
    }

    public function down()
    {
        Schema::table(ImportManager::DB_TABLE_PREFIX . '_site_good_types', function (Blueprint $table) {
            $table->dropColumn(['height']);
        });
    }
}
