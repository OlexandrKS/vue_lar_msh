<?php

namespace LuckyWeb\Importer\Updates;

use App\ServiceLayer\Importer\ImportManager;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class UpdateSiteTypeNamePlural20190606 extends Migration
{
    public function up()
    {
        Schema::table(ImportManager::DB_TABLE_PREFIX . '_site_good_types', function (Blueprint $table) {
            $table->string('plural')->nullable()->after('type_name');
        });
    }

    public function down()
    {
        Schema::table(ImportManager::DB_TABLE_PREFIX . '_site_good_types', function (Blueprint $table) {
            $table->dropColumn(['plural']);
        });
    }
}
