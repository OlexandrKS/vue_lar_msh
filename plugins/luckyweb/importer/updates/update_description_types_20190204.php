<?php

namespace LuckyWeb\Importer\Updates;

use App\ServiceLayer\Importer\ImportManager;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class UpdateDescriptionTypes20190204 extends Migration
{
    public function up()
    {
        Schema::table(ImportManager::DB_TABLE_PREFIX . '_description_types', function (Blueprint $table) {
            $table->string('desc_name_1_eng')->nullable();
            $table->string('desc_name_2_eng')->nullable();
        });
    }

    public function down()
    {
        Schema::table(ImportManager::DB_TABLE_PREFIX . '_description_types', function (Blueprint $table) {
            $table->dropColumn(['desc_name_1_eng', 'desc_name_2_eng']);
        });
    }
}
