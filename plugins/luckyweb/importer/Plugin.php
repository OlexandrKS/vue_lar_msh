<?php

namespace LuckyWeb\Importer;

use Backend;
use System\Classes\PluginBase;

/**
 * Importer Plugin Information File
 */
class Plugin extends PluginBase
{
    // Prefix for configs
    const CONFIG_PREFIX = 'luckyweb.importer';

    /**
     * @var bool
     */
    public $elevated = true;

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Importer',
            'description' => 'No description provided yet...',
            'author'      => 'LuckyWeb',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'LuckyWeb\Importer\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'luckyweb.importer.some_permission' => [
                'tab' => 'Importer',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'importer' => [
                'label'       => 'Importer',
                'url'         => Backend::url('luckyweb/importer/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['luckyweb.importer.*'],
                'order'       => 500,
            ],
        ];
    }
}
