<?php namespace LuckyWeb\User\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Bonus Invites Back-end Controller
 */
class BonusInvites extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('LuckyWeb.User', 'user', 'bonusinvites');
    }
}
