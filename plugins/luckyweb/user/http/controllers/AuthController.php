<?php

namespace LuckyWeb\User\Http\Controllers;

use App\ServiceLayer\Sentry\Sentry;
use Exception;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
use LuckyWeb\User\Classes\SocialAuthManager;
use LuckyWeb\User\Facades\Auth;
use October\Rain\Support\Facades\Flash;

class AuthController extends Controller
{
    protected $availableSocialProviders = [
        'facebook', 'twitter', 'google', 'vkontakte'
    ];

    /**
     * Redirect the user to the provider authentication page.
     *
     * @param string $code social provider code
     * @return Response
     */
    public function redirectToProvider($code)
    {
        if(in_array($code, $this->availableSocialProviders)) {
            return Socialite::driver($code)->redirect();
        }
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @param string $code social provider code
     * @return Response
     */
    public function handleProviderCallback($code)
    {
        if(in_array($code, $this->availableSocialProviders) && !Auth::check()) {
            try {
                $socialUser = Socialite::driver($code)->user();
                if($socialUser) {
                    $mail = $socialUser->getEmail() ?? data_get($socialUser, 'accessTokenResponseBody.email');

                    if(empty($mail)) {
                        Flash::error('Сервис '.$code.' не предоставил Ваш email. Попробуйте войти через другой сервис или через формы входа/регистрации.');
                        return Redirect::to('/account');
                    }

                    $authManager = new SocialAuthManager($socialUser, $code);
                    $user = $authManager->handle();

                    if($user) {
                        if(!$user->is_activated) {
                            Session::put('social_confirm_id', $user->id);
                            return Redirect::to(url('/account/social-confirmation'));
                        }

                        Auth::login($user);
                        return Redirect::to(url('/account-orders'));
                    }
                }
            }
            catch(Exception $e) {
                Sentry::captureException($e);

                Flash::error('Что-то пошло не так! Попробуйте войти через другой сервис или форму авторизации/регистрации');
                return Redirect::to('/account');
            }

        }
        Flash::error('Что-то пошло не так! Попробуйте войти через другой сервис или форму авторизации/регистрации');
        return Redirect::to('/account');
    }

}
