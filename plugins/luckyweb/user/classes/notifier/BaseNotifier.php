<?php namespace LuckyWeb\User\Classes\Notifier;


use LuckyWeb\User\Models\User;

abstract class BaseNotifier
{
    /**
     * @var User
     */
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Check if need to notify
     * @return bool
     */
    abstract public function check();

    /**
     * Process notification actions
     * @param array $data
     * @return bool
     */
    abstract public function notify($data);

    /**
     * Return notification data
     * @return mixed
     */
    abstract public function getData();

    /**
     * Return notification code
     * @return mixed
     */
    abstract public function getCode();

    /**
     * Should be run when notification processed
     * @return mixed
     */
    abstract public function processed();
}