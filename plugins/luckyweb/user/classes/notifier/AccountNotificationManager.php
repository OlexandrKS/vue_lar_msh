<?php namespace LuckyWeb\User\Classes\Notifier;

use LuckyWeb\User\Classes\Notifier\Types\BonusApply;
use LuckyWeb\User\Models\User;


class AccountNotificationManager
{
    const TYPE_BONUS_APPLY = 1;

    /**
     * @var User
     */
    protected $user;

    /**
     * Available notifier types
     * @var array
     */
    protected $types = [
        1 => BonusApply::class
    ];

    /**
     * AccountNotificationManager constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Check if need to notify
     * @return BaseNotifier
     */
    public function check()
    {
        foreach($this->types as $typeClass) {
            $type = new $typeClass($this->user);
            if($type && $type->check()) {
                return $type;
            }
        }
    }

    /**
     * Process notification actions
     * @param int $typeId
     * @param array $data
     * @return bool
     */
    public function notify($typeId, $data)
    {
        return $this->getTypeNotifier($typeId)->notify($data);
    }

    /**
     * Get notifier by type id
     * @param $typeId
     * @return BaseNotifier
     */
    protected function getTypeNotifier($typeId)
    {
        $typeClass = $this->types[$typeId];
        return new $typeClass($this->user);
    }
}