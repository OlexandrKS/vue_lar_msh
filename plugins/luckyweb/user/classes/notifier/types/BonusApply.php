<?php namespace LuckyWeb\User\Classes\Notifier\Types;

use Illuminate\Support\Facades\Mail;
use LuckyWeb\User\Classes\Notifier\BaseNotifier;
use Illuminate\Support\Facades\Session;

class BonusApply extends BaseNotifier
{

    /**
     * Check if need to notify
     * @return bool
     */
    public function check()
    {
        return Session::has('account_notification.bonus_apply.data');
    }

    /**
     * Process notification actions
     * @param array $data
     * @return bool
     */
    public function notify($data)
    {
        // Save data for account popup notification
        Session::put('account_notification.bonus_apply.data', $data);

        // Email to client
        if(!empty($this->user->email)) {
            $receiver = $this->user->email;
            Mail::queue('luckyweb.user::mail.client.bonus_apply', $this->getData(), function ($message) use ($receiver) {
                $message->to($receiver);
            });
        }
    }

    /**
     * Return notification data
     * @return mixed
     */
    public function getData()
    {
        return array_merge(Session::get('account_notification.bonus_apply.data'),[
            'online_store_link' => url('/?utm_source=bonus_card&utm_medium=email&utm_campaign=vam_nachisleno_1000&utm_content=online_store'),
            'contacts_link' => url('/contacts?utm_source=bonus_card&utm_medium=email&utm_campaign=vam_nachisleno_1000&utm_content=adresa_salonov'),
            'bonus_share_link' => url('/shares?action=open&id=3&utm_source=bonus_card&utm_medium=email&utm_campaign=vam_nachisleno_1000&utm_content=uslovia_bonusov')
        ]);
    }

    /**
     * Return notification code
     * @return mixed
     */
    public function getCode()
    {
        return 'bonus_apply';
    }

    /**
     * Should be run when notification processed
     * @return mixed
     */
    public function processed()
    {
        return Session::forget('account_notification.bonus_apply.data');
    }
}