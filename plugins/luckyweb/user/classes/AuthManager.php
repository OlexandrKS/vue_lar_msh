<?php namespace LuckyWeb\User\Classes;

use Illuminate\Auth\Events\Attempting;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Logout;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use October\Rain\Auth\Manager as RainAuthManager;
use LuckyWeb\User\Models\Settings as UserSettings;
use LuckyWeb\User\Models\UserGroup as UserGroupModel;

class AuthManager extends RainAuthManager
{
    const AUTH_TYPE_PHONE = 1;
    const AUTH_TYPE_CARD = 2;
    const AUTH_TYPE_EMAIL = 3;

    protected static $instance;

    protected $sessionKey = 'user_auth';

    protected $userModel = 'LuckyWeb\User\Models\User';

    protected $groupModel = 'LuckyWeb\User\Models\UserGroup';

    protected $throttleModel = 'LuckyWeb\User\Models\Throttle';

    public function init()
    {
        $this->useThrottle = UserSettings::get('use_throttle', $this->useThrottle);
        $this->requireActivation = UserSettings::get('require_activation', $this->requireActivation);
        parent::init();
    }

    /**
     * {@inheritDoc}
     */
    public function extendUserQuery($query)
    {
        $query->withTrashed();
    }

    /**
     * {@inheritDoc}
     */
    public function login($user, $remember = true)
    {
        if ($user->is_guest) {
            throw new AuthException('Пользователь не зарегистрирован.');
        }

        /*
         * Fire the 'beforeLogin' event
         */
        $user->beforeLogin();

        /*
         * Activation is required, user not activated
         */
        if ($this->requireActivation && !$user->is_activated) {
            throw new AuthException('Не удается войти в кабинет, т.к. пользователь не активирован.');
        }

        $this->user = $user;
        /*
         * Create session/cookie data to persist the session
         */
        $toPersist = [$user->getKey(), $user->getPersistCode()];
        Session::put($this->sessionKey, $toPersist);

        if ($remember) {
            Cookie::queue(Cookie::forever($this->sessionKey, $toPersist));
        }

        try {
            app('events')->dispatch(new Login($user, $remember));
        } catch (\Exception $e) {}

        /*
         * Fire the 'afterLogin' event
         */
        $user->afterLogin();
    }

    /**
     * {@inheritDoc}
     */
    public function register(array $credentials, $activate = false, $autoLogin = true)
    {
        if ($guest = $this->findGuestUserByCredentials($credentials)) {
            return $this->convertGuestToUser($guest, $credentials, $activate);
        }

        /** @var Authenticatable $user */
        $user = parent::register($credentials, $activate, $autoLogin);

        try {
            app('events')->dispatch(new Registered($user));
        } catch (\Exception $e) {}

        return $user;
    }

    //
    // Guest users
    //

    public function findGuestUserByCredentials(array $credentials)
    {
        if ($email = array_get($credentials, 'email')) {
            return $this->findGuestUser($email);
        }

        return null;
    }

    public function findGuestUser($email)
    {
        $query = $this->createUserModelQuery();

        return $user = $query
            ->where('email', $email)
            ->where('is_guest', 1)
            ->first();
    }

    /**
     * Registers a guest user by giving the required credentials.
     *
     * @param array $credentials
     * @return Models\User
     */
    public function registerGuest(array $credentials)
    {
        $user = $this->findGuestUserByCredentials($credentials);
        $newUser = false;

        if (!$user) {
            $user = $this->createUserModel();
            $newUser = true;
        }

        $user->fill($credentials);
        $user->is_guest = true;
        $user->save();

        // Add user to guest group
        if ($newUser && $group = UserGroupModel::getGuestGroup()) {
            $user->groups()->add($group);
        }

        // Prevents revalidation of the password field
        // on subsequent saves to this model object
        $user->password = null;

        return $this->user = $user;
    }

    /**
     * Converts a guest user to a registered user.
     *
     * @param Models\User $user
     * @param array $credentials
     * @param bool $activate
     * @return Models\User
     */
    public function convertGuestToUser($user, $credentials, $activate = false)
    {
        $user->fill($credentials);
        $user->convertToRegistered(false);

        // Remove user from guest group
        if ($group = UserGroupModel::getGuestGroup()) {
            $user->groups()->remove($group);
        }

        if ($activate) {
            $user->attemptActivation($user->getActivationCode());
        }

        // Prevents revalidation of the password field
        // on subsequent saves to this model object
        $user->password = null;

        return $this->user = $user;
    }

    /**
     * Attempts to authenticate the given user according to the passed credentials.
     *
     * @param array $credentials The user login details
     * @param bool $remember Store a non-expire cookie for the user
     * @param int $type
     * @return
     * @throws AuthException
     */
    public function authenticate(array $credentials, $remember = true, $type = 3)
    {
        try {
            app('events')->dispatch(new Attempting($credentials, $remember));
        } catch (\Exception $e) {}

        if (! empty($credentials['phone'])) {
            $loginName = 'phone';
        } elseif (!empty($credentials['email'])) {
            $loginName = 'email';
        } elseif(!empty($credentials['card_code'])){
            $loginName = 'card_code';
        }

        // Detect login name
//        switch($type) {
//            case static::AUTH_TYPE_PHONE:
//                $loginName = 'phone';
//                break;
//            case static::AUTH_TYPE_CARD:
//                $loginName = 'card_code';
//                break;
//            case static::AUTH_TYPE_EMAIL:
//                $loginName = 'email';
//                break;
//        }

        if (empty($credentials['email']) && empty($credentials['phone']) && empty($credentials['card_code'])) {
            throw new AuthException(sprintf('Логин обязателен для входа.'));
        }

        if (empty($credentials['password']) && empty($credentials['card_pin']) ) {
            throw new AuthException('Пароль обязателен для входа.');
        }

        /*
         * If throttling is enabled, check they are not locked out first and foremost.
         */
        if ($this->useThrottle) {
            $throttle = $this->findThrottleByLogin($credentials[$loginName], $this->ipAddress);
            $throttle->check();
        }

        /*
         * Look up the user by authentication credentials.
         */
        try {
            $user = $this->findUserByCredentials($credentials);
        }
        catch (AuthException $ex) {
            if ($this->useThrottle) {
                $throttle->addLoginAttempt();
            }

            throw $ex;
        }

        if ($this->useThrottle) {
            $throttle->clearLoginAttempts();
        }

        $user->clearResetPassword();
        $this->login($user, $remember);

        return $this->user;
    }

    /**
     * Finds a user by the login value.
     * @param string $login
     * @return null
     */
    public function findUserByLogin($login)
    {
        $query = $this->createUserModelQuery();
        $user = $query->where('email', $login)
            ->orWhere('phone', $login)
            ->orWhere('card_code', $login)
            ->first();
        return $user ?: null;
    }

    /**
     * Finds a user by the given credentials.
     * @param array $credentials
     * @return mixed
     * @throws AuthException
     */
    public function findUserByCredentials(array $credentials)
    {
        $model = $this->createUserModel();
        $query = $this->createUserModelQuery();
        $hashableAttributes = $model->getHashableAttributes();
        $hashedCredentials = [];

        /*
         * Build query from given credentials
         */
        foreach ($credentials as $credential => $value) {
            // All excepted the hashed attributes
            if (in_array($credential, $hashableAttributes)) {
                $hashedCredentials = array_merge($hashedCredentials, [$credential => $value]);
            }
            else {
                $query = $query->where($credential, '=', $value);
            }
        }

        if (!$user = $query->first()) {
            throw new AuthException('Пользователь с такими данными не найден.');
        }

        /*
         * Check the hashed credentials match
         */
        foreach ($hashedCredentials as $credential => $value) {

            if (!$user->checkHashValue($credential, $value)) {
                // Incorrect password
                if ($credential == 'password') {
                    throw new AuthException('Неверный пароль');
                }

                // User not found
                throw new AuthException('Пользователь с такими данными не найден.');
            }
        }

        return $user;
    }

    /**
     * Find a throttle record by login and ip address
     */
    public function findThrottleByLogin($loginName, $ipAddress)
    {
        $user = $this->findUserByLogin($loginName);
        if (!$user) {
            throw new AuthException('Пользователь с такими данными не найден.');
        }

        $userId = $user->getKey();
        return $this->findThrottleByUserId($userId, $ipAddress);
    }

    /**
     * @inheritDoc
     */
    public function logout()
    {
        /** @var Authenticatable|null $user */
        $user = $this->user;

        parent::logout();

        try {
            app('events')->dispatch(new Logout($user));
        } catch (\Exception $e) {}
    }

    /**
     * @return string
     */
    public function getSessionKey()
    {
        return $this->sessionKey;
    }
}
