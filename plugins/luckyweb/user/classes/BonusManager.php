<?php namespace LuckyWeb\User\Classes;


use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use LuckyWeb\User\Classes\Notifier\AccountNotificationManager;
use LuckyWeb\User\Models\BonusEvent;
use LuckyWeb\User\Models\User;

class BonusManager
{
    /**
     * Apply bonuses
     * @param User $user
     * @param float $amount
     * @param null|Carbon $expiredAt
     */
    public function apply(User $user,  $amount, Carbon $expiredAt = null)
    {
        //Create bonus event ready to be exported to 1C
        BonusEvent::create([
            'type_id' => BonusEvent::TYPE_ACTIVATION,
            'user_id' => $user->id,
            'amount' => $amount,
            'description' => 'Бонусы за регистрацию',
            'applied_at' => Carbon::now(),
            'expired_at' => $expiredAt
        ]);

        // Email to manager instead of auto apply via 1C
        $receiver = env('BONUS_APPLY_MAIL');
        if(!empty($receiver)) {
            $data = [
                'user' => $user->toArray(),
                'invite' => [
                    'amount' => $amount,
                    'expired_at' => $expiredAt
                ]
            ];

            Mail::queue('luckyweb.user::mail.manager.bonus_apply', $data, function ($message) use ($receiver) {
                $message->to($receiver);
            });
        }

        $notifier = new AccountNotificationManager($user);
        $notifier->notify(AccountNotificationManager::TYPE_BONUS_APPLY, [
            'amount' => $amount,
            'expired_at' => $expiredAt
        ]);
    }
}