<?php namespace LuckyWeb\User\Classes;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Laravel\Socialite\AbstractUser;
use Luckyweb\Ms\Models\FavoriteList;
use LuckyWeb\User\Models\User;

class SocialAuthManager
{
    /**
     * @var AbstractUser
     */
    protected $socialUser;

    /**
     * @var string
     */
    protected $socialProviderId;

    /**
     * SocialAuthManager constructor.
     * @param AbstractUser $socialUser
     * @param string $socialProviderId
     */
    public function __construct(AbstractUser $socialUser, $socialProviderId)
    {
        $this->socialUser = $socialUser;
        $this->socialProviderId = $socialProviderId;
    }

    /**
     * Finds and returns the user attached to the given provider. If one doesn't
     * exist, it's created. If one exists but isn't attached, it's attached.
     * @return User
     */
    public function handle()
    {
        // Are we already attached?
        $user = $this->findUser();

        if (!$user) {
            // Try to get user by unique fields
            $user = $this->findUserByUniqueFields();

            // No user with this email exists - create one
            if (!$user) {
                // Register the user
                $user = $this->registerUser();
                $this->sendSuccessRegistrationMail($user);
            }
            else {
                $user = $this->attachProvider($user);
            }
        } // Provider was found, return the attached user

        return $user;
    }

    /**
     * Attach a provider to a user
     * @param User $user
     * @return User
     */
    protected function attachProvider(User $user)
    {
        $user->social_provider_id = $this->socialProviderId;
        $user->social_id = $this->socialUser->getId();
        $user->save();

        return $user;
    }

    /**
     * Register a new user with given details and attach a provider to them.
     * @return User
     */
    protected function registerUser()
    {
        User::unguard();

        $password = Hash::make(str_random(16));
        $user = User::create([
            'name' => $this->socialUser->getName(),
            'email' => $this->getEmail(),
            'password' => $password,
            'password_confirmation' => $password,
            'is_guest' => true
        ]);
        User::reguard();

        $fList = new FavoriteList();
        $fList->user_id = $user->getKey();
        $fList->list_name = 'По умолчанию';
        $fList->save();

        return $this->attachProvider($user);
    }

    /**
     * Looks for a provider with given provider ID and token
     * @return User|null
     */
    protected function findUser()
    {
        return User::where('social_provider_id', $this->socialProviderId)
            ->where('social_id', $this->socialUser->getId())
            ->where('is_activated', true)
            ->first();
    }

    /**
     * Try to get user by given unique fields
     * @return User|null
     */
    protected function findUserByUniqueFields()
    {
        $user = null;
        $email = $this->getEmail();
        if(!empty($email)) {
            $user = User::where('email', $email)->first();
        }
        return $user;
    }

    /**
     * @return null|string
     */
    protected function getEmail()
    {
        return $this->socialUser->getEmail() ?? data_get($this->socialUser, 'accessTokenResponseBody.email');
    }

    /**
     * Send success registration mail to user
     * @param User $user
     */
    protected function sendSuccessRegistrationMail(User $user)
    {
        // Send email to client
    }
}
