<?php namespace LuckyWeb\User\Components;

use Cms\Classes\ComponentBase;
use Exception;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use LuckyWeb\User\Facades\Auth;
use October\Rain\Exception\ValidationException;
use October\Rain\Support\Facades\Flash;
use Illuminate\Support\Facades\Session;
use LuckyWeb\MS\Models\FavoriteListItem;
use System\Classes\CombineAssets;
use LuckyWeb\User\Models\BonusInvite;
use LuckyWeb\User\Classes\BonusManager;

class SignIn extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'SignIn Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->addCss(CombineAssets::combine([
            '/themes/shara/assets/vendor/animate/animate.css'
        ], base_path()));

        $this->addJs(CombineAssets::combine([
            '/themes/shara/assets/js/plugins/jquery.maskedinput/jquery.maskedinput.min.js',
            '/themes/shara/assets/vendor/bootstrap-notify/bootstrap-notify.min.js',
            '/plugins/luckyweb/user/components/signin/assets/js/sign_in.js'
        ], base_path()), ['async']);

        $this->page['user'] = Auth::getUser();
    }

    /**
     * Show sign in form
     * @return array
     */
    public function onSignInPopup()
    {
        return ['.js-signin_modal .modal-body' => $this->renderPartial('signIn::popup')];
    }

    /**
     * Sign in user
     */
    public function onSignIn()
    {
        try {
            /*
             * Validate input
             */
            $data = post();

            if (array_key_exists('card_code', $data) && $data['card_code'] != '') {
                // append leading zeros to card number
                $multiplier = Config::get('luckyweb.ms::account_card_length') - mb_strlen($data['card_code']);
                $data['card_code'] = str_repeat("0", $multiplier > 0 ? $multiplier : 0).$data['card_code'];
            }

            switch (post('type')) {
                case 1:
                    $rules = [
                        'phone' => ['required', 'regex:/^7[0-9]{10}$/'],
                        'password' => 'required|between:4,255'
                    ];
                    $data['phone'] = preg_replace('/[^0-9]+/', '', $data['phone']);
                    break;
                case 2:
                    $rules = [
                        'card_code' => 'required|string|min:1|max:'.Config::get('luckyweb.ms::account_card_length'),
                        'card_pin' => 'required|digits:4'
                    ];
                    break;
                case 3:
                    $rules = [
                        'email' => 'required|email|between:6,255',
                        'password' => 'required|between:4,255'
                    ];
                    break;
                default:
                    throw new Exception('Что-то пошло не так! Перезагрузите страницу и попробуйте снова.');
            }

            $attributes = [
                'phone' => '"Номер телефона"',
                'email' => '"Email"',
                'password' => '"Пароль"',
                'card_code' => '"Номер карты"',
                'card_pin' => '"PIN"'
            ];

            $validation = Validator::make($data, $rules, [], $attributes);

            if ($validation->fails()) {
                throw new ValidationException($validation);
            }

            /*
             * Authenticate user
             */
            $credentials = array_only($data, [
                'email', 'phone', 'password', 'card_code', 'card_pin'
            ]);

            Event::fire('luckyweb.user.beforeAuthenticate', [$this, $credentials]);

            $user = Auth::authenticate($credentials, true, array_get($data, 'type'));

            if ($user && $user->name != '') {
                Session::flash('showWelcomeMessage', true);
            }

            FavoriteListItem::fillFromCookie();

            // Redirect when current page is cart
            if ($_SERVER['REQUEST_URI'] == '/cart') {
                return redirect('/cart');
            }

            /*
             * Redirect to account page
             */
            return Redirect::to($this->pageUrl($user->isFirstVisit() ? 'account' : 'account-orders'));
        } catch (Exception $ex) {
            if (Request::ajax())
                throw $ex;
            else
                Flash::error($ex->getMessage());
        }
    }

    /**
     * Logout user
     * @return mixed
     */
    public function onLogout()
    {
        Auth::logout();
        return Redirect::to(url('/'));
    }

    /**
     * Return url to social provider
     * @param $code
     * @return string
     */
    public function getProviderUrl($code)
    {
        return url('/auth/'.$code);
    }
}
