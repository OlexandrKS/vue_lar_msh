<?php namespace LuckyWeb\User\Components;

use LuckyWeb\User\Facades\Auth;
use Mail;
use System\Classes\CombineAssets;
use Validator;
use ValidationException;
use ApplicationException;
use Cms\Classes\ComponentBase;
use LuckyWeb\User\Models\User as UserModel;

class ResetPassword extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'luckyweb.user::lang.reset_password.reset_password',
            'description' => 'luckyweb.user::lang.reset_password.reset_password_desc'
        ];
    }

    public function defineProperties()
    {
        return [
            'paramCode' => [
                'title'       => 'luckyweb.user::lang.reset_password.code_param',
                'description' => 'luckyweb.user::lang.reset_password.code_param_desc',
                'type'        => 'string',
                'default'     => 'code'
            ]
        ];
    }

    public function onRun()
    {
        $this->addCss(CombineAssets::combine([
            '/themes/shara/assets/vendor/animate/animate.css'
        ], base_path()));

        $this->addJs(CombineAssets::combine([
            '/themes/shara/assets/vendor/bootstrap-notify/bootstrap-notify.min.js',
            '/plugins/luckyweb/user/components/resetpassword/assets/js/reset_password.js'
        ], base_path()), ['async']);
    }

    /**
     * Show reset password form
     */
    public function onShowRestore()
    {
        return ['.js-signin_modal .modal-body' => $this->renderPartial('resetPassword::popup')];
    }

    /**
     * Trigger the password reset email
     */
    public function onRestorePassword()
    {
        $rules = [
            'email' => 'required|email|between:6,255'
        ];

        $validation = Validator::make(post(), $rules);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        if (!$user = UserModel::findByEmail(post('email'))) {
            throw new ApplicationException(trans('luckyweb.user::lang.account.invalid_user'));
        }

        $code = implode('!', [$user->id, $user->getResetPasswordCode()]);
        $link = $this->pageUrl('account').'/'.$code.'#restore-password';

        $data = [
            'name' => $user->name,
            'link' => $link,
            'code' => $code
        ];

        Mail::send('luckyweb.user::mail.restore', $data, function($message) use ($user) {
            $message->to($user->email, $user->full_name);
        });
    }

    /**
     * Perform the password reset
     */
    public function onResetPassword()
    {
        $rules = [
            'code'     => 'required',
            'password' => 'required|between:4,255'
        ];

        $validation = Validator::make(post(), $rules);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        /*
         * Break up the code parts
         */
        $parts = explode('!', post('code'));
        if (count($parts) != 2) {
            throw new ValidationException(['code' => trans('luckyweb.user::lang.account.invalid_activation_code')]);
        }

        list($userId, $code) = $parts;

        if (!$code || !strlen(trim($code))) {
            throw new ApplicationException(trans('rainlab.user::lang.account.invalid_activation_code'));
        }

        if (!strlen(trim($userId)) || !($user = Auth::findUserById($userId))) {
            throw new ApplicationException(trans('luckyweb.user::lang.account.invalid_user'));
        }

        if (!$user->attemptResetPassword($code, post('password'))) {
            throw new ValidationException(['code' => trans('luckyweb.user::lang.account.invalid_activation_code')]);
        }

        Auth::login($user);
    }

    /**
     * Returns the reset password code from the URL
     * @return string
     */
    public function code()
    {
        $routeParameter = $this->property('paramCode');

        return $this->param($routeParameter);
    }
}