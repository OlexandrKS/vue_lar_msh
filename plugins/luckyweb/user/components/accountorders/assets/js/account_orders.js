var AccountOrders = function () {

    var initExpandButton = function(){
        $('.js-order_list').on('click', '.js-expand_button', function(e){
            e.preventDefault();
            var $this = $(this);
            var ids = $this.data('id').toString().split(',');
            var $order = $this.parents('.js-order');

            if($this.hasClass('active')) {
                $this.removeClass('active').find('>.icon').removeClass('fa-flip-vertical');
                $.each(ids, function(index, value){
                    $order.find('.js-expand_block[data-id='+value+']').slideUp();
                    $order.find('.js-expand_button[data-id='+value+']')
                        .removeClass('active').find('>.icon').removeClass('fa-flip-vertical');
                });
            }
            else {
                $this.addClass('active').find('>.icon').addClass('fa-flip-vertical');
                $.each(ids, function(index, value){
                    if(value == 1) {
                        $('.js-order_list .js-expand_block[data-id='+value+']').slideUp();
                        $('.js-order_list .js-expand_button[data-id='+value+']')
                            .removeClass('active').find('>.icon').removeClass('fa-flip-vertical');
                    }

                    $order.find('.js-expand_block[data-id='+value+']').slideDown();
                    $order.find('.js-expand_button[data-id='+value+']')
                        .addClass('active').find('>.icon').addClass('fa-flip-vertical');
                });
            }
        });
    };

    return {
        init: function () {
            initExpandButton();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    AccountOrders.init();
});
