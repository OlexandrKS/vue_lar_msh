<?php namespace LuckyWeb\User\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use LuckyWeb\User\Facades\Auth;
use LuckyWeb\User\Models\Order;
use LuckyWeb\User\Models\OrderItem;
use System\Classes\CombineAssets;

class AccountOrders extends ComponentBase
{
    /**
     * List of orders
     * @var mixed
     */
    public $orders;

    public function componentDetails()
    {
        return [
            'name'        => 'Orders Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'pageNumber' => [
                'title'       => 'Номер страницы',
                'description' => 'Номер страницы для пагинации',
                'type'        => 'string',
                'default'     => '{{ :page }}',
            ],
            'postsPerPage' => [
                'title'             => 'Количество отзывов',
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'Количество отзывов на странице',
                'default'           => '10',
            ],
        ];
    }

    public function onRun()
    {
        $this->addJs(CombineAssets::combine([
            '/plugins/luckyweb/user/components/accountorders/assets/js/account_orders.js'
        ], base_path()));

        $this->orders = Order::where('user_id', Auth::getUser()->id)
            ->where('status_updated_at', '>', Carbon::now()->subDays(env('SUCCESS_ORDER_DISPLAY_DAYS', 7))->toDateTimeString())
            ->orderBy('id', 'desc')
            ->paginate($this->property('postsPerPage'), $this->property('pageNumber'));
    }

    /**
     * Format and localize date
     * @param $date
     * @return string
     */
    public function getLocalizedDate($date)
    {
        $month = [
            1 => 'января',
            'февраля',
            'марта',
            'апреля',
            'мая',
            'июня',
            'июля',
            'августа',
            'сентября',
            'октября',
            'ноября',
            'декабря'
        ];

        return str_replace('m', $month[$date->month], $date->format('d \\m Y'));
    }

    /**
     * Prepare products string for order info
     * @param $order
     * @return string
     */
    public function getProductsString($order)
    {
        $calc = ['quantity' => 0, 'amount' => 0];
        $order->items->each(function ($item) use(&$calc) {
            $calc['quantity'] += $item->quantity;
            $calc['amount'] += $item->amount;
        });

        return $calc['quantity'].' '
            .trans_choice('{0} товаров|{1} товар|[2,4] товара|[5,Inf] товаров', intval($calc['quantity'])).' на '
            .number_format($calc['amount'], 0, '.', ' ');
    }

    /**
     * Return proper image name for given product
     * @param $product
     * @return string
     */
    public function getProductImageName($product)
    {
        $name = explode(' ', $product['name']);
        if($name[0] == 'SF') {
            return 'sofos';
        }
        elseif($name[0] == 'IMP') {
            return 'imperial';
        }

        return 'ms';
    }

    /**
     * Return status label
     * @param $statusId
     * @return mixed
     */
    public function getStatusTitle($statusId)
    {
        return OrderItem::getStatusLabels($statusId)[1];
    }

    /**
     * Prepare status array for given current status
     * @param $statusId
     * @return static
     */
    public function prepareStatusArray($statusId)
    {
        $statuses = [
            [
                'id' => OrderItem::STATUS_PRODUCTION,
                'icon' => 'production'
            ],
            ($statusId == OrderItem::STATUS_WAIT_PAYMENT
                ? [
                    'id' => OrderItem::STATUS_WAIT_PAYMENT,
                    'icon' => 'shipment_wait',
                ]
                : [
                    'id' => OrderItem::STATUS_SHIPMENT,
                    'icon' => 'shipment'
                ]
            ),
            [
                'id' => OrderItem::STATUS_DELIVERING,
                'icon' => 'delivering'
            ],
            [
                'id' => OrderItem::STATUS_DELIVERY_COORDINATION,
                'icon' => 'coordination'
            ],
            [
                'id' => OrderItem::STATUS_DELIVERED,
                'icon' => 'delivered'
            ],
        ];

        $isActivated = false;
        return collect($statuses)->reverse()->map(function($item) use($statusId, &$isActivated) {
            $item['name'] = $this->getStatusTitle($item['id']);
            $item['description'] = OrderItem::getStatusLabels($item['id'])[2];
            if($isActivated) {
                $item['active'] = true;
            }
            elseif($item['id'] == $statusId) {
                $item['active'] = true;
                $isActivated = true;
            }
            return $item;
        })->reverse()->toArray();
    }

    /**
     * Calculate pagination first range
     * @param mixed $items
     * @return int
     */
    public function getPaginationRangeFirst($items)
    {
        $offset = $items->lastPage() - $items->currentPage() < 2
            ? $items->currentPage()-$items->lastPage()+4
            : 2;

        return $items->currentPage()-$offset > 0 ? $items->currentPage()-$offset : 1;
    }

    /**
     * Calculate pagination last range
     * @param mixed $items
     * @return int
     */
    public function getPaginationRangeLast($items)
    {
        $offset = $items->currentPage() < 3
            ? 5 - $items->currentPage()
            : 2;

        return $items->currentPage()+$offset < $items->lastPage()
            ?  $items->currentPage()+$offset
            : $items->lastPage();
    }
}
