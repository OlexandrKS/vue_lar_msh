<?php namespace Luckyweb\User\Components;

use App\ServiceLayer\Sentry\Sentry;
use Cms\Classes\ComponentBase;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\URL;
use LuckyWeb\MS\Models\AdminSettings;
use LuckyWeb\User\Facades\Auth;

class ProdBoard extends ComponentBase
{

    protected $authToken;

    public function componentDetails()
    {
        return [
            'name' => 'ProdBoard Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {

    }

    public function onSignInProdBoard()
    {
        if (Auth::check()) {
            $user = Auth::getUser();
            // identity mebelShara9674
            $identity = 'mebelShara' . $user->id;
            $responseToken = $this->clientToken($identity);

            if ($responseToken == null) {
                $client = new Client;

                $response = $client->post('https://api.prodboard.com/api/clients.update', [
                    'headers' => [
                        'api_key' => config('prodboard.api_key'),
                    ],
                    'form_params' => [
                        'identity' => $identity,
                        'email' => $user->email,
                        'phone' => $user->phone,
                        'name' => $user->name
                    ],
                ]);

                $response = json_decode($response->getBody(), true);

                if ($response['success']) {
                    // Лид ProdBoard
                    $data = [
                        'id' => $user->id,
                        'name' => $user->name,
                        'email' => $user->email,
                        'phone' => $user->phone,
                    ];

                    Event::fire('luckyweb.prodboard.register', [$data]);

                    $responseToken = $this->clientToken($identity);
                } else {
                    Sentry::captureException($this->getResponseException($response));
                }
            }

            return Redirect::to(URL::to('/') . '/planner#token-' . $responseToken);
        }

        return Redirect::to(Request::getPathInfo() . '/#registration');
    }

    /**
     * @param $response
     * @return \Exception
     */
    private function getResponseException($response)
    {
        return new class ($response) extends \Exception {
            public $response;

            public function __construct($response)
            {
                $this->response = $response;

                parent::__construct("ProdBoard registration error");
            }
        };
    }

    protected function clientToken($identity)
    {
        $client = new Client;
        $response = $client->post('https://api.prodboard.com/api/clients.token', [
            'headers' => [
                'api_key' => config('prodboard.api_key'),
            ],
            'form_params' => ['identity' => $identity],
        ]);
        return json_decode($response->getBody(), true);
    }
}
