<?php namespace Luckyweb\User\Components;

use Cms\Classes\ComponentBase;
use LuckyWeb\User\Facades\Auth;
use LuckyWeb\User\Models\PromoCodeEvent;

class PromoCode extends ComponentBase
{
    public $promoCodes;

    public function componentDetails()
    {
        return [
            'name' => 'PromoCode Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'code' => [
                'title' => 'Promo code',
                'description' => 'Promo code string for activation',
                'type' => 'string',
            ]
        ];
    }

    public function onRun()
    {
        if (Auth::check()) {
            $user = Auth::getUser();
            $this->promoCodes = PromoCodeEvent::where('user_id', '=', $user->id)->with(['promo_code'])->get();
        }
    }
}
