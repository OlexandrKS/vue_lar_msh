<?php namespace LuckyWeb\User\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use LuckyWeb\User\Facades\Auth;
use LuckyWeb\User\Models\User;
use October\Rain\Exception\ValidationException;
use System\Classes\CombineAssets;
use LuckyWeb\User\Models\Settings as UserSettings;
use LuckyWeb\MS\Models\FavoriteList;
use LuckyWeb\MS\Models\FavoriteListItem;
use Illuminate\Support\Facades\Mail;
use LuckyWeb\User\Classes\BonusManager;
use LuckyWeb\User\Models\BonusInvite;

class SocialConfirmation extends ComponentBase
{
    /**
     * @var User
     */
    public $user;


    public function componentDetails()
    {
        return [
            'name'        => 'SocialConfirmation Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->retrieveUser();

        // Not registered eat
        if(!$this->user) {
            return Redirect::to($this->pageUrl('home'));
        }

        $this->addJs(CombineAssets::combine([
            '/plugins/luckyweb/user/components/socialconfirmation/assets/js/social_confirmation.js',
            '/themes/shara/assets/js/plugins/jquery.maskedinput/jquery.maskedinput.min.js',
        ], base_path()));
    }

    /**
     * User confirm his data
     */
    public function onConfirm()
    {
        try {
            if (!UserSettings::get('allow_registration', true)) {
                throw new ApplicationException(trans('luckyweb.user::lang.account.registration_disabled'));
            }

            $this->retrieveUser();

            // ID of user find by card.
            /*if(empty(Session::get('user_id'))) {
                throw new Exception('Произошла ошибка. Повторите регистрацию снова.');
            }*/

            /*
             * Validate input
             */
            $data = post();
            $data['phone'] = preg_replace('/[^0-9]+/', '', $data['phone']);

            $rules = [
                'name' => 'required',
                'last_name' => 'required',
                'phone' => ['required', 'regex:/^7[0-9]{10}$/', 'unique:users,phone,'.$this->user->id],
                'email' => 'required|email|between:6,255|unique:users,email,'.$this->user->id,
                'password' => 'required|between:4,255|confirmed',
                'password_confirmation' => 'required',
                'policy_accept' => 'accepted'
            ];


            $attributes = [
                'name' => '"Имя"',
                'last_name' => '"Фамилия"',
                'phone' => '"Телефон"',
                'email' => '"Email"',
                'password' => '"Пароль"',
                'password_confirmation' => '"Подтверждение пароля"',
                'policy_accept' => '"Политику конфиденциальности"'
            ];

            $validation = Validator::make($data, $rules, [], $attributes);
            if ($validation->fails()) {
                throw new ValidationException($validation);
            }

            $this->user->generateCard();

            Auth::convertGuestToUser($this->user, $data, true);
            Auth::login($this->user);

            Session::forget('social_confirm_id');

            // Apply bonuses for bonus-invite promotion
            if($bonusId = Session::pull('bonus_invite_id')){
                $bonusInvite = BonusInvite::where('id', $bonusId)
                    ->where('status_id', BonusInvite::STATUS_ENABLED)
                    ->first();

                if($bonusInvite) {
                    (new BonusManager())->apply($user, $bonusInvite->amount, $bonusInvite->expired_at);
                }
            }
            
            return Redirect::to($this->pageUrl($this->user->isFirstVisit() ? 'account' : 'account-orders'));
        }
        catch (Exception $ex) {
            if (Request::ajax()) throw $ex;
            else Flash::error($ex->getMessage());
        }
    }


    /**
     * Sends the activation email to a user
     * @param  User $user
     * @return void
     */
    protected function sendActivationEmail($user)
    {
        $code = implode('!', [$user->id, $user->getActivationCode()]);
        $link = $this->pageUrl('account').'/'.$code;

        $data = [
            'name' => $user->name,
            'link' => $link,
            'code' => $code
        ];

        Mail::send('luckyweb.user::mail.activate', $data, function($message) use ($user) {
            $message->to($user->email, $user->name);
        });
    }


    /**
     * Retrieve current user
     */
    protected function retrieveUser()
    {
        $this->user = User::where('id', Session::get('social_confirm_id'))
            ->where('is_activated', false)
            ->whereNotNull('social_id')
            ->first();
    }
}
