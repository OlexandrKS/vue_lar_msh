var SignIn = function () {
    var $ = jQuery;

    var initPopupButton = function(){
        $('body').on('click', '.js-signin_button', function(e){
            e.preventDefault();

            $.request('signIn::onSignInPopup', {
                success: function(resp, status, err) {
                    this.success(resp, status, err);
                    $('.js-signin_modal').modal('show');
                   // $("[name='phone']").mask('+7 999 999 99 99');
                }
            })
        });
    };

    var initSignInForm = function() {
        $('.js-signin_modal').on('submit', '.js-signin_form', function(e){
            e.preventDefault();

            var $form = $(this);
            $form.find("button[type='submit']").prop('disabled', true);
            resetErrors($form);
            $form.request('signIn::onSignIn', {
                error: function(resp, status, err) {
                    $form.find("button[type='submit']").prop('disabled', false);
                    if (typeof resp.responseJSON != 'undefined') {
                        displayErrors($form, resp.responseJSON.X_OCTOBER_ERROR_FIELDS);
                    }
                    else {
                        this.error(resp, status, err);
                    }
                },
                success: function (resp, status, err) {
                    var $this = this

                    window.afterSignInCallback = window.afterSignInCallback || function () {};

                    $.when(window.afterSignInCallback())
                        .then(function () {
                            $this.success(resp, status, err);
                        })
                }
            });
        });
    };

    var initCustomErrorHandler = function(){
        $(window).on('ajaxErrorMessage', function(event, message){
            event.preventDefault();
            $.notify(message, {
                    type: "danger",
                    placement: {
                        align: 'center'
                    }
                }
            );
        })
    };

    var resetErrors = function($form) {
        $form.find('.warning-block').each(function () {
            $(this).hide();
        });
    };

    var displayErrors = function($form, errors) {
        $.each(errors, function (field, message) {
            var $wb = $form.find("[name='"+field+"']").siblings('.warning-block');
            $wb.find('span').html(message[0]);
            $wb.show();
        });
    };

    return {
        init: function () {
            initPopupButton();
            initSignInForm();
            initCustomErrorHandler();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    SignIn.init();
});
