var ResetPassword = function () {
    var $ = jQuery;

    var initPopupButton = function(){
        $('body').on('click', '.js-reset_password_button', function(e){
            e.preventDefault();

            $.request('resetPassword::onShowRestore', {
                success: function(resp, status, err) {
                    this.success(resp, status, err);
                    $('.js-signin_modal').modal('show');
                }
            })
        });
    };

    var initRestoreForm = function() {
        $('.js-signin_modal').on('submit', '.js-restore_form', function(e){
            e.preventDefault();

            var $form = $(this);
            $form.find("button[type='submit']").prop('disabled', true);
            resetErrors($form);
            $form.request('resetPassword::onRestorePassword', {
                update: {'resetPassword::reset': '#partialUserResetForm'},
                error: function(resp, status, err) {
                    $form.find("button[type='submit']").prop('disabled', false);
                    if (typeof resp.responseJSON != 'undefined'){
                        displayErrors($form, resp.responseJSON.X_OCTOBER_ERROR_FIELDS);
                    }
                    else {
                        this.error(resp, status, err);
                    }
                }
            });
        });
    };

    var initResetForm = function() {
        $('.js-signin_modal').on('submit', '.js-reset_form', function(e){
            e.preventDefault();

            var $form = $(this);
            $form.find("button[type='submit']").prop('disabled', true);
            resetErrors($form);
            $form.request('resetPassword::onResetPassword', {
                update: {'resetPassword::complete': '#partialUserResetForm'},
                error: function(resp, status, err) {
                    $form.find("button[type='submit']").prop('disabled', false);
                    if (typeof resp.responseJSON != 'undefined'){
                        displayErrors($form, resp.responseJSON.X_OCTOBER_ERROR_FIELDS);
                    }
                    else {
                        this.error(resp, status, err);
                    }
                }
            });
        });
    };

    var resetErrors = function($form) {
        $form.find('.warning-block').each(function () {
            $(this).hide();
        });
    };

    var displayErrors = function($form, errors) {
        $.each(errors, function (field, message) {
            var $wb = $form.find("[name='"+field+"']").siblings('.warning-block');
            $wb.find('span').html(message[0]);
            $wb.show();
        });
    };

    return {
        init: function () {
            initPopupButton();
            initRestoreForm();
            initResetForm();

            // User came from restore email. Show reset form
            if(location.hash == '#restore-password') {
                $.request('resetPassword::onShowRestore', {
                    success: function(resp, status, err) {
                        this.success(resp, status, err);
                        $('.js-signin_modal').modal('show');
                    }
                })
            }
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    ResetPassword.init();
});