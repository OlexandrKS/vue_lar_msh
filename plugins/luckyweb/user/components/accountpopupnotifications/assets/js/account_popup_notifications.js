var AccountPopupNotifications = function () {
    var $ = jQuery;

    var showPopup = function() {
        $('.js-notification_popup').modal('show');
    };

    return {
        init: function () {
            showPopup();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    AccountPopupNotifications.init();
});