<?php namespace LuckyWeb\User\Components;

use Cms\Classes\ComponentBase;
use LuckyWeb\User\Facades\Auth;
use LuckyWeb\User\Models\BonusEvent;
use System\Classes\CombineAssets;

class AccountBonuses extends ComponentBase
{
    /**
     * List of bonus events
     * @var mixed
     */
    public $bonusEvents;

    public function componentDetails()
    {
        return [
            'name'        => 'AccountBonuses Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $authUser = Auth::getUser();
        if ($authUser) {
            $this->bonusEvents = BonusEvent::where('user_id', $authUser->id)
                ->whereNull('exported_at')
                ->orderBy('id', 'desc')->get();
        }

        $this->addJs(CombineAssets::combine([
            '/plugins/luckyweb/user/components/accountbonuses/assets/js/account_bonuses.js'
        ], base_path()));
    }

    /**
     * Filter bonus list
     */
    public function onFilter()
    {
        $authUser = Auth::getUser();
        if ($authUser) {
            $query = BonusEvent::where('user_id', $authUser->id)
                ->whereNull('exported_at');
            if(post('typeId')) {
                $query->where('type_id', post('typeId'));
            }

            $this->bonusEvents = $query->orderBy('id', 'desc')->get();
        }

        return ['.js-bonuses_list' => $this->renderPartial('accountBonuses::bonuses', ['bonusEvents' => $this->bonusEvents])];
    }

    /**
     * Return class for given event type
     * @param int $type
     * @return string
     */
    public function getBonusEventTypeClass($type)
    {
        switch($type) {
            case BonusEvent::TYPE_ACCRUAL:
                return 'accrued';
            case BonusEvent::TYPE_ACTIVATION:
                return 'activated';
            case BonusEvent::TYPE_CHARGE:
                return 'charged';
            case BonusEvent::TYPE_DEACTIVATION:
                return 'deactivated';
        }
    }

    public function formatEventAmount($event)
    {
        $amount = (int)($event->isNegative() ? $event->amount *-1 : $event->amount);
        return ($amount < 0 ? '' : '+').$amount;
    }
}
