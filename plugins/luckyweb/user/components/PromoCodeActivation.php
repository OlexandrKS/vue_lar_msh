<?php

namespace LuckyWeb\User\Components;

use Cms\Classes\ComponentBase;
use Exception;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use LuckyWeb\User\Facades\Auth;
use LuckyWeb\User\Models\PromoCode;
use Illuminate\Support\Facades\Session;
use October\Rain\Exception\ValidationException;
use October\Rain\Support\Facades\Flash;
use System\Classes\CombineAssets;

class PromoCodeActivation extends ComponentBase
{
    public $email;

    public function componentDetails()
    {
        return [
            'name' => 'PromoCodeActivation Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'code' => [
                'title' => 'Promo code',
                'description' => 'Promo code string for activation',
                'type' => 'string',
            ]
        ];
    }

    public function onRun()
    {
        //eyJpdiI6IkhidFwvTGYxSzd1clZBbVRhXC9WdXVEdz09IiwidmFsdWUiOiJJclFRZGZoanNLd2hwaUROQURtKythV3VoOXNySVdnK0pUZXJmOTI3dlwvZz0iLCJtYWMiOiI5ZGYzOTA5Njc2ZjdmYzlhOWEyYTQyMTU3NDM4ZmQzZmRkNjY4N2QwZWZkZDcxY2M0MTcxMjMyZDcyYzQ0YTNmIn0%3D
        $this->addJs(CombineAssets::combine([
            '/plugins/luckyweb/user/components/promocodeactivation/assets/js/promo_code_activation.js',
            '/themes/shara/assets/js/plugins/jquery.maskedinput/jquery.maskedinput.min.js'
        ], base_path()), ['async']);

        if (!($code = $this->property('code')) || !($email_code = $this->property('e'))) {
            return $this->controller->run('404');
        }

        if (Auth::check()) {
            $this->setPromoCodesForUser($code);

            return Redirect::to($this->pageUrl('account-promo-codes'));
        } else {
            $this->email = ($email_code != 'email') ? base64_decode($email_code) : '';
            $this->page['response'] = $this->renderPartial('@registered_user.htm');
        }
    }

    public function onSignUp()
    {
        if (!($code = $this->property('code'))) {
            return $this->controller->run('404');
        }

        try {
            $data = post();
            $data['phone'] = preg_replace('/[^0-9]+/', '', $data['phone']);
            $rules = [
                'name' => 'required',
                'last_name' => 'required',
                'phone' => ['required', 'regex:/^7[0-9]{10}$/', 'unique:users,phone,' . Session::get('user_id')],
                'email' => 'required|email|between:6,255|unique:users,email,' . Session::get('user_id'),
                'password' => 'required|between:4,255|confirmed',
                'password_confirmation' => 'required',
                'policy_accept' => 'accepted'
            ];

            $attributes = [
                'name' => '"Имя"',
                'last_name' => '"Фамилия"',
                'phone' => '"Телефон"',
                'email' => '"Email"',
                'password' => '"Пароль"',
                'password_confirmation' => '"Подтверждение пароля"',
                'policy_accept' => '"Политику конфиденциальности"'
            ];

            $validation = Validator::make($data, $rules, [], $attributes);
            if ($validation->fails()) {
                throw new ValidationException($validation);
            } else {
                $user = Auth::register($data, false);

                // Generate site version of the card
                $user->generateCard();
                $user->save();
                $user->attemptActivation($user->getActivationCode());

                Auth::login($user);
                $this->setPromoCodesForUser($code);

                return Redirect::to($this->pageUrl('account-promo-codes'));

            }
        } catch (Exception $ex) {
            if (Request::ajax()) throw $ex;
            else Flash::error($ex->getMessage());
        }
    }

    /**
     * @return bool|string
     */
    public function onSignIn()
    {
        if (!($code = $this->property('code'))) {
            return $this->controller->run('404');
        }

        return $this->setPromoCodesForUser($this->property('code'));
    }

    protected function setPromoCodesForUser($code)
    {
        /** @var PromoCode $promoCode */
        $promoCode = PromoCode::where('code', $code)->first();

        if (!$promoCode || !Auth::check())
            return false;

        return $promoCode->activate(Auth::getUser());
    }

    /**
     * @param $code
     * @return string
     */
    protected function decodeEmail($code)
    {
        $qDecoded = urldecode(decrypt($code));

        return ($qDecoded);
    }
}
