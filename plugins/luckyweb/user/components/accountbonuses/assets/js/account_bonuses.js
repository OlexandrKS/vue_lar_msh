var AccountBonuses = function () {

    var initPopovers = function () {
        $('[data-toggle="infobox"]').popover({
            html: true,
            trigger: 'hover'
        });
    };

    var initBonusFilter = function(){
        $('.js-bonus_filter input').on('change', function(){
            $.request('accountBonuses::onFilter', {
                data: {
                    typeId: $(this).val()
                }
            })
        });
    };

    return {
        init: function () {
            initPopovers();
            initBonusFilter();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    AccountBonuses.init();
});
