var SignUp = function () {
    var $ = jQuery;

    var initPopupButton = function () {
        $('body').on('click', '.js-signup_button', function (e) {
            e.preventDefault();

            $.request('signUp::onSignUpPopup', {
                success: function (resp, status, err) {
                    this.success(resp, status, err);
                    $('.js-signup_modal').modal('show');
                    $('.js-signin_modal').modal('hide');
                }
            })
        });
    };

    var initHashUrlRegistration = function () {
        $(function () {
            if (window.location.hash == '#registration') {
                $.request('signUp::onSignUpPopup', {
                    success: function (resp, status, err) {
                        this.success(resp, status, err);
                        $('.js-signup_modal').modal('show');
                        $('.js-signin_modal').modal('hide');
                    }
                })
            }
        })
    };

    var initSignUpForm = function () {
        $('.js-signup_modal').on('submit', '.js-signup_form', function (e) {
            e.preventDefault();

            var $form = $(this);
            $form.find("button[type='submit']").prop('disabled', true);
            resetErrors($form);
            $form.request('signUp::onSignUp', {
                error: function (resp, status, err) {
                    $form.find("button[type='submit']").prop('disabled', false);
                    if (typeof resp.responseJSON != 'undefined') {
                        displayErrors($form, resp.responseJSON.X_OCTOBER_ERROR_FIELDS);
                    }
                    else {
                        this.error(resp, status, err);
                    }
                }
            });
        });
    };

    var initCardForm = function () {
        $('.js-signup_modal').on('submit', '.js-card_form', function (e) {
            e.preventDefault();

            var $form = $(this);
            $form.find("button[type='submit']").prop('disabled', true);
            resetErrors($form);
            $form.request('signUp::onCardSearch', {
                error: function (resp, status, err) {
                    $form.find("button[type='submit']").prop('disabled', false);
                    if (typeof resp.responseJSON != 'undefined') {
                        displayErrors($form, resp.responseJSON.X_OCTOBER_ERROR_FIELDS);
                    }
                    else {
                        this.error(resp, status, err);
                    }
                }
            });
        });
    };

    var resetErrors = function ($form) {
        $form.find('.warning-block').hide();
    };

    var displayErrors = function ($form, errors) {
        $.each(errors, function (field, message) {
            var $wb = $form.find("[name='" + field + "']").siblings('.warning-block');
            $wb.find('span').html(message[0]);
            $wb.show();
        });
    };

    /**
     * Input filter. Allowed only cyrillic symbols.
     * Capitalize first symbol in word
     */
    var initFullNameFields = function () {
        $('.js-cyrCapitalize').on('input', function (e) {
            var $this = $(this);

            if ($this.data('value') == $this.val()) {
                return;
            }
            var start = this.selectionStart;
            var end = this.selectionEnd;
            var input = $this.val();
            if (input.length > 1) {
                input = input.split(' ')
                    .map(function (word) {
                        return word.length > 0 ? word[0].toUpperCase() + word.substr(1) : word;
                    })
                    .join(' ');
            }
            $this.val(input);
            $this.data('value', input);
            this.setSelectionRange(start, end);
        });
    };

    var initSignUpFormInvite = function () {
        $('body').on('click', '.sign-up-email-btn.invite', function (e) {
            e.preventDefault();
            $.request('signUp::onSocialRegistrationForm', {
                success: function (resp, status, err) {
                    this.success(resp, status, err);
                    $('.js-signin_modal').modal('hide');
                    $('.js-signup_modal').modal('show');
                    initPhoneMask()
                }
            });
        });
    };

    var initPhoneMask = function() {
      //  $("[name='phone']").mask('+7 999 999 99 99');
    };

    return {
        init: function () {
            initHashUrlRegistration();
            initPopupButton();
            initSignUpForm();
            initSignUpFormInvite();
            initCardForm();
        },
        initFullNameFields: initFullNameFields
    };
}();

// Initialize when page loads
jQuery(function () {
    SignUp.init();
});
