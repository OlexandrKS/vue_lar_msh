var AccountAside = function() {

    var initDropDownMenu = function(){
        $('.js_account_menu_dropdown').on('click', function(e){
            e.preventDefault();

            var $menu = $('.js-account_menu');
            if($menu.hasClass('open')) {
                $menu.removeClass('open').slideUp();
                $('.js-current').slideDown();
                $(this).find('.down').removeClass('fa-chevron-up').addClass('fa-chevron-down');
            }
            else {
                $menu.addClass('open').slideDown();
                $('.js-current').slideUp();
                $(this).find('.down').removeClass('fa-chevron-down').addClass('fa-chevron-up');
            }
        });
    };

    var initExpandButton = function(){
        $('.js-expand_button').on('click', function(e){
            e.preventDefault();

            var $this = $(this);
            var $menu = $this.parent().siblings('.js-expand_list');
            if($menu.hasClass('open')) {
                $menu.removeClass('open').slideUp();
                $this.find('.fa').removeClass('fa-minus').addClass('fa-plus');
            }
            else {
                $('.js-expand_list').removeClass('open').slideUp();
                $('.js-expand_button .fa').removeClass('fa-minus').addClass('fa-plus');

                $menu.addClass('open').slideDown();
                $this.find('.fa').removeClass('fa-plus').addClass('fa-minus');
            }
        });
    };

    return {
        init: function () {
            initDropDownMenu();
            initExpandButton();
        }
    };
}();

// Initialize when page loads
jQuery(function(){ AccountAside.init(); });
