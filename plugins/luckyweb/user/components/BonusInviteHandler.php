<?php namespace Luckyweb\User\Components;

use Illuminate\Support\Facades\Redirect;
use LuckyWeb\User\Facades\Auth;
use LuckyWeb\User\Models\BonusInvite;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Session;

class BonusInviteHandler extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'bonusInfo Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        if(Auth::check()){
            return Redirect::to('404');
        }

        $bonusInvite = BonusInvite::where('code', $this->param('bonus_id'))
            ->where('status_id', BonusInvite::STATUS_ENABLED)
            ->first();

        if($bonusInvite){
            $this->page['bonus_invite'] = $bonusInvite;
            Session::put('bonus_invite_id', $bonusInvite->id);
        }
        else{
            return Redirect::to('404');
        }

    }
}
