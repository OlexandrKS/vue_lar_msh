<?php namespace LuckyWeb\User\Components;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Lang;
use Auth;
use LuckyWeb\User\Classes\BonusManager;
use LuckyWeb\User\Models\BonusInvite;
use Mail;
use Event;
use Flash;
use Input;
use Request;
use Redirect;
use System\Classes\CombineAssets;
use Validator;
use Illuminate\Support\Facades\Session;
use ValidationException;
use ApplicationException;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use LuckyWeb\User\Models\Settings as UserSettings;
use Exception;

class Account extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'luckyweb.user::lang.account.account',
            'description' => 'luckyweb.user::lang.account.account_desc'
        ];
    }

    public function defineProperties()
    {
        return [
            'redirect' => [
                'title'       => 'luckyweb.user::lang.account.redirect_to',
                'description' => 'luckyweb.user::lang.account.redirect_to_desc',
                'type'        => 'dropdown',
                'default'     => ''
            ],
            'paramCode' => [
                'title'       => 'luckyweb.user::lang.account.code_param',
                'description' => 'luckyweb.user::lang.account.code_param_desc',
                'type'        => 'string',
                'default'     => 'code'
            ],
            'forceSecure' => [
                'title'       => 'Force secure protocol',
                'description' => 'Always redirect the URL with the HTTPS schema.',
                'type'        => 'checkbox',
                'default'     => 0
            ],
        ];
    }

    public function getRedirectOptions()
    {
        return [''=>'- none -'] + Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    /**
     * Executed when this component is bound to a page or layout.
     */
    public function onRun()
    {
        /*
         * Redirect to HTTPS checker
         */
        if ($redirect = $this->redirectForceSecure()) {
            return $redirect;
        }

        /*
         * Activation code supplied
         */
        $routeParameter = $this->property('paramCode');

        if ($activationCode = $this->param($routeParameter)) {
            $this->onActivate($activationCode);
        }

        $this->page['isActivation'] = Session::has('isActivation');
        $this->page['user'] = $this->user();
        $this->page['loginAttribute'] = $this->loginAttribute();
        $this->page['loginAttributeLabel'] = $this->loginAttributeLabel();
        $this->page['showWelcomeMessage'] = Session::get('showWelcomeMessage');
        if($this->user()) {
            //$this->page['isOpenQuestionary'] = empty($this->user()->questionary_exported_at) && $this->user()->is_questionary_exported;
            $this->page['isOpenQuestionary'] = false;
            if($this->user()->isFirstVisit()) {
                $this->user()->touchFirstVisit();
            }
        }

        $this->addCss(CombineAssets::combine([
            '/themes/shara/assets/vendor/animate/animate.css'
        ], base_path()));

        $this->addJs(CombineAssets::combine([
            '/themes/shara/assets/vendor/bootstrap-notify/bootstrap-notify.min.js',
            '/plugins/luckyweb/user/components/account/assets/js/account.js'
        ], base_path()));
    }

    /**
     * Returns the logged in user, if available
     */
    public function user()
    {
        if (!Auth::check()) {
            return null;
        }

        return Auth::getUser();
    }

    /**
     * Returns the login model attribute.
     */
    public function loginAttribute()
    {
        return UserSettings::get('login_attribute', UserSettings::LOGIN_EMAIL);
    }

    /**
     * Returns the login label as a word.
     */
    public function loginAttributeLabel()
    {
        return $this->loginAttribute() == UserSettings::LOGIN_EMAIL
            ? Lang::get('luckyweb.user::lang.login.attribute_email')
            : Lang::get('luckyweb.user::lang.login.attribute_username');
    }


    /**
     * Activate the user
     * @param  string $code Activation code
     */
    public function onActivate($code = null)
    {
        try {
            $code = post('code', $code);

            /*
             * Break up the code parts
             */
            $parts = explode('!', $code);
            if (count($parts) != 2) {
                throw new ValidationException(['code' => Lang::get('luckyweb.user::lang.account.invalid_activation_code')]);
            }

            list($userId, $code) = $parts;

            if (!strlen(trim($userId)) || !($user = Auth::findUserById($userId))) {
                throw new ApplicationException(Lang::get('luckyweb.user::lang.account.invalid_user'));
            }

            if (!$user->attemptActivation($code)) {
                throw new ValidationException(['code' => Lang::get('luckyweb.user::lang.account.invalid_activation_code')]);
            }

            Flash::success(Lang::get('luckyweb.user::lang.account.success_activation'));

            /*
             * Sign in the user
             */
            Auth::login($user);

            Session::forget('isActivation');

            // Apply bonuses for bonus-invite promotion
            if($bonusId = Session::pull('bonus_invite_id')){
                $bonusInvite = BonusInvite::where('id', $bonusId)
                    ->where('status_id', BonusInvite::STATUS_ENABLED)
                    ->first();

                if($bonusInvite) {
                    (new BonusManager())->apply($user, $bonusInvite->amount, $bonusInvite->expired_at);
                }
            }

        }
        catch (Exception $ex) {
            if (Request::ajax()) throw $ex;
            else Flash::error($ex->getMessage());
        }
    }

    /**
     * Show update form
     * @return array
     */
    public function onShowUpdate()
    {
        return ['.js-account_container' => $this->renderPartial('account::update', ['user' => $this->user()])];
    }

    /**
     * Show user info
     * @return array
     */
    public function onShowInfo()
    {
        return ['.js-account_container' => $this->renderPartial('account::info', ['user' => $this->user()])];
    }

    /**
     * Update the user
     */
    public function onUpdate()
    {
        if (!$user = $this->user()) {
            return;
        }

        /*
         * Validate input
         */
        $data = array_except(post(), ['email']);
        $data['phone'] = preg_replace('/[^0-9]+/', '', $data['phone']);
        $data['viber'] = preg_replace('/[^0-9]+/', '', $data['viber']);
        $data['whatsapp'] = preg_replace('/[^0-9]+/', '', $data['whatsapp']);
        $data['telegram'] = preg_replace('/[^0-9]+/', '', $data['telegram']);
        if(!empty(array_get($data,'birth_date'))) {
            $data['birth_date'] = Carbon::parse($data['birth_date']);
        }

        $rules = [
            'name' => 'required',
            'last_name' => 'required',
            'other_name' => 'required',
            'phone' => ['required', 'regex:/^7[0-9]{10}$/', 'unique:users,phone,'.$user->id],
            //'email' => 'required|email|between:6,255|unique:users,email,'.$user->id,
            'birth_date' => 'required|date',
            'gender_id' => 'required|integer',
        ];

        $attributes = [
            'name' => '"Имя"',
            'last_name' => '"Фамилия"',
            'other_name' => '"Отчество"',
            'phone' => '"Телефон"',
            //'email' => '"Email"',
            'birth_date' => '"Дата рождения"',
            'gender_id' => '"Пол"',
        ];

        $validation = Validator::make($data, $rules, [], $attributes);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        $user->fill($data);
        $user->is_exported = false;
        $extended = $user->extended;
        $extended['additional'] = array_merge(array_get($extended, 'additional', []), array_only($data, [
            'city', 'viber', 'whatsapp', 'telegram'
        ]));
        $user->extended = $extended;
        $user->save();

        return ['.js-account_container' => $this->renderPartial('account::info', ['user' => $user])];
    }

    /**
     * Return form for password change
     */
    public function onPasswordChangeForm()
    {
        return ['.js-account_container' => $this->renderPartial('account::password_update')];
    }

    /**
     * Update user password
     */
    public function onUpdatePassword()
    {
        if (!$user = $this->user()) {
            return;
        }

        $rules = [
            'password' => 'required|min:5',
            'password_confirmation' => 'required|min:5|same:password',
        ];

        $attributes = [
            'password' => '"Новый пароль"',
            'password_confirmation' => '"Подтверждение пароля"',
        ];

        $validation = Validator::make(post(), $rules, [], $attributes);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        $user->password = post('password');
        $user->forceSave();

        Auth::login($user);

        return ['.js-account_container' => $this->renderPartial('account::info', ['user' => $user])];
    }

    /**
     * Deactivate user
     */
    public function onDeactivate()
    {
        if (!$user = $this->user()) {
            return;
        }

        if (!$user->checkHashValue('password', post('password'))) {
            throw new ValidationException(['password' => Lang::get('luckyweb.user::lang.account.invalid_deactivation_pass')]);
        }

        $user->delete();
        Auth::logout();

        Flash::success(post('flash', Lang::get('luckyweb.user::lang.account.success_deactivation')));

        /*
         * Redirect
         */
        if ($redirect = $this->makeRedirection()) {
            return $redirect;
        }
    }

    /**
     * Trigger a subsequent activation email
     */
    public function onSendActivationEmail()
    {
        try {
            if (!$user = $this->user()) {
                throw new ApplicationException(Lang::get('luckyweb.user::lang.account.login_first'));
            }

            if ($user->is_activated) {
                throw new ApplicationException(Lang::get('luckyweb.user::lang.account.already_active'));
            }

            Flash::success(Lang::get('luckyweb.user::lang.account.activation_email_sent'));

            $this->sendActivationEmail($user);

        }
        catch (Exception $ex) {
            if (Request::ajax()) throw $ex;
            else Flash::error($ex->getMessage());
        }

        /*
         * Redirect
         */
        if ($redirect = $this->makeRedirection()) {
            return $redirect;
        }
    }

    /**
     * Return questionary popup
     * @return array
     */
    public function onOpenQuestionary()
    {
        $structure = UserSettings::get('questionary_structure');

        return ['.js-signup_modal .modal-content' => $this->renderPartial('account::questionary', [
            'structure' => $structure,
            'user' => $this->user()
        ])];
    }

    /**
     * Questionary form submitted
     * @return array
     */
    public function onQuestionary()
    {
        /*
         * Validate input
         */
        $data = post();
        $rules = [];
        $customMessages=[];
        $attributes = [];

        // Generate rules and custom messages for dynamic questionary fields
        foreach (UserSettings::get('questionary_structure') as $group) {
            foreach ($group['items'] as $type => $items) {
                // Set rule for checkboxes, radio buttons etc.
                if(count($items) > 0) {
                    $rules['q.' . $group['code_group'].'.items.'.$type] = 'required';
                    $customMessages['q.' . $group['code_group'] . '.items.'.$type.'.required'] = 'Выберите варианты';
                }
            }
        }

        $validation = Validator::make($data, $rules, $customMessages, $attributes);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        $user = $this->user();
        $extended = $user->extended;
        $extended['questionary'] = $data['q'];
        $extended['additional']['child_birth_year'] = array_get($data, 'child_birth_year');
        $user->extended = $extended;

        // Send only one time just after registration
        if(!$user->questionary_exported_at) {
            $user->is_questionary_exported = false;
        }
        $user->save();

        return Redirect::to($this->pageUrl('account'));
    }

    /**
     * Sends the activation email to a user
     * @param  User $user
     * @return void
     */
    protected function sendActivationEmail($user)
    {
        $code = implode('!', [$user->id, $user->getActivationCode()]);
        $link = $this->currentPageUrl([
            $this->property('paramCode') => $code
        ]);

        $data = [
            'name' => $user->name,
            'link' => $link,
            'code' => $code
        ];

        Mail::send('luckyweb.user::mail.activate', $data, function($message) use ($user) {
            $message->to($user->email, $user->name);
        });
    }

    /**
     * Redirect to the intended page after successful update, sign in or registration.
     * The URL can come from the "redirect" property or the "redirect" postback value.
     * @return mixed
     */
    protected function makeRedirection()
    {
        $redirectUrl = $this->pageUrl($this->property('redirect'))
            ?: $this->property('redirect');

        if ($redirectUrl = post('redirect', $redirectUrl)) {
            return Redirect::to($redirectUrl);
        }
    }

    /**
     * Checks if the force secure property is enabled and if so
     * returns a redirect object.
     * @return mixed
     */
    protected function redirectForceSecure()
    {
        if (
            Request::secure() ||
            Request::ajax() ||
            !$this->property('forceSecure')
        ) {
            return;
        }

        return Redirect::secure(Request::path());
    }
}
