var SocialConfirmation = function () {
    var $ = jQuery;

    var initUpdateForm = function() {
        $('.js-confirm_form').on('submit', function(e){
            e.preventDefault();
            var $form = $(this);
            $form.find("button[type='submit']").prop('disabled', true);
            resetErrors($form);
            $form.request('socialConfirmation::onConfirm', {
                error: function(resp, status, err) {
                    $form.find("button[type='submit']").prop('disabled', false);
                    if (typeof resp.responseJSON != 'undefined'){
                        displayErrors($form, resp.responseJSON.X_OCTOBER_ERROR_FIELDS);
                    }
                    else {
                        this.error(resp, status, err);
                    }
                }
            });
        });
    };

    var resetErrors = function ($form) {
        $form.find('.warning-block').hide();
    };

    var displayErrors = function ($form, errors) {
        $.each(errors, function (field, message) {
            var $wb = $form.find("[name='" + field + "']").siblings('.warning-block');
            $wb.find('span').html(message[0]);
            $wb.show();
        });
    };



    /**
     * Input filter. Allowed only cyrillic symbols.
     * Capitalize first symbol in word
     */
    var initFullNameFields = function() {
        $('.js-cyrCapitalize').on('input', function(e){
            var $this = $(this);

            if($this.data('value') != $this.val()) {

                // Save cursor positions
                var start = this.selectionStart;
                var end = this.selectionEnd;

                // Left only cyrillic symbols
                //var input = $this.val().replace(/[^а-яА-ЯїЇєЄіІёЁ ]/g, '');
                var input = $this.val();

                // Capitalize first symbol in the word
                if(input.length > 1) {
                    input = input.split(' ')
                        .map(function (word) {
                            return word.length > 0 ? word[0].toUpperCase() + word.substr(1) : word;
                        })
                        .join(' ');
                }

                // Save value
                $this.val(input);
                $this.data('value', input);

                // Restore cursor positions
                this.setSelectionRange(start, end);
            }
        });
    };

    var initPhoneMask = function() {
     //   $("[name='phone']").mask('+7 999 999 99 99');
    };
    
    return {
        init: function () {
            initPhoneMask();
            initFullNameFields();
            initUpdateForm();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    SocialConfirmation.init();
});
