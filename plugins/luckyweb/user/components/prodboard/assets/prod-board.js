var ProdBoard = function () {
    var $ = jQuery;

    var initProdBoardToken = function () {

    };

    return {
        init: function () {
            initProdBoardToken();
        },
    };
}();

// Initialize when page loads
jQuery(function () {
    ProdBoard.init();
});
