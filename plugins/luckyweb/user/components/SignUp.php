<?php namespace LuckyWeb\User\Components;

use Cms\Classes\ComponentBase;
use Exception;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use LuckyWeb\User\Facades\Auth;
use LuckyWeb\User\Models\User;
use October\Rain\Exception\ApplicationException;
use October\Rain\Exception\ValidationException;
use October\Rain\Support\Facades\Flash;
use LuckyWeb\User\Models\Settings as UserSettings;
use LuckyWeb\MS\Models\FavoriteList;
use LuckyWeb\MS\Models\FavoriteListItem;
use System\Classes\CombineAssets;


class SignUp extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'SignUp Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->addJs(CombineAssets::combine([
            '/plugins/luckyweb/user/components/signup/assets/js/sign_up.js',
            '/themes/shara/assets/js/plugins/jquery.maskedinput/jquery.maskedinput.min.js'
        ], base_path()), ['async']);
    }

    /**
     * Show sign up form
     * @return array
     */
    public function onSignUpPopup()
    {
        return ['.js-signup_modal .modal-content' => $this->renderPartial('signUp::have_card')];
    }

    /**
     * Show register form with filled data
     * @return array
     */
    public function onCardForm()
    {
        return ['.js-signup_modal .modal-content' => $this->renderPartial('signUp::card_form')];
    }

    /**
     * Show social register form
     * @return array
     */
    public function onSocialRegistrationForm()
    {
        return ['.js-signup_modal .modal-content' => $this->renderPartial('signUp::social_register')];
    }

    /**
     * Show empty register form
     * @return array
     */
    public function onRegistrationForm()
    {
        return ['.js-signup_modal .modal-content' => $this->renderPartial('signUp::register_form')];
        //return ['.js-signup_modal .modal-content' => $this->renderPartial('signUp::get_card')];
    }

    /**
     * User has card and want to register with it
     */
    public function onCardSearch()
    {
        $data = post();

        if (array_key_exists('card_code', $data) && $data['card_code'] != '') {
            // append leading zeros to card number
            $multiplier = Config::get('luckyweb.ms::account_card_length') - mb_strlen($data['card_code']);
            $data['card_code'] = str_repeat("0", $multiplier > 0 ? $multiplier : 0) . $data['card_code'];
        }

        $rules = [
            'card_code' => 'required|digits_between:1,' . Config::get('luckyweb.ms::account_card_length'),
            'card_pin' => 'required|digits:4'
        ];

        $attributes = [
            'card_code' => '"Номер карты"',
            'card_pin' => '"PIN"'
        ];

        $validation = Validator::make($data, $rules, [], $attributes);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        $user = User::where('card_code', array_get($data, 'card_code'))
            ->where('card_pin', array_get($data, 'card_pin'))
            ->first();

        if (!$user) {
            throw new ApplicationException('Пользователь с таким номером карты не найден');
        }

        if ($user->is_activated) {
            //return ['.js-signup_modal .modal-content' => $this->renderPartial('signUp::card_duplicate_message')];
            /*
             * Authenticate user
             */
            $credentials = array_only($data, [
                'card_code', 'card_pin'
            ]);

            Event::fire('luckyweb.user.beforeAuthenticate', [$this, $credentials]);

            $user = Auth::authenticate($credentials, true, 2);

            if ($user && $user->name != '') {
                Session::flash('showWelcomeMessage', true);
            }

            /*
             * Redirect to account page
             */
            return Redirect::to($this->pageUrl('account'));
        } else {
            Session::put('user_id', $user->id);
            return ['.js-signup_modal .modal-content' => $this->renderPartial('signUp::register_form', ['user' => $user])];
        }
    }

    /**
     * User submit registration form
     */
    public function onSignUp()
    {
        try {
            if (!UserSettings::get('allow_registration', true)) {
                throw new ApplicationException(trans('luckyweb.user::lang.account.registration_disabled'));
            }

            // ID of user find by card.
            /*if(empty(Session::get('user_id'))) {
                throw new Exception('Произошла ошибка. Повторите регистрацию снова.');
            }*/

            /*
             * Validate input
             */
            $data = post();
            $data['phone'] = preg_replace('/[^0-9]+/', '', $data['phone']);

            $rules = [
                'name' => 'required',
                'last_name' => 'required',
                'phone' => ['required', 'regex:/^7[0-9]{10}$/', 'unique:users,phone,' . Session::get('user_id')],
                'email' => 'required|email|between:6,255|unique:users,email,' . Session::get('user_id'),
                'password' => 'required|between:4,255|confirmed',
                'password_confirmation' => 'required',
                'policy_accept' => 'accepted'
            ];

            $attributes = [
                'name' => '"Имя"',
                'last_name' => '"Фамилия"',
                'phone' => '"Телефон"',
                'email' => '"Email"',
                'password' => '"Пароль"',
                'password_confirmation' => '"Подтверждение пароля"',
                'policy_accept' => '"Политику конфиденциальности"'
            ];

            $validation = Validator::make($data, $rules, [], $attributes);
            if ($validation->fails()) {
                throw new ValidationException($validation);
            }

            /*
             * Register user
             */
            $requireActivation = UserSettings::get('require_activation', true);
            $automaticActivation = UserSettings::get('activate_mode') == UserSettings::ACTIVATE_AUTO;
            $userActivation = UserSettings::get('activate_mode') == UserSettings::ACTIVATE_USER;

            $user = null;
            if (Session::has('user_id')) {
                $user = User::find(Session::get('user_id'));
            }

            if ($user) {
                Auth::convertGuestToUser($user, $data, true);
            } else {
                $user = Auth::register($data, $automaticActivation);

                // Generate site version of the card
                $user->generateCard();

                $user->save();
            }

            /*
             * Activation is by the user, send the email
             */
            if ($userActivation) {
                $this->sendActivationEmail($user);

                Session::put('isActivation', true);
                //Flash::success(Lang::get('luckyweb.user::lang.account.activation_email_sent'));
            }

            /*
             * Automatically activated or not required, log the user in
             */
            if ($automaticActivation || !$requireActivation) {
                Auth::login($user);
            }

            /*
             * Create default list and add data from cookie
             */
            $flist = new FavoriteList;
            $flist->list_name = 'По умолчанию';
            $flist->user_id = $user->id;
            $flist->save();

            FavoriteListItem::fillFromCookie();

            Session::forget('user_id');

            /*
             * Redirect to account page
             */
            return Redirect::to($this->pageUrl('account'));
        } catch (Exception $ex) {
            if (Request::ajax()) throw $ex;
            else Flash::error($ex->getMessage());
        }
    }

    /**
     * Sends the activation email to a user
     * @param  User $user
     * @return void
     */
    protected function sendActivationEmail($user)
    {
        $code = implode('!', [$user->id, $user->getActivationCode()]);
        $link = $this->pageUrl('account') . '/' . $code;

        $data = [
            'name' => $user->name,
            'link' => $link,
            'code' => $code
        ];

        Mail::send('luckyweb.user::mail.activate', $data, function ($message) use ($user) {
            $message->to($user->email, $user->name);
        });
    }

    /**
     * Return url to social provider
     * @param $code
     * @return string
     */
    public function getProviderUrl($code)
    {
        return url('/auth/' . $code);
    }
}
