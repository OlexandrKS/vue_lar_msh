var Account = function () {
    var $ = jQuery;

    var initRegisterForm = function() {
        $('.js-register_form').on('submit', function(e){
            e.preventDefault();

            var $form = $(this);
            $form.find("button[type='submit']").prop('disabled', true);
            resetErrors($form);
            $form.request('account::onRegister', {
                error: function(resp, status, err) {
                    $form.find("button[type='submit']").prop('disabled', false);
                    if (typeof resp.responseJSON != 'undefined'){
                        displayErrors($form, resp.responseJSON.X_OCTOBER_ERROR_FIELDS);
                    }
                    else {
                        this.error(resp, status, err);
                    }
                }
            });
        });
    };

    var initSignInForm = function() {
        $('.js-signin_form').on('submit', function(e){
            e.preventDefault();

            var $form = $(this);
            $form.find("button[type='submit']").prop('disabled', true);
            resetErrors($form);
            $form.request('account::onSignin', {
                error: function(resp, status, err) {
                    $form.find("button[type='submit']").prop('disabled', false);
                    if (typeof resp.responseJSON != 'undefined'){
                        displayErrors($form, resp.responseJSON.X_OCTOBER_ERROR_FIELDS);
                    }
                    else {
                        this.error(resp, status, err);
                    }
                }
            });
        });
    };

    var initUpdateButton = function() {
        $('.js-account_container').on('click', '.js-update_button', function(e){
            e.preventDefault();

            $.request('account::onShowUpdate', {
                success: function (resp, status, err) {
                    this.success(resp, status, err);

                    $(".js-update_info_form [name='phone']").inputmask({mask: '+7(999) 999-99-99', showMaskOnHover: false});
                    $("[name='viber']").inputmask({mask: '+7(999) 999-99-99', showMaskOnHover: false});
                    $("[name='whatsapp']").inputmask({mask: '+7(999) 999-99-99', showMaskOnHover: false});
                    $("[name='telegram']").inputmask({mask: '+7(999) 999-99-99', showMaskOnHover: false});
                    $("[name='birth_date']").inputmask({mask: '99.99.9999', showMaskOnHover: false});
                }
            });
        });
    };

    var initUpdateForm = function() {
        $('.js-account_container').on('submit', '.js-update_info_form', function(e){
            e.preventDefault();

            var $form = $(this);
            $form.find("button[type='submit']").prop('disabled', true);
            resetErrors($form);
            $form.request('account::onUpdate', {
                error: function(resp, status, err) {
                    $form.find("button[type='submit']").prop('disabled', false);
                    if (typeof resp.responseJSON != 'undefined'){
                        displayErrors($form, resp.responseJSON.X_OCTOBER_ERROR_FIELDS);
                    }
                    else {
                        this.error(resp, status, err);
                    }
                },
                success: function (resp, status, err) {
                    $.notify('Данные успешно сохранены', {
                            type: "success",
                            placement: {align: 'center'}
                    });

                    this.success(resp, status, err);
                }
            });
        });
    };

    var initUpdatePasswordForm = function() {
        $('.js-account_container').on('submit', '.js-update_password_form', function(e){
            e.preventDefault();

            var $form = $(this);
            $form.find("button[type='submit']").prop('disabled', true);
            resetErrors($form);
            $form.request('account::onUpdatePassword', {
                error: function(resp, status, err) {
                    $form.find("button[type='submit']").prop('disabled', false);
                    if (typeof resp.responseJSON != 'undefined'){
                        displayErrors($form, resp.responseJSON.X_OCTOBER_ERROR_FIELDS);
                    }
                    else {
                        this.error(resp, status, err);
                    }
                },
                success: function (resp, status, err) {
                    $.notify('Данные успешно сохранены', {
                        type: "success",
                        placement: {align: 'center'}
                    });

                    this.success(resp, status, err);
                }
            });
        });
    };

    var resetErrors = function($form) {
        $form.find('.form-group.has-error').each(function () {
            $(this).removeClass('has-error').find('em.error').hide();
        });
    };

    var displayErrors = function($form, errors) {
        $.each(errors, function (field, message) {
            var $fg = $form.find("[name='"+field+"']").parents('.form-group').addClass('has-error');
            var $e = $fg.find("em.error");
            if ($e.length) {
                $e.eq(0).html(message).show();
            }
            else {
                $fg.append('<em class="error">'+message+'</em>');
            }
        });
    };

    var popupResetErrors = function($form) {
        $form.find('.warning-block').hide();
    };

    var popupDisplayErrors = function($form, errors) {
        var isScrolled = false;

        $.each(errors, function (field, message) {
            var $wb = $form.find("[data-code='"+field+"']");
            $wb.find('span').html(message[0]);
            $wb.show();

            if(!isScrolled) {
                $('.js-signup_modal').animate({
                    scrollTop: ($wb.parents('.js-q_row').position().top + 150) + 'px'
                }, 'fast');
                isScrolled = true;
            }
        });
    };

    var initQuestionaryForm = function() {
        $('.js-signup_modal').on('submit', '.js-questionary_form', function(e){
            e.preventDefault();

            var $form = $(this);
            $form.find("button[type='submit']").prop('disabled', true);
            popupResetErrors($form);
            $form.request('account::onQuestionary', {
                error: function(resp, status, err) {
                    $form.find("button[type='submit']").prop('disabled', false);
                    if (typeof resp.responseJSON != 'undefined'){
                        popupDisplayErrors($form, resp.responseJSON.X_OCTOBER_ERROR_FIELDS);
                    }
                    else {
                        this.error(resp, status, err);
                    }
                }
            });
        });
    };

    var openQuestionary = function() {
        $.request('account::onOpenQuestionary', {
            success: function(resp, status, err) {
                this.success(resp, status, err);
                $('.js-signup_modal').modal('show');
            }
        })
    };

    var initQuestionaryElements = function() {
        $('body')
            .on('change', '.js-q_radio', function(){
                var action = $(this).data('action');
                var $element = $(this).parents('.radio-group').siblings('.js_control_element');

                if(action == 'enable') {
                    $element.find('.css-checkbox').removeClass('disabled')
                        .find('input').prop('disabled', false);
                }
                else {
                    $element.find('.css-checkbox').addClass('disabled')
                        .find('input').prop('disabled', true);
                }
            })
            .on('change', '.js-checkbox_group input', function(){
                var $group = $(this).parents('.js-checkbox_group');
                var allowedCount = parseInt($group.data('max'));
                var currentCount = $group.find('input:checked').length;

                if(allowedCount > 0 && currentCount > allowedCount) {
                    $(this).prop('checked', false);
                }
            })
            .on('change', '.js-q_with_input', function(){
                if($(this).is(':checked')) {
                    $(this).parents('.js-check_text').find('.css-text').prop('disabled', false);
                }
                else {
                    $(this).parents('.js-check_text').find('.css-text').prop('disabled', true);
                }
            })
            .on('change', '.js-q_no_input', function () {
                $(this).parents('.js-q_group').find('.css-text').prop('disabled', true);
            })
    };

    var initShowPasswordButton = function() {
        $('.js-account_container').on('click', '.js-show_password_button', function(e){
            e.preventDefault();

            var $input = $(this).parents('.js-password_input_row').find('input');
            if ($input.attr("type") === "password") {
                $input.attr("type", "text");
                $(this).text('Скрыть');
            } else {
                $input.attr("type", "password");
                $(this).text('Показать');
            }
        });
    };

    /**
     * Input filter. Allowed only cyrillic symbols.
     * Capitalize first symbol in word
     */
    var initFullNameFields = function() {
        $('.js-account_container').on('input', '.js-cyrCapitalize', function(e){
            var $this = $(this);

            if($this.data('value') != $this.val()) {

                // Save cursor positions
                var start = this.selectionStart;
                var end = this.selectionEnd;

                // Left only cyrillic symbols
                //var input = $this.val().replace(/[^а-яА-ЯїЇєЄіІёЁ ]/g, '');
                var input = $this.val();

                // Capitalize first symbol in the word
                if(input.length > 1) {
                    input = input.split(' ')
                        .map(function (word) {
                            return word.length > 0 ? word[0].toUpperCase() + word.substr(1) : word;
                        })
                        .join(' ');
                }

                // Save value
                $this.val(input);
                $this.data('value', input);

                // Restore cursor positions
                this.setSelectionRange(start, end);
            }
        });
    };

    return {
        init: function () {
            initRegisterForm();
            initSignInForm();
            initQuestionaryForm();
            initUpdateButton();
            initUpdateForm();
            initQuestionaryElements();
            initShowPasswordButton();
            initUpdatePasswordForm();
            initFullNameFields();

            if(AccountData.isOpenQuestionary) {
                openQuestionary();
            }
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    Account.init();
});
