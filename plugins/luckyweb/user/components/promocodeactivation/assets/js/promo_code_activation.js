var PromoCodeActivation = function () {
    var $ = jQuery;

    var initSignUpForm = function () {
        $('.container').on('submit', '.js-signup_promo_code_form', function (e) {
            e.preventDefault();

            var $form = $(this);
            $form.find("button[type='submit']").prop('disabled', true);
            resetErrors($form);
            $form.request('promoCodeActivation::onSignUp', {
                error: function (resp, status, err) {
                    $form.find("button[type='submit']").prop('disabled', false);
                    if (typeof resp.responseJSON != 'undefined') {
                        displayErrors($form, resp.responseJSON.X_OCTOBER_ERROR_FIELDS);
                    } else {
                        this.error(resp, status, err);
                    }
                }
            });
        });
    };

    var resetErrors = function ($form) {
        $form.find('.warning-block').hide();
    };

    var displayErrors = function ($form, errors) {
        $.each(errors, function (field, message) {
            var $wb = $form.find("[name='" + field + "']").siblings('.warning-block');
            $wb.find('span').html(message[0]);
            $wb.show();
        });
    };

    /**
     * Input filter. Allowed only cyrillic symbols.
     * Capitalize first symbol in word
     */

    var initPhoneMask = function () {
        $("[name='phone']").mask('+7 999 999 99 99');
    };

    var initAfterSignInCallback =  function () {
        window.afterSignInCallback = function () {
            $.when($('.js-signup_promo_code_form').request('promoCodeActivation::onSignIn'))
                .then(function () {
                    window.location = '/account-promo-codes';
                });
        }
    }

    return {
        init: function () {
            initSignUpForm();
            initPhoneMask();
            initAfterSignInCallback();
        },
    };
}();

// Initialize when page loads
jQuery(function () {
    PromoCodeActivation.init();
});
