<?php namespace LuckyWeb\User\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use LuckyWeb\User\Facades\Auth;
use System\Classes\CombineAssets;

class AccountAside extends ComponentBase
{
    /**
     * Current menu list
     * @var array
     */
    public $menu;

    /**
     * Info panel data
     * @var array
     */
    public $info;


    public function componentDetails()
    {
        return [
            'name'        => 'AccountMenu Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        if(!Auth::check()) {
            return false;
        }

        $this->addJs(CombineAssets::combine([
            '/plugins/luckyweb/user/components/accountaside/assets/js/account_aside.js'
        ], base_path()));

        $this->menu = [
            'account-orders' => [
                'link' => url('account-orders'),
                'title' => 'История заказов',
                'icon' => 'fa fa-file-text'
            ],
            'prod-board' => [
                'link' => '#',
                'title' => 'Проект кухонь',
                'icon' => 'fa fa-cube',
                'request' => 'onSignInProdBoard',
                'target' => '_blank'
            ],
            'account' => [
                'link' => url('account'),
                'title' => 'Личные данные',
                'icon' => 'fa fa-user'
            ],
            'account-bonuses' => [
                'link' => url('account-bonuses'),
                'title' => 'Бонусный счет',
                'icon' => 'fa fa-gift'
            ],
            'account-promo-codes' => [
                'link' => url('account-promo-codes'),
                'title' => 'Мои промокоды',
                'icon' => 'fa fa-percent'
            ],
            'logout' => [
                'custom' => 'v-on:click=$store.dispatch("user/logout")',
                'link' => url('/'),
                // 'request' => 'onLogout',
                'title' => 'Выход',
                'icon' => 'fa fa-sign-out'
            ],
        ];

        $this->info['deactivatedThisMonth'] = $this->deactivatedThisMonth();
        $this->info['allDeactivated'] = $this->allDeactivated();
    }

    /**
     * Return bonuses count deactivated this month
     * @return mixed
     */
    public function deactivatedThisMonthCount()
    {
        return $this->deactivatedThisMonth()->reduce(function ($carry, $item) {
            return $carry + intval($item['Summa']);
        }, 0);
    }

    /**
     * Return list of bonuses deactivated this month
     */
    protected function deactivatedThisMonth()
    {
        return collect(array_get(Auth::getUser()->extended, 'bonuses.active_list', []))
            ->map(function($item){
                $item['DateEnd'] = Carbon::parse($item['DateEnd']);
                return $item;
            })
            ->filter(function($item){
                return Carbon::now()->addMonth()->gte($item['DateEnd']);
            });
    }

    /**
     * Return list of all bonuses that should be deactivated
     */
    protected function allDeactivated()
    {
        return collect(array_get(Auth::getUser()->extended, 'bonuses.active_list', []))
            ->map(function($item){
                $item['DateEnd'] = Carbon::parse($item['DateEnd']);
                return $item;
            });
    }
}
