<?php namespace Luckyweb\User\Components;

use Cms\Classes\ComponentBase;
use LuckyWeb\User\Classes\Notifier\AccountNotificationManager;
use LuckyWeb\User\Facades\Auth;
use System\Classes\CombineAssets;

class AccountPopupNotifications extends ComponentBase
{
    /**
     * Notification code
     * @var string
     */
    public $code;

    /**
     * Notification data
     * @var array
     */
    public $data;

    public function componentDetails()
    {
        return [
            'name'        => 'AccountPopupNotifications Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        if(!Auth::check()) {
            return false;
        }

        $notifier = (new AccountNotificationManager(Auth::getUser()))->check();
        if($notifier){
            $this->code = $notifier->getCode();
            $this->data = $notifier->getData();
            $notifier->processed();
        }

        $this->addJs(CombineAssets::combine([
            '/plugins/luckyweb/user/components/accountpopupnotifications/assets/js/account_popup_notifications.js'
        ], base_path()));
    }

}
