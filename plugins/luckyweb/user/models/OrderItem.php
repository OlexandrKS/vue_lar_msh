<?php namespace LuckyWeb\User\Models;

use Model;

/**
 * OrderItem Model
 *
 * @property integer $order_id
 * @property string $good_id
 * @property string $color_id
 * @property string $name
 * @property string $color
 * @property float $amount
 * @property float $quantity
 * @property integer $status_id
 *
 */
class OrderItem extends Model
{
    const STATUS_PRODUCTION = 1;
    const STATUS_SHIPMENT = 4;
    const STATUS_DELIVERY_COORDINATION = 5;
    const STATUS_DELIVERING = 7;
    const STATUS_DEFERRED = 10;
    const STATUS_CANCELED = 11;
    const STATUS_DELIVERED = 12;
    const STATUS_RETURN = 13;
    const STATUS_WAIT_PAYMENT = 33;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_user_order_items';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'order_id',
        'good_id',
        'color_id',
        'name',
        'color',
        'amount',
        'quantity',
        'status_id',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'order' => ['LuckyWeb\User\Models\Order'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public static $statuses = [
        1 => ['PRODUCTION', 'В производстве', 'Ваш товар находиться в производстве.'],
        4 => ['SHIPMENT', 'Готовится к отгрузке в ваш город', 'Ваш товар готовиться к отгрузке в ваш город'],
        5 => ['DELIVERY_COORDINATION', 'Согласование времени доставки', 'Ожидайте звонок от менеджера для согласования времени доставки'],
        7 => ['DELIVERING', 'Едет в ваш город', 'Товар едет в ваш город'],
        10 => ['DEFERRED', 'Отсрочен'],
        11 => ['CANCELED', 'Отменен'],
        12 => ['DELIVERED', 'Товар доставлен', 'Товар доставлен :)'],
        13 => ['RETURN', 'Возврат'],
        33 => ['WAIT_PAYMENT', 'Ожидается оплата', 'Ваш товар готов к отгрузке. Необходимо произвести полную оплату в салоне, где оформляли заказ.'],
    ];

    public static function getStatusLabels($status)
    {
        return array_key_exists($status, static::$statuses) ? static::$statuses[$status]: ['NULL', 'NULL'];
    }
}
