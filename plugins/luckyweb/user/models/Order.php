<?php

namespace LuckyWeb\User\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use October\Rain\Database\Model;

/**
 * Order Model
 *
 *
 * @property integer $status_id
 * @property string $external_id
 * @property string $agreement_number
 * @property string $agreement_date
 * @property string $delivery_date
 * @property float $product_amount
 * @property float $product_debt
 * @property float $service_amount
 * @property float $service_paid
 * @property float $delivery_amount
 * @property float $delivery_paid
 * @property float $reclamation_amount
 * @property float $reclamation_paid
 * @property float $total_amount
 * @property float $total_debt
 * @property float $total_paid
 * @property string $extended
 * @property Carbon $status_updated_at
 * @property Carbon $applied_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 *
 * @property-read OrderItem $items
 *
 */
class Order extends Model
{
    use SoftDeletes;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_user_orders';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'external_id',
        'user_id',
        'agreement_number',
        'agreement_date',
        'delivery_date',
        'product_amount',
        'product_debt',
        'service_amount',
        'service_paid',
        'delivery_amount',
        'delivery_paid',
        'reclamation_amount',
        'reclamation_paid',
        'total_amount',
        'total_debt',
        'total_paid',
        'applied_at',
        'status_updated_at',
        'status_id'
    ];

    protected $dates = [
        'agreement_date',
        'delivery_date',
        'applied_at',
        'status_updated_at'
    ];

    protected $jsonable = [
        'extended'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'items' => ['LuckyWeb\User\Models\OrderItem'],
    ];
    public $belongsTo = [
        'user' => ['LuckyWeb\User\Models\User'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function beforeSave()
    {
        $item = static::find($this->id);
        if ($item && $item->status_id != $this->status_id) {
            $this->status_updated_at = Carbon::now();
        }
    }

    /**
     * Sort and return order items
     */
    public function getItemsSorted()
    {
        $weights = [
            OrderItem::STATUS_PRODUCTION => 1,
            OrderItem::STATUS_WAIT_PAYMENT => 2,
            OrderItem::STATUS_SHIPMENT => 2,
            OrderItem::STATUS_DELIVERING => 3,
            OrderItem::STATUS_DELIVERY_COORDINATION => 4,
            OrderItem::STATUS_DELIVERED => 5,
            OrderItem::STATUS_CANCELED => 10,
            OrderItem::STATUS_DEFERRED => 11,
            OrderItem::STATUS_RETURN => 12,

        ];

        return $this->items->sortBy(function ($item) use ($weights) {
            return array_get($weights, $item->status_id);
        });
    }
}
