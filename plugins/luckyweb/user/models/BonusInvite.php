<?php namespace LuckyWeb\User\Models;

use Model;

/**
 * BonusInvite Model
 */
class BonusInvite extends Model
{
    protected $dates = ['expired_at'];

    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_user_bonus_invites';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @var array Statuses
     */
    public static $statuses = [
        1 => ['ENABLED', 'Активна'],
        2 => ['DISABLED', 'Не активна'],
    ];

    /**
     * Return status label for controller list
     * @param $status
     * @return array|mixed
     */
    public static function getStatusLabels($status)
    {
        return array_key_exists($status, static::$statuses) ? static::$statuses[$status]: ['NULL', 'NULL'];
    }

    public function getStatusIdOptions($keyValue = null)
    {
        $statuses = [];
        foreach (static::$statuses as $key => $value) {
            $statuses[$key] = $value[1];
        }
        return $statuses;
    }
}
