<?php

namespace LuckyWeb\User\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable;
use October\Rain\Database\Model;

/**
 * PromoCodeEvent Model
 *
 * @property string code
 * @property integer promo_code_id
 * @property integer user_id
 * @property Carbon exported_at
 * @property Carbon applied_at
 * @property Carbon created_at
 * @property-read User $user
 * @property-read PromoCode $promo_code
 *
 * @method static $this available(Authenticatable $user = null, $total = null, $code = null)
 *
 * @mixin \Eloquent
 */
class PromoCodeEvent extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_user_promo_code_events';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['id'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array
     */
    protected $casts = [
        'exported_at' => 'datetime',
        'applied_at'  => 'datetime',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'promo_code' => [
            PromoCode::class,
            'key' => 'promo_code_id'
        ],
        'user' => User::class,
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @param \October\Rain\Database\Builder $query
     * @param User|null $user
     * @param float|null $total
     *
     * @param null $code
     * @return \October\Rain\Database\Builder
     */
    public function scopeAvailable($query, $user = null, $total = null, $code = null)
    {
        if ($user && $user instanceof User) {
            $query->where('user_id', $user->getKey());
        }

        if ($code) {
            $query->where('code', $code);
        }

        $now = Carbon::now();

        return $query->whereNull('applied_at')
            ->where('created_at', '>=', Carbon::now()->subDay(10)->toDateTimeString()) // todo refactor condition
            ->whereHas('promo_code', function ($query) use ($now, $total) {
                if ($total !== null) {
                    $query->where('available_from', '<=', (float)$total);
                }

                $query->where('start_at', '<=', $now->toDateTimeString())
                    ->where('expired_at', '>=', $now->toDateTimeString())
                    ->where('status', true);
            });
    }

}
