<?php

namespace LuckyWeb\User\Models;

use Carbon\Carbon;
use October\Rain\Database\Model;

/**
 * PromoCode Model
 *
 * @property string $name
 * @property string $code
 * @property integer $type
 * @property float $amount
 * @property float $available_from
 * @property boolean $status
 * @property Carbon $start_at
 * @property Carbon $expired_at
 *
 * @mixin \Eloquent
 */
class PromoCode extends Model
{
    const STATUS_DISABLED = 0;
    const STATUS_ENABLED = 1;

    const TYPE_DELIVERY = 1;
    const TYPE_SALE = 2;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_user_promo_codes';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['id'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array
     */
    protected $casts = [
        'start_at' => 'datetime',
        'expired_at' => 'datetime',
        'status' => 'boolean',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'events' => [
            PromoCodeEvent::class,
            'key' => 'promo_code_id'
        ]
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @return array
     */
    public function getStatusOptions()
    {
        return [
            static::STATUS_ENABLED => 'Включен',
            static::STATUS_DISABLED => 'Отключен',
        ];
    }

    /**
     * @return array
     */
    public function getTypeOptions()
    {
        return [
            static::TYPE_DELIVERY => 'На доставку',
            static::TYPE_SALE => 'На товар',
        ];
    }

    /**
     * @return Carbon|null
     */
    public function getStartAtAttribute()
    {
        $value = array_get($this->attributes, 'start_at');

        if (!$value) {
            return Carbon::now();
        }

        return new Carbon($value);
    }

    /**
     * @param User $user
     * @return bool|PromoCodeEvent
     * @throws \Throwable
     */
    public function activate(User $user)
    {
        $now = Carbon::now()->startOfDay();

        if ($this->start_at->greaterThan($now) || $this->expired_at->lessThan($now) || !$this->status) {
            return false;
        }

        $event = new PromoCodeEvent([
            'user_id' => $user->getKey(),
            'promo_code_id' => $this->getKey(),
            'code' => $this->code,
        ]);

        $event->saveOrFail();

        return $event;
    }

    /**
     * Обновление значения промокода в связаных событиях
     */
    public function beforeUpdate()
    {
        if ($this->isDirty('code')) {
            $this->events()
                ->update([
                    'code' => $this->code
                ]);
        }
    }
}
