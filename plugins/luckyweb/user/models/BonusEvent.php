<?php namespace LuckyWeb\User\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Model;

/**
 * BonusEvent Model
 *
 * @property int $amount
 * @property int $type_id
 * @property \Carbon\Carbon $applied_at
 * @property \Carbon\Carbon $expired_at
 */
class BonusEvent extends Model
{
    use SoftDeletes;

    const TYPE_ACCRUAL = 1;
    const TYPE_ACTIVATION = 2;
    const TYPE_CHARGE = 3;
    const TYPE_DEACTIVATION = 4;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_user_bonus_events';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'external_id',
        'user_id',
        'type_id',
        'amount',
        'description',
        'expired_at',
        'imported_at',
        'exported_at',
        'applied_at',
        'deleted_at'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'expired_at',
        'imported_at',
        'exported_at',
        'applied_at',
        'deleted_at'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'user' => ['LuckyWeb\User\Models\User'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function isNegative()
    {
        return $this->type_id == 3 || $this->type_id == 4;
    }
}
