<?php

namespace Luckyweb\User\Models;

use October\Rain\Database\Model;

/**
 *
 * * PromoCodeEvent Model
 *
 * @property string code
 * @property integer user_id
 * @property string provider_id
 * @property string social_id
 * @property string name
 * @property string last_name
 * @property string email
 *
 *
 * @mixin \Eloquent
 */
class SocialUser extends Model
{

    const GOOGLE_PROVIDER = 'google';
    const FACEBOOK_PROVIDER = 'facebook';
    const VK_PROVIDER = 'vkontakte';

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_user_social_users';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'user' => User::class,
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
