<?php

namespace LuckyWeb\User\Models;

use App\Domain\Entities\Account\Bonuses;
use App\Domain\Entities\Accounting\CartItem;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Config;
use Str;
use Auth;
use Mail;
use Event;
use October\Rain\Auth\Models\User as UserBase;
use LuckyWeb\User\Models\Settings as UserSettings;

/**
 * @property-read Bonuses $bonuses
 * @property-read array $bonuses_safe
 *
 * @mixin \Eloquent
 */
class User extends UserBase implements Authenticatable
{
    use \October\Rain\Database\Traits\SoftDeleting;

    /**
     * @var string The database table used by the model.
     */
    protected $table = 'users';

    /**
     * Validation rules
     */
    public $rules = [
        //'email'    => 'between:6,255|email|unique:users',
        'password' => 'between:4,255|confirmed',
        'password_confirmation' => 'between:4,255'
    ];

    /**
     * @var array Relations
     */
    public $hasMany = [
        'bonus_events' => BonusEvent::class,
        'cart_items' => CartItem::class,
        'promo_codes' => PromoCodeEvent::class,
    ];

    public $belongsToMany = [
        'groups' => ['LuckyWeb\User\Models\UserGroup', 'table' => 'users_groups']
    ];

    public $attachOne = [
        'avatar' => ['System\Models\File']
    ];

    /**
     * @var array The attributes that are mass assignable.
     */
    protected $fillable = [
        'name',
        'surname',
        'login',
        'username',
        'email',
        'password',
        'password_confirmation',
        'phone',
        'last_name',
        'other_name',
        'gender_id',
        'birth_date',
        'external_id',
        'card_code',
        'card_pin',
    ];

    /**
     * Purge attributes from data set.
     */
    protected $purgeable = ['password_confirmation', 'send_invite'];

    protected $dates = [
        'last_seen',
        'deleted_at',
        'created_at',
        'updated_at',
        'activated_at',
        'last_login',
        'birth_date'
    ];

    protected $jsonable = [
        'extended'
    ];

    public static $loginAttribute = null;

    /**
     * Sends the confirmation email to a user, after activating.
     * @param  string $code
     * @return void
     */
    public function attemptActivation($code)
    {
        $result = parent::attemptActivation($code);

        if ($result === false) {
            return false;
        }

        if ($mailTemplate = UserSettings::get('welcome_template')) {
            Mail::sendTo($this, $mailTemplate, $this->getNotificationVars());
        }

        Event::fire('luckyweb.user.activate', [$this]);

        return true;
    }

    /**
     * Converts a guest user to a registered one and sends an invitation notification.
     * @return void
     */
    public function convertToRegistered($sendNotification = true)
    {
        // Already a registered user
        if (!$this->is_guest) {
            return;
        }

        if ($sendNotification) {
            $this->generatePassword();
        }

        $this->is_guest = false;
        $this->save();

        if ($sendNotification) {
            $this->sendInvitation();
        }
    }

    //
    // Constructors
    //

    /**
     * Looks up a user by their email address.
     * @return self
     */
    public static function findByEmail($email)
    {
        if (!$email) {
            return;
        }

        return self::where('email', $email)->first();
    }

    /**
     * Looks up a user by their phone number.
     * @return self
     */
    public static function findByPhoneNumber($phoneNumber)
    {
        if (!$phoneNumber) {
            return;
        }

        return self::where('phone', $phoneNumber)->first();
    }

    //
    // Getters
    //

    /**
     * Gets a code for when the user is persisted to a cookie or session which identifies the user.
     * @return string
     */
    public function getPersistCode()
    {
        $block = UserSettings::get('block_persistence', false);

        if ($block || !$this->persist_code) {
            return parent::getPersistCode();
        }

        return $this->persist_code;
    }

    /**
     * Returns the public image file path to this user's avatar.
     */
    public function getAvatarThumb($size = 25, $options = null)
    {
        if (is_string($options)) {
            $options = ['default' => $options];
        }
        elseif (!is_array($options)) {
            $options = [];
        }

        // Default is "mm" (Mystery man)
        $default = array_get($options, 'default', 'mm');

        if ($this->avatar) {
            return $this->avatar->getThumb($size, $size, $options);
        }
        else {
            return '//www.gravatar.com/avatar/'.
                md5(strtolower(trim($this->email))).
                '?s='.$size.
                '&d='.urlencode($default);
        }
    }

    /**
     * Returns the name for the user's login.
     * @return string
     */
    public function getLoginName()
    {
        if (static::$loginAttribute !== null) {
            return static::$loginAttribute;
        }

        return static::$loginAttribute = UserSettings::get('login_attribute', UserSettings::LOGIN_EMAIL);
    }

    //
    // Scopes
    //

    public function scopeIsActivated($query)
    {
        return $query->where('is_activated', 1);
    }

    public function scopeFilterByGroup($query, $filter)
    {
        return $query->whereHas('groups', function($group) use ($filter) {
            $group->whereIn('id', $filter);
        });
    }

    /**
     * Search existing user by IDs
     */
    public function scopeSearchByIds($query, $id, $external_id)
    {
        return $query->where(function ($query) use ($id, $external_id) {
            if (!is_null($id)) {
                $query->orWhere('id', $id);
            }
            if (!is_null($external_id)) {
                $query->orWhere('external_id', $external_id);
            }
        });
    }

    /**
     * Search existing user that matches provided attributes
     */
    public function scopeSearchByAttributes($query, $attrs)
    {
        $query->where(function ($query) use ($attrs) {
            foreach ($attrs as $field => $value) {
                if(!empty($value)) {
                    $query->orWhere($field, $value);
                }
            }
        });
    }

    //
    // Events
    //

    public function beforeSave()
    {
        $this->phone = preg_replace('/[^0-9]+/u', '', $this->phone);
    }

    /**
     * Before validation event
     * @return void
     */
    public function beforeValidate()
    {
        /*
         * Guests are special
         */
        if ($this->is_guest && !$this->password) {
            $this->generatePassword();
        }

        /*
         * When the username is not used, the email is substituted.
         */
        if (
            (!$this->username) ||
            ($this->isDirty('email') && $this->getOriginal('email') == $this->username)
        ) {
            $this->username = $this->email;
        }
    }

    /**
     * After create event
     * @return void
     */
    public function afterCreate()
    {
        $this->restorePurgedValues();

        if ($this->send_invite) {
            $this->sendInvitation();
        }
    }

    /**
     * After login event
     * @return void
     */
    public function afterLogin()
    {
        $this->last_login = $this->last_seen = $this->freshTimestamp();

        if ($this->trashed()) {
            $this->restore();

            Mail::sendTo($this, 'luckyweb.user::mail.reactivate', [
                'name' => $this->name
            ]);

            Event::fire('luckyweb.user.reactivate', [$this]);
        }
        else {
            parent::afterLogin();
        }

        Event::fire('luckyweb.user.login', [$this]);
    }

    /**
     * After delete event
     * @return void
     */
    public function afterDelete()
    {
        if ($this->isSoftDelete()) {
            Event::fire('luckyweb.user.deactivate', [$this]);
            return;
        }

        $this->avatar && $this->avatar->delete();

        parent::afterDelete();
    }

    //
    // Banning
    //

    /**
     * Ban this user, preventing them from signing in.
     * @return void
     */
    public function ban()
    {
        Auth::findThrottleByUserId($this->id)->ban();
    }

    /**
     * Remove the ban on this user.
     * @return void
     */
    public function unban()
    {
        Auth::findThrottleByUserId($this->id)->unban();
    }

    /**
     * Check if the user is banned.
     * @return bool
     */
    public function isBanned()
    {
        $throttle = Auth::createThrottleModel()->where('user_id', $this->id)->first();
        return $throttle ? $throttle->is_banned : false;
    }

    //
    // Last Seen
    //

    /**
     * Checks if the user has been seen in the last 5 minutes, and if not,
     * updates the last_seen timestamp to reflect their online status.
     * @return void
     */
    public function touchLastSeen()
    {
        if ($this->isOnline()) {
            return;
        }

        $oldTimestamps = $this->timestamps;
        $this->timestamps = false;

        $this
            ->newQuery()
            ->where('id', $this->id)
            ->update(['last_seen' => $this->freshTimestamp()])
        ;

        $this->last_seen = $this->freshTimestamp();
        $this->timestamps = $oldTimestamps;
    }

    /**
     * Returns true if the user has been active within the last 5 minutes.
     * @return bool
     */
    public function isOnline()
    {
        return $this->getLastSeen() > $this->freshTimestamp()->subMinutes(5);
    }

    /**
     * Returns the date this user was last seen.
     * @return Carbon\Carbon
     */
    public function getLastSeen()
    {
        return $this->last_seen ?: $this->created_at;
    }

    /**
     * Check if this first visit in account
     * @return bool
     */
    public function isFirstVisit()
    {
        return array_get($this->extended, 'is_first_visit', true);
    }

    /**
     * Update first visit flag
     */
    public function touchFirstVisit()
    {
        $this->extended = array_merge(is_array($this->extended) ? $this->extended : [], [
            'is_first_visit' => false
        ]);
        $this->save();
    }

    /**
     * Generate card code and pin
     */
    public function generateCard()
    {
        if(empty($this->card_code)) {
            $digits = Config::get('luckyweb.ms::account_card_length') - 1;
            $this->card_code = 'S'.sprintf('%0'.$digits.'d', $this->id);
            $this->card_pin = sprintf('%04d', rand(1, 9999));
            $card = [];
            $card['card_code'] = $this->card_code;
            $card['card_pin'] = $this->card_pin;
            return $card;
        }
    }

    /**
     * Check is user registered by bonus invite link
     */
    public function getBonusRegistrationAttribute()
    {
        $bonus = BonusEvent::whereNull('imported_at')
            ->where('user_id', $this->id)
            ->where('type_id', BonusEvent::TYPE_ACTIVATION)
            ->first();
        return $bonus ? $bonus->amount : null;
    }

    //
    // Utils
    //

    /**
     * Returns the variables available when sending a user notification.
     * @return array
     */
    protected function getNotificationVars()
    {
        $vars = [
            'name'  => $this->name,
            'email' => $this->email,
            'card_code' => $this->card_code,
            'card_pin' => $this->card_pin,
            'username' => $this->username,
            'login' => $this->getLogin(),
            'password' => $this->getOriginalHashValue('password'),
        ];

        /*
         * Extensibility
         */
        $result = Event::fire('luckyweb.user.getNotificationVars', [$this]);
        if ($result && is_array($result)) {
            $vars = call_user_func_array('array_merge', $result) + $vars;
        }

        return $vars;
    }

    /**
     * Sends an invitation to the user using template "rainlab.user::mail.invite".
     * @return void
     */
    protected function sendInvitation()
    {
        Mail::sendTo($this, 'luckyweb.user::mail.invite', $this->getNotificationVars());
    }

    /**
     * Assigns this user with a random password.
     * @return void
     */
    protected function generatePassword()
    {
        $this->password = $this->password_confirmation = Str::random(6);
    }

    /**
     * @inheritDoc
     */
    public function getAuthIdentifierName()
    {
        return $this->getKeyName();
    }

    /**
     * @return Bonuses
     */
    public function getBonusesAttribute(): Bonuses
    {
        return new Bonuses($this->bonuses_safe);
    }

    /**
     * @return array
     */
    public function getBonusesSafeAttribute()
    {
        return array_merge([
            'accrued' => 0,
            'activated' => 0,
            'charged' => 0,
            'deactivated' => 0,
            'active_list' => [],
        ], array_get($this->extended, 'bonuses', []));
    }
}
