<?php namespace LuckyWeb\User\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateBonusEventsTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_user_bonus_events', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('external_id')->nullable()->default(null)->index();
            $table->integer('type_id')->unsigned()->nullable()->default(null)->index();
            $table->integer('user_id')->unsigned()->nullable()->default(null)->index();
            $table->decimal('amount', 10, 2)->nullable()->default(null);
            $table->text('description')->default(null)->nullable();
            $table->timestamp('applied_at')->nullable()->default(null);
            $table->timestamp('deleted_at')->nullable()->default(null)->index();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_user_bonus_events');
    }
}
