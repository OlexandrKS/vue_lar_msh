<?php namespace LuckyWeb\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateUsers20170828 extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->boolean('is_exported')->default(false);
            $table->timestamp('exported_at')->nullable()->default(null);
            $table->string('external_id')->nullable()->default(null)->unique()->after('id');
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->dropColumn('is_exported');
            $table->dropColumn('exported_at');
            $table->dropColumn('external_id');
        });
    }
}
