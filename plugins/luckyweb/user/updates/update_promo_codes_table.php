<?php

namespace LuckyWeb\User\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class UpdatePromoCodesTable extends Migration
{
    public function up()
    {
        if (! Schema::hasColumn('luckyweb_user_promo_codes', 'type')) {
            Schema::table('luckyweb_user_promo_codes', function ($table) {
                $table->integer('type')->default(0)->after('code');
            });
        }
    }

    public function down()
    {
        if (Schema::hasColumn('luckyweb_user_promo_codes', 'type')) {
            Schema::table('luckyweb_user_promo_codes', function ($table) {
                $table->dropColumn('type');
            });
        }
    }
}
