<?php namespace LuckyWeb\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateUsers20180321 extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->string('social_provider_id')->index()->nullable()->default(null);
            $table->string('social_id')->index()->nullable()->default(null);
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->dropColumn(['social_provider_id', 'social_id']);
        });
    }
}
