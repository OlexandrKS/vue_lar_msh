<?php namespace LuckyWeb\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateUsers20170823 extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->string('phone')->nullable()->default(null)->unique()->after('email');
            $table->string('card_code')->nullable()->default(null)->unique()->after('phone');
            $table->string('card_pin')->nullable()->default(null)->index()->after('card_code');
            $table->string('last_name')->nullable()->default(null)->after('name');
            $table->string('other_name')->nullable()->default(null)->after('last_name');
            $table->integer('gender_id')->nullable()->default(null)->unsigned()->after('card_pin');
            $table->date('birth_date')->nullable()->default(null)->after('gender_id');
            $table->mediumText("extended")->nullable()->after('birth_date');
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->dropColumn('phone');
            $table->dropColumn('card_code');
            $table->dropColumn('card_pin');
            $table->dropColumn('last_name');
            $table->dropColumn('other_name');
            $table->dropColumn('gender_id');
            $table->dropColumn('birth_date');
            $table->dropColumn('extended');
        });
    }
}
