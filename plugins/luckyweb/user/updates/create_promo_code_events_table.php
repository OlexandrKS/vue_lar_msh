<?php

namespace LuckyWeb\User\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class CreatePromoCodeEventsTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_user_promo_code_events', function(Blueprint $table) {
            $table->increments('id');
            $table->string('code', 100); // Normalisation
            $table->unsignedInteger('promo_code_id');
            $table->unsignedInteger('user_id');
            $table->timestamp('exported_at')->nullable();
            $table->timestamp('applied_at')->nullable();
            $table->timestamps();

            $table->unique(['promo_code_id', 'user_id'], 'unique_promo_code_for_user');
            $table->index(['promo_code_id', 'user_id', 'exported_at'], 'exchange_filter');
            $table->index(['code', 'user_id', 'applied_at'], 'user_apply_filter');
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_user_promo_code_events');
    }
}
