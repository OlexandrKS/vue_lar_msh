<?php namespace LuckyWeb\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateUsers20170925 extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->boolean('is_questionary_exported')->index()->default(true)->after('is_exported');
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->dropColumn('is_questionary_exported');
        });
    }
}
