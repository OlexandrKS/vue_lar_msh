<?php namespace LuckyWeb\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateUsers20190103 extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->string('ga_session_id')->nullable()->default(null)->index();
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->dropColumn('ga_session_id');
        });
    }
}
