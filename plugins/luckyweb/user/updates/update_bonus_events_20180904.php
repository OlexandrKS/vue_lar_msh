<?php namespace LuckyWeb\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateBonusEvents20180904 extends Migration
{
    public function up()
    {
        Schema::table('luckyweb_user_bonus_events', function($table)
        {
            $table->timestamp('expired_at')->nullable()->default(null)->index()->after('deleted_at');
            $table->timestamp('imported_at')->nullable()->default(null)->index()->after('expired_at');
            $table->timestamp('exported_at')->nullable()->default(null)->index()->after('imported_at');
        });
    }

    public function down()
    {
        Schema::table('luckyweb_user_bonus_events', function($table)
        {
            $table->dropColumn('expired_at');
            $table->dropColumn('imported_at');
            $table->dropColumn('exported_at');
        });
    }
}
