<?php namespace LuckyWeb\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateUsers20170914 extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->dropUnique('users_login_unique');
            $table->dropUnique('users_phone_unique');
            $table->dropUnique('users_email_unique');
            $table->string('password')->nullable()->default(null)->change();
            $table->string('email')->nullable()->default(null)->index()->change();
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->unique('login');
            $table->unique('phone');
            $table->unique('email');
            $table->dropIndex('users_email_index');
            $table->string('email')->change();
            $table->string('password')->change();
        });
    }
}
