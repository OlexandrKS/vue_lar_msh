<?php namespace Luckyweb\User\Updates;

use Luckyweb\User\Models\SocialUser;
use LuckyWeb\User\Models\User;
use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSocialUsersTable extends Migration
{
    public function up()
    {
        $users = User::select('id','social_id','social_provider_id')->whereNotNull('social_provider_id')->get();
        Schema::create('luckyweb_user_social_users', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->string('provider_id')->index();
            $table->string('social_id')->index();

            $table->unique(['provider_id', 'social_id'], 'unique_social_user');

            $table->timestamps();
        });
        foreach($users as $user){
            $socialUser = new SocialUser();
            $socialUser->user_id = $user->id;
            $socialUser->provider_id = $user->social_provider_id;
            $socialUser->social_id = $user->social_id;
            $socialUser->save();
        }
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_user_social_users');
    }
}
