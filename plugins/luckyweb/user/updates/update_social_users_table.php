<?php namespace Luckyweb\User\Updates;

use Luckyweb\User\Models\SocialUser;
use LuckyWeb\User\Models\User;
use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateSocialUsersTable extends Migration
{
    public function up()
    {
        Schema::table('luckyweb_user_social_users', function (Blueprint $table) {
            $table->string('name')->nullable()->after('social_id');
            $table->string('last_name')->nullable()->after('name');
            $table->string('email')->nullable()->after('last_name');

            $table->index('name');
            $table->index('last_name');
            $table->index('email');
        });
    }

    public function down()
    {
        Schema::table('luckyweb_user_social_users', function ($table) {
            $table->dropColumn('name');
            $table->dropColumn('last_name');
            $table->dropColumn('email');
        });
    }
}
