<?php namespace LuckyWeb\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateOrders20171117 extends Migration
{
    public function up()
    {
        Schema::table('luckyweb_user_orders', function($table)
        {
            $table->integer('status_id')->nullable()->default(null)->index()->after('user_id');
            $table->timestamp('status_updated_at')->nullable()->default(null)->index()->after('status_id');
        });
    }

    public function down()
    {
        Schema::table('luckyweb_user_orders', function($table)
        {
            $table->dropColumn('status_id');
            $table->dropColumn('status_updated_at');
        });
    }
}
