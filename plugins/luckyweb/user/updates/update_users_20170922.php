<?php namespace LuckyWeb\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateUsers20170922 extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->timestamp('questionary_exported_at')->nullable()->default(null)->index();
            $table->index('is_exported');
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->dropColumn('questionary_exported_at');
            $table->dropIndex('users_is_exported_index');
        });
    }
}
