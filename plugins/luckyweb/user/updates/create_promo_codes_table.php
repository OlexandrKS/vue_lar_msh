<?php

namespace LuckyWeb\User\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class CreatePromoCodesTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_user_promo_codes', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code', 100);
            $table->decimal('amount', 15, 2);
            $table->decimal('available_from', 15, 2)->default(0);
            $table->unsignedTinyInteger('status')->default(1);
            $table->timestamp('start_at')->useCurrent();
            $table->timestamp('expired_at')->nullable();
            $table->timestamps();

            $table->unique('code');
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_user_promo_codes');
    }
}
