<?php namespace LuckyWeb\User\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOrderItemsTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_user_order_items', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('order_id')->nullable()->default(null)->index();
            $table->string('good_id')->nullable()->default(null)->index();
            $table->string('color_id')->nullable()->default(null)->index();
            $table->string('name')->nullable()->default(null);
            $table->string('color')->nullable()->default(null);
            $table->decimal('amount', 15, 2)->nullable()->default(null);
            $table->decimal('quantity', 15, 3)->nullable()->default(1);
            $table->integer('status_id')->unsigned()->nullable()->default(null)->index();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_user_order_items');
    }
}
