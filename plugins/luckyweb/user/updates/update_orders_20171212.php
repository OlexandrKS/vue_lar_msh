<?php namespace LuckyWeb\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateOrders20171212 extends Migration
{
    public function up()
    {
        Schema::table('luckyweb_user_orders', function($table)
        {
            $table->mediumText('extended')->nullable()->default(null)->after('total_paid');
        });
    }

    public function down()
    {
        Schema::table('luckyweb_user_orders', function($table)
        {
            $table->dropColumn('extended');
        });
    }
}
