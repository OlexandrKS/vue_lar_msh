<?php namespace LuckyWeb\User\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateBonusInvitesTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_user_bonus_invites', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('name')->nullable()->default(null)->index();
            $table->string('code')->nullable()->default(null)->index();
            $table->decimal('amount', 10, 2)->nullable()->default(null);
            $table->timestamp('expired_at')->nullable()->default(null);
            $table->integer('status_id')->unsigned()->nullable()->default(1)->index();
            $table->integer('type_id')->unsigned()->nullable()->default(1)->index();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_user_bonus_invites');
    }
}
