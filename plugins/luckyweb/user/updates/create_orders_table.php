<?php namespace LuckyWeb\User\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_user_orders', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('external_id')->nullable()->default(null)->index();
            $table->integer('user_id')->unsigned()->nullable()->default(null)->index();
            $table->string('agreement_number')->nullable()->default(null)->index();
            $table->timestamp('agreement_date')->nullable()->default(null)->index();
            $table->timestamp('delivery_date')->nullable()->default(null)->index();

            $table->decimal('product_amount', 15, 2)->nullable()->default(null);
            $table->decimal('product_debt', 15, 2)->nullable()->default(null);
            $table->decimal('service_amount', 15, 2)->nullable()->default(null);
            $table->decimal('service_paid', 15, 2)->nullable()->default(null);
            $table->decimal('delivery_amount', 15, 2)->nullable()->default(null);
            $table->decimal('delivery_paid', 15, 2)->nullable()->default(null);
            $table->decimal('reclamation_amount', 15, 2)->nullable()->default(null);
            $table->decimal('reclamation_paid', 15, 2)->nullable()->default(null);

            $table->decimal('total_amount', 15, 2)->nullable()->default(null);
            $table->decimal('total_debt', 15, 2)->nullable()->default(null);
            $table->decimal('total_paid', 15, 2)->nullable()->default(null);

            $table->timestamp('applied_at')->nullable()->default(null)->index();
            $table->timestamp('deleted_at')->nullable()->default(null)->index();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_user_orders');
    }
}
