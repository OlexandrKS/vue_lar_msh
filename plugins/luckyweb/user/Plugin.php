<?php namespace LuckyWeb\User;

use App;
use Event;
use Backend;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use LuckyWeb\User\Components\GooglePlus;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;
use Illuminate\Foundation\AliasLoader;
use LuckyWeb\User\Models\MailBlocker;

class Plugin extends PluginBase
{
    /**
     * @var boolean Determine if this plugin should have elevated privileges.
     */
    public $elevated = true;

    public function pluginDetails()
    {
        return [
            'name'        => 'luckyweb.user::lang.plugin.name',
            'description' => 'luckyweb.user::lang.plugin.description',
            'author'      => 'Alexey Bobkov, Samuel Georges',
            'icon'        => 'icon-user',
            'homepage'    => 'https://github.com/rainlab/user-plugin'
        ];
    }

    public function register()
    {
        $alias = AliasLoader::getInstance();
        $alias->alias('Auth', 'LuckyWeb\User\Facades\Auth');

        App::singleton('user.auth', function() {
            return \LuckyWeb\User\Classes\AuthManager::instance();
        });

        /*
         * Apply user-based mail blocking
         */
        Event::listen('mailer.prepareSend', function($mailer, $view, $message){
            return MailBlocker::filterMessage($view, $message);
        });
    }

    public function boot()
    {
        parent::boot();

        $socialite = app()->make('Laravel\Socialite\Contracts\Factory');
        $socialite->extend(
            'vkontakte',
            function ($app) use ($socialite) {
                return $socialite->buildProvider(\SocialiteProviders\VKontakte\Provider::class, $app['config']['services.vkontakte']);
            }
        );
    }

    public function registerComponents()
    {
        return [
            'LuckyWeb\User\Components\Session'       => 'session',
            'LuckyWeb\User\Components\Account'       => 'account',
            'LuckyWeb\User\Components\ResetPassword' => 'resetPassword',
            'LuckyWeb\User\Components\AccountAside' => 'accountAside',
            'LuckyWeb\User\Components\AccountBonuses' => 'accountBonuses',
            'LuckyWeb\User\Components\AccountOrders' => 'accountOrders',
            'LuckyWeb\User\Components\SocialConfirmation' => 'socialConfirmation',
            'LuckyWeb\User\Components\BonusInviteHandler' => 'bonusInviteHandler',
            'LuckyWeb\User\Components\AccountPopupNotifications' => 'accountPopupNotification',
            'LuckyWeb\User\Components\ProdBoard' => 'prodBoard',
            'LuckyWeb\User\Components\PromoCodeActivation' => 'promoCodeActivation',
            'LuckyWeb\User\Components\PromoCode' => 'promoCode',
        ];
    }

    public function registerPermissions()
    {
        return [
            'luckyweb.users.access_users' => ['tab' => 'luckyweb.user::lang.plugin.tab', 'label' => 'luckyweb.user::lang.plugin.access_users'],
            'luckyweb.users.access_groups' => ['tab' => 'luckyweb.user::lang.plugin.tab', 'label' => 'luckyweb.user::lang.plugin.access_groups'],
            'luckyweb.users.access_settings' => ['tab' => 'luckyweb.user::lang.plugin.tab', 'label' => 'luckyweb.user::lang.plugin.access_settings'],
            'luckyweb.users.access_account_config' => ['tab' => 'luckyweb.user::lang.plugin.tab', 'label' => 'Управление кабинетом пользователей']
        ];
    }

    public function registerNavigation()
    {
        return [
            'user' => [
                'label'       => 'luckyweb.user::lang.users.menu_label',
                'url'         => Backend::url('luckyweb/user/users'),
                'icon'        => 'icon-user',
                'iconSvg'     => 'plugins/luckyweb/user/assets/images/user-icon.svg',
                'permissions' => ['luckyweb.users.*'],
                'order'       => 500,

                'sideMenu' => [
                    'users' => [
                        'label'       => 'luckyweb.user::lang.users.menu_label',
                        'url'         => Backend::url('luckyweb/user/users'),
                        'icon'        => 'icon-user',
                        'permissions' => ['luckyweb.users.*'],
                    ],
                    'bonusinvites' => [
                        'label'       => 'Бонусные ссылки',
                        'icon'        => 'icon-external-link-square',
                        'url'         => Backend::url('luckyweb/user/bonusinvites'),
                        'permissions' => ['luckyweb.users.access_account_config'],
                    ],
                    'promocodes' => [
                        'label'       => 'Промокоды',
                        'icon'        => 'icon-gift',
                        'url'         => Backend::url('luckyweb/user/promocodes'),
                        'permissions' => ['luckyweb.users.access_account_config'],
                    ],
                ],
            ]
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'luckyweb.user::lang.settings.menu_label',
                'description' => 'luckyweb.user::lang.settings.menu_description',
                'category'    => SettingsManager::CATEGORY_USERS,
                'icon'        => 'icon-cog',
                'class'       => 'LuckyWeb\User\Models\Settings',
                'order'       => 500,
                'permissions' => ['luckyweb.users.access_settings'],
            ]
        ];
    }

    public function registerMailTemplates()
    {
        return [
            'luckyweb.user::mail.activate'   => 'Activation email sent to new users.',
            'luckyweb.user::mail.welcome'    => 'Welcome email sent when a user is activated.',
            'luckyweb.user::mail.restore'    => 'Password reset instructions for front-end users.',
            'luckyweb.user::mail.new_user'   => 'Sent to administrators when a new user joins.',
            'luckyweb.user::mail.reactivate' => 'Notification for users who reactivate their account.',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function registerMarkupTags()
    {
        return [
            'filters' => [
                'int' => function($value) {
                    return intval($value);
                },
                'phone' => function($value) {
                    if(strlen($value) != 11) {
                        return $value;
                    }

                    $code = substr($value, 0,1);
                    $area = substr($value, 1,3);
                    $prefix = substr($value,4,3);
                    $number1 = substr($value,7,2);
                    $number2 = substr($value,9,2);
                    return '+'.$code.' ('.$area.') '.$prefix.'-'.$number1.'-'.$number2;
                },
                'phoneEdit' => function($value) {
                    return ltrim($value, '7');
                },
                'money' => function($value) {
                    return number_format($value, 0, '.', ' ');
                },
            ],
            'functions' => [
                'isIp' => function($ip) {
                    return Request::ip() == $ip;
                },
            ]
        ];
    }
}
