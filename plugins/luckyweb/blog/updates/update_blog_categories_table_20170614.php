<?php namespace LuckyWeb\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateBlogCategoriesTable20170614 extends Migration
{

    public function up()
    {
        if (Schema::hasTable('rainlab_blog_categories')) {
            Schema::table('rainlab_blog_categories', function ($table) {
                $table->integer('site_id')->unsigned()->index()->nullable()->default(null)->after('parent_id');
            });
        }
    }

    public function down()
    {
        if (Schema::hasTable('rainlab_blog_categories'))
        {
            Schema::table('rainlab_blog_categories', function($table)
            {
                $columns = [
                    'site_id'
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('rainlab_blog_categories', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }

}