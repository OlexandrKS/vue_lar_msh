<?php namespace LuckyWeb\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateBlogPostsTable20170116 extends Migration
{

    public function up()
    {
        if (Schema::hasTable('rainlab_blog_posts')) {
            Schema::table('rainlab_blog_posts', function ($table) {
                $table->text('description')->nullable()->default(null)->after('excerpt');
            });
        }
    }

    public function down()
    {
        if (Schema::hasTable('rainlab_blog_posts'))
        {
            Schema::table('rainlab_blog_posts', function($table)
            {
                $columns = [
                    'description'
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('rainlab_blog_posts', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
