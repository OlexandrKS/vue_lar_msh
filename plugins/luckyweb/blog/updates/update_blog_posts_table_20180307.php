<?php namespace LuckyWeb\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateBlogPostsTable20180307 extends Migration
{

    public function up()
    {
        if (Schema::hasTable('rainlab_blog_posts')) {
            Schema::table('rainlab_blog_posts', function ($table) {
                $table->boolean('is_top')->nullable()->default(false)->after('published')->index();
            });
        }
    }

    public function down()
    {
        if (Schema::hasTable('rainlab_blog_posts'))
        {
            Schema::table('rainlab_blog_posts', function($table)
            {
                $columns = [
                    'is_top'
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('rainlab_blog_posts', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
