<?php namespace LuckyWeb\Blog\Components;

use Cms\Classes\ComponentBase;
use LuckyWeb\Blog\Models\Post;
//use LuckyWeb\MS\Models\Site;

class Top extends ComponentBase
{
    /**
     * Post list
     * @var Collection
     */
    public $posts;

    public function componentDetails()
    {
        return [
            'name'        => 'Top',
            'description' => 'Display blog top posts'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->posts = Post::isPublished()
            /*->whereHas('categories', function($query){
                $query->where('site_id', Site::getCurrentSite()->id);
            })*/
            ->where('is_top', true)
            ->orderBy('published_at', 'DESC')
            ->take(5)
            ->get();

        /*
         * Add a "url" helper attribute for linking to each post and category
         */
        $this->posts->each(function($post) {
            $post->setUrl('blog-post', $this);
        });
    }
}