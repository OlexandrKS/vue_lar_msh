<?php namespace LuckyWeb\Blog\Components;

use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use LuckyWeb\Blog\Models\Post as BlogPost;
//use LuckyWeb\MS\Models\Site;
//use Illuminate\Support\Facades\Redirect;
//use RainLab\Translate\Classes\Translator;

class Post extends ComponentBase
{
    /**
     * @var LuckyWeb\Blog\Models\Post The post model used for display.
     */
    public $post;

    /**
     * @var string Reference to the page name for linking to categories.
     */
    public $categoryPage;

    public function componentDetails()
    {
        return [
            'name'        => 'luckyweb.blog::lang.settings.post_title',
            'description' => 'luckyweb.blog::lang.settings.post_description'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => 'luckyweb.blog::lang.settings.post_slug',
                'description' => 'luckyweb.blog::lang.settings.post_slug_description',
                'default'     => '{{ :slug }}',
                'type'        => 'string'
            ],
            'categoryPage' => [
                'title'       => 'luckyweb.blog::lang.settings.post_category',
                'description' => 'luckyweb.blog::lang.settings.post_category_description',
                'type'        => 'dropdown',
                'default'     => 'blog/category',
            ],
        ];
    }

    public function getCategoryPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onRun()
    {
        $this->categoryPage = $this->page['categoryPage'] = $this->property('categoryPage');
        $this->post = $this->page['post'] = $this->loadPost();

        if($redirect = $this->getSlugTitleFixRedirect()) {
            return $redirect;
        }

        $this->page['controller'] = $this->controller;
    }

    protected function loadPost()
    {
        $slug = $this->property('slug');

        $post = new BlogPost;

        $post = $post->isClassExtendedWith('RainLab.Translate.Behaviors.TranslatableModel')
            ? $post->transWhere('slug', $slug)
            : $post->where('slug', $slug);

        $post = $post/*->whereHas('categories', function($query){
            $query->where('site_id', Site::getCurrentSite()->id);
        })*/->isPublished()->first();

        /*
         * Add a "url" helper attribute for linking to each category
         */
        if ($post && $post->categories->count()) {
            $post->categories->each(function($category) {
                $category->setUrl($this->categoryPage, $this->controller);
            });
        }

        return $post;
    }

    public function previousPost()
    {
        return $this->getPostSibling(-1);
    }

    public function nextPost()
    {
        return $this->getPostSibling(1);
    }

    protected function getPostSibling($direction = 1)
    {
        if (!$this->post) {
            return;
        }

        $method = $direction === -1 ? 'previousPost' : 'nextPost';

        if (!$post = $this->post->$method()) {
            return;
        }

        $postPage = $this->getPage()->getBaseFileName();

        $post->setUrl($postPage, $this->controller);

        $post->categories->each(function($category) {
            $category->setUrl($this->categoryPage, $this->controller);
        });

        return $post;
    }

    /**
     * Return redirect for uk slug
     * @return mixed
     */
    protected function getSlugTitleFixRedirect()
    {
        /*$translator = Translator::instance();

        if($translator->getLocale() == 'uk' && $this->post->slug != $this->property('slug')) {
            $this->post->setUrl($this->getPage()->getBaseFileName(), $this->controller);
            return Redirect::to($this->post->url, 301);
        }*/

        return null;
    }
}
