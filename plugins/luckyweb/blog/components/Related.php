<?php namespace LuckyWeb\Blog\Components;

use Cms\Classes\ComponentBase;
use LuckyWeb\Blog\Models\Post as PostModel;
//use LuckyWeb\MS\Models\Site;

class Related extends ComponentBase
{
    public $posts;

    public function componentDetails()
    {
        return [
            'name'        => 'Related Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'post' => [
                'title'       => 'Current post',
                'description' => 'Current post',
                'default'     => '',
                'type'        => 'object',
            ],
        ];
    }

    public function onRender()
    {
        $post = $this->property('post');
        $categoryIds = $post->categories()->lists('id');

        $this->posts = PostModel::isPublished()
            ->where('id', '!=', $post->id)
            ->whereHas('categories', function($q) use ($categoryIds) {
                $q->whereIn('id', $categoryIds);
                    //->where('site_id', Site::getCurrentSite()->id);
            })
            ->orderBy('published_at', 'DESC')
            ->take(3)
            ->get();

        /*
         * Add a "url" helper attribute for linking to each post and category
         */
        $this->posts->each(function($post) {
            $post->setUrl('blog-post', $this);
        });
    }
}