<?php namespace LuckyWeb\Blog;

use Backend;
use Controller;
use LuckyWeb\Blog\Models\Post;
use System\Classes\PluginBase;
use LuckyWeb\Blog\Classes\TagProcessor;
use LuckyWeb\Blog\Models\Category;
use Event;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'luckyweb.blog::lang.plugin.name',
            'description' => 'luckyweb.blog::lang.plugin.description',
            'author'      => 'LuckyWeb',
            'icon'        => 'icon-pencil',
            'homepage'    => 'https://github.com/rainlab/blog-plugin'
        ];
    }

    public function registerComponents()
    {
        return [
            'LuckyWeb\Blog\Components\Post'       => 'blogPost',
            'LuckyWeb\Blog\Components\Posts'      => 'blogPosts',
            'LuckyWeb\Blog\Components\Categories' => 'blogCategories',
            'LuckyWeb\Blog\Components\RssFeed'    => 'blogRssFeed',
            'LuckyWeb\Blog\Components\Top'        => 'blogTop',
            'LuckyWeb\Blog\Components\Related'    => 'blogRelated',
        ];
    }

    public function registerPermissions()
    {
        return [
            'luckyweb.blog.access_posts' => [
                'tab'   => 'luckyweb.blog::lang.blog.tab',
                'label' => 'luckyweb.blog::lang.blog.access_posts'
            ],
            'luckyweb.blog.access_categories' => [
                'tab'   => 'luckyweb.blog::lang.blog.tab',
                'label' => 'luckyweb.blog::lang.blog.access_categories'
            ],
            'luckyweb.blog.access_other_posts' => [
                'tab'   => 'luckyweb.blog::lang.blog.tab',
                'label' => 'luckyweb.blog::lang.blog.access_other_posts'
            ],
            'luckyweb.blog.access_import_export' => [
                'tab'   => 'luckyweb.blog::lang.blog.tab',
                'label' => 'luckyweb.blog::lang.blog.access_import_export'
            ],
            'luckyweb.blog.access_publish' => [
                'tab'   => 'luckyweb.blog::lang.blog.tab',
                'label' => 'luckyweb.blog::lang.blog.access_publish'
            ]
        ];
    }

    public function registerNavigation()
    {
        return [
            'blog' => [
                'label'       => 'luckyweb.blog::lang.blog.menu_label',
                'url'         => Backend::url('luckyweb/blog/posts'),
                'icon'        => 'icon-pencil',
                'iconSvg'     => 'plugins/luckyweb/blog/assets/images/blog-icon.svg',
                'permissions' => ['luckyweb.blog.*'],
                'order'       => 300,

                'sideMenu' => [
                    'new_post' => [
                        'label'       => 'luckyweb.blog::lang.posts.new_post',
                        'icon'        => 'icon-plus',
                        'url'         => Backend::url('luckyweb/blog/posts/create'),
                        'permissions' => ['luckyweb.blog.access_posts']
                    ],
                    'posts' => [
                        'label'       => 'luckyweb.blog::lang.blog.posts',
                        'icon'        => 'icon-copy',
                        'url'         => Backend::url('luckyweb/blog/posts'),
                        'permissions' => ['luckyweb.blog.access_posts']
                    ],
                    'categories' => [
                        'label'       => 'luckyweb.blog::lang.blog.categories',
                        'icon'        => 'icon-list-ul',
                        'url'         => Backend::url('luckyweb/blog/categories'),
                        'permissions' => ['luckyweb.blog.access_categories']
                    ]
                ]
            ]
        ];
    }

    public function registerFormWidgets()
    {
        return [
            'LuckyWeb\Blog\FormWidgets\Preview' => [
                'label' => 'Preview',
                'code'  => 'preview'
            ]
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     */
    public function register()
    {
        /*
         * Register the image tag processing callback
         */
        TagProcessor::instance()->registerCallback(function($input, $preview) {
            if (!$preview) {
                return $input;
            }

            return preg_replace('|\<img src="image" alt="([0-9]+)"([^>]*)\/>|m',
                '<span class="image-placeholder" data-index="$1">
                    <span class="upload-dropzone">
                        <span class="label">Click or drop an image...</span>
                        <span class="indicator"></span>
                    </span>
                </span>',
            $input);
        });
    }

    public function boot()
    {
        /*
         * Register menu items for the RainLab.Pages plugin
         */
        Event::listen('pages.menuitem.listTypes', function() {
            return [
                'blog-category'       => 'luckyweb.blog::lang.menuitem.blog_category',
                'all-blog-categories' => 'luckyweb.blog::lang.menuitem.all_blog_categories',
                'blog-post'           => 'luckyweb.blog::lang.menuitem.blog_post',
                'all-blog-posts'      => 'luckyweb.blog::lang.menuitem.all_blog_posts',
            ];
        });

        Event::listen('pages.menuitem.getTypeInfo', function($type) {
            if ($type == 'blog-category' || $type == 'all-blog-categories') {
                return Category::getMenuTypeInfo($type);
            }
            elseif ($type == 'blog-post' || $type == 'all-blog-posts') {
                return Post::getMenuTypeInfo($type);
            }
        });

        Event::listen('pages.menuitem.resolveItem', function($type, $item, $url, $theme) {
            if ($type == 'blog-category' || $type == 'all-blog-categories') {
                return Category::resolveMenuItem($item, $url, $theme);
            }
            elseif ($type == 'blog-post' || $type == 'all-blog-posts') {
                return Post::resolveMenuItem($item, $url, $theme);
            }
        });
    }
}
