<?php
//get products in search dropdown
Route::get(
    'api/luckyweb/search/products/{query?}/{count?}',
    'LuckyWeb\Search\Http\Controllers\SearchController@sendQuery'
);