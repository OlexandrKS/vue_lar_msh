<?php namespace Luckyweb\Search;

use Backend;
use System\Classes\PluginBase;

/**
 * search Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'search',
            'description' => 'No description provided yet...',
            'author'      => 'luckyweb',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConsoleCommand('search.esset', 'LuckyWeb\Search\Console\ElasticSearchSettings');
        $this->registerConsoleCommand('search.esindex', 'LuckyWeb\Search\Console\ElasticSearchIndex');
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Luckyweb\Search\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'luckyweb.search.some_permission' => [
                'tab' => 'search',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'search' => [
                'label'       => 'search',
                'url'         => Backend::url('luckyweb/search/search'),
                'icon'        => 'icon-leaf',
                'permissions' => ['luckyweb.search.*'],
                'order'       => 500,
            ],
        ];
    }
}
