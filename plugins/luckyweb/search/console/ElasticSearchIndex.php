<?php

namespace LuckyWeb\Search\Console;

use App\Backend\Entities\Constructor\Page;
use App\Backend\Enums\Constructor\PageType;
use App\ServiceLayer\Sentry\Sentry;
use Illuminate\Console\Command;
use LuckyWeb\MS\Models\Product;
use GuzzleHttp;

use Cviebrock\LaravelElasticsearch\Facade as Elasticsearch;

class ElasticSearchIndex extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'search:esindex';

    /**
     * @var string The console command description.
     */
    protected $description = 'Index tables to ElasticSearch.';

    /**
     * @var GuzzleHttp\Client
     */
    private $httpClient;

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $uri = config('elasticsearch.connections.default.hosts.scheme', 'http') . '://' .
            config('elasticsearch.connections.default.hosts.host', 'localhost') . ':' .
            config('elasticsearch.connections.default.hosts.port', '9200');

        $this->httpClient = new GuzzleHttp\Client([
            'base_uri' => $uri
        ]);

        try {
            $this->info('Deleting index entries...');
            $this->clearIndex('categories', 'category');
            $this->clearIndex('products', 'product');

            $this->info('Seeding index entries...');
            $products = $this->getProducts();
            $categories = $this->getCatalogCategories();

            $this->info('Creating category index...');
            $this->createCategoryIndex($categories);

            $this->info(PHP_EOL . 'Creating product index...');
            $this->createProductIndex($products);
        } catch (\Exception $e) {
            Sentry::captureException($e);
        };
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

    /**
     * @param string $index
     * @param string $type
     * @return object elasticsearch
     */
    public function clearIndex($index, $type)
    {

        $result = Elasticsearch::deleteByQuery([
            'index' => $index,
            'type' => $type,
            'body' => [
                'query' => [
                    'match_all' => new \stdClass()
                ]
            ]
        ]);
        $this->info('Type "' . $type . '" at index "' . $index . '" deleted!');
        return $result;
    }

    /**
     * create category index in elasticsearch
     * @param array $categories
     *
     */
    public function createCategoryIndex($categories)
    {
        $categoryBar = $this->output->createProgressBar(count($categories));

        foreach ($categories as $type) {
            $this->httpClient->put('/categories/_doc/'.$type['id'],[
                GuzzleHttp\RequestOptions::JSON => $type
            ]);
            $categoryBar->advance();
        }
        $categoryBar->finish();
    }

    /**
     * Get products with preview url picture
     * @return array products
     */
    public function getProducts()
    {
        $products = Product::has('images')
            ->get();

        $products->load('images', 'good_type_site');

        foreach ($products as $product) {
            $product->setAttribute('image_preview', $product->preview_image->url_preview);
            $product->setAttribute('image_preview_desktop',  $product->preview_desktop_image->url_preview);
            $product->setAttribute('image_preview_mobile', $product->preview_mobile_image->url_preview);

        }

        $arrProducts = $products->toArray();
        return $arrProducts;
    }

    /**
     * Get categories with page slug
     * @return array products
     */
    public function getCatalogCategories()
    {
        return Page::whereIn('type', [PageType::CATEGORY, PageType::CATEGORY_LIST])
            ->get()
            ->toArray();
    }

    /**
     * create product index in elasticsearch
     * @param array $products *
     */
    public function createProductIndex($products)
    {
        $productBar = $this->output->createProgressBar(count($products));

        foreach ($products as $product) {
            $this->httpClient->put('/products/_doc/'.$product['id'],[
                GuzzleHttp\RequestOptions::JSON => $product
            ]);
            $productBar->advance();
        }
        $productBar->finish();
    }
}
