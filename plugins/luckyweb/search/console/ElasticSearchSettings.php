<?php

namespace LuckyWeb\Search\Console;

use App\ServiceLayer\Sentry\Sentry;
use Illuminate\Console\Command;
use GuzzleHttp;

use Cviebrock\LaravelElasticsearch\Facade as Elasticsearch;

class ElasticSearchSettings extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'search:esset';

    /**
     * @var string The console command description.
     */
    protected $description = 'Settings and mapping in ElasticSearch.';

    /**
     * @var GuzzleHttp\Client
     */
    private $httpClient;

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $uri = config('elasticsearch.connections.default.hosts.scheme', 'http') . '://' .
            config('elasticsearch.connections.default.hosts.host', 'localhost') . ':' .
            config('elasticsearch.connections.default.hosts.port', '9200');

        $this->httpClient = new GuzzleHttp\Client([
            'base_uri' => $uri
        ]);

        try {
            $this->info('Configuration in progress...');
            $this->setSettings();
            $this->info('Success!');
        } catch (\Exception $e) {
            Sentry::captureException($e);
        };
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

    /**
     * set mapping and settings for elasticsearch
     * @return void
     */
    public function setSettings()
    {
        try {
            $this->httpClient->delete('/categories');
        } catch (\Exception $exception) {}

        $this->httpClient->put('/categories', [
            GuzzleHttp\RequestOptions::JSON => [
                'settings' => [
                    'analysis' => [
                        'analyzer' => [
                            'autocomplete' => [
                                'tokenizer' => 'autocomplete',
                                'filter' => [
                                    'lowercase',
                                ],
                            ],
                            'autocomplete_search' => [
                                'tokenizer' => 'lowercase',
                            ],
                        ],
                        'tokenizer' => [
                            'autocomplete' => [
                                'type' => 'edge_ngram',
                                'min_gram' => 1,
                                'max_gram' => 10,
                                'token_chars' => [
                                    'letter'
                                ]
                            ]
                        ]
                    ]
                ],
                'mappings' => [
                    'dynamic_templates' => [
                        [
                            'name_analyzers' => [
                                'match' => 'name',
                                'mapping' => [
                                    'type' => 'text',
                                    'analyzer' => 'autocomplete',
                                    'search_analyzer' => 'autocomplete_search',
                                ]
                            ]
                        ]
                    ]
                ]
//                'mappings' => [
//                    'category' => [
//                        'properties' => [
//                            'name' => [
//                                'type' => 'text',
//                                'analyzer' => 'autocomplete',
//                                'search_analyzer' => 'autocomplete_search',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'slug' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'id' => [
//                                'type' => 'text'
//                            ],
//                            'title' => [
//                                'type' => 'text'
//                            ],
//                            'breadcrumbs' => [
//                                'type' => 'text'
//                            ],
//                            'type' => [
//                                'type' => 'text'
//                            ],
//                            'description' => [
//                                'type' => 'text'
//                            ]
//                        ]
//                    ]
//                ]
            ]
        ]);

        try {
            $this->httpClient->delete('/products');
        } catch (\Exception $exception) {}

        $this->httpClient->put('/products', [
            GuzzleHttp\RequestOptions::JSON => [
                'settings' => [
                    'analysis' => [
                        'analyzer' => [
                            'autocomplete' => [
                                'tokenizer' => 'autocomplete',
                                'filter' => [
                                    'lowercase',
                                ],
                            ],
                            'autocomplete_search' => [
                                'tokenizer' => 'lowercase',
                            ],
                        ],
                        'tokenizer' => [
                            'autocomplete' => [
                                'type' => 'edge_ngram',
                                'min_gram' => 1,
                                'max_gram' => 10,
                                'token_chars' => [
                                    'letter'
                                ]
                            ]
                        ]
                    ]
                ],
                'mappings' => [
                    'dynamic_templates' => [
                        [
                            'type_name_analyzers' => [
                                'path_match' => 'good_type_site.type_name',
                                'mapping' => [
                                    'type' => 'text',
                                    'analyzer' => 'autocomplete',
                                    'search_analyzer' => 'autocomplete_search',
                                ]
                            ]
                        ],
                        [
                            'name_analyzers' => [
                                'match' => 'name',
                                'mapping' => [
                                    'type' => 'text',
                                    'analyzer' => 'autocomplete',
                                    'search_analyzer' => 'autocomplete_search',
                                ]
                            ]
                        ]
                    ]
                ]
//                'mappings' => [
//                    'product' => [
//                        'properties' => [
//                            'also_in_spec_list' => [
//                                'type' => 'long',
//                            ],
//                            'assembling_cost' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'installments' => [
//                                'type' => 'text'
//                            ],
//                            'category_id' => [
//                                'type' => 'long'
//                            ],
//                            'category_invisible' => [
//                                'type' => 'long'
//                            ],
//                            'color_id' => [
//                                'type' => 'long'
//                            ],
//                            'depth' => [
//                                'type' => 'long'
//                            ],
//                            'discount_b' => [
//                                'type' => 'long'
//                            ],
//                            'discount_promo' => [
//                                'type' => 'long'
//                            ],
//                            'discount_special' => [
//                                'type' => 'long'
//                            ],
//                            'furnisher_id' => [
//                                'type' => 'long'
//                            ],
//                            'good_id' => [
//                                'type' => 'long'
//                            ],
//                            'good_type_id' => [
//                                'type' => 'long'
//                            ],
//                            'has_mixed_promo' => [
//                                'type' => 'long'
//                            ],
//                            'height' => [
//                                'type' => 'long'
//                            ],
//                            'max_order_item' => [
//                                'type' => 'long'
//                            ],
//                            'package_id' => [
//                                'type' => 'long'
//                            ],
//                            'position_column' => [
//                                'type' => 'long'
//                            ],
//                            'position_mp_column' => [
//                                'type' => 'long'
//                            ],
//                            'position_mp_row' => [
//                                'type' => 'long'
//                            ],
//                            'position_row' => [
//                                'type' => 'long'
//                            ],
//                            'residual_indicator' => [
//                                'type' => 'long'
//                            ],
//                            'set_id' => [
//                                'type' => 'long'
//                            ],
//                            'site_type_id' => [
//                                'type' => 'long'
//                            ],
//                            'site_width' => [
//                                'type' => 'long'
//                            ],
//                            'sub_category_id' => [
//                                'type' => 'long'
//                            ],
//                            'width' => [
//                                'type' => 'long'
//                            ],
//                            'week_promo' => [
//                                'type' => 'long'
//                            ],
//                            'created_at' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'updated_at' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'video_url' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'variant_of_design' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'type_name_for_page' => [
//                                'type' => 'text',
//                                'analyzer' => 'autocomplete',
//                                'search_analyzer' => 'autocomplete_search',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'units' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'subname' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'price_top_sale' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'slug' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'price_top_b' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'price_special' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'price_promo' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'price_lower_sale' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'image_preview' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'image_preview_desktop' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'image_preview_mobile' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'price_lower_b' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'price_description' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'delivery_cost' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'description' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 512
//                                    ]
//                                ]
//                            ],
//                            'description1' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'description1_name' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'description2' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'description2_name' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'dimensions' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'discount_condition' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'discount_sale' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'id' => [
//                                'type' => 'text',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                            'name' => [
//                                'type' => 'text',
//                                'analyzer' => 'autocomplete',
//                                'search_analyzer' => 'autocomplete_search',
//                                'fields' => [
//                                    'keyword' => [
//                                        'type' => 'keyword',
//                                        'ignore_above' => 256
//                                    ]
//                                ]
//                            ],
//                        ]
//                    ]
//                ]
            ]
        ]);
    }
}
