<?php namespace LuckyWeb\Search\Http\Controllers;
/**
 * Created by PhpStorm.
 * User: User
 * Date: 21.02.2019
 * Time: 16:19
 */
use Illuminate\Routing\Controller;
use LuckyWeb\MS\Models\SiteGoodType;
use LuckyWeb\MS\Models\Product;
use Illuminate\Support\Facades\Input;
use Cviebrock\LaravelElasticsearch\Facade as Elasticsearch;
use LuckyWeb\MS\Models\CatalogCategory;

/**
 * @todo refactor
 */
class SearchController extends Controller
{

    public function __construct()
    {

    }

    //get searching data from categories index
    public function getCatalogCategories($query){
        if (config('elasticsearch.enabled')) {
            $search_query = Elasticsearch::search([
                'index' => 'categories',
                'size' => 3,
                'body' => [
                    'query' => [
                        'dis_max' => [
                            'queries' => [
                                ['multi_match' => [
                                    'type' => 'most_fields',
                                    'query' => $query,
                                    'boost' => 6.0,
                                    'fields' => ['name'],
                                ]]
                            ],
                        ]
                    ]
                ]
            ]);

            $search_results = $search_query['hits'];

            $search_hits = [];
            foreach ($search_results['hits'] as $result) {
                $search_hits [] = $result['_source'];
            }

            return $search_hits;
        }
    }

    //gather data from input query
    public function sendQuery($query, $count){
        if (config('elasticsearch.enabled')) {
            $categories = $this->getCatalogCategories($query);
            $search_query = Elasticsearch::search([
                'index' => 'products',
                'size' => $count,
                'body' => [
                    'query' => [
                        'bool' => [
                            'should' => [
                                    [['multi_match' => [
                                        'type' => 'phrase',
                                        'query' => $query,
                                        'fields' => ['good_type_site.type_name^2', 'name'],
                                        'boost' => 20.0

                                    ]]]
                            ],
                            'must_not' => [
                                ['term' => [
                                    'category_invisible' => 1
                                ]],
                                ['term' => [
                                    'also_in_spec_list' => 1
                                ]]
                            ],
                        ]
                    ]
                ]
            ]);
            $search_results = $search_query['hits'];

            $search_hits = [];
            foreach ($search_results['hits'] as $result) {
                $search_hits [] = $result['_source'];
            }

            $groups = [];

            //group for null-sitecategory products
            $otherGroup = new SiteGoodType([
                'type_id' => 666,
                'type_name' => 'Без категории',
                'width' => 300,
            ]);

            $products = hydrate(Product::class, $search_hits);
            $products->load('category');
            /** @var Product $product */
            $index = 0;


            //remap array as $name_of_group => $products
            foreach ($products as $product) {
                if (is_null($product->good_type_site)) {
                    $product->site_type_id = $otherGroup->type_id;
                    $product->setRelation('good_type_site', $otherGroup);
                }

                $groups[$product->good_type_site->type_name][] = collect($product);
            }

            $groups = collect($groups);

            $response = [
                'categories' => $categories,
                'groups' => $groups
            ];
            return $response;
        }
    }
}
