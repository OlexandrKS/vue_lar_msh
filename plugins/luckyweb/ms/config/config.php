<?php  
return [
    'image_path' => storage_path().'/images',
    'image_minimized_path' => storage_path().'/images/minimized',
    'image_preview_path' => storage_path().'/images/preview',
    'image_url_path' => str_replace(env('IS_WINDOWS', false) ? base_path().'\\' : base_path(), '', storage_path()).'/images',
    'image_url_minimized_path' => str_replace(env('IS_WINDOWS', false) ? base_path().'\\' : base_path(), '', storage_path()).'/images/minimized',
    'image_url_preview_path' => str_replace(env('IS_WINDOWS', false) ? base_path().'\\' : base_path(), '', storage_path()).'/images/preview',

    'mail_username' => env('MAIL_USERNAME'),
    'mail_password' => env('MAIL_PASSWORD'),
    'mail_path' => env('MAIL_PATH'),

    'account_card_length' => 7,
];
