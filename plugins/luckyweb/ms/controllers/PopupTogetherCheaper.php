<?php namespace LuckyWeb\MS\Controllers;

use BackendMenu;
use Redirect;
use Backend\Classes\Controller;
use October\Rain\Support\Facades\Flash;

/**
 * Popup Together Cheaper Back-end Controller
 */
class PopupTogetherCheaper extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('LuckyWeb.MS', 'pagepopup', 'popuptogethercheaper');
    }

    /**
     * @return mixed
     */
    public function onCopy()
    {
        $result = new PopupTogetherCheaper();
        $result->fill(request()->PopupTogetherCheaper);
        $result->save();

        //Send success message
        Flash::success('Popup Together Cheaper copied success!');

        return Redirect::to('/backend/luckyweb/ms/popuptogethercheaper/update/' . $result->id);
    }

    public function listExtendQuery($query, $definition = null)
    {
        $query->whereNull('html');
    }
}
