<?php namespace LuckyWeb\MS\Controllers;

use BackendMenu;
use Redirect;
use Backend\Classes\Controller;
use October\Rain\Support\Facades\Flash;
use Luckyweb\Ms\Models\PagePopup as PagePopupModel;

/**
 * Page Popup Back-end Controller
 */
class PagePopup extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('LuckyWeb.MS', 'pagepopup', 'pagepopup');
    }

    /**
     * @return mixed
     */
    public function onCopy()
    {
        $result = new PagePopupModel;
        $result->fill(request()->PagePopup);
        $result->save();

        //Send success message
        Flash::success('PagePopup "'.$result->popup_name.'" copied success!');

        return Redirect::to('/backend/luckyweb/ms/pagepopup/update/'.$result->id);
    }

    public function listExtendQuery($query, $definition = null)
    {
        $query->whereNull('html');
    }
}
