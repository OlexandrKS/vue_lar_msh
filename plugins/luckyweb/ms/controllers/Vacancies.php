<?php namespace LuckyWeb\MS\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Vacancies Back-end Controller
 */
class Vacancies extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('LuckyWeb.MS', 'hr', 'vacancies');

        $this->addCss('/plugins/luckyweb/ms/assets/css/backend.css?'.time());
    }
}
