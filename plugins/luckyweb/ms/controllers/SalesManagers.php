<?php namespace LuckyWeb\MS\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Sales Managers Back-end Controller
 */
class SalesManagers extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('LuckyWeb.MS', 'reviews', 'salesmanagers');
    }
}