<?php namespace LuckyWeb\MS\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Illuminate\Support\Facades\DB;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\Review;

/**
 * Product Groups Back-end Controller
 */
class ProductGroups extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('LuckyWeb.MS', 'products', 'productgroups');
    }

    public function onRelationManageAdd() {

        // Before
        switch (post('_relation_field')) {
            case 'products':
                if(count(post('checked')) > 0) {
                    // Delete products from other groups
                    DB::table('luckyweb_ms_products_groups')->whereIn('product_id', post('checked'))->delete();
                }
                break;
        }

        $result = $this->asExtension('RelationController')->onRelationManageAdd();

        // After
        switch (post('_relation_field')) {
            case 'products':
                if(count(post('checked')) > 0) {
                    // Update product reviews with current group
                    $slugs = Product::whereIn('id', post('checked'))->lists('slug');
                    if(count($slugs) > 0) {
                        Review::whereIn('product_slug', $slugs)->update(['product_group_id' => $this->formGetModel()->id]);
                    }
                }
                break;
        }

        return $result;
    }
}