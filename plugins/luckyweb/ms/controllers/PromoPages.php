<?php namespace LuckyWeb\MS\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use LuckyWeb\MS\Models\PromoBlock;
use LuckyWeb\MS\Models\PromoPage;
use October\Rain\Support\Facades\Flash;

/**
 * Promo Pages Back-end Controller
 */
class PromoPages extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('LuckyWeb.MS', 'promoboard', 'promopages');

        // Grab the drag and drop requirements
        $this->addCss('/plugins/luckyweb/ms/assets/css/sortable.css');
        $this->addJs('/plugins/luckyweb/ms/assets/js/html5sortable.js');
        $this->addJs('/plugins/luckyweb/ms/assets/js/sortable.js');
        $this->addCss('/plugins/luckyweb/ms/assets/css/backend.css?'.time());
    }


    public function update_onUpdatePosition()
    {
        $moved = [];
        $position = 0;
        if (($reorderIds = post('checked')) && is_array($reorderIds) && count($reorderIds)) {
            foreach ($reorderIds as $id) {
                $record = PromoBlock::find($id)->pages()->where('id', post('model_id'))->first();
                if (in_array($id, $moved) || !$record)
                    continue;

                $record->pivot->sort_order = $position;
                $record->pivot->save();
                $moved[] = $id;
                $position++;
            }
            Flash::success('Сортировка успешно сохранена');
        }

        $model = PromoPage::find(post('model_id'));
        $this->initForm($model);
        $this->initRelation($model, 'blocks');

        return $this->relationRefresh('blocks');
    }
}