<?php namespace LuckyWeb\MS\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Description Hints Back-end Controller
 */
class DescriptionHints extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('LuckyWeb.MS', 'reviews', 'descriptionhints');
    }
}
