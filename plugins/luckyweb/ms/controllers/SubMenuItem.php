<?php namespace Luckyweb\Ms\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Sub Menu Item Back-end Controller
 */
class SubMenuItem extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('LuckyWeb.MS', 'menu', 'submenuitem');
    }
}
