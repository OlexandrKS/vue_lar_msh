<?php namespace Luckyweb\Ms\Controllers;

use BackendMenu;
//use Backend\Widgets\MediaManager;
use Backend\Classes\Controller;

/**
 * Menu Item Back-end Controller
 */
class MenuItem extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';


    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('LuckyWeb.MS', 'menu', 'menuitem');
//        if ($this->user && $this->user->hasAccess('media.*')) {
//            $manager = new MediaManager($this, 'ocmediamanager');
//            $manager->bindToController();
//        }

    }
}
