<?php namespace LuckyWeb\MS\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Illuminate\Support\Facades\Event;
use LuckyWeb\MS\Models\Review;
use LuckyWeb\MS\Models\SalesManager;
use LuckyWeb\MS\Widgets\MessageHistory;

/**
 * Reviews Back-end Controller
 */
class Reviews extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('LuckyWeb.MS', 'reviews', 'reviews');

        $this->addCss('/plugins/luckyweb/ms/assets/css/backend.css?'.time());
        $this->addJs('/modules/backend/widgets/lists/assets/js/october.list.js?v1');

        $messageHistory = new MessageHistory($this, array_get($this->params, 0, null));
        $messageHistory->alias = 'messageHistory';
        $messageHistory->bindToController();
    }


    public function onForwardCorrespondence()
    {
        foreach(post('checked') as $managerId) {
            $manager = SalesManager::find(intval($managerId));
            if($manager) {
                $review = Review::where('id', intval($this->params[0]))
                    ->with('messages')->with('city')
                    ->first();
                if($review->messages->count() > 0) {
                    Event::fire('luckyweb.ms.forwardCorrespondence', [$review, $manager]);
                }
            }
        }
    }

    public function onManagersList()
    {
        $managers = SalesManager::get();

        return [
            'result' => $this->makePartial('managers_list', [
                'managers' => $managers
            ])
        ];
    }
}