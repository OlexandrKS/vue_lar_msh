<?php namespace LuckyWeb\MS\Controllers;

use BackendMenu;
use Redirect;
use Backend\Classes\Controller;
use October\Rain\Support\Facades\Flash;
use Luckyweb\Ms\Models\PagePopupHTML as PagePopupHTMLModel;

/**
 * Page Popup H T M L Back-end Controller
 */
class PagePopupHTML extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('LuckyWeb.MS', 'pagepopup', 'pagepopuphtml');
    }

    public function listExtendQuery($query, $definition = null)
    {
        $query->whereNotNull('html');
    }

    /**
     * @return mixed
     */
    public function onCopy()
    {
        $result = new PagePopupHTMLModel;
        $result->fill(request()->PagePopupHTML);
        $result->url_hash = request()->PagePopupHTML['url_hash'].str_random(5);
        $result->save();

        //Send success message
        Flash::success('PagePopup "'.$result->popup_name.'" copied success!');

        return Redirect::to('/backend/luckyweb/ms/pagepopuphtml/update/'.$result->id);
    }
}
