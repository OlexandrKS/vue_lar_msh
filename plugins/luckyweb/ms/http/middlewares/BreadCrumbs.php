<?php
namespace LuckyWeb\MS\Http\Middlewares;

use LuckyWeb\MS\Models\Category;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\ShopCity;
use LuckyWeb\MS\Models\SubCategory;
use Closure;

class BreadCrumbs
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $url = \Request::path();

        // Fix for local server
        if(env('APP_ENV') == 'local') {
            $url = str_replace('/mebel-shara', '', $url);
        }

        $urls = explode('/', $url);
        $crumbs = [];

        /*create urls for every explode*/
        if (!empty($urls) && $url != '/')
        {
            foreach ($urls as $key => $value)
            {
                $prev_urls = [];
                for ($i = 0; $i <= $key; $i++)
                {
                    $prev_urls[] = $urls[$i];
                }

                if ($key == count($urls) - 1) $crumbs[$key]['url'] = '';
                elseif (!empty($prev_urls)) $crumbs[$key]['url'] = url(count($prev_urls) > 1 ? implode('/', $prev_urls) : '/');

                $crumbs[$key]['text'] = $value;
                if ($crumbs[$key]['text'] == 'magaziny-mebeli')
                    $crumbs[$key]['url'] = url('contacts');

            }
        }
        /*end urls*/

        for ($key=0; $key<count($crumbs);$key++)
        {
           $result = BreadCrumbs::getData($crumbs[$key]['text'], []);

           if (!is_null($result))
               $crumbs[$key]['text'] = $result;
           else
//               if($key>1)
//                    $crumbs=array_slice($crumbs,0,$key);
//                else
                {
                    unset($crumbs[$key]);
                    $crumbs = array_values($crumbs);
                    $key--;
                }
        }
        /*add main page*/
        if(!empty($crumbs)) {
            array_splice($crumbs, 0, 0, [[]]);
            $crumbs[0]['text'] = "Интернет-Магазин «Мебель Шара»";
            $crumbs[0]['url'] = url("/");
        }

        $request->attributes->Add(['breadcrumbs' => $crumbs]);
        return $next($request);
    }

    public static function getData($data, $arr_prov){

        $prov =  (empty($arr_prov)) ? BreadCrumbs::$providers : $arr_prov;

        foreach($prov as $item)
        {
            switch ($item)
            {
                case 'Category':
                    $query = Category::where('slug',$data)->select('name')->first();
                    if (!is_null($query))
                        return $query->toArray()['name'];
                    break;
                case 'Product':
                    $query = Product::where('slug',$data)->select('name')->first();
                    if (!is_null($query))
                        return $query->toArray()['name'];
                    break;
                case 'SubCategory':
                    $query = SubCategory::where('slug',$data)->select('name')->first();
                    if (!is_null($query))
                        return $query->toArray()['name'];
                    break;
                case 'Sities':
                    $query = ShopCity::where('slug',$data)->select('name')->first();
                    if (!is_null($query))
                        return $query->toArray()['name'];
                    break;
                case 'Temporary':
                    $query = !empty(BreadCrumbs::$temp_arr[$data]);
                    if($query)
                        return BreadCrumbs::$temp_arr[$data];
                    break;
            }
        }
        return null;
    }

    static $providers = ['Category', 'Product', 'Temporary', 'SubCategory', 'Sities'];

    static $temp_arr = [
        'contacts' => 'Адреса магазинов',
        'magaziny-mebeli' => 'Адреса магазинов',
        'vacancies' => 'Работа в Мебель Шара',
        'kontaktnye-dannye' => 'Контактные данные',
        'promo-board' => 'Пакетное предложение',
        'oplata' => 'Оплата',
        'dostavka-i-sborka' => 'Доставка и Сборка',
        'samovyvoz' => 'Самовывоз',
        'obmen-i-vozvrat' => 'Обмен и Возврат',
        'garantiya' => 'Гарантия',
        'voprosy' => 'Вопросы и ответы',
        'otzyvy' => 'Отзывы покупателей',
        'kuchni_lp1' => 'Супер акция',
        'kuchni_lp2' => 'Равенна класик',
        'shares' => 'Наши акции',
        'favorites-list' => 'Мой список',
        'favorites' => 'Мои списки',
        'cart' => 'Корзина',
    ];
}