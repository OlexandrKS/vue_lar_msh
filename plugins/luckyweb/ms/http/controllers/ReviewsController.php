<?php namespace LuckyWeb\MS\Http\Controllers;

use Illuminate\Routing\Controller;

use Config;
use Exception;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use LuckyWeb\MS\Models\Product;

class ReviewsController extends Controller
{
    /**
     * Current request
     */
    protected $request;

    /**
     * Create a new controller instance.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getProducts() {
        $pageSize = 30;
        $skip = $this->request->input('page', 0) * $pageSize;
        $phrase = !empty($this->request->input('q')) ? $this->request->input('q') : 'a';

        try {
            $products = Product::where('name', 'LIKE', $phrase.'%')
                ->orWhere('subname', 'LIKE', $phrase.'%')
                ->skip($skip)
                ->take($pageSize)
                ->get(['id', 'slug', 'name', 'subname']);

            $data['items'] = [];
            foreach($products as $key => $product) {
                $data['items'][$key] = $product->toArray();
                $data['items'][$key]['id'] = $product->slug;
                $data['items'][$key]['image'] = $product->images->first()->getPreviewSource();
            }

            return Response::json($data);
        }
        catch(Exception $e) {
            Log::info('ReviewsController->getProducts(): '.$e->getMessage().' '.$e->getTraceAsString());
        }
    }


}
