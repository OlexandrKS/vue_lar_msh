<?php namespace LuckyWeb\MS\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Response;

use Config;
use Exception;

use LuckyWeb\MS\Models\Image;

class ImageController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
    }

    public function minimize($image_id) {
        if (!($image = Image::find($image_id))) {
            return response()->json([$image_id=>'not found']);
        }
        return response()->json($image->minimize());
    }


}
