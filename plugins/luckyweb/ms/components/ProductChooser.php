<?php namespace Luckyweb\Ms\Components;

use Cms\Classes\ComponentBase;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\FavoriteList;
use LuckyWeb\MS\Models\FavoriteListItem;
use LuckyWeb\User\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use October\Rain\Exception\AjaxException;
use System\Classes\CombineAssets;

class ProductChooser extends ComponentBase
{
    /**
     * Path of theme-level assets
     * @var string $themeAssetsPath
     */
    public $themeAssetsPath = '/themes/shara/assets';

    public function componentDetails()
    {
        return [
            'name'        => 'productchooser Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->addJs(CombineAssets::combine([
            '/plugins/luckyweb/ms/components/productchooser/assets/js/product_chooser.js'
        ], base_path()), ['async']);
    }

    /**
     * Render chooser form
     */
    public function onShowChooserForm()
    {
        $temp = Cookie::get(FavoriteListItem::COOKIE_NAME, []);
        $temp = collect($temp);
        $default_list_id = Auth::getUser() ? FavoriteList::getDefaultListId() : null;

        $product = Product::where('id', post('product_id'))->first();

        $user = Auth::getUser();
        $list = null;
        if($user) {
            $list = FavoriteList::where('user_id', $user->id)->get();
            if (count($list) == 0) {
                $flist = new FavoriteList;
                $flist->list_name = 'По умолчанию';
                $flist->user_id = $user->id;
                $flist->save();
                $list = FavoriteList::where('user_id', $user->id)->get();
            }
        }
        else
            FavoriteListItem::addProductToFavList($temp, post('product_id'), $default_list_id, 1);

        if(post('product_id') || post('product_name')) {
            return [
                '.js-product_chooser_form_modal .modal-content' => $this->renderPartial(
                    'productchooser::product_form', [
                        'product' => $product,
                        'production_mode' => strtolower(env('APP_ENV')) == 'production',
                        'user' => $user,
                        'list' => $list
                    ]
                ),
                '.js-count_cart' => $this->renderPartial(
                    'cart::header_count_cart_item.htm', [
                        'cart_count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser())
                    ]
                ),
                '.js-count_product_list' => $this->renderPartial(
                    'productchooser::header_count_list_item.htm', [
                        'user' =>Auth::getUser(),
                        'count' => FavoriteListItem::getCountFavList($temp, Auth::getUser())
                    ]
                ),
            ];
        }
    }

    public function onChooserSubmit()
    {
        $temp = Cookie::get(FavoriteListItem::COOKIE_NAME, []);
        $temp = collect($temp);
        FavoriteListItem::addProductToFavList($temp, post('product_id'), post('selector'), post('count'));
        $product = Product::where('id', post('product_id'))->first();
        $user = Auth::getUser();
        return [
            '.js-product_chooser_form_modal .modal-content' => $this->renderPartial(
                'productchooser::success_message.htm', [
                    'production_mode' => strtolower(env('APP_ENV')) == 'production',
                    'product' => $product
                ]
            ),
            '.js-count_product_list' => $this->renderPartial(
                'productchooser::header_count_list_item.htm', [
                    'user' => $user,
                    'count' => FavoriteListItem::getCountFavList($temp, $user),
                ]
            ),
            '.js-count_cart' => $this->renderPartial(
                'cart::header_count_cart_item.htm', [
                    'cart_count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser())
                ]
            ),
            '.js-count_product_list_mob' => $this->renderPartial(
                'productchooser::mob_header_count_list_item.htm', [
                    'user' => $user,
                    'count' => FavoriteListItem::getCountFavList($temp, $user),
                ]
            ),
            '.js-count_cart_mob' => $this->renderPartial(
                'cart::mob_header_count_cart_item.htm', [
                    'cart_count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser())
                ]
            ),
        ];
    }

    public function onDeleteListItem()
    {
        $user = Auth::getUser();
        $temp = Cookie::get(FavoriteListItem::COOKIE_NAME, []);
        $temp = collect($temp);
        FavoriteListItem::removeProductFromFavList($temp, post('product_id'), post('list_id'));
        $list_id = empty($user) ? null : post('list_id');
        $counts = FavoriteListItem::getProductCountsFromFavList($list_id);
        $products = FavoriteListItem::getProductCountsFromFavList($list_id, true, ['product_id']);

        return[
            '.js-list_product_chooser' => $this->renderPartial(
                'productchooser::product_list_page_list.htm', [
                    'products' => $products,
                    'param' => post('list_id'),
                    'counts' => $counts
                ]
            ),
            '.js-count_product_list' => $this->renderPartial(
                'productchooser::header_count_list_item.htm', [
                    'user' =>$user,
                    'count' => FavoriteListItem::getCountFavList($temp, $user)
                ]
            ),
            '.js-count_cart' => $this->renderPartial(
                'cart::header_count_cart_item.htm', [
                    'cart_count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser())
                ]
            ),
            '.js-count_product_list_mob' => $this->renderPartial(
                'productchooser::mob_header_count_list_item.htm', [
                    'user' => $user,
                    'count' => FavoriteListItem::getCountFavList($temp, $user),
                ]
            ),
            '.js-count_cart_mob' => $this->renderPartial(
                'cart::mob_header_count_cart_item.htm', [
                    'cart_count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser())
                ]
            ),
        ];
    }

    public function onDeleteList()
    {
        $temp = Cookie::get(FavoriteListItem::COOKIE_NAME, []);
        $temp = collect($temp);
        $list = FavoriteList::removeFavList(post('list_id'));
        return[
            '.js-favorites_product_chooser' => $this->renderPartial(
                'productchooser::favorite_list_page.htm', [
                    'list' => $list,
                ]
            ),
            '.js-count_product_list' => $this->renderPartial(
                'productchooser::header_count_list_item.htm', [
                    'user' => Auth::getUser(),
                    'count' => FavoriteListItem::getCountFavList($temp, Auth::getUser())
                ]
            ),
            '.js-cart-fav-list' => $this->renderPartial(
                'productchooser::favorite_list_cart_page.htm', [
                    'list' => $list,
                ]
            ),
            '.js-count_product_list_mob' => $this->renderPartial(
                'productchooser::mob_header_count_list_item.htm', [
                    'user' => Auth::getUser(),
                    'count' => FavoriteListItem::getCountFavList($temp, Auth::getUser()),
                ]
            ),
        ];
    }

    public function onChangeCountListItem()
    {
        $temp = Cookie::get(FavoriteListItem::COOKIE_NAME, []);
        $temp = collect($temp);
        FavoriteListItem::updateProductCount($temp, post('product_id'), post('count'), post('list_id'));

        return[
            '.js-count_cart' => $this->renderPartial(
                'cart::header_count_cart_item.htm', [
                    'cart_count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser())
                ]
            ),
            '.js-count_product_list' => $this->renderPartial(
                'productchooser::header_count_list_item.htm', [
                    'user' =>Auth::getUser(),
                    'count' => FavoriteListItem::getCountFavList($temp, Auth::getUser())
                ]
            ),
        ];
    }

    public function onAddFavoriteListSubmit()
    {
        $user = Auth::getUser();
        if($user) {
            $flist = new FavoriteList;
            $flist->list_name = post('list_name');
            $flist->user_id = $user->id;
            $flist->save();
            $temp = FavoriteList::where('user_id', $user->id)->get();

            /*for cart popup*/
            $default_list_id = Auth::getUser() ? FavoriteList::getDefaultListId() : null;
            $temp_cart = Cookie::get(FavoriteListItem::COOKIE_NAME, []);
            $temp_cart = collect($temp_cart);
            $cnt = FavoriteListItem::getProductCountsFromProduct($temp_cart, $default_list_id);
            $arr = FavoriteListItem::getProductCountsFromFavList($temp_cart, $default_list_id, true, ['product_id']);
            $supp = FavoriteListItem::getSupportsArray($temp_cart, $default_list_id);
            return [
                '.js-user-list_product_chooser' => $this->renderPartial(
                    'productchooser::user_favorite_list.htm', [
                        'list' => $temp,
                    ]
                ),
                '.js-favorites_product_chooser' => $this->renderPartial(
                    'productchooser::favorite_list_page.htm', [
                        'list' => $temp,
                    ]
                ),
                '.js-cart-fav-list' => $this->renderPartial(
                    'productchooser::favorite_list_cart_page.htm', [
                        'list' => $temp,
                    ]
                ),
                '.js-cart-header' => $this->renderPartial(
                    'cart::cart_popup.htm', [
                        'cart_products' => $arr,
                        'cart_counts' => $cnt,
                        'cart_supports' => $supp,
                    ]
                ),
            ];
        }
        else
        {
            throw new AjaxException(['.js-product_chooser_form_modal .modal-content' => $this->renderPartial(
                'productchooser::auth_message.htm', [
                ]
            ),]);
        }
    }

    public function onRenameFavList(){
        $item = FavoriteList::where('id', post('list_id'))->first();
        $item->list_name = post('list_name');
        $item->save();
        return true;
    }

    public function onNoneAuth()
    {
        return[
            '.js-product_chooser_form_modal .modal-content' => $this->renderPartial(
                'productchooser::auth_message.htm', [
                ]
            ),
        ];
    }

    public function onClearData()
    {
        FavoriteListItem::truncate();
    }
}
