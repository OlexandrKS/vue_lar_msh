var QuickOrder = function () {

    function phoneMask () {
        $("[name='tel']").inputmask("+7(999) 999-99-99");
    }

    function displayErrors(form, errors) {
        $.each(errors, function (field, message) {
            $("[name='" + field + "']", form).siblings('.error-block').html(message[0]);
        });
    }

    function resetErrors(form) {
        form.find('.error-block').html('');
    }

    /**
     * Criteo quickorder send
     *
     * @param vId
     * @param vPrice
     * @param vQuantity
     */
    function clickoutconv(vId, vPrice, vQuantity) {
        window.criteo_q = window.criteo_q || [];

        window.criteo_q.push(
            { event: "trackTransaction", id: Math.floor(Math.random()*99999999999),
                item: [{ id: vId, price: vPrice, quantity: vQuantity }]});
    }

    var initHashQuickOrderFormShow = function(){
        if (window.location.hash == '#kitchenform') {

            $('#js-quick-order').modal();

            $.request('onQuickOrderFormShow', {
                data: {},
                success: function (data) {
                    this.success(data);
                    phoneMask();

                    var btn = $('.js-quick-order-btn');
                    if(btn.data('title')){
                        $('.js-quick_order_form .modal-title').text(btn.data('title'));
                    }
                    if (btn.data('type') === 'order') {
                        $('#js-quick-order .btn-success').text('Заказать');
                    }
                }
            });
        }
    }

    var initQuickOrderFormShow = function () {
        $('.js-quick-order-btn').on('click', function (e) {
            e.preventDefault();
            var $this = $(this)

            window.__tmp_criteo_q_data = {
                nid: $this.data('nid'),
                price: $this.data('price')
            }

            $('#js-quick-order').modal();

            $.request('onQuickOrderFormShow', {
                data: {},
                success: function (data) {
                    this.success(data);
                    phoneMask();

                    var btn = $('.js-quick-order-btn');
                    if(btn.data('title')){
                        $('.js-quick_order_form .modal-title').text(btn.data('title'));
                    }
                    if (btn.data('type') === 'order') {
                        $('#js-quick-order .btn-success').text('Заказать');
                    }
                }
            });
        });
    };

    var initQuickOrderFormSubmit = function () {

        $('body').on('submit', '.js-quick_order_form', function (e) {
            e.preventDefault();

            var form = $(this);
            var btn = form.find('[type="submit"]');
            resetErrors(form);
            btn.attr('disabled', true);

            if (window.__tmp_criteo_q_data) {
                clickoutconv(window.__tmp_criteo_q_data.nid, window.__tmp_criteo_q_data.price, 1);
                window.__tmp_criteo_q_data = undefined;
            }

            $.request('onQuickOrderFormSubmit', {
                error: function (resp, status, err) {
                    if (typeof resp.responseJSON != 'undefined') {
                        displayErrors(form, resp.responseJSON.X_OCTOBER_ERROR_FIELDS);
                    }
                    else {
                        this.error(resp, status, err);
                    }
                },
                data: {
                    'id': $('.js-quick-order-btn').data('id'),
                    'user_name': $('input[name=user_name]', form).val(),
                    'tel': $('input[name=tel]', form).val()
                },
                complete: function () {
                    btn.removeAttr('disabled')
                }
            });
        });
    };

    return {
        init: function () {
            initHashQuickOrderFormShow();
            initQuickOrderFormShow();
            initQuickOrderFormSubmit();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    QuickOrder.init();
});
