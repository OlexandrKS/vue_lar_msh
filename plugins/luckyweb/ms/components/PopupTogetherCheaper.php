<?php

namespace LuckyWeb\MS\Components;

use App\Domain\Entities\Catalog\Offer;
use Cms\Classes\ComponentBase;
use Jenssegers\Agent\Agent;
use LuckyWeb\MS\Models\Product;
use System\Classes\CombineAssets;
use LuckyWeb\MS\Models\PopupTogetherCheaper as PopupTogether;

class PopupTogetherCheaper extends ComponentBase
{
    /**
     * Current parent product instance
     * @var Product
     */
    public $product;

    /**
     * Item of support products
     * @var mixed
     */
    public $supportProduct;

    public $popupTogetherCheaper;

    public $offer;


    public function componentDetails()
    {
        return [];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->popupTogetherCheaper = PopupTogether::find(1);
        $is_device = false;
        if (!empty($this->popupTogetherCheaper->devices)) {
            $agent = new Agent();
            foreach ($this->popupTogetherCheaper->devices as $device) {
                switch ($device) {
                    case 'mobile':
                        if ($agent->isMobile()) $is_device = true;
                        break;
                    case 'tablet':
                        if ($agent->isTablet()) $is_device = true;
                        break;
                    case 'pc':
                        if ($agent->isDesktop()) $is_device = true;
                        break;
                }
            }
        }
        if ($is_device && $this->popupTogetherCheaper->active) {
            $this->addCss('/plugins/luckyweb/ms/components/popuptogethercheaper/assets/css/popup_together_cheaper.css');
            $this->addJs(CombineAssets::combine([
                '/plugins/luckyweb/ms/components/popuptogethercheaper/assets/js/popup_together_cheaper.js'
            ], base_path()));
        }
    }

    /**
     * Component initializing
     */
    public function onRender()
    {
        $this->product = $this->property('product');

        if ($this->product) {
            $this->supportProduct = $this->product->getSupportProducts()->first();
        }

        $this->offer = Offer::togetherCheap($this->product->getKey(), $this->supportProduct->getKey())
            ->with('primary', 'primary.nid')
            ->first();
    }
}
