<?php namespace LuckyWeb\MS\Components;

use App\Domain\Entities\Catalog\Offer;
use App\Domain\Enums\Catalog\OfferType;
use Cms\Classes\ComponentBase;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\TogetherCheeperPivot;
use System\Classes\CombineAssets;

class ProductTogetherCheaper extends ComponentBase
{
    /**
     * Current parent product instance
     * @var Product
     */
    public $product;

    /**
     * List of support products
     * @var mixed
     */
    public $supportProducts;

    public $offers;


    public function componentDetails()
    {
        return [
            'name'        => 'ProductTogetherCheaper Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'product' => [
                'title'             => 'Product',
                'description'       => 'Current parent product',
                'default'           => '',
                'type'              => 'object'
            ],
        ];
    }

    public function onRun()
    {
//        $this->addJs(CombineAssets::combine([
//            '/plugins/luckyweb/ms/components/producttogethercheaper/assets/js/product_together_cheaper.js'
//        ], base_path()));
    }

    /**
     * Component initializing
     */
    public function onRender()
    {
        $this->product = $this->property('product');

        if($this->product) {
            $this->supportProducts = $this->product->getSupportProducts();

            $this->offers = Offer::where('type', OfferType::TOGETHER_CHEAP)
                ->where('primary_id', $this->product->getKey())
                ->where('properties->order', 1)
                ->orderByDesc('properties->saving')
                ->with('primary', 'primary.nid', 'products')
                ->get();
        }
    }

    /**
     * Render select list
     */
    public function onShowOffersList()
    {
        if(post('product_id') && post('category_id')) {
            $product = Product::find(post('product_id'));

            return [
                '.js-together_select_modal .modal-content' => $this->renderPartial(
                    'producttogethercheaper::select_list', [
                        'product' => $product,
                        'support' => $product->getSupportProductsByCategory(post('category_id'))
                    ]
                )
            ];
        }
    }

    /**
     * Renter offer item
     * @return array
     */
    public function onOfferSelected()
    {
        if(post('product_id') && post('support_id')) {
            $supportProductId = TogetherCheeperPivot::find(post('support_id'))
                ->getSupportProduct()
                ->getKey();

            $offer = Offer::togetherCheap(post('product_id'), $supportProductId)
                ->with('primary', 'primary.nid')
                ->first();

            return [
                'item' => $this->renderPartial(
                    'producttogethercheaper::offer_item', [
                        'product' => $offer->primary,
                        'support' => $offer->support_product,
                        'offer' => $offer
                    ]
                )
            ];
        }
    }
}
