var ProductGrid = function () {

    var initSlider = function(){
        $('.js-image_slider').owlCarousel({
            singleItem: true,
            pagination: false,
            items: 1,
            autoplay: true,
            autoplayTimeout: 400000,
            autoplayHoverPause: true,
            autoplaySpeed: 5000,
            navSpeed: 1000,
            dotsSpeed: 1000,
            loop: true,
            navigation: false
        });
    };

    var initSetPopup = function() {
        $('.js-set_popup').on('click', function(e){
            e.preventDefault();

            var $this = $(this);
            if($this.hasClass('open')) {
                $this.find('i').removeClass('fa-chevron-up')
                    .addClass('fa-chevron-down');

                $this.removeClass('open');
                $this.siblings('.js-set_content').fadeOut();
            }
            else {
                $this.find('i').removeClass('fa-chevron-down')
                    .addClass('fa-chevron-up');

                $('.js-set_content').fadeOut();

                $this.addClass('open');
                $this.siblings('.js-set_content').fadeIn();
            }
        })
    };

    var initClose = function()
    {
        $(document).click(function(event) {
            if(!$(event.target).closest(".js-set_popup").length && $('.js-set_popup').hasClass('open')) {
                $('.js-set_popup')
                    .removeClass('open')
                    .find('i').removeClass('fa-chevron-up')
                    .addClass('fa-chevron-down');

                $('.js-set_content').fadeOut();
            }
        });
    };

    var initProductImpressionsTracker = function(){
        appear({
            elements: function elements(){
                return document.getElementsByClassName('product');
            },
            appear: function appear(el){
                try {
                    // Ecommerce event for GTM. Product Impressions
                    window.dataLayer = window.dataLayer || [];
                    dataLayer.push({
                        'ecommerce': {
                            'impressions': [
                                $(el).data('ecommerce')
                            ]
                        },
                        'event': 'gtm-ee-event',
                        'gtm-ee-event-category': 'Enhanced Ecommerce',
                        'gtm-ee-event-action': 'Product Impressions',
                        'gtm-ee-event-non-interaction': 'True'
                    });
                } catch(e){}
            }
        });
    };

    return {
        init: function () {
            initSetPopup();
            initClose();
            //initSlider();

            if(ProductGridVariables.productionMode) {
                initProductImpressionsTracker();
            }
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    ProductGrid.init();
});
