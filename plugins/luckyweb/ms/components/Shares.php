<?php namespace LuckyWeb\MS\Components;

use Cms\Classes\ComponentBase;
use LuckyWeb\MS\Models\Share;
use System\Classes\CombineAssets;

class Shares extends ComponentBase
{
    public $shares;

    public function componentDetails()
    {
        return [
            'name'        => 'Shares Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->shares = Share::activated()->orderBy('sort_order', 'asc')->get();

        $this->addJs(CombineAssets::combine([
            '/plugins/luckyweb/ms/components/shares/assets/js/shares.js'
        ], base_path()));
    }

    /**
     * Handle share detail request
     * @return array
     */
    public function onShareDetail()
    {
        if(post('share_id')) {
            $share = Share::activated()->find(post('share_id'));

            if($share) {
                return [
                    '.js-share_modal' => $this->renderPartial('shares::detail', ['share' => $share])
                ];
            }
        }
    }
}
