var Credit = function () {

    var creditModal = $('.js-credit-modal');
    var creditForm = $('form', creditModal);

    function initPhoneMask() {
        $('[name=phone]', creditForm).mask('+7 999 999 99 99');
    }
    function initPassportMask() {
        $('[name=passport]', creditForm).mask('99-99 999999');
    }

    function displayErrors($form, errors) {
        $.each(errors, function (field, message) {
            $("[name='" + field + "']", $form).siblings('.error-block').html(message[0]);
        });
    }

    function resetErrors($form) {
        $form.find('.error-block').html('');
    }

    var initFormSubmit = function () {
        creditForm.on('submit', function (e) {
            e.preventDefault();

            var submitButton = this.querySelector('[type="submit"]')
            submitButton.classList.add('disabled');

            resetErrors(creditForm);

            // var formData = $(this).serializeArray();

            var formData = {};

            formData.name = $('[name=name]', creditForm).val();
            formData.second_name = $('[name=second_name]', creditForm).val();
            formData.last_name = $('[name=last_name]', creditForm).val();
            formData.passport = $('[name=passport]', creditForm).val();
            formData.phone = $('[name=phone]', creditForm).val();
            // formData.comment = $('[name=comment]', creditForm).val();
            formData.privacy = $('[name=privacy]:checked', creditForm).val();

            $.request('onFormSubmit', {
                error: function (resp, status, err) {
                    if (typeof resp.responseJSON != 'undefined') {
                        displayErrors(creditForm, resp.responseJSON.X_OCTOBER_ERROR_FIELDS);
                    }
                    else {
                        this.error(resp, status, err);
                    }
                },
                data: {
                    form_data: formData,
                },
                success: function (data) {
                    this.success(data);

                    $.request('payment::onCreditSubmit', {
                        data: {
                            credit_form_data: data,
                        }
                    });
                },
                complete: function () {
                    submitButton.classList.remove('disabled');
                }
            });

        });
    };


    return {
        init: function () {
            initFormSubmit();
            initPhoneMask();
            initPassportMask();
        }
    }

}();

// Initialize when page loads
jQuery(function () {
    Credit.init();
});
