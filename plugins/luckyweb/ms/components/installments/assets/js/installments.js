var Installments = function () {
    var initJQ = function () {
        $('.installments-page .timer').countDown({
            always_show_days: true,
            with_labels: false,
            separator_days: ':'
        });

        $("[name='call-phone']").inputmask("+7 (999) 999-99-99");
    }

    var initForm = function () {
        var button = $('#call-request button'),
            form = $('#call-request');

        form.on('submit', function (e) {
            e.preventDefault();

            $(button).attr('disabled', true);

            $.request('onCallRequest', {
                data: {
                    phone: $('[name="call-phone"]').val()
                }
            })
                .done(function (data) {
                    $('.installments-modal').modal('show');
                    $("[name='call-phone']").val(null);
                })
                .fail(function (e) {
                    console.log(e)
                })
                .always(function () {
                    button.removeAttr('disabled')
                })
        })
    }

    return {
        init: function () {
            initJQ();
            initForm();
        }
    }
}();

jQuery(function () {
    Installments.init();
})
