var Vacancies = function () {

    var initCitySelect = function(){
        $('.js-city_select').on('change', function() {
            var cityId = $(this).val();
            var index = $(this).data('index');

            $.request('onCitySelected', {
                data: {
                    city_id: cityId,
                    index: index
                }
            });
        });
    };

    var initRequestFormButton = function(){
        $('body').on('click', '.js-request_form_button', function(){
            var vacancyId = $(this).data('id');
            showRequestForm(vacancyId);
        })
    };

    var initRequestFormLink = function(){
        if(findGetParameter('action') == 'form') {
            showRequestForm(findGetParameter('vacancy_id'));
        }
    };

    var findGetParameter = function(parameterName) {
        var result = null,
            tmp = [];
        var items = location.search.substr(1).split("&");
        for (var index = 0; index < items.length; index++) {
            tmp = items[index].split("=");
            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        }
        return result;
    };

    var showRequestForm = function(vacancyId) {
        if(vacancyId == '' || vacancyId == undefined) {
            return false;
        }

        $.request('onShowVacancyRequestForm', {
            data: {
                vacancy_id: vacancyId
            },
            success: function(data){
                this.success(data);
                $('.js-phone').inputmask('+7(999) 999-99-99');
                $('.js-masked-date').inputmask('99.99.9999');
                $('.js-file_upload input[type=file]').on('change', function(){
                    var fileText = $(this).val().replace('C:\\fakepath\\', '');
                    $(this).parents('.js-file_upload').find('.js-file_value').text(fileText);
                });
                $('.js-vacancy_request_modal').modal('show');
            }
        });
    };

    var initRequestFormSubmit = function(){
        $('.js-vacancy_request_modal').on('submit', 'form', function(e){
            e.preventDefault();
            var $form = $(this);
            var formData = new FormData(this);
            formData.append('city_id', parseInt($('.js-city_select').val()));

            $form.find('button[type=submit]').prop('disabled', true);
            $.ajax({
                headers: {
                    'X-OCTOBER-REQUEST-HANDLER': 'onSubmitVacancyRequestForm'
                },
                type: 'post',
                cache: false,
                contentType: false,
                processData: false,
                data: formData,
                error: function (jqXHR) {
                    displayFieldErrors(jqXHR);
                    return false;
                },
                complete: function(data)
                {
                    $(Object.keys(data.responseJSON)[0]).html(data.responseJSON[Object.keys(data.responseJSON)[0]]);
                    $form.find('button[type=submit]').prop('disabled', false);
                }
            });
        });
    };

    var displayFieldErrors = function(jqXHR) {
        var $form = $('.js-vacancy_request_modal form');
        $form.find('.form-group.has-error').each(function () {
            $(this).removeClass('has-error').find('.error').hide();
        });

        if (typeof jqXHR.responseJSON != 'undefined') {
            $.each(jqXHR.responseJSON.X_OCTOBER_ERROR_FIELDS, function (field, message) {
                var $group = $form.find("[name='"+field+"']").parents('.form-group').addClass('has-error');
                var $error = $group.find(".error");
                if ($error.length) {
                    $error.eq(0).text(message).show();
                }
                else {
                    $group.append('<div class="error">'+message+'</div>');
                }
            });
        }
    };

    var initCollapsibles = function() {
        $('.js-vacancies_list').on('show.bs.collapse', '.js-collapsible', function(){
            $(this).siblings('.js-panel_heading').find('.js-collapsible_icon')
                .removeClass('fa-plus')
                .addClass('fa-minus');
        }).on('hide.bs.collapse', '.js-collapsible', function(){
            $(this).siblings('.js-panel_heading').find('.js-collapsible_icon')
                .removeClass('fa-minus')
                .addClass('fa-plus');
        });
    };

    return {
        init: function () {
            initCitySelect();
            initRequestFormSubmit();
            initRequestFormButton();
            initCollapsibles();
            initRequestFormLink();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    Vacancies.init();
});
