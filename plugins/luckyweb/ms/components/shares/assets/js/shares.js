var Shares = function () {

    var initShareDetailButton = function(){
        $('.js-share_detail').on('click', function(e) {
            var shareId = $(this).data('id');

            e.preventDefault();
            openModal(shareId);
        });
    };

    var initOpenFormLink = function(){
        if(findGetParameter('action') == 'open') {
            openModal(findGetParameter('id'));
        }
    };

    var openModal = function(id){
        $.request('onShareDetail', {
            data: {
                share_id: id
            },
            success: function(data){
                this.success(data);
                if(!$.isEmptyObject(data)) {
                    $('.js-share_modal').modal('show');
                }
            }
        });
    };

    var findGetParameter = function(parameterName) {
        var result = null,
            tmp = [];
        var items = location.search.substr(1).split("&");
        for (var index = 0; index < items.length; index++) {
            tmp = items[index].split("=");
            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        }
        return result;
    };

    return {
        init: function () {
            initShareDetailButton();
            initOpenFormLink();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    Shares.init();
});
