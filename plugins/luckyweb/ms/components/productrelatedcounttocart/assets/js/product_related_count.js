var ProductRelatedCount = function () {

    var actionBuy = function(){
        $('.js-complect-buy-button').on('click', function(e) {
            e.preventDefault();
            $.request('onProductToCart', {
                data: { },
                success: function(data){
                    this.success(data);
                    console.log(data);
                    $('.js-cart_form_modal').modal('show');
                    Cart.initAddMinus();
                    actionClearAfterBuy();
                }
            });
        });
    };

    var actionsRow = function(){
        var div = $('.product-related-count');

        div.find('.pm').on('click', function(e){
            e.preventDefault();
            var numb = Number( $(this).parent().find('.number').text() );
            if ($(this).attr("id") == 'minus') {
                if (numb > 0) //--numb;
                $(this).parent().find('.number').text(--numb);
            }
            else
            if ($(this).attr("id") == 'plus')
            {
                if(numb<9) //++numb;
                $(this).parent().find('.number').text(++numb);
            }

            var productId = $(this).data('id');
            var productPrice = $(this).data('price');

            if(productId){
                $.request('onAddRelatedProduct', {
                    data: {
                        product_id: productId,
                        count: numb,
                        product_price: productPrice
                    },
                    success: function(data){
                        this.success(data);
                        console.log(data);
                        actionBuy();
                    }
                });
            }

            if (numb > 0)
            {
                 $(this)
                    .parent()
                    .parent()
                    .parent()
                    .parent()
                    .parent()
                    .parent()
                    .addClass('rect-r');
            }
            else
            {
                  $(this)
                    .parent()
                    .parent()
                    .parent()
                    .parent()
                    .parent()
                    .parent()
                    .removeClass('rect-r');
            }
        })
    };

    var actionClearAfterBuy = function(){
        var div = $('.product-related-count');
        div.find('.number').text(0);
        $(div)
            .parent()
            .parent()
            .parent()
            .parent()
            .parent()
            .removeClass('rect-r');
    };
    return {
        init: function () {
            actionsRow();
            actionBuy();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    ProductRelatedCount.init();
});


