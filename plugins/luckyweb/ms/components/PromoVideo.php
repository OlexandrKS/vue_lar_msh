<?php

namespace LuckyWeb\MS\Components;

use Cms\Classes\ComponentBase;
use LuckyWeb\MS\Models\Promotion;
use System\Classes\CombineAssets;

class PromoVideo extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'PromoVideo Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->addJs(CombineAssets::combine([
            '/plugins/luckyweb/ms/components/promovideo/assets/js/promo_video.js'
        ], base_path()), ['async']);

        $this->page['promotion'] = $promotion = Promotion::current()->first();
        $this->page['video'] = $promotion->getMonthlyYoutbeVideo();
    }

    /**
     * @deprecated
     * @return array
     */
    public function onModalShow()
    {
        $promo = Promotion::current()->first();
        return [
            '.js-promo_video_modal .modal-content' => $this->renderPartial(
                'promovideo::modal_content',
                ['promotion' => $promo]
            )
        ];
    }
}
