<?php namespace LuckyWeb\MS\Components;

use Cms\Classes\ComponentBase;
use LuckyWeb\MS\Models\ProductPassport as ProductPassportModel;
use System\Classes\CombineAssets;

class ProductPassport extends ComponentBase
{

    /**
     * Path of theme-level assets
     * @var string $themeAssetsPath
     */
    public $themeAssetsPath = '/themes/shara/assets';

    /**
     * Current product passport
     * @var ProductPassportModel
     */
    public $passport;

    public function componentDetails()
    {
        return [
            'name'        => 'ProductPassport Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'product' => [
                'title'             => 'Current product',
                'description'       => 'If set - get passport related to product',
                'default'           => '',
                'type'              => 'string'
            ],
        ];
    }

    public function onRender()
    {
        $product = $this->property('product');

        if(!empty($product)) {
            $this->passport = ProductPassportModel::where('product_name', $product->name)
                ->where('category_id', $product->category_id)->first();
        }

//        if($this->passport) {
//            $this->addJs(CombineAssets::combine([
//                '/plugins/luckyweb/ms/components/productpassport/assets/js/product_passport.js'
//            ], base_path()));
//        }
    }

    /**
     * Return available pages array
     * @return array
     */
    public function getPages()
    {
        $pages = [];
        if($this->passport) {
            foreach ($this->passport->pages as $page) {
                $pages[] = ['src' => $page->getPath()];
            }
        }
        return $pages;
    }
}
