<?php namespace LuckyWeb\MS\Components;

use Cms\Classes\ComponentBase;
use Jenssegers\Agent\Facades\Agent;
use LuckyWeb\MS\Models\Image;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\Promotion;
use System\Classes\CombineAssets;

class ProductRelated extends ComponentBase
{
    /**
     * Path of theme-level assets
     * @var string $themeAssetsPath
     */
    public $themeAssetsPath = '/themes/shara/assets';

    /**
     * Collection of page products
     * @var mixed $products
     */
    public $products;

    /**
     * Current mixed promotion
     * @var Promotion
     */
    public $mixedPromotion;

    /**
     * Database field of product row position
     * @var string $rowField
     */
    public $rowField;

    /**
     * Database field of product column position
     * @var string $columnField
     */
    public $columnField;

    /**
     * Container width
     * @var integer $maxWidth
     */
    public $maxWidth = 1170;

    /**
     * Double product padding (padding*2)
     * @var integer $padding
     */
    public $padding = 30;

    public function componentDetails()
    {
        return [
            'name'        => 'ProductGrid Component',
            'description' => 'Generates the grid of prodicts for category page and not only'
        ];
    }

    public function defineProperties()
    {
        return [
            'product' => [
                'title'             => 'Product slug',
                'description'       => 'Current parent product',
                'default'           => '',
                'type'              => 'object'
            ],
        ];
    }

    public function productionMode()
    {
        return strtolower(env('APP_ENV')) == 'production';
    }

    /**
     * Component initializing
     */
    public function onRender()
    {
        $product = $this->property('product');

        if($product) {
            $this->products = $product->getNestedElementPositions();
        }
    }
}
