var Faq = function () {

    var displayFieldErrors = function(jqXHR) {
        var $form = $('.js-question_form form')
        $form.find('.form-group.has-error').each(function () {
            $(this).removeClass('has-error').find('.error').hide();
        });

        if (typeof jqXHR.responseJSON != 'undefined') {
            $.each(jqXHR.responseJSON.X_OCTOBER_ERROR_FIELDS, function (field, message) {
                var $group = $form.find("[name='"+field+"']").parents('.form-group').addClass('has-error');
                var $error = $group.find(".error");
                if ($error.length) {
                    $error.eq(0).text(message).show();
                }
                else {
                    $group.append('<div class="error">'+message+'</div>');
                }
            });
        }
    };

    var initFormSubmit = function(){
        $('.js-question_form form').on('submit', function(e){
            var $form = $(this);

            e.preventDefault();
            $form.find('button[type=submit]').prop('disabled', true);
            $form.request('onQuestionAdd', {
                error: function(jqXHR, status, obj) {
                    displayFieldErrors(jqXHR);
                    return false;
                },
                complete: function() {
                    $form.find('button[type=submit]').prop('disabled', false);
                }
            })
        });
    };

    var initFormToggleButton = function(){
        $('.js-question_form_toggle').on('click', function(){
            if($('.js-question_form').hasClass('active')) {
                $('.js-question_form').removeClass('active').slideUp();
            }
            else {
                $('.js-question_form').addClass('active').slideDown();
            }
        })
    };

    var initCollapsibles = function() {
        $('.js-collapsible').on('show.bs.collapse', function(){
            $(this).siblings('.js-panel_heading').find('.js-collapsible_icon')
                .removeClass('fa-plus')
                .addClass('fa-minus');
        }).on('hide.bs.collapse' , function(){
            $(this).siblings('.js-panel_heading').find('.js-collapsible_icon')
                .removeClass('fa-minus')
                .addClass('fa-plus');
        });
    };

    return {
        init: function () {
            $(".js-question_form [name='phone']").inputmask("9(999) 999-99-99");
            initFormSubmit();
            initFormToggleButton();
            initCollapsibles();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    Faq.init();
});
