<?php namespace LuckyWeb\MS\Components;

use Cms\Classes\ComponentBase;
use LuckyWeb\MS\Classes\Helper\Youtube;
use System\Classes\CombineAssets;

class VideoPopup extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'VideoPopup Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->addJs(CombineAssets::combine([
            '/plugins/luckyweb/ms/components/videopopup/assets/js/video_popup.js'
        ], base_path()), ['async']);
    }

    public function onVideoPopup()
    {
        if(empty(post('url'))) {
            return null;
        }

        $video = new Youtube(post('url'));
        $title = post('title');

        return [
            '.js-popup_video_modal .modal-content' => $this->renderPartial(
                'videopopup::modal_content',
                ['video' => $video, 'title' => $title]
            )
        ];
    }
}
