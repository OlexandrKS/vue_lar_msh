<?php

namespace LuckyWeb\MS\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use GuzzleHttp\Psr7\Request;
use Illuminate\Contracts\Cookie\QueueingFactory;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use LuckyWeb\MS\Classes\PriceCalculator\Calculators\BuyTodayDiscount;
use LuckyWeb\MS\Classes\PriceCalculator\Factory;
use LuckyWeb\MS\Classes\PriceCalculator\OrderPrice;
use LuckyWeb\MS\Models\FavoriteList;
use LuckyWeb\MS\Models\FavoriteListItem;
use Luckyweb\Ms\Models\OrderItem;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\User\Facades\Auth;
use LuckyWeb\User\Models\PromoCodeEvent;
use LuckyWeb\User\Models\Settings as UserSettings;
use LuckyWeb\User\Models\User;
use October\Rain\Exception\ApplicationException;
use System\Classes\CombineAssets;

use LuckyWeb\MS\Models\Order;

class Payment extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Payment Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function init()
    {
        $this->addJs(CombineAssets::combine([
            '/plugins/luckyweb/ms/components/payment/js/payment.js'
        ], base_path()), ['async']);
    }

    public function defineProperties()
    {
        return [];
    }

    /**
     * Card pay
     */
    public function onCardPay()
    {
        $orderData = $this->getData(post());
        $return_url = $this->getThankYouUrl($orderData);

        $this->loginUser($orderData);

        $isUsedBonuses = array_get($orderData, 'use_bonus', 0) > 0;
        $isUsedPromoCode = $this->setUsedPromoCode((string) array_get($orderData, 'promo_code', ''), !$isUsedBonuses);
        $this->useBuyTodayDiscount(! ($isUsedBonuses || $isUsedPromoCode));

        Event::fire('luckyweb.ms.orderSuccess', [$orderData]);

        FavoriteListItem::ClearCart();

        return Redirect::to($return_url);
    }

    /**
     *  Credit
     */

    public function onCredit() {

        $orderData = $this->getData(Input::get());

        session()->put('creditOrderData', $orderData);

        return $orderData;
    }

    /**
     * @return bool
     */
    public function onCreditSubmit()
    {
        session()->forget(BuyTodayDiscount::SESSION_KEY);

        if (!session()->has('creditOrderData')) return false;

        $data = session()->get('creditOrderData');
        $creditFormData = request()->input('credit_form_data');

        $data['first_name'] = $creditFormData['name'];
        $data['second_name'] = $creditFormData['second_name'];
        $data['middle_name'] = $creditFormData['last_name'];
        $data['phone'] = $creditFormData['phone'];
        $data['passport'] = $creditFormData['passport'];

        $userId = $this->loginUser($data);
        FavoriteListItem::ClearCart();

        // create order
        $creditOrder = Order::create([
            'total_price' => $data['total'],
            'payment_method' => Order::PAY_METHOD_CREDIT,
            'status' => Order::CREDIT_STATUS_SENT,
            'user_id' => $userId,
        ]);

        $data['order_id'] = $creditOrder->id;

        foreach ($data['products'] as $product) {
            OrderItem::create([
                'order_id' => $data['order_id'],
                'product_id' => $product['id'],
                'name' => $product['name'],
                'quantity' => $product['count'],
                'total_price' => $product['total'],
            ]);
        }

        Event::fire('luckyweb.ms.orderSuccess', [$data]);

        session()->forget('creditOrderData');
        return Redirect::to('/thanks-for-order');
    }

    /**
     * Get amount (в копейках)
     */
    private function getAmount($value)
    {
        return (int)$value * 100;
    }

    /**
     * Get order data info
     * @param $post
     * @return array
     */
    private function getData($post)
    {
        $return = array();
        $list_id = $post['list'];
        $temp = Cookie::get(Cart::COOKIE_NAME, []);
        $temp = collect($temp);

        if (empty($list_id))
            $list_id = Auth::getUser() ? FavoriteList::getDefaultListId() : null;
        $products = FavoriteListItem::getProductCountsFromFavList($temp, $list_id, true, ['product_id']);
        $this_counts = FavoriteListItem::getProductCountsFromProduct($temp, $list_id);
        $supports = FavoriteListItem::getSupportsArray($temp, $list_id);
        $delivery_method = post('delivery_method');
        $assembly = post('assembling');
        $user_phone = post('phone');

        $promo_code = post('promo_code');
        /*$promo_code_amount = 0;
        if ($promo_code) {
            $promo_code_event = PromoCodeEvent::where('code', '=', $promo_code)
                ->where('user_id', '=', Auth::getUser()->id)
                ->with(['promo_code'])->first();
            $now = Carbon::now()->startOfDay();
            if ($promo_code_event && $promo_code_event->promo_code->status
                && $promo_code_event->applied_at == null && $promo_code_event->promo_code->expired_at >= $now
                && $promo_code_event->promo_code->start_at <= $now) {
                $promo_code_amount = $promo_code_event->promo_code->amount;
            }
        }*/

        /** @var OrderPrice $orderPrice */
        $orderPrice = (new OrderPrice())->calculate(
            Factory::transformDataForPriceCalculator($products, $this_counts, $supports)
        );

        if(!empty($products)) {
            $i = 0;
            /** @var Product $product */
            foreach ($products as $product) {
                // TODO перетянуть в калькулятор с офферами
                if ($supports[$product->id] > 0)
                {
                    $saveDiscount = $product->getActivePrice() - $product->price_special;
                    $pTotal = ($product->getActivePrice() * $this_counts[$i]) - ($supports[$product->id] * $saveDiscount);
                } else {
                    $saveDiscount = 0;
                    $pTotal = ($product->getActivePrice() * $this_counts[$i]);
                }
                $discount = $saveDiscount>0 ? " Скидка ($saveDiscount) на каждую единицу" : '';
                $return['products'][$i] = [
                    'id' => $product->id,
                    'name' => $product->name,
                    'subname' => $product->subname,
                    'type_name_for_page' => $product->type_name_for_page,
                    'count' => $this_counts[$i],
                    'total' => $pTotal,
                    'discount' => $discount,
                    'url' => url('/catalog/' . $product->category->slug . '/' . $product->slug)
                ];
                $i++;
            }
        }

        $total = $orderPrice->getTotalPrice();
        $total_delivery = $orderPrice->getDeliveryPrice();
        $total_assembling = $orderPrice->getAssemblingPrice();
        $use_bonus = $orderPrice->getUseBonuses();
        $buyTodayDiscount = $orderPrice->getBuyTodayDiscount();
        $promoCodeDiscount = $orderPrice->getPromoCodeDiscount();

        $return['total'] = $total;
        $return['total_delivery'] = ($delivery_method != 'option1') ? $total_delivery : 0;
        $return['total_assembling'] = $total_assembling;
        $return['use_bonus'] = $use_bonus;
        $return['assembly'] = $assembly;
        $return['list_id'] = $list_id;
        $return['user_phone'] = $user_phone;
        $return['buy_today_discount'] = $buyTodayDiscount;
        $return['promo_code_amount'] = $promoCodeDiscount;
        $return['promo_code'] = $promo_code;

        return $return + $post;
    }

    /**
     * Get thankyou page redirect url path
     * @param $orderData
     * @return string
     */
    private function getThankYouUrl($orderData)
    {
        $params = array();
        $params['list_id'] = $orderData['list_id'];
        $params['ids']  = implode(',', array_column($orderData['products'], 'id'));
        $params['qnt']  = implode(',', array_column($orderData['products'], 'count'));

        $discount = 0;

        if ($value = $orderData['use_bonus']) {
            $discount = $value;
        } elseif ($value = $orderData['promo_code_amount']) {
            $discount = $value;
        } elseif ($value = $orderData['promo_code_amount']) {
            $discount = $value;
        }

        $params['amount'] = $orderData['total'] + $orderData['total_delivery'] + $orderData['total_assembling']
            - $discount;

        return url('/').'/thanks?'.http_build_query($params);
    }

    /**
     * User submit registration oder form
     */
    protected function signUp($formData)
    {
        try {
            if (!UserSettings::get('allow_registration', true)) {
                throw new ApplicationException(trans('luckyweb.user::lang.account.registration_disabled'));
            }
            // Validate if user email or phone exist
            $data = $formData;

            $data['phone'] = preg_replace('/[^0-9]+/', '', $data['phone']);
            $data['name'] = $data['first_name'];
            $data['last_name'] = $data['second_name'];
            $rules = [
                'phone' => 'unique:users,phone,' . Session::get('user_id'),
                'email' => 'unique:users,email,' . Session::get('user_id'),
            ];

            $attributes = [
                'phone' => '"Телефон"',
                'email' => '"Email"',
            ];

            /** @var \Illuminate\Contracts\Validation\Validator $validation */
            $validation = Validator::make($data, $rules, [], $attributes);

            if ($validation->fails()) {
                $user = User::findByEmail($data['email']);
                Auth::setUser($user);

                return $user->getKey();
            }

            /** @var User $user */
            $user = Auth::register($data, false, true);
            // Generate site version of the card
            $user->generateCard();
            $user->save();
            $user->attemptActivation($user->getActivationCode());

            $userId = $user->id;

            // Create default list and add data from cookie
            $flist = new FavoriteList();
            $flist->list_name = 'По умолчанию';
            $flist->user_id = $userId;
            $flist->save();

            FavoriteListItem::fillFromCookie();

            return $userId;
        } catch (\Exception $ex) {
            if (Request::ajax()) throw $ex;
            else Flash::error($ex->getMessage());
        }
    }

    /**
     * Get Google Analytics client ID
     */
    public function getClientIdGA()
    {
        $cid = "not-found";
        if (isset($_COOKIE['_ga'])) {
            list($version, $domainDepth, $cid1, $cid2) = explode('.', $_COOKIE["_ga"]);
            $cid = $cid1 . '.' . $cid2;
        }
        return $cid;
    }

    /**
     * Get Google Analytics session ID (client ID + timestamp)
     */
    private function getSessionIdGA()
    {
        $cid = $this->getClientIdGA();
        $timestamp = round(microtime(true), 3) * 1000;

        return $cid.'_'.$timestamp;
    }

    /*
     * Log in after order
     */
    protected function loginUser($orderData)
    {
        $userId = null;
        if (!Auth::getUser()) {
            $userId = $this->signUp($orderData);
            if ($userId) {
                $user = User::find($userId);

                $credentials = ['card_code' => $user->card_code, 'card_pin' => $user->card_pin];
                Event::fire('luckyweb.user.beforeAuthenticate', [$this, $credentials]);
                Auth::authenticate($credentials, true, 2);
            }
        }
        if (Auth::getUser()) {
            $id = Auth::getUser()->id;

            $user = User::find($id);
            $user->ga_session_id = $this->getSessionIdGA();
            $user->save();

            $userId = $id;
        }
        return $userId;
    }

    /**
     * Использовать скидку "Купи сегодня"
     * @param bool $use
     */
    private function useBuyTodayDiscount(bool $use = true)
    {
        if (request()->hasCookie(BuyTodayDiscount::COOKIE_NAME) && $use) {
            $cookieValue = json_decode(request()->cookie(BuyTodayDiscount::COOKIE_NAME), true);
            $cookieValue['used'] = true;
            $cookieValue = json_encode($cookieValue);

            // Создаем время жизни куки до конца текущего дня + 1 минута.
            $now = Carbon::now();
            $cookieLife = Carbon::now()
                ->endOfDay()
                ->addMinute()
                ->addHours(3) // Fix timezone
                ->diffInMinutes($now);

            session()->forget(BuyTodayDiscount::SESSION_KEY);
            request()->cookies->set(BuyTodayDiscount::COOKIE_NAME, $cookieValue);
            // Позволяем использовать акцию на следующий день.
            $cookie = cookie(BuyTodayDiscount::COOKIE_NAME, $cookieValue, $cookieLife, null, null, false, false);
            app(QueueingFactory::class)->queue($cookie);
        }
    }

    /**
     * @param string $code
     * @param bool $use
     * @return bool
     */
    protected function setUsedPromoCode(string $code, bool $use = true)
    {
        if (Auth::check() && $use) {
            return PromoCodeEvent::query()
                ->where('user_id', '=', Auth::getUser()->id)
                ->where('code', '=', $code)
                ->update(['applied_at' => Carbon::now()->startOfDay()]);
        }

        return false;
    }
}
