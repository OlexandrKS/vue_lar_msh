<?php

namespace LuckyWeb\MS\Components;

use Cms\Classes\CodeBase;
use Cms\Classes\ComponentBase;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;
use LuckyWeb\MS\Classes\SendPulseManager;
use LuckyWeb\User\Facades\Auth;
use Jenssegers\Agent\Agent;
use LuckyWeb\MS\Models\PagePopup;
use LuckyWeb\MS\Models\PagePopupHTML;
use LuckyWeb\User\Models\PromoCode;
use Session;
use Input;

use System\Classes\CombineAssets;

class PagePopups extends ComponentBase
{
    /**
     * Path of theme-level assets
     * @var string $themeAssetsPath
     */
    public $themeAssetsPath = '/themes/shara/assets';

    public $popup;
    public $isMobile;
    public $sendPulseObj;

    /*
         * Organic sources
         */
    protected $organic_sources = ['www.google' => ['q='],
        'daum.net/' => ['q='],
        'eniro.se/' => ['search_word=', 'hitta:'],
        'naver.com/' => ['query='],
        'yahoo.com/' => ['p='],
        'msn.com/' => ['q='],
        'bing.com/' => ['q='],
        'aol.com/' => ['query=', 'encquery='],
        'lycos.com/' => ['query='],
        'ask.com/' => ['q='],
        'altavista.com/' => ['q='],
        'search.netscape.com/' => ['query='],
        'cnn.com/SEARCH/' => ['query='],
        'about.com/' => ['terms='],
        'mamma.com/' => ['query='],
        'alltheweb.com/' => ['q='],
        'voila.fr/' => ['rdata='],
        'search.virgilio.it/' => ['qs='],
        'baidu.com/' => ['wd='],
        'alice.com/' => ['qs='],
        'yandex.com/' => ['text='],
        'najdi.org.mk/' => ['q='],
        'aol.com/' => ['q='],
        'seznam.cz/' => ['q='],
        'search.com/' => ['q='],
        'wp.pl/' => ['szukai='],
        'online.onetcenter.org/' => ['qt='],
        'szukacz.pl/' => ['q='],
        'yam.com/' => ['k='],
        'pchome.com/' => ['q='],
        'kvasir.no/' => ['q='],
        'sesam.no/' => ['q='],
        'ozu.es/' => ['q='],
        'terra.com/' => ['query='],
        'mynet.com/' => ['q='],
        'ekolay.net/' => ['q='],
        'rambler.ru/' => ['words=']
    ];

    public function __construct(CodeBase $cmsObject = null, $properties = [])
    {
        $this->sendPulseObj = new SendPulseManager();
        parent::__construct($cmsObject, $properties);

    }

    public function componentDetails()
    {
        return [
            'name' => 'PagePopup Component',
            'description' => 'Generates the popup with params'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    /**
     * Component initializing
     */
    public function onRun()
    {
//        Session::forget('openPagePopup');
        if (Session::has('openPagePopup')) {
            $popups = PagePopup::where('id', '=', Session::get('openPagePopup'))->with(['urls'])->get();
        } else {
            $popups = PagePopup::where('active', '=', true)->with(['urls'])->get();
        }
        $inputs = ['utm_source', 'utm_medium', 'utm_campaign', 'utm_content'];
        $current_popup = null;
        foreach ($popups as $popup) {
            if ($popup->page_show == 'all_page') {
                $current_popup = $popup;
                break;
            }
            if ($popup->page_show == 'this_page' || $popup->page_show == 'not_this_page') {
                $is_url = $popup->urls->contains('url', $this->currentPageUrl());
                if ($popup->page_show == 'this_page' && $is_url) {
                    $current_popup = $popup;
                    break;
                } elseif ($popup->page_show == 'not_this_page' && !$is_url) {
                    $current_popup = $popup;
                    break;
                }
            }
        }

        if ($current_popup && $current_popup->variant_footer_popup == 'subscribe'
            && $current_popup->promo_code != 0 && Auth::check()) {
            $is_in_books = $this->sendPulseObj->getEmailInfo(Auth::getUser()->email);
            if (isset($is_in_books->is_error) && $is_in_books->is_error) $current_popup = null;
        }

        $is_advertising = $is_organic = $is_direct_hit = false;
        if (!empty($current_popup->source_traffic)) {
            foreach ($current_popup->source_traffic as $traffic) {
                switch ($traffic) {
                    case 'advertising':
                        $is_advertising = true;
                        break;
                    case 'organic':
                        $is_organic = true;
                        break;
                    case 'direct_hit':
                        $is_direct_hit = true;
                        break;
                }
            }
        }

        if ($is_advertising && (Input::get('utm_source') || Input::get('utm_medium') || Input::get('utm_campaign') || Input::get('utm_content'))) {
            // if 4 param required
            if ($current_popup) {
                $this->popup = $current_popup->where(function ($query) use ($inputs) {
                    foreach ($inputs as $value) {
                        $query->where($value, '=', (Input::get($value) ? Input::get($value) : ''));
                    }
                })->first();
                if (!$this->popup) {
                    // if 3 param required
                    $this->popup = $current_popup->where(function ($orquery) use ($inputs) {
                        foreach ($inputs as $value) {
                            if (Input::get($value)) {
                                $orquery->orWhere(function ($query) use ($inputs, $value) {
                                    foreach ($inputs as $input) {
                                        if ($input == $value) {
                                            $query->where($input, '=', '');
                                        } else {
                                            $query->where($input, '=', Input::get($input));
                                        }
                                    }
                                    return $query;
                                });
                            }
                        }
                        return $orquery;
                    })->first();
                    if (!$this->popup) {
                        // if 2 param required
                        $this->popup = $current_popup->where(function ($orquery) use ($inputs) {
                            $index = 0;
                            foreach ($inputs as $value) {
                                $index++;
                                foreach (array_slice($inputs, $index) as $input) {
                                    $orquery->orWhere(function ($query) use ($inputs, $input, $value) {
                                        foreach ($inputs as $except) {
                                            if ($except == $value || $except == $input) {
                                                $query->where($except, '=', (Input::get($except) ? Input::get($except) : ''));
                                            } else {
                                                $query->where($except, '=', '');
                                            }
                                        }
                                        return $query;
                                    });
                                }
                            }
                            return $orquery;
                        })->first();
                        if (!$this->popup) {
                            // if 1 param required
                            $this->popup = $current_popup->where(function ($orquery) use ($inputs) {
                                foreach ($inputs as $value) {
                                    if (Input::get($value)) {
                                        $orquery->orWhere(function ($query) use ($inputs, $value) {
                                            $query->where($value, '=', Input::get($value));
                                            foreach ($inputs as $input) {
                                                if ($input != $value) {
                                                    $query->where($input, '=', '');
                                                }
                                            }
                                            return $query;
                                        });
                                    }
                                }
                                return $orquery;
                            })->first();
                        }
                    }
                }
            }
        }
        if (($is_organic && isset($_SERVER['HTTP_REFERER']) && $this->isTrafficOrganic($_SERVER['HTTP_REFERER'])) || $is_direct_hit)
            $this->popup = $current_popup;
        if ($this->popup) {
            Session::put('openPagePopup', $this->popup->id);
        } else {
            $this->popup = null;
        }
        $is_device = false;
        if (!empty($this->popup->devices)) {
            $agent = new Agent();
            foreach ($this->popup->devices as $device) {
                switch ($device) {
                    case 'mobile':
                        if ($agent->isMobile()) $this->isMobile = $is_device = true;
                        break;
                    case 'tablet':
                        if ($agent->isTablet()) $is_device = true;
                        break;
                    case 'pc':
                        if ($agent->isDesktop()) $is_device = true;
                        break;
                }
            }
        }

        if ($is_device) {
            $this->addJs(CombineAssets::combine([
                '/plugins/luckyweb/ms/components/pagepopups/assets/js/page_popup.js'
            ], base_path()));
        }
    }

    public function onHidePagePopup()
    {
        Session::put('openPagePopup', false);
    }

    public function onShowPagePopupHTML()
    {
        $result = PagePopupHTML::where('url_hash', '=', post('url_hash'))->firstOrFail();
        return [
            '.myPagePopupHTMLModalContent' => $result->html
        ];
    }

    public function onSendEmailActivatePromoCode()
    {
        $email = request()->input('email');
        $is_in_books = $this->sendPulseObj->getEmailInfo($email);
        if (isset($is_in_books->is_error) && $is_in_books->is_error && Session::has('openPagePopup')) {
            $popup = PagePopup::find((Session::get('openPagePopup')));
            $promo_code = PromoCode::findOrFail($popup->promo_code);
            if ($promo_code) {
                $url = url('promo-code', ['code' => $promo_code->code, 'e' => base64_encode($email)]);
                $client = new Client;
                $response = $client->post('https://events.sendpulse.com/events/id/806e6f895f674201b599c5e6c512e5f7/7051313', [
                    RequestOptions::JSON => ['email' => $email, 'active_promocode_url' => $url],
                ]);
                $response = json_decode($response->getBody(), true);
                if ($response['result']) {
                    $this->sendPulseObj->addEmailsInBook([['email' => $email]]);
                    return 'На указанный вами электронный адрес ' . $email . ' был выслан промокод!';
                }
            }
        }
        return 'На Ваш электронный адрес ' . $email . ' уже  был выслан промокод!';
    }

    protected function encodeEmail($email)
    {
        $qEncoded = urlencode(encrypt($email));
        return ($qEncoded);
    }

    /**
     * Check if source is organic
     * @param string $referrer The referrer page
     * @return true if organic, false if not
     */
    protected function isTrafficOrganic($referrer)
    {
        foreach ($this->organic_sources as $searchEngine => $queries) {
            if (strpos($referrer, $searchEngine) !== false) {
                foreach ($queries as $query) {
                    if (strpos($referrer, $query) !== false) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
