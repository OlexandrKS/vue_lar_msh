<?php namespace LuckyWeb\MS\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use LuckyWeb\MS\Models\Vacancy;
use LuckyWeb\MS\Models\VacancyRequest;
use Sentry\Util\JSON;
use System\Classes\CombineAssets;

class Vacancies extends ComponentBase
{
    /**
     * Available cities list
     * @var array
     */
    public $availableCities;

    /**
     * Available cities list
     * @var JSON
     */
    public $jsonCities;

    /**
     * Component index
     * @var int
     */
    public $index;

    /**
     * Path of theme-level assets
     * @var string $themeAssetsPath
     */
    public $themeAssetsPath = '/themes/shara/assets';

    public function componentDetails()
    {
        return [
            'name'        => 'Vacancies Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'index' => [
                'title'             => 'Component index',
                'description'       => 'Component index',
                'default'           => '1',
                'type'              => 'integer'
            ],
        ];
    }

    public function onRun()
    {
        $this->addJs(CombineAssets::combine([
            $this->themeAssetsPath.'/js/plugins/inputmask/jquery.inputmask.min.js',
            '/plugins/luckyweb/ms/components/vacancies/assets/js/vacancies.js'
        ], base_path()));

        // Get all cities used in vacancies
        Vacancy::activated()->get()->each(function($item) {
            foreach ($item->cities as $city) {
                $this->availableCities[$city->id] = $city;
            }
        });

        $this->availableCities = collect($this->availableCities)->sortBy('name')->all();

        $this->jsonCities = json_encode( $this->availableCities);
    }

    public function onRender()
    {
        $this->index = $this->property('index');
    }

    /**
     * Render vacancies list for given city id
     * @return array
     */
    public function onCitySelected()
    {
        if(post('city_id')) {
            $vacancies = Vacancy::activated()->whereHas('cities', function($query){
                $query->where('id', post('city_id'));
            })->get();

            return [
                '.js-vacancies_list_'.post('index', 1) => $this->renderPartial(
                    'vacancies::vacancies_list',
                    ['vacancies' => $vacancies, 'index' => post('index', 1)]
                )
            ];
        }
    }

    /**
     * Render vacancies request form for given vacancy id
     */
    public function onShowVacancyRequestForm()
    {
        if(post('vacancy_id')) {
            $vacancy = Vacancy::activated()->where('id', post('vacancy_id'))->first();
            if($vacancy) {
                return [
                    '.js-vacancy_request_modal .modal-content' => $this->renderPartial(
                        'vacancies::vacancy_request_form',
                        ['vacancy' => $vacancy]
                    )
                ];
            }
        }
    }

    /**
     * Save vacancy request form
     */
    public function onSubmitVacancyRequestForm()
    {
        $post = post();
        $post['phone'] = preg_replace('/[^0-9]+/', '', $post['phone']);

        $request = new VacancyRequest();
        $request->fill($post);
        if(Input::hasFile('photo') && Input::file('photo')->isValid()) {
            $request->photo = Input::file('photo');
        }
        if(Input::hasFile('resume') && Input::file('resume')->isValid()) {
            $request->resume = Input::file('resume');
        }

        $request->save();

        Event::fire('luckyweb.ms.vacancyRequestCreated', $request);
        return ['.js-vacancy_request_modal .modal-content' => $this->renderPartial('vacancies::success_message')];
    }
}
