<?php namespace LuckyWeb\MS\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Kalnoy\Nestedset\Collection;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\Review;
use LuckyWeb\MS\Models\ReviewMessage;
use LuckyWeb\MS\Models\ShopCity;
use October\Rain\Exception\ValidationException;
use System\Classes\CombineAssets;

class Reviews extends ComponentBase
{
    /**
     * Path of theme-level assets
     * @var string $themeAssetsPath
     */
    public $themeAssetsPath = '/themes/shara/assets';

    /**
     * Reviews list
     */
    public $reviews;

    /**
     * Parameter to use for the page number
     * @var string
     */
    public $pageParam;

    /**
     * Related product slug
     * @var string
     */
    public $product;

    /**
     * Related products
     * @var \Illuminate\Support\Collection
     */
    public $products;

    /**
     * User likes from Cookie
     * @var array
     */
    public $likes;

    /**
     * Reviews filer
     * @var
     */
    public $filter;


    public function componentDetails()
    {
        return [
            'name'        => 'Reviews',
            'description' => 'Provide user reviews functional'
        ];
    }

    public function defineProperties()
    {
        return [
            'pageNumber' => [
                'title'       => 'Номер страницы',
                'description' => 'Номер страницы для пагинации',
                'type'        => 'string',
                'default'     => '{{ :page }}',
            ],
            'postsPerPage' => [
                'title'             => 'Количество отзывов',
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'Количество отзывов на странице',
                'default'           => '10',
            ],
            'product' => [
                'title'             => 'Product slug',
                'description'       => 'If set - get/set reviews related to product',
                'default'           => '',
                'type'              => 'object'
            ],
        ];
    }

    public function onRun()
    {
//        $this->addJs(CombineAssets::combine([
            // $this->themeAssetsPath.'/js/plugins/inputmask/jquery.inputmask.min.js',
//            $this->themeAssetsPath.'/js/plugins/select2/js/select2.full.min.js',
//            $this->themeAssetsPath.'/js/plugins/select2/js/i18n/ru.js',
//            '/plugins/luckyweb/ms/components/reviews/assets/js/reviews.js'
//        ], base_path()));

//        $this->addCss(CombineAssets::combine([
//            $this->themeAssetsPath.'/js/plugins/select2/css/select2.css'
//        ], base_path()));

        $this->pageParam = $this->page['pageParam'] = $this->paramName('pageNumber');
        $this->page['reviewPageNumber'] = $this->property('pageNumber');
        $this->page['landingMode'] = $this->property('landingMode');

        // Updated page title if 2nd+ page of reviews.
        if(!empty(in_array($this->page->id, ['product', 'lending']) && $this->page['reviewPageNumber'])) {
            $this->page->title = 'Страница '.$this->page['reviewPageNumber'].'. Отзывы покупателей о '.$this->page->title;
            $this->page->description = 'Страница '.$this->page['reviewPageNumber'].' отзывов. '.$this->page->description;
        }

        $this->likes = Cookie::get('rw_likes', []);
        $this->filter['sorting'] = Cookie::get('rw_sorting', 2);
        $this->filter['city_id'] = Cookie::get('rw_city_id');
    }

    public function onRender()
    {
        $this->product = $this->property('product');

        $query = Review::where('status_id', Review::STATUS_APPROVED);

        if ($this->product) {
            $query->where('product_slug', $this->product->slug);
        }

        if(!empty($this->filter['city_id'])) {
            $query->where('city_id', $this->filter['city_id']);
        }
        if($this->filter['sorting'] == 1) {
            $query->orderBy('created_at', 'DESC');
        }
        elseif($this->filter['sorting'] == 2) {
            $query->orderByRaw('likes-dislikes DESC, id DESC');
        }

        if($this->product) {
            if($this->product->reviewGroups->first()) {
                $query = Review::where('status_id', Review::STATUS_APPROVED)
                    ->where('product_group_id', $this->product->reviewGroups->first()->id);
            }
            elseif(!empty($this->product->slug)) {
                $query = Review::where('status_id', Review::STATUS_APPROVED)
                    ->where('product_slug', $this->product->slug);

            }
        }

        if($this->property('postsPerPage') > 0) {
            $this->reviews = $query->paginate($this->property('postsPerPage'), $this->property('pageNumber'));
        }
        else {
            $this->reviews = $query->get();
        }

        $slugs = array_unique(array_pluck($this->reviews->items(), 'product_slug'));

        $this->products = Product::whereIn('slug', $slugs)->get();

//        else {
//            $query = Review::where('status_id', Review::STATUS_APPROVED);
//        }
//
//        if(!empty($this->filter['city_id'])) {
//            $query->where('city_id', $this->filter['city_id']);
//        }
//        if($this->filter['sorting'] == 1) {
//            $query->orderBy('created_at', 'DESC');
//        }
//        elseif($this->filter['sorting'] == 2) {
//            $query->orderByRaw('likes-dislikes DESC, id DESC');
//        }
//
//        if($this->property('postsPerPage') > 0) {
//            $this->reviews = $query->paginate($this->property('postsPerPage'), $this->property('pageNumber'));
//        }
//        else {
//            $this->reviews = $query->get();
//        }
    }


    /**
     * Fires when user submit add review form
     */
    public function onReviewAdd() {

        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => ['required', 'regex:/^7[0-9]{10}$/'],
            'city_id' => 'required',
            'contract_number' => 'required_if:is_client,1|regex:/[0-9]{3}-[0-9]+/',
            'text' => 'required',
            'is_accept' => 'accepted'
        ];
        $customMessages = [
            'required' => 'Обязательное поле',
            'email' => 'Неверный формат email',
            'phone.regex' => 'Неверный формат номера телефона',
            'accepted' => 'Вы должны принять условия размещения отзывов',
            'contract_number.required_if' => 'Обязательное поле для наших клиентов',
            'contract_number.regex' => 'Неверный формат номера договора'
        ];
        $post = post();
        $post['phone'] = preg_replace('/[^0-9]+/', '', $post['phone-number']);
        unset($post['phone-number']);
        if(isset($post['contract_number'])) {
            $post['contract_number'] = preg_replace('/[^0-9|-]+/', '', $post['contract_number']);
        }
        $post['text'] = strip_tags($post['text']);

        $validator = Validator::make($post, $rules, $customMessages);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $review = new Review($post);

        // Set product group
        if(isset($post['product_slug']) && !empty($post['product_slug'])) {
            $product = Product::findBySlug($post['product_slug']);
            if($product && $product->reviewGroups->first()) {
                $review->product_group_id = $product->reviewGroups->first()->id;
            }
        }

        $review->save();

        // Create review message
        if($review) {
            $messages = new ReviewMessage();
            $messages->text = $post['text'];
            $messages->review_id = $review->id;
            $messages->save();

            Event::fire('luckyweb.ms.reviewCreated', [$review]);
        }

        return [
            '.js-review_form' => $this->renderPartial('reviews::success_add_message.htm'),
        ];
    }

    /**
     * Handle user like event
     */
    public function onLike()
    {
        $this->likes = Cookie::get('rw_likes', []);
        $review = Review::find(intval(post('review_id')));

        if(!empty($review)) {
            if(isset($this->likes[$review->id]) && $this->likes[$review->id] == 1) {
                unset($this->likes[$review->id]);
                $review->decrement('likes');
            }
            elseif(isset($this->likes[$review->id]) && $this->likes[$review->id] == -1) {
                $this->likes[$review->id] = 1;
                $review->decrement('dislikes');
                $review->increment('likes');
            }
            else {
                $this->likes[$review->id] = 1;
                $review->increment('likes');
            }
            Cookie::queue('rw_likes', $this->likes, 525600);
        }
        return ['likes_html' => $this->renderPartial('reviews::likes', ['review' => $review])];
    }

    /**
     * Handle user dislike event
     */
    public function onDislike()
    {
        $this->likes = Cookie::get('rw_likes', []);
        $review = Review::find(intval(post('review_id')));

        if(!empty($review)) {
            if(isset($this->likes[$review->id]) && $this->likes[$review->id] == -1) {
                unset($this->likes[$review->id]);
                $review->decrement('dislikes');
            }
            elseif(isset($this->likes[$review->id]) && $this->likes[$review->id] == 1) {
                $this->likes[$review->id] = -1;
                $review->decrement('likes');
                $review->increment('dislikes');
            }
            else {
                $this->likes[$review->id] = -1;
                $review->increment('dislikes');
            }
            Cookie::queue('rw_likes', $this->likes, 525600);
        }
        return ['likes_html' => $this->renderPartial('reviews::likes', ['review' => $review])];
    }

    /**
     * Handle sort control switching
     */
    public function onSetFilter()
    {
        if(!empty(post('sorting'))) {
            Cookie::queue('rw_sorting', post('sorting'), 43200);
            Cookie::queue('rw_city_id', post('city_id'), 43200);
            Session::flash('sorting_changed', true);

            if($this->property('product')) {
                return Redirect::to($this->pageUrl('product', [
                    'category_slug' => $this->page->param('category_slug'),
                    'product_slug' => $this->page->param('product_slug'),
                    'page' => 1
                ]));
            }
            else {
                return Redirect::to($this->pageUrl($this->page->id));
            }
        }
    }

    /**
     * TRUE if need to open to review on product page
     * @return bool
     */
    public function isOpenReviews()
    {
        return $this->page['reviewPageNumber'] || Session::has('sorting_changed') || $this->page['landingMode'];
    }

    /**
     * TRUE if need to scroll to review on product page
     * @return bool
     */
    public function isScrollToReviews()
    {
        return $this->page['reviewPageNumber'] || Session::has('sorting_changed');
    }

    /**
     * @return array
     */
    public function getSortingValues()
    {
        return [
            1 => 'По дате',
            2 => 'По рейтингу'
        ];
    }

    /**
     * Get like for given review if exists
     * @param $reviewId
     * @return mixed|null
     */
    public function getLike($reviewId)
    {
        return isset($this->likes[$reviewId]) ? $this->likes[$reviewId] : null;
    }

    /**
     * Calculate pagination first range
     * @return int
     */
    public function getPaginationRangeFirst()
    {
        $offset = $this->reviews->lastPage() - $this->reviews->currentPage() < 2
            ? $this->reviews->currentPage()-$this->reviews->lastPage()+4
            : 2;

        return $this->reviews->currentPage()-$offset > 0 ? $this->reviews->currentPage()-$offset : 1;
    }

    /**
     * Calculate pagination last range
     * @return int
     */
    public function getPaginationRangeLast()
    {
        $offset = $this->reviews->currentPage() < 3
            ? 5 - $this->reviews->currentPage()
            : 2;

        return $this->reviews->currentPage()+$offset < $this->reviews->lastPage()
            ?  $this->reviews->currentPage()+$offset
            : $this->reviews->lastPage();
    }

    /**
     * Return cities list
     * @return mixed
     */
    public function getCities() {
        return ShopCity::orderBy('name', 'ASC')->get();
    }

    /**
     * Return products for given review
     * @param $review
     * @return \Illuminate\Support\Collection
     */
    public function getProducts($review) {
        /*if($review->product_group) {
            return $review->product_group->products;
        }
        else*/if($review->product_slug) {
            return collect([Product::findBySlug($review->product_slug)]);
        }
    }
}
