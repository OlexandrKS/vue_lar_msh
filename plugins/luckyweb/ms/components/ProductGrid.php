<?php

namespace LuckyWeb\MS\Components;

use Cms\Classes\ComponentBase;

use Jenssegers\Agent\Facades\Agent;
use LuckyWeb\MS\Models\Category;
use LuckyWeb\MS\Models\Image;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\Promotion;
use LuckyWeb\MS\Models\SubCategory;
use October\Rain\Database\Collection;
use System\Classes\CombineAssets;

class ProductGrid extends ComponentBase
{
    /**
     * Path of theme-level assets
     * @var string $themeAssetsPath
     */
    public $themeAssetsPath = '/themes/shara/assets';

    /**
     * Coolection of page products
     * @var Collection $products
     */
    public $products;

    /**
     * Current mixed promotion
     * @var \LuckyWeb\MS\Models\Promotion
     */
    public $mixedPromotion;

    /**
     * Database field of product row position
     * @var string $rowField
     */
    public $rowField;

    /**
     * Database field of product column position
     * @var string $columnField
     */
    public $columnField;

    /**
     * Container width
     * @var integer $maxWidth
     */
    public $maxWidth = 1170;

    /**
     * Double product padding (padding*2)
     * @var integer $padding
     */
    public $padding = 30;

    /**
     * @var Category
     */
    public $category;

    public function componentDetails()
    {
        return [
            'name' => 'ProductGrid Component',
            'description' => 'Generates the grid of prodicts for category page and not only'
        ];
    }

    public function defineProperties()
    {
        return [
            'category_slug' => [
                'title' => 'Category slug',
                'description' => 'Category slug',
                'default' => '{{ :category_slug }}',
                'type' => 'string'
            ],
            'subcategory_slug' => [
                'title' => 'Subcategory slug',
                'description' => 'Subcategory slug',
                'default' => '{{ :subcategory_slug }}',
                'type' => 'string'
            ]
        ];
    }

    public function productionMode()
    {
        return strtolower(env('APP_ENV')) == 'production';
    }

    /**
     * Component initializing
     */
    public function onRun()
    {
        // category mode
        $this->category = Category::findBySlug($this->property('category_slug'));
        if ($this->category) {
            $this->rowField = 'position_row';
            $this->columnField = 'position_column';

            $subcategory = SubCategory::findBySlug($this->property('subcategory_slug'));
            $query = Product::where('category_id', $this->category->id)
                ->has('images');

            if ($subcategory) {
                $query->where('sub_category_id', $subcategory->id);
            }

            $this->products = $query->where($this->rowField, '!=', 0)
                ->where($this->columnField, '!=', 0)
                ->orderBy($this->rowField)
                ->orderBy($this->columnField)
                ->get();
        } else { // mixed promotions mode
            $this->rowField = 'position_mp_row';
            $this->columnField = 'position_mp_column';
            $this->products = Product::where('has_mixed_promo', '=', true)
                ->where($this->rowField, '!=', 0)
                ->where($this->columnField, '!=', 0)
                ->has('images')
                ->orderBy($this->rowField)
                ->orderBy($this->columnField)
                ->get();
        }

        $this->products->load('nid');

        $this->mixedPromotion = Promotion::mixedPromotions()->current()->first();

        $this->addJs(CombineAssets::combine([
            $this->themeAssetsPath.'/vendor/appear/appear.min.js',
            '/plugins/luckyweb/ms/components/productgrid/assets/js/product_grid.js'
        ], base_path()));
    }

    /**
     * Returns array of calculated proportions for products in the row
     *
     * @param integer $row - The row
     *
     * @return array - [product_id => [
     *                      'width' => ..,   // orig width
     *                      'heigh' => ..,   // orig height
     *                      'scale' => ..,   // calculated image scale
     *                      'scale_container' => ..,   // calculated product scale of row width
     *                     ], ...]
     */
    public function rowProportions($row)
    {
        $products = $this->products->where($this->rowField, $row)->sortBy($this->columnField);
        $maxWidth = $this->maxWidth;
        $padding = $this->padding;

        $proportions = [];
        $maxHeight = 0;
        /** @var Product $product */
        foreach ($products as $product) {
            $size = $product->getPreviewImage(Image::ASSIGNMENT_PREVIEW_DESKTOP)->image_size;
            $proportions[$product->getKey()] = [
                'width' => $size[0],
                'height' => $size[1],
            ];
            if ($size[1] > $maxHeight) {
                $maxHeight = $size[1];
            }
        }
        $totalWidth = 0;
        foreach ($proportions as $key => $data) {
            $proportions[$key]['scale'] = $maxHeight / $data['height'];
            $totalWidth += $data['width'] * $proportions[$key]['scale'];
        }
        $scale = ($maxWidth - $padding * ($products->count() - 1)) / $totalWidth;
        $i = 0;
        foreach ($proportions as $key => $data) {
            $proportions[$key]['scale'] *= $scale;

            //$padDiv = $i == 0 || $i == count($proportions) - 1 ? 2 : 1;
            //Added two paddings for first and last items in the row too.
            $padDiv = 2;

            $proportions[$key]['scale_container'] = ($data['width'] * $proportions[$key]['scale'] + $padding / $padDiv) / $maxWidth;
            $i++;
        }

        return $proportions;
    }

    /**
     * Returnes the row of given product
     * @param Product $product
     *
     * @return integer
     */
    public function productRow($product)
    {
        return $product->{$this->rowField};
    }

    /**
     * Returnes the column of given product
     * @param Product $product
     *
     * @return integer
     */
    public function productColumn($product)
    {
        return $product->{$this->columnField};
    }

    /**
     * Returns number of products in the given row
     *
     * @return integer
     */
    public function productsInRow($row)
    {
        return $this->products->where($this->rowField, $row)->count();
    }

    /**
     * Check if need to show simple grid
     * @return mixed
     */
    public function isSimpleGrid()
    {
        return Agent::isPhone();
    }

}
