var ProductRelated = function () {
    var timer = undefined;

    var initRelatedDetailButton = function(){
        $('.js-related_detail').on('click', function(e) {
            var relatedId = $(this).data('id');

            e.preventDefault();

            $.request('onRelatedDetail', {
                data: {
                    related_id: relatedId
                },
                success: function(data){
                    this.success(data);
                    var $owl = $('.js-related_modal .js-related_slider').owlCarousel({
                        singleItem: true,
                        pagination: false,
                        items: 1,
                        autoplay: true,
                        autoplayTimeout: 400000,
                        autoplayHoverPause: true,
                        autoplaySpeed: 1000,
                        navSpeed: 1000,
                        dotsSpeed: 1000,
                        loop: true,
                        navigation: false,
                        lazyLoad: true,
                        lazyLoadEager: 1
                    }).data('owlCarousel');

                    $('.js-related_modal .js-slider_controls .slider-control').on('click', function(){
                        $owl.goTo($(this).data('id'));
                    });
                    $('.js-related_modal').modal('show');
                }
            });
        });
    };

    var equalizerRows = function(){
        var max = 0;
        $('.js-related_row').each(function(key, row) {
            max = 0;
            var $items = $(row).find('>div');
            $items.each(function(key, item) {
                var height = $(item).height();

                if(height > max) {
                    max = height;
                }
            });

            $items.css('min-height', max+'px');
        });
    };

    var initProductImpressionsTracker = function(){
        appear({
            elements: function elements(){
                return document.getElementsByClassName('product');
            },
            appear: function appear(el){
                try {
                    // Ecommerce event for GTM. Product Impressions
                    window.dataLayer = window.dataLayer || [];
                    dataLayer.push({
                        'ecommerce': {
                            'impressions': [
                                $(el).data('ecommerce')
                            ]
                        },
                        'event': 'gtm-ee-event',
                        'gtm-ee-event-category': 'Enhanced Ecommerce',
                        'gtm-ee-event-action': 'Product Impressions',
                        'gtm-ee-event-non-interaction': 'True'
                    });
                } catch(e){}
            }
        });
    };

    var initLazy = function() {
        $('.js-nav_tabs a[href=#elements]').on('show.bs.tab', function (e) {
            $($(this).attr('href')+' .js-lazy_tab').lazy();
        });
    };

    var initRowUpdater = function () {
        $('#elements .js-lazy_tab').on('load',function(){

            clearTimeout(timer);
            timer = setTimeout(function () {
                equalizerRows();
            }, 200);
        });
    };

    return {
        init: function () {
            initLazy();
            initRelatedDetailButton();
            initRowUpdater();

            $('a[href="#elements"]').on('shown.bs.tab', function (e) {
                if(ProductRelatedVariables.productionMode) {
                    initProductImpressionsTracker();
                }
            });
        }
    };
}();

// Initialize when page was loaded
$(function() {
    $(document).ready(function () {
        ProductRelated.init();
    });
});