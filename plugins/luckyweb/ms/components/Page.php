<?php namespace LuckyWeb\MS\Components;

use App\Backend\ServiceLayer\Constructor\Manager;
use Cms\Classes\ComponentBase;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Lang;
use App\Backend\Entities\Constructor\Page as ConstructorPage;
use App\Backend\Entities\Constructor\ComponentContent;
use LuckyWeb\MS\Models\Image;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\Review;
use LuckyWeb\MS\Models\SiteGoodType;

class Page extends ComponentBase
{
    public $contents;
    public $text;

    public function componentDetails()
    {
        return [
            'name' => 'Page Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $manager = app(Manager::class);

        try {
            $this->contents = $manager->render(request(), $this->page);
        } catch (ModelNotFoundException $e) {
            return $this->controller->run('404');
        }
    }
}
