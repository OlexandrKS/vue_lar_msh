var Cart = function () {
    var requiredFilled = false;
    var checkboxChecked = false;

    var initCartFormButton = function(){
        $('.js-product_cart_button').on('click', function(e){
            e.preventDefault();
            var productId = $(this).data('id');

            $.request('onAddProduct', {
                data: {
                    product_id: productId,
                },
                success: function(data){
                    this.success(data);
                    $('.js-cart_form_modal').modal('show');
                    actionsRow();
                    initSetCartPopup();
                    initCartClose();
                    initMagnificPopup();
                }
            });
        })
    };

    var checkOS = function(){
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;

        // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
            return "Windows Phone";
        }

        if (/android/i.test(userAgent)) {
            return "Android";
        }

        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
            return "iOS";
        }

        return "unknown";
    };

    var actionPhoneMask = function(){
       // if(checkOS() != "Android")
           $(".js-cart_order_form [name='phone']").mask('+7 999 999 99 99');
    };

    var initCartSupportFormButton = function(){
        $('.js-product_cart_support_button').on('click', function(e){
            e.preventDefault();
            var $this = $(this);
            var productId = $this.data('id');
            var supportId = $this.data('support-id');

            if(CartVariables.productionMode) {
                sendPromotionClickEvent();
            }

            $.request('onAddProduct', {
                data: {
                    product_id: productId,
                    support_id: supportId
                },
                success: function(data){
                    this.success(data);
                    $('.js-cart_form_modal').modal('show');
                    actionsRow();
                    initSetListPopup();
                    initListClose();
                }
            });
        })
    };

    var sendPromotionClickEvent = function(){
        try {
            // Ecommerce event for GTM. Promotion Clicks
            window.dataLayer = window.dataLayer || [];
            dataLayer.push({
                'ecommerce': {
                    'promoClick': {
                        'promotions': [
                            {
                                'name': 'Together Cheaper: Buy Button'
                            }
                        ]
                    }
                },
                'event': 'gtm-ee-event',
                'gtm-ee-event-category': 'Enhanced Ecommerce',
                'gtm-ee-event-action': 'Promotion Clicks',
                'gtm-ee-event-non-interaction': 'False'
            });
        } catch(e){}
    };

    var initEmptyCartFormButton = function(){
        $('.js-empty_order_list').on('click', function(e){
            e.stopPropagation();
            $.request('onEmptyList', {
                data: {
                },
                success: function(data){
                    this.success(data);
                    $('.js-cart_form_modal').modal('show');
                    actionsRow();
                }
            });
        })
    };

    var setScrollPosition = function(scrollPoss) {
        $('.newcart').scrollTop(scrollPoss);
    };

    var initDeleteCartItem = function () {
        $('body').on('click', '.js-remove_cart_item', function () {

            var scrollPoss = $('.newcart').scrollTop();
            var productId = $(this).data('id');
            var listId = $(this).data('request-data');
            var deliveryMethod = $('input[name=delivery_method]:checked').val();
            var useBonus = $('input[name=use_bonus]').val();
            var assembly = $('input[name=assembly]:checked').is(":checked");

            $.request('onDeleteCartItem', {
                data: {
                    cart_id: listId,
                    product_id: productId,
                    delivery_method: deliveryMethod,
                    use_bonus: useBonus,
                    assembly: assembly ? assembly : '',
                },
                success: function (data) {
                    this.success(data);
                    initEmptyCartFormButton();
                    actionsRow();
                    initSetCartPopup();
                    initCartClose();
                    initMagnificPopup();
                    setScrollPosition(scrollPoss);
                }
            });
        });
    };

    var displayFieldErrors = function($form, jqXHR) {
        $form.find('.form-group.has-error').each(function () {
            $(this).removeClass('has-error').find('.error').hide();
        });

        if (typeof jqXHR.responseJSON != 'undefined') {
            $.each(jqXHR.responseJSON.X_OCTOBER_ERROR_FIELDS, function (field, message) {
                var $group = $form.find("[name='"+field+"']").parents('.form-group').addClass('has-error');
                var $error = $group.find(".error");
                if ($error.length) {
                    $error.eq(0).text(message).show();
                }
                else {
                    $group.append('<div class="error">'+message+'</div>');
                }
            });
        }
    };

    var initCartButtonPopup = function(){
        $('.js-count_cart')
            .on('click', function(e){
                e.preventDefault();

                $.request('onShowCart', {
                    success: function(data){
                        this.success(data);
                        $('.js-cart_form_modal').modal('show');
                        actionsRow();
                        initSetCartPopup();
                        initCartClose();
                        initMagnificPopup();
                    }
                });
            });
    };

    var actionsRow = function(){
        var div = $('.cart-product');
        div.find('.plusminus').on('click', function(e){
            e.preventDefault();
            var numb = Number( $(this).parent().find('.number').text() );
            var scrollPoss = $('.newcart').scrollTop();

            if ($(this).attr("id") == 'minus') {
                if (numb > 1) --numb;
                    //$(this).parent().find('.number').text(--numb);
            }
            else
            if ($(this).attr("id") == 'plus')
            {
                if(numb<10) ++numb;
                    //$(this).parent().find('.number').text(++numb);
            }
            else
            if ($(this).attr("id") == 'count')
            {
                if(numb<10) ++numb;
                    //$(this).parent().find('.number').text(++numb);
            }
            var productId = $(this).data('id');
            var listId = $(this).data('request-data');
            var delivery_method = $('input[name=delivery_method]:checked').val();
            var use_bonus = $('input[name=use_bonus]').val();
            var assembly = $('input[name=assembly]:checked').is(":checked");
            if(productId){
                $.request('onChangeCountCartItem', {
                    data: {
                        product_id: productId,
                        count: numb,
                        list_id: listId,
                        delivery_method: delivery_method,
                        use_bonus: use_bonus,
                        assembly: assembly ? assembly : '',
                    },
                    success: function(data){
                        this.success(data);

                        actionsRow();
                        initSetCartPopup();
                        initCartClose();
                        initMagnificPopup();
                        setScrollPosition(scrollPoss);
                    }
                });
            }
            //var price = Number($(this).parent().parent().parent().find('#price').text());
            //$(this).parent().parent().parent().find('#summ').text(formatNumber(numb*price)+' ');
            //summTotal();
        })
    }

    var initChangeCount = function () {
        $('body').on('change', '.changecount', function(event) {
            event.preventDefault();
            var numb = Number( $(this).val() );
            var scrollPoss = $('.newcart').scrollTop();
            var productId = $(this).data('id');
            var listId = $(this).data('request-data');
            var delivery_method = $('input[name=delivery_method]:checked').val();
            var use_bonus = $('input[name=use_bonus]').val();
            var assembly = $('input[name=assembly]:checked').is(":checked");
            if(productId){
                $.request('onChangeCountCartItem', {
                    data: {
                        product_id: productId,
                        count: numb,
                        list_id: listId,
                        delivery_method: delivery_method,
                        use_bonus: use_bonus,
                        assembly: assembly ? assembly : '',
                    },
                    success: function(data){
                        this.success(data);
                        initSetCartPopup();
                        initCartClose();
                        initMagnificPopup();
                        setScrollPosition(scrollPoss);
                    }
                });
            }
        });
    }

    function formatNumber (num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")
    }

    var summTotal = function(){
        var total = 0;
        $('.summ').each(function () {
            total += Number.parseInt($(this).text().replace(/\s+/g, ''));
            console.log(total);
        });
        $('#total').text(formatNumber(total)+' ');
    };

    var initItemRemoveButton = function(){
        $('body').on('click', '.js-remove_cart_item', function(){
            var $this = $(this);

            try {
                window.dataLayer = window.dataLayer || [];
                dataLayer.push({
                    'ecommerce': {
                        'remove': {
                            'products': [
                                $this.data('ecommerce')
                            ]
                        }
                    },
                    'event': 'gtm-ee-event',
                    'gtm-ee-event-category': 'Enhanced Ecommerce',
                    'gtm-ee-event-action': 'Removing a Product from a Shopping Cart',
                    'gtm-ee-event-non-interaction': 'False'
                });
            } catch(e){}
        });
    };

    var initSetCartPopup = function() {
        $('.js-set_popup-cart').on('click', function(e){
            e.preventDefault();
            var $this = $(this);
            if($this.hasClass('open')) {
                $this.find('i').removeClass('fa-chevron-up')
                    .addClass('fa-chevron-down');

                $this.removeClass('open');
                $this.siblings('.js-set_content').fadeOut();
            }
            else {
                $this.find('i').removeClass('fa-chevron-down')
                    .addClass('fa-chevron-up');

                $('.js-set_content').fadeOut();

                $this.addClass('open');
                $this.siblings('.js-set_content').fadeIn();
            }
        })
    };

    var initCartClose = function()
    {
        $(document).click(function(event) {
            if($(event.target).hasClass('content'))
                if(!$(event.target).closest(".js-set_popup-cart").length && $('.js-set_popup-cart').hasClass('open')) {
                    $('.js-set_popup-cart')
                        .removeClass('open')
                        .find('i').removeClass('fa-chevron-up')
                        .addClass('fa-chevron-down');

                    $('.js-set_content').fadeOut();
                }
        });
    };

    var sendEvent = function(step) {

        // Ecommerce event for GTM. The product checkout data
        try {
            window.dataLayer = window.dataLayer || [];
            dataLayer.push({
                'ecommerce': {
                    'checkout': {
                        'actionField': {'step': step},
                        'products': CartVariables.products
                    }
                },
                    'event': 'gtm-ee-event',
                        'gtm-ee-event-category': 'Enhanced Ecommerce',
                        'gtm-ee-event-action': 'Checkout Step '+step,
                        'gtm-ee-event-non-interaction': 'False'
                });
        } catch(e){}
    };

    var initMagnificPopup = function()
    {
        var showing = false;
        $('.img_link').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-img-mobile',
            image: {
                verticalFit: true
            },
            zoom: {
                enabled: true,
                duration: 200
            },
            gallery: {
                enabled: true
            },
            callbacks: {
                open: function() {
                    if ($('.js-cart_form_modal').css('display') == 'block') {
                        showing = true;
                        $('.js-cart_form_modal').modal('hide');
                    }
                },
                close: function() {
                    if(showing == true) {
                        $('.js-cart_form_modal').modal('show');
                        showing = false;
                    }
                }
            }
        });
    };

    var initDetailPopup = function(){
        $('.js-click_picture_in_cart').on('click', function(e) {
            var productId = $(this).data('id');

            e.preventDefault();
            $.request('onProductDetail', {
                data: {
                    product_id: productId
                },
                success: function(data){
                    this.success(data);
                    $('.js-detail_product_modal').modal('show');
                }
            });
        });
    };

    var initCartRecalculation = function() {
        $(function() {
            $('body').on('change', 'select[name=delivery_method], input[name=use_bonus], input[name=assembly], select[name=promo_code]',function () {
                $(this).tab('show');
                var delivery_method = $('select[name=delivery_method]').val();
                var scrollPoss = $('.newcart').scrollTop();

                var promo_code = 'default';
                var use_bonus = 0;

                var promoCodeInput = document.querySelector('select[name=promo_code]');
                var bonusesInput = document.querySelector('input[name=use_bonus]');

                if (promoCodeInput) {
                    promo_code = promoCodeInput.value;
                }

                if (bonusesInput) {
                    use_bonus = bonusesInput.value;
                }

                var assembly = $('input[name=assembly]:checked').is(":checked");
                var listId = $(this).data('request-data');

                $('input[name=assembling]').val(assembly);

                if (use_bonus == 0 && promo_code === 'default') {
                    promoCodeInput.style.display = 'block'
                    bonusesInput.style.display = 'block'
                }

                if (use_bonus > 0) {
                    promoCodeInput.style.display = 'none'
                }

                if (promo_code !== 'default') {
                    bonusesInput.style.display = 'none'
                }

                $.request('onChangeCountCartItem', {
                    data: {
                        delivery_method: delivery_method,
                        use_bonus: use_bonus,
                        assembly: assembly ? assembly : '',
                        list_id: listId,
                        promo_code: promo_code
                    },
                    success: function(data){
                        this.success(data);
                        actionsRow();
                        initSetCartPopup();
                        initCartClose();
                        initMagnificPopup();
                        setScrollPosition(scrollPoss);
                    }
                });
            });
        });
    };

    var initMaxBonus = function() {
        $(function() {
            var input = $('input[name=use_bonus]');
            $('body').on('DOMSubtreeModified', '.js-cart_amount_info', function() {
                input.val(minmax());
            });
            input.keyup(function () {
                input.val(minmax());
            });
            function minmax()
            {
                var bonuses = parseInt(input.attr('data-bonuses'));
                var cost    = parseInt($('#dataAmount').text());
                var min     = parseInt(input.attr('min'));
                var value   = parseInt(input.val());
                var max     = parseInt(input.attr('max'));;

                if (cost < 15000) {
                    max = 0;
                } else {
                    if (bonuses < max) {
                        max = bonuses;
                    }
                }

                if(value < min || isNaN(value))
                    return ;
                else if(value > max)
                    return max;
                else return value;
            }
        });
    };

    var initBpmOnlineFields = function()
    {
        $('.form-control_name').val($('input[name=second_name]').val() + ' ' + $('input[name=first_name]').val());
        $('input[name=second_name], input[name=first_name], input[name=middle_name]').change(function(){
            $('.form-control_name').val($('input[name=second_name]').val() + ' ' + $('input[name=first_name]').val());
        });
        $('.form-control_phone').val($('input[name=phone]').val());
        $('input[name=phone]').change(function(){
            $('.form-control_phone').val($('input[name=phone]').val());
        });
        $('.form-control_email').val($('input[name=email]').val());
        $('input[name=email]').change(function(){
            $('.form-control_email').val($('input[name=email]').val());
        });
        $('.form-control_address').val($('input[name=city]').val() + ', ' + $('input[name=street]').val() + ', ' + $('input[name=house]').val() + ' ' + $('input[name=apartment]').val());
        $('input[name=city], input[name=street], input[name=house], input[name=apartment]').change(function(){
            $('.form-control_address').val($('input[name=city]').val() + ', ' + $('input[name=street]').val() + ', ' + $('input[name=house]').val() + ' ' + $('input[name=apartment]').val());
        });
    };

    var initDeliveryMethodTabs = function () {
        $('select[name=delivery_method]').on('change', function () {
            $("option:selected", this).tab('show')
        });

    }

    var initButtonValidation = function () {

        var phonePattern = /\d{11}/g;
        var step2Btn = $('#step2next');
        var step3Btn = $('#step3next');
        var step4Btn = $('#step4next');

        var activeButton = function (button) {
            button.removeClass('disabled');
        };
        var disableButton = function (button) {
            button.addClass('disabled');
        };

        // validation step 2
        step2Btn.on('click', function () {
            step2Validation() ? activeButton(step3Btn) : disableButton(step3Btn);
        });

        var step2Validation = function () {
            var name = $('input[name=first_name]').val();
            var phone = $('.newcartform input[name=phone]').val().replace(/[+() ]/g, '');
            var email = $('input[name=email]').val();

            return (name && phone && phone.match(phonePattern) && email) ? true : false;
        };

        $('input[name=first_name]').on('input', function () {
            step2Validation() ? activeButton(step3Btn) : disableButton(step3Btn);
        });
        $('input[name=phone]').on('change', function () {
            step2Validation() ? activeButton(step3Btn) : disableButton(step3Btn);
        });
        $('input[name=email]').on('input', function () {
            step2Validation() ? activeButton(step3Btn) : disableButton(step3Btn);
        });

        // validation step 3
        step3Btn.on('click', function () {
            $('[name=delivery_method]').trigger('change');
        });
        $('[name=delivery_method]').on('change', function () {

            disableButton(step4Btn);

            if ($('[name=delivery_method] option:selected').val() == 'option3') {

                $('[name=issue_point]').on('change', function () {
                    $('option:selected', this).val() == '- выберите -' ? disableButton(step4Btn) : activeButton(step4Btn);
                });
            }

            if ($('[name=delivery_method] option:selected').val() == 'option2') {
                /* var city = $('input[name=city]').val();
                city ? activeButton(step4Btn) : disableButton(step4Btn);

                $('input[name=city]').on('input', function () {
                    $(this).val() ? activeButton(step4Btn) : disableButton(step4Btn);
                });*/

                activeButton(step4Btn);
            }

            if ($('[name=delivery_method] option:selected').val() == 'option1') {
                activeButton(step4Btn);
            }
        });

        // validation step 4
        $('input[name="is_accept"]:last').on('change', function() {
            if ($(this).is(':checked')) {
                $('#paystep').removeClass('disabled');
                $('#credit').removeClass('disabled');
            } else {
                $('#paystep').addClass('disabled');
                $('#credit').addClass('disabled');
            }
        });
    };

    return {
        init: function () {
            //initMagnificPopup();
            initCartFormButton();
            initCartSupportFormButton();
            initCartButtonPopup();
            initEmptyCartFormButton();
            actionsRow();
            initChangeCount();
            actionPhoneMask();
            initDetailPopup();
            initCartRecalculation();
            initMaxBonus();
            initBpmOnlineFields();
            initDeliveryMethodTabs();
            initButtonValidation();
            initDeleteCartItem();

            if(CartVariables.productionMode) {
                initItemRemoveButton();
            }
        },
        initAddMinus:function(){
            actionsRow();
            initSetCartPopup();
            initCartClose();
            initMagnificPopup();
        },
        initSupport:function(){
            initCartSupportFormButton();
        },
        initEmptyCart:function(){
            initEmptyCartFormButton();
        },
        initMagnific:function(){
            initMagnificPopup();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    Cart.init();
});
