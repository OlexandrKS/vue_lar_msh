<?php namespace LuckyWeb\MS\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use LuckyWeb\MS\Models\AdminSettings;
use LuckyWeb\MS\Models\Product;
use October\Rain\Exception\ValidationException;
use System\Classes\CombineAssets;

class LendingOrder extends ComponentBase
{
    /**
     * Path of theme-level assets
     * @var string $themeAssetsPath
     */
    public $themeAssetsPath = '/themes/shara/assets';

    public function componentDetails()
    {
        return [
            'name'        => 'LendingOrder Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->addJs(CombineAssets::combine([
            $this->themeAssetsPath.'/js/plugins/inputmask/jquery.inputmask.min.js',
            '/plugins/luckyweb/ms/components/lendingorder/assets/js/lending_order.js'
        ], base_path()));
    }

    /**
     * Render lending order form
     */
    public function onShowOrderForm()
    {
        if(post('product_id') || post('product_name')) {

            $product = post('product_id') ? Product::find(post('product_id')) : null;
            return [
                '.js-order_form_modal .modal-content' => $this->renderPartial(
                    'lendingorder::order_form', [
                        'product_name' => post('product_name'),
                        'product' => $product,
                        'production_mode' => strtolower(env('APP_ENV')) == 'production']
                )
            ];
        }
    }

    /**
     * Submit lending order
     * @return array
     * @throws ValidationException
     */
    public function onOrderSubmit()
    {
        $rules = [
            'first_name' => 'required',
            'email' => 'email',
            'phone' => ['required', 'regex:/^7[0-9]{10}$/'],
            'is_accept' => 'accepted'
        ];
        $customMessages = [
            'required' => 'Обязательное поле',
            'email' => 'Неверный формат email',
            'phone.regex' => 'Неверный формат номера телефона',
            'accepted' => 'Для формирования заказа необходимо подтвердить согласие на обработку персональных данных',
        ];
        $post = post();
        $post['phone'] = preg_replace('/[^0-9]+/', '', $post['phone']);
        $validator = Validator::make($post, $rules, $customMessages);
        $product = null;
        if(!empty(array_get($post,'product_id'))) {
            Product::whereIn('id', explode(',', $post['product_id']))->get()
                ->each(function($product) use(&$post) {
                $post['products'][] = [
                    'name' => $product->name . ' ' . $product->subname,
                    'url' => url('/catalog/'.$product->category->slug.'/'.$product->slug)
                ];
            });
        }

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $receiver = AdminSettings::instance()->lending_order_manager_mail;
        if (!empty($receiver)) {
            Mail::queue('luckyweb.ms::mail.manager.lending_order_created', ['data' => $post], function($message) use($receiver, $post) {
                $message->to($receiver);
                $message->subject('Оформлен заказ на '. array_get($post, 'products.0.name'));
            });
        }

        if(array_get($post,'type', 1) == 2) {
            return [
                '.js-order_form_inline' => $this->renderPartial('lendingorder::success_message_inline.htm', [
                    'production_mode' => strtolower(env('APP_ENV')) == 'production',
                    'product' => $product
                ]),
            ];
        }
        else {
            return [
                '.js-order_form_modal .modal-content' => $this->renderPartial('lendingorder::success_message.htm', [
                    'production_mode' => strtolower(env('APP_ENV')) == 'production',
                    'product' => $product
                ]),
            ];
        }
    }
}
