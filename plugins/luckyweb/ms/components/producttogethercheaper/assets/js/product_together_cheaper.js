var ProductTogetherCheaper = function () {

    var initSelectOfferButton = function(){
        $('.js-offer').on('click', '.js-select_offer', function(e){
            e.preventDefault();
            var $this = $(this);
            var productId = $this.data('id');
            var categoryId = $this.data('category-id');

            if(ProductTogetherCheaperVariables.productionMode) {
                sendPromotionClickEvent();
            }

            $.request('onShowOffersList', {
                data: {
                    product_id: productId,
                    category_id: categoryId
                },
                success: function(data){
                    this.success(data);
                    $('.js-together_select_modal').modal('show');

                    $('.js-offer').removeClass('open');
                    $this.parents('.js-offer').addClass('open');
                }
            });
        })
    };

    var sendPromotionClickEvent = function(){
        try {
            // Ecommerce event for GTM. Promotion Clicks
            window.dataLayer = window.dataLayer || [];
            dataLayer.push({
                'ecommerce': {
                    'promoClick': {
                        'promotions': [
                            {
                                'name': 'Together Cheaper: Select Button'
                            }
                        ]
                    }
                },
                'event': 'gtm-ee-event',
                'gtm-ee-event-category': 'Enhanced Ecommerce',
                'gtm-ee-event-action': 'Promotion Clicks',
                'gtm-ee-event-non-interaction': 'False'
            });
        } catch(e){}
    };

    var initOfferSelectedButton = function(){
        $('.js-together_select_modal').on('click', '.js-offer_selected', function(e){
            e.preventDefault();
            var $this = $(this);
            var productId = $this.data('id');
            var supportId = $this.data('support-id');

            $.request('onOfferSelected', {
                data: {
                    product_id: productId,
                    support_id: supportId
                },
                success: function(data){
                    this.success(data);
                    $('.js-offer.open').html(data['item']);
                    $('.js-together_select_modal').modal('hide');
                    Cart.initSupport(); /* for add products cart popup form after change*/
                }
            });
        })
    };

    var initTogetherCheaperTabShow = function(){
        $('a[href="#together_cheaper"]').on('shown.bs.tab', function (e) {
            try {
                // Ecommerce event for GTM. Promotion Impressions
                window.dataLayer = window.dataLayer || [];
                dataLayer.push({
                    'ecommerce': {
                        'promoView': {
                            'promotions': [
                                {
                                    'name': 'Together Cheaper'
                                }
                            ]
                        }
                    },
                    'event': 'gtm-ee-event',
                    'gtm-ee-event-category': 'Enhanced Ecommerce',
                    'gtm-ee-event-action': 'Promotion Clicks',
                    'gtm-ee-event-non-interaction': 'False'
                });
            } catch(e){}
        })
    };

    var initLazy = function() {
        $('.js-nav_tabs a[href=#together_cheaper]').on('show.bs.tab', function (e) {
            $($(this).attr('href')+' .js-lazy_tab').lazy();
        });
    };

    return {
        init: function () {
            initLazy();
            initSelectOfferButton();
            initOfferSelectedButton();

            if(typeof ProductTogetherCheaperVariables != 'undefined'
                && ProductTogetherCheaperVariables.productionMode) {
                initTogetherCheaperTabShow();
            }
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    ProductTogetherCheaper.init();
});
