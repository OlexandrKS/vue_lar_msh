var ProductReserve = function () {
    var initFormSubmit = function(){
        $('.js-reserve_modal').on('submit', 'form', function(e){
            e.preventDefault();
            var $form = $(this);

            $form.find('button[type=submit]').prop('disabled', true);
            $form.request('onReserve', {
                error: function(jqXHR, status, obj) {
                    displayFieldErrors(jqXHR);
                    return false;
                },
                complete: function() {
                    $form.find('button[type=submit]').prop('disabled', false);
                }
            });
        });
    };

    var initCitySelect = function(){
        $('.js-reserve_modal').on('change', '.js-city', function(){
            $.request('onCitySelected', {
                data: {
                    city_id: $(this).val()
                }
            })
        });
    };

    var displayFieldErrors = function(jqXHR) {
        var $form = $('.js-reserve_modal form');
        $form.find('.form-group.has-error').each(function () {
            $(this).removeClass('has-error').find('.error').hide();
        });

        if (typeof jqXHR.responseJSON != 'undefined') {
            $.each(jqXHR.responseJSON.X_OCTOBER_ERROR_FIELDS, function (field, message) {
                var $group = $form.find("[name='"+field+"']").parents('.form-group').addClass('has-error');
                var $error = $group.find(".error");
                if ($error.length) {
                    $error.eq(0).text(message).show();
                }
                else {
                    $group.append('<div class="error">'+message+'</div>');
                }
            });
        }
    };

    return {
        init: function () {
            initFormSubmit();
            initCitySelect();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    ProductReserve.init();
});
