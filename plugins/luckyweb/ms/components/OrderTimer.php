<?php namespace Luckyweb\Ms\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Request;
use LuckyWeb\MS\Classes\PriceCalculator\Calculators\BuyTodayDiscount;
use System\Classes\CombineAssets;

class OrderTimer extends ComponentBase
{
    public $path;
    public $isShow;
    public function componentDetails()
    {
        return [
            'name'        => 'OrderTimer Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    function onRun()
    {
        $this->path = Request::path();

        $cookieValue = json_decode(request()->cookie(BuyTodayDiscount::COOKIE_NAME), true);
        $this->isShow = array_get($cookieValue, 'used', true) === false &&
            Carbon::now()->lessThan(Carbon::createFromTimestamp(array_get($cookieValue, 'expired', 1)));

        $this->page['discount'] = session(BuyTodayDiscount::SESSION_KEY, 0);

    }

    public function init()
    {
        $this->addJs(CombineAssets::combine([
            '/plugins/luckyweb/ms/components/ordertimer/assets/js/timer.js',
        ], base_path()), ['async']);
    }
}
