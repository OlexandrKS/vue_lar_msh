var Documents = function () {
    var $ = jQuery;

    var initPopup = function() {
        $('.js-document').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-img-mobile',
            image: {
                verticalFit: true
            },
            gallery: {
                enabled: true
            }
        });
    };

    return {
        init: function () {
            initPopup();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    Documents.init();
});