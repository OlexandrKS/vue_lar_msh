var ProductViewPhone = function () {

    var initViewPhoneButton = function(){
        $('.js-view_phone_button').on('click', function (e) {
            e.preventDefault();
            closeHolders();

            var $this = $(this);
            if($this.hasClass('active')) {
                $('.js-additional_block .js-view_phone_holder').slideUp();
                $this.removeClass('active');
            }
            else {
                $('.js-additional_block .js-view_phone_holder').slideDown();
                $this.addClass('active');
            }
        });
    };

    var closeHolders = function(){
        $('.js-additional_block .js-product_reviews_holder').hide();
        //$('.js-additional_block .js-view_phone_holder').hide();

        $('.js-product_reviews_button').removeClass('active');
        //$('.js-view_phone_button').removeClass('active');
    };

    var initCitySelect = function(){
        $('.js-view_phone_holder .js-city').on('change', function(){
            $('.js-view_phone_holder form').find('.form-group.has-error').each(function () {
                $(this).removeClass('has-error').find('.error').hide();
            });

            $.request('onCitySelected', {
                data: {
                    city_id: $(this).val()
                }
            });
        });
    };

    var initShopSelect = function() {
        $('.js-view_phone_holder .js-shop').on('change', function(){
            $('.js-view_phone_holder form').find('.form-group.has-error').each(function () {
                $(this).removeClass('has-error').find('.error').hide();
            });

            $(this).request('onViewPhone', {
                error: function(jqXHR, status, obj) {
                    displayFieldErrors(jqXHR);
                    return false;
                }
            });
        });
    };

    var displayFieldErrors = function(jqXHR) {
        var $form = $('.js-view_phone_holder form');
        $form.find('.form-group.has-error').each(function () {
            $(this).removeClass('has-error').find('.error').hide();
        });

        if (typeof jqXHR.responseJSON != 'undefined') {
            $.each(jqXHR.responseJSON.X_OCTOBER_ERROR_FIELDS, function (field, message) {
                var $group = $form.find("[name='"+field+"']").parents('.form-group').addClass('has-error');
                var $error = $group.find(".error");
                if ($error.length) {
                    $error.eq(0).text(message).show();
                }
                else {
                    $group.append('<div class="error">'+message+'</div>');
                }
            });
        }
    };

    return {
        init: function () {
            initViewPhoneButton();
            initShopSelect();
            initCitySelect();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    ProductViewPhone.init();
});
