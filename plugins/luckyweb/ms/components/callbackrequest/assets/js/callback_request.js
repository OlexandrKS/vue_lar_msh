var CallbackRequest = function () {

    var initCallbackFormButton = function(){
        $('.js-callback_form_button').on('click', function(e){
            e.preventDefault();
            var productId = $(this).data('id');
            var productName = $(this).data('name');
            var type = $(this).data('type');

            $.request('onShowCallbackForm', {
                data: {
                    product_id: productId,
                    product_name: productName,
                    type: type
                },
                success: function(data){
                    this.success(data);
                    $('.js-phone').inputmask('+7(999) 999-99-99');
                    $('.js-callback_form_modal').modal('show');
                }
            });
        })
    };

    var initCallbackFormSubmit = function(){
        $('.js-callback_form_modal').on('submit', 'form', function(e){
            e.preventDefault();

            var $form = $(this);

            $form.find('button[type=submit]').prop('disabled', true);
            $form.request('onCallbackSubmit', {
                error: function(jqXHR, status, obj) {
                    displayFieldErrors($form, jqXHR);
                    return false;
                },
                complete: function() {
                    $form.find('button[type=submit]').prop('disabled', false);
                }
            });
        });
    };

    var displayFieldErrors = function($form, jqXHR) {
        $form.find('.form-group.has-error').each(function () {
            $(this).removeClass('has-error').find('.error').hide();
        });

        if (typeof jqXHR.responseJSON != 'undefined') {
            $.each(jqXHR.responseJSON.X_OCTOBER_ERROR_FIELDS, function (field, message) {
                var $group = $form.find("[name='"+field+"']").parents('.form-group').addClass('has-error');
                var $error = $group.find(".error");
                if ($error.length) {
                    $error.eq(0).text(message).show();
                }
                else {
                    $group.append('<div class="error">'+message+'</div>');
                }
            });
        }
    };

    return {
        init: function () {
            $('.js-phone').inputmask('+7(999) 999-99-99');
            initCallbackFormSubmit();
            initCallbackFormButton();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    CallbackRequest.init();
});
