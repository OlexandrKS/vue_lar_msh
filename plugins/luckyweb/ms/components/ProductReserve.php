<?php namespace LuckyWeb\MS\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Validator;
use LuckyWeb\MS\Models\Shop;
use LuckyWeb\MS\Models\ShopCity;
use October\Rain\Exception\ValidationException;
use System\Classes\CombineAssets;

class ProductReserve extends ComponentBase
{
    /**
     * Related product slug
     * @var mixed
     */
    public $product;

    public function componentDetails()
    {
        return [
            'name'        => 'ProductReserve Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->addJs(CombineAssets::combine([
            '/plugins/luckyweb/ms/components/productreserve/assets/js/product_reserve.js'
        ], base_path()));
    }

    public function onRender()
    {
        $this->product = $this->property('product');
    }

    /**
     * Return cities list
     * @return mixed
     */
    public function getCities()
    {
        return ShopCity::orderBy('name', 'ASC')->get();
    }

    /**
     * Return shops list
     * @param $cityId
     * @return mixed
     */
    public function getShops($cityId)
    {
        if(!empty($cityId)) {
            return Shop::where('city_id', $cityId)
                ->orderBy('mall_name', 'ASC')->get();
        }
        else {
            return Shop::orderBy('mall_name', 'ASC')->get();
        }
    }

    public function onReserve()
    {
        $rules = [
            'name' => 'required',
            'product_id' => 'required',
            'phone' => ['required', 'regex:/^7[0-9]{10}$/'],
            'city_id' => 'required',
            'shop_id' => 'required'
        ];
        $customMessages = [
            'required' => 'Обязательное поле',
            'phone.regex' => 'Неверный формат номера телефона',
        ];
        $post = post();
        $post['phone'] = preg_replace('/[^0-9]+/', '', $post['phone']);

        $validator = Validator::make($post, $rules, $customMessages);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $reserve = new \LuckyWeb\MS\Models\ProductReserve($post);
        $reserve->save();

        Event::fire('luckyweb.ms.reserveCreated', $reserve);

        return [
            '.js-reserve_modal .modal-content' => $this->renderPartial('productreserve::success_message')
        ];
    }

    public function onCitySelected()
    {
        return [
            '.js-shop_select' => $this->renderPartial('productreserve::shop_select', ['cityId' => post('city_id')])
        ];
    }

    public function onModalShow()
    {
        return [
            '.js-reserve_modal .modal-content' => $this->renderPartial('productreserve::form')
        ];
    }
}
