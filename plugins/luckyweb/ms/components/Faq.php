<?php namespace LuckyWeb\MS\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Validator;
use October\Rain\Exception\ValidationException;
use LuckyWeb\MS\Models\Faq as FaqModel;
use System\Classes\CombineAssets;

class Faq extends ComponentBase
{
    /**
     * Path of theme-level assets
     * @var string $themeAssetsPath
     */
    public $themeAssetsPath = '/themes/shara/assets';

    /**
     * Faqs list
     * @var Collection
     */
    public $faqs;

    public function componentDetails()
    {
        return [
            'name'        => 'Faq',
            'description' => 'Display FAQ block'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->addJs(CombineAssets::combine([
            $this->themeAssetsPath.'/js/plugins/inputmask/jquery.inputmask.min.js',
            '/plugins/luckyweb/ms/components/faq/assets/js/faq.js'
        ], base_path()));
        
        $this->faqs = FaqModel::enabled()->get();
    }
    
    public function onQuestionAdd()
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'city' => 'required',
            'phone' => ['regex:/^7[0-9]{10}$/'],
            'text' => 'required',
            'is_accept' => 'accepted'
        ];
        $customMessages = [
            'required' => 'Обязательное поле',
            'email' => 'Неверный формат email',
            'phone.regex' => 'Неверный формат номера телефона',
            'accepted' => 'Вы должны подтверждить согласие на обработку Ваших персональных данных.',
        ];
        $post = post();
        $post['phone'] = preg_replace('/[^0-9]+/', '', $post['phone']);
        $validator = Validator::make($post, $rules, $customMessages);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        Event::fire('luckyweb.ms.questionCreated', json_encode($post));
        if(isset($post['is_duplicate']) && $post['is_duplicate']) {
            Event::fire('luckyweb.ms.questionCreatedForClient', json_encode($post));
        }

        return [
            '.js-question_form' => $this->renderPartial('faq::success_message.htm'),
        ];
    }
}