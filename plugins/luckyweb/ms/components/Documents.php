<?php namespace LuckyWeb\MS\Components;

use Cms\Classes\ComponentBase;
use LuckyWeb\MS\Models\Document;
use System\Classes\CombineAssets;

class Documents extends ComponentBase
{
    /**
     * Documents list
     * @var mixed
     */
    public $documents;

    public function componentDetails()
    {
        return [
            'name'        => 'Documents Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'code' => [
                'title'             => 'Block code',
                'description'       => 'Specify documents block',
                'default'           => '',
                'type'              => 'string'
            ],
        ];
    }

    public function onRender()
    {
        $this->documents = Document::where('code', $this->property('code'))->get();

        if($this->documents->count() > 0) {

            $this->addJs(CombineAssets::combine([
                '/plugins/luckyweb/ms/components/documents/assets/js/documents.js'
            ], base_path()));
        }
    }
}
