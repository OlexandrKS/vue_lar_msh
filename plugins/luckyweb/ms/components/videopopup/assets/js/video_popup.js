var VideoPopup = function () {

    var initCloseHandler = function(){
        $('.js-popup_video_modal').on('hide.bs.modal', function (e) {
            $(this).find('.modal-content').empty();
        });
    };

    var initVideoButton = function(){
        $('.js-popup_video_button').on('click', function(e){
            e.preventDefault();

            $.request('onVideoPopup', {
                data: {
                    title: $(this).data('title'),
                    url: $(this).data('url')
                },
                complete: function(){
                    $('.js-popup_video_modal').modal('show');
                }
            })
        })
    };

    return {
        init: function () {
            initVideoButton();
            initCloseHandler();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    VideoPopup.init();
});
