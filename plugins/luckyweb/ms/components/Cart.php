<?php

namespace Luckyweb\Ms\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Illuminate\Contracts\Cookie\QueueingFactory;
use LuckyWeb\MS\Classes\PriceCalculator\Calculators\BuyTodayDiscount;
use LuckyWeb\MS\Classes\PriceCalculator\Factory;
use LuckyWeb\MS\Classes\PriceCalculator\OrderPrice;
use LuckyWeb\User\Facades\Auth;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\FavoriteList;
use LuckyWeb\MS\Models\FavoriteListItem;
use LuckyWeb\User\Models\PromoCodeEvent;
use October\Rain\Database\Collection;
use Illuminate\Support\Facades\Cookie;
use System\Classes\CombineAssets;

class Cart extends ComponentBase
{
    const CART_TIME_LIFE = 60 * 24 * 30;
    const COOKIE_NAME = 'productList'; //'cartList';

    /**
     * Path of theme-level assets
     * @var string $themeAssetsPath
     */
    public $themeAssetsPath = '/themes/shara/assets';

    /**
     * Info panel data
     * @var array
     */
    public $info;

    public function componentDetails()
    {
        return [
            'name' => 'Cart Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function init()
    {
        $this->addJs($this->themeAssetsPath . '/js/plugins/jquery.maskedinput/jquery.maskedinput.min.js');
        $this->addJs(CombineAssets::combine([
            // $this->themeAssetsPath.'/js/plugins/jquery.maskedinput/jquery.maskedinput.min.js',
            // $this->themeAssetsPath.'/js/plugins/inputmask/jquery.inputmask.min.js',
            $this->themeAssetsPath . '/vendor/bpmonline/create-object.js',
            $this->themeAssetsPath . '/vendor/bpmonline/track-cookies.js',
            $this->themeAssetsPath . '/js/classes/bpmonline.js',
            '/plugins/luckyweb/ms/components/cart/assets/js/cart.js'
        ], base_path()), ['async']);

        if (Auth::check()) {
            $this->info['deactivatedThisMonth'] = $this->deactivatedThisMonth();
            $this->info['allDeactivated'] = $this->allDeactivated();

            //TODO
            $default_list_id = Auth::getUser() ? FavoriteList::getDefaultListId() : null;
            $temp = Cookie::get(self::COOKIE_NAME, []);
            $temp = collect($temp);

            $cnt = FavoriteListItem::getProductCountsFromProduct($temp, $default_list_id);
            $arr = FavoriteListItem::getProductCountsFromFavList($temp, $default_list_id, true, ['product_id']);
            $supp = FavoriteListItem::getSupportsArray($temp, $default_list_id);

            /** @var OrderPrice $orderPrice */
            $orderPrice = (new OrderPrice())->calculate($this->transformDataForPriceCalculator($arr, $cnt, $supp));
            $this->info['promoCodesArray'] = PromoCodeEvent::available(Auth::getUser(), $orderPrice->getWithoutSpecialsPrice())
                ->get();
        }
    }

    /**
     * Sorry, it's temp copy-paste onAddProduct
     *
     * @return array
     * @todo rewrite!!!
     */
    public function onShowCart()
    {
        $default_list_id = Auth::getUser() ? FavoriteList::getDefaultListId() : null;
        $temp = Cookie::get(self::COOKIE_NAME, []);
        $temp = collect($temp);

        $cnt = FavoriteListItem::getProductCountsFromProduct($temp, $default_list_id);
        $arr = FavoriteListItem::getProductCountsFromFavList($temp, $default_list_id, true, ['product_id']);
        $supp = FavoriteListItem::getSupportsArray($temp, $default_list_id);

        /** @var OrderPrice $orderPrice */
        $orderPrice = (new OrderPrice())->calculate($this->transformDataForPriceCalculator($arr, $cnt, $supp));

        return [
            '.js-cart_form_modal .modal-content' => $this->renderPartial(
                'cart::cart_form.htm', [
                    'cart_products' => $arr,
                    'cart_counts' => $cnt,
                    'cart_supports' => $supp,
                    'addedProducts' => [],
                    'production_mode' => strtolower(env('APP_ENV')) == 'production',
                    'count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser()),
                    'orderPrice' => $orderPrice,
                    'discount' => $orderPrice->getBuyTodayDiscount(),
                    //'more_products' => $products,
                ]
            ),
            '.js-cart-header' => $this->renderPartial(
                'cart::cart_popup.htm', [
                    'cart_products' => $arr,
                    'cart_counts' => $cnt,
                    'cart_supports' => $supp,
                    'orderPrice' => $orderPrice,
                    'discount' => $orderPrice->getBuyTodayDiscount(),
                ]
            ),
            '.js-count_cart' => $this->renderPartial(
                'cart::header_count_cart_item.htm', [
                    'cart_count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser())
                ]
            ),
            '.js-count_product_list' => $this->renderPartial(
                'productchooser::header_count_list_item.htm', [
                    'count' => FavoriteListItem::getCountFavList($temp, Auth::getUser())
                ]
            ),
            '.js-count_product_list_mob' => $this->renderPartial(
                'productchooser::mob_header_count_list_item.htm', [
                    'user' => Auth::getUser(),
                    'count' => FavoriteListItem::getCountFavList($temp, Auth::getUser()),
                ]
            ),
            '.js-count_cart_mob' => $this->renderPartial(
                'cart::mob_header_count_cart_item.htm', [
                    'cart_count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser())
                ]
            ),
        ];
    }

    /**
     * Render add
     */
    public function onAddProduct()
    {
        $this->buyTodayCookie();

        $default_list_id = Auth::getUser() ? FavoriteList::getDefaultListId() : null;
        $temp = Cookie::get(self::COOKIE_NAME, []);
        $temp = collect($temp);
        FavoriteListItem::addProductToFavList($temp, post('product_id'), $default_list_id, 1, post('support_id'));
        if (post('support_id'))
            FavoriteListItem::addProductToFavList($temp, post('support_id'), $default_list_id, 1);
        $cnt = FavoriteListItem::getProductCountsFromProduct($temp, $default_list_id);
        $arr = FavoriteListItem::getProductCountsFromFavList($temp, $default_list_id, true, ['product_id']);
        $supp = FavoriteListItem::getSupportsArray($temp, $default_list_id);
        $addedProducts = [];
        if (!empty(post('product_id'))) {
            $addedProducts[] = Product::where('id', post('product_id'))->first();
        }
        if (!empty(post('support_id'))) {
            $addedProducts[] = Product::where('id', post('support_id'))->first();
        }
        //$products = Product::where('description1_name', $product->description1_name)->limit(4)->get();

        /** @var OrderPrice $orderPrice */
        $orderPrice = (new OrderPrice())->calculate($this->transformDataForPriceCalculator($arr, $cnt, $supp));

        session([BuyTodayDiscount::SESSION_KEY => $orderPrice->getBuyTodayDiscount()]);

        return [
            '.js-cart_form_modal .modal-content' => $this->renderPartial(
                'cart::cart_form.htm', [
                    'cart_products' => $arr,
                    'cart_counts' => $cnt,
                    'cart_supports' => $supp,
                    'addedProducts' => $addedProducts,
                    'production_mode' => strtolower(env('APP_ENV')) == 'production',
                    'count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser()),
                    'orderPrice' => $orderPrice,
                    'discount' => $orderPrice->getBuyTodayDiscount(),
                    //'more_products' => $products,
                ]
            ),
            '.js-cart-header' => $this->renderPartial(
                'cart::cart_popup.htm', [
                    'cart_products' => $arr,
                    'cart_counts' => $cnt,
                    'cart_supports' => $supp,
                    'orderPrice' => $orderPrice,
                    'discount' => $orderPrice->getBuyTodayDiscount(),
                ]
            ),
            '.js-count_cart' => $this->renderPartial(
                'cart::header_count_cart_item.htm', [
                    'cart_count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser())
                ]
            ),
            '.js-count_product_list' => $this->renderPartial(
                'productchooser::header_count_list_item.htm', [
                    'count' => FavoriteListItem::getCountFavList($temp, Auth::getUser())
                ]
            ),
            '.js-count_product_list_mob' => $this->renderPartial(
                'productchooser::mob_header_count_list_item.htm', [
                    'user' => Auth::getUser(),
                    'count' => FavoriteListItem::getCountFavList($temp, Auth::getUser()),
                ]
            ),
            '.js-count_cart_mob' => $this->renderPartial(
                'cart::mob_header_count_cart_item.htm', [
                    'cart_count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser())
                ]
            ),
        ];
    }

    public function onDeleteCartItem()
    {
        $user = Auth::getUser();
        $list_id = FavoriteList::getDefaultList();
        $temp = Cookie::get(self::COOKIE_NAME, []);
        $temp = collect($temp);
        FavoriteListItem::removeProductFromFavList($temp, post('product_id'), $list_id);

        $arr = FavoriteListItem::getProductCountsFromFavList($temp, $list_id, true, ['product_id']);
        $cnt = FavoriteListItem::getProductCountsFromProduct($temp, $list_id);
        $supp = FavoriteListItem::getSupportsArray($temp, $list_id);
        $delivery_method = post('delivery_method');
        $use_bonus = post('use_bonus');
        $assembly = post('assembly');

        /** @var OrderPrice $orderPrice */
        $orderPrice = (new OrderPrice())->calculate($this->transformDataForPriceCalculator($arr, $cnt, $supp));

        session([BuyTodayDiscount::SESSION_KEY => $orderPrice->getBuyTodayDiscount()]);

        return [
            '.js-cart-product' => $this->renderPartial(
                'cart::cart_list.htm', [
                    'cart_products' => $arr,
                    'cart_counts' => $cnt,
                    'cart_supports' => $supp,
                    'param' => post('cart_id'),
                ]
            ),
            '.js-cart-header' => $this->renderPartial(
                'cart::cart_popup.htm', [
                    'cart_products' => $arr,
                    'cart_counts' => $cnt,
                    'cart_supports' => $supp,
                    'orderPrice' => $orderPrice,
                    'discount' => $orderPrice->getBuyTodayDiscount(),
                ]
            ),
            '.js-count_cart' => $this->renderPartial(
                'cart::header_count_cart_item.htm', [
                    'cart_count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser())
                ]
            ),
            '.js-count_product_list' => $this->renderPartial(
                'productchooser::header_count_list_item.htm', [
                    'user' => $user,
                    'count' => FavoriteListItem::getCountFavList($temp, $user)
                ]
            ),
            '.js-cart-page_fav_list' => $this->renderPartial(
                'cart::fav_lists.htm', [
                    'cart_products' => $arr,
                    'cart_counts' => $cnt,
                    'list' => FavoriteList::getLists(),
                    'param' => post('cart_id'),
                    'user' => $user,
                    'count' => FavoriteListItem::getCountFavList($temp, $user)
                ]
            ),
            '.js-cart-fav_list' => $this->renderPartial(
                'cart::cart_fav.htm', [
                    'list' => FavoriteList::getLists(),
                    'param' => post('cart_id'),
                ]
            ),
            '.js-count_product_list_mob' => $this->renderPartial(
                'productchooser::mob_header_count_list_item.htm', [
                    'user' => $user,
                    'count' => FavoriteListItem::getCountFavList($temp, $user),
                ]
            ),
            '.js-count_cart_mob' => $this->renderPartial(
                'cart::mob_header_count_cart_item.htm', [
                    'cart_count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser())
                ]
            ),
            '.js-cart_amount_info' => $this->renderPartial(
                'cart::cart_amount_info.htm', [
                    'cart_products' => $arr,
                    'cart_counts' => $cnt,
                    'cart_supports' => $supp,
                    'delivery_method' => $delivery_method,
                    'use_bonus' => $use_bonus,
                    'assembly' => $assembly,
                    'orderPrice' => $orderPrice,
                ]
            ),
        ];
    }

    public function onDeleteCartItemFromForm()
    {
        $user = Auth::getUser();
        $list_id = FavoriteList::getDefaultList();
        $temp = Cookie::get(self::COOKIE_NAME, []);
        $temp = collect($temp);
        FavoriteListItem::removeProductFromFavList($temp, post('product_id'), $list_id);

        $cnt = FavoriteListItem::getProductCountsFromProduct($temp, $list_id);
        $arr = FavoriteListItem::getProductCountsFromFavList($temp, $list_id, true, ['product_id']);
        $supp = FavoriteListItem::getSupportsArray($temp, $list_id);

        /** @var OrderPrice $orderPrice */
        $orderPrice = (new OrderPrice())->calculate($this->transformDataForPriceCalculator($arr, $cnt, $supp));

        session([BuyTodayDiscount::SESSION_KEY => $orderPrice->getBuyTodayDiscount()]);

        return [
            '.js-cart-header' => $this->renderPartial(
                'cart::cart_popup.htm', [
                    'cart_products' => $arr,
                    'cart_counts' => $cnt,
                    'cart_supports' => $supp,
                    'discount' => $orderPrice->getBuyTodayDiscount(),
                ]
            ),
            '.js-count_cart' => $this->renderPartial(
                'cart::header_count_cart_item.htm', [
                    'cart_count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser()),
                    'cart_supports' => $supp,
                ]
            ),
            '.js-cart_form_modal .modal-content' => $this->renderPartial(
                'cart::cart_form.htm', [
                    'cart_products' => $arr,
                    'cart_counts' => $cnt,
                    'cart_supports' => $supp,
                    'count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser()),
                    'orderPrice' => $orderPrice,
                    'discount' => $orderPrice->getBuyTodayDiscount(),
                ]
            ),
            '.js-count_product_list' => $this->renderPartial(
                'productchooser::header_count_list_item.htm', [
                    'user' => $user,
                    'count' => FavoriteListItem::getCountFavList($temp, $user)
                ]
            ),
            '.js-count_product_list_mob' => $this->renderPartial(
                'productchooser::mob_header_count_list_item.htm', [
                    'user' => $user,
                    'count' => FavoriteListItem::getCountFavList($temp, $user),
                ]
            ),
            '.js-count_cart_mob' => $this->renderPartial(
                'cart::mob_header_count_cart_item.htm', [
                    'cart_count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser())
                ]
            ),
        ];
    }

    public function onChangeCountCartItem()
    {
        $list = FavoriteList::getDefaultList();
        $temp = Cookie::get(self::COOKIE_NAME, []);
        $temp = collect($temp);
        if (!empty(post('count')) && !empty(post('product_id')))
            FavoriteListItem::updateProductCount($temp, post('product_id'), post('count'), $list);

        $cnt = FavoriteListItem::getProductCountsFromProduct($temp, $list);
        $arr = FavoriteListItem::getProductCountsFromFavList($temp, $list, true, ['product_id']);
        $supp = FavoriteListItem::getSupportsArray($temp, $list);
        $delivery_method = post('delivery_method');
        $use_bonus = post('use_bonus');
        $assembly = post('assembly');

        /** @var OrderPrice $orderPrice */
        $orderPrice = (new OrderPrice())->calculate($this->transformDataForPriceCalculator($arr, $cnt, $supp));

        session([BuyTodayDiscount::SESSION_KEY => $orderPrice->getBuyTodayDiscount()]);

        $promo_code_amount = $orderPrice->getPromoCodeDiscount();
        /*if (Auth::check() && post('promo_code')) {
            $promo_code_event = PromoCodeEvent::where('code', '=', post('promo_code'))
                ->where('user_id', '=', Auth::getUser()->id)
                ->with(['promo_code'])->first();
            $now = Carbon::now()->startOfDay();
            if ($promo_code_event && $promo_code_event->promo_code->status
                && $promo_code_event->applied_at == null && $promo_code_event->promo_code->expired_at >= $now
                && $promo_code_event->promo_code->start_at <= $now) {
                $promo_code_amount = $promo_code_event->promo_code->amount;
            }
        }*/
        return [
            '.js-cart-header' => $this->renderPartial(
                'cart::cart_popup.htm', [
                    'cart_products' => $arr,
                    'cart_counts' => $cnt,
                    'orderPrice' => $orderPrice,
                    'discount' => $orderPrice->getBuyTodayDiscount(),
                ]
            ),
            '.js-cart-product' => $this->renderPartial(
                'cart::cart_list.htm', [
                    'cart_products' => $arr,
                    'cart_counts' => $cnt,
                    'param' => post('list_id'),
                    'cart_supports' => $supp,
                ]
            ),
            '.js-cart_form_modal .modal-content' => $this->renderPartial(
                'cart::cart_form.htm', [
                    'cart_products' => $arr,
                    'cart_counts' => $cnt,
                    'cart_supports' => $supp,
                    'count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser()),
                    'orderPrice' => $orderPrice,
                    'discount' => $orderPrice->getBuyTodayDiscount(),
                ]
            ),
            '.js-count_cart' => $this->renderPartial(
                'cart::header_count_cart_item.htm', [
                    'cart_count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser())
                ]
            ),
            '.js-count_product_list' => $this->renderPartial(
                'productchooser::header_count_list_item.htm', [
                    'user' => Auth::getUser(),
                    'count' => FavoriteListItem::getCountFavList($temp, Auth::getUser())
                ]
            ),
            '.js-count_product_list_mob' => $this->renderPartial(
                'productchooser::mob_header_count_list_item.htm', [
                    'user' => Auth::getUser(),
                    'count' => FavoriteListItem::getCountFavList($temp, Auth::getUser()),
                ]
            ),
            '.js-count_cart_mob' => $this->renderPartial(
                'cart::mob_header_count_cart_item.htm', [
                    'cart_count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser())
                ]
            ),
            '.js-cart-fav_list' => $this->renderPartial(
                'cart::cart_fav.htm', [
                    'cart_products' => $arr,
                    'cart_counts' => $cnt,
                    'list' => FavoriteList::getLists(),
                    'param' => post('list_id'),
                    'user' => Auth::getUser(),
                    'count' => FavoriteListItem::getCountFavList($temp, Auth::getUser())
                ]
            ),
            '.js-cart_amount_info' => $this->renderPartial(
                'cart::cart_amount_info.htm', [
                    'cart_products' => $arr,
                    'cart_counts' => $cnt,
                    'cart_supports' => $supp,
                    'delivery_method' => $delivery_method,
                    'use_bonus' => $use_bonus,
                    'assembly' => $assembly,
                    'param' => post('list_id'),
                    'orderPrice' => $orderPrice,
                    'promo_code_amount' => $promo_code_amount,
                ]
            ),
        ];
    }

    public function onEmptyList()
    {
        return [
            '.js-cart_form_modal .modal-content' => $this->renderPartial(
                'cart::success_message.htm', [
                ]
            ),
        ];
    }

    /**
     * Product detail modal
     * @return array
     */
    public function onProductDetail()
    {
        $product = Product::find(post('product_id'));
        //return post('product_id');
        if ($product) {
            return [
                '.js-detail_product_modal .modal-content' => $this->renderPartial(
                    'cart::detail',
                    [
                        'product' => $product,
                        'productionMode' => (env('APP_ENV') == 'production')
                    ]
                )
            ];
        }
    }

    /**
     * Return list of bonuses deactivated this month
     */
    protected function deactivatedThisMonth()
    {
        return collect(array_get(Auth::getUser()->extended, 'bonuses.active_list', []))
            ->map(function ($item) {
                $item['DateEnd'] = Carbon::parse($item['DateEnd']);
                return $item;
            })
            ->filter(function ($item) {
                return Carbon::now()->addMonth()->gte($item['DateEnd']);
            });
    }

    /**
     * Return list of all bonuses that should be deactivated
     */
    protected function allDeactivated()
    {
        return collect(array_get(Auth::getUser()->extended, 'bonuses.active_list', []))
            ->map(function ($item) {
                $item['DateEnd'] = Carbon::parse($item['DateEnd']);
                return $item;
            });
    }

    /**
     * @param Collection $products
     * @param Collection $count
     * @param Collection $supportProducts
     * @return mixed
     */
    private function transformDataForPriceCalculator($products, $count, $supportProducts)
    {
        return Factory::transformDataForPriceCalculator($products, $count, $supportProducts);
    }

    /**
     * Установка куки для скидки на первую покупку
     */
    private function buyTodayCookie()
    {
        if (!request()->hasCookie(BuyTodayDiscount::COOKIE_NAME)) {
            $expired = Carbon::now()->endOfDay()->timestamp;
            $used = false;

            $cookieValue = json_encode(compact('expired', 'used'));

            request()->cookies->set(BuyTodayDiscount::COOKIE_NAME, $cookieValue);
            // Создаем куку с временем жизни 11 дней: 1 день активна и 10 дней ждет своего часа.
            $cookie = cookie(BuyTodayDiscount::COOKIE_NAME, $cookieValue, 11 * 24 * 60, null, null, false, false);
            app(QueueingFactory::class)->queue($cookie);
        }
    }
}
