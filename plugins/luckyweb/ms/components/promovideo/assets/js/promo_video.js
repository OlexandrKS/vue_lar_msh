var PromoVideo = function () {

    var initCloseHandler = function(){
        $('.js-video_modal').on('hide.bs.modal', function (e) {
            $(this).find('.modal-content').empty();
        });
    };

    return {
        init: function () {
            initCloseHandler();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    PromoVideo.init();
});
