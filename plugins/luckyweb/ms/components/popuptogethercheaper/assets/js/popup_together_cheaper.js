var PopupTogetherCheaper = function () {
    var initPopup = function () {
        var popup = $('.js-together_item_modal');
        setTimeout(function () {
            popup.modal('show');
        }, popup.data('timeout') * 1000);
    };


    var initClickOfferModal = function () {
        $('.offers').on('click', '.js-offer-modal', function (e) {
            e.preventDefault();
            var tab_together_cheaper = $('a[href="#together_cheaper"]');
            $('.js-together_item_modal').modal('hide');
            tab_together_cheaper.tab('show');
            var top = tab_together_cheaper.offset().top;
            $('body,html').animate({scrollTop: top}, 800);
        });
    };

    return {
        init: function () {
            initPopup();
            initClickOfferModal();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    PopupTogetherCheaper.init();
});
