<?php namespace Luckyweb\MS\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Session;
use LuckyWeb\MS\Models\FavoriteListItem;
use LuckyWeb\MS\Models\FavoriteList;
use LuckyWeb\User\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use System\Classes\CombineAssets;

class ProductRelatedCountToCart extends ComponentBase
{
    public $product;

    public $themeAssetsPath = '/themes/shara/assets';
    public function componentDetails()
    {
        return [
            'name'        => 'ProductRelatedCountToCart Component',
            'description' => 'No description provided yet...productrelatedcounttocart'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function init()
    {
        $this->addJs(CombineAssets::combine([
            '/plugins/luckyweb/ms/components/productrelatedcounttocart/assets/js/product_related_count.js'
        ], base_path()));
    }

    public function onRender()
    {
        $this->product = $this->property('product');
    }

    public function onAddRelatedProduct()
    {
        $session_name = 'toCart' . '.products';
        $product = (string) post('product_id');
        //get and clear session
        $arr = Session::pull($session_name);
        $arr = collect($arr);
        $index = -1;
        if(!empty(post('product_id')))
        {
            for($i=0; $i < count($arr); $i++)
                if($arr[$i]['product_id']==$product)
                {
                    $index = $i;
                    break;
                }

            if ($index!=-1){
                if(post('count')>0) {
                    $arr = $arr->toArray();
                    $arr[$index]['count'] = post('count');
                }
                else{
                    $arr->forget($index);
                    $arr = $arr->values();
                }
            }
            else
                $arr->push(['product_id' => $product, 'count' => post('count'), 'price' => post('product_price')]);
        }
        // calculate total sum
        $sum = 0;
        $sum_count = 0;
        for($i=0; $i < count($arr); $i++) {
            $sum += ((int)$arr[$i]['count'] * (float)$arr[$i]['price']);
            $sum_count += (int)$arr[$i]['count'];
        }
        Session::put('toCart' . '.total', $sum);

        $trans_count = trans_choice('{0} товаров|{1} товар|[2,4] товара|[5,Inf] товаров', $sum_count);

        //insert array to session
        Session::put($session_name, $arr);
        return[
                '.js-set_top_bar' => $this->renderPartial(
                    'productrelatedcounttocart::top_bar.htm', [
                        'data' => $sum_count==0 ? null :[$sum, $sum_count, $trans_count],
                    ]
                ),
            ];
        //return Session::all();
    }

    public function onProductToCart(){
        $session_name = 'toCart' . '.products';
        //get and clear session
        $arr = Session::pull($session_name);
        $arr = collect($arr);

        //add to cart
        $list_id = FavoriteList::getDefaultList();
        $temp = Cookie::get(Cart::COOKIE_NAME, []);
        $temp = collect($temp);

        //add to cart from session
        for($i=0; $i < count($arr); $i++) {
            FavoriteListItem::addProductToFavList($temp, $arr[$i]['product_id'], $list_id, $arr[$i]['count'], null);
        }

        //update cart popup
        $cnt = FavoriteListItem::getProductCountsFromProduct($temp, $list_id);
        $arr = FavoriteListItem::getProductCountsFromFavList($temp, $list_id, true, ['product_id']);
        $supp = FavoriteListItem::getSupportsArray($temp, $list_id);
        return[
            '.js-cart_form_modal .modal-content' => $this->renderPartial(
                'cart::cart_form.htm', [
                    'cart_products' => $arr,
                    'cart_counts' => $cnt,
                    'cart_supports' => $supp,
                    'count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser())
                ]
            ),
            '.js-count_cart' => $this->renderPartial(
                'cart::header_count_cart_item.htm', [
                    'cart_count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser())
                ]
            ),
            '.js-count_product_list' => $this->renderPartial(
                'productchooser::header_count_list_item.htm', [
                    'count' => FavoriteListItem::getCountFavList($temp, Auth::getUser())
                ]
            ),
            '.js-set_top_bar' => $this->renderPartial(
                'productrelatedcounttocart::top_bar.htm', [
                    'data' => null,
                ]
            ),
            '.js-cart-header' => $this->renderPartial(
                'cart::cart_popup.htm', [
                    'cart_products' => $arr,
                    'cart_counts' => $cnt,
                    'cart_supports' => $supp,
                    'count' => FavoriteListItem::getCountDefaultFavList($temp, Auth::getUser())
                ]
            ),
        ];
    }
}
