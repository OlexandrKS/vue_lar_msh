<?php

namespace LuckyWeb\MS\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Jenssegers\Agent\Facades\Agent;
use LuckyWeb\MS\Models\AdminSettings;
use LuckyWeb\MS\Models\Image;
use LuckyWeb\MS\Models\Product;
use October\Rain\Exception\ValidationException;
use System\Classes\CombineAssets;

class Installments extends ComponentBase
{
    public $partialName;

    public function componentDetails()
    {
        return [
            'name' => 'Installments Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function onRun()
    {
        $this->prepareNow();

        $this->addJs(CombineAssets::combine([
            '/themes/shara/assets/js/plugins/inputmask/jquery.inputmask.min.js',
            '/plugins/luckyweb/ms/components/installments/assets/js/installments.js'
        ], base_path()), ['async']);
    }

    protected function prepareNow()
    {
        $now = Carbon::now();
        $endOfMonth = Carbon::now()->endOfMonth();

        $days = $endOfMonth
            ->diff($now)
            ->days;

        $this->page['days_form'] = $this->formatByCount($days, 'день', 'дня', 'дней');

        if ($month = request()->input('month')) {
            $this->partialName = $month;
            return;
        }

        switch ($now->month) {
            case 3:
                $this->partialName = 'march';
                break;
            case 4:
                $this->partialName = 'april';
                break;
            case 5:
                $this->partialName = 'may';
                break;
        }
    }

    /**
     * @return array
     * @throws ValidationException
     */
    public function onCallRequest()
    {
        $phone = str_replace([' ', '-', '(', ')', '+'], '', request()->input('phone'));

        $validation = Validator::make(compact('phone'), [
            'phone' => ['required', 'regex:/^7[0-9]{10}$/'],
        ], [
            'phone.regex' => 'Неверный формат номера',
        ]);

        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        $receiver = trim(AdminSettings::instance()->lending_order_manager_mail);

        if (!empty($receiver)) {
            Mail::send('luckyweb.ms::mail.manager.installments_request_created', compact('phone'),
                function ($message) use ($receiver) {
                    $message->to($receiver);
                }
            );
        }

        return [];
    }

    /**
     * Check if need to show simple grid
     * @return mixed
     */
    public function isSimpleGrid()
    {
        return Agent::isPhone();
    }

    /**
     * @param $count
     * @param $form1
     * @param $form2
     * @param $form3
     * @return mixed
     */
    public function formatByCount($count, $form1, $form2, $form3)
    {
        $count = abs($count) % 100;
        $lcount = $count % 10;
        if ($count >= 11 && $count <= 19) return ($form3);
        if ($lcount >= 2 && $lcount <= 4) return ($form2);
        if ($lcount == 1) return ($form1);
        return $form3;
    }
}
