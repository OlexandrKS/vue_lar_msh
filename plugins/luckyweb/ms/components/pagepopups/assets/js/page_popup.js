var PagePopup = function () {

    var initPagePopup = function () {
        var pagePopup = $('.modalpagepopup');
        setTimeout(function () {
            pagePopup.modal({
                keyboard: false,
                backdrop: 'static'
            });
            $('.modal-backdrop').removeClass("modal-backdrop");
            pagePopup.modal('show');
            $('.modal-open').css('padding-right', 0);
        }, pagePopup.data('timeout') * 1000);

    };

    var initPagePopupLink = function () {
        $('.page_popup_yes').click(function (e) {
            e.preventDefault();
            window.location.href = $(this).data('href') ? $(this).data('href') : window.location.href;
        });
    };

    var initPagePopupFloat = function () {
        $('#drawer-tab, .page_popup_no').click(function () {
            $('#promo-drawer').toggleClass('promo-drawer-open');
            $('.fa-angle-double-up, .fa-angle-double-down').toggleClass('fa-angle-double-up fa-angle-double-down');
        });
    };

    var initPagePopupButtonColor = function () {
        $(function () {
            $('.modalpagepopup button, #promo-drawer button').each(function () {
                $(this).css('background-color', $(this).data('color'));
            });

            $('.modalpagepopup a, #promo-drawer a').each(function () {
                $(this).css('color', $(this).data('color'));
            });
        });
    };

    var initHidePagePopup = function () {
        $('.modalpagepopup .close, .modalpagepopup .page_popup_no, .sp-button, .modalpagepopup .close-outer').click(function () {
            $.request('onHidePagePopup');
        });
    };

    var initHashUrlOpenPagePopupHTML = function () {
        $(window).on('hashchange', function (e) {
            console.log(e.newURL);
            var hash = window.location.hash;
            if (hash.indexOf('#popupinfo_', 0) == 0) {
                $.request('onShowPagePopupHTML', {
                    data: {
                        url_hash: hash
                    },
                    success: function (data) {
                        this.success(data);
                        $('#myPagePopupHTMLModal').modal('show');
                    },
                    error: function (data) {
                        self.setLoadingBar(false)
                        deferred.reject(data.responseText)
                    }
                })

            }
        });
    }

    var initHashUrlClosePagePopupHTML = function () {
        $('#myPagePopupHTMLModal').on('hidden.bs.modal', function () {
            location.hash = '#';
        })
    }

    var initButtonSendFormSubscribe = function () {
        $('#form-subscribe').submit(function (e) {
            e.preventDefault();
            var email = $('.input-subscribe').val();
            $.request('onSendEmailActivatePromoCode', {
                data: {email: email},
                success: function (response) {
                    if(response.result) {
                        $('.subscribe-response').empty().html(response.result);
                    }
                }
            })
        });
    };

    return {
        init: function () {
            initPagePopupButtonColor();
            initPagePopup();
            initPagePopupLink();
            initPagePopupFloat();
            initHidePagePopup();
            initHashUrlOpenPagePopupHTML();
            initHashUrlClosePagePopupHTML();
            initButtonSendFormSubscribe();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    PagePopup.init();
});
