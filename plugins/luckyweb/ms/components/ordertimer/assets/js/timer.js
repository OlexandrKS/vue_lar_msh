var Timer = function () {

    var cookieName = "fb_cal"

    var getCookie = function (name) {
        var  matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));

        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    var factory = function () {
        var cookieValue = getCookie(cookieName)

        return JSON.parse(cookieValue ? cookieValue : "{\"expired\":1,\"used\":true}")
    }

    var isShow = function () {
        var buyToday = factory()

        return Math.floor(Date.now() / 1000) <= buyToday.expired && !buyToday.used
    }

    var expiredAt = function () {
        var buyToday = factory()

        return buyToday.expired
    }

    var getTargetDate = function () {
        return new Date(expiredAt() * 1000);
    };

    var timer = $('.js-banner-timer');

    var getCountdown = function (timer) {
        var targetDate = getTargetDate();
        var days, hours, minutes, seconds;
        var currentDate = new Date().getTime();
        var secondsLeft = (targetDate - currentDate) / 1000;

        // days = pad(parseInt(secondsLeft / 86400));
        secondsLeft = secondsLeft % 86400;

        hours = pad(parseInt(secondsLeft / 3600));
        secondsLeft = secondsLeft % 3600;

        minutes = pad(parseInt(secondsLeft / 60));

        seconds = pad(parseInt(secondsLeft % 60));

        $('.second', timer).eq(0).text(seconds[0]);
        $('.second', timer).eq(1).text(seconds[1]);
        $('.minute', timer).eq(0).text(minutes[0]);
        $('.minute', timer).eq(1).text(minutes[1]);
        $('.hour', timer).eq(0).text(hours[0]);
        $('.hour', timer).eq(1).text(hours[1]);
    };

    var pad = function (n) {
        return (n < 10 ? '0' : '') + n;
    };

    var initTimer = function (timer) {
        setInterval(function () {
            getCountdown(timer);
        }, 1000);
    };

    var initTimerPopup = function () {
        $('.js-cart_form_modal').on('show.bs.modal', function () {
            var timer = $('.js-cart_form_modal .js-banner-timer');

            initTimer(timer);
        });
    };

    var headerTimer = function () {
        var activeClass = 'active';

        isShow() ? $('.js-banner-timer').addClass(activeClass) : null;

        $('.js-cart_form_modal').on('show.bs.modal', function () {
            isShow() ? $('.js-cart_form_modal .js-banner-timer').addClass(activeClass) : null;
        });
    }

    return {
        init: function () {
            headerTimer();
            initTimer();
            initTimerPopup();
        },
    };
}();

jQuery(function () {
    Timer.init();
});
