<?php namespace LuckyWeb\MS\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use LuckyWeb\MS\Models\AdminSettings;
use LuckyWeb\MS\Models\Product;
use October\Rain\Exception\ValidationException;
use System\Classes\CombineAssets;

class CallbackRequest extends ComponentBase
{
    /**
     * Path of theme-level assets
     * @var string $themeAssetsPath
     */
    public $themeAssetsPath = '/themes/shara/assets';

    public function componentDetails()
    {
        return [
            'name'        => 'CallbackRequest Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
//        $this->addJs(CombineAssets::combine([
//            $this->themeAssetsPath.'/js/plugins/inputmask/jquery.inputmask.min.js',
//            '/plugins/luckyweb/ms/components/callbackrequest/assets/js/callback_request.js'
//        ], base_path()));
    }

    /**
     * Render lending order form
     */
    public function onShowCallbackForm()
    {
        if(post('product_id') || post('product_name')) {

            $type = post('type', 'callback');
            return [
                '.js-callback_form_modal .modal-content' => $this->renderPartial(
                    'callbackrequest::types/'.$type, [
                        'product_id' => post('product_id'),
                        'product_name' => post('product_name'),
                        'type' => $type,
                        'production_mode' => strtolower(env('APP_ENV')) == 'production']
                )
            ];
        }
    }

    /**
     * Submit lending order
     * @return array
     * @throws ValidationException
     */
    public function onCallbackSubmit()
    {
        $rules = [
            'first_name' => 'required',
            'phone' => ['required', 'regex:/^7[0-9]{10}$/'],
            'is_accept' => 'accepted'
        ];
        $customMessages = [
            'required' => 'Обязательное поле',
            'phone.regex' => 'Неверный формат номера телефона',
            'accepted' => 'Для формирования заказа необходимо подтвердить согласие на обработку персональных данных',
        ];
        $post = post();
        $post['phone'] = preg_replace('/[^0-9]+/', '', $post['phone']);
        $validator = Validator::make($post, $rules, $customMessages);
        if($post['product_id'] && $product = Product::find($post['product_id'])) {
            $post['product_name'] = $product->name . ' ' . $product->subname;
            $post['product_url'] = url('/catalog/'.$product->category->slug.'/'.$product->slug);
        }

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $this->handleRequest($post);

        return [
            '.js-callback_form_modal .modal-content' => $this->renderPartial('callbackrequest::success_message.htm', [
                'production_mode' => strtolower(env('APP_ENV')) == 'production'
            ]),
        ];
    }

    /**
     * Do necessary actions
     * @param $post
     */
    protected function handleRequest($post)
    {
        switch (array_get($post, 'type', 'callback')) {
            case 'callback':
                $receiver = AdminSettings::instance()->lending_order_manager_mail;
                if (!empty($receiver)) {
                    Mail::queue('luckyweb.ms::mail.manager.callback_request_created', ['data' => $post], function($message) use($receiver, $post) {
                        $message->to($receiver);
                        $message->subject('Запрос на консультацию | '.$post['product_name']);
                    });
                }
                break;
            case 'design':
                $receiver = AdminSettings::instance()->design_mail;
                if (!empty($receiver)) {
                    Mail::queue('luckyweb.ms::mail.manager.design_request_created', ['data' => $post], function($message) use($receiver, $post) {
                        $message->to($receiver);
                        $message->subject('Запрос на бесплатный дизайн проект | '.$post['product_name']);
                    });
                }
                break;
        }
    }
}
