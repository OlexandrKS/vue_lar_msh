var Menu = function () {

    var menu = $('.side-menu');
    var subMenu = $('.menu-sub-list', menu);
    var activeClass = 'active';

    var timeoutId = 0;
    // var timeoutSubId = 0;

    var deleteTimeOut = function () {
      if(timeoutId !== 0) {
          clearTimeout(timeoutId);
          timeoutId = 0;
      }
    };

    // var deleteNextTimeOut = function () {
    //     if(timeoutSubId !== 0) {
    //         clearTimeout(timeoutSubId);
    //         timeoutSubId = 0;
    //     }
    // };

    var subMenuShow = function() {
        timeoutId = setTimeout(function () {
            menu.addClass(activeClass);
            subMenu.addClass(activeClass);
        }, 400);
    };

    var hideSubMenu = function() {
        deleteTimeOut();
        //TODO
        // timeoutSubId = setTimeout(function () {
        //     menu.removeClass(activeClass);
        //     subMenu.removeClass(activeClass);
        // }, 700);
        menu.removeClass(activeClass);
        subMenu.removeClass(activeClass);
    };

    var showCategory = function () {
        $('li a', menu).hover(function() {
            if($(this).is(':hover')) {
                var categoryId = $(this).data('cat-id');
                var categories = $('.sub-list-category');
                var category = $('.sub-list-category[data-cat-id=' + categoryId + ']');

                categories.removeClass(activeClass);
                category.addClass(activeClass);
                if(timeoutId === 0 && categoryId !== undefined) subMenuShow();
            } else {
                deleteTimeOut();
            }
        });
        //TODO
        // subMenu.hover(function () {
        //     if($(this).is(':hover')) {
        //         deleteNextTimeOut();
        //     }
        // })
    };

    var initToggleMenu = function() {

        // show menu
        // menuList.hover(subMenuShow);
        showCategory();

        // hide menu
        menu.hover(function() {}, hideSubMenu);
        $('.site-logo').on('mouseenter',hideSubMenu);
        $('.top-nav').on('mouseenter', hideSubMenu);
        $('.menu-list-wrap', menu).on('mousemove', function (e) {
            if (e.target.className === 'menu-list-wrap') { hideSubMenu(); }
        });
    };

    var initMobileMenuOpen = function () {
        $('.js-m-menu-show').on('click', function () {
            $('.js-m-menu').addClass('active');
            $('body').addClass('m-menu-open');
        });
    };
    var initMobileMenuClose = function () {
        $('.js-m-btn-close').on('click', function () {
            $('.js-m-menu').removeClass('active');
            $('body').removeClass('m-menu-open');

            $('.js-m-menu .sub-menu').each(function () {
                $(this).removeClass('active');
            });
        });
    };
    var initMobileSubMenuOpen = function () {
        $('.js-m-menu > .list > .item').on('click', function () {
            $('.sub-menu', this).addClass('active');
        });
    };
    var initMobileSubMenuClose = function () {
        $('body').on('click', '.js-close-m-sub-menu', function () {
            $(this).parent('.sub-menu').removeClass('active');
        });
    };

    return {
        init: function () {
            initToggleMenu();
            initMobileMenuOpen();
            initMobileMenuClose();
            initMobileSubMenuOpen();
            initMobileSubMenuClose();
        }
    };

}();

// Initialize when page loads
// jQuery(function () {
//     Menu.init();
// });
