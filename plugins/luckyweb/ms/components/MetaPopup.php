<?php namespace LuckyWeb\MS\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Request;
use System\Classes\CombineAssets;

class MetaPopup extends ComponentBase
{
    /**
     * Popup id
     * @var int
     */
    public $popupId;


    public function componentDetails()
    {
        return [
            'name'        => 'MetaPopup Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->popupId = $this->detect();

        $this->addJs(CombineAssets::combine([
            '/plugins/luckyweb/ms/components/metapopup/assets/js/meta_popup.js'
        ], base_path()), ['async']);
    }

    /**
     * Detect id by meta
     */
    protected function detect()
    {
        $source = env('source');
        if(empty($source)) {
            return null;
        }

        $source = str_replace(' ','', $source);
        $sources = explode(',', $source);

        if(in_array(Request::input('utm_source'), $sources)) {
            return 1;
        }
    }

    /**
     * Return content for popup for given meta tags
     * @return mixed
     */
    public function getContent()
    {
        switch ($this->popupId){
            case 1:
                return $this->renderPartial('metapopup::ny_gifts');
        }
    }

    /**
     * Return class for popup
     * @return string
     */
    public function getClass()
    {
        switch ($this->popupId) {
            case 1:
                return 'ny-gifts';
        }
    }
}
