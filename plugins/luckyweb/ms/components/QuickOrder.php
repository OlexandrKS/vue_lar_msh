<?php namespace LuckyWeb\MS\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use LuckyWeb\MS\Models\AdminSettings;
use LuckyWeb\MS\Models\Product;
use October\Rain\Exception\ValidationException;
use System\Classes\CombineAssets;

class QuickOrder extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'QuickOrder Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function init()
    {
        $this->addJs(CombineAssets::combine([
            '/plugins/luckyweb/ms/components/quickorder/assets/js/quickorder.js',
        ], base_path()), ['async']);
    }

    public function onQuickOrderFormShow()
    {
        return ['#js-quick-order .modal-content' => $this->renderPartial('quickorder::form.htm')];
    }

    public function onQuickOrderFormSubmit()
    {
        $postData = post();
        $telTmp = $postData['tel'];
        $postData['tel'] = str_replace([' ',  '-', '(', ')', '+'], '', $postData['tel']);

        $rules = [
            'user_name' => 'required',
            'tel' => ['required', 'regex:/^7[0-9]{10}$/'],
        ];

        $customMessages = [
            'required' => 'Обязательное поле',
            'tel.regex' => 'Неверный формат номера',
        ];

        $validation = Validator::make($postData, $rules, $customMessages);

        if ($validation->fails()) {
            throw new ValidationException($validation);
        }
        $postData['tel'] = $telTmp;

        /** @var Product $product */
        $product = Product::find($postData['id']);

        $mailData = [
            'name' => $postData['user_name'],
            'phone' => $postData['tel'],
            'productType' => $product->type_name_for_page,
            'productTitle' => $product->name,
            'productId' => $product->getKey(),
            'url' => URL::current(),
        ];

        Event::fire('luckyweb.ms.quickOrder', [$mailData]);

        return ['#js-quick-order .modal-content' => $this->renderPartial('quickorder::done.htm')];
    }
}
