<?php namespace LuckyWeb\MS\Components;

use Cms\Classes\ComponentBase;
use LuckyWeb\MS\Models\EmployerReward;

class EmployerRewards extends ComponentBase
{
    /**
     * Current rewards list
     * @var mixed
     */
    public $rewards;


    public function componentDetails()
    {
        return [
            'name'        => 'EmployerRewards Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->rewards = EmployerReward::orderBy('year', 'desc')->get();
    }
}
