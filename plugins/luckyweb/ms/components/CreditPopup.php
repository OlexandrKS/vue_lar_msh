<?php namespace LuckyWeb\MS\Components;

use Cms\Classes\ComponentBase;

class CreditPopup extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'CreditPopup Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
