<?php namespace LuckyWeb\MS\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Log;
use LuckyWeb\MS\Models\PromoPage;

class PromoBoard extends ComponentBase
{

    /**
     * Promo page
     * @var mixed
     */
    public $page;

    public function componentDetails()
    {
        return [
            'name'        => 'PromoBoard',
            'description' => 'Display promo board'
        ];
    }

    public function defineProperties()
    {
        return [
            'mode' => [
                'title'       => 'Режим работы',
                'type'        => 'string',
                'default'     => 'test',
            ],
            'id' => [
                'title'             => 'ID страницы',
                'type'              => 'string',
                'default'           => '',
            ],
        ];
    }

    public function onRun()
    {
        $this->page = null;
        if($this->property('mode') == 'production') {
            $this->page = PromoPage::published()->first();
        }
        else {
            //Log::info()
            if(!empty($this->property('id'))) {
                $id = base64_decode($this->property('id'));
                $this->page = PromoPage::find($id);
            }
            else {
                $this->page = PromoPage::first();
            }
        }
    }

}