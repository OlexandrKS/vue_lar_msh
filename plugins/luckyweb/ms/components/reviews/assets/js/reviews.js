var Reviews = function () {

    var initRadioButtons = function(){
        $('.js-is_client').on('change', function() {
            if ($('.js-is_client:checked').val() == 1) {
                $('.js-contract_holder label').append(' <abbr class="text-danger">*</abbr>');
                $('.js-contract_holder input').prop('disabled', false);
            }
            else {
                $('.js-contract_holder input').prop('disabled', true);
                $('.js-contract_holder label abbr').remove();
            }
        });
    };

    var initFormToggleButton = function(){
        $('.js-review_form_toggle').on('click', function(){
            if($('.js-review_form').hasClass('active')) {
                $('.js-review_form').removeClass('active').slideUp();
            }
            else {
                $('.js-review_form').addClass('active').slideDown();
            }
        })
    };

    var displayFieldErrors = function(jqXHR) {
        var $form = $('.js-review_form form');
        $form.find('.form-group.has-error').each(function () {
            $(this).removeClass('has-error').find('.error').hide();
        });

        if (typeof jqXHR.responseJSON != 'undefined') {
            $.each(jqXHR.responseJSON.X_OCTOBER_ERROR_FIELDS, function (field, message) {
                var $group = $form.find("[name='"+field+"']").parents('.form-group').addClass('has-error');
                var $error = $group.find(".error");
                if ($error.length) {
                    $error.eq(0).text(message).show();
                }
                else {
                    $group.append('<div class="error">'+message+'</div>');
                }
            });
        }
    };

    var initSelectPlaceholderFix = function(){
        $('select').change(function() {
            if ($(this).children('option:first-child').is(':selected')) {
                $(this).addClass('placeholder');
            } else {
                $(this).removeClass('placeholder');
            }
        });
    };
    
    var initFormSubmit = function(){
        $('.js-review_form form').on('submit', function(e){
            var $form = $(this);

            e.preventDefault();
            $form.find('button[type=submit]').prop('disabled', true);

            if(ReviewsVariables.productionMode) {
                yaCounter33317453.reachGoal('send_feedback');
            }
            $form.request('onReviewAdd', {
                error: function(jqXHR, status, obj) {
                    displayFieldErrors(jqXHR);
                    return false;
                },
                complete: function() {
                    $form.find('button[type=submit]').prop('disabled', false);
                }
            })
        });
    };

    var initShowAllButton = function(){
        $('.js-show_all').on('click', function(e){
            e.preventDefault();
            var $messagesList = $(this).parent().siblings('.js-messages_list');
            if($messagesList.hasClass('active')) {
                $messagesList.slideUp().removeClass('active');
                $(this).find('b').text($(this).data('close-message'));
            }
            else {
                $messagesList.slideDown().addClass('active');
                $(this).find('b').text('Скрыть');
            }
            $(this).blur();
        });
    };
    
    var initProductReviewsButton = function(){
        $('.js-product_reviews_button').on('click', function(e){
            e.preventDefault();

            closeHolders();
            var $this = $(this);
            if($this.hasClass('active')) {
                $('.js-additional_block .js-product_reviews_holder').slideUp();
                $this.removeClass('active');
            }
            else {
                $('.js-additional_block .js-product_reviews_holder').slideDown();
                $this.addClass('active');
            }
        });
    };

    // var scrollToReview = function(){
    //     $('html, body').animate({
    //         scrollTop: $(".js-additional_block").offset().top - 50
    //     }, 300);
    // };

    var formatProduct = function (item) {
        var template = '';
        if(item.image) {
            template = '<div class="row">' +
                '<div class="col-sm-4">' +
                    '<img src="' + item.image + '">' +
                '</div>' +
                '<div class="col-sm-8">' +
                    '<h3>' + item.name + '</h3>' +
                    '<div class="subname">(' + item.subname + ')</div>' +
                '</div>' +
                '</div>';
        }
        return template;
    };

    var formatProductSelection = function (item) {
        return item.name ? item.name + '(' + item.subname + ')' : 'Выберите товар';
    };

    // var initProductSelect = function() {
    //     $('.js-product_select').select2({
    //         ajax: {
    //             url: ReviewsVariables.getProducts,
    //             dataType: 'json',
    //             delay: 250,
    //             data: function (params) {
    //                 return {
    //                     q: params.term,
    //                     page: params.page
    //                 };
    //             },
    //             processResults: function (data, params) {
    //                 params.page = params.page || 0;
    //
    //                 return {
    //                     results: data.items,
    //                     pagination: {
    //                         more: data.items.length > 0
    //                     }
    //                 };
    //             },
    //             cache: true
    //         },
    //         escapeMarkup: function (markup) {
    //             return markup;
    //         },
    //         minimumInputLength: 1,
    //         templateResult: formatProduct,
    //         templateSelection: formatProductSelection,
    //         language: "ru"
    //     });
    // };

    var initLikeButton = function() {
        $('.js-likes_wrapper').on('click', '.js-like', function(e){
            var $this = $(this);

            e.preventDefault();
            $.request('onLike', {
                data: {
                    review_id: $this.data('id')
                },
                complete: function(data) {
                    if(data.responseJSON.likes_html != undefined) {
                        $this.parents('.js-likes_wrapper').html(data.responseJSON.likes_html);
                    }
                }
            })
        });
    };

    var initDislikeButton = function() {
        $('.js-likes_wrapper').on('click', '.js-dislike', function(e){
            var $this = $(this);

            e.preventDefault();
            $.request('onDislike', {
                data: {
                    review_id: $this.data('id')
                },
                complete: function(data) {
                    if(data.responseJSON.likes_html != undefined) {
                        $this.parents('.js-likes_wrapper').html(data.responseJSON.likes_html);
                    }
                }
            });
        });
    };

    var initSortSelect = function() {
        $('.js-reviews_filter').on('change', function(){
            var $form = $(this).parents('form');
            console.log($form);
            $form.request('onSetFilter');
        });
    };

    var initReviewCount = function() {
        if(typeof ReviewsVariables != 'undefined') {
            if(ReviewsVariables.reviewsCount > 0) {
                $('.js-review_count').text(ReviewsVariables.reviewsCount);
            }
            else {
                $('.js-product_star').addClass('mute');
            }
        }
    };

    var closeHolders = function(){
        //$('.js-additional_block .js-product_reviews_holder').hide();
        $('.js-additional_block .js-view_phone_holder').hide();

        //$('.js-product_reviews_button').removeClass('active');
        $('.js-view_phone_button').removeClass('active');
    };

    var initReviewButton = function(){
        $('.js-review_button').on('click', function (e) {
            e.preventDefault();

            $('[href=#reviews]').tab('show');

            $('html, body').animate({
                scrollTop: ($('.js-product_tabs').offset().top - 100) + 'px'
            }, 'fast');
        })
    };

    return {
        init: function () {
            initReviewButton();
            initReviewCount();
            $("[name='phone-number']").inputmask("7 (999) 999-99-99");
            $("[name='contract_number']").inputmask({
                mask : "999-99999999999[9]{100}",
                greedy: false
            });
            initFormSubmit();
            initSelectPlaceholderFix();
            initRadioButtons();
            initFormToggleButton();
            initShowAllButton();
            initProductReviewsButton();
            // initProductSelect();
            initLikeButton();
            initDislikeButton();
            initSortSelect();
        },
        ready: function(){
            if(typeof ReviewsVariables != 'undefined' && ReviewsVariables.scrollToBlock) {
               // scrollToReview();
            }
        },
        displayFieldErrors: displayFieldErrors
    };
}();

// Initialize when page loads
jQuery(function () {
    Reviews.init();
});
// Initialize when page ready
jQuery(function () {
    $(document).ready(function() {
        Reviews.ready();
    });
});
