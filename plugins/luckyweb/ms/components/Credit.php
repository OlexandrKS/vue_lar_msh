<?php namespace Luckyweb\Ms\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use October\Rain\Exception\ValidationException;
use System\Classes\CombineAssets;

class Credit extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Credit Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function init()
    {
        $this->addJs(CombineAssets::combine([
            '/plugins/luckyweb/ms/components/credit/assets/js/credit.js',
        ], base_path()), ['async']);
    }

    public function onFormSubmit()
    {

        $data = Input::get('form_data');

        $rules = [
            'name' => 'required',
            'second_name' => 'required',
            'last_name' => 'required',
            'passport' => 'required',
            'phone' => 'required',
            'privacy' => 'accepted'
        ];

        $attributes = [
            'name' => '',
            'second_name' => '',
            'last_name' => '',
            'passport' => '',
            'phone' => '',
            'privacy' => 'политику конфиденциальности'
        ];

        $validation = Validator::make($data, $rules, [], $attributes);

        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

/*        if (!empty($receiver)) {
            Mail::send('luckyweb.ms::mail.manager.credit_order_created', $data, function ($message) use ($receiver) {
                $message->to($receiver);
            });
        }*/


        return $data;

    }
}
