<?php namespace Luckyweb\Ms\Components;

use Cms\Classes\ComponentBase;
use Session;
use Luckyweb\Ms\Models\MenuItem;
use Luckyweb\Ms\Models\Product;

class MenuConstructor extends ComponentBase
{
    /**
     * Path of theme-level assets
     * @var string $themeAssetsPath
     */
    public $themeAssetsPath = '/themes/shara/assets';
    public $menu;

    public function componentDetails()
    {
        return [
            'name'        => 'MenuCunstructor Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->page['menu_vertical'] = MenuItem::query()
            ->where('orientation_id', MenuItem::ORIENTATION_VERTICAL)
            ->orderby('order')
            ->get();

        $this->addJs('components/menuconstructor/assets/js/menuconstructor.js?'.time());
    }

    public function getProductById($id)
    {
        return Product::where('id',$id)->first();
    }
}
