var ProductChooser = function () {

    var initChooserFormButton = function(){
        $('.js-product_chooser_form_button').on('click', function(e){
            e.preventDefault();
            var productId = $(this).data('id');
            var productName = $(this).data('name');

            $.request('onShowChooserForm', {
                data: {
                    product_id: productId,
                    product_name: productName
                },
                success: function(data){
                    this.success(data);
                    $('.js-product_chooser_form_modal').modal('show');
                    actionsRow();
                    actionsRadio();
                }
            });
        })
    };

    var actionsRow = function(){
        var div = $('.list-product');

        div.find('.plusminus').on('click', function(e){
            e.preventDefault();
            var numb = Number( $(this).parent().find('.number').text() );

            if ($(this).attr("id") == 'minus') {
                if (numb > 1)
                    $(this).parent().find('.number').text(--numb);
            }
            else
                if ($(this).attr("id") == 'plus')
                {
                    if(numb<10)
                       $(this).parent().find('.number').text(++numb);
                }
            var productId = $(this).data('id');
            var listId = $(this).data('request-data');
            if(productId){
                $.request('onChangeCountListItem', {
                    data: {
                        product_id: productId,
                        count: numb,
                        list_id:listId
                    },
                    success: function(data){
                        this.success(data);
                    }
                });
            }
            $('#count').val(numb);
            var price = Number($(this).parent().parent().parent().parent().find('#price').text());
            $(this).parent().parent().parent().parent().find('#summ').text(formatNumber(numb*price)+' ');
            summTotal();
        })
    };

    function formatNumber (num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")
    }

    var summTotal = function(){
        var total = 0;
        $('.summ').each(function (i) {
            total += Number($(this).text().replace(/\s+/g, ''));
        });
        $('#total').text(formatNumber(total)+' ');
    };

    var actionsRadio = function(){
        $('input[type="radio"]').on('click', function(e){
            $('#selector').val($(this).val());
        })
        $('input[type="radio"]').get(0).click();//select first el auto
    };

    var actionRenameList = function(){
        $('.js-rename-list').on('click', function(e){
            e.preventDefault();
            var parentDiv = $(this).parent();
            parentDiv.addClass('edit');

            parentDiv.find('.js-fav-list_name_lbl').hide();
            parentDiv.find('.date').hide();
            parentDiv.find('.js-fav-list_name_form').show();
            $(this).hide();
        })
    };

    var actionAfterRenameList = function(num){
        var divBlock = $(".fav-list-"+String(num)+'.edit');
        var newName = divBlock.find('.js-fav-list_name_form input').val();

        divBlock.find('.js-fav-list_name_lbl').show();
        divBlock.find('.date').show();
        divBlock.find('.js-fav-list_name_form').hide();
        divBlock.find('.js-rename-list').show();
        divBlock.removeClass('edit');

        var count = divBlock.find('.js-fav-list_name_lbl').data('count');
        divBlock = $(".fav-list-"+String(num));
        divBlock.find('.js-fav-list_name_lbl').text( newName + " (" + count + ")");
    };

    var actionNoneAuth = function(){
        $('.js-order-list_none_auth').on('click', function(e){
            e.stopPropagation();
            $.request('onNoneAuth', {
                data: {
                },
                success: function(data){
                    this.success(data);
                    $('.js-product_chooser_form_modal').modal('show');
                    actionsRow();
                }
            });
        })
    };

    var initSetListPopup = function() {
        $('.js-set_popup-list').on('click', function(e){
            e.preventDefault();

            var $this = $(this);
            if($this.hasClass('open')) {
                $this.find('i').removeClass('fa-chevron-up')
                    .addClass('fa-chevron-down');

                $this.removeClass('open');
                $this.siblings('.js-set_content').fadeOut();
            }
            else {
                $this.find('i').removeClass('fa-chevron-down')
                    .addClass('fa-chevron-up');

                $('.js-set_content').fadeOut();

                $this.addClass('open');
                $this.siblings('.js-set_content').fadeIn();
            }
        })
    };

    var initListClose = function()
    {
        $(document).click(function(event) {
            if($(event.target).hasClass('content'))
                if(!$(event.target).closest(".js-set_popup-list").length && $('.js-set_popup-list').hasClass('open')) {
                    $('.js-set_popup-list')
                        .removeClass('open')
                        .find('i').removeClass('fa-chevron-up')
                        .addClass('fa-chevron-down');

                    $('.js-set_content').fadeOut();
                }
        });
    };

    return {
        init: function () {
            initChooserFormButton();
            actionsRow();
            actionRenameList();
            actionNoneAuth();
            initSetListPopup();
            initListClose();
        },
        initRadio:function(){
            actionsRadio();
            var list = $('input[type="radio"]');
            list.get(list.length-1).click();
        },
        showModal:function(){
            $('.js-product_chooser_form_modal').modal('show');
        },
        afterRename:function(num){
            actionAfterRenameList(num);
        },
        initRename:function(){
            initSetListPopup();
            actionRenameList();
            initListClose();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    ProductChooser.init();
});
