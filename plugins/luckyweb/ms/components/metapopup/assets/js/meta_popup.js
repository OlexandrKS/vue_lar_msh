var MetaPopup = function () {

    var openModal = function(){
        $(window).load(function(){
            $('.js-meta_modal').modal('show');
            //$('.modal-backdrop').removeClass("modal-backdrop");
        });
    };

    return {
        init: function () {
            if(typeof MetaPopupData !== 'undefined' && MetaPopupData.isOpen) {
                openModal();
            }
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    MetaPopup.init();
});
