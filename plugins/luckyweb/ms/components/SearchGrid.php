<?php namespace LuckyWeb\MS\Components;

use Cms\Classes\ComponentBase;

use GuzzleHttp\Stream\GuzzleStreamWrapper;
use GuzzleHttp;
use Illuminate\Database\Eloquent\Builder as Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use LuckyWeb\MS\Models\Image;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\SiteGoodType;
use LuckyWeb\MS\Models\CatalogCategory;

use Request;
use Illuminate\Support\Facades\Input;
use Cviebrock\LaravelElasticsearch\Facade as Elasticsearch;

class SearchGrid extends ComponentBase
{

    /**
     * Collection of page products
     * @var Collection $products
     */
    public $products;

    /**
     * @var
     */
    public $query;
    public $pagination;
    public $nextTile;

    public function componentDetails()
    {
        return [
            'name' => 'SearchGrid Component',
            'description' => 'Generates the grid of prodicts for search page'
        ];
    }

    public function productionMode()
    {
        return strtolower(env('APP_ENV')) == 'production';
    }

    /**
     * Component initializing
     */
    public function onRun()
    {
        if (config('elasticsearch.enabled')) {
            if (Input::has('query')) {
                $q = Input::get('query');

                $perPage = 27;
                $page = LengthAwarePaginator::resolveCurrentPage();
                $from = ($page - 1) * $perPage;

                $tmp = [
                    'index' => 'products',
                    'size' => $perPage,
                    'from' => $from,
                    'body' => [
                        'query' => [
                            'bool' => [
                                'should' => [
                                    [['multi_match' => [
                                        'type' => 'phrase',
                                        'query' => $q,
                                        'fields' => ['good_type_site.type_name^2', 'name'],
                                        'boost' => 20.0

                                    ]]]
                                ],
                                'must_not' => [
                                    ['term' => [
                                        'category_invisible' => 1
                                    ]],
                                    ['term' => [
                                        'also_in_spec_list' => 1
                                    ]]
                                ],
                            ]
                        ]
                    ]
                ];
                $query = Elasticsearch::search($tmp);
                $searchResults = $query['hits'];

                //gather searching results
                $search_hits = [];
                foreach ($searchResults['hits'] as $result) {

                    $search_hits [] = $result['_source'];
                }

                $groups = [];

                //group for null-sitecategory products
                $otherGroup = new SiteGoodType([
                    'type_id' => 100,
                    'type_name' => 'Без категории',
                    'width' => 300,
                ]);

                $products = hydrate(Product::class, $search_hits);

                $p = new LengthAwarePaginator($products, array_get($searchResults, 'total.value', 0), $perPage);
                $p->setPath('/search');
                $p->appends([
                    'query' => $q,
                ]);

                /** @var Product $product */

                //remap array as $name_of_group => $products
                foreach ($products as $product) {
                    if (is_null($product->good_type_site)) {
                        $product->site_type_id = $otherGroup->type_id;
                        $product->setRelation('good_type_site', $otherGroup);
                    }
                }
                $next = [
                    'url' => $p->nextPageUrl(),
                    'text' => 'Следующая страница'
                ];
                $this->nextTile = $next;
                //.dd($this->nextTitle);
                $this->query = $q;
                $this->products = $products;
                $this->pagination = $p;
            }
        }

    }
}
