<?php namespace LuckyWeb\MS\Components;

use Cms\Classes\ComponentBase;
use Exception;
use Illuminate\Support\Facades\Validator;
use LuckyWeb\MS\Models\Shop;
use LuckyWeb\MS\Models\ShopCity;
use October\Rain\Exception\ValidationException;
use System\Classes\CombineAssets;

class ProductViewPhone extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'ProductViewPhone Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
//        $this->addJs(CombineAssets::combine([
//            '/plugins/luckyweb/ms/components/productviewphone/assets/js/product_view_phone.js'
//        ], base_path()));
    }

    /**
     * Return cities list
     * @return mixed
     */
    public function getCities()
    {
        return ShopCity::orderBy('name', 'ASC')->get();
    }

    /**
     * Return shops list
     * @param $cityId
     * @return mixed
     */
    public function getShops($cityId)
    {
        if(!empty($cityId)) {
            return Shop::where('city_id', $cityId)
                ->orderBy('mall_name', 'ASC')->get();
        }
        else {
            return Shop::orderBy('mall_name', 'ASC')->get();
        }
    }

    public function onViewPhone()
    {

        $rules = [
            'city_id' => 'required',
            'shop_id' => 'required',
        ];
        $customMessages = [
            'required' => 'Обязательное поле',
        ];

        $validator = Validator::make(post(), $rules, $customMessages);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $shop = null;
        try {
            $city = ShopCity::findOrFail(post('city_id'));
            $shop = Shop::where('city_id', $city->id)
                ->where('id', post('shop_id'))
                ->firstOrFail();
        }
        catch(Exception $e) {}

        return [
            '.js-phone_number' => $this->renderPartial('productviewphone::phone_number', ['shop' => $shop])
        ];
    }

    public function onCitySelected()
    {
        return [
            '.js-shop_select' => $this->renderPartial('productviewphone::shop_select', ['cityId' => post('city_id')])
        ];
    }
}
