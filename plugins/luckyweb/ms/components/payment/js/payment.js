var Payment = function () {

	var initCardPay = function (){
        $('#paystep').click(function (e){
            e.preventDefault();
            var amount = parseInt($('#dataAmount').text());
            $(this).addClass('disabled')

            $('#orderForm').request('payment::onCardPay', {
                complete: function () {
                    $(this).removeClass('disabled')
                }
            });
        })
    };

    var initSelectPaymentMethod = function () {
        $('select[name=payment_method]').on('change', function () {
            var paymentMethod = $(this).val();

            $('button[data-p-type]').hide();
            $('button[data-p-type = ' + paymentMethod + ']').show();
        });
    };

    var initSelectPaymentMethodChange = function () {
        $('select[name=payment_method]').trigger('change');
    };

    var initCreditForm = function () {
        $('#credit').on('click', function (e) {
            e.preventDefault();

            $('#orderForm').request('payment::onCredit', {
                success: function (data) {
                    this.success(data);

                    var creditForm = $('.js-credit-modal form');

                    $('[name=name]', creditForm).val(data.first_name);
                    $('[name=second_name]', creditForm).val(data.second_name);
                    $('[name=phone]', creditForm).val(data.phone);
                }
            });

        });
    };

    return {
        init: function () {
            initCardPay();
            initCreditForm();
            initSelectPaymentMethod();
            initSelectPaymentMethodChange();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    Payment.init();
});
