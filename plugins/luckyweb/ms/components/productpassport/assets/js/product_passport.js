var ProductPassport = function () {

    var initGalleryButton = function(){
        $('.js-passport_button').on('click', function(e) {
            e.preventDefault();
            if(typeof ProductPassportVariables != 'undefined' && ProductPassportVariables.items.length > 0) {
                $.magnificPopup.open({
                    type:'image',
                    items: ProductPassportVariables.items,
                    gallery: {
                        enabled: true
                    }
                });
            }
        });
    };

    return {
        init: function () {
            initGalleryButton();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    ProductPassport.init();
});
