$(document).on('click', '#add-new-url', function (e) {
    e.preventDefault();
    var time = $.now();
    var first_url_input = $('#first-url-input').clone();
    first_url_input.addClass("cl-" + time);
    first_url_input.find('.unlink-input').attr('id', time);
    first_url_input.find('input').val("");
    $('#list-fields-url').append(first_url_input);
    return false;
});
$(document).on('click', '.unlink-input', function (e) {
    e.preventDefault();
    $('.cl-' + $(this).attr('id')).remove();
});
