<?php
namespace LuckyWeb\MS\Console;

use Illuminate\Console\Command;
use LuckyWeb\MS\Models\Furnisher;
use LuckyWeb\MS\Models\VacancyCity;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Log;

use LuckyWeb\MS\Classes\ImportManager;

use LuckyWeb\MS\Models\Settings;
use LuckyWeb\MS\Models\Image;
use LuckyWeb\MS\Models\Category;
use LuckyWeb\MS\Models\SubCategory;
use LuckyWeb\MS\Models\Color;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\Promotion;
use LuckyWeb\MS\Models\ShopCity;
use LuckyWeb\MS\Models\ShopRegion;
use LuckyWeb\MS\Models\Shop;
use LuckyWeb\MS\Models\Set;
use LuckyWeb\MS\Models\GoodType;
use LuckyWeb\MS\Models\NestedPosition;
use LuckyWeb\MS\Models\Specification;
use LuckyWeb\MS\Models\SpecificationValue;
use LuckyWeb\MS\Models\TogetherCheeperPivot;
use LuckyWeb\MS\Models\SiteGoodType;



class ImportCommand extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'ms:import';

    /**
     * @var string The console command description.
     */
    protected $description = 'Imports data from backoffice ftp';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        // TODO transaction
        $importManager = new ImportManager();

        if (!$this->option('force') && !$importManager->canImport()) {
            $this->error('Import is not allowed');
            return;
        }

        // TODO rewrite to console output
        Log::info('Import started: '.$this->argument('mode'));


        $this->info("*** IMPORT FROM BACKOFFICE ***");

        if ($this->argument('mode')=='data' || $this->argument('mode')=='all') {
            $importManager->importData();
            $this->info('Updating entities');
            echo "Category\n";
            Category::updateImported();
            echo "SubCategory\n";
            SubCategory::updateImported();
            echo "Color\n";
            Color::updateImported();
            echo "Product\n";
            Product::updateImported();
            echo "Promotion\n";
            Promotion::updateImported();
            echo "Shop\n";
            Shop::updateImported();
            echo "Set\n";
            Set::updateImported();
            echo "GoodType\n";
            GoodType::updateImported();
            echo "NestedPosition\n";
            NestedPosition::updateImported();
            if ($this->argument('mode')=='data') {
                echo "Image relations\n";
                Image::updateProductRelations();
            }
            echo "Specification\n";
            Specification::updateImported();
            echo "SpecificationValue\n";
            SpecificationValue::updateImported();
            echo "Furnisher\n";
            Furnisher::updateImported();
            echo "SiteGoodType\n";
            SiteGoodType::updateImported();
            echo "TogetherCheeperPivot\n";
            TogetherCheeperPivot::updateImported();


            // Update vacancy cities
            echo "VacancyCity\n";
            VacancyCity::seedFromShopCities();
        }

        if ($this->argument('mode')=='images' || $this->argument('mode')=='all') {
            if ($importManager->importImages()) {
                Image::updateProductRelations();
            }
        }

        $importManager->cleanImportFlagFile();

        if (($this->argument('mode')=='images' || $this->argument('mode')=='all') && $this->option('minimize')) {
            $this->info('Call minimize');
            $this->call('ms:minimize', []);
        }

        Log::info('Import completed: '.$this->argument('mode'));
    }



    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['mode', InputArgument::REQUIRED, 'Import mode: data|images|shops|all'],
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['minimize', null, InputOption::VALUE_NONE, 'Minimize images after import.', null],
            ['force', null, InputOption::VALUE_NONE, 'Force import on production.', null],
        ];
    }

    protected function debug($object) {
        $this->output->writeln(
            is_array($object) || is_object($object) ? print_r($object, true) : $object);
    }

}
