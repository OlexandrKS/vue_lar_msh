<?php namespace LuckyWeb\MS\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use LuckyWeb\User\Models\Order;
use LuckyWeb\MS\Models\LastExport;
use Google\Cloud\BigQuery\BigQueryClient;

class BigqueryCommand extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'ms:bigquery';

    /**
     * @var string The console command description.
     */
    protected $description = 'Loading Data into BigQuery from a Local Data Source';

    /**
     * @var object Last export
     */
    protected $lastExport = null;

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $rows = [];

        // get bigquery last expost
        $this->lastExport = LastExport::where([
            'export_type' => Order::class,
            'export_into' => 'Bigquery'
        ])->first();

        // new orders
        $orders = Order::with(['user'])
            ->where('status_updated_at', '>', $this->lastExport->exported_at)
            ->get();

        // get current time
        $time = date("Y-m-d H:i:s");

        foreach ($orders as $order) {
            // get city and region
            $getCityRegion = $this->getCityRegion(preg_match("/^.+\=(.+)\-.+$/", $order->external_id, $mathes) ? $mathes[1] : $order->external_id);
            // add orders to data
            $rows[] = [
                'insertId' => $order->id,
                'data' => [
                    'user_id' => $order->user ? ($order->user->card_code ? $order->user->card_code : '') : '', // Номер карты
                    'transaction_id' => preg_match("/^.+\=(.+)\=.+$/", $order->external_id, $mathes) ? $mathes[1] : $order->external_id, // Номер заказа
                    'transaction_status' => $this->getStatus($order->status_id), // Статус заказа
                    'transaction_revenue' => $order->total_amount, // Итого по заказу
                    'transaction_created' => strtotime($order->created_at), // Дата заказа
                    'transaction_changed' => strtotime($order->updated_at), // Время последнего изменения транзакции
                    'transaction_city' => $getCityRegion[0], // Город
                    'transaction_region' => $getCityRegion[1] // Регион
                ]
            ];
        }
        // stream data into bigquery
        if (count($rows) > 0) {
            $this->stream_row(env('GOOGLE_PROJECT_ID'), env('BIGQUERY_DATABASE'), env('BIGQUERY_TABLE'), env('GOOGLE_APPLICATION_CREDENTIALS'), $time, $rows);
        } else {
            print('No new data' . PHP_EOL);
        }
    }

    /**
     * Stream a row of data into your BigQuery table
     * Example:
     * ```
     * $data = [
     *     "field1" => "value1",
     *     "field2" => "value2",
     * ];
     * stream_row($projectId, $datasetId, $tableId, $data);
     * ```.
     *
     * @param string $projectId The Google project ID.
     * @param string $datasetId The BigQuery dataset ID.
     * @param string $tableId   The BigQuery table ID.
     * @param string $serviceAccountPath   The BigQuery credentials.
     * @param string $time      Export time
     * @param string $data      An associative array representing a row of data.
     * @param string $insertId  An optional unique ID to guarantee data consistency.
     */
    public function stream_row($projectId, $datasetId, $tableId, $serviceAccountPath, $time, $rows, $insertId = null)
    {
        // instantiate the bigquery table service
        $bigQuery = new BigQueryClient([
            'projectId' => $projectId,
            'keyFilePath' => $serviceAccountPath,
        ]);
        $dataset = $bigQuery->dataset($datasetId);
        $table = $dataset->table($tableId);

        $insertResponse = $table->insertRows($rows);
        if ($insertResponse->isSuccessful()) {
            print('Data streamed into BigQuery successfully' . PHP_EOL);
            $this->lastExport->exported_at = $time;
            $this->lastExport->save();
        } else {
            foreach ($insertResponse->failedRows() as $row) {
                foreach ($row['errors'] as $error) {
                    printf('%s: %s' . PHP_EOL, $error['reason'], $error['message']);
                }
            }
        }
    }

    /**
     * Get string status by status id
     */
    protected function getStatus($key)
    {
        $status = [
            1  => 'in_process', // ['PRODUCTION', 'В производстве', 'Ваш товар находиться в производстве.'],
            4  => 'awaiting_fulfillment', // ['SHIPMENT', 'Готовится к отгрузке в ваш город', 'Ваш товар готовиться к отгрузке в ваш город'],
            5  => 'awaiting_shipment', // ['DELIVERY_COORDINATION', 'Согласование времени доставки', 'Ожидайте звонок от менеджера для согласования времени доставки'],
            11 => 'cancelled', // ['CANCELED', 'Отменен'],
            12 => 'completed', // ['DELIVERED', 'Товар доставлен', 'Товар доставлен :)'],
            13 => 'refunded' // ['RETURN', 'Возврат'],
        ];
        return isset($status[$key]) ? $status[$key] : $key;
    }

    /**
     * Get city and region by short code
     */
    protected function getCityRegion($key)
    {
        $cities = [
            'АЛ1' => ['Алексин', 'Тульская'],
            'БГК' => ['Борисоглебск', 'Воронежская'],
            'БЛ1' => ['Белгород', 'Белгородская'],
            'БЛ2' => ['Белгород', 'Белгородская'],
            'БЛ3' => ['Белгород', 'Белгородская'],
            'БЛ4' => ['Белгород', 'Белгородская'],
            'БЛ5' => ['Белгород', 'Белгородская'],
            'БР1' => ['Брянск', 'Брянская'],
            'БР3' => ['Брянск', 'Брянская'],
            'БР4' => ['Брянск', 'Брянская'],
            'БР5' => ['Брянск', 'Брянская'],
            'ВЖ1' => ['Воронеж', 'Воронежская'],
            'ВЖ2' => ['Воронеж', 'Воронежская'],
            'ВЖ3' => ['Воронеж', 'Воронежская'],
            'ВЖ4' => ['Воронеж', 'Воронежская'],
            'ВЖ5' => ['Воронеж', 'Воронежская'],
            'ВЖ8' => ['Воронеж', 'Воронежская'],
            'ВЗМ' => ['Вязьма', 'Смоленская'],
            'ДОН' => ['Донской', 'Тульская'],
            'ДСО' => ['Орёл', 'Орловская'],
            'ЕЛ1' => ['Елец', 'Липецкая'],
            'ЖГК' => ['Железногорск', 'Курская'],
            'ИМШ' => ['ИМШ', 'ИМШ'],
            'КГ2' => ['Калуга', 'Калужская'],
            'КЛ3' => ['Калуга', 'Калужская'],
            'КЛ4' => ['Калуга', 'Калужская'],
            'КЛГ' => ['Калуга', 'Калужская'],
            'КЛЦ' => ['Клинцы', 'Брянская'],
            'КР2' => ['Курск', 'Курская'],
            'КР4' => ['Курск', 'Курская'],
            'КР5' => ['Курск', 'Курская'],
            'КРС' => ['Курск', 'Курская'],
            'КРЧ' => ['Курчатов', 'Курская'],
            'ЛВН' => ['Ливны', 'Орловская'],
            'ЛДН' => ['Людиново', 'Калужская'],
            'ЛСК' => ['Лиски', 'Воронежская'],
            'МЦК' => ['Мценск', 'Орловская'],
            'НК1' => ['Новомосковск', 'Тульская'],
            'ОР5' => ['Орёл', 'Орловская'],
            'ОРЛ' => ['Орёл', 'Орловская'],
            'ПД1' => ['Подольск', 'Московская'],
            'ПНА' => ['Орёл', 'Орловская'],
            'ПТН' => ['Орёл', 'Орловская'],
            'РЗ1' => ['Рязань', 'Рязанская'],
            'РЗ2' => ['Рязань', 'Рязанская'],
            'РЗ3' => ['Рязань', 'Рязанская'],
            'СМ1' => ['Смоленск', 'Смоленская'],
            'СМ2' => ['Смоленск', 'Смоленская'],
            'СМ3' => ['Смоленск', 'Смоленская'],
            'СО1' => ['Старый Оскол', 'Белгородская'],
            'СО2' => ['Старый Оскол', 'Белгородская'],
            'ТБ1' => ['Тамбов', 'Тамбовская'],
            'ТБ2' => ['Тамбов', 'Тамбовская'],
            'ТБ3' => ['Тамбов', 'Тамбовская'],
            'ТЛ2' => ['Тула', 'Тульская'],
            'ЩКН' => ['Щекино', 'Тульская'],
            'ЮБ1' => ['Москва', 'Московская']
        ];
        return isset($cities[$key]) ? $cities[$key] : array($key, $key);
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
