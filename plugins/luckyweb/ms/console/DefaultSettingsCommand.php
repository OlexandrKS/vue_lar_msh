<?php
namespace LuckyWeb\MS\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
//require_once "plugins/luckyweb/ms/updates/settings_seeder.php";
use LuckyWeb\MS\Updates\SettingsSeeder;

class DefaultSettingsCommand extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'ms:defaults';

    /**
     * @var string The console command description.
     */
    protected $description = 'Generates default seetings';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $seeder = new SettingsSeeder();
        $seeder->run();
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
            //['flow_id', InputArgument::REQUIRED, 'Flow id.'],
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            //['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

    protected function debug($object) {
        $this->output->writeln(
            is_array($object) || is_object($object) ? print_r($object, true) : $object);
    }

}
