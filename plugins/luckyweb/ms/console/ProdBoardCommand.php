<?php namespace LuckyWeb\MS\Console;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use LuckyWeb\User\Models\User;
use Symfony\Component\Console\Helper\ProgressBar;

class ProdBoardCommand extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'ms:prodboardcommand';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $this->output->writeln('Start Export!');

        $fp = fopen(storage_path('temp/prod.csv'), 'w+');
        $dtVancouver = Carbon::create(2019, 3, 1, 0, 0, 0);
        $users = User::select('id', 'name', 'email', 'phone')->where('created_at', '>=', $dtVancouver)->get()->toArray();
        $bar = new ProgressBar($this->getOutput(), count($users));
        foreach ($users as $user) {
            $identity = 'mebelShara' . $user['id'];
            $responseToken = $this->clientToken($identity);
            if ($responseToken != null) {
                fputcsv($fp, $user);
            }
            $bar->advance();
        }
        fclose($fp);
        $this->output->writeln('');
        $this->output->writeln('End Export!');

    }

    protected function clientToken($identity)
    {
        $client = new Client;
        $response = $client->post('https://api.prodboard.com/api/clients.token', [
            'headers' => [
                'api_key' => config('prodboard.api_key'),
            ],
            'form_params' => ['identity' => $identity],
        ]);
        return json_decode($response->getBody(), true);
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
