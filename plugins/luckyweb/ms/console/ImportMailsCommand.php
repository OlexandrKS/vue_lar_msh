<?php
namespace LuckyWeb\MS\Console;

use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use LuckyWeb\MS\Classes\Imap\IMAPMail;
use LuckyWeb\MS\Classes\Imap\IMAPManager;
use LuckyWeb\MS\Models\Review;
use LuckyWeb\MS\Models\ReviewMessage;
use LuckyWeb\MS\Models\SalesManager;
use Symfony\Component\Console\Input\InputArgument;


class ImportMailsCommand extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'ms:import-mails';

    /**
     * @var string The console command description.
     */
    protected $description = 'Imports mails';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        switch ($this->argument('cmd')) {
            case 'reviews':
                $this->importReviewsMails();
                break;
        }
    }

    /**
     * Import reviews emails
     */
    protected function importReviewsMails()
    {
        $imap = IMAPManager::instance();
        while($mail = $imap->getFirstNew()) {
            if(!$this->getClientsMessage($mail)) {
                $this->getManagersMessage($mail);
            }
        }
    }

    /**
     * Try to get and save message from client
     * @param IMAPMail $mail
     * @return bool TRUE - processed message
     */
    protected function getClientsMessage($mail) {
        preg_match('/\[mebelshara\.ru отзыв #(\d+)\]/', $mail->getSubject(), $matches);
        $review = null;
        if(isset($matches[1])) { // Try to get by ID
            $review = Review::where('id', $matches[1])
                ->where('email', $mail->getFromAddress())
                ->first();
        }
        if(!$review) { // Get last by email
            $review = Review::where('email', $mail->getFromAddress())->orderBy('id', 'DESC')->first();
        }

        if($review) {
            try {
                $message = new ReviewMessage();
                $message->mail_id = $mail->getId();
                $message->text = $mail->getMessage();
                $message->created_at = new Carbon($mail->getDate());
                $message->review_id = $review->id;
                $message->direction = ReviewMessage::INPUT_ID;
                $message->save();

                $review->info_status_id = Review::INFO_STATUS_WAIT_ADMIN;
                if($review->status_id == Review::STATUS_APPROVED || $review->status_id == Review::STATUS_REJECTED) {
                    $review->status_id = Review::STATUS_PROCESSING;
                }
                $review->save();

                return true;
            }
            catch(Exception $e) {
                Log::info('ImportMailsCommand->getClientsMessage(): '.$e->getMessage().' - '.$e->getTraceAsString());
            }
        }

        return false;
    }

    /**
     * Try to get and save message from sales manager
     * @param IMAPMail $mail
     * @return bool TRUE - processed message
     */
    protected function getManagersMessage($mail) {
        preg_match('/\[mebelshara\.ru переписка #(\d+)\]/', $mail->getSubject(), $matches);
        $review = null;
        if(isset($matches[1])) { // Try to get by ID
            if(SalesManager::where('email', $mail->getFromAddress())->first()) {
                $review = Review::where('id', $matches[1])->first();
            }
        }

        if($review) {
            try {
                $message = new ReviewMessage();
                $message->mail_id = $mail->getId();
                $message->text = $mail->getMessage();
                $message->created_at = new Carbon($mail->getDate());
                $message->review_id = $review->id;
                $message->direction = ReviewMessage::FROM_TM_ID;
                $message->save();

                $review->info_status_id = Review::INFO_STATUS_RECEIVED_FROM_TM;
                if($review->status_id == Review::STATUS_APPROVED || $review->status_id == Review::STATUS_REJECTED) {
                    $review->status_id = Review::STATUS_PROCESSING;
                }
                $review->save();

                return true;
            }
            catch(Exception $e) {
                Log::info('ImportMailsCommand->getManagersMessage(): '.$e->getMessage().' - '.$e->getTraceAsString());
            }
        }

        return false;
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['cmd', InputArgument::REQUIRED, 'Command'],
            // available commands:
            // reviews - Import review mails
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            //['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

    protected function debug($object) {
        $this->output->writeln(
            is_array($object) || is_object($object) ? print_r($object, true) : $object);
    }

}
