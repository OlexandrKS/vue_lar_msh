<?php

namespace LuckyWeb\MS\Console;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Console\Command;
use Luckyweb\Ms\Models\FavoriteList;
use Luckyweb\Ms\Models\FavoriteListItem;
use LuckyWeb\MS\Models\Image;
use LuckyWeb\Sentry\Facades\Sentry;

class AbandonedCartCommand extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'ms:abandonedcartcommand';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $client = new Client;
        $now = Carbon::now();
        $from = $now->subHour(1)->toDateTimeString();
        $to = $now->addMinutes(5)->toDateTimeString();
        $list_carts = FavoriteList::whereBetween('updated_at', [$from, $to])->with(['user', 'items' => function ($q) {
            $q->orderBy('created_at', 'DESC');
        }])->get();

        foreach ($list_carts as $list_cart) {
            try {
                $form_data = [];
                $form_data['email'] = $list_cart->user->email;
                $form_data['name'] = $list_cart->user->name;
                $total_price = 0;
                if (count($list_cart->items) > 0) {
                    /** @var $item FavoriteListItem */
                    foreach ($list_cart->items as $item) {
                        $form_data['products'][] = [
                            'name' => $item->product->name,
                            'url' => url('/catalog/' . $item->product->category->slug . '/' . $item->product->slug),
                            'img' => $item->product->getPreviewImage(Image::ASSIGNMENT_PREVIEW_MOBILE)->getPreviewSource(),
                            'count' => $item->count,
                            'price' => number_format($item->product->getActivePrice(), 0, '', ' ') . ' &#8381;',
                        ];
                        $total_price += $item->count * $item->product->getActivePrice();
                    }
                    $form_data['total_price'] = number_format($total_price, 0, '', ' ') . ' &#8381;';

                    $client->post('https://events.sendpulse.com/events/id/e241eb6999f24d4cd7191234f659111f/7051313', [
                        RequestOptions::JSON => $form_data,
                    ]);
                }
            } catch (\Exception $exception) {
                Sentry::error($exception);
            }
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
