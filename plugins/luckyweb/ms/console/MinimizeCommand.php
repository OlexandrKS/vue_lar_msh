<?php
namespace LuckyWeb\MS\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use FTP;
use DB;
use Config;
use Symfony\Component\Yaml\Yaml;

use LuckyWeb\MS\Models\Settings;
use LuckyWeb\MS\Models\Image;
use LuckyWeb\MS\Models\Category;
use LuckyWeb\MS\Models\Product;

use Intervention\Image\ImageManagerStatic as IM;




class MinimizeCommand extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'ms:minimize';

    /**
     * @var string The console command description.
     */
    protected $description = 'Minimizes imported images';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $this->info("*** Minimizing images ***");

        // TODO move minimization process to workers or microservice
        ini_set("memory_limit","1024M");

        $images = Image::all();
        foreach ($images as $image) {
            $debug = $image->minimize();
            $this->debug($debug['file'].' --> '.$debug['status']);

        }

    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
            //['flow_id', InputArgument::REQUIRED, 'Flow id.'],
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            //['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

    protected function debug($object) {
        $this->output->writeln(
            is_array($object) || is_object($object) ? print_r($object, true) : $object);
    }

}
