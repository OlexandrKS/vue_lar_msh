<?php
namespace LuckyWeb\MS\Console;

use LuckyWeb\MS\Classes\Exchange\Exchange;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;


class ExchangeCommand extends Command
{

    /**
     * @var string The console command name.
     */
    protected $name = 'ms:1c';

    /**
     * @var string The console command description.
     */
    protected $description = '1C exchange calls';

    /**
     * Execute the console command.
     * @return void
     */

    public function handle()
    {
        $exchange = new Exchange();
        switch ($this->argument('cmd')) {
            case 'import':
                $exchange->import();
                break;
            case 'export':
                $exchange->export();
                break;
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['cmd', InputArgument::REQUIRED, 'Exchange commands.'],
            // available commands:
            // import - Import from 1C all available data
            // export - Export to 1C all necessary data
            // export-open - Export all open applications and related payments
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            //['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

    protected function debug($object) {
        $this->output->writeln(
            is_array($object) || is_object($object) ? print_r($object, true) : $object);
    }

}
