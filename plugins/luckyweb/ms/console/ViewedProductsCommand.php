<?php namespace LuckyWeb\MS\Console;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Console\Command;
use LuckyWeb\MS\Models\Image;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\ViewedProductsUser;
use LuckyWeb\User\Models\User;

class ViewedProductsCommand extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'ms:viewedproductscommand';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $yesterday = date("Y-m-d", strtotime('-1 days'));
        $viewed_product = ViewedProductsUser::whereDate('created_at', $yesterday)->get();
        $result = [];
        foreach ($viewed_product as $element) {
            $result[$element['user_id']][] = $element->product_id;
        }
        foreach ($result as $user_key => $item) {
            $user = User::find($user_key);
            if ($user) {
                $form_data = [];
                $form_data['email'] = $user->email;
                $form_data['name'] = $user->name;
                foreach ($item as $product_id) {
                    /** @var Product $product */
                    $product = Product::find($product_id);
                    if ($product) {
                        $form_data['products'][] = [
                            'name' => $product->name,
                            'url' => url('/catalog/' . $product->category->slug . '/' . $product->slug),
                            'img' => $product->getPreviewImage(Image::ASSIGNMENT_PREVIEW_MOBILE)->getPreviewSource(),
                            'price' => number_format($product->getActivePrice(), 0, '', ' ') . ' &#8381;',
                            'old_price' => (!$product->isWeekPromo()) ? '<p style="text-decoration: line-through">' . number_format((int)$product->getStrikedPrice(), 0, '', ' ') . ' &#8381;</p>' : '',
                            'discount' => (!$product->isWeekPromo()) ? '<p>Скидка на товар <span style="border: 1px solid red; border-radius: 50%; background: red; color: white; padding: 3px;">-' . (int)$product->getActiveDiscount() . '&#37;</span></p>' : ''
                        ];
                    }
                }
                $client = new Client;
                $response = $client->post('https://events.sendpulse.com/events/id/aa340983b813489489c6d462122cbe1f/7051313', [
                    RequestOptions::JSON => $form_data,
                ]);
                $response = json_decode($response->getBody(), true);
                if ($response['result'] == true) {
                    $viewed_product->each(function ($item) {
                        $item->delete();
                    });
                }
            }
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
