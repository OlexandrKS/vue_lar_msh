<?php
namespace LuckyWeb\MS\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use LuckyWeb\User\Models\Order;
use LuckyWeb\User\Models\OrderItem;
use Google\Cloud\BigQuery\BigQueryClient;



class TestCommand extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'ms:test';

    /**
     * @var string The console command description.
     */
    protected $description = 'Imports data from backoffice ftp';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $rows = [];
        foreach (Order::with('items')->get() as $order) {
            foreach ($order->items as $item) {
                $rows[] = [
                    'insertId' => $item->id,
                    'data' => [
                        'user_id' => $order->user_id, //	STRING	Значение должно совпадать со значением поля с идентификатором пользователя в источнике данных модели атрибуции
                        'transaction_id' => $order->external_id,  //	STRING	Идентификатор транзакции
                        'product_id' => $item->good_id,  //	STRING	Идентификатор товара
                        'product_price' => $item->amount,  //	FLOAT	Стоимость товара
                        'product_quantity' => $item->quantity, //	INTEGER	Количество товаров в заказе
                        'time' => $order->applied_at->toDateTimeString(),
                    ]
                ];
            }
        }
        //print_r($rows);
        $this->stream_row('mebelshara-201120', 'mebelshara', 'orders', $rows);
    }

    /**
     * Stream a row of data into your BigQuery table
     * Example:
     * ```
     * $data = [
     *     "field1" => "value1",
     *     "field2" => "value2",
     * ];
     * stream_row($projectId, $datasetId, $tableId, $data);
     * ```.
     *
     * @param string $projectId The Google project ID.
     * @param string $datasetId The BigQuery dataset ID.
     * @param string $tableId   The BigQuery table ID.
     * @param string $data      An associative array representing a row of data.
     * @param string $insertId  An optional unique ID to guarantee data consistency.
     */
    public function stream_row($projectId, $datasetId, $tableId, $rows, $insertId = null)
    {
        // instantiate the bigquery table service
        $bigQuery = new BigQueryClient([
            'projectId' => $projectId,
        ]);
        $dataset = $bigQuery->dataset($datasetId);
        $table = $dataset->table($tableId);

        $insertResponse = $table->insertRows($rows);
        if ($insertResponse->isSuccessful()) {
            print('Data streamed into BigQuery successfully' . PHP_EOL);
        } else {
            foreach ($insertResponse->failedRows() as $row) {
                foreach ($row['errors'] as $error) {
                    printf('%s: %s' . PHP_EOL, $error['reason'], $error['message']);
                }
            }
        }
    }



    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
            //['flow_id', InputArgument::REQUIRED, 'Flow id.'],
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            //['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

    protected function debug($object) {
        $this->output->writeln(
            is_array($object) || is_object($object) ? print_r($object, true) : $object);
    }

}
