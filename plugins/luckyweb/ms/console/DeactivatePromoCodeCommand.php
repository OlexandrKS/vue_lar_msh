<?php namespace LuckyWeb\MS\Console;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Console\Command;
use LuckyWeb\User\Models\PromoCodeEvent;

class DeactivatePromoCodeCommand extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'ms:deactivatepromocodecommand';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $now = Carbon::now();
        $this->startEvent($now->subDay(7));
    }

    protected function startEvent($date)
    {
        $promo_codes = PromoCodeEvent::query()->available()
            ->where('created_at', $date)
            ->get();
        /** @var PromoCodeEvent $promo_code */
        foreach ($promo_codes as $promo_code) {
            $user = $promo_code->user;
            $client = new Client;
            $client->post('https://events.sendpulse.com/events/id/95ebc99a0abc4b03a31b291b134f0a62/7051313', [
                RequestOptions::JSON => ['email' => $user->email, 'name' => $user->name],
            ]);
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
