<?php namespace LuckyWeb\MS\Console;

use Illuminate\Console\Command;
use LuckyWeb\MS\Models\ProductGroup;
use LuckyWeb\MS\Models\Review;
use LuckyWeb\MS\Models\VacancyCity;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Support\Facades\DB;


class UtilityCommand extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'ms:util';

    /**
     * @var string The console command description.
     */
    protected $description = 'Imports data from backoffice ftp';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        switch ($this->argument('cmd')) {
            case 'vac-cities':
                VacancyCity::seedFromShopCities();
                break;
            case 'change_likes':
                $this->changeLikes($this->argument('division'));
                break;
            case 'set_types_for_groups':
                // For single usage
                $this->setupGroupTypes();
                break;
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['cmd', InputArgument::REQUIRED, 'Test command'],
            ['division', InputArgument::OPTIONAL, 'How mach number division'],
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            //['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

    protected function debug($object) {
        $this->output->writeln(
            is_array($object) || is_object($object) ? print_r($object, true) : $object);
    }

    /**
     * @param int $division number division
     */
    protected function changeLikes($division = 2)
    {
        $division = !empty($division) ? $division : 2;
        Review::where('dislikes','>','0')
            ->update(['dislikes' => DB::raw("dislikes / $division")]);
    }

    /**
     * Add type 1 (for reviews) for all existing product groups
     */
    protected function setupGroupTypes()
    {
        ProductGroup::get()->each(function($item){
            $item->types()->attach(1);
        });
    }

}
