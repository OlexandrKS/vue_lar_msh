<?php namespace LuckyWeb\MS\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use LuckyWeb\User\Models\Order;
use LuckyWeb\MS\Models\LastExport;
use LuckyWeb\User\Models\OrderItem;

class AnalyticsCommand extends Command
{
    // TODO move to settings or env
    const GA_URL = 'https://www.google-analytics.com/collect';
    const GA_TID = 'UA-68299257-1';
    const GA_VER = 1;
    const GA_DH  = 'www.mebelshara.ru';
    const GA_DP  = '/cart';
    const GA_DT  = 'Корзина';
    const GA_NI  = '1';
    const GA_CU  = 'RUB';
    const GA_T  = 'event';
    const GA_PA = 'purchase';

    /**
     * @var string The console command name.
     */
    protected $name = 'ms:analytics';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * @var object Last export
     */
    protected $orderLastExport = null;

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        // get Analytics last exports
        $this->orderLastExport = LastExport::where([
            'export_type' => OrderItem::class,
            'export_into' => 'Analytics'
        ])->first();

        // get new orders
        $orders = Order::with(['user', 'items'])
            ->where('created_at', '>', $this->orderLastExport->exported_at)
            ->get();

        foreach ($orders as $order) {
            if ($this->parseTransactionShort($order->external_id) == 'ИМШ') {
                $this->refundTransaction($order);
            }
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

    public function refundTransaction($order)
    {
       $data = [
           'v' => self::GA_VER,               // Version.
           't' => self::GA_T,                 // Pageview hit type.
           'tid' => self::GA_TID,             // Tracking ID / Property ID.
           'cu' => self::GA_CU,               // Currency code.
           'cid' =>  $this->parseClientId($order->user->ga_session_id), // Client ID
           'uid' => $order->user->card_code,  // User ID
           'dh' => self::GA_DH,               // Document hostname.
           'dp' => self::GA_DP,               // Page.
           'dt' => self::GA_DT,               // Title.
           'ni' => self::GA_NI,               // Non-interaction parameter.
           'ti' => $this->parseTransactionId($order->external_id),     // Transaction ID. Required.
           'ta' => $this->parseTransactionShort($order->external_id),  // Affiliation.
           'tr' => $order->total_amount,      // Revenue.
           'tt' => 0.00,                      // Tax.
           'ts' => $order->delivery_amount,   // Shipping.
           // &tcc=SUMMER2013                 // Transaction coupon.
           'pa' => self::GA_PA,               // Product action (purchase). Required.
           'z' => Carbon::now()->format('YmdHis') . rand() // cache blocking
        ];
        $i = 1;
        foreach ($order->items as $item) {
            $temp = [
                'pr'.$i.'nm' => $item->name,
                'pr'.$i.'id' => $item->color_id . '.' . $item->good_id . '.',
                'pr'.$i.'pr' => $item->amount,
                'pr'.$i.'qt' => $item->quantity,
            ];
            $data = array_merge($data, $temp);
            $i++;
        }
        return $this->gateway($data);
    }

    /**
     * Формирование запроса в ga
     * @param string[] $data данные в запросе
     * @return $response
     */
    private function gateway($data)
    {
        $options = array(
            CURLOPT_HEADER      => true,
            CURLOPT_NOBODY      => true,
            CURLOPT_TIMEOUT     => 3,
            CURLOPT_USERAGENT   => ""
        );

        $url = self::GA_URL . '?' . http_build_query($data);
        $curl = curl_init($url);
        curl_setopt_array($curl, $options);
        $response = curl_exec($curl);

        if ($response !== FALSE) {
            $this->orderLastExport->exported_at = Carbon::now()->toDateTimeString();
            $this->orderLastExport->save();
        }

        $response = json_decode($response, true);
        curl_close($curl);
        return $response;
    }

    private function parseTransactionId($external_id)
    {
        return preg_match("/^.+\=(.+)\=.+$/", $external_id, $mathes) ? $mathes[1] : $external_id;
    }

    private function parseTransactionShort($external_id)
    {
        return preg_match("/^.+\=(.+)\-.+$/", $external_id, $mathes) ? $mathes[1] : $external_id;
    }

    private function parseClientId($ga_session_id) {
        return explode('_', $ga_session_id)[0];
    }
}
