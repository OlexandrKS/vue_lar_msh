<?php namespace LuckyWeb\MS;

use Carbon\Carbon;
use Exception;
use LuckyWeb\MS\Classes\EventListener;
use LuckyWeb\MS\Components\Installments;
use Luckyweb\Ms\Components\MenuConstructor;
use LuckyWeb\MS\Components\Page;
use System\Classes\PluginBase;

use App;
use Backend;

/**
 * MS Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * @var array
     */
    public $require = [
        'LuckyWeb.Importer',
    ];

    public static function isMobile()
    {
        // TODO get info from Illuminate request
        $useragent = $_SERVER['HTTP_USER_AGENT'];

        if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
            return true;
        }

        return false;
    }

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Мебель-Шара',
            'description' => 'Интернет-магазин Мебель-Шара',
            'author'      => 'LuckyWeb.pro',
            'icon'        => 'icon-shopping-cart'
        ];
    }

    public function register()
    {
        // Service provider
        App::register('LuckyWeb\MS\Classes\CLog\CLogServiceProvider');

        // Register alias
        $aliasLoader = \Illuminate\Foundation\AliasLoader::getInstance();
        $aliasLoader->alias('CL', 'LuckyWeb\MS\Facades\CLog'); // TODO 💩

        // Register console commands
        $this->registerConsoleCommand('ms.import', 'LuckyWeb\MS\Console\ImportCommand');
        $this->registerConsoleCommand('ms.defaults', 'LuckyWeb\MS\Console\DefaultSettingsCommand');
        $this->registerConsoleCommand('ms.minimize', 'LuckyWeb\MS\Console\MinimizeCommand');
        $this->registerConsoleCommand('ms.test', 'LuckyWeb\MS\Console\TestCommand');
        $this->registerConsoleCommand('ms.import-mails', 'LuckyWeb\MS\Console\ImportMailsCommand');
        $this->registerConsoleCommand('ms.1c', 'LuckyWeb\MS\Console\ExchangeCommand');
        $this->registerConsoleCommand('ms.util', 'LuckyWeb\MS\Console\UtilityCommand');
        // $this->registerConsoleCommand('ms.bigquery', 'LuckyWeb\MS\Console\BigqueryCommand');
        $this->registerConsoleCommand('ms.analytics', 'LuckyWeb\MS\Console\AnalyticsCommand');
        $this->registerConsoleCommand('ms.owox', 'LuckyWeb\MS\Console\OwoxCommand');
        $this->registerConsoleCommand('ms.prodboard', 'LuckyWeb\MS\Console\ProdBoardCommand');
        $this->registerConsoleCommand('ms.abandonedcartcommand', 'LuckyWeb\MS\Console\AbandonedCartCommand');
        $this->registerConsoleCommand('ms.viewedproductscommand', 'LuckyWeb\MS\Console\ViewedProductsCommand');
        $this->registerConsoleCommand('ms.deactivatepromocodecommand', 'LuckyWeb\MS\Console\DeactivatePromoCodeCommand');
    }

    public function registerSettings() {
        return [
            'settings' => [
                'label'       => 'Системные настройки сайта',
                'description' => 'Системные настройки сайта Мебель-Шара',
                'category'    => 'Настройки',
                'icon'        => 'icon-cogs',
                'class'       => 'LuckyWeb\MS\Models\Settings',
                'order'       => 500,
                'keywords'    => 'импорт',
                'permissions' => ['luckyweb.ms.system'],
            ],
            'adminsettings' => [
                'label'       => 'Настройки сайта',
                'description' => 'Настройки сайта Мебель-Шара',
                'category'    => 'Настройки',
                'icon'        => 'icon-cog',
                'class'       => 'LuckyWeb\MS\Models\AdminSettings',
                'order'       => 500,
                'keywords'    => 'админ',
                'permissions' => ['luckyweb.ms.system'],
            ]

        ];
    }

    public function registerComponents()
    {
        return [
//            'LuckyWeb\MS\Components\Payment' => 'payment',
            'LuckyWeb\MS\Components\ProductGrid' => 'productGrid',
            'LuckyWeb\MS\Components\Reviews' => 'reviews',
            'LuckyWeb\MS\Components\Faq' => 'faq',
            'LuckyWeb\MS\Components\PromoBoard' => 'promoboard',
            'LuckyWeb\MS\Components\PromoVideo' => 'promovideo',
            'LuckyWeb\MS\Components\ProductPassport' => 'productpassport',
            'LuckyWeb\MS\Components\ProductViewPhone' => 'productviewphone',
            'LuckyWeb\MS\Components\ProductReserve' => 'productreserve',
            'LuckyWeb\MS\Components\VideoPopup' => 'videopopup',
            'LuckyWeb\MS\Components\Vacancies' => 'vacancies',
            'LuckyWeb\MS\Components\EmployerRewards' => 'employerrewards',
            'LuckyWeb\MS\Components\Shares' => 'shares',
            'LuckyWeb\MS\Components\ProductRelated' => 'productrelated',
//            'LuckyWeb\MS\Components\LendingOrder' => 'lendingorder',
            'LuckyWeb\MS\Components\Documents' => 'documents',
            'LuckyWeb\MS\Components\MetaPopup' => 'metapopup',
            'LuckyWeb\MS\Components\CallbackRequest' => 'callbackrequest',
            'LuckyWeb\MS\Components\ProductTogetherCheaper' => 'producttogethercheaper',
            'LuckyWeb\MS\Components\PopupTogetherCheaper' => 'popuptogethercheaper',
            'LuckyWeb\MS\Components\ProductChooser' => 'productchooser',
//            'LuckyWeb\MS\Components\Cart' => 'cart',
//            'LuckyWeb\MS\Components\ProductRelatedCountToCart' => 'productrelatedcounttocart',
//            'LuckyWeb\MS\Components\QuickOrder' => 'quickorder',
            'LuckyWeb\MS\Components\PagePopups' => 'pagepopups',
//            'LuckyWeb\MS\Components\CreditPopup' => 'creditpopup',
//            'LuckyWeb\MS\Components\Credit' => 'credit',
            'LuckyWeb\MS\Components\OrderTimer' => 'ordertimer',
            Installments::class => 'installments',
//            MenuConstructor::class => 'menuconstructor',
            Page::class => 'Page',
            'LuckyWeb\MS\Components\SearchGrid' => 'searchGrid',
        ];
    }

    public function registerSchedule($schedule)
    {
        if (App::environment() == 'production') {
//            $schedule->command('ms:import all --minimize')->withoutOverlapping();
//            $schedule->command('ms:import all --minimize --force')->at('2:00')->withoutOverlapping();
            // $schedule->command('ms:bigquery')->hourly();
            $schedule->command('ms:analytics')->everyFiveMinutes();
            $schedule->command('ms:owox')->everyFiveMinutes();
            $schedule->command('admin:backup')->dailyAt('3:00');
            $schedule->command('ms:abandonedcartcommand')->everyFiveMinutes();
            $schedule->command('ms:viewedproductscommand')->dailyAt('11:00');
            $schedule->command('ms:deactivatepromocodecommand')->dailyAt('9:00');
        }
//        else {
//            $schedule->command('ms:import all --minimize')->everyFiveMinutes()->withoutOverlapping();
//        }
        $schedule->command('ms:import-mails reviews')->withoutOverlapping();
        $schedule->command('ms:1c import')->withoutOverlapping();
        $schedule->command('ms:1c export')->withoutOverlapping();
    }

    public function registerNavigation()
    {
        return [

            'seo' => [
                'label'       => 'SEO',
                'url'         => Backend::url('luckyweb/ms/pagemetas'),
                'icon'        => 'icon-dot-circle-o',
                'permissions' => ['luckyweb.ms.seo'],
                'order'       => 1,

                'sideMenu' => [
                    'pagemetas' => [
                        'label'       => 'Метаданные страниц',
                        'icon'        => 'icon-code',
                        'url'         => Backend::url('luckyweb/ms/pagemetas'),
                        'permissions' => ['luckyweb.ms.seo'],
                    ],
                ],
            ],
            'promoboard' => [
                'label'       => 'Промо страницы',
                'url'         => Backend::url('luckyweb/ms/promopages'),
                'icon'        => 'icon-gift',
                'permissions' => ['luckyweb.ms.*'],
                'order'       => 2,

                'sideMenu' => [
                    'promopages' => [
                        'label'       => 'Страницы',
                        'icon'        => 'icon-file-image-o',
                        'url'         => Backend::url('luckyweb/ms/promopages'),
                        'permissions' => ['luckyweb.ms.*'],
                    ],
                    'promoblocks' => [
                        'label'       => 'Блоки',
                        'icon'        => 'icon-picture-o',
                        'url'         => Backend::url('luckyweb/ms/promoblocks'),
                        'permissions' => ['luckyweb.ms.*'],
                    ]
                ],
            ],
            'reviews' => [
                'label'       => 'Отзывы и вопросы',
                'url'         => Backend::url('luckyweb/ms/reviews'),
                'icon'        => 'icon-bullhorn',
                'permissions' => ['luckyweb.ms.reviews'],
                'order'       => 3,

                'sideMenu' => [
                    'reviews' => [
                        'label'       => 'Отзывы',
                        'icon'        => 'icon-bullhorn',
                        'url'         => Backend::url('luckyweb/ms/reviews'),
                        'permissions' => ['luckyweb.ms.reviews'],
                    ],
                    'salesmanagers' => [
                        'label'       => 'Менеджеры',
                        'icon'        => 'icon-users',
                        'url'         => Backend::url('luckyweb/ms/salesmanagers'),
                        'permissions' => ['luckyweb.ms.reviews'],
                    ],
                    'faqs' => [
                        'label'       => 'Вопросы и ответы',
                        'icon'        => 'icon-question',
                        'url'         => Backend::url('luckyweb/ms/faqs'),
                        'permissions' => ['luckyweb.ms.reviews'],
                    ],
                    'shares' => [
                        'label'       => 'Акции',
                        'icon'        => 'icon-slideshare',
                        'url'         => Backend::url('luckyweb/ms/shares'),
                        'permissions' => ['luckyweb.ms.web'],
                    ],
                    'documents' => [
                        'label'       => 'Документы',
                        'icon'        => 'icon-file',
                        'url'         => Backend::url('luckyweb/ms/documents'),
                        'permissions' => ['luckyweb.ms.web'],
                    ],
                    'descriptionhints' => [
                        'label'       => 'Попапы описания',
                        'icon'        => 'icon-comment-o',
                        'url'         => Backend::url('luckyweb/ms/descriptionhints'),
                        'permissions' => ['luckyweb.ms.web'],
                    ]
                ],
            ],
            'products' => [
                'label'       => 'Товары',
                'url'         => Backend::url('luckyweb/ms/productpassports'),
                'icon'        => 'icon-shopping-cart',
                'permissions' => ['luckyweb.ms.products'],
                'order'       => 4,
                'sideMenu' => [
                    'productpassports' => [
                        'label'       => 'Паспорта товаров',
                        'icon'        => 'icon-book',
                        'url'         => Backend::url('luckyweb/ms/productpassports'),
                        'permissions' => ['luckyweb.ms.products'],
                    ],
                    'productgroups' => [
                        'label'       => 'Группы товаров',
                        'icon'        => 'icon-cubes',
                        'url'         => Backend::url('luckyweb/ms/productgroups'),
                        'permissions' => ['luckyweb.ms.products'],
                    ],
                ]
            ],
            'hr' => [
                'label'       => 'Персонал',
                'url'         => Backend::url('luckyweb/ms/vacancies'),
                'icon'        => 'icon-users',
                'permissions' => ['luckyweb.ms.hr'],
                'order'       => 5,

                'sideMenu' => [
                    'vacancies' => [
                        'label'       => 'Вакансии',
                        'icon'        => 'icon-user-plus',
                        'url'         => Backend::url('luckyweb/ms/vacancies'),
                        'permissions' => ['luckyweb.ms.hr'],
                    ],
                    'employerrewards' => [
                        'label'       => 'Медали',
                        'icon'        => 'icon-certificate',
                        'url'         => Backend::url('luckyweb/ms/employerrewards'),
                        'permissions' => ['luckyweb.ms.hr'],
                    ],
                    'vacancycities' => [
                        'label'       => 'Города',
                        'icon'        => 'icon-map-marker',
                        'url'         => Backend::url('luckyweb/ms/vacancycities'),
                        'permissions' => ['luckyweb.ms.hr'],
                    ]
                ],
            ],
            'pagepopup' => [
                'label'       => 'Попапы',
                'url'         => Backend::url('luckyweb/ms/pagepopup'),
                'icon'        => 'icon-calendar-check-o',
                'permissions' => ['luckyweb.ms.pagepopup'],
                'order'       => 6,

                'sideMenu' => [
                    'pagepopup' => [
                        'label'       => 'Попапы',
                        'icon'        => 'icon-calendar-check-o',
                        'url'         => Backend::url('luckyweb/ms/pagepopup'),
                        'permissions' => ['luckyweb.ms.pagepopup'],
                    ],
                    'pagepopuphtml' => [
                        'label'       => 'Попапы HTML',
                        'icon'        => 'icon-html5',
                        'url'         => Backend::url('luckyweb/ms/pagepopuphtml'),
                        'permissions' => ['luckyweb.ms.pagepopup'],
                    ],
                    'popuptogethercheaper' => [
                        'label'       => 'Попап Вместе дешевле',
                        'icon'        => 'icon-clone',
                        'url'         => Backend::url('luckyweb/ms/popuptogethercheaper/update/1'),
                        'permissions' => ['luckyweb.ms.pagepopup'],
                    ]

                ],
            ],

            'menu' => [
                'label' => 'Меню',
                'url' => Backend::url('luckyweb/ms/menuitem'),
                'icon' => 'icon-book',
                'permissions' => ['luckyweb.ms.menu'],
                'order' => 6,

                'sideMenu' => [
                    'menu' => [
                        'label' => 'Меню',
                        'url' => Backend::url('luckyweb/ms/menuitem'),
                        'icon' => 'icon-book',
                        'permissions' => ['luckyweb.ms.menu'],
                    ],
                    'submenucategories' => [
                        'label' => 'Категории меню',
                        'icon' => 'icon-sitemap',
                        'url' => Backend::url('luckyweb/ms/submenucategory'),
                        'permissions' => ['luckyweb.ms.menu'],
                    ],
                    'submenuitem' => [
                        'label' => 'Подпункты меню',
                        'icon' => 'icon-map-signs',
                        'url' => Backend::url('luckyweb/ms/submenuitem'),
                        'permissions' => ['luckyweb.ms.menu'],
                    ],
                ]
            ]

        ];
    }

    public function registerPermissions()
    {
        $tab = 'Управление сайтом Мебель Шара';
        $order = 0;
        return [
            'luckyweb.ms.web' => ['label' => 'Управление сайтом', 'tab' => $tab, 'order'=>$order++],
            'luckyweb.ms.reviews' => ['label' => 'Управление отзывами', 'tab' => $tab, 'order'=>$order++],
            'luckyweb.ms.hr' => ['label' => 'Управление персоналом', 'tab' => $tab, 'order'=>$order++],
            'luckyweb.ms.products' => ['label' => 'Управление товарами', 'tab' => $tab, 'order'=>$order++],
            'luckyweb.ms.seo' => ['label' => 'Настройки SEO', 'tab' => $tab, 'order'=>$order++],
            'luckyweb.ms.system' => ['label' => 'Системные настройки', 'tab' => $tab, 'order'=>$order++],
            'luckyweb.ms.pagepopup' => ['label' => 'Управление попапами', 'tab' => $tab, 'order'=>$order++],
        ];
    }

    public function boot()
    {
        EventListener::instance()->listenReviews();
        EventListener::instance()->listenQuestions();
        EventListener::instance()->listenReserves();
        EventListener::instance()->listenVacancyRequests();
        EventListener::instance()->listenOrders();
        EventListener::instance()->listenProdboard();
        // \Carbon\Carbon::setTestNow(new \Carbon\Carbon('2016-05-01'));

//        $this->app['Illuminate\Contracts\Http\Kernel']
//            ->prependMiddleware('LuckyWeb\MS\Http\Middlewares\BreadCrumbs');
    }

    public function registerMailTemplates()
    {
        return [
            'luckyweb.ms::mail.client.review_created' => 'Письмо клиенту об успешном создании отзыва',
            'luckyweb.ms::mail.client.review_answer_created' => 'Письмо клиенту с ответом администратора на отзыв',
        ];
    }

    public function registerMarkupTags()
    {
        return [
            'filters' => [
                'date2str' => function ($timeStr) {
                    if (empty($timeStr) || $timeStr == '0000-00-00')
                        return '';
                    try {
                        return Carbon::parse($timeStr)->format('d.m.Y');
                    } catch (Exception $e) {
                        return $timeStr;
                    }
                },
                'replaceLineEnds' => function ($str) {
                    return str_replace("\r\n", '<br/>', $str);
                },
                'timestamped' => function ($value) {
                    return $value . '?' . time();
                },
                'wasset' => function ($name) {
                    return wasset($name);
                }
            ]
        ];
    }
}
