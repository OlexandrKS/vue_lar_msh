<?php namespace LuckyWeb\MS\Models;

use Model;

/**
 * ReviewMessage Model
 */
class ReviewMessage extends Model
{
    use \October\Rain\Database\Traits\Validation;

    const INPUT_ID = 1;
    const OUTPUT_ID = 2;
    const FROM_TM_ID = 3;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_review_messages';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'review' => ['LuckyWeb\MS\Models\Review'],
        'backendUser' => ['Backend\Models\User'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public $rules = [
        'text' => 'required'
    ];

}