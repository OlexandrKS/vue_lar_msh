<?php namespace LuckyWeb\MS\Models;

use Carbon\Carbon;
use Exception;
use Model;

/**
 * VacancyRequest Model
 */
class VacancyRequest extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Purgeable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_vacancy_requests';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'vacancy_id',
        'city_id',
        'first_name',
        'last_name',
        'middle_name',
        'birth_date',
        'citizenship',
        'phone',
        'email',
        'education',
        'specialty',
        'experience',
        'additional_information',
        'is_accept'
    ];

    protected $purgeable = [
        'is_accept'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'city' => ['LuckyWeb\MS\Models\VacancyCity'],
        'vacancy' => ['LuckyWeb\MS\Models\Vacancy'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'photo' => 'System\Models\File',
        'resume' => 'System\Models\File',
    ];
    public $attachMany = [];

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = [
        'birth_date'
    ];

    public $rules = [
        'vacancy_id'  => 'required',
        'city_id'     => 'required',
        'first_name'  => 'required',
        'last_name'   => 'required',
        'birth_date'  => 'required|date',
        'citizenship' => 'required',
        'phone'       => ['required', 'regex:/^7[0-9]{10}$/'],
        'education'   => 'required',
        'is_accept'   => 'accepted'
    ];

    public $attributeNames = [
        'vacancy_id'  => '"Вакансия"',
        'city_id'     => '"Город"',
        'first_name'  => '"Имя"',
        'last_name'   => '"Фамилия"',
        'birth_date'  => '"Дата рождения"',
        'citizenship' => '"Гражданство"',
        'phone'       => '"Контактный телефон"',
        'education'   => '"Образование"',
    ];

    public $customMessages = [
        'accepted' => 'Вы должны подтверждить согласие на обработку Ваших персональных данных.',
    ];

    /**
     * Setter for birth_date field
     * @param $value
     */
    public function setBirthDateAttribute($value)
    {
        try {
            if(!empty($value)) {
                $this->attributes['birth_date'] = Carbon::parse($value)->format('Y-m-d');
            }
            else {
                $this->attributes['birth_date'] = null;
            }
        }
        catch(Exception $e) {
            $this->attributes['birth_date'] = null;
        }
    }
}
