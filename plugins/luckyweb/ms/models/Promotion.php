<?php namespace LuckyWeb\MS\Models;

use Model;
use DB;
use Carbon\Carbon;
use LuckyWeb\MS\Classes\Helper\Youtube;

/**
 * Promotion Model
 */
class Promotion extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_promotions';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    protected $dates = ['date'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'images' => ['LuckyWeb\MS\Models\Image'],
    ];
    public $belongsTo = [
        'category' => ['LuckyWeb\MS\Models\Category']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * Fills table(s) with imported data from _import_ tables
     */
    public static function updateImported() {
        DB::table((new static)->table)->truncate();
        Model::unguard();
        $items = DB::table('luckyweb_ms_import_promotions')->get();
        foreach ($items as $item) {
            $dateParts = explode('.', $item->date);
            static::create([
                'id' => $item->id,
                'name' => $item->name,
                'date' => '20'.$dateParts[2].'-'.$dateParts[1].'-'.$dateParts[0],
                'banner_title' => $item->banner1,
                'banner_subtitle' => $item->banner2,
                'category_id' => $item->category_id == '' ? null : $item->category_id,
                'video_title' => $item->video_title,
                'video_url' => $item->link_to_video,
            ]);
        }
        Model::reguard();
    }

    /**
     * Returns current promotion
     */
    public function scopeCurrent($query)
    {
        $now = Carbon::now();
        return $query
            ->whereRaw('YEAR(date) = '.$now->year)
            ->whereRaw('MONTH(date) = '.$now->month)
            ->orderBy('date', 'desc');
    }

    /**
     * Reruns mixed promotion
     */
    public function scopeMixedPromotions($query)
    {
        return $query->where(function($query){
            $query->where('category_id', 0)
                ->orWhereNull('category_id');
        });
    }

    /**
     * Creates Youtube helper object for current monthly video
     *
     * @return LuckyWeb\MS\Classes\Helper\Youtube
     */
    public function getMonthlyYoutbeVideo()
    {
        return new Youtube($this->video_url);
    }


}
