<?php namespace LuckyWeb\MS\Models;

use Model;

/**
 * ProductGroupType Model
 */
class ProductGroupType extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_product_group_types';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['name'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
