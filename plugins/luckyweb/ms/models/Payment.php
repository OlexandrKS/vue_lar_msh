<?php namespace LuckyWeb\MS\Models;

use App;
use Model;

/**
 * Payment Model
 */
class Payment extends Model
{

    const SBERBANK_API_PROD_URL = 'https://securepayments.sberbank.ru/payment/rest/';
    const SBERBANK_API_TEST_URL = 'https://3dsec.sberbank.ru/payment/rest/';

    const STATUS_REGISTER = 0;      // заказ зарегистрирован, но не оплачен;
    const STATUS_HOLDING = 1;       // предавторизованная сумма удержана (для двухстадийных платежей);
    const STATUS_PAID = 2;          // проведена полная авторизация суммы заказа;
    const STATUS_CANCELED = 3;      // авторизация отменена;
    const STATUS_RETURN = 4;        // по транзакции была проведена операция возврата;
    const STATUS_AUTH_CONTROL = 5;  // инициирована авторизация через сервер контроля доступа банка-эмитента;
    const STATUS_REJECTED = 6;      // авторизация отклонена.

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_payments';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * Регистрация заказа
     * Метод register.do
     * @param string $order_number номер заказа в магазине
     * @param integer $amount сумма заказа
     * @param string $return_url страница, на которую необходимо вернуть пользователя
     * @return string[]
     */
    function register($order_number, $amount, $return_url)
    {
        $data = array(
            'orderNumber'   => $order_number . "_" . time(),
            'amount'        => $amount,
            'returnUrl'     => $return_url,
        );
        $response = $this->gateway('register.do', $data);
        return $response;
    }

    /**
     * Статус заказа по orderId
     * Метод getOrderStatusExtended.do
     * @param string $orderId номер заказа в ПШ
     * @return string[]
     */
    public function getOrderStatusExtended()
    {
        $data = array('orderId' => $this->orderId);
        $response = $this->gateway('getOrderStatusExtended.do', $data);
        return $response;
    }

    /**
     * Формирование запроса в платежный шлюз и парсинг JSON-ответа
     * @param string $method метод запроса в ПШ
     * @param string[] $data данные в запросе
     * @return string[]
     */
    private function gateway($method, $data)
    {
        $data['userName'] = env('SBERBANK_API_USERNAME');
        $data['password'] = env('SBERBANK_API_PASSWORD');

        if (App::environment() == 'production') {
            $url = self::SBERBANK_API_PROD_URL;
        } else {
            $url = self::SBERBANK_API_TEST_URL;
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . $method,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_POSTFIELDS => http_build_query($data, '', '&'),
            CURLOPT_SSLVERSION => 6
        ));
        $response = curl_exec($curl);
        $response = json_decode($response, true);
        curl_close($curl);
        return $response;
    }
}
