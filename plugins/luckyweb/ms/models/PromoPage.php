<?php namespace LuckyWeb\MS\Models;

use Model;

/**
 * PromoPage Model
 */
class PromoPage extends Model
{
    const STATUS_NEW = 1;
    const STATUS_PUBLISHED = 2;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_promo_pages';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['name','status_id'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'blocks' => [
            'LuckyWeb\MS\Models\PromoBlock',
            'table'=>'luckyweb_ms_promo_pages_blocks',
            'key' => 'page_id',
            'otherKey' => 'block_id',
            'scope' => 'sorted',
            'pivot' => ['sort_order']
        ],
        'blocks_published' => [
            'LuckyWeb\MS\Models\PromoBlock',
            'table'=>'luckyweb_ms_promo_pages_blocks',
            'key' => 'page_id',
            'otherKey' => 'block_id',
            'scope' => 'sortedPublished',
            'pivot' => ['sort_order']
        ]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @var array Statuses
     */
    public static $statuses = [
        1 => ['NEW', 'Новая'],
        2 => ['PUBLISHED', 'Опубликована'],
    ];

    public function beforeSave() {
        $entity = static::find($this->id);
        if($entity && $this->status_id != $entity->status_id && $this->status_id == static::STATUS_PUBLISHED) {
            static::where('status_id', static::STATUS_PUBLISHED)->update([
                'status_id' => static::STATUS_NEW
            ]);
        }
    }

    public function scopePublished($query)
    {
        return $query->where('status_id', static::STATUS_PUBLISHED);
    }

    /**
     * Return status label for controller list
     * @param $status
     * @return array|mixed
     */
    public static function getStatusLabels($status)
    {
        return array_key_exists($status, static::$statuses) ? static::$statuses[$status]: ['NULL', 'NULL'];
    }

    public function getStatusIdOptions($keyValue = null)
    {
        $statuses = [];
        foreach (static::$statuses as $key => $value) {
            $statuses[$key] = $value[1];
        }
        return $statuses;
    }
}