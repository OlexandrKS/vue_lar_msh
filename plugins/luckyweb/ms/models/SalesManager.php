<?php namespace LuckyWeb\MS\Models;

use Model;

/**
 * SalesManager Model
 */
class SalesManager extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_sales_managers';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public $rules = [
        'name' => 'required',
        'email' => 'required|email',
    ];

    public $customMessages = [
        'required' => 'Обязательное поле',
        'email' => 'Неверный формат email',
    ];

}