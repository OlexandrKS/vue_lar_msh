<?php namespace LuckyWeb\MS\Models;

use Model;
use DB;
use Str;
use Log;


/**
 * SubCategory Model
 */
class SubCategory extends Model
{
    use \October\Rain\Database\Traits\Sluggable;
    use \LuckyWeb\MS\Classes\Traits\OwnedSite;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_sub_categories';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    protected $slugs = ['slug' => 'name', 'slug_auto' => 'name'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'products' => ['LuckyWeb\MS\Models\Product'],
    ];
    public $belongsTo = [
        'category' => ['LuckyWeb\MS\Models\Category'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public static function findBySlug($slug)
    {
        $category = static::whereSlug($slug)->first();
        if (!$category) {
            $category = static::where('slug_auto', '=', $slug)->first();
        }
        return $category;
    }

    /**
     * Published scrope. Reserved for further use
     */
    public function scopePublished($query)
    {
        return $query->orderBy('order'); //->whereActive(true);
    }

    /**
     * Fills table(s) with imported data from _import_ tables
     */
    public static function updateImported()
    {
        static::truncate();
        Model::unguard();

        $query = DB::table('luckyweb_ms_import_sub_categories');
        static::ownedSiteExtendQuery($query);
        $items = $query->get();

        foreach ($items as $item) {
            $sub_category = static::create([
                'id' => $item->id,
                'category_id' => $item->category_id,
                'name' => $item->name,
                'order' => $item->order,
            ]);

            if ($item->slug!='') {
                // handle non-unique slugs

                $genSlug = function ($slug, $cnt) {
                    return preg_replace('/\s+/', '-', $slug).($cnt==0 ? '' : '-'.$cnt);
                };

                $cnt = 0;
                while (static::where('slug', '=', $genSlug($item->slug, $cnt))->first()) {
                    $cnt++;
                }

                // overload slug
                $sub_category->slug = $genSlug($item->slug, $cnt);
            }
            // Append parent category slug to generated slug
            $sub_category->slug = $sub_category->category->slug.'-'.$sub_category->slug;
            $sub_category->save();
        }
        DB::table((new static)->table)
            ->whereIn('id', DB::table((new ImportGoodType)->table)->distinct('sub_category_id')->lists('sub_category_id'))
            ->update(['active' => true]);

        Model::reguard();
    }

    /**
     * Returns array of subcategory pages urls
     *
     * @return array
     */
    public static function getSitemapUrls()
    {
        return static::published()->get()->map(function ($subcategory) {
            return [
                'url' => '/catalog/subcategory/'.$subcategory->category->slug.'/'.$subcategory->slug,
                'lastmod' => $subcategory->updated_at->toDateString(),
                'priority' => '0.5'
            ];
        })->toArray();
    }

    /**
     * Breakes name into multiple html lines (inserts <br />)
     *
     * @return string
     */
    public function getBreakedName()
    {
        if (count(explode(' ', $this->name))==2) {
            return preg_replace('/[ ]+/iu', '<br />', $this->name);
        }
        return preg_replace('/ и/iu', '<br />и', $this->name);
    }
}
