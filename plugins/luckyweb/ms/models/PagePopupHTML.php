<?php namespace LuckyWeb\MS\Models;

use Model;

/**
 * PagePopupHTML Model
 */
class PagePopupHTML extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_page_popups';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'popup_name',
        'html',
        'url_hash',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
