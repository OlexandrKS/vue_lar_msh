<?php namespace LuckyWeb\MS\Models;

use Model;
use DB;

/**
 * Specification Model
 */
class Specification extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_specifications';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'id',
        'name',
        'sort',
        'popup',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * Fills table(s) with imported data from _import_ tables
     */
    public static function updateImported()
    {
        DB::table((new static)->table)->truncate();

        $query = DB::table('luckyweb_ms_import_specifications');
        $items = $query->get();

        foreach ($items as $item) {
            static::create([
                'id' => $item->spec_id,
                'name' => $item->spec_name,
                'sort' => $item->sort,
                'popup' => $item->popup,
            ]);
        }
    }
}
