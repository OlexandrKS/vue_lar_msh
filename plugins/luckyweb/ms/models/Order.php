<?php

namespace Luckyweb\Ms\Models;

use October\Rain\Database\Model;

/**
 * Order Model
 */
class Order extends Model
{
    const PAY_METHOD_PAYMENT = 'payment';
    const PAY_METHOD_CREDIT = 'credit';

    // credit statuses
    const CREDIT_STATUS_NEW = 'new';
    const CREDIT_STATUS_SENT = 'sent';

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_orders';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'user_id',
        'name',
        'total_price',
        'status',
        'payment_method',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'items' => ['LuckyWeb\Ms\Models\OrderItem'],
    ];
    public $belongsTo = [
        'user' => ['LuckyWeb\User\Models\User'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
