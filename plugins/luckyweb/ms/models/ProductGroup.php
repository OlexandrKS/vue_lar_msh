<?php namespace LuckyWeb\MS\Models;

use Luckyweb\Customcontent\Models\Page;
use October\Rain\Database\Model;

/**
 * ProductGroup Model
 */
class ProductGroup extends Model
{
    const TYPE_REVIEWS = 1;
    const TYPE_CONSTRUCTOR = 2;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_product_groups';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['name', 'page_id'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'page' => ['LuckyWeb\Customcontent\Models\Page'],
    ];
    public $belongsToMany = [
        'products' => [
            'LuckyWeb\MS\Models\Product',
            'table' => 'luckyweb_ms_products_groups',
            'key' => 'group_id',
            'otherKey' => 'product_id'
        ],
        'types' => [
            'LuckyWeb\MS\Models\ProductGroupType',
            'table' => 'luckyweb_ms_types_groups',
            'key' => 'group_id',
            'otherKey' => 'type_id'
        ],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * Groups for reviews
     * @param $query
     * @return mixed
     */
    public function scopeForReviews($query) {
        return $query->whereHas('types', function($query){
            $query->where('id', static::TYPE_REVIEWS);
        });
    }

    /**
     * Groups for constructor
     * @param $query
     * @return mixed
     */
    public function scopeForConstructor($query) {
        return $query->whereHas('types', function($query){
            $query->where('id', static::TYPE_CONSTRUCTOR);
        });
    }

    /**
     * For controller dropdown
     * @return array
     */
    public function getPageIdOptions()
    {
        return collect(Page::lists('title', 'id'))->prepend('Нет', 0)->toArray();
    }
}