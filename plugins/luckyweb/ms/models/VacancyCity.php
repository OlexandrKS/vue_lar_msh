<?php namespace LuckyWeb\MS\Models;

use Carbon\Carbon;
use Model;

/**
 * VacancyCity Model
 */
class VacancyCity extends Model
{
    use \October\Rain\Database\Traits\Sluggable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_vacancy_cities';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    protected $slugs = ['slug' => 'name'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * Seed vacancy cities from shop cities
     */
    public static function seedFromShopCities()
    {
        $inserts = [];
        ShopCity::get()->each(function($item) use(&$inserts) {
           if(static::where('slug', $item->slug)->count() == 0) {
               $inserts[] = [
                   'name' => $item->name,
                   'slug' => $item->slug,
                   'created_at' => Carbon::now(),
                   'updated_at' => Carbon::now()
               ];
           }
        });

        static::insert($inserts);
    }
}
