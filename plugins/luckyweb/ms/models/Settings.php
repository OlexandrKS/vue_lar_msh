<?php namespace LuckyWeb\MS\Models;

use Model;

/**
 * Settings Model
 */
class Settings extends Model
{

    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'luckyweb_ms_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';

}
