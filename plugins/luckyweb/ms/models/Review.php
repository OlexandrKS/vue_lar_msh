<?php

namespace LuckyWeb\MS\Models;

use October\Rain\Database\Model;

/**
 * Review Model
 *
 * @mixin \Eloquent
 */
class Review extends Model
{
    use \October\Rain\Database\Traits\Purgeable;
    
    const STATUS_NEW = 1;
    const STATUS_APPROVED = 2;
    const STATUS_REJECTED = 3;
    const STATUS_PROCESSING = 4;

    const INFO_STATUS_WAIT_ADMIN = 1;
    const INFO_STATUS_WAIT_CLIENT = 2;
    const INFO_STATUS_CLOSED = 3;
    const INFO_STATUS_SENT_TO_TM = 4;
    const INFO_STATUS_RECEIVED_FROM_TM = 5;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_reviews';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'city_id',
        'is_client',
        'contract_number',
        'text',
        'product_slug'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'messages' => ['LuckyWeb\MS\Models\ReviewMessage'],
    ];
    public $belongsTo = [
        'city' => ['LuckyWeb\MS\Models\ShopCity'],
        'product_group' => ['LuckyWeb\MS\Models\ProductGroup']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @var array Statuses
     */
    public static $statuses = [
        1 => ['NEW', 'Новый'],
        2 => ['APPROVED', 'Опубликован'],
        3 => ['REJECTED', 'Отклонен'],
        4 => ['PROCESSING', 'Обрабатывается']
    ];

    /**
     * @var array of info statuses
     */
    public static $infoStatuses = [
        1 => ['WAIT_ADMIN', 'Ожидает ответ администратора'],
        2 => ['WAIT_CLIENT', 'Ожидает ответ клиента'],
        3 => ['CLOSED', 'Закрыт'],
        4 => ['SENT_TO_TM', 'Отправлено ТМу'],
        5 => ['RECEIVED_FROM_TM', 'Получен ответ от ТМа']
    ];

    protected $purgeable = [
        'product_title'
    ];

    public function beforeDelete() {
        $this->messages->each(function($item){
            $item->delete();
        });
    }

    public function beforeSave() {
        $review = static::find($this->id);
        if($review && $review->status_id != $this->status_id) {
            // Review status changed to approved or rejected
            if($this->status_id == Review::STATUS_APPROVED) {
                if($last = $this->messages->last()) { // Add publication line
                    $last->is_publication = true;
                    $last->save();
                }
            }
            if($this->status_id == Review::STATUS_APPROVED || $this->status_id == Review::STATUS_REJECTED) {
                $this->info_status_id = Review::INFO_STATUS_CLOSED;
            }
        }

        if (empty($this->product_slug)) {
            $this->product_slug = null;
        }
        if (empty($this->product_group_id)) {
            $this->product_group_id = null;
        }
    }
    
    /**
     * Return status label for controller list
     * @param $status
     * @return array|mixed
     */
    public static function getStatusLabels($status)
    {
        return array_key_exists($status, static::$statuses) ? static::$statuses[$status]: ['NULL', 'NULL'];
    }

    /**
     * Return info status label for controller list
     * @param $status
     * @return array|mixed
     */
    public static function getInfoStatusLabels($status)
    {
        return array_key_exists($status, static::$infoStatuses) ? static::$infoStatuses[$status]: ['NULL', 'NULL'];
    }
    
    /**
     * For controller dropdown
     * @return array
     */
    public function getStatusIdOptions()
    {
        $result = [];
        foreach (static::$statuses as $key => $value){
            $result[$key] = $value[1];
        }

        return $result;
    }

    /**
     * For controller dropdown
     * @return array
     */
    public function getProductSlugOptions()
    {
        $result = [];
        Product::published()
            ->orderBy('name')
            ->get(['id', 'slug', 'name', 'subname'])
            ->each(function($item) use(&$result){
                $result[$item->slug] = $item->name.' ('.$item->subname.')';
            });
        return $result;
    }

    /**
     * For controller dropdown
     * @return mixed
     */
    public function getProductGroupIdOptions()
    {
        $values = ProductGroup::lists('name', 'id');
        return $values;
    }
}
