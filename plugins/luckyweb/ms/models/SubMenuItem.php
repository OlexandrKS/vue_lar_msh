<?php namespace Luckyweb\Ms\Models;

use Illuminate\Support\Collection;
use Model;

/**
 * SubMenuItem Model
 */
class SubMenuItem extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_sub_menu_items';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'sub_menu_category' => ['LuckyWeb\MS\Models\SubMenuCategory', 'key' => 'menu_category_id']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public static function getTypeLabels($type)
    {
        $menu = SubMenuCategory::select('id', 'name')
            ->where('id', $type)
            ->get();
        $s = new Collection($menu->toArray()[0]);
        return $s->values()->toArray();
    }

    public function getSubMenu()
    {
        $menu = SubMenuCategory::select('id', 'name')->get();
        $menu = $menu->pluck('name', 'id');
        return $menu;
    }

    /**
     * @return array
     */
    public function  getMenuCategoryIdOptions()
    {
        return $this->getSubMenu();
    }
}
