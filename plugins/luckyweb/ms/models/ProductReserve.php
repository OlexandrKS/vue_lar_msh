<?php namespace LuckyWeb\MS\Models;

use Model;

/**
 * ProductReserve Model
 */
class ProductReserve extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_product_reserves';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'phone',
        'city_id',
        'shop_id',
        'time_to_call',
        'message',
        'product_id'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'product' => ['LuckyWeb\MS\Models\Product'],
        'city' => ['LuckyWeb\MS\Models\ShopCity'],
        'shop' => ['LuckyWeb\MS\Models\Shop'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
