<?php namespace Luckyweb\Ms\Models;

use Model;

/**
 * OrderItem Model
 */
class OrderItem extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_order_items';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'order_id',
        'product_id',
        'name',
        'quantity',
        'total_price'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'order' => ['LuckyWeb\Ms\Models\Order'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
