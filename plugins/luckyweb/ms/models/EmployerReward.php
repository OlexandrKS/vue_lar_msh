<?php namespace LuckyWeb\MS\Models;

use Model;

/**
 * EmployerRewards Model
 */
class EmployerReward extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_employer_rewards';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'year',
        'src'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
