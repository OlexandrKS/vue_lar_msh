<?php namespace LuckyWeb\MS\Models;

use Illuminate\Support\Facades\Log;
use Model;

/**
 * Faq Model
 */
class Faq extends Model
{
    const STATUS_DISABLED = 1;
    const STATUS_ENABLED = 2;
    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_faqs';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'status_id',
        'question',
        'answer',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @var array Statuses
     */
    public static $statuses = [
        1 => ['DISABLED', 'Скрыт'],
        2 => ['ENABLED', 'Опубликован'],
    ];

    /**
     * Return status label for controller list
     * @param $status
     * @return array|mixed
     */
    public static function getStatusLabels($status)
    {
        return array_key_exists($status, static::$statuses) ? static::$statuses[$status]: ['NULL', 'NULL'];
    }

    public function getStatusIdOptions($keyValue = null)
    {
        $statuses = [];
        foreach (static::$statuses as $key => $value) {
            $statuses[$key] = $value[1];
        }
        return $statuses;
    }

    public function scopeEnabled($query)
    {
        return $query->where('status_id', static::STATUS_ENABLED);
    }
}