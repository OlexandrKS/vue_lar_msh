<?php

namespace LuckyWeb\Ms\Models;

use Carbon\Carbon;
use Model;
use Illuminate\Support\Facades\DB;
use  LuckyWeb\Ms\Models\SeoPage;
use October\Rain\Database\Collection;

class SeoPageLinks extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_seo_page_links';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['slug', 'page_id', 'position'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'seoPages' => [
            'LuckyWeb\MS\Models\SeoPage',
            'key' => 'page_id'
        ]
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @return array
     */
    static public function generateRandomIds()
    {
        $tmp = [];

        for ($i = 0; $i < 6; $i++) {
            $tmp [] = rand(1, SeoPage::count());
        }

        return $tmp;
    }

    /**
     * @param string $slug
     * @return Collection
     */
    static public function getReLinks(string $slug)
    {
        $data = [];
        $pages = SeoPageLinks::where('slug', $slug)
            ->with('seoPages')
            ->get();

        if ($pages->count()) {
            return $pages->pluck('seoPages');
        }

        $randIds = self::generateRandomIds();
        $newPages = SeoPage::whereIn('id', $randIds)->get();
        $i = 0;

        foreach ($newPages as $page) {
            ++$i;
            $data[] = [
                'slug' => $slug,
                'page_id' => $page->id,
                'position' => $i,
            ];
        }

        DB::table('luckyweb_ms_seo_page_links')->insert($data);

        return $newPages;
    }

}
