<?php namespace LuckyWeb\MS\Models;

use Model;

/**
 * Document Model
 */
class Document extends Model
{
    // Types
    const TYPE_IMAGE = 1;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_documents';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'type_id',
        'code'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'image' => 'System\Models\File'
    ];
    public $attachMany = [];

    public static $types = [
        1 => 'Изображение'
    ];

    public static function getTypeLabels($value)
    {
        return array_get(static::$types, $value, null);
    }

    public function getTypeIdOptions($keyValue = null)
    {
        return static::$types;
    }

    // Scopes

    public function scopeOfType($query, $typeId)
    {
        return $query->where('type_id', $typeId);
    }

    public function beforeCreate()
    {
        if(empty($this->code)) {
            $this->code = substr(md5(uniqid(rand(), true)),0,8);
        }
    }
}
