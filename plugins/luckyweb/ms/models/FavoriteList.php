<?php

namespace Luckyweb\Ms\Models;

use LuckyWeb\User\Facades\Auth;
use October\Rain\Database\Model;

/**
 * FavoriteList Model
 *
 * @mixin \Eloquent
 */
class FavoriteList extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_favorite_lists';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'items' =>['LuckyWeb\MS\Models\FavoriteListItem', 'key' => 'list_id'],
    ];
    public $belongsTo = [
        'user' =>['LuckyWeb\User\Models\User', 'key' => 'user_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public static function removeFavList($list){
        if(Auth::getUser()) {
            FavoriteListItem::where('list_id', $list)->delete();
            FavoriteList::find($list)->delete();

            return FavoriteList::where('user_id', Auth::getUser()->id)->get();
        }
        return null;
    }

    public static function getDefaultListId(){
        if(Auth::getUser()) {
            $list = FavoriteList::where('user_id', Auth::getUser()->id)
                ->orderBy('id', 'asc')
                ->first();

            return $list ? $list->id : null;
        }
    }

    public static function getDefaultList(){
        return Auth::getUser() ? FavoriteList::getDefaultListId() : null;
    }

    public static function getLists(){
        if(Auth::getUser())
        return FavoriteList::where('user_id', Auth::getUser()->id)
            ->orderBy('id', 'asc')
            ->get();
    }

    public static function updatedAt($list){
        if(Auth::getUser()) {
            $el = FavoriteList::find($list);
            $tmp = $el->list_name;
            $el->list_name = 'update';
            $el->save();
            $el->list_name = $tmp;
            $el->save();
        }
    }

    public function getItemsCount(){
        $list = $this;
        $sum = 0;
        foreach ($list->items as $item) {
            $sum += $item->count;
        }
        return $sum;
    }
}
