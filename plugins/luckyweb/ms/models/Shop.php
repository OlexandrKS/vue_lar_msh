<?php namespace LuckyWeb\MS\Models;

use Model;
use DB;

/**
 * Shop Model
 */
class Shop extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_shops';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'region' => ['LuckyWeb\MS\Models\ShopRegion'],
        'city' => ['LuckyWeb\MS\Models\ShopRegion'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public static function updateImported()
    {
        $records = ImportShop::orderBy('id')->get();
        DB::table((new Shop)->table)->truncate();

        foreach ($records as $r) {
            $region = ShopRegion::whereName($r->region)->first();
            if (!$region) {
                $region = new ShopRegion;
                $region['name'] = $r->region;
                $region->save();
            }
            $city = ShopCity::whereName($r->city)->first();
            if (!$city) {
                $city = new ShopCity;
                $city->name = $r->city;
                $city->region = $region;
                $city->save();
            }
            $shop = new Shop;
            $shop->region = $region;
            $shop->city = $city;

            //print_r($r->longlat);
            $r->longlat = preg_replace('/\s+/', '', $r->longlat);
            preg_match_all('/\(([0-9\.]+)\)/iu', $r->longlat, $matches);
            //$this->debug($matches);
            //print_r($matches);
            try {
                $shop->latitude = $matches[1][0];
                $shop->longitude = $matches[1][1];
            }
            catch (\Exception $e) {
                //print_r("Error in longlat for ".$r->mall_name);
                $shop->latitude = '';
                $shop->longitude = '';
            }

            $shop->number = $r->id;
            $shop->map_number = $r->map_number;
            $shop->mall_name = $r->mall_name;
            $shop->street_address = $r->street_address;
            $shop->mode_1 = $r->mode_1;
            $shop->mode_2 = $r->mode_2;
            $shop->phone = $r->phone;
            $shop->discount_center = $r->discount_center;

            $shop->save();
        }
    }

}
