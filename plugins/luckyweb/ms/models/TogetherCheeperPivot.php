<?php

namespace LuckyWeb\MS\Models;

use DB;
use Carbon\Carbon;
use October\Rain\Database\Model;

/**
 * TogetherCheeperPivot Model
 *
 * @mixin \Eloquent
 */
class TogetherCheeperPivot extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_together_cheeper_pivot';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'form_good_id',
        'form_package_id',
        'supp_good_id',
        'supp_package_id',
        'supp_category_id',
        'supp_price_lower_b',
        'saving',
        'the_best',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * Fills table(s) with imported data from _import_ tables
     */
    public static function updateImported()
    {
        DB::table((new static)->table)->truncate();

        $query = DB::table('luckyweb_ms_import_together_cheeper');
        $items = $query->get();

        $now = Carbon::now()->toDateTimeString();
        $rows = [];

        foreach ($items as $item) {

            $product = Product::where('good_id', $item->supp_good_id)
                ->orWhere('package_id', $item->supp_package_id)
                ->first();

            $rows[] = [
                'form_good_id' => $item->form_good_id,
                'form_package_id' => $item->form_package_id,
                'supp_good_id' => $item->supp_good_id,
                'supp_package_id' => $item->supp_package_id,
                'supp_category_id' => $product ? $product->category_id : null,
                'supp_price_lower_b' => $product ? $product->price_lower_b : null,
                'saving' => $item->saving,
                'the_best' => $item->the_best,
                'created_at' => $now,
                'updated_at' => $now,
            ];

            unset($product);

            if (count($rows) >= 1000) {
                DB::table((new static)->table)->insert($rows);
                $rows = [];
            }
        }
        DB::table((new static)->table)->insert($rows);
    }

    /**
     * Return Product instance of the support product
     * @return Product
     */
    public function getSupportProduct()
    {
        return Product::where('good_id', $this->supp_good_id)
            ->orWhere('package_id', $this->supp_package_id)->first();
    }

    /**
     * Return Product instance of the form product
     * @return Product
     */
    public function getFormProduct()
    {
        return Product::where('good_id', $this->form_good_id)
            ->orWhere('package_id', $this->form_package_id)->first();
    }
}
