<?php namespace LuckyWeb\MS\Models;

use Model;

/**
 * ExchangeExport Model
 */
class ExchangeExport extends Model
{
    const STATUS_PREPARE = 1;
    const STATUS_READY_TO_IMPORT = 2;
    const STATUS_IMPORTED = 3;

    const STATUS_ERROR = 20;
    const STATUS_ERROR_DATA_PARSE = 21;

    /**
     * @var string The database connection
     */
    protected $connection = 'exchange';

    /**
     * @var string The database table used by the model.
     */
    public $table = 'exchange_from_front';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'status_id',
        'entity_id',
        'type_id',
        'front_user_id',
        '1c_user_id',
        'data',
        'exported_at',
        'imported_at'
    ];

    protected $dates = [
        'exported_at',
        'imported_at',
    ];

    protected $jsonable = [
        'data'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
