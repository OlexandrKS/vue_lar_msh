<?php

namespace LuckyWeb\MS\Models;

use App\Backend\Entities\Constructor\Page;
use DB;
use October\Rain\Database\Model;

/**
 * SiteGoodType Model
 * @property int $type_id
 * @property string $type_name
 * @property string $plural
 * @property int $width
 * @property int $height
 * @property-read Page|null $constructor_page
 *
 * @mixin \Eloquent
 */
class SiteGoodType extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_site_good_types';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'type_id',
        'type_name',
        'plural',
        'width',
        'height',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'products' => [
            'LuckyWeb\MS\Models\Product',
            'key' => 'site_type_id',
            'otherKey' => 'type_id'
        ]
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    private $page = null;
    private $isLoadedPage = false;

    /**
     * Fills table(s) with imported data from _import_ tables
     */
    public static function updateImported()
    {
        DB::table((new static)->table)->truncate();

        $query = DB::table('luckyweb_ms_import_site_good_types');
        $items = $query->get();

        $fields = [
            'type_id',
            'type_name',
            'plural',
            'width',
            'height',
        ];

        foreach ($items as $item) {
            $attrs = [];

            foreach ($fields as $field) {
                $attrs[$field] = $item->$field;
            }

            static::create($attrs);
        }
    }

    /**
     * @return Page|null
     * @throws \Exception
     */
    public function getConstructorPageAttribute()
    {
        $cacheKey = "page_for_site_good_type_{$this->type_id}";

        if (cache()->store()->has($cacheKey)) {
            return cache()->store()->get($cacheKey);
        }

        $page = Page::query()
            ->where('properties->category', $this->type_id)
            ->first();

        // cache()->store()->put($cacheKey, $page, 60);

        return $page;
    }

    /**
     * @param $value
     * @throws \Exception
     */
    public function setConstructorPageAttribute($value)
    {
        $cacheKey = "page_for_site_good_type_{$this->type_id}";

        // cache()->store()->put($cacheKey, $value, 60);
    }
}
