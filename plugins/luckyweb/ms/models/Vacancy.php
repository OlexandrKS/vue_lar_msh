<?php namespace LuckyWeb\MS\Models;

use Model;

/**
 * Vacancy Model
 */
class Vacancy extends Model
{
    const STATUS_NEW = 1;
    const STATUS_ACTIVATED = 2;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_vacancies';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'duties',
        'conditions',
        'demands',
        'status_id'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'cities' => [
            'LuckyWeb\MS\Models\VacancyCity',
            'table'=>'luckyweb_ms_vacancies_cities',
            'otherKey' => 'city_id'
        ],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @var array Statuses
     */
    public static $statuses = [
        1 => ['NEW', 'Новый'],
        2 => ['ACTIVATED', 'Опубликован'],
    ];

    public function scopeActivated($query) {
        return $query->where('status_id', static::STATUS_ACTIVATED);
    }

    /**
     * Return status label for controller list
     * @param $status
     * @return array|mixed
     */
    public static function getStatusLabels($status)
    {
        return array_key_exists($status, static::$statuses) ? static::$statuses[$status]: ['NULL', 'NULL'];
    }

    /**
     * For controller dropdown
     * @return array
     */
    public function getStatusIdOptions()
    {
        $result = [];
        foreach (static::$statuses as $key => $value){
            $result[$key] = $value[1];
        }

        return $result;
    }

    /**
     * For controller dropdown
     * @return array
     */
    public function getCityIdOptions()
    {
        $result = [];
        ShopCity::orderBy('name')
            ->get(['id', 'name'])
            ->each(function($item) use(&$result){
                $result[$item->id] = $item->name;
            });

        return $result;
    }
}
