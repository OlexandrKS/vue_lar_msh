<?php namespace Luckyweb\Ms\Models;

use Model;
use Illuminate\Support\Facades\Cookie;
use LuckyWeb\User\Facades\Auth;
use October\Rain\Support\Collection;

/**
 * FavoriteListItem Model
 */
class FavoriteListItem extends Model
{
    const FAVORITE_TIME_LIFE = 60*24*30;
    const COOKIE_NAME = 'productList';
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_favorite_list_items';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'favorite_list' =>['LuckyWeb\MS\Models\FavoriteList', 'key' => 'list_id'],
        'product' =>['LuckyWeb\MS\Models\Product', 'key' => 'product_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * Add product from cookie list to db table - favlist
     */
    public static function fillFromCookie(){
        $cookie_fav_list = Cookie::get(self::COOKIE_NAME, []);
        if(!empty($cookie_fav_list))
        {
            foreach($cookie_fav_list as $item)
            {
                $fitem = new FavoriteListItem;
                $fitem->product_id = $item['product_id'];
                $fitem->list_id = Auth::getUser()->fav_list->first()->id;
                $fitem->count = $item['count'];
                $fitem->save();

            }
            Cookie::queue('productList', [], 1);
        }
    }

    /**
     * Return products from cookie list
     * @return collection
     */
    public static function getCookie(){
        $temp = Cookie::get(self::COOKIE_NAME, []);
        return new Collection($temp);
    }

    /**
     * Return products from cookie list after deleting
     * @param $temp list from cookie
     * @param $id for delete
     * @return collection
     */
    public static function removeItemFromCookie($temp, $id){
        if ( $temp->contains('product_id', $id) )
            $temp->forget(
                $temp->search(
                    $temp->where('product_id', $id)
                        ->first()
                )
            );
        return $temp;
    }

    public static function getSupportIdFromCookie($temp, $id){
        if ( $temp->contains('product_id', $id) ) {
            $temp = $temp->where('product_id', $id)
                ->first();
            return $temp['support_id'];
        }
        return 0;
    }

    public static function getCountFavList($temp, $user){
        if($user)
            return FavoriteListItem::join('luckyweb_ms_favorite_lists as fl', 'list_id', '=', 'fl.id')
                ->join('luckyweb_ms_products as products', 'product_id', '=', 'products.id')
                ->where('user_id', $user->id)
                //->select('products.*')
                //->select('count')
                ->sum('count');
        else
            return $temp->sum('count');
    }

    public static function getCountDefaultFavList($temp, $user){
        if($user) {
            $list = FavoriteList::getDefaultListId();
            return FavoriteListItem::join('luckyweb_ms_favorite_lists as fl', 'list_id', '=', 'fl.id')
                ->join('luckyweb_ms_products as products', 'product_id', '=', 'products.id')
                ->where('user_id', $user->id)
                ->where('fl.id', $list)
//                ->select('products.*')
//                ->count();
                ->sum('count');
        }
        else
            //return count($temp);
            return $temp->sum('count');
    }

    public static function addProductToFavList($temp, $id, $list, $count = 1, $supp_id=null){
        /*закоментував добавлення всіх елементів товару, для того, щоб зробити - разом дешевше*/
//        $product = Product::where('id', $id)->first();
//        $pkg_product = $product->getPackageProducts();
//
//        if (count($pkg_product)>0)
//        {
//            $ids = $pkg_product->pluck('id')->toArray();
//        }
//        else
            $ids = [$id];

        if (Auth::getUser()) {
            foreach($ids as $adder) {
                $f_item = FavoriteListItem::where('product_id', $adder)
                    ->where('list_id', $list)
                    ->first();
                if (!$f_item) {
                    $f_item = new FavoriteListItem;
                    $f_item->product_id = $adder;
                    $f_item->list_id = $list;
                }
                //if exist -> update count
                $f_item->count += 1;
                $f_item->support_id = $supp_id;
                $f_item->save();
                FavoriteList::updatedAt($list);/*list update update_at*/
            }
        } else {
            foreach($ids as $adder) {
                if ($temp->contains('product_id', $adder)) {
                    // product count++
                    $count += $temp->where('product_id', $adder)
                        ->first()['count'];
                    //if exist in cookie -> remove
                    $temp->forget(
                        $temp->search(
                            $temp->where('product_id', $adder)
                                ->first()
                        )
                    );
                }
                //and add new item
                $temp->push(['product_id' => $adder, 'count' => $count, 'support_id' => $supp_id]);
            }
            Cookie::queue(self::COOKIE_NAME, $temp, self::FAVORITE_TIME_LIFE);
        }
    }

    public static function removeProductFromFavList($temp, $id, $list){
        if(Auth::getUser()) {
            FavoriteListItem::where('product_id', $id)
                ->where('list_id', $list)
                ->delete();
            FavoriteList::updatedAt($list);/*list update update_at*/
        }
        else {
            $temp = self::removeItemFromCookie($temp, $id);
            Cookie::queue(self::COOKIE_NAME, $temp, self::FAVORITE_TIME_LIFE);
        }
    }

    public static function getProductFromFavList($temp, $list = null, $field = 'product_id', $fields = ['*'])
    {
        if(Auth::getUser()) {
            if (empty($list)) {
                return Product::whereIn('id', $temp->pluck($field))
                    ->get();
            } else
                return Product::join('luckyweb_ms_favorite_list_items as fli', 'luckyweb_ms_products.id', '=', 'fli.product_id')
                    ->join('luckyweb_ms_favorite_lists as fl', 'fli.list_id', '=', 'fl.id')
                    ->where('fl.id', $list)
                    ->select($fields)
                    ->get();
        }
        else
        {
            return collect($temp)->pluck($field);
        }
    }

    public static function getProductCountsFromFavList($temp, $list = null, $is_product = false, $fields = null)
    {
        if (empty($list)) {
            $temp = collect($temp);
        }
        else
            $temp = FavoriteListItem::join('luckyweb_ms_favorite_lists as fl', 'list_id', '=', 'fl.id')
                ->join('luckyweb_ms_products as products', 'product_id', '=', 'products.id')
                ->where('fl.id', $list)
                ->select('products.id as product_id', 'count')
                ->get();

        if (is_null($fields))
            return  $is_product == false ? $temp->pluck('count', 'product_id') : Product::whereIn('id', $temp->pluck('count', 'product_id') )->get();
        else
            return  $is_product == false ? $temp->pluck($fields) : Product::whereIn('id', $temp->pluck($fields) )->get();
    }

    /**
     * Return count products in fav list
     * @param $list - num of fav_list
     * @return integer
     */
    public static function getProductCountsFromProduct($temp, $list)
    {
        $products = FavoriteListItem::getProductCountsFromFavList($temp, $list, true, ['product_id']);
        $counts = [];
        $x = FavoriteListItem::getProductCountsFromFavList($temp,$list);
        $count = $x->values()->all();
        $x = FavoriteListItem::getProductCountsFromFavList($temp,$list, false, ['product_id']);
        foreach ($products as $item)
            array_push($counts, $count[ array_search( $item->id, $x->values()->all()) ]);

        return $counts;
    }

    public static function updateProductCount($temp, $id, $count, $list){
        $user = Auth::getUser();
        if(empty($user)) {
            $supp_id = self::getSupportIdFromCookie($temp, $id);
            $temp = self::removeItemFromCookie($temp,$id);
            $temp->push(['product_id' => $id, 'count' => $count, 'support_id' => $supp_id ]);
            Cookie::queue(self::COOKIE_NAME, $temp, self::FAVORITE_TIME_LIFE);
            return $id.'good'.$count.'  '.$list;
        }
        else{
            FavoriteListItem::where('product_id', $id)
                ->where('list_id', $list)
                ->update(['count' => $count]);
        }
    }

    /**
     * Return flag if product has cheaper product
     * @param $id - id of product
     * @param $list - id of fav list
     * @return boolean
     */
    public static function isCheaper($id, $list){
        $product = Product::where('id',$id)->first();
        $supports = $product->getSupportProducts();
        $in_list = Product::join('luckyweb_ms_favorite_list_items as fli', 'luckyweb_ms_products.id', '=', 'fli.product_id')
            ->where('fli.list_id', $list)
            ->select('luckyweb_ms_products.*')
            ->get()
            ->toArray();
        $list = collect($in_list)->pluck('id');

        foreach ($supports as $item)
            if($list->contains($item->getSupportProduct()->id))
                return true;
        return false;
    }

    /**
     * Return true if product has support product in fav list
     * @param $supp_id - id of product
     * @param $list - id of fav list
     * @return boolean
     */
    public static function hasSupportProduct($temp, $supp_id, $list){
        $user = Auth::getUser();
        if(empty($user)) {
                if ( $temp->contains('product_id', $supp_id) )
                return true;
            return false;
        }
        else {
            $in_list = Product::join('luckyweb_ms_favorite_list_items as fli', 'luckyweb_ms_products.id', '=', 'fli.product_id')
                ->where('fli.list_id', $list)
                ->where('luckyweb_ms_products.id', $supp_id)
                ->select('luckyweb_ms_products.*')
                ->get();
            if (count($in_list))
                return true;
            return false;
        }
    }

    public static function getCountProduct($temp, $id, $list)
    {
        $user = Auth::getUser();
        if(empty($user)) {
            return $temp->pluck('count', 'product_id')[$id];
        }
        else{
            $temp = FavoriteListItem::where('product_id', $id)
                    ->where('list_id', $list)
                    ->select('count')
                    ->first()->toArray();
            return $temp['count'];
        }
    }

    public static function getSmalestCountValue($prod, $supp){
        return $prod > $supp ? $supp : $prod;
    }

    public static function getSupportsArray($temp, $list){
        $user = Auth::getUser();
        $counts = [];
        if(empty($user))
            $cart_products = $temp;
        else
            $cart_products = FavoriteListItem::join('luckyweb_ms_favorite_lists as fl', 'list_id', '=', 'fl.id')
                ->where('fl.id', $list)
                ->get();

            foreach($cart_products as $item)
            {
                if(FavoriteListItem::hasSupportProduct($temp, $item['support_id'], $list))
                    array_push($counts, [$item['product_id'] => FavoriteListItem::getSmalestCountValue(FavoriteListItem::getCountProduct($temp, $item['product_id'], $list), FavoriteListItem::getCountProduct($temp, $item['support_id'], $list))]);
                else
                    array_push($counts, [$item['product_id'] => 0]);
            }


       return collect($counts)->collapse();
    }

    public static function ClearCart(){
        $user = Auth::getUser();
        if($user) {
            $list = FavoriteList::getDefaultListId();
            return FavoriteListItem::join('luckyweb_ms_favorite_lists as fl', 'list_id', '=', 'fl.id')
                ->join('luckyweb_ms_products as products', 'product_id', '=', 'products.id')
                ->where('user_id', $user->id)
                ->where('fl.id', $list)
                ->delete();
        }
        else
            return Cookie::queue(self::COOKIE_NAME, [], self::FAVORITE_TIME_LIFE);
    }
}
