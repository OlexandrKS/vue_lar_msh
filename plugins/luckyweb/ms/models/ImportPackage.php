<?php namespace LuckyWeb\MS\Models;

use Model;

/**
 * ImportPackage Model
 */
class ImportPackage extends Model
{
    use \LuckyWeb\MS\Classes\Traits\OwnedSite;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_import_packages';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'good_type' => ['LuckyWeb\MS\Models\ImportGoodType', 'key' => 'good_type_id'],
        'description_type' => ['LuckyWeb\MS\Models\ImportDescriptionType', 'key' => 'description_type_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}
