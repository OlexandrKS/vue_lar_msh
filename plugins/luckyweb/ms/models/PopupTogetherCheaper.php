<?php namespace LuckyWeb\MS\Models;


use October\Rain\Database\Model;

/**
 * PagePopup Model
 *
 * @var integer timeout
 * @var string devices
 * @var boolean active
 */
class PopupTogetherCheaper extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_together_cheaper_popup';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'timeout',
        'active'
    ];

    protected $jsonable = ['devices'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /** @var array devices */
    public static $devices = [
        'mobile' => 'Мобильные',
        'tablet' => 'Планшеты',
        'pc' => 'ПК'
    ];

    public function getDevicesOptions()
    {
        $result = [];
        foreach (static::$devices as $key => $value) {
            $result[$key] = $value;
        }
        return $result;
    }
}
