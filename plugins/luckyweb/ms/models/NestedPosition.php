<?php namespace LuckyWeb\MS\Models;

use Model;
use DB;

/**
 * NestedPosition Model
 */
class NestedPosition extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_nested_positions';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'package_product_id',
        'good_product_id',
        'position_row',
        'position_column',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'package' => ['LuckyWeb\MS\Models\Product', 'key' => 'package_product_id'],
        'good' => ['LuckyWeb\MS\Models\Product', 'key' => 'good_product_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * Fills table(s) with imported data from _import_ tables
     */
    public static function updateImported()
    {
        DB::table((new static)->table)->truncate();

        $query = DB::table('luckyweb_ms_import_spec_positions');
        $items = $query->get();

        foreach ($items as $item) {
            static::create([
                'package_product_id' => Product::renderProductId($item->package_color_id, null, $item->package_id),
                'good_product_id' => Product::renderProductId($item->good_color_id, $item->good_id, null),
                'position_row' => (integer)ceil($item->position1 / 1000),
                'position_column' => (integer)($item->position1 % 1000),
            ]);
        }

    }
}
