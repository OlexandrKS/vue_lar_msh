<?php namespace LuckyWeb\MS\Models;


use October\Rain\Database\Model;

/**
 * ViewedProductsUser Model
 *
 * @property string product_id
 * @property integer user_id
 */
class ViewedProductsUser extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_viewed_products_user';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['id'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    protected $jsonable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [
        'product' => [
            'LuckyWeb\MS\Models\Product',
            'key' => 'id',
            'otherKey' => 'product_id'
        ]
    ];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
//        'image' => 'System\Models\File'
    ];
    public $attachMany = [];
}
