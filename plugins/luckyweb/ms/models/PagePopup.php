<?php namespace LuckyWeb\MS\Models;


use LuckyWeb\User\Models\PromoCode;
use October\Rain\Database\Model;
use System\Models\File;

/**
 * PagePopup Model
 *
 * @property integer $id
 * @property string $popup_name
 * @property integer $type
 * @property string $float_header
 * @property string $left_button_id
 * @property string $left_button_text
 * @property string $left_button_color
 * @property string $left_button_link
 * @property string $right_button_id
 * @property string $right_button_text
 * @property string $right_button_color
 * @property int $timeout
 * @property string $message
 * @property string $image_url
 * @property string $type_button
 * @property string $variant_footer_popup
 * @property string $text_subscribe
 * @property string $modal_size
 * @property int $promo_code
 *
 * @property-read File $image
 */
class PagePopup extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_page_popups';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'popup_name',
        'type',
        'conversions',
//        'url',
        'utm_source',
        'utm_medium',
        'utm_campaign',
        'utm_content',
        'float_header',
        'left_button_id',
        'left_button_text',
        'left_button_color',
        'left_button_link',
        'right_button_id',
        'right_button_text',
        'right_button_color',
        'timeout',
        'message',
        'html',
        'image_url',
        'type_button',
        'page_show',
        'devices',
        'source_traffic',
        'variant_footer_popup',
        'text_subscribe',
        'active',
        'promo_code'
    ];

    public $rules = [
        'popup_name' => 'unique',

    ];

    public $customMessages = [
        'unique' => 'This attribute unique',
    ];

    protected $jsonable = ['devices', 'source_traffic'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [

    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'image' => 'System\Models\File'
    ];
    public $attachMany = [];

    /**
     * @var array Types
     */
    public static $types = [
        1 => ['left', 'Слева'],
        2 => ['right', 'Справа'],
        3 => ['center', 'Центр'],
        4 => ['float', 'Поплавок'],
    ];

    /**
     * @var array Color
     */
    public static $colors = [
        '#ed4f20' => ['#ed4f20', 'Оранжевый'],
        '#55a53e' => ['#55a53e', 'Зеленый'],
        '#767676' => ['#767676', 'Серый'],
    ];

    /** @var array PageShow */
    public static $page_show = [
        'all_page' => 'Показывать на всех страницах',
        'this_page' => 'Показывать только на этих страницах',
        'not_this_page' => 'Показывать на всех страницах, кроме'
    ];

    /** @var array devices */
    public static $devices = [
        'mobile' => 'Мобильные',
        'tablet' => 'Планшеты',
        'pc' => 'ПК'
    ];

    /** @var array source_traffic */
    public static $source_traffic = [
        'advertising' => 'Реклама',
        'organic' => 'Органический',
        'direct_hit' => 'Прямой заход'
    ];

    /** @var array variant_footer_popup */
    public static $variant_footer_popup = [
        'button' => 'Кнопки',
        'subscribe' => 'Подписка',
        'not_footer' => 'Без футера'
    ];

    /** @var array modal_size */
    public static $modal_size = [
        'sm' => 'Маленький',
        'md' => 'Средний',
        'lg' => 'Большой',
    ];

    /** @var array type_button */
    public static $type_button = [
        'button' => 'Кнопки',
        'link' => 'Ссылки',
    ];


    /**
     * @return \October\Rain\Database\Relations\HasMany
     */
    public function urls()
    {
        return $this->hasMany(PagePopupUrl::class, 'page_popup_id');
    }


    public function getTypeOptions()
    {
        $result = [];
        foreach (static::$types as $key => $value) {
            $result[$key] = $value[1];
        }
        return $result;
    }

    public function getLeftButtonColorOptions()
    {
        return $this->getColorArray();
    }

    public function getRightButtonColorOptions()
    {
        return $this->getColorArray();
    }

    protected function getColorArray()
    {
        $result = [];
        foreach (static::$colors as $key => $value) {
            $result[$key] = $value[1];
        }
        return $result;
    }

    public function getPageShowOptions()
    {
        return $this->generateOptions(static::$page_show);
    }

    public function getDevicesOptions()
    {
        return $this->generateOptions(static::$devices);
    }

    public function getSourceTrafficOptions()
    {
        return $this->generateOptions(static::$source_traffic);
    }

    public function getVariantFooterPopupOptions()
    {
        return $this->generateOptions(static::$variant_footer_popup);
    }

    public function getModalSizeOptions()
    {
        return $this->generateOptions(static::$modal_size);
    }

    public function getTypeButtonOptions()
    {
        return $this->generateOptions(static::$type_button);
    }

    public function getPromoCodeOptions()
    {
        $promo_code = PromoCode::where('status', true)
            ->where('start_at', '<=', date("Y-m-d"))
            ->where('expired_at', '>=', date("Y-m-d"))
            ->get()->toArray();
        $promo_code = array_combine(array_column($promo_code, 'id'), array_map(function ($item) {
            return $item['name'] . ' | ' . $item['code'] . ' | Сумма скидки - ' . $item['amount'];
        }, $promo_code));
        return array_merge(['0' => '-- Выберете промокод --'], $promo_code);
    }


    public function filterFields($fields, $context = null)
    {
        if ($fields->{'variant_footer_popup'}->value == 'button') {
            $fields->{'text_subscribe'}->hidden = true;
            $fields->{'promo_code'}->hidden = true;
        } elseif ($fields->{'variant_footer_popup'}->value == 'subscribe') {
            $fields->{'type_button'}->hidden = true;
            $fields->{'right_button_id'}->hidden = true;
            $fields->{'left_button_id'}->hidden = true;
            $fields->{'right_button_color'}->hidden = true;
            $fields->{'left_button_color'}->hidden = true;
            $fields->{'right_button_text'}->hidden = true;
            $fields->{'left_button_text'}->hidden = true;
            $fields->{'left_button_link'}->hidden = true;
        } elseif ($fields->{'variant_footer_popup'}->value == 'not_footer') {
            $fields->{'type_button'}->hidden = true;
            $fields->{'right_button_id'}->hidden = true;
            $fields->{'left_button_id'}->hidden = true;
            $fields->{'right_button_color'}->hidden = true;
            $fields->{'left_button_color'}->hidden = true;
            $fields->{'right_button_text'}->hidden = true;
            $fields->{'left_button_text'}->hidden = true;
            $fields->{'left_button_link'}->hidden = true;
            $fields->{'text_subscribe'}->hidden = true;
        }
    }

    public function afterSave()
    {
        if ($_POST['PagePopup']['pattern']) {
            foreach ($_POST['PagePopup']['pattern'] as $item) {

                PagePopupUrl::where('url', '<>', $item)->where('page_popup_id', '=', $this->attributes['id'])->delete();
                $find_popup_url = PagePopupUrl::where('url', '=', $item)
                    ->where('page_popup_id', $this->attributes['id'])->first();
                if (!$find_popup_url) {
                    $popup_url = new PagePopupUrl();
                    $popup_url->url = $item;
                    $popup_url->page_popup_id = $this->attributes['id'];
                    $popup_url->save();
                }
            }
        }
    }

    protected function generateOptions($array)
    {
        $result = [];
        foreach ($array as $key => $value) {
            $result[$key] = $value;
        }
        return $result;
    }
}
