<?php namespace LuckyWeb\MS\Models;

use Illuminate\Support\Facades\DB;
use Model;

/**
 * Furnishers Model
 */
class Furnisher extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_furnishers';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['id', 'name'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * Fills table(s) with imported data from _import_ tables
     */
    public static function updateImported()
    {
        DB::table((new static)->table)->truncate();

        $query = DB::table('luckyweb_ms_import_furnishers');
        $items = $query->get();

        foreach ($items as $item) {
            static::create([
                'id' => $item->id,
                'name' => $item->name,
            ]);
        }
    }
}
