<?php namespace LuckyWeb\MS\Models;

use Model;
use DB;
use Carbon\Carbon;

/**
 * SpecificationValue Model
 */
class SpecificationValue extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_specification_values';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'id',
        'name',
        'sort',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * Fills table(s) with imported data from _import_ tables
     */
    public static function updateImported()
    {
        DB::table((new static)->table)->truncate();

        $query = DB::table('luckyweb_ms_import_specification_values');
        $items = $query->get();

        foreach ($items as $item) {
            static::create([
                'id' => $item->value_id,
                'name' => $item->value_name,
                'sort' => $item->sort,
            ]);
        }

        $now = Carbon::now()->toDateTimeString();

        $query = DB::table('luckyweb_ms_import_specification_values_categories');
        $items = $query->get();
        $rows = [];
        foreach ($items as $item) {
            $rows[] = [
                'category_id' => $item->category_id,
                'specification_id' => $item->spec_id,
                'value_id' => $item->value_id,
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }
        DB::table('luckyweb_ms_specif_values_cats')->truncate();
        DB::table('luckyweb_ms_specif_values_cats')->insert($rows);

        $query = DB::table('luckyweb_ms_import_specification_values_sets');
        $items = $query->get();
        $rows = [];
        foreach ($items as $item) {
            $rows[] = [
                'value_id' => $item->value_id,
                'category_id' => $item->category_id,
                'set_id' => $item->set_id,
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }
        DB::table('luckyweb_ms_specif_values_sets')->truncate();
        DB::table('luckyweb_ms_specif_values_sets')->insert($rows);
    }
}
