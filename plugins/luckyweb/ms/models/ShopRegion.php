<?php namespace LuckyWeb\MS\Models;

use Model;

/**
 * ShopRegion Model
 */
class ShopRegion extends Model
{
    use \October\Rain\Database\Traits\Sluggable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_shop_regions';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    protected $slugs = ['slug' => 'name'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'shops' => ['LuckyWeb\MS\Models\Shop', 'key' => 'region_id'],
        'cities' => ['LuckyWeb\MS\Models\ShopCity', 'key' => 'region_id'],
    ];

    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public static function findBySlug($slug) {
        return static::whereSlug($slug)->first();
    }

}
