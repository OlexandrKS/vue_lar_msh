<?php

namespace Luckyweb\Ms\Models;

use October\Rain\Database\Model;

/**
 * MenuItem Model
 *
 * @mixin \Eloquent
 */
class MenuItem extends Model
{
    const TYPE_PRODUCTS = 1;
    const TYPE_POPUP_IMAGE = 2;

    const ORIENTATION_HORIZONTAL = 1;
    const ORIENTATION_VERTICAL = 2;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_menu_items';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];
    protected $jsonable = ['right_side', 'bottom_side'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'sub_menu_category' => ['LuckyWeb\MS\Models\SubMenuCategory', 'key' => 'menu_id'],
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'popup_image' => 'System\Models\File',
//        'icon' => 'System\Models\File',
    ];
    public $attachMany = [

    ];

    /**
     * @var array $types
     */
    public static $types = [
        1 => ['PRODUCTS', 'Товары'],
        2 => ['POPUP_IMAGE', 'Изображение'],
    ];

    /**
     * @var array $orientations
     */
    public static $orientations = [
        1 => ['HORIZONTAL', 'Горизонтальное'],
        2 => ['VERTICAL', 'Вертикальное'],
    ];

    /**
     * Return type label for controller list
     * @param string $type
     * @return array|mixed
     */
    public static function getTypeLabels($type)
    {
        return array_key_exists($type, static::$types) ? static::$types[$type] : ['NULL', 'NULL'];
    }

    /**
     * Return type label for controller list
     * @param string $orientation
     * @return array|mixed
     */
    public static function getOrientationLabels($orientation)
    {
        return array_key_exists($orientation, static::$orientations) ? static::$orientations[$orientation] : ['NULL', 'NULL'];
    }

    /**
     * @return array
     */
    public function getTypeIdOptions()
    {
        $types = [];
        foreach (static::$types as $key => $value) {
            $types[$key] = $value[1];
        }
        return $types;
    }

    /**
     * @return array
     */
    public function getOrientationIdOptions()
    {
        $orientations = [];
        foreach (static::$orientations as $key => $value) {
            $orientations[$key] = $value[1];
        }
        return $orientations;
    }

    public function getProduct()
    {
        $types = Product::select('id', 'name')->get();
        $types = $types->pluck('name', 'id');
        return $types;
    }

    /**
     * @return array
     */
    public function getProductOptions()
    {
        return $this->getProduct();
    }

    /**
     * @return array
     */
    public function getColumnByNum($num)
    {
        $columns =
            SubMenuCategory::select()
                ->where('column_num', $num)
                ->where('menu_id', $this->id)
                ->get();
        return $columns;
    }

    public function isActiveMenuItem()
    {
        $sub_item = SubMenuCategory::where('menu_id', $this->id)
            ->where('url', '/' . request()->path())
            ->first();
        if ($sub_item) return true;
        return false;
    }
}
