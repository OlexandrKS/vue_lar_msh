<?php namespace LuckyWeb\MS\Models;

use Model;

/**
 * ShopCity Model
 */
class ShopCity extends Model
{
    use \October\Rain\Database\Traits\Sluggable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_shop_cities';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    protected $slugs = ['slug' => 'name'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'shops' => [
            'LuckyWeb\MS\Models\Shop',
            'key' => 'city_id',
        ],
    ];
    public $belongsTo = [
        'region' => ['LuckyWeb\MS\Models\ShopRegion', 'key' => 'region_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public static function findBySlug($slug) {
        return static::whereSlug($slug)->first();
    }

    /**
     * Returns array of shop pages urls
     *
     * @return array
     */
    public static function getSitemapUrls()
    {
        return static::with('region')->get()->map(function ($city) {
            return [
                'url' => '/magaziny-mebeli/'.$city->region->slug.'/'.$city->slug,
                'lastmod' => $city->updated_at->toDateString(),
                'priority' => '0.25'
            ];
        })->toArray();
    }

}
