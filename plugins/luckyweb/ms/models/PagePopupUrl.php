<?php namespace LuckyWeb\MS\Models;


use October\Rain\Database\Model;

/**
 * PagePopup Model
 */
class PagePopupUrl extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_page_popups_url';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    protected $jsonable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
//        'image' => 'System\Models\File'
    ];
    public $attachMany = [];
}
