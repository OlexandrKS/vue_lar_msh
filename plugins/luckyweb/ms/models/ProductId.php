<?php namespace LuckyWeb\MS\Models;

use Illuminate\Support\Facades\DB;
use October\Rain\Database\Model;

/**
 * ProductId Model
 */
class ProductId extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_product_ids';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'id',
        'numeric_id'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];


    /**
     * Update product numeric ids
     */
    public static function updateIds()
    {
        DB::statement('
          INSERT IGNORE INTO `luckyweb_ms_product_ids` (id, numeric_id)
          SELECT id, SUBSTRING(CAST(CONV(SUBSTRING(CAST(SHA(id) as char), 1, 16), 16, 10) as char), 1, 10) 
          FROM luckyweb_ms_products
        ');
    }
}
