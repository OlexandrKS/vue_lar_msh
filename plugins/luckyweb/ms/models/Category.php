<?php

namespace LuckyWeb\MS\Models;

use DB;
use October\Rain\Database\Model;
use Str;
use Log;

/**
 * Category Model
 */
class Category extends Model
{
    use \October\Rain\Database\Traits\Sluggable;
    use \LuckyWeb\MS\Classes\Traits\OwnedSite;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_categories';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [''];

    protected $slugs = ['slug' => 'name', 'slug_auto' => 'name'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'products' => ['LuckyWeb\MS\Models\Product'],
        'images' => ['LuckyWeb\MS\Models\Image'],
        'promotions' => ['LuckyWeb\MS\Models\Promotion'],
        'subcategories' => ['LuckyWeb\MS\Models\SubCategory'],
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public static function findBySlug($slug) {
        $category = static::whereSlug($slug)->first();
        if (!$category) {
            $category = static::where('slug_auto', '=', $slug)->first();
        }
        return $category;
    }

    /**
     * Published scrope. Reserved for further use
     */
    public function scopePublished($query) {
        return $query->orderBy('order'); //->whereActive(true);
    }

    /**
     * Fills table(s) with imported data from _import_ tables
     */
    public static function updateImported() {
        DB::table((new static)->table)->truncate();
        Model::unguard();

        $query = DB::table('luckyweb_ms_import_categories');
        static::ownedSiteExtendQuery($query);
        $items = $query->get();

        foreach ($items as $item) {
            $category = static::create([
                'id' => $item->id,
                'name' => $item->name,
                'price_from' => $item->price_from,
                'order' => $item->order,
                'price_description' => $item->price_description,
                'banner_week_1' => $item->banner_week_1,
                'banner_week_2' => $item->banner_week_2,
            ]);

            if ($item->slug!='') {
                // handle non-unique slugs

                $genSlug = function ($slug, $cnt) {
                    return preg_replace('/\s+/', '-', $slug).($cnt==0 ? '' : '-'.$cnt);
                };

                $cnt = 0;
                while (static::where('slug', '=', $genSlug($item->slug, $cnt))->first()) {
                    $cnt++;
                }

                // overload slug
                $category->slug = $genSlug($item->slug, $cnt);
                $category->save();
            }
        }
        DB::table((new static)->table)
            ->whereIn('id', DB::table((new ImportGoodType)->table)->distinct('category_id')->lists('category_id'))
            ->update(['active' => true]);

        Model::reguard();
    }

    /**
     * Returns array of category pages urls
     *
     * @return array
     */
    public static function getSitemapUrls()
    {
        return static::published()->get()->map(function ($category) {
            return [
                'url' => '/catalog/'.$category->slug,
                'lastmod' => $category->updated_at->toDateString(),
                'priority' => '0.75'
            ];
        })->toArray();
    }

    /**
     * Breakes name into multiple html lines (inserts <br />)
     *
     * @return string
     */
    public function getBreakedName() {
        if (count(explode(' ', $this->name))==2) {
            return preg_replace('/[ ]+/iu', '<br />', $this->name);
        }
        return preg_replace('/ и/iu', '<br />и', $this->name);
    }

    /**
     * Check if category is week promo
     * @return bool
     */
    public function isWeekPromo()
    {
        return !empty($this->banner_week_1) && !empty($this->banner_week_1);
    }

    public static function getSlider()
    {
        $cacheKey = 'slider_categories';

        if (cache()->store()->has($cacheKey)) {
            return cache()->store()->get($cacheKey);
        }

        $items = static::published()
            ->with('images', 'subcategories')
            ->get();

        cache()->store()->set($cacheKey, $items);

        return $items;
    }
}
