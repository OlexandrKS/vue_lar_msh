<?php namespace LuckyWeb\MS\Models;

use Model;

/**
 * PageMeta Model
 */
class PageMeta extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_page_metas';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['url', 'title', 'description', 'keywords'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}
