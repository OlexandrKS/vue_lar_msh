<?php

namespace LuckyWeb\MS\Models;

use App\Domain\Entities\Catalog\Offer;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Jenssegers\Agent\Facades\Agent;
use DB;
use Log;

use LuckyWeb\MS\Classes\Traits\OwnedSite;
use LuckyWeb\MS\Models\ImportGoodsColors;
use LuckyWeb\MS\Models\ImportGoodType;
use October\Rain\Database\Model;
use October\Rain\Database\Traits\Sluggable;
use October\Rain\Support\Collection;

/**
 * Product Model
 *
 * @property double $buy_active_price Расчитаная калькулятором цена
 * @property int $buy_count Количество в единиц заказе
 * @property int $buy_support Количество дополнительных продуктов по акции "вместе дешевле"
 * @property bool $buy_is_special Товар продается по акционной цене
 *
 * @property-read Image $preview_image
 * @property-read Image $preview_desktop_image
 * @property-read Image $preview_mobile_image
 *
 * @property Offer $primary_offer
 * @property SiteGoodType $good_type_site
 * @property-read Offer[]|Collection $offers
 * @property-read Image[]|Collection $images
 *
 * @property-read string $url Ссылка на страницу
 *
 * @method \Illuminate\Database\Eloquent\Builder displayable()
 *
 * @mixin \Eloquent
 */
class Product extends Model
{
    use Sluggable, OwnedSite;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_products';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array
     */
    protected $slugs = ['slug' => ['name', 'subname']];

    /**
     * @var array Relations
     */
    public $hasOne = [
        'nid' => [
            'LuckyWeb\MS\Models\ProductId',
            'key' => 'id',
            'otherKey' => 'id'
        ],
        'primary_offer' => [
            Offer::class,
            'key' => 'primary_id',
            'scope' => 'storedPrimary'
        ],
    ];
    public $hasMany = [
        'together_cheaper' => [
            Offer::class,
            'key' => 'primary_id',
            'scope' => 'togetherCheaper'
        ],
        'images' => [
            'LuckyWeb\MS\Models\Image',
            // 'scope' => 'sorted',
        ],
        'detail_images' => [
            'LuckyWeb\MS\Models\Image',
            'scope' => 'detail'
        ]
    ];
    public $belongsTo = [
        'category' => ['LuckyWeb\MS\Models\Category'],
        'subcategory' => ['LuckyWeb\MS\Models\SubCategory', 'key' => 'sub_category_id'],
        'color' => ['LuckyWeb\MS\Models\Color'],
        'good_type' => ['LuckyWeb\MS\Models\GoodType'],
        'furnisher' => ['LuckyWeb\MS\Models\Furnisher'],
        'good_type_site' => [
            'LuckyWeb\MS\Models\SiteGoodType',
            'key' => 'site_type_id',
            'otherKey' => 'type_id',
        ],
    ];
    public $belongsToMany = [
        'reviewGroups' => [
            'LuckyWeb\MS\Models\ProductGroup',
            'table' => 'luckyweb_ms_products_groups',
            'key' => 'product_id',
            'otherKey' => 'group_id',
            'scope' => 'forReviews'
        ],
        'constructorGroups' => [
            'LuckyWeb\MS\Models\ProductGroup',
            'table' => 'luckyweb_ms_products_groups',
            'key' => 'product_id',
            'otherKey' => 'group_id',
            'scope' => 'forConstructor'
        ],
        'goods' => [
            'LuckyWeb\MS\Models\Product',
            'table' => 'luckyweb_ms_goods_packages',
            'key' => 'package_product_id',
            'otherKey' => 'good_product_id',
        ],
        'packages' => [
            'LuckyWeb\MS\Models\Product',
            'table' => 'luckyweb_ms_goods_packages',
            'key' => 'good_product_id',
            'otherKey' => 'package_product_id',
        ],
        'offers' => [
            Offer::class,
            'table' => 'offer_product_pivot',
            'key' => 'product_id',
            'otherKey' => 'offer_id',
        ],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public static function findBySlug($slug)
    {
        $product = static::whereSlug($slug)->first();

        if (!$product) {
            return static::where('slug', 'LIKE', '%' . $slug)->first();
        }

        return $product;
    }

    /**
     * Published scrope. Reserved for further use
     */
    public function scopePublished($query)
    {
        return $query;
    }

    public function scopeMixedPromo($query)
    {
        return $query->whereHasMixedPromo(true);
    }

    public function beforeCreate()
    {
        $this->id = static::renderProductId($this->color_id, $this->good_id, $this->package_id);
    }

    public static function renderProductId($color_id, $good_id, $package_id)
    {
        return $color_id . '.' . $good_id . '.' . $package_id;
    }

    /**
     * Fills table(s) with imported data from _import_ tables
     */
    public static function updateImported()
    {
        DB::table((new static)->table)->truncate();

        Model::unguard();
        $query = ImportGoodsColors::withGood();
        $goodsColors = $query->get();

        foreach ($goodsColors as $gc) {
            if (!$gc->good || !$gc->good->good_type) {
                //print_r($gc);
                continue;

            }
            /*if ($gc->good->only_for_set_of_packages) {
                continue;
            }*/

            //list($name, $subname) = static::splitTitle($gc->title);
            $parts = explode('|', $gc->title);
            $productAttrs = [
                'color_id' => $gc->color_id,
                'good_id' => $gc->good->id,
                'good_type_id' => $gc->good->good_type_id,
                'name' => $parts[0],
                'subname' => $gc->color,
                'units' => count($parts) > 1 ? $parts[1] : null,
                'description' => preg_replace("/\"+/u", "\"", $gc->good->description),
                'description1' => $gc->good->description1,
                'description2' => $gc->good->description2,
                'description1_name' => $gc->good->description_type ? $gc->good->description_type->desc_name_1 : null,
                'description2_name' => $gc->good->description_type ? $gc->good->description_type->desc_name_2 : null,
                'dimensions' => $gc->good->overall_dimensions,
                'category_id' => $gc->good->category_id ? $gc->good->category_id :
                    $gc->good->good_type->category_id,
                'sub_category_id' => $gc->good->sub_category_id ? $gc->good->sub_category_id :
                    $gc->good->good_type->sub_category_id,
                'price_top_b' => $gc->good->price_top_b,
                'price_lower_b' => $gc->good->price_lower_b,
                'discount_b' => $gc->good->discount_b,
                'price_special' => $gc->good->price_special,
                'discount_special' => $gc->good->discount_special,
                'discount_promo' => $gc->good->discount_promo,
                'discount_condition' => $gc->good->condition_for_discount,
                'price_promo' => $gc->good->price_promo,
                'price_top_sale' => $gc->price_top_sale,
                'price_lower_sale' => $gc->price_lower_sale,
                'discount_sale' => $gc->discount_sale,
                'position_row' => (integer)ceil($gc->position_in_category / 1000),
                'position_column' => (integer)($gc->position_in_category % 1000),
                'has_mixed_promo' => (boolean)$gc->good->mixed_promo,
                'position_mp_row' => (integer)ceil($gc->position_in_mixed_promo / 1000),
                'position_mp_column' => (integer)($gc->position_in_mixed_promo % 1000),
                'video_url' => $gc->link_to_video,
                'price_description' => $gc->good->price_description,
                'week_promo' => $gc->good->week_promo,
                'set_id' => $gc->good->set_id == '' ? null : $gc->good->set_id,
                'also_in_spec_list' => $gc->also_in_spec_list == '' ? null : $gc->also_in_spec_list,
                'max_order_item' => $gc->good->max_order_item,
                'residual_indicator' => $gc->residual_indicator,
                'delivery_cost' => $gc->good->delivery_cost,
                'assembling_cost' => $gc->good->assembling_cost,
                'type_name_for_page' => $gc->good->type_name_for_page,
                'width' => $gc->good->width,
                'depth' => $gc->good->depth,
                'height' => $gc->good->height,
                'category_invisible' => $gc->category_invisible,
                'variant_of_design' => $gc->variant_of_design,
                'furnisher_id' => $gc->good->furnisher_id,
                'site_type_id' => $gc->good->site_type_id != '' ? (int)$gc->good->site_type_id : null,
                'site_width' => $gc->good->site_width != '' ? (int)$gc->good->site_width : null,
                'item_number' => $gc->good->item_number,
                'installments' => $gc->good->installments,
            ];

            $product = static::create($productAttrs);

            if ($gc->slug != '') {
                $product->slug = preg_replace('/\s+/', '-', $gc->slug) . '-' . $product->slug;
                $product->save();
            }
            /*DB::table((new Image)->table)
                ->whereGoodId($gc->good_id)
                ->whereColorId($gc->color_id)
                    ->update(['product_id'=>$product->id]);*/
        }

        $query = ImportPackagesColors::withPackage();

        $packagesColors = $query->get();

        foreach ($packagesColors as $pc) {
            if (!$pc->package || !$pc->package->good_type) {
                continue;
                //print_r($gc);
            }
            //list($name, $subname) = static::splitTitle($pc->title);
            $parts = explode('|', $pc->title);
            $productAttrs = [
                'color_id' => $pc->color_id,
                'package_id' => $pc->package->id,
                'good_type_id' => $pc->package->good_type_id,
                'name' => trim($parts[0]),
                'subname' => $pc->color,
                'units' => count($parts) > 1 ? trim($parts[1]) : null,
                'description' => preg_replace("/\"+/u", "\"", $pc->package->description),
                'description1' => $pc->package->description1,
                'description2' => $pc->package->description2,
                'description1_name' => $pc->package->description_type ? $pc->package->description_type->desc_name_1 : null,
                'description2_name' => $pc->package->description_type ? $pc->package->description_type->desc_name_2 : null,
                'dimensions' => $pc->package->overall_dimensions,
                'category_id' => $pc->package->category_id ? $pc->package->category_id :
                    $pc->package->good_type->category_id,
                'sub_category_id' => $pc->package->sub_category_id ? $pc->package->sub_category_id :
                    $pc->package->good_type->sub_category_id,
                'price_top_b' => $pc->package->price_top_b,
                'price_lower_b' => $pc->package->price_lower_b,
                'discount_b' => $pc->package->discount_b,
                'price_special' => $pc->package->price_special,
                'discount_special' => $pc->package->discount_special,
                'discount_promo' => $pc->package->discount_promo,
                'discount_condition' => $pc->package->condition_for_discount,
                'price_promo' => $pc->package->price_promo,
                'price_top_sale' => $pc->price_top_sale,
                'price_lower_sale' => $pc->price_lower_sale,
                'discount_sale' => $pc->discount_sale,
                'position_row' => (integer)ceil($pc->position_in_category / 1000),
                'position_column' => (integer)($pc->position_in_category % 1000),
                'has_mixed_promo' => (boolean)$pc->package->mixed_promo,
                'position_mp_row' => (integer)ceil($pc->position_in_mixed_promo / 1000),
                'position_mp_column' => (integer)($pc->position_in_mixed_promo % 1000),
                'video_url' => $pc->link_to_video,
                'price_description' => $pc->package->price_description,
                'week_promo' => $pc->package->week_promo,
                'set_id' => $pc->package->set_id == '' ? null : $pc->package->set_id,
                'also_in_spec_list' => $pc->also_in_spec_list == '' ? null : $pc->also_in_spec_list,
                'max_order_item' => $pc->package->max_order_item,
                'residual_indicator' => $pc->residual_indicator,
                'delivery_cost' => $pc->package->delivery_cost,
                'assembling_cost' => $pc->package->assembling_cost,
                'type_name_for_page' => $pc->package->type_name_for_page,
                'width' => $pc->package->width,
                'depth' => $pc->package->depth,
                'height' => $pc->package->height,
                'category_invisible' => $pc->category_invisible,
                'variant_of_design' => $pc->variant_of_design,
                'furnisher_id' => $pc->package->furnisher_id,
                'site_type_id' => $pc->package->site_type_id != '' ? (int)$pc->package->site_type_id : null,
                'site_width' => $pc->package->site_width != '' ? (int)$pc->package->site_width : null,
                'item_number' => $pc->package->item_number,
                'installments' => $pc->package->installments
            ];
            $product = static::create($productAttrs);

            if ($pc->slug != '') {
                $product->slug = preg_replace('/\s+/', '-', $pc->slug) . '-' . $product->slug;
                $product->save();
            }
        }
        Model::reguard();

        $items = DB::table('luckyweb_ms_import_goods_packages')->get();
        $links = [];
        foreach ($items as $item) {
            $good_product_id = static::renderProductId($item->good_color_id, $item->good_id, null);
            $package_product_id = static::renderProductId($item->package_color_id, null, $item->package_id);
            $links[$good_product_id . '-' . $package_product_id] = [
                'good_product_id' => $good_product_id,
                'package_product_id' => $package_product_id,
                'quantity' => $item->quantity,
            ];
        }

        DB::table('luckyweb_ms_goods_packages')->truncate();
        DB::table('luckyweb_ms_goods_packages')->insert(array_values($links));
        ProductId::updateIds();
    }

    /**
     * Splits title of good-color
     * @param string $tidy_get_release
     *
     * @return array - [name, subname]
     */
    public static function splitTitle($title)
    {
        $parts1 = explode('(', $title);
        if (count($parts1) < 2) {
            return [$title, null];
        }
        $parts2 = explode(')', $parts1[1]);
        return [$parts1[0], $parts2[0]];
    }

    /**
     * Returns array of products pages urls
     *
     * @return array
     */
    public static function getSitemapUrls()
    {
        //return array_map(function ($item))
        return static::available()
            ->with('category')
            ->get()
            ->map(function ($product) {
                $data = [
                    'url' => '/catalog/' . $product->category->slug . '/' . $product->slug,
                    'priority' => '0.25',
                    'lastmod' => $product->updated_at->toDateString()
                ];

                $images = $product->detail_images()->get();

                if ($images->count() > 0) {
                    $caption = 'Мебель Шара, ' . $product->category->name . ', ' . $product->name . ' ' . $product->subname;

                    $data['images'] = $images->map(function ($image) use (&$caption) {
                        $data = [
                            'src' => $image->getSource(),
                            'caption' => $caption,
                        ];

                        $caption = null;

                        return $data;
                    })->toArray();
                }

                return $data;
            })->toArray();
    }


    /**
     * Generates and return the array of available product's descritpions
     *
     * @return array - Array of product's of descriptions ('Description name' => 'Description value')
     */
    public function getDescriptions()
    {
        $result = [];
        for ($i = 1; $i < 3; $i++) {
            if ($this->attributes['description' . $i . '_name'] && !empty($this->attributes['description' . $i])) {
                $result[$this->attributes['description' . $i . '_name']] = $this->attributes['description' . $i];
            }
        }
        return $result;
    }

    /**
     * Checks if the product has given description
     * @param integer $index - Index of description (1 or 2)
     *
     * @return boolean
     */
    public function hasDescription($index)
    {
        if ($index > 2) {
            return false;
        }
        return $this['description' . $index . '_name'] != null;
    }

    /**
     * Return active price to show
     * @return float
     */
    public function getActivePrice()
    {
        $priorityOrder = [
            'price_lower_sale',
            'price_promo',
            'price_lower_b',
        ];
        foreach ($priorityOrder as $field) {
            if ($this->$field > 0) {
                return $this->$field;
            }
        }
        return $this->price_lower_b;
    }

    /**
     * Return active discount to show
     * @return float
     */
    public function getActiveDiscount()
    {
        $priorityOrder = [
            'discount_sale',
            'discount_promo',
            'discount_b',
        ];
        foreach ($priorityOrder as $field) {
            if ($this->$field > 0) {
                return $this->$field;
            }
        }
        return $this->discount_b;
    }

    /**
     * Returnes striked price
     * @return float
     */
    public function getStrikedPrice()
    {
        return $this->price_top_sale > 0 ? $this->price_top_sale : $this->price_top_b;
    }

    /**
     * Patch.
     * Returns mixed discount for product.
     * TBD: replace with import data
     */
    public function getMixedDisount()
    {
        /*$sixties = [
            'intero-vengedub', 'dallas-new-kashtan', 'deli-16-m-shimo-temnyy'
        ];
        return in_array($this->slug, $sixties) ? 60 : 50;*/
        return floor(20 * ($this->price_top_b - $this->price_promo) / $this->price_top_b) * 5;
    }


    public function getVideoShortUrl()
    {
        return 'https://youtu.be/' . $this->getYoutubeShortCode();
    }

    public function getVideoPreviewSrc()
    {
        return 'https://img.youtube.com/vi/' . $this->getYoutubeShortCode() . '/hqdefault.jpg';
    }

    public function getYoutubeShortCode()
    {
        if (!$this->video_url) {
            return null;
        }
        $parts = parse_url($this->video_url);
        if ($parts === FALSE) {
            return null;
        }

        if ($parts['host'] == 'youtu.be') { // short url
            return ltrim($parts['path'], '/');
        }

        parse_str($parts['query'], $qparts);
        return $qparts['v'];
    }

    public function getPriceDescription()
    {
        return $this->price_description == '' ? $this->category->price_description : $this->price_description;
    }

    /**
     * Check if product is week promo
     * @return bool
     */
    public function isWeekPromo()
    {
        return $this->week_promo == 1;
    }

    public function getNestedGoods()
    {
        if (!$this->package_id) {
            return [];
        }
        return $this->goods()
            ->where('good_type_id', '=', $this->good_type_id)
            ->get();
    }

    public function getNestedGoodSets()
    {
        if (!$this->package_id) {
            return [];
        }
        $result = [];
        $setGoods = $this->goods()
            ->where('good_type_id', '=', $this->good_type_id)
            ->where('set_id', '>', 0)
            ->select('set_id', 'color_id')
            ->get();
        foreach ($setGoods as $good) {
            $result[$good->color_id . '.' . $good->set_id] = null;
        }
        return array_keys($result);
    }

    public function getNestedElementIds()
    {
        $setsColors = $this->getNestedGoodSets();
        $query = $this
            ->whereIn(DB::raw("CONCAT(`color_id`, '.', `set_id`)"), $setsColors)
            ->where('also_in_spec_list', true);
        //echo $query->toSql()."\n";
        //print_r($query->getBindings());
        return $query->lists('id');
    }

    public function getNestedElementPositions()
    {
        //$ids = $this->getNestedElementIds();
        return NestedPosition::with('good')
            ->whereHas('good', function ($query) {
                $query->where('also_in_spec_list', true);
            })
            ->where('package_product_id', '=', $this->id)
            //->whereIn('good_product_id', $ids)
            ->where('position_row', '!=', 0)
            ->where('position_column', '!=', 0)
            ->orderBy('position_row')
            ->orderBy('position_column')->get();
    }

    /**
     * Check id product has nested elements
     * @return bool
     */
    public function hasNestedElementPositions()
    {
        return NestedPosition::whereHas('good', function ($query) {
                $query->where('also_in_spec_list', true);
            })
                ->where('package_product_id', '=', $this->id)
                ->where('position_row', '!=', 0)
                ->where('position_column', '!=', 0)
                ->count() > 0;
    }

    /**
     * Return same products wih different colors
     * @return mixed
     */
    public function getColorProducts()
    {
        if ($this->package_id) {
            $colors = static::where('set_id', $this->set_id)
                ->whereNotNull('package_id')
                ->orderBy('subname', 'ASC')
                ->get();
        } else {
            $colors = static::where('set_id', $this->set_id)
                ->orderBy('subname', 'ASC')
                ->get();
        }

        $colors->load('images');

        return $colors;
    }

    /**
     * Return nested product if current product is package
     * @return mixed
     */
    public function getPackageProducts()
    {
        if ($this->package_id) {
            $result = collect(DB::table('luckyweb_ms_import_goods_packages')
                ->where('package_id', $this->package_id)
                ->where('package_color_id', $this->color_id)
                ->get());

            return Product::whereIn('good_id', $result->pluck('good_id'))
                ->with('images', 'good_type_site')
                ->get()
                ->filter(function ($item) use ($result) {
                    return is_numeric($result->search(function ($resultItem) use ($item) {
                        return $resultItem->good_id == $item->good_id
                            && $resultItem->good_color_id == $item->color_id;
                    }));
                })->map(function ($item) use ($result) {
                    $set = $result->get($result->search(function ($resultItem) use ($item) {
                        return $resultItem->good_id == $item->good_id
                            && $resultItem->good_color_id == $item->color_id;
                    }));
                    if ($set) {
                        $item->set_qunatity = $set->quantity;
                    }
                    return $item;
                });
        }
    }

    /**
     * Return list of support products
     * @return mixed
     */
    public function getSupportProducts()
    {
        $categories = [];
        return TogetherCheeperPivot::where(function ($q) {
            $q->where('form_good_id', $this->good_id)
                ->orWhere('form_package_id', $this->package_id);
        })
            ->whereNotNull('supp_category_id')
            ->orderBy('the_best', 'desc')
            ->orderBy('saving', 'desc')
            ->orderBy('supp_price_lower_b', 'asc')
            ->get()
            ->filter(function ($item) use (&$categories) {

                $supportProduct = $item->getSupportProduct();
                if ($supportProduct && $supportProduct->price_lower_sale == 0
                    && $supportProduct->price_promo == 0
                    && !in_array($item->supp_category_id, $categories)) {
                    $categories[] = $item->supp_category_id;
                    return true;
                }
            });
    }

    /**
     * Return list of support products
     * @param int $categoryId
     * @return mixed
     */
    public function getSupportProductsByCategory($categoryId)
    {
        return TogetherCheeperPivot::where(function ($q) {
            $q->where('form_good_id', $this->good_id)
                ->orWhere('form_package_id', $this->package_id);
        })
            ->where('supp_category_id', $categoryId)
            ->orderBy('saving', 'desc')
            ->orderBy('supp_price_lower_b', 'asc')
            ->get()
            ->filter(function ($item) {
                $supportProduct = $item->getSupportProduct();
                if ($supportProduct && $supportProduct->price_lower_sale == 0
                    && $supportProduct->price_promo == 0) {
                    return true;
                }
            });
    }

    /**
     * Return color field title
     * @return string
     */
    public function getColorTitle()
    {
        if (in_array($this->category_id, [7, 9])) { // Диваны и углы
            return 'Обивка';
        } else {
            return 'Вариант комплектации';
        }
    }

    /**
     * Return availability class
     * @return string
     */
    public function getAvailabilityClass()
    {
        switch ($this->residual_indicator) {
            case 0:
            default:
                return 'pre-order';
            case 1:
                return 'limited';
            case 2:
                return 'in-stock';
        }
    }

    /**
     * Return price set text depends on product properties
     * @return string
     */
    public function getPriceSetText()
    {
        if ($this->category_id == 1 && $this->sub_category_id == 2) {
            return 'В цену погонного метра входит:';
        } else {
            return 'В цену комплекта входит:';
        }
    }

    /**
     * Return flag if product has cheaper product
     * @param $list - num of fav_list
     * @return boolean
     */
    public function isCheaper($list = null)
    {
        $supports = $this->getSupportProducts();

        if (is_null($list)) {
            $list_id = FavoriteList::getDefaultList();
            $list = FavoriteListItem::getProductFromFavList($list_id, 'product_id', 'luckyweb_ms_products.id');
            $list = collect($list)->pluck('id');
        } else {
            $in_list = Product::join('luckyweb_ms_favorite_list_items as fli', 'luckyweb_ms_products.id', '=', 'fli.product_id')
                ->where('fli.list_id', $list)
                ->select('luckyweb_ms_products.*')
                ->get()
                ->toArray();
            $list = collect($in_list)->pluck('id');
        }
        foreach ($supports as $item)
            if ($list->contains($item->getSupportProduct()->id))
                return true;
        return false;
    }

    /**
     * Check if can show together cheaper block for product
     * @return bool
     */
    public function isShowTogetherCheaper()
    {
        return $this->price_special > 0
            && ($this->getActivePrice() - $this->price_special) > 0
            && $this->price_lower_sale == 0
            && $this->price_promo == 0;
    }

    /**
     * Return product description with injected hints
     */
    public function getDescriptionWithHints()
    {
        return DescriptionHint::inject($this->description);
    }

    /**
     * Return data array for ecommerce events
     * @param $data
     * @return array
     */
    public function getDataForEcommerce(array $data = [])
    {
        return array_merge([
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->getActivePrice(),
            'category' => $this->category->name . ($this->subcategory ? '/' . $this->subcategory->name : ''),
            'variant' => $this->subname
        ], $data);
    }

    /**
     * Return popup specifications data for current product
     * @return array
     * @throws \Exception
     */
    public function getPopupSpecifications()
    {
        $cacheKey = "product.popupSpecs.{$this->id}";
        $cacheMinutes = 24*60*60;

        return cache()->remember($cacheKey, $cacheMinutes, function () {
            $valueIds = DB::table('luckyweb_ms_specif_values_sets')
                ->where('set_id', $this->set_id)
                ->where('category_id', $this->category_id)
                ->get()
                ->pluck('value_id');

            $keyIds = DB::table('luckyweb_ms_specif_values_cats')
                ->whereIn('value_id', $valueIds)
                ->where('category_id', $this->category_id)
                ->get();

            $values = SpecificationValue::whereIn('id', $valueIds)
                ->orderBy('sort', 'ASC')
                ->orderBy('name', 'ASC')
                ->get();


            return Specification::whereIn('id', $keyIds->pluck('specification_id'))
                ->where('popup', 1)
                ->orderBy('sort', 'ASC')
                ->orderBy('name', 'ASC')
                ->get()
                ->map(function ($item) use (&$result, $keyIds, $values) {
                    $items = [];
                    $keyIds->filter(function ($k) use ($item) {
                        return $k->specification_id == $item->id;
                    })->each(function ($v) use (&$items, $values) {

                        $val = $values->first(function ($i) use ($v) {
                            return $i->id == $v->value_id;
                        });

                        if ($val) {
                            $items[] = $val->toArray();
                        }
                    });

                    $item['items'] = $items;

                    return $item;
                });
        });
    }

    /**
     * Return specifications data for current product
     * @return array
     * @throws \Exception
     */
    public function getSpecifications()
    {
        $cacheKey = "product.specs.{$this->id}";
        $cacheMinutes = 24*60*60;

        return cache()->remember($cacheKey, $cacheMinutes, function () {
            $valueIds = DB::table('luckyweb_ms_specif_values_sets')
                ->where('set_id', $this->set_id)
                ->where('category_id', $this->category_id)
                ->get()
                ->pluck('value_id');

            $keyIds = DB::table('luckyweb_ms_specif_values_cats')
                ->whereIn('value_id', $valueIds)
                ->where('category_id', $this->category_id)
                ->get();

            $values = SpecificationValue::whereIn('id', $valueIds)
                ->get();

            return Specification::whereIn('id', $keyIds->pluck('specification_id'))
                ->where('popup', 0)
                ->orderBy('sort', 'ASC')
                ->orderBy('name', 'ASC')
                ->get()
                ->map(function ($item) use (&$result, $keyIds, $values) {
                    $items = [];
                    $keyIds->filter(function ($k) use ($item) {
                        return $k->specification_id == $item->id;
                    })->each(function ($v) use (&$items, $values) {

                        $val = $values->first(function ($i) use ($v) {
                            return $i->id == $v->value_id;
                        });

                        if ($val) {
                            $items[] = $val->toArray();
                        }
                    });

                    $item['items'] = collect($items)
                        ->sortBy('sort')
                        ->toArray();

                    return $item;
                });
        });
    }

    /**
     * Return delivery cost
     * @return float
     */
    public function getDeliveryCost()
    {
        return $this->delivery_cost > 450 ? $this->delivery_cost : 450;
    }

    /**
     * Return assembling cost
     * @return float
     */
    public function getAssemblingCost()
    {
        // Not for divany and ugly
        if (!in_array($this->category_id, [7, 9])) {
            return $this->assembling_cost > 500 ? $this->assembling_cost : 500;
        }

        return $this->assembling_cost;
    }

    /**
     * Return product image for category list.d
     *
     * @deprecated
     * @param null $type
     * @return Image
     */
    public function getPreviewImage($type = null)
    {
        return $this->getPreviewImageByAssigment($type);

//        if (is_null($type)) {
//            $type = Agent::isPhone() ? Image::ASSIGNMENT_PREVIEW_MOBILE : Image::ASSIGNMENT_PREVIEW_DESKTOP;
//        }
//
//        if ($image = $this->images()->where('assignment', $type)->first()) {
//            return $image;
//        }
//
//        if ($type == 2 && $image = $this->images()->where('assignment', Image::ASSIGNMENT_PREVIEW_DESKTOP)->first()) {
//            return $image;
//        }
//
//        return $this->images()->first();
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDisplayable($query)
    {
        return $query->where('category_invisible', false)
            ->where('position_row', '>', 0)
            ->where('position_column', '>', 0);
    }

    /**
     * @todo refactor
     * @return int
     * @throws \Exception
     */
    public function getReviewsCountAttribute()
    {
        return cache()->remember("constructor.product.reviews.{$this->slug}", 30, function () {
            $this->loadMissing('reviewGroups');

            if ($this->reviewGroups->first()) {
                return Review::where('status_id', Review::STATUS_APPROVED)
                    ->where('product_group_id', $this->reviewGroups->first()->id)
                    ->count();
            }

            return Review::where('status_id', Review::STATUS_APPROVED)
                ->where('product_slug', $this->slug)
                ->count();
        });
    }


    public function getUrlAttribute()
    {
        return url("/catalog/{$this->category->slug}/{$this->slug}");
    }

    /**
     * @return float|\Illuminate\Cache\CacheManager|int|mixed
     * @throws \Exception
     */
    public function getStarsAttribute()
    {
        $key = $this->id;
        if(Cache::has($key))
        {
            return cache($key);
        }

        $reviewCounts = $this->getReviewsCountAttribute();

        if($reviewCounts != 0)
        {
            $stars = (float)(4+rand(0,8)/10);
            Cache::forever($key,$stars);
            return $stars;
        }

        return $reviewCounts;
    }
    /**
     * Случайная генерация свойства новинки
     *
     * @todo
     * @return bool
     */
    public function getIsNewAttribute()
    {
        return rand(0, 4) === 4;
    }

    /**
     * Случайная генерация свойства "Эко"
     *
     * @todo
     * @return bool
     */
    public function getIsEcoAttribute()
    {
        return rand(0, 2) === 2;
    }

    /**
     * Return products grouped by color
     * @return mixed
     */
    public function getColorGroupedProducts()
    {
        $colorGroups = [];
        $colorProducts = $this->getColorProducts()
            ->groupBy('color_id');

        foreach($colorProducts as $items)
        {
            foreach ($items as $item)
            {
                $colorGroups[$item->color_id][] = $item;
            }
        }

        return $colorGroups;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeDefaultSort($query)
    {
        return $query
            ->orderBy('position_row')
            ->orderBy('position_column');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeAscPriceSort($query)
    {
        return $query
            ->orderBy('price_lower_b', 'asc');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeDescPriceSort($query)
    {
        return $query
            ->orderBy('price_lower_b', 'desc');
    }

    /**
     * @param $assigment
     * @return Collection
     */
    private function filterImagesByPreviewType($assigment)
    {
        return $this->images->sortBy(function (Image $image) {
            return preg_replace('/\D/', '${1}', mb_substr($image->local_name, -6, 6));
        })->filter(function (Image $image) use ($assigment) {
            return $image->assignment == $assigment;
        });
    }

    /**
     * @param $assigment
     * @return Image
     */
    private function getPreviewImageByAssigment($assigment)
    {
        $image = $this->filterImagesByPreviewType($assigment)
            ->first();

        if (!$image) {
            $image = $this->filterImagesByPreviewType(Image::ASSIGNMENT_COMMON)
                ->first();
        }

        return $image;
    }

    /**
     * @return Image
     */
    public function getPreviewMobileImageAttribute()
    {
        return cache()->remember("preview_image_mobile_{$this->id}", 60, function () {
            $image = $this->getPreviewImageByAssigment(Image::ASSIGNMENT_PREVIEW_MOBILE);

            if (!$image) {
                $image = $this->preview_desktop_image;
            }

            return $image;
        });
    }

    /**
     * @return Image
     */
    public function getPreviewDesktopImageAttribute()
    {
        return cache()->remember("preview_image_desktop_{$this->id}", 60, function () {
            return $this->getPreviewImageByAssigment(Image::ASSIGNMENT_PREVIEW_DESKTOP);
        });
    }

    /**
     * @return Image
     */
    public function getPreviewImageAttribute()
    {
        return Agent::isMobile() ? $this->preview_mobile_image : $this->preview_desktop_image;
    }

    /**
     * @todo супер костыль, который убивает производительность в 20 раз, убрать как можно быстрее
     * @param \Illuminate\Database\Query\Builder $query
     * @return mixed
     */
    public function scopeAvailable($query)
    {
        return $query->has('images')
            ->has('primary_offer')
            ->leftJoin('luckyweb_ms_nested_positions', 'luckyweb_ms_nested_positions.good_product_id', '=', 'luckyweb_ms_products.id')
            ->where('category_invisible', false)
            ->where(function ($q) {
                return $q->where('luckyweb_ms_products.position_row', '>', 0)
                    ->orWhere('luckyweb_ms_nested_positions.position_row', '>', 0);
            })
            ->distinct()
            ->select('luckyweb_ms_products.*');
    }

    /**
     * @param $query
     * @param $siteGoodTypeId
     * @return mixed
     */
    public function scopeGrid($query, $siteGoodTypeId)
    {
        return $query->where('site_type_id', $siteGoodTypeId)
            ->available();
    }

    /**
     * @return float|int
     */
    public function getPurchaseBonusesAttribute()
    {
        return (int) floor($this->getActivePrice() / 15000) * 500;
    }

    /**
     * @return float
     */
    public function getCreditFromAttribute()
    {
        return round($this->getActivePrice() / 24);
    }

    /**
     * @return bool
     */
    public function getCanTakeCreditAttribute()
    {
        return $this->getActivePrice() >= 2000;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeInstallments($query)
    {
        return $query->where('installments', '=',1)
            ->where('price_lower_b','>', 2000)
            ->available();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeWeekly($query)
    {
        return $query->where('week_promo', '=',1)->available();
    }

}
