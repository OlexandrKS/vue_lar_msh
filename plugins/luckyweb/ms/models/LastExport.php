<?php namespace LuckyWeb\MS\Models;

use Model;

/**
 * LastExport Model
 */
class LastExport extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_last_exports';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
