<?php namespace LuckyWeb\MS\Models;

use Model;

/**
 * ImportGoodsColors Model
 */
class ImportGoodsColors extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_import_goods_colors';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'good' => ['LuckyWeb\MS\Models\ImportGoods', 'key' => 'good_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function scopeWithGood($query)
    {
        return $query->whereHas('good', function ($query) {
            return $query->ownedContent();
        });
    }

}
