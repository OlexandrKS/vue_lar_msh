<?php

namespace LuckyWeb\MS\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use October\Rain\Database\Model;

/**
 * Image Model.
 *
 * @property string $remote_time_mdt
 * @property string $local_name
 * @property bool   $minimized
 * @property array  $image_size
 * @property array  $image_preview_size
 * @property string $product_id
 * @property int    $assignment
 * @property-read string $url
 * @property-read string $url_preview
 * @property-read string $storage_path
 * @property-read string $storage_path_preview
 *
 * @mixin \Eloquent
 */
class Image extends Model
{
    const ASSIGNMENT_COMMON = 0;
    const ASSIGNMENT_PREVIEW_DESKTOP = 1;
    const ASSIGNMENT_PREVIEW_MOBILE = 2;

    /**
     * @var string the database table used by the model
     */
    public $table = 'luckyweb_ms_images';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['id'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array
     */
    protected $casts = [
        'image_size'         => 'array',
        'image_preview_size' => 'array',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'product' => Product::class,
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @param array $fileInfo
     *
     * @return string
     *
     * @deprecated
     */
    public static function getTimeString(array $fileInfo)
    {
        return $fileInfo['month'].' '.$fileInfo['day'].' '.$fileInfo['time'];
    }

    /**
     * @param static $query
     *
     * @return static
     */
    public function scopePromotion($query)
    {
        return $query->whereNotNull('promotion_id');
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeSorted($query)
    {
        return $query->orderByRaw('cast(substr(local_name, -5, 5) as unsigned) ASC');
    }

    /**
     * @param static $query
     *
     * @return static
     */
    public function scopeDetail($query)
    {
        return $query->where(function ($q) {
            $q->whereNull('assignment')
                ->orWhere('assignment', static::ASSIGNMENT_COMMON);
        })->orderByRaw('cast(SUBSTRING_INDEX(local_name, "_", -1) as unsigned) ASC');
    }

    /**
     * @param $fileInfo
     *
     * @return bool
     */
    public function isChanged($fileInfo)
    {
        return $this->remote_size != $fileInfo['size']; //|| $this->remote_time_mdt != static::getTimeString($fileInfo);
    }

    public function updateRelations()
    {
        preg_match_all('/(([a-z]+)([0-9]+))/iu', $this->local_name, $matches);
        foreach ($matches[2] as $key => $name) {
            $this->{$name.'_id'} = $matches[3][$key];
        }
    }

    public function updateProductId()
    {
        $this->product_id = $this->color_id.'.'.$this->good_id.'.'.$this->package_id;
    }

    /* Model events */

    public function beforeSave()
    {
        $this->updateRelations();
    }

    public function beforeDelete()
    {
        $storage = Storage::disk('images');

        $storage->delete($this->local_name);
        $storage->delete("minimized/{$this->local_name}");
        $storage->delete("preview/{$this->local_name}");
    }

    /**
     * @return string
     *
     * @deprecated
     */
    public function getLocalPath()
    {
        return config('luckyweb.ms::image_path').'/'.$this->local_name;
    }

    /**
     * @return string
     *
     * @deprecated
     */
    public function getLocalPreviewPath()
    {
        return $this->minimized ?
            config('luckyweb.ms::image_preview_path').'/'.$this->local_name :
            $this->getLocalPath();
    }

    /**
     * @return string
     */
    public function getStoragePathAttribute()
    {
        return $this->local_name;
    }

    /**
     * @return string
     */
    public function getStoragePathPreviewAttribute()
    {
        return $this->minimized ? "preview/{$this->local_name}" : $this->storage_path;
    }

    /**
     * @return string
     *
     * @deprecated
     */
    public function getSource()
    {
        return $this->url;
    }

    /**
     * @return string
     *
     * @deprecated
     */
    public function getPreviewSource()
    {
        return $this->url_preview;
    }

    /**
     * @param string $prefix
     *
     * @return string
     */
    private function urlWithPrefix($prefix = '')
    {
        return cache()->remember("image_url_$prefix/{$this->local_name}", 600, function () use ($prefix) {
            return Storage::disk('images')
                ->url("$prefix{$this->local_name}");
        });
    }

    /**
     * @return string
     */
    public function getUrlAttribute()
    {
        $prefix = $this->minimized ? 'minimized/' : '';

        return $this->urlWithPrefix($prefix);
    }

    /**
     * @return string
     */
    public function getUrlPreviewAttribute()
    {
        $prefix = $this->minimized ? 'preview/' : '';

        return $this->urlWithPrefix($prefix);
    }

    /**
     * @throws \Exception
     *
     * @return array|mixed
     */
    public function getPreviewSize()
    {
        return $this->image_preview_size ?? [0, 0];
    }

    /**
     * @return bool
     */
    protected function minimizable()
    {
        return $this->product_id || $this->category_id || $this->promotion_id || 'map.jpg' == $this->local_name;
    }

    /**
     * @deprecated
     */
    public static function updateProductRelations()
    {
        DB::statement(
            'UPDATE `luckyweb_ms_images` SET `product_id` = '.
            "CONCAT(IFNULL(`color_id`, ''), '.', IFNULL(`good_id`, ''), '.', IFNULL(`package_id`, ''))");
    }

    /**
     * @param int $tw
     * @param int $th
     *
     * @return float|int
     *
     * @deprecated
     */
    public function getProductSliderScaledWidth($tw = 975, $th = 579)
    {
        list($iw, $ih) = $this->image_size;
        $scale = 100 * $th * $iw / ($tw * $ih);

        return $scale > 100 ? 100 : $scale;
    }

    /**
     * @param int $tw
     * @param int $th
     *
     * @return float|int
     *
     * @deprecated
     */
    public function getProductSliderMarginTop($tw = 975, $th = 579)
    {
        list($iw, $ih) = $this->image_size;
        $margin = ($th - $ih * $tw / $iw) * 2 / 3;

        return $margin <= 0 ? 0 : $margin;
    }

    /**
     * @return array
     *
     * @deprecated
     */
    public function getImageSize()
    {
        return $this->image_size;
    }

    /**
     * TODO deprecate.
     */
    public static function getStaticMap()
    {
        return static::whereLocalName('map.jpg')->first();
    }
}
