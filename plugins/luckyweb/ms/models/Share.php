<?php namespace LuckyWeb\MS\Models;

use Model;

/**
 * Shares Model
 */
class Share extends Model
{
    const STATUS_NEW = 1;
    const STATUS_ACTIVATED = 2;

    const TYPE_POPUP = 1;
    const TYPE_LINK = 2;
    const TYPE_POPUP_IMAGE = 3;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_shares';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'text',
        'status_id',
        'sort_order'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'image' => 'System\Models\File',
        'popup_image' => 'System\Models\File',
    ];
    public $attachMany = [];

    /**
     * @var array Statuses
     */
    public static $statuses = [
        1 => ['NEW', 'Новая'],
        2 => ['ACTIVATED', 'Опубликована'],
    ];

    /**
     * @var array $types
     */
    public static $types = [
        1 => ['POPUP', 'Модальное окно'],
        2 => ['LINK', 'Ссылка'],
        3 => ['POPUP_IMAGE', 'Всплывающее изображение'],
    ];

    public function scopeActivated($query) {
        return $query->where('status_id', static::STATUS_ACTIVATED);
    }

    /**
     * Return status label for controller list
     * @param $status
     * @return array|mixed
     */
    public static function getStatusLabels($status)
    {
        return array_key_exists($status, static::$statuses) ? static::$statuses[$status]: ['NULL', 'NULL'];
    }

    /**
     * @return array
     */
    public function getStatusIdOptions()
    {
        $statuses = [];
        foreach (static::$statuses as $key => $value) {
            $statuses[$key] = $value[1];
        }
        return $statuses;
    }

    /**
     * Return type label for controller list
     * @param string $type
     * @return array|mixed
     */
    public static function getTypeLabels($type)
    {
        return array_key_exists($type, static::$types) ? static::$types[$type]: ['NULL', 'NULL'];
    }

    /**
     * @return array
     */
    public function getTypeIdOptions()
    {
        $types = [];
        foreach (static::$types as $key => $value) {
            $types[$key] = $value[1];
        }
        return $types;
    }
}
