<?php namespace LuckyWeb\MS\Models;

use Model;

/**
 * PromoItem Model
 */
class PromoItem extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_promo_items';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'block' => ['LuckyWeb\MS\Models\PromoBlock',
            'key' => 'block_id'
        ],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = ['image' => 'System\Models\File'];
    public $attachMany = [];

    public function getWidthOptions()
    {
        $width = [];
        for ($i = 12; $i >= 1; $i--) {
            $width[$i] = $i;
        }
        return $width;
    }

    public function getOffsetOptions()
    {
        $offset = [];
        for ($i = 0; $i <= 5; $i++) {
            $offset[$i] = $i;
        }
        return $offset;
    }

    public function scopeSorted($query)
    {
        return $query->orderBy('sort_order', 'ASC');
    }
}