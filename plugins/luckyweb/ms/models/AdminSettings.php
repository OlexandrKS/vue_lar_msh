<?php namespace LuckyWeb\MS\Models;

use Model;

/**
 * Settings Model
 */
class AdminSettings extends Model
{

    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'luckyweb_ms_admin_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';

}
