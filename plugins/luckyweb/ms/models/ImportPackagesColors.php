<?php namespace LuckyWeb\MS\Models;

use Model;

/**
 * ImportPackagesColors Model
 */
class ImportPackagesColors extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_import_packages_colors';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'package' => ['LuckyWeb\MS\Models\ImportPackage', 'key' => 'package_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function scopeWithPackage($query)
    {
        return $query->whereHas('package', function ($query) {
            return $query->ownedContent();
        });
    }

}
