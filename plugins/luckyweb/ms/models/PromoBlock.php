<?php namespace LuckyWeb\MS\Models;


use October\Rain\Database\Model;

/**
 * PromoBlock Model
 */
class PromoBlock extends Model
{
    const STATUS_NEW = 1;
    const STATUS_PUBLISHED = 2;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_promo_blocks';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'items' => ['LuckyWeb\MS\Models\PromoItem',
            'key' => 'block_id',
            'scope' => 'sorted'
        ],
    ];
    public $belongsTo = [];
    public $belongsToMany = [
        'pages' => [
            'LuckyWeb\MS\Models\PromoPage',
            'table'=>'luckyweb_ms_promo_pages_blocks',
            'key' => 'block_id',
            'otherKey' => 'page_id',
            'pivot' => ['sort_order']
        ],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @var array Statuses
     */
    public static $statuses = [
        1 => ['NEW', 'Новый'],
        2 => ['PUBLISHED', 'Опубликован'],
    ];

    /**
     * Return status label for controller list
     * @param $status
     * @return array|mixed
     */
    public static function getStatusLabels($status)
    {
        return array_key_exists($status, static::$statuses) ? static::$statuses[$status]: ['NULL', 'NULL'];
    }

    public function getStatusIdOptions($keyValue = null)
    {
        $statuses = [];
        foreach (static::$statuses as $key => $value) {
            $statuses[$key] = $value[1];
        }
        return $statuses;
    }

    public function scopeSortedPublished($query)
    {
        //return $query->where('status_id', static::STATUS_PUBLISHED)
        //    ->orderBy('luckyweb_ms_promo_pages_blocks.sort_order', 'ASC');
    }

    public function scopeSorted($query)
    {
        //return $query->orderBy('luckyweb_ms_promo_pages_blocks.sort_order', 'ASC');
    }
}
