<?php namespace LuckyWeb\MS\Models;

use Model;
use DB;

/**
 * Color Model
 */
class Color extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_colors';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * Fills table(s) with imported data from _import_ tables
     */
    public static function updateImported() {
        DB::table((new static)->table)->truncate();
        Model::unguard();
        $items = DB::table('luckyweb_ms_import_colors')->get();
        foreach ($items as $item) {
            static::create([
                'id' => $item->id,
                'name' => $item->name,
            ]);
        }
        Model::reguard();
    }

}
