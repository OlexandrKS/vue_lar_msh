<?php namespace Luckyweb\Ms\Models;

use Model;
use October\Rain\Support\Collection;

/**
 * SubMenuCategory Model
 */
class SubMenuCategory extends Model
{
    const COLUMN_FIRST = 1;
    const COLUMN_SECOND = 2;
    const COLUMN_THIRD = 3;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_sub_menu_categories';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'sub_menu_item' => ['LuckyWeb\MS\Models\SubMenuItem', 'key' => 'menu_category_id'],
    ];
    public $belongsTo = [
        'menu_item' => ['LuckyWeb\MS\Models\MenuItem', 'key' => 'menu_id']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @var array $cols
     */
    public static $cols = [
        1 => ['COLUMN_FIRST', 'Первая'],
        2 => ['COLUMN_SECOND', 'Вторая'],
        3 => ['COLUMN_THIRD', 'Третья'],
    ];

    /**
     * @return array
     */
    public function getColumnNumOptions()
    {
        $columns = [];
        foreach (static::$cols as $key => $value) {
            $columns[$key] = $value[1];
        }
        return $columns;
    }

    /**
     * Return type label for controller list
     * @param integer $column
     * @return array|mixed
     */
    public static function getColumnNumLabels($column)
    {
        return array_key_exists($column, static::cols) ? static::$cols[$column]: ['NULL', 'NULL'];
    }

    public static function getTypeLabels($type)
    {
        $menu = MenuItem::select('id', 'name')
            ->where('id', $type)
            ->get();
        $s = new Collection($menu->toArray()[0]);
        return $s->values()->toArray();
    }

    public function getMenu()
    {
        $menu = MenuItem::select('id', 'name')->get();
        $menu = $menu->pluck('name', 'id');
        return $menu;
    }

    /**
     * @return array
     */
    public function  getMenuIdOptions()
    {
        return $this->getMenu();
    }

}
