<?php namespace LuckyWeb\MS\Models;

use Model;

/**
 * DescriptionHint Model
 */
class DescriptionHint extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_description_hints';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'text'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * Inject hints to given text
     * @param $text
     */
    /*public static function inject($text)
    {
        preg_match_all("/<#([^#>]+)#>/", $text, $matches);

        foreach ($matches[1] as &$match) {
            $match = trim($match);
        }

        static::whereIn('name', $matches[1])
            ->each(function($item) use($matches, &$text){
                $search = array_get($matches[0], array_search($item->name, $matches[1]));
                if($search) {
                    $replace = '<span><a href="#" class="js-desc_hint_button">'.$item->name.'</a><span class="js-desc_hint">'.$item->text.'</span></span>';

                    $text = str_replace($search, $replace, $text);
                }
            });

        return $text;
    }*/

    /**
     * Inject hints to given text
     * @param $text
     * @return string
     */
    public static function inject($text)
    {
        static::get()->each(function($item) use(&$text) {
            $replace = '<a href="#" tabindex="0" data-html="true" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content=\''.$item->text.'\'>'.$item->name.'</a>';

            $text = preg_replace('/[\s|,|.|!|?]('.str_replace(' ', '\s', $item->name).')[\s|,|.|!|?]/i', ' '.$replace.' ', $text);
        });

        return $text;
    }
}
