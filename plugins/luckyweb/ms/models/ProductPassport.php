<?php namespace LuckyWeb\MS\Models;

use Illuminate\Support\Facades\Log;
use Model;

/**
 * ProductPassport Model
 */
class ProductPassport extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'luckyweb_ms_product_passports';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'logo' => 'System\Models\File'
    ];
    public $attachMany = [
        'pages' => 'System\Models\File'
    ];

    /**
     * For controller dropdown
     * @return array
     */
    public function getProductNameOptions()
    {
        $result = Product::published()->groupBy('name')->lists('name', 'name');
        return $result;
    }

    /**
     * For controller dropdown
     * @return array
     */
    public function getCategoryIdOptions()
    {
        $result = Category::published()->lists('name', 'id');
        return $result;
    }
}