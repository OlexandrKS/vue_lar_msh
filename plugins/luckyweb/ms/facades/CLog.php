<?php namespace LuckyWeb\MS\Facades;

use Illuminate\Support\Facades\Facade;

class CLog extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return \LuckyWeb\MS\Classes\CLog\CLog::class; }
}
