<?php

namespace LuckyWeb\MS\Classes\Markets\WooCommerce;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use LuckyWeb\MS\Classes\Markets\Traits\ManagerStorageCache;
use LuckyWeb\MS\Models\Image;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\ProductPassport;
use XMLWriter;

class WooManager
{

    use ManagerStorageCache;

    /**
     * @var XMLWriter
     */
    protected $writer;

    /**
     * YmlManager constructor.
     */
    public function __construct()
    {
        $this->filename = 'export_' . Carbon::now()->timestamp . '.yml';
        $this->storageDirectory = 'markets/woo';

        $this->writer = new XMLWriter();
        $this->writer->openMemory();
    }

    /**
     * Generate and return YML
     */
    public function generate()
    {
        $this->writer->startDocument('1.0', 'utf-8');
        $this->writer->startElement('document');

        Product::available()
            ->with('detail_images', 'category', 'subcategory')
            ->get()
            ->each(function (Product $item) {

                $this->writer->startElement('product');

                $this->writer->writeElement('title', $item->type_name_for_page . ' ' . $item->name);

                $this->writer->writeElement('color', $item->subname);
                if (!empty($item->variant_of_design)) {
                    $this->writer->writeElement('variation', $item->variant_of_design);
                }

                $this->writer->writeElement('oldprice', $item->getStrikedPrice());
                $this->writer->writeElement('price', $item->getActivePrice());
                $this->writer->writeElement('description', $item->description);

                if (!empty($item->height)) {
                    $this->writer->writeElement('height', $item->height);
                }
                if (!empty($item->width)) {
                    $this->writer->writeElement('width', $item->width);
                }
                if (!empty($item->depth)) {
                    $this->writer->writeElement('depth', $item->depth);
                }

                foreach ($item->getDescriptions() as $name => $value) {
                    if ($name != 'Габаритные размеры') {
                        $this->writer->writeElement(str_slug($name, '_'), $value);
                    }
                }

                // Images
                $item->detail_images->each(function (Image $image, $key) {
                    $this->writer->writeElement('image_url' . ($key + 1), $image->url);
                });

                $this->writer->writeElement('available', in_array($item->residual_indicator, [1, 2]) ? 'true' : 'false');
                $this->writer->writeElement('category', $item->category->name);

                if (!empty($item->sub_category_id)) {
                    $this->writer->writeElement('subcategory', $item->subcategory->name);
                }

                // Specifications
                $specifications = $item->getSpecifications();
                if ($specifications->count()) {
                    $content = '';
                    foreach ($specifications as $spec) {
                        $content .= '<div class="row item"><div class="col-sm-4 name"><strong>' . $spec['name'] . '</strong></div>';
                        $content .= '<div class="col-sm-8 value">';
                        foreach ($spec->items as $key => $value) {
                            $content .= ($key > 0 ? ', ' : '') . $value['name'];
                        }
                        $content .= '</div>';
                        $content .= '</div>';
                    }
                    $this->writer->startElement('full_description');
                    $this->writer->writeCdata($content);
                    $this->writer->endElement();
                }

                // Popup specifications
                $specifications = $item->getPopupSpecifications();
                if ($specifications->count()) {
                    $content = '<ul>';
                    foreach ($specifications as $spec) {
                        foreach ($spec->items as $key => $value) {
                            $content .= '<li>' . $value['name'] . '</li>';
                        }
                    }
                    $content .= '</ul>';
                    $this->writer->startElement('hover');
                    $this->writer->writeCdata($content);
                    $this->writer->endElement();
                }

                // Passport
                $passport = ProductPassport::where('product_name', $item->name)
                    ->where('category_id', $item->category_id)
                    ->first();

                if ($passport) {
                    $this->writer->writeElement('passport1', $passport->logo->getPath());

                    $passport->pages->each(function ($page, $key) {
                        $this->writer->writeElement('passport' . ($key + 2), $page->getPath());
                    });
                }

                $this->writer->fullEndElement();
            });

        $this->writer->fullEndElement();
        $this->writer->endDocument();

        Storage::put($this->storageDirectory . '/' . $this->filename, $this->writer->outputMemory());
    }
}
