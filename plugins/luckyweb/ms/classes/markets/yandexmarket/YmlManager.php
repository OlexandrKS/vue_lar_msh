<?php

namespace LuckyWeb\MS\Classes\Markets\YandexMarket;

use Bukashk0zzz\YmlGenerator\Model\Category;
use Bukashk0zzz\YmlGenerator\Model\Currency;
use Bukashk0zzz\YmlGenerator\Model\Delivery;
use Bukashk0zzz\YmlGenerator\Model\Offer\OfferParam;
use Bukashk0zzz\YmlGenerator\Model\ShopInfo;
use Bukashk0zzz\YmlGenerator\Settings;

use Carbon\Carbon;
use LuckyWeb\MS\Classes\Markets\Traits\ManagerStorageCache;
use LuckyWeb\MS\Classes\Markets\YandexMarket\Model\Offer\OfferDelivery;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\Category as CategoryModel;

class YmlManager
{
    use ManagerStorageCache;

    /**
     * @var Generator
     */
    protected $generator;

    /**
     * List of category IDs that must be excluded
     * @var array
     */
    protected $categoriesExclude;

    /**
     * List of subcategory IDs that must be excluded
     * @var array
     */
    protected $subCategoriesExclude;

    /**
     * YmlManager constructor.
     * @param array $categoriesExclude
     */
    public function __construct($categoriesExclude = [], $subCategoriesExclude = [])
    {
        $this->categoriesExclude = $categoriesExclude;
        $this->subCategoriesExclude = $subCategoriesExclude;

        $this->filename = 'export_' . Carbon::now()->timestamp . '.yml';
        $this->storageDirectory = $this->getDirectory();

        $settings = new Settings();
        $settings->setEncoding('utf-8');
        $settings->setOutputFile($this->storageDirectory . '/' . $this->filename);

        $this->generator = new Generator($settings);
    }

    /**
     * Generate and return YML
     */
    public function generate()
    {
        // Creating ShopInfo object (https://yandex.ru/support/webmaster/goods-prices/technical-requirements.xml#shop)
        $shopInfo = (new ShopInfo())
            ->setName('Мебель Шара')
            ->setCompany('ООО Мебель Шара')
            ->setUrl('https://www.mebelshara.ru');

        // Creating currencies array (https://yandex.ru/support/webmaster/goods-prices/technical-requirements.xml#currencies)
        $currencies = [];
        $currencies[] = (new Currency())
            ->setId('RUR')
            ->setRate(1);

        // Creating categories array (https://yandex.ru/support/webmaster/goods-prices/technical-requirements.xml#categories)
        $categories = [];
        CategoryModel::whereNotIn('id', $this->categoriesExclude)
            ->get()
            ->each(function ($item) use (&$categories) {
                $categories[] = (new Category())
                    ->setId($item->id)
                    ->setName($item->name);

                $item->subcategories->each(function ($sub) use (&$categories, $item) {
                    if (!in_array($sub->id, $this->subCategoriesExclude)) {
                        $categories[] = (new Category())
                            ->setId($item->id * 100 + $sub->id)
                            ->setParentId($item->id)
                            ->setName($sub->name);
                    }
                });
            });

        // Optional creating deliveries array (https://yandex.ru/support/partnermarket/elements/delivery-options.xml)
        $deliveries = [];
        $deliveries[] = (new Delivery())
            ->setCost(300)
            ->setDays('3-5');

        // Creating offers array (https://yandex.ru/support/webmaster/goods-prices/technical-requirements.xml#offers)
        $offers = [];
        Product::available()
            ->whereNotIn('category_id', $this->categoriesExclude)
            ->whereNotIn('sub_category_id', $this->subCategoriesExclude)
            ->get()
            ->each(function (Product $item) use (&$offers) {
                $offer = (new OfferDelivery())
                    ->setId($item->nid->numeric_id)
                    ->setAvailable(in_array($item->residual_indicator, [1, 2]))
                    ->setUrl(url('/catalog/' . $item->category->slug . '/' . $item->slug))
                    ->setPrice($item->getActivePrice())
                    ->setCurrencyId('RUR')
                    ->setCategoryId($item->category->id)
                    ->setPickup(true)
                    ->setName(data_get($item->good_type_site, 'type_name') . ' ' . $item->name)
                    ->setDescription($item->description);

                if ($item->getActivePrice() < $item->getStrikedPrice()) {
                    $offer->setOldPrice($item->getStrikedPrice());
                }

                if (implode('_', $this->categoriesExclude) == '12') {
                    $offer->setSalesNotes('Предоплата 100%');
                } else {
                    $offer->setSalesNotes('Предоплата от 10%');
                }

                if ($item->delivery_cost > 0) {
                    $offer->setDelivery(true);
                    $offer->addDelivery((new Delivery())
                        ->setCost($item->getDeliveryCost()));
                }


                $item->detail_images->each(function ($image) use (&$offer) {
                    $offer->addPicture($image->getSource());
                });

                foreach ($item->getDescriptions() as $name => $value) {
                    $param = new OfferParam();
                    $param->setName($name);
                    $param->setValue($value);

                    $offer->addParam($param);
                }

                $offers[] = $offer;
            });

        $this->generator->generate(
            $shopInfo,
            $currencies,
            $categories,
            $offers,
            $deliveries
        );
    }

    /**
     * Return directory name for cache files
     * @return string
     */
    protected function getDirectory()
    {
        $dirSuffix = '';
        if (count($this->categoriesExclude) > 0) {
            $dirSuffix = '_ce_' . implode('_', $this->categoriesExclude);
        }

        return 'markets/yandex' . $dirSuffix;
    }
}
