<?php

namespace LuckyWeb\MS\Classes\Markets\MailTorg;

use Bukashk0zzz\YmlGenerator\Model\Category;
use Bukashk0zzz\YmlGenerator\Model\Currency;
use Bukashk0zzz\YmlGenerator\Model\Offer\OfferParam;
use Bukashk0zzz\YmlGenerator\Model\Offer\OfferSimple;
use Bukashk0zzz\YmlGenerator\Settings;

use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use LuckyWeb\MS\Classes\Markets\MailTorg\Model\ShopInfo;
use LuckyWeb\MS\Classes\Markets\Traits\ManagerStorageCache;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\Category as CategoryModel;

class TorgManager
{
    use ManagerStorageCache;

    /**
     * @var Generator
     */
    protected $generator;

    /**
     * Current export file name
     * @var string
     */
    protected $filename;

    /**
     * YmlManager constructor.
     */
    public function __construct()
    {
        $this->filename = 'export_' . Carbon::now()->timestamp . '.xml';
        $this->storageDirectory = 'markets/mailtorg';

        $settings = new Settings();
        $settings->setEncoding('utf-8');
        $settings->setOutputFile($this->storageDirectory . '/' . $this->filename);

        $this->generator = new Generator($settings);
    }

    /**
     * Generate and return YML
     */
    public function generate()
    {
        // Creating ShopInfo object (https://yandex.ru/support/webmaster/goods-prices/technical-requirements.xml#shop)
        $shopInfo = (new ShopInfo())
            ->setName('Мебель Шара')
            ->setCompany('ООО Мебель Шара')
            ->setUrl('https://www.mebelshara.ru');

        // Creating currencies array (https://yandex.ru/support/webmaster/goods-prices/technical-requirements.xml#currencies)
        $currencies = [];
        $currencies[] = (new Currency())
            ->setId('RUR')
            ->setRate(1);

        // Creating categories array (https://yandex.ru/support/webmaster/goods-prices/technical-requirements.xml#categories)
        $categories = [];
        CategoryModel::get()->each(function ($item) use (&$categories) {
            $categories[] = (new Category())
                ->setId($item->id)
                ->setName($item->name);

            $item->subcategories->each(function ($sub) use (&$categories, $item) {
                $categories[] = (new Category())
                    ->setId($item->id * 100 + $sub->id)
                    ->setParentId($item->id)
                    ->setName($sub->name);
            });
        });

        // Creating offers array (https://yandex.ru/support/webmaster/goods-prices/technical-requirements.xml#offers)
        $offers = [];
        Product::available()
            ->get()
            ->each(function ($item) use (&$offers) {
                $offer = (new OfferSimple())
                    ->setId($item->nid->numeric_id)
                    ->setAvailable(in_array($item->residual_indicator, [1, 2]))
                    ->setUrl(url('/catalog/' . $item->category->slug . '/' . $item->slug))
                    ->setPrice($item->getActivePrice())
                    ->setCurrencyId('RUR')
                    ->setCategoryId($item->category->id)
                    ->setPickup(true)
                    ->setName(data_get($item->good_type_site, 'type_name') . ' ' . $item->name)
                    ->setDescription($item->description);

                if ($item->getActivePrice() < $item->getStrikedPrice()) {
                    $offer->setOldPrice($item->getStrikedPrice());
                }

                $item->detail_images->each(function ($image) use (&$offer) {
                    $offer->addPicture($image->getSource());
                });

                foreach ($item->getDescriptions() as $name => $value) {
                    $param = new OfferParam();
                    $param->setName($name);
                    $param->setValue($value);

                    $offer->addParam($param);
                }

                $offers[] = $offer;
            });

        $this->generator->generate(
            $shopInfo,
            $currencies,
            $categories,
            $offers
        );
    }
}
