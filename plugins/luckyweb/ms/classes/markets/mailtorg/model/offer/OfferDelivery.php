<?php namespace LuckyWeb\MS\Classes\Markets\MailTorg\Model\Offer;

use Bukashk0zzz\YmlGenerator\Model\Delivery;
use Bukashk0zzz\YmlGenerator\Model\Offer\OfferSimple;

/**
 * Class OfferSimple
 */
class OfferDelivery extends OfferSimple
{
    /**
     * @var array
     */
    private $deliveries = [];

    /**
     * Add delivery
     *
     * @param Delivery $delivery
     *
     * @return $this
     */
    public function addDelivery(Delivery $delivery)
    {
        if (count($this->deliveries) < 5) {
            $this->deliveries[] = $delivery;
        }

        return $this;
    }

    /**
     * Set deliveries
     *
     * @param array $deliveries
     *
     * @return $this
     */
    public function setDeliveries(array $deliveries)
    {
        $this->deliveries = $deliveries;

        return $this;
    }

    /**
     * Get picture list
     *
     * @return array
     */
    public function getDeliveries()
    {
        return $this->deliveries;
    }
}
