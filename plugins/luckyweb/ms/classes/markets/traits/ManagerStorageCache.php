<?php

namespace LuckyWeb\MS\Classes\Markets\Traits;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

trait ManagerStorageCache
{
    /**
     * Current export file name
     * @var string
     */
    protected $filename;

    /**
     * Path to the storage directory where the file should be stored
     * @var string
     */
    protected $storageDirectory;

    /**
     * Return export file content using some cache logic
     */
    public function getContent()
    {
        $this->storageDirectory = trim($this->storageDirectory, '/');

        if (!Storage::exists($this->storageDirectory)) {
            Storage::makeDirectory($this->storageDirectory);
        }

        $files = collect(Storage::files($this->storageDirectory));

        if ($files->count() > 0) {
            $fileName = $files->first();
            preg_match('/^(.*)_(\d+)\.(.*)$/i', $fileName, $matches);
            $date = array_get($matches, 2, null);

            if (Carbon::createFromTimestamp($date)->diffInDays(Carbon::now()) < 1) {
                // File is actual. Return it.
                return Storage::get($fileName);
            }

            // Remove all old files
            $files->each(function ($name) {
                Storage::delete($name);
            });
        }

        // File doesn't exist or is too old. Generate new and return
        $this->generate();

        return Storage::get($this->storageDirectory.'/'.$this->filename);
    }
}
