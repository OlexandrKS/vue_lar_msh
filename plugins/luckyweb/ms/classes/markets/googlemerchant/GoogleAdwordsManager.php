<?php

namespace LuckyWeb\MS\Classes\Markets\GoogleMerchant;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use League\Csv\Writer;
use LuckyWeb\MS\Classes\Markets\Traits\ManagerStorageCache;
use LuckyWeb\MS\Models\Product;

class GoogleAdwordsManager
{

    use ManagerStorageCache;

    /**
     * @var Generator
     */
    protected $generator;

    /**
     * Current export file name
     * @var string
     */
    protected $filename;

    /**
     * YmlManager constructor.
     */
    public function __construct()
    {
        $this->filename = 'export_' . Carbon::now()->timestamp . '.csv';
        $this->storageDirectory = 'markets/google_adwords';
    }

    /**
     * Generate and return YML
     */
    public function generate()
    {
        $resource = fopen('php://memory', 'w+');
        $filename = $this->storageDirectory . '/' . $this->filename;

        $csv = Writer::createFromStream($resource);
        $csv->setDelimiter(',');
        $csv->insertOne([
            'ID',
            'Item title',
            'Final URL',
            'Image URL',
            'Item subtitle',
            'Item description',
            'Item category',
            'Price',
            'Sale price'
        ]);

        Product::available()
            ->get()
            ->each(function (Product $item) use (&$csv) {
                $csv->insertOne([
                    $item->nid->numeric_id,
                    data_get($item->good_type_site, 'type_name') . ' ' . $item->name,
                    url('/catalog/' . $item->category->slug . '/' . $item->slug),
                    $item->images->count() > 0 ? $item->images->first()->getSource() : '',
                    $item->subname,
                    $item->description,
                    $item->category->name,
                    $item->getStrikedPrice() . ' RUB',
                    ($item->getActivePrice() < $item->getStrikedPrice()) ? $item->getActivePrice() . ' RUB' : ''
                ]);
            });

        Storage::put($filename, $resource);
        fclose($resource);
    }
}
