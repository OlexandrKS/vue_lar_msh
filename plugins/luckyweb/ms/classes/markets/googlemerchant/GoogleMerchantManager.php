<?php

namespace LuckyWeb\MS\Classes\Markets\GoogleMerchant;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use LuckyWeb\MS\Classes\Markets\Traits\ManagerStorageCache;
use LuckyWeb\MS\Models\Product;
use LukeSnowden\GoogleShoppingFeed\Containers\GoogleShopping;
use LukeSnowden\GoogleShoppingFeed\Item;

class GoogleMerchantManager
{
    use ManagerStorageCache;

    const DEFAULT_DESCRIPTION = 'Официальный сайт Мебель Шара | 55 фирменных салона. Европейский ЭКО стандарт качества от группы компаний Imperial. Гарантия 36 месяцев.';

    /**
     * Current export file name
     * @var string
     */
    protected $filename;

    /**
     * YmlManager constructor.
     */
    public function __construct()
    {
        $this->filename = 'export_' . Carbon::now()->timestamp . '.xml';
        $this->storageDirectory = 'markets/google_merchant';
    }

    /**
     * Generate and return YML
     */
    public function generate()
    {
        GoogleShopping::title('Мебель Шара');
        GoogleShopping::link('https://www.mebelshara.ru');
        GoogleShopping::setIso4217CountryCode('RUB');

        Product::available()
            ->get()
            ->each(function ($item) {
                $i = GoogleShopping::createItem();
                $i->id($item->nid->numeric_id);
                $i->title(data_get($item->good_type_site, 'type_name') . ' ' . $item->name);
                $i->description($item->description ?: self::DEFAULT_DESCRIPTION);
                $i->link(url('/catalog/' . $item->category->slug . '/' . $item->slug));
                $i->price($item->getActivePrice());
                $i->condition(Item::BRANDNEW);
                $i->availability(in_array($item->residual_indicator, [1, 2]) ? Item::INSTOCK : Item::PREORDER);

                $i->shipping(GoogleShopping::createShipping()
                    ->setPrice(300)
                );

                if ($item->getActivePrice() < $item->getStrikedPrice()) {
                    $i->sale_price($item->getActivePrice());
                }

                $i->customWithNamespace(
                    'sale_price_effective_date',
                    (new \DateTime('first day of this month'))->format('Y-m-d\T00:00O') . '/' . (new \DateTime('last day of this month'))->format('Y-m-d\T00:00O')
                );

                if ($item->detail_images->count() > 0) {
                    $i->image_link($item->detail_images->first()->getSource());

                    if ($item->detail_images->count() > 1) {
                        $i->additional_image_link($item->detail_images->splice(1)->map(function ($image) {
                            return $image->getSource();
                        })->toArray());
                    }
                }

                $i->color($item->subname);
                $i->brand($item->furnisher->name);
            });

        Storage::put($this->storageDirectory . '/' . $this->filename, GoogleShopping::asRss(false));
    }
}
