<?php

namespace LuckyWeb\MS\Classes\Markets\SmartBanner;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use LuckyWeb\MS\Classes\Markets\Traits\ManagerStorageCache;
use LuckyWeb\MS\Models\Product;
use XMLWriter;

class SmartBannerManager
{
    use ManagerStorageCache;

    /**
     * @var XMLWriter
     */
    protected $writer;

    /**
     * YmlManager constructor.
     */
    public function __construct()
    {
        $this->filename = 'export_' . Carbon::now()->timestamp . '.yml';
        $this->storageDirectory = 'markets/smartbanner';

        $this->writer = new XMLWriter();
        $this->writer->openMemory();
    }

    /**
     * Generate and return YML
     */
    public function generate()
    {
        $this->writer->startDocument('1.0', 'utf-8');
        $this->writer->startElement('document');

        Product::available()
            ->get()
            ->each(function ($item) {
                $this->writer->startElement('offer');
                $this->writer->writeAttribute('id', $item->nid->numeric_id);
                $this->writer->writeAttribute('available', in_array($item->residual_indicator, [1, 2]) ? 'true' : 'false');

                $this->writer->writeElement('url', url('/catalog/' . $item->category->slug . '/' . $item->slug));
                $this->writer->writeElement('price', $item->getActivePrice());
                if ($item->getStrikedPrice() > $item->getActivePrice()) {
                    $this->writer->writeElement('oldprice', $item->getStrikedPrice());
                }
                $this->writer->writeElement('currencyId', 'RUB');
                $this->writer->writeElement('categoryId', $item->category_id);
                $this->writer->writeElement('picture', $item->images->first()->getSource());
                $this->writer->writeElement('store', 'true');
                $this->writer->writeElement('pickup', 'pickup');
                $this->writer->writeElement('delivery', 'false');
                $this->writer->writeElement('name', data_get($item->good_type_site, 'type_name') . ' ' . $item->name);
                $this->writer->writeElement('description', $item->description);
                $this->writer->writeElement('sales_notes', 'Оплата: Наличные, Б/Н, пластиковые карты, кредит');
                $this->writer->writeElement('manufacturer_warranty', 'true');
                $this->writer->writeElement('country_of_origin', 'Россия');

                $this->writer->endElement();
            });

        $this->writer->fullEndElement();
        $this->writer->endDocument();

        Storage::put($this->storageDirectory . '/' . $this->filename, $this->writer->outputMemory());
    }
}
