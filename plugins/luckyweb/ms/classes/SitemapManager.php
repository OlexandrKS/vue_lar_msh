<?php

namespace LuckyWeb\MS\Classes;

use App\Backend\Entities\Constructor\Page;
use Luckyweb\Customcontent\Models\Page as LegacyPage;
use LuckyWeb\MS\Models\Category;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\ShopCity;
use LuckyWeb\MS\Models\SubCategory;

/**
 * Generates sitemap.xml
 */

class SitemapManager {

    protected $rootUrls = [
        ['url' => '/', 'priority' => '1'],
//        ['url' => '/catalog/mega-skidki', 'priority' => '0.5'],
        ['url' => '/contacts', 'priority' => '0.75'],
        ['url' => '/otzyvy', 'priority' => '0.75'],
        ['url' => '/contacts', 'priority' => '0.5']
    ];

    /**
     * Generates sitemap.xml content
     * @return string
     */
    public function generate()
    {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
            .$this->urlset(
                $this->urls(
                    $this->getUrls()));
    }

    /**
     * Generates <urlset> tag with content
     * @param string $content
     * @return string
     */
    protected function urlset($content = '')
    {
        return '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" '
            //.'xmlns:video="http://www.google.com/schemas/sitemap-video/1.1"'
            .'>'
            ."\n".$content.'</urlset>';
    }

    /**
     * Generates <url> and <loc> tags for given url with extended content
     * @param string $url - Page url
     * @param string $extended - Extended content
     * @return string
     */
    protected function url($url = '', $extended = '')
    {
        return "<url>".($url != '' ? '<loc>'.url($url).'</loc>' : '').$extended."</url>";
    }

    /**
     * Generates multiple <url> tags for given array of urls
     * @param array $urls - Page urls
     * @return string
     */
    protected function urls(array $urls)
    {
        return implode("\n", array_map(function ($url) {
            if (is_array($url)) {
                $extended = $this->priority($url);
                $extended .= $this->lastmod($url);
                $extended .= $this->images($url);

                return $this->url($url['url'], $extended);
            }
            else {
                return $this->url($url);
            }
        }, $urls));
    }

    /**
     * Write image tag
     * @param $image
     * @return string
     */
    protected function image($image)
    {
        $src = is_string($image) ? $image : $image['src'];
        $caption = is_array($image) && array_key_exists('caption', $image) ? $image['caption'] : null;
        return "<image:image>"
            ."<image:loc>".$src."</image:loc>"
            .($caption ? "<image:caption>".$caption."</image:caption>" : "")
            ."</image:image>";
    }

    /**
     * Write images
     * @param array $data
     * @return string
     */
    protected function images($data)
    {
        $images = array_get($data, 'images', []);
        if(empty($images)) {
            return '';
        }

        return implode("\n", array_map(function ($image) {
            return $this->image($image);
        }, $images));
    }

    /**
     * Write priority tag
     * @param array $data
     * @return string
     */
    protected function priority($data)
    {
        $priority = array_get($data, 'priority', []);

        if(empty($priority)) {
            return '';
        }

        return "<priority>".$priority."</priority>\n";
    }

    /**
     * Write lastmod tag
     * @param array $data
     * @return string
     */
    protected function lastmod($data)
    {
        $lastmod = array_get($data, 'lastmod', []);

        if(empty($lastmod)) {
            return '';
        }

        return "<lastmod>".$lastmod."</lastmod>\n";
    }

    /**
     * Return urls for sitemap
     * @return array
     */
    protected function getUrls()
    {
        return array_merge(
            $this->rootUrls,
            ShopCity::getSitemapUrls(),
//            Category::getSitemapUrls(),
//            SubCategory::getSitemapUrls(),
            Product::getSitemapUrls(),
            $this->getCustomContentPages()
        );
    }

    /**
     * Return array of custom content pages.
     * @return mixed
     */
    protected function getCustomContentPages()
    {
        $legacy = LegacyPage::whereNotNull('slug')
            ->whereNotNull('breadcrumbs')
            ->get()
            ->map(function($item){
            return [
                'url' => $item->slug,
                'priority' => '0.5',
                'lastmod' => $item->updated_at->toDateString()
            ];
        })->toArray();

        $page = Page::whereNotNull('slug')
            ->whereNotNull('breadcrumbs')
            ->get()
            ->map(function($item) {
                return [
                    'url' => $item->slug,
                    'priority' => '0.5',
                    'lastmod' => $item->updated_at->toDateString()
                ];
            })->toArray();

        return array_merge($legacy, $page);
    }
}
