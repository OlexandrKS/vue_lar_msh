<?php

namespace LuckyWeb\MS\Classes;

use Exception;
use Illuminate\Support\Facades\Config;
use Sendpulse\RestApi\ApiClient;
use Sendpulse\RestApi\Storage\FileStorage;

class SendPulseManager
{
    protected $obj;

    const BOOK_ID = 2334912;

    public function __construct()
    {
        try {
            $this->obj = new ApiClient(Config::get('sendpulse.client_id'), Config::get('sendpulse.secret_key'), new FileStorage());
        } catch (Exception $e) {
        }
    }

    public function getEmailsFromBook($book_id = null)
    {
        $book_id = $book_id ?? self::BOOK_ID;
        return $this->obj->getEmailsFromBook($book_id);
    }

    public function addEmailsInBook($emails)
    {
        return $this->obj->addEmails(self::BOOK_ID, $emails);
    }

    public function getEmailInfo($email)
    {
        return $this->obj->getEmailInfo(self::BOOK_ID, $email);
    }

    public function getEmailGlobalInfo($email)
    {
        return $this->obj->getEmailGlobalInfo($email);
    }

}
