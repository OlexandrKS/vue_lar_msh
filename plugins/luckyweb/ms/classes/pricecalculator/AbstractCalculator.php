<?php

namespace LuckyWeb\MS\Classes\PriceCalculator;

class AbstractCalculator
{
    /**
     * @return string
     */
    public static function name()
    {
        return str_slug(class_basename(static::class));
    }
}
