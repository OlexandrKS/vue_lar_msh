<?php

namespace LuckyWeb\MS\Classes\PriceCalculator;

use LuckyWeb\MS\Models\Product;
use October\Rain\Database\Collection;

class Factory
{
    /**
     * @param Collection $products
     * @param Collection $count
     * @param Collection $supportProducts
     * @return Collection
     */
    public static function transformDataForPriceCalculator($products, $count, $supportProducts)
    {
        $i = 0;

        /** @var Product $product */
        foreach ($products as $product) {
            $product->buy_count = $count[$i];
            $product->buy_support = $supportProducts->get($product->getKey(), 0);

            $i++;
        }

        return $products;
    }
}
