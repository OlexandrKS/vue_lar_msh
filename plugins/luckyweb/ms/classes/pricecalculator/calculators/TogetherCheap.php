<?php

namespace LuckyWeb\MS\Classes\PriceCalculator\Calculators;

use Illuminate\Support\Facades\Cookie;
use LuckyWeb\MS\Classes\PriceCalculator\AbstractCalculator;
use LuckyWeb\MS\Classes\PriceCalculator\OrderPrice;
use Luckyweb\Ms\Components\Cart;
use Luckyweb\Ms\Models\FavoriteList;
use Luckyweb\Ms\Models\FavoriteListItem;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\User\Facades\Auth;

class TogetherCheap extends AbstractCalculator
{
    /**
     * @param OrderPrice $price
     * @param $next
     * @return mixed
     */
    public function handle(OrderPrice $price, $next)
    {
        $upTotal = 0;

        $price->products()->each(function (Product $product) use ($price, &$upTotal) {
            if ($product->buy_support) {
                $price->activate($product->getKey(), static::name());
                $product->buy_active_price = $product->price_special;
                $product->buy_is_special = true;

                // Расчитываем прибавку к общей суме исходя из количества дополнительных товаров
                $cheapCount = $product->buy_support > $product->buy_count ? $product->buy_count : $product->buy_support;
                $withoutCheep = $product->buy_count - $cheapCount;
                $upTotal += ($product->getActivePrice() - $product->buy_active_price) * $withoutCheep;
            }
        });

        /** @var OrderPrice $result */
        $result = $next($price);

        $result->updateTotalPrice($upTotal);

        return $result;
    }
}
