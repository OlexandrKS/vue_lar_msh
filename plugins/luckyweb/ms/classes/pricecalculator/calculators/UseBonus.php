<?php

namespace LuckyWeb\MS\Classes\PriceCalculator\Calculators;

use LuckyWeb\MS\Classes\PriceCalculator\OrderPrice;

class UseBonus
{
    public function handle(OrderPrice $price, $next)
    {
        /** @var OrderPrice $result */
        $result = $next($price);

        if ($result->getWithoutSpecialsPrice() >= 15000) {
            $bonuses = (int) request()->input('use_bonus', 0);
            $bonuses = $bonuses > 5000 ? 5000 : $bonuses; // Max bonuses

            $result->setUseBonuses($bonuses);
        }

        return $result;
    }
}
