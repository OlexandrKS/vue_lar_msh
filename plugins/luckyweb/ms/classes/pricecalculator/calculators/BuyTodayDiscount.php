<?php

namespace LuckyWeb\MS\Classes\PriceCalculator\Calculators;

use Carbon\Carbon;
use LuckyWeb\MS\Classes\PriceCalculator\AbstractCalculator;
use LuckyWeb\MS\Classes\PriceCalculator\OrderPrice;

class BuyTodayDiscount extends AbstractCalculator
{
    const COOKIE_NAME = 'fb_cal';
    const SESSION_KEY = 'first_buy_price';

    /**
     * @param OrderPrice $price
     * @param $next
     * @return OrderPrice
     */
    public function handle(OrderPrice $price, $next)
    {
        /** @var OrderPrice $result */
        $result = $next($price);

        if (! request()->hasCookie(static::COOKIE_NAME)) {
            return $result;
        }

        $dto = json_decode(request()->cookie(static::COOKIE_NAME), true);
        $now = Carbon::now();

        if ($now->lessThan(Carbon::createFromTimestamp($dto['expired']))
            && $dto['used'] === false &&
            $result->getUseBonuses() == 0 // вместе с бонусами нельзя использовать
        ) {
            $total = $result->getWithoutSpecialsPrice();
            $percent = $total * 0.05; // скидка на Н процентов
            $sale = ceil($percent / 50) * 50; // округляем до 50 рублей

            $result->updateBuyTodayDiscount($sale);
        }

        return $result;
    }
}
