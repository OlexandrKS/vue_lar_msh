<?php

namespace LuckyWeb\MS\Classes\PriceCalculator\Calculators;

use LuckyWeb\MS\Classes\PriceCalculator\AbstractCalculator;
use LuckyWeb\MS\Classes\PriceCalculator\OrderPrice;
use LuckyWeb\MS\Models\Product;

class Special extends AbstractCalculator
{
    /**
     * @param OrderPrice $price
     * @param $next
     * @return mixed
     */
    public function handle(OrderPrice $price, $next)
    {
        $price->products()->each(function (Product $product) use ($price) {
            if ($price->isActive($product->getKey(), TogetherCheap::name())) {
                return;
            }

            // Если у товара активная скидка больше 50% - он является акционным
            if ($product->getActiveDiscount() >= 50) {
                $price->activate($product->getKey(), static::name());

                $product->buy_active_price = $product->getActivePrice();
                $product->buy_is_special = true; // Помечаем товар акционным
            }
        });

        return $next($price);
    }
}
