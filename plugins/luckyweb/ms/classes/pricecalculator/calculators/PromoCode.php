<?php

namespace LuckyWeb\MS\Classes\PriceCalculator\Calculators;

use Carbon\Carbon;
use LuckyWeb\MS\Classes\PriceCalculator\AbstractCalculator;
use LuckyWeb\MS\Classes\PriceCalculator\OrderPrice;
use LuckyWeb\User\Facades\Auth;
use LuckyWeb\User\Models\PromoCodeEvent;

class PromoCode extends AbstractCalculator
{
    /**
     * @param OrderPrice $price
     * @param $next
     * @return mixed
     */
    public function handle(OrderPrice $price, $next)
    {
        /** @var OrderPrice $result */
        $result = $next($price);

        $promoCode = request()->input('promo_code');

        if ($promoCode && Auth::check()) {
            $total = $result->getWithoutSpecialsPrice();

            $model = PromoCodeEvent::available(Auth::getUser())
                ->where('code', $promoCode)->first();

            if ($model instanceof PromoCodeEvent && $total >= $model->promo_code->available_from) {
                $price->setPromoCodeDiscount((float)$model->promo_code->amount);
            }
        }

        return $result;
    }
}
