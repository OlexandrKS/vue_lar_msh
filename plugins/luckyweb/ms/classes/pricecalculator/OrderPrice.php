<?php

namespace LuckyWeb\MS\Classes\PriceCalculator;

use Illuminate\Pipeline\Pipeline;
use LuckyWeb\MS\Classes\PriceCalculator\Calculators\BuyTodayDiscount;
use LuckyWeb\MS\Classes\PriceCalculator\Calculators\PromoCode;
use LuckyWeb\MS\Classes\PriceCalculator\Calculators\Special;
use LuckyWeb\MS\Classes\PriceCalculator\Calculators\TogetherCheap;
use LuckyWeb\MS\Classes\PriceCalculator\Calculators\UseBonus;
use LuckyWeb\MS\Models\Product;
use October\Rain\Database\Collection;

class OrderPrice
{
    /**
     * @var array
     */
    private $calculators = [
        TogetherCheap::class,
        Special::class,
        UseBonus::class,
        PromoCode::class,
        BuyTodayDiscount::class,
    ];

    /**
     * @var array
     */
    private $active = [];

    /**
     * @var int
     */
    private $delivery = 0;

    /**
     * @var int
     */
    private $assembling = 0;

    /**
     * @var int
     */
    private $total = 0;

    /**
     * @var int
     */
    private $withoutSpecials = 0;

    /**
     * @var int
     */
    private $useBonuses = 0;

    /**
     * Temp property
     * TODO move to another location
     *
     * @var int
     */
    private $buyTodayDiscount = 0;

    /**
     * @var float
     */
    private $promoCodeDiscount = 0;

    /**
     * @var Collection
     */
    private $products;

    /**
     * @return Collection
     */
    public function products()
    {
        return $this->products;
    }

    /**
     * @param Collection $products
     */
    private function fillProductPrices(Collection $products)
    {
        $products->each(function (Product $product) {
            $product->buy_active_price = $product->price_lower_b;

            $this->updateDeliveryPrice($product->getDeliveryCost() * $product->buy_count);
            $this->updateAssemblingPrice($product->getAssemblingCost() * $product->buy_count);
        });
    }

    /**
     * @param string $calculator
     * @param $productId
     * @return bool
     */
    public function isActive($productId, $calculator)
    {
        return array_search($calculator, array_get($this->active, $productId, []));
    }

    /**
     * Пометить, что продукт использует опеределный модификатор расчета
     *
     * @param $productId
     * @param string $calculator
     */
    public function activate($productId, $calculator)
    {
        if (! array_key_exists($productId, $this->active)) {
            $this->active[$productId] = [];
        }

        $this->active[$productId][] = $calculator;
    }

    /**
     * Расчитываем цену заказа используя пайплайн (middleware-like style)
     *
     * @param Collection $products
     * @return mixed
     */
    public function calculate(Collection $products)
    {
        $this->unsetAssemblingPrice();
        $this->unsetDeliveryPrice();
        $this->active = [];
        $this->products = $products;
        $this->fillProductPrices($products);

        $pipe = new Pipeline(app());

        $price = $pipe->send($this)
            ->through($this->calculators)
            ->then($this->response());

        return $price;
    }

    /**
     * Метод, считающий суму заказа
     *
     * @return \Closure
     */
    public function response()
    {
        return function (OrderPrice $priceOrder) {
            $this->products->each(function (Product $product) {
                $this->total += $product->buy_active_price * $product->buy_count;

                // Расчет сумы товаров, на которых не применилась ни одна скидка
                if (! $product->buy_is_special) {
                    $this->withoutSpecials += $product->price_lower_b * $product->buy_count;
                }
            });

            return $priceOrder;
        };
    }

    /**
     * @param int $price
     */
    public function updateDeliveryPrice($price = 0)
    {
        $this->delivery += $price;
    }

    /**
     * @return void
     */
    public function unsetDeliveryPrice()
    {
        $this->delivery = 0;
    }

    /**
     * @param int $price
     */
    public function updateAssemblingPrice($price = 0)
    {
        $this->assembling += $price;
    }

    /**
     * @return void
     */
    public function unsetAssemblingPrice()
    {
        $this->assembling = 0;
    }

    /**
     * @return int
     */
    public function getDeliveryPrice()
    {
        return $this->delivery;
    }

    /**
     * @return int
     */
    public function getAssemblingPrice()
    {
        return $this->assembling;
    }

    /**
     * @return int
     */
    public function getTotalPrice()
    {
        return $this->total;
    }

    /**
     * @param int $price
     */
    public function updateTotalPrice($price = 0)
    {
        $this->total += $price;
    }

    /**
     * Сумма заказа товаров без специальных акций и скидок.
     *
     * @return int
     */
    public function getWithoutSpecialsPrice()
    {
        return $this->withoutSpecials;
    }

    /**
     * @param int $price
     */
    public function updateWithoutSpecialsPrice($price = 0)
    {
        $this->withoutSpecials += $price;
    }

    /**
     * @param string $productId
     * @return Product
     */
    public function getPriceByProductId($productId)
    {
        return $this->products->find($productId);
    }

    /**
     * @return int
     */
    public function getUseBonuses()
    {
        return $this->useBonuses;
    }

    /**
     * @param int $useBonuses
     */
    public function setUseBonuses($useBonuses)
    {
        $this->useBonuses = $useBonuses;
    }

    /**
     * @return int
     */
    public function getBuyTodayDiscount()
    {
        return $this->buyTodayDiscount;
    }

    /**
     * @param int $discount
     */
    public function updateBuyTodayDiscount($discount = 0)
    {
        $this->buyTodayDiscount += $discount;
    }

    /**
     * @return float
     */
    public function getPromoCodeDiscount()
    {
        return $this->promoCodeDiscount;
    }

    /**
     * @param int $discount
     */
    public function setPromoCodeDiscount($discount = 0)
    {
        $this->promoCodeDiscount = $discount;
    }
}
