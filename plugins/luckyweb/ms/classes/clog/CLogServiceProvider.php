<?php namespace LuckyWeb\MS\Classes\CLog;

use October\Rain\Support\ServiceProvider;

class CLogServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CLog::class, function ($app) {
            return new CLog();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [CLog::class];
    }

}
