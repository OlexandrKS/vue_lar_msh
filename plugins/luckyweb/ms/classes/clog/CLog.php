<?php namespace LuckyWeb\MS\Classes\CLog;

/**
 * Dumps variables to console
 * @deprecated
 */
class CLog
{
    public function write(...$msgs)
    {
        $parts = [];
        foreach ($msgs as $msg) {
            $parts[] = is_array($msg) || is_object($msg) ? print_r($msg, true) : $msg;
        }
        echo implode(' ', $parts)."\n";
    }
}
