<?php namespace LuckyWeb\MS\Classes\Exchange;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use LuckyWeb\User\Models\BonusEvent;
use LuckyWeb\User\Models\PromoCodeEvent;
use LuckyWeb\User\Models\User;

class Exporter
{
    /**
     * Array of application field names that must be not exported
     * @var array
     */
    protected $exportUserFieldsExcept = [
        'password',
        'activation_code',
        'persist_code',
        'reset_password_code',
        'permissions',
        'is_activated',
        'activated_at',
        'last_login',
        'last_seen',
        'is_guest',
        'is_superuser',
        'id',
        'external_id',
        'extended',
        'is_exported',
        'exported_at',
        'questionary_exported_at'
    ];

    /**
     * Export all available for export applications
     * @param callable $callback
     */
    public function exportUsers($callback)
    {
        // Get applications need to export
        $users = User::selectRaw('*, id as front_user_id, external_id as 1c_user_id')
            ->where('is_exported', false)
            ->where('is_activated', true)
            ->get();

        // Set hidden fields
        $data = [];
        foreach ($users as $user) {
            $user->addHidden($this->exportUserFieldsExcept);
            $data[] = array_merge($user->toArray(), array_get($user->extended, 'additional', []));
        }

        if($callback(collect($data))) {
            $this->usersExported($users);
        }
    }

    /**
     * Make necessary actions after users exported
     * @param Collection $items
     */
    protected function usersExported(Collection $items)
    {
        // Set exported flag and timestamp
        $items->each(function($item){
            $item->is_exported = true;
            $item->exported_at = Carbon::now();
            $item->save();
        });
    }


    /**
     * Export all available questionaries
     * @param callable $callback
     */
    public function exportQuestionaries($callback)
    {
        // Get applications need to export
        $users = User::selectRaw('*, id as front_user_id, external_id as 1c_user_id')
            ->where('is_questionary_exported', false)
            ->where('is_activated', true)
            ->get();

        // Set hidden fields
        $data = [];
        foreach ($users as $user) {
            $questionary = array_get($user->extended,'questionary');
            if(!empty($questionary)) {
                $data[] = [
                    'id' => $user->id ? $user->id : $user->external_id,
                    'front_user_id' => $user->id,
                    '1c_user_id' => $user->external_id,
                    'data' => $this->formatQuestionaryData($questionary)
                ];
            }
        }

        if($callback(collect($data))) {
            $this->questionariesExported($users);
        }
    }

    /**
     * Format questionary data to 1C format
     * @param array $data
     * @return array
     */
    protected function formatQuestionaryData(array $data)
    {
        $formattedData = [];
        foreach ($data as $groupKey => $group) {
            foreach ($group['items'] as $type => $items) {
                foreach ($items as $itemKey) {
                    $formattedItem = [
                        'code_group' => $groupKey,
                        'code_item' => intval($itemKey),
                    ];

                    if (!empty(array_get($group, 'clarification.'.$itemKey))) {
                        $formattedItem['clarification'] = $group['clarification'][$itemKey];
                    }
                    $formattedData[] = $formattedItem;
                }
            }
        }

        return $formattedData;
    }

    /**
     * Make necessary actions after questionaries exported
     * @param Collection $items
     */
    protected function questionariesExported(Collection $items)
    {
        // Set exported flag and timestamp
        $items->each(function($item){
            $item->is_questionary_exported = true;
            $item->questionary_exported_at = Carbon::now();
            $item->save();
        });
    }

    /**
     * Export bonus events
     * @param $callback
     */
    public function exportBonusEvents($callback)
    {
        $bonuses = BonusEvent::whereNull('imported_at')
            ->whereNull('exported_at')
            ->whereNotNull('applied_at')
            ->with('user')
            ->get();

        $formatedBonuses = $bonuses->map(function($item) {
            return [
                'id' => $item->id,
                'front_user_id' => $item->user_id,
                '1c_user_id' => $item->user->external_id,
                'amount' => $item->amount,
                'type_id' => $item->type_id,
                'description' => $item->description,
                'datebonusend' => $item->expired_at->format('d.m.Y'),
                'applied_at' => $item->applied_at ? $item->applied_at->toDateTimeString() : null,
                'deleted_at' => $item->deleted_at ? $item->deleted_at->toDateTimeString() : null
            ];
        });

        if($callback(collect($formatedBonuses))) {
            $this->bonusEventsExported($bonuses);
        }
    }

    /**
     * Make necessary actions after questionaries exported
     * @param Collection $items
     */
    protected function bonusEventsExported(Collection $items)
    {
        // Set exported flag and timestamp
        $items->each(function($item){
            $item->exported_at = Carbon::now();
            $item->save();
        });
    }

    /**
     * @param callable $callback
     */
    public function exportPromoCodes(callable $callback)
    {
        /** @var \October\Rain\Database\Collection $events */
        $events = PromoCodeEvent::query()
            ->whereNull('exported_at')
            ->with('user', 'promo_code')
            ->get();

        $transformed = $events->map(function (PromoCodeEvent $event) {
            return [
                'front_user_id' => $event->user->getKey(),
                '1c_user_id' => $event->user->external_id,
                'discountamount' => $event->promo_code->amount,
                'limitoforder' => $event->promo_code->available_from,
                'discountname' => $event->promo_code->name,
                'datebegin' => $event->promo_code->start_at->format('d.m.Y'),
                'dateend' => $event->promo_code->expired_at->format('d.m.Y'),
                'promocode' => $event->code,
            ];
        });

        if (call_user_func($callback, $transformed)) {
            $this->promoCodesExported($events);
        }
    }

    /**
     * @param Collection $collection
     */
    private function promoCodesExported(Collection $collection)
    {
        $ids = $collection->pluck('id');
        $now = Carbon::now();

        PromoCodeEvent::query()
            ->whereIn('id', $ids)
            ->update([
                'exported_at' => $now,
                'updated_at' => $now,
            ]);
    }
}
