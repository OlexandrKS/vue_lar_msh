<?php

namespace LuckyWeb\MS\Classes\Exchange;

use Cms\Models\MaintenanceSetting;
use LuckyWeb\MS\Classes\Exchange\Providers\ExchangeProvider;
use Illuminate\Support\Collection;
use CL;

class Exchange
{
    const TYPE_USER = 1;
    const TYPE_BONUS_EVENT = 2;
    const TYPE_BONUSES = 3;
    const TYPE_QUESTIONARY_STRUCTURE = 4;
    const TYPE_QUESTIONARY = 5;
    const TYPE_ORDERS = 6;
    const TYPE_PROMO_CODE = 8;

    /**
     * @var ExchangeProvider
     */
    protected $provider;

    /**
     * @var Importer
     */
    protected $importer;

    /**
     * @var Exporter
     */
    protected $exporter;


    public function __construct()
    {
        $this->provider = (new ExchangeManager())->driver();
        $this->importer = new Importer();
        $this->exporter = new Exporter();
    }

    /**
     * Import user profiles from 1C
     */
    public function importUsers()
    {
        CL::write('importUsers');
        // Maintenance mode enabled. Do nothing.
        if(MaintenanceSetting::instance()->is_enabled) {
            return false;
        }

        $result = $this->provider->import(static::TYPE_USER, function(array $data){
            return $this->importer->importUser($data);
        });

        return $result;
    }

    /**
     * Import user bonuses events from 1C
     */
    public function importBonusEvents()
    {
        CL::write('importBonusEvents');
        // Maintenance mode enabled. Do nothing.
        if(MaintenanceSetting::instance()->is_enabled) {
            return false;
        }

        $result = $this->provider->import(static::TYPE_BONUS_EVENT, function(array $data){
            return $this->importer->importBonusEvent($data);
        });

        return $result;
    }

    /**
     * Import user calculated bonuses
     */
    public function importBonuses()
    {
        CL::write('importBonuses');
        // Maintenance mode enabled. Do nothing.
        if(MaintenanceSetting::instance()->is_enabled) {
            return false;
        }

        $result = $this->provider->import(static::TYPE_BONUSES, function(array $data){
            return $this->importer->importBonuses($data);
        });

        return $result;
    }

    /**
     * Import questionary structure
     * @return bool
     */
    public function importQuestionaryStructure()
    {
        CL::write('importQuestionaryStructure');
        // Maintenance mode enabled. Do nothing.
        if(MaintenanceSetting::instance()->is_enabled) {
            return false;
        }

        $result = $this->provider->import(static::TYPE_QUESTIONARY_STRUCTURE, function(array $data){
            return $this->importer->importQuestionaryStructure($data);
        });

        return $result;
    }

    /**
     * Import questionary structure
     * @return bool
     */
    public function importQuestionary()
    {
        CL::write('importQuestionary');
        // Maintenance mode enabled. Do nothing.
        if(MaintenanceSetting::instance()->is_enabled) {
            return false;
        }

        $result = $this->provider->import(static::TYPE_QUESTIONARY, function(array $data){
            return $this->importer->importQuestionary($data);
        });

        return $result;
    }

    /**
     * Import orders
     * @return bool
     */
    public function importOrders()
    {
        CL::write('importOrders');
        // Maintenance mode enabled. Do nothing.
        if(MaintenanceSetting::instance()->is_enabled) {
            return false;
        }

        $result = $this->provider->import(static::TYPE_ORDERS, function(array $data){
            return $this->importer->importOrder($data);
        });

        return $result;
    }

    /**
     * Export user profiles to 1C
     */
    public function exportUsers()
    {
        // Maintenance mode enabled. Do nothing.
        if(MaintenanceSetting::instance()->is_enabled) {
            return false;
        }

        $this->exporter->exportUsers(function(Collection $items) {
           return $this->provider->export(static::TYPE_USER, $items);
        });
    }

    /**
     * Export user questionaries to 1C
     */
    public function exportQuestionaries()
    {
        // Maintenance mode enabled. Do nothing.
        if(MaintenanceSetting::instance()->is_enabled) {
            return false;
        }

        $this->exporter->exportQuestionaries(function(Collection $items) {
            return $this->provider->export(static::TYPE_QUESTIONARY, $items);
        });
    }

    /**
     * Export user bonus events to 1C
     */
    public function exportBonusEvents()
    {
        // Maintenance mode enabled. Do nothing.
        if(MaintenanceSetting::instance()->is_enabled) {
            return false;
        }

        $this->exporter->exportBonusEvents(function(Collection $items) {
            return $this->provider->export(static::TYPE_BONUS_EVENT, $items);
        });
    }

    /**
     * Export promo codes
     */
    public function exportPromoCodes()
    {
        // Maintenance mode enabled. Do nothing.
        if(MaintenanceSetting::instance()->is_enabled) {
            return false;
        }

        $this->exporter->exportPromoCodes(function (Collection $collection) {
            return $this->provider->export(static::TYPE_PROMO_CODE, $collection);
        });
    }

    /**
     * Import by type
     * @param null|int $type
     * @return bool
     */
    public function import($type = null)
    {
        switch($type) {
            case static::TYPE_USER:
                return $this->importUsers();
            case static::TYPE_BONUS_EVENT:
                return $this->importBonusEvents();
            case static::TYPE_BONUSES:
                return $this->importBonuses();
            case static::TYPE_QUESTIONARY_STRUCTURE:
                return $this->importQuestionaryStructure();
            case static::TYPE_QUESTIONARY:
                return $this->importQuestionary();
            case static::TYPE_ORDERS:
                return $this->importOrders();
            default:
                return $this->importAll();
        }
    }

    /**
     * Export by type
     * @param null|int $type
     * @return bool
     */
    public function export($type = null)
    {
        switch($type) {
            case static::TYPE_USER:
                return $this->exportUsers();
            case static::TYPE_QUESTIONARY:
                return $this->exportQuestionaries();
            case static::TYPE_BONUS_EVENT:
                return $this->exportBonusEvents();
            case static::TYPE_PROMO_CODE:
                return $this->exportPromoCodes();
            default:
                return $this->exportAll();
        }
    }

    /**
     * Import all available data
     * @return bool
     */
    protected function importAll()
    {
        return $this->importUsers()
            || $this->importBonusEvents()
            || $this->importBonuses()
            || $this->importQuestionaryStructure()
            || $this->importQuestionary()
            || $this->importOrders();
    }

    /**
     * Export all necessary data
     * @return bool
     */
    protected function exportAll()
    {
        return $this->exportUsers()
            || $this->exportQuestionaries()
            || $this->exportBonusEvents()
            || $this->exportPromoCodes();
    }
}
