<?php namespace LuckyWeb\MS\Classes\Exchange;


use Carbon\Carbon;
use Luckyweb\Ms\Models\FavoriteList;
use LuckyWeb\User\Models\BonusEvent;
use LuckyWeb\User\Models\Order;
use LuckyWeb\User\Models\OrderItem;
use LuckyWeb\User\Models\Settings;
use LuckyWeb\User\Models\User;

class Importer
{
    /**
     * Array of application field names that must be updated
     * @var array
     */
    protected $importUserFieldsOnly = [
        'name',
        'last_name',
        'other_name',
        'email',
        'phone',
        'card_code',
        'card_pin',
        'gender_id',
        'birth_date'
    ];

    /**
     * Array of application field names that must be updated
     * @var array
     */
    protected $importBonusEventFieldsOnly = [
        'type_id',
        'amount',
        'description',
        'applied_at',
        'deleted_at',
    ];

    protected $userSearchAttributes = [
        'email',
        'phone',
        'card_code',
    ];

    /**
     * ExchangeHandler constructor.
     */
    public function __construct()
    {

    }

    /**
     * Import user profile
     * @param array $data
     * @return bool
     */
    public function importUser(array $data)
    {
        if(empty(array_get($data, 'front_user_id'))
            && empty(array_get($data, '1c_user_id'))) {
            return false;
        }

        $importData = array_only($data, $this->importUserFieldsOnly);

        // Search existing user by IDs
        $user = User::searchByIds(array_get($data, 'front_user_id'), array_get($data, '1c_user_id'))->first();
        // Search by attrs if user was not found by IDs
        if (!$user) {
            $user = User::searchByAttributes($this->prepareUserSearchAttributes($importData))->first();
        }

        if($user) {
            $user->update(array_merge($importData, [
                'external_id' => array_get($data, '1c_user_id')
            ]));
        }
        else {
            $user = User::create(array_merge($importData, [
                'external_id' => array_get($data, '1c_user_id')
            ]));

            $fList = new FavoriteList();
            $fList->user_id = $user->getKey();
            $fList->list_name = 'По умолчанию';
            $fList->save();
        }

        return true;
    }

    /**
     * Creates array of search attrs - values to search existing user by imported data
     *
     * @param array $importData             Filtered importData
     *
     * @return array
     */
    protected function prepareUserSearchAttributes($importData)
    {
        $searchAttrs = array_only($importData, $this->userSearchAttributes);

        if (array_key_exists('phone', $searchAttrs)) {
            $searchAttrs['phone'] = preg_replace('/[^0-9]+/u', '', $searchAttrs['phone']);
        }

        return array_map(function ($value) {
            return preg_replace('/\s+/iu', '', $value);
        }, $searchAttrs);
    }

    /**
     * Import bonus events
     * @param array $data
     * @return bool
     */
    public function importBonusEvent(array $data)
    {
        if(empty(array_get($data, 'id'))
            || (empty(array_get($data, '1c_user_id'))
                && empty(array_get($data, 'front_user_id'))
            )) {
            return false;
        }

        $importData = array_only($data, $this->importBonusEventFieldsOnly);
        $importData['user_id'] = array_get($data, 'front_user_id');
        $importData['imported_at'] = Carbon::now();
        if(empty($importData['user_id'])) {
            $user = User::where('external_id', $data['1c_user_id'])->first();
            if(!$user) { // User not found. Do nothing.
                return false;
            }
            $importData['user_id'] = $user->id;
        }

        if(!empty(array_get($data, 'type_id'))) {
            BonusEvent::updateOrCreate([
                'external_id' => array_get($data, 'id'),
                'type_id' => array_get($data, 'type_id'),
                'user_id' => $importData['user_id']
            ], $importData);
        }
        else {
            BonusEvent::where('external_id', array_get($data, 'id'))
                ->where('user_id', $importData['user_id'])
                ->update($importData);
        }


        return true;
    }

    /**
     * Import calculated bonuses
     * @param array $data
     * @return bool
     */
    public function importBonuses(array $data)
    {
        if(empty(array_get($data, 'front_user_id'))
            && empty(array_get($data, '1c_user_id'))) {
            return false;
        }

        $user = User::where('id', array_get($data, 'front_user_id'))
            ->orWhere('external_id', array_get($data, '1c_user_id'))
            ->first();

        if(!$user) { // User not found. Do nothing.
            return false;
        }

        $extended = $user->extended;
        $extended['bonuses']['accrued'] = array_get($data, 'accrued_bonuses');
        $extended['bonuses']['activated'] = array_get($data, 'activated_bonuses');
        $extended['bonuses']['charged'] = array_get($data, 'charged_bonuses');
        $extended['bonuses']['deactivated'] = array_get($data, 'deactivated_bonuses');
        $extended['bonuses']['active_list'] = array_get($data, 'ListOfActive');
        $user->extended = $extended;
        $user->save();

        return true;
    }

    /**
     * Import questionary structure
     * @param array $data
     * @return bool
     */
    public function importQuestionaryStructure(array $data)
    {
        $formattedData = [];
        foreach ($data as $group) {
            $formattedGroup = array_except($group, ['items']);
            foreach ($group['items'] as $item) {
                $formattedGroup['items'][$item['type']][] = $item;
            }
            $formattedData[] = $formattedGroup;
        }

        return Settings::set('questionary_structure', $formattedData);
    }

    /**
     * Import user questionary data
     * @param array $data
     * @return bool
     */
    public function importQuestionary(array $data)
    {
        if(empty(array_get($data, 'front_user_id'))
            && empty(array_get($data, '1c_user_id'))) {
            return false;
        }

        $user = User::where('id', array_get($data, 'front_user_id'))
            ->orWhere('external_id', array_get($data, '1c_user_id'))
            ->first();

        if(!$user) { // User not found. Do nothing.
            return false;
        }

        $formattedData = [];
        foreach ($data['data'] as $item) {
            $formattedData[$item['code_group']]['items'][$item['type']][] = $item['code_item'];

            $clarification = array_get($item, 'clarification');
            if(!empty($clarification)) {
                $formattedData[$item['code_group']]['clarification'][$item['code_item']] = $clarification;
            }
        }

        $extended = $user->extended;
        $extended['questionary'] = $formattedData;
        $user->extended = $extended;
        $user->save();

        return true;
    }

    /**
     * Import user orders
     * @param array $data
     * @return bool
     */
    public function importOrder(array $data)
    {
        if(empty(array_get($data, 'front_user_id'))
            && empty(array_get($data, '1c_user_id'))) {
            return false;
        }

        $user = User::where('id', array_get($data, 'front_user_id'))
            ->orWhere('external_id', array_get($data, '1c_user_id'))
            ->first();

        if(!$user) { // User not found. Do nothing.
            return false;
        }

        // Create or update order
        $importData = [
            'external_id' => array_get($data, 'id'),
            'user_id' => $user->id,
            'agreement_number' => array_get($data, 'Nom_RP'),
            'agreement_date' => array_get($data, 'Date_RP'),
            'delivery_date' => array_get($data, 'Delivery_at'),
            'product_amount' => array_get($data, 'Order_Tovar'),
            'product_debt' => array_get($data, 'Rest_Tovar'),
            'service_amount' => array_get($data, 'Order_Uslugi'),
            'service_paid' => array_get($data, 'Pay_Uslugi'),
            'delivery_amount' => array_get($data, 'Order_Dostavki'),
            'delivery_paid' => array_get($data, 'Pay_Dostavki'),
            'reclamation_amount' => array_get($data, 'Order_Reklam'),
            'reclamation_paid' => array_get($data, 'Pay_Reklam'),
            'total_amount' => array_get($data, 'Order_Total'),
            'total_debt' => array_get($data, 'Rest_Total'),
            'total_paid' => array_get($data, 'Pay_Total'),
            'deleted_at' => array_get($data, 'deleted_at'),
            'applied_at' => array_get($data, 'applied_at'),
        ];

        $order = Order::updateOrCreate([
            'external_id' => array_get($data, 'id')
        ], $importData);

        $this->setExtendedFields($order, $data);

        $goodIds = [];
        $colorIds = [];
        $weights = [
            OrderItem::STATUS_PRODUCTION => 1,
            OrderItem::STATUS_WAIT_PAYMENT => 2,
            OrderItem::STATUS_SHIPMENT => 2,
            OrderItem::STATUS_DELIVERING => 3,
            OrderItem::STATUS_DELIVERY_COORDINATION => 4,
            OrderItem::STATUS_DELIVERED => 5
        ];
        $resultStatus = ['id' => OrderItem::STATUS_DELIVERED, 'weight' => 5];

        foreach (array_get($data, 'Tovar', []) as $key => $item) {
            // For order status detect
            $resultStatus['count'] = array_get($resultStatus,'count', 0)+1;
            if($statusId = array_get($item,'Status')) {
                // Detect worst status
                if ($resultStatus['weight'] > array_get($weights, $statusId, 100)) {
                    $resultStatus['id'] = $statusId;
                    $resultStatus['weight'] = array_get($weights, $statusId, 100);
                }

                // Calculate special statuses count
                if (in_array($statusId, [OrderItem::STATUS_DEFERRED, OrderItem::STATUS_RETURN, OrderItem::STATUS_CANCELED])) {
                    $resultStatus['special'][$statusId] = array_get($resultStatus, 'special.' . $statusId, 0) + 1;
                }
            }

            // Create or update product
            $ids = explode('=', array_get($item,'ProdID'));
            $goodIds[] = array_get($ids,0);
            $colorIds[] = array_get($ids,1);
            $importData = [
                'order_id' => $order->id,
                'good_id' => array_get($ids,0),
                'color_id' => array_get($ids,1),
                'name' => array_get($item,'Product'),
                'color' => array_get($item,'Color'),
                'amount' => array_get($item,'Summa'),
                'quantity' => array_get($item,'Amount'),
                'status_id' => array_get($item,'Status'),
            ];

            OrderItem::updateOrCreate([
                'order_id' => $order->id,
                'good_id' => array_get($ids,0),
                'color_id' => array_get($ids,1)
            ], $importData);
        }

        // Delete items
        OrderItem::where('order_id', $order->id)
            ->where(function($query) use($goodIds, $colorIds) {
                $query->whereNotIn('good_id', $goodIds)
                ->orWhereNotIn('color_id', $colorIds);
            })
            ->delete();

        // Save order status
        $order->status_id = $resultStatus['id'];

        // Check for special status
        if(isset($resultStatus['special'])) {
            foreach ($resultStatus['special'] as $id => $count) {
                if ($count == $resultStatus['count']) {
                    $order->status_id = $id;
                }
            }
        }
        $order->save();

        return true;
    }

    /**
     * @param Order $order
     * @param array $data
     */
    protected function setExtendedFields(Order $order, array $data)
    {
        $extended = $order->extended;

        $extended['purchase_place'] = array_get($data, 'Store_RP');

        $order->extended = $extended;
    }
}
