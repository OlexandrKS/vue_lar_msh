<?php namespace LuckyWeb\MS\Classes\Exchange;


use LuckyWeb\MS\Classes\Exchange\Providers\MysqlProvider;
use Illuminate\Support\Manager;


class ExchangeManager extends Manager
{
    /**
     * Lock constructor.
     */
    public function __construct()
    {
        parent::__construct(app());
    }

    /**
     * Get the default driver name.
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        return config('finplugs.creditup::exchange.provider', 'mysql');
    }

    /**
     * Create an instance of the MysqlProvider
     * @return MysqlProvider
     */
    public function createMysqlDriver()
    {
        return new MysqlProvider();
    }
}