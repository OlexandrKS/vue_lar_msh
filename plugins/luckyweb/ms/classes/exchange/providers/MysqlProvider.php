<?php namespace LuckyWeb\MS\Classes\Exchange\Providers;


use Carbon\Carbon;
use Exception;
use LuckyWeb\MS\Models\ExchangeExport;
use LuckyWeb\MS\Models\ExchangeImport;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class MysqlProvider implements ExchangeProvider
{
    /**
     * CsvProvider constructor.
     * @throws Exception
     */
    public function __construct()
    {
        if (!Schema::connection('exchange')->hasTable('exchange_from_1c')
            || !Schema::connection('exchange')->hasTable('exchange_from_front')) {
            throw new Exception('Import or export tables are not available.');
        }
    }

    /**
     * Import entity from 1C
     * @param int $typeId - entity type ID
     * @param callable $callback - callback for handler
     * @return bool
     */
    public function import($typeId, $callback)
    {
        $processed = 0;
        ExchangeImport::whereIn('status_id', [ExchangeImport::STATUS_READY_TO_IMPORT/*, ExchangeImport::STATUS_ERROR*/])
            ->where('type_id', $typeId)
            ->orderBy('exported_at', 'ASC')
            ->chunk(500, function ($items) use ($callback, $processed) {
                foreach ($items as $item) {
                    $processed += $items->count();
                    try {
                        if ($callback($item->data)) {
                            $item->setStatus(ExchangeImport::STATUS_IMPORTED);
                        } else {
                            $item->setStatus(ExchangeImport::STATUS_ERROR);
                        }
                    }
                    catch (Exception $e) {
                        if ($e instanceof ModelNotFoundException) {
                            $item->setStatus(ExchangeImport::STATUS_ERROR_NOT_FOUND);
                        }
                        else {
                            Log::info('Import exception: '.$e->getMessage().' - '.$e->getTraceAsString());
                            $item->setStatus(ExchangeImport::STATUS_ERROR);
                        }
                    }
                }
            });

        return $processed > 0;
    }

    /**
     * Export entity to 1C
     * @param int $typeId - entity type
     * @param Collection $items
     * @return bool
     */
    public function export($typeId, Collection $items)
    {
        if($items->count() == 0) {
            return false;
        }

        $exportItems = [];
        $items->each(function($item) use($typeId, &$exportItems) {
            try {
                $exportItems[] = [
                    'entity_id' => array_get($item,'id'),
                    'type_id' => $typeId,
                    'status_id' => ExchangeExport::STATUS_READY_TO_IMPORT,
                    'front_user_id' => array_get($item,'front_user_id'),
                    '1c_user_id' => array_get($item,'1c_user_id'),
                    'data' => json_encode($item),
                    'exported_at' => Carbon::now(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];
            }
            catch(Exception $e) {
                Log::info('Export exception: '.$e->getMessage().' - '.$e->getTraceAsString());
            }
        });

        if(count($exportItems) > 0) {
            return ExchangeExport::insert($exportItems) > 0;
        }

        return false;
    }

    /*
     * Helper functions
     */

    /**
     * @param $item
     * @return mixed
     */
    protected function getAmount($item)
    {
        $amountFields = [
            'amount', 'loan_amount'
        ];

        foreach($amountFields as $field) {
            if($item->$field) {
                return $item->$field;
            }
        }
    }
}