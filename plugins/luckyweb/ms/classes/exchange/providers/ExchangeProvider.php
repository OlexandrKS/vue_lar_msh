<?php namespace LuckyWeb\MS\Classes\Exchange\Providers;


use Illuminate\Support\Collection;

interface ExchangeProvider
{
    /**
     * Import entities from 1C
     * @param int $typeId - entity type ID
     * @param callable $callback - callback for handler
     * @return bool
     */
    public function import($typeId, $callback);

    /**
     * Export entity to 1C
     * @param int $typeId - entity type ID
     * @param Collection $items
     * @return bool
     */
    public function export($typeId, Collection $items);
}