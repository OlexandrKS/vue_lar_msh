<?php

namespace LuckyWeb\MS\Classes;

use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Mail;
use App\ServiceLayer\BPM\LeadManager;
use LuckyWeb\MS\Models\AdminSettings;
use LuckyWeb\MS\Models\Review;
use LuckyWeb\MS\Models\ReviewMessage;
use System\Models\MailSetting;

class EventListener
{
    use \October\Rain\Support\Traits\Singleton;

    public function listenReviews()
    {
        // Send email notification when review is created
        Event::listen('luckyweb.ms.reviewCreated', function($review) {
            $this->onReviewCreated($review);
        });

        // Send email notification when admin is wrote answer
        Event::listen('luckyweb.ms.reviewAnswerCreated', function($review, $message) {
            $this->onReviewAnswerCreated($review, $message);
        });

        // Send forward review correspondence
        Event::listen('luckyweb.ms.forwardCorrespondence', function($review, $recipient) {
            $this->onForwardCorrespondence($review, $recipient);
        });
    }

    public function listenQuestions()
    {
        // Send email notification when question is created
        Event::listen('luckyweb.ms.questionCreated', function($questionInfo) {
            $this->onQuestionCreated($questionInfo);
        });

        // Send email notification to client when question is created
        Event::listen('luckyweb.ms.questionCreatedForClient', function($questionInfo) {
            $this->questionCreatedForClient($questionInfo);
        });
    }

    public function listenReserves()
    {
        // Send email notification when reserve is created
        Event::listen('luckyweb.ms.reserveCreated', function($reserve) {
            $this->onReserveCreated($reserve);
        });
    }

    public function listenVacancyRequests()
    {
        // Send email notification when vacancy request is created
        Event::listen('luckyweb.ms.vacancyRequestCreated', function($request) {
            $this->onVacancyRequestCreated($request);
        });
    }

    public function listenOrders()
    {
        // Send email notification when order successful
        Event::listen('luckyweb.ms.orderSuccess', function($order) {
            $this->onOrderSuccess($order);
        });

        Event::listen('luckyweb.ms.quickOrder', function($data) {
            $this->onQuickOrder($data);
        });
    }

    public function listenProdboard()
    {
        Event::listen('luckyweb.prodboard.register', function ($data) {
            $this->onProdboardRegister($data);
        });

        Event::listen('luckyweb.prodboard.sendBasket', function ($data) {
            $this->onProdboardBasket($data);
        });
    }

    /* Reviews */

    /**
     * Handle reviewCreated event
     * @param $review Review
     */
    protected function onReviewCreated($review)
    {
        if ($review->email) {
            $data = [
                'name' => $review->name,
                'id' => $review->id
            ];

            $receiverData = [
                'email' => $review->email,
                'name' => $review->name
            ];

            Mail::queue('luckyweb.ms::mail.client.review_created', $data, function($message) use($receiverData)
            {
                $message->to($receiverData['email'], $receiverData['name']);
            });
        }
    }

    /**
     * Handle reviewAnswerCreated event
     * @param $review Review
     * @param $message ReviewMessage
     */
    protected function onReviewAnswerCreated($review, $message)
    {
        $review->info_status_id = Review::INFO_STATUS_WAIT_CLIENT;
        $review->save();

        if ($review->email) {
            $data = [
                'answer' => $message->text,
                'answer_html' => str_replace("\r\n", '<br/>', $message->text),
                'id' => $review->id
            ];
            $receiverData = [
                'email' => $review->email,
                'name' => $review->name
            ];
            Mail::queue('luckyweb.ms::mail.client.review_answer_created', $data, function($message) use($receiverData)
            {
                $message->to($receiverData['email'], $receiverData['name']);
            });
        }
    }

    /**
     * Handle forwardCorrespondence event
     * @param $review Review
     * @param $recipient
     */
    protected function onForwardCorrespondence($review, $recipient)
    {
        $review->info_status_id = Review::INFO_STATUS_SENT_TO_TM;
        $review->save();

        $manager = [
            'email' => $recipient->email,
            'name' => $recipient->name,
        ];
        if ($recipient->email) {
            $data = [
                'review' => $review,
                'id' => $review->id
            ];

            Mail::queue('luckyweb.ms::mail.manager.review_messages_forwarded', $data, function($message) use($manager)
            {
                $message->to($manager['email'], $manager['name']);
            });
        }
    }

    /* Questions */

    protected function onQuestionCreated($questionInfo)
    {
        $questionInfo = json_decode($questionInfo, true);
        $receiver = AdminSettings::instance()->manager_mail;
        if (isset($questionInfo['email']) && !empty($receiver)) {
            $data = [
                'email' => array_get($questionInfo, 'email'),
                'name' => array_get($questionInfo, 'name', null),
                'city' => array_get($questionInfo, 'city', null),
                'phone' => array_get($questionInfo, 'phone', null),
                'text' => array_get($questionInfo, 'text', null)
            ];

            $senderData = [
                'email' => array_get($questionInfo, 'email'),
                'name' => array_get($questionInfo, 'name', null)
            ];

            Mail::queue('luckyweb.ms::mail.manager.question_created', $data, function($message) use($senderData, $receiver)
            {
                $message->from($senderData['email'], $senderData['name']);
                $message->to($receiver);
            });
        }
    }

    protected function questionCreatedForClient($questionInfo)
    {
        $questionInfo = json_decode($questionInfo, true);
        $sender = [
            'email' => AdminSettings::instance()->manager_mail,
            'name' => MailSetting::instance()->sender_name
        ];
        if (isset($questionInfo['email']) && !empty($sender['email'])) {
            $data = [
                'text' => array_get($questionInfo, 'text', null)
            ];

            $receiver = array_get($questionInfo, 'email');

            Mail::queue('luckyweb.ms::mail.client.question_created', $data, function($message) use($receiver, $sender)
            {
                $message->from($sender['email'], $sender['name']);
                $message->to($receiver);
            });
        }
    }

    /* Reserves */

    protected function onReserveCreated($reserve)
    {
        $receiver = AdminSettings::instance()->reserve_manager_mail;
        if ($reserve && !empty($receiver)) {
            Mail::queue('luckyweb.ms::mail.manager.reserve_created', ['reserve' => $reserve], function($message) use($receiver)
            {
                $message->to($receiver);
            });
        }
    }

    /* Vacancy requests */

    protected function onVacancyRequestCreated($request)
    {
        $receiver = AdminSettings::instance()->vacancy_manager_mail;
        if ($request && !empty($receiver)) {
            $files = [];
            if($request->photo) {
                $files[] = $request->photo->getLocalPath();
            }
            if($request->resume) {
                $files[] = $request->resume->getLocalPath();
            }

            Mail::queue('luckyweb.ms::mail.manager.vacancy_request_created', ['request' => $request], function($message) use($receiver, $files)
            {
                $message->to($receiver);
                foreach ($files as $file) {
                    $message->attach($file);
                }
            });
        }
    }

    /**
     * @deprecated
     * Mail manager after a successful order payment
     * @param $order order info data
     */
    public function onOrderSuccess($order)
    {
        $receiver = AdminSettings::instance()->lending_order_manager_mail;

        if (!empty($receiver)) {
            Mail::queue('luckyweb.ms::mail.manager.cart_order_created', ['data' => $order], function($message) use($receiver, $order) {
                $message->to($receiver);
                $message->subject('Оформлен заказ на '. array_get($order, 'products.0.name'));
            });
        }

        /** @var \App\ServiceLayer\BPM\LeadManager $manager */
        $manager = app(LeadManager::class);
        $manager->sendCheckoutEvent($order);
    }

    /**
     * @param $data
     */
    public function onProdboardRegister($data)
    {
        Mail::queue('luckyweb.user::mail.new_user_prodboard', $data, function ($message) {
            $message->to('sale@mebelshara.ru'); // todo move to env|config
            $message->subject('Регистрация нового пользователя на ProdBoard');
        });

        /** @var \App\ServiceLayer\BPM\LeadManager $manager */
        $manager = app(LeadManager::class);
        $manager->sendProdBoardRegister();
    }

    /**
     * @param $data
     */
    public function onProdboardBasket($data)
    {
        Mail::queue('luckyweb.user::mail.send_basket_prodboard', $data, function ($message) {
            $message->to('sale@mshara.ru'); // todo move to env|config
            $message->subject('Оформлен заказ на ProdBoard');
        });

        /** @var \App\ServiceLayer\BPM\LeadManager $manager */
        $manager = app(LeadManager::class);
        $manager->sendProdBoardBasketEvent($data);
    }

    /**
     * @deprecated
     * @param $data
     */
    public function onQuickOrder($data)
    {
        $receiver = AdminSettings::instance()->lending_order_manager_mail;

        if (!empty($receiver)) {
            Mail::send('luckyweb.ms::mail.manager.quick_order_created', $data, function ($message) use ($receiver) {
                $message->to($receiver);
            });
        }

        /** @var LeadManager $manager */
        $manager = app(LeadManager::class);
        $manager->sendQuickOrderEvent($data);
    }
}
