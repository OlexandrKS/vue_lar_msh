<?php namespace LuckyWeb\MS\Classes;

use Request;

use LuckyWeb\MS\Models\PageMeta;


/**
 * Finds page meta data from database according to current url path
 */

class PageMetaManager {

    /**
     * Singleton model
     * @var LuckyWeb\MS\Models\PageMeta $model
     */
    protected static $model;

    /**
     * Returns title or default
     *
     * @param string $default - default title
     *
     * @return string
     */
    public static function getTitle($default = null)
    {
        return static::getMeta('title', $default);
    }

    /**
     * Returns description or default
     *
     * @param string $default - default description
     *
     * @return string
     */
    public static function getDescription($default = null)
    {
        return static::getMeta('description', $default);
    }

    /**
     * Returns header or default
     *
     * @param string $default - default header
     *
     * @return string
     */
    public static function getHeader($default = null)
    {
        return static::getMeta('header', $default);
    }

    /**
     * Returns keywords or default
     *
     * @param string $default - default keywords
     *
     * @return string
     */
    public static function getKeywords($default = null)
    {
        return static::getMeta('keywords', $default);
    }

    /**
     * Returns given meta field or default
     *
     * @param string $field - meta field name
     * @param string $default - default field value
     *
     * @return string
     */
    public static function getMeta($field, $default = null)
    {
        $content = static::getModel()->{$field};
        return $content ? $content : $default;
    }

    /**
     * Finds model accoring to request path and resutns singleton PageMeta instance
     *
     * @return LuckyWeb\MS\Models\PageMeta
     */
    protected static function getModel()
    {
        if (!static::$model) {
            static::$model = PageMeta::where('url', '=', '/'.ltrim(Request::path(), '/'))->first();
            if (!static::$model) {
                static::$model = new PageMeta;
            }
        }
        return static::$model;
    }

}
