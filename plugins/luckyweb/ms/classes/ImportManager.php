<?php namespace LuckyWeb\MS\Classes;

use Config;
use FTP;
use DB;
use App;

use Symfony\Component\Yaml\Yaml;
use League\Csv\Reader;

use LuckyWeb\MS\Models\Settings;
use LuckyWeb\MS\Models\Image;


/**
* Import Manager
*
* Imports data from backoffice ftp
*
* @author luckyweb.pro
 * @deprecated
*/
class ImportManager {

    /**
     * Import settings
     * @var array $settings
     */
    protected $settings;

    public function __construct()
    {
        $this->settings = Yaml::parse(Settings::get('import'));
        $this->initFtpSettings();
    }

    /**
    * Imports data from backend ftp
    * @deprecated
    * @return void
    */
    public function importData()
    {
        if (!ini_get("auto_detect_line_endings")) {
            ini_set("auto_detect_line_endings", '1');
        }

        // get csv files and their field maps from setings
        $csvFiles = $this->csvImportMap();

        // overload ftp settings
        // TODO: implement extend to overload options


        foreach ($csvFiles as $fileName=>$fileImportOptions) {
            $this->debug("\n*** Importing ".$fileName);

            // Download raw .csv file
            $raw = mb_convert_encoding(FTP::connection()->readFile($this->csvFilePath($fileName)), "utf-8", "windows-1251");

            // init csv reader
            $csv = Reader::createFromString($raw)->setDelimiter(';');

            // Parse .csv to associative array
            $data = $csv->fetchAssoc();

            // Iterate over parsed data and save it to database
            $table = DB::table('luckyweb_ms_import_'.$fileImportOptions['table']);
            $table->truncate();
            $values = [];
            $fieldMap = $fileImportOptions['fields'];
            //print_r($fieldMap);
            $i = 0;
            $records = [];
            foreach ($data as $row) {
                //echo ".";
                $i++;
                if ($this->emptyRow($row)) {
                    $this->debug("Row ".($i)." of ".count($data)." is empty");
                    continue;
                }
                foreach ($row as $csvField=>$value) {
                    if (array_key_exists($csvField, $fieldMap)) {
                        $values[$fieldMap[$csvField]] = $value;
                    }
                    /*else {
                        echo $csvField.' = '.$value."\n";
                    }*/
                }
                $records[] = $values;

                if (count($records) > 1000) {
                    //echo "Insert (".count($records).")\n";
                    $table->insert($records);
                    $records = [];
                }
            }
            //echo "Insert (".count($records).")\n";
            $table->insert($records);
        }
        return 'done';
    }

    /**
     * Imports images from backend ftp
     * @deprecated
     */
    public function importImages()
    {
        $remotePath = $this->imageFtpPath();
        $listing = FTP::connection()->getDirListingDetailed($remotePath);
        //$this->debug($listing);
        if (!$listing || !count($listing)) {
            echo "Images not found\n";
            return false;
        }

        $nameMap = [
            'ТОВ'=>'good',
            'ЦВ'=>'color',
            'НК' => 'package',
            'КАТ' => 'category',
            'АКЦ' => 'promotion',

        ];
        $patterns = array_map(function ($patt) { return '/'.$patt.'/iu'; }, array_keys($nameMap));
        $replacements = array_values($nameMap);
        $localPath = Config::get('luckyweb.ms::image_path');
        if (!file_exists($localPath)) {
            mkdir($localPath, 0777, true);
        }
        //$this->debug($patterns);
        //$this->debug($replacements);

        $images = Image::all();
        foreach ($images as $image) {
            if (!array_key_exists($image->remote_name, $listing)) {
                $this->debug("Delete ".$image->remote_name);
                $image->delete();
            }
        }

        //$filesToUppload = DB::table('luckyweb_ms_import_files')->whereIn('owned_site', [0,1])->lists('file');
        $filesToUppload = DB::table('luckyweb_ms_import_files')->whereIn('owned_site', [0,1])->get();

        $i = 0;
        //foreach ($listing as $fileRemote=>$info) {
        foreach ($filesToUppload as $importFile) {
            $fileRemote = $importFile->file;
            if (isset($listing[$fileRemote])) {
                $info = $listing[$fileRemote];
            }
            else {
                echo "Warning! File ".$fileRemote." not found on FTP server\n";
                continue;
            }
            if (($image = Image::whereRemoteName($fileRemote)->first()) && !$image->isChanged($info)) {
                echo $fileRemote." is not changed\n";
                continue;
            }
            if ($fileRemote == 'Thumbs.db') {
                continue;
            }
            $localFile = preg_replace($patterns, $replacements, $fileRemote);
            echo $remotePath.'/'.$fileRemote.' --> '.$localPath.'/'.$localFile.' ... ';
            if (FTP::connection()->downloadFile($remotePath.'/'.$fileRemote, $localPath.'/'.$localFile)) {
                echo "OK\n";
            }
            else {
                echo " reconnecting... ";
                FTP::reconnect();
                echo " trying... ";
                if (!FTP::connection()->downloadFile($remotePath.'/'.$fileRemote, $localPath.'/'.$localFile)) {
                    echo "FAIL\n";
                    continue;
                }
                else {
                    $i=0;
                    echo "OK\n";
                }
            }
            if (!($image = Image::whereRemoteName($fileRemote)->first())) {
                $image = new Image;
            }
            $image->remote_time_mdt = Image::getTimeString($info);
            $image->remote_size = $info['size'];
            $image->remote_name = $fileRemote;
            $image->local_name = $localFile;
            $image->minimized = false;
            $image->short_descr = $importFile->short_descr;
            $image->assignment = $importFile->assignment;
            $image->save();
        }

        // Create hash file => record of file descriptions
        $descrs = [];
        foreach (DB::table('luckyweb_ms_import_files')->get() as $descr) {
            $descrs[$descr->file] = $descr;
        }

        echo "Updating assignments and descrs...\n";
        // Add description data to Image
        foreach (Image::get() as $image) {
            if (array_key_exists($image->remote_name, $descrs)) {
                $image->short_descr = $descrs[$image->remote_name]->short_descr;
                $image->assignment = $descrs[$image->remote_name]->assignment;
                $image->save();
            }
        }
        echo "done\n";

        return true;
    }

    /**
     * Checks if import is forced for production mode
     *
     * @return boolean
     */
    public function canImport()
    {
        $listing = FTP::connection()->getDirListingDetailed($this->ftpPath());
        return
            App::environment()!='production' ||
            array_key_exists(array_get($this->settings, 'updateFlagFile', 'do-not-update'), $listing);
    }

    /**
     * Deletes update.txt if env is production
     */
    public function cleanImportFlagFile()
    {
        if (App::environment()=='production') {
            FTP::connection()->delete($this->getUpdateFlagFilePath());
        }
    }

    /**
     * Detects if given row is empty
     *
     * @param integer row - csv row
     *
     * @return boolean
     */
    protected function emptyRow($row)
    {
        return count(array_filter($row, function ($val) { return $val!=''; }))==0;
    }

    /**
     * Reads ftp part from settings
     *
     * @param array $extended  Extended FTP options
     *
     * @return array
     */
    protected function ftpSettings(array $extended = [])
    {
        return array_get($this->settings, 'ftp', $extended);
    }

    /**
     * Overloads Config ftp access with system settings ftp access
     *
     * @return void
     */
    protected function initFtpSettings()
    {
        Config::set('ftp.connections.ms', array_replace_recursive(Config::get('ftp.connections.ms'), $this->ftpSettings()));
        $this->settings['ftp'] = Config::get('ftp.connections.ms');
    }

    /**
     * Returns csv import map (csv files and field map)
     *
     * @param array $extended  Extended csv map
     *
     * @return array
     */
    protected function csvImportMap(array $extended = [])
    {
        return array_get($this->settings, 'csv', $extended);
    }

    protected function ftpPath()
    {
        return rtrim(array_get($this->ftpSettings(), 'path'),'/');
    }

    /**
     * Returnes ftp path of give csv file
     *
     * @param string $fileNameNoExt - File name without .csv extension
     *
     * @return string
     */
    protected function csvFilePath($fileNameNoExt)
    {
        return $this->ftpPath().'/'.$fileNameNoExt.'.csv';
    }

    /**
     * Returnes ftp path image directory
     *
     * @return string
     */
    protected function imageFtpPath()
    {
        return rtrim(array_get($this->ftpSettings(), 'imagePath'),'/');
    }

    /**
     * Returns ftp path of update file
     */
    protected function getUpdateFlagFilePath()
    {
        return rtrim(array_get($this->ftpSettings(), 'path'),'/').'/'.
            array_get($this->settings, 'updateFlagFile', 'do-not-update');
    }

    public function getSettings($dotKey = null, $default = null)
    {
        return is_null($dotKey) ? $this->settings : array_get($this->settings, $dotKey, $default);
    }

    protected function debug($object) {
        echo(
            is_array($object) || is_object($object) ? print_r($object, true) : $object)."\n";
    }

}
