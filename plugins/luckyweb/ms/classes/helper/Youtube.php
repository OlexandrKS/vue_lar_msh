<?php namespace LuckyWeb\MS\Classes\Helper;

class Youtube
{

    /**
     * Youtube url
     * @var string $url
     */
    protected $url;

    /**
     * Video shortcode
     * @var string $shortcode
     */
    public $shortCode;

    /**
     * @param string $url - Youtbe video url
     */
    public function __construct($url)
    {
        $this->setUrl($url);
    }

    /**
     * Url setter. Parses url and sets shotCode property
     *
     * @param string $url - Youtbe video url
     */
    public function setUrl($url)
    {
        $this->url = $url;
        $this->shortCode = $this->getShortCode();
    }

    /**
     * Generates short link to youtube video
     *
     * @return string
     */
    public function getVideoShortUrl()
    {
        return 'https://youtu.be/'.$this->shortCode;
    }

    /**
     * Generates link preview image source
     *
     * @return string
     */
    public function getVideoPreviewSrc()
    {
        return 'https://img.youtube.com/vi/'.$this->shortCode.'/hqdefault.jpg';
    }

    /**
     * Generates link for embedded player
     *
     * @param bool $autoplay - start playing video on open
     * @param bool $rel - not show related videos after watching
     * @return string
     */
    public function getVideoEmbedUrl($autoplay = false, $rel = false)
    {
        $params = [];
        if($autoplay) {
            $params['autoplay'] = 1;
        }
        if(!$rel) {
            $params['rel'] = 0;
        }

        return count($params) > 0
            ? 'https://www.youtube.com/embed/'.$this->shortCode.'?'.http_build_query($params)
            : 'https://www.youtube.com/embed/'.$this->shortCode;
    }


    /**
     * Parses url and returnes video short code
     */
    protected function getShortCode()
    {
        if (!$this->url) {
            return null;
        }
        $parts = parse_url($this->url);
        if ($parts === FALSE) {
            return null;
        }

        if ($parts['host']=='youtu.be') { // short url
            return ltrim($parts['path'], '/');
        }

        parse_str($parts['query'], $qparts);
        return $qparts['v'];
    }

}
