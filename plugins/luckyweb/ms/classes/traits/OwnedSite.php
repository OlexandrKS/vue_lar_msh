<?php namespace LuckyWeb\MS\Classes\Traits;

use LuckyWeb\MS\Classes\ImportManager;

trait OwnedSite
{
    /**
     * Used in classes like Category when loading from import tables
     *
     * @param Builder $query        Query builder from Import table
     */
    public static function ownedSiteExtendQuery($query)
    {
        $im = new ImportManager;
        $ownedFlags = $im->getSettings('ownedSiteFlags');
        if (is_array($ownedFlags) && count($ownedFlags)) {
            $query->whereIn('owned_site', $ownedFlags);
        }
    }

    /**
     * Used in Import classes like ImportGoods
     */
    public function scopeOwnedContent($query)
    {
        static::ownedSiteExtendQuery($query);
        return $query;
    }
}
