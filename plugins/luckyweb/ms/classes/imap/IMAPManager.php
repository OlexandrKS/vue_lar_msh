<?php namespace LuckyWeb\MS\Classes\Imap;


use Exception;
use Illuminate\Support\Facades\Config;
use LuckyWeb\MS\Models\Settings;
use PhpImap\Mailbox;

class IMAPManager
{
    use \October\Rain\Support\Traits\Singleton;

    /**
     * Path to IMAP server
     * @var string
     */
    protected $path;

    /**
     * Mail account user name
     * @var string
     */
    protected $username;

    /**
     * Mail account password
     * @var string
     */
    protected $password;

    /**
     * Path to save attachments
     * @var string
     */
    protected $pathToAttachments;

    /**
     * Current mailbox
     * @var mixed
     */
    protected $mailbox;

    /**
     * ID of last imported email
     * @var integer
     */
    protected $lastImportedId;

    /**
     * Array of new messages IDs
     * @var array
     */
    public $newMessagesIds = [];


    protected function init()
    {
        $this->path = Config::get('luckyweb.ms::mail_path');
        $this->username = Config::get('luckyweb.ms::mail_username');
        $this->password = Config::get('luckyweb.ms::mail_password');
        $this->pathToAttachments = storage_path('temp');
        $this->lastImportedId = Settings::instance()->mail_last_imported_id ? Settings::instance()->mail_last_imported_id : 0;
    }

    /**
     * Update IMAP connection
     * @return bool
     * @throws Exception
     */
    protected function connect() {
        if(!$this->mailbox) {
            $this->mailbox = new Mailbox($this->path, $this->username, $this->password, $this->pathToAttachments);
            if (!$this->mailbox) {
                throw new Exception('IMAP connection failed');
            }
        }

        return true;
    }

    /**
     * Search emails by custom criteria
     * @param string $criteria
     * @return array
     * @throws Exception
     */
    public function search($criteria = 'ALL')
    {
        if($this->connect()) {
            return $this->mailbox->searchMailbox($criteria);
        }
    }

    /**
     * @param $id
     * @return IMAPMail
     * @throws Exception
     */
    public function get($id) {
        if(!empty(intval($id)) && $this->connect()) {
            return new IMAPMail($this->mailbox->getMail($id, false));
        }
    }

    /**
     * Update new messages array
     */
    public function updateNewMessages()
    {
        $this->newMessagesIds = array_where($this->search('SINCE '.date('Y-m-d')), function($key, $value) {
            return $value > $this->lastImportedId;
        });
    }

    /**
     * Retrieve first new message
     * @return bool|IMAPMail
     */
    public function getFirstNew()
    {
        if(count($this->newMessagesIds) == 0) {
            $this->updateNewMessages();
        }
        if(count($this->newMessagesIds) > 0) {
            $mail = $this->get(array_shift($this->newMessagesIds));

            if($mail && !empty($mail->getFromAddress())) {
                $this->lastImportedId = $mail->getId();

                // If the last item - save it's ID
                if(count($this->newMessagesIds) == 0) {
                    $settings = Settings::instance();
                    $settings->mail_last_imported_id = $this->lastImportedId;
                    $settings->save();
                }
                return $mail;
            }
        }
        return false;
    }

}