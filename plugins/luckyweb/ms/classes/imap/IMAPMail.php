<?php namespace LuckyWeb\MS\Classes\Imap;


use Exception;
use Illuminate\Support\Facades\Log;
use InvalidArgumentException;
use PhpImap\IncomingMail;
use Yangqi\Htmldom\Htmldom;

class IMAPMail
{
    /**
     * Mail instance
     * @var IncomingMail
     */
    protected $mail;

    public function __construct($mail) {
        if(!$mail) {
            throw new InvalidArgumentException('IncomingMail object is required');
        }
        $this->mail = $mail;
    }

    /**
     * Return mail id
     */
    public function getId()
    {
        return $this->mail->id;
    }

    /**
     * Return date
     * @return mixed
     */
    public function getDate()
    {
        return $this->mail->date;
    }

    /**
     * Return subject
     * @return string
     */
    public function getSubject()
    {
        return $this->mail->subject;
    }

    /**
     * Return from name
     * @return string
     */
    public function getFromName()
    {
        return $this->mail->fromName;
    }

    /**
     * Return from address
     * @return string
     */
    public function getFromAddress()
    {
        return $this->mail->fromAddress;
    }

    /**
     * Return to array
     * @return array
     */
    public function getTo()
    {
        return $this->mail->to;
    }

    /**
     * Return cc array
     * @return array
     */
    public function getCc()
    {
        return $this->mail->cc;
    }

    /**
     * Return replyTo array
     * @return array
     */
    public function getReplyTo()
    {
        return $this->mail->replyTo;
    }

    /**
     * Return mail converted to string
     * @return string
     */
    public function toString()
    {
        return $this->mail->toString;
    }

    /**
     * Return messageId
     * @return mixed
     */
    public function getMessageId()
    {
        return $this->mail->messageId;
    }

    /**
     * Return mail text
     * @param bool $removeSignature TRUE - remove mail signature
     * @return string
     */
    public function getText($removeSignature = false)
    {
        $text = $this->mail->textPlain;
        if($removeSignature) {
            $text = preg_replace('/--[^--]+$/i','', $text);
        }
        
        return trim($text);
    }
    
    /**
     * Return mail html
     * @return string
     */
    public function getHtml()
    {
        return $this->mail->textHtml;
    }

    /**
     * Return message from plane text
     * Remove history and other unwanted chars
     * @return string
     */
    public function getMessageFromText()
    {
        if($this->mail->textPlain) {
            $text = preg_replace('/(^>.*)/ms', '', $this->mail->textPlain);
            $text = preg_replace('/(^--.*)/ms', '', $text);
            $text = preg_replace('/(^\d{1,2}\s.*\s\d{4}.*\.,\s\d{2}:\d{2}.*)/ms', '', $text);
            $text = preg_replace('/(^\d{2}\.\d{2}\.\d{4},\s\d{2}:\d{2}.*)/ms', '', $text);
            $text = preg_replace('/(^\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}.*)/ms', '', $text);
            $text = trim($text);
            $text = str_replace(array("\r\n\r\n"), "\r\n", $text);
            $text = str_replace(array("\n\n"), "\n", $text);
            $text = str_replace(array("\r\r"), "\r", $text);
            $text = str_replace(array("\r\r"), "\r", $text);
            $text = str_replace("*", "", $text);
            return $text;
        }
        else {
            return null;
        }
    }

    /**
     * Return message from html
     * Remove unwanted tags
     * @return string
     */
    public function getMessageFromHtml()
    {
        if($this->mail->textHtml) {
            try {
                $html = new Htmldom($this->mail->textHtml);
                //Remove extra
                $extra = $html->find('.gmail_extra', 0);
                if ($extra) {
                    $extra->outertext = '';
                }
                // Get inner text
                $html->load($html->find('div', 0)->innertext);
                // Split by rows
                foreach ($html->find('div') as $row) {
                    $row->outertext = "\n" . $row->innertext;
                }

                return strip_tags($html);
            }
            catch(Exception $e) {
                Log::info('IMAPMail->getMessageFromHtml(): '.$e->getMessage().' - '.$e->getTraceAsString());
            }
        }
        return null;
    }

    public function getMessage()
    {
        $message = $this->getMessageFromText();
        if(!$message) {
            $message = $this->getMessageFromHtml();
        }
        return $message;
    }
}