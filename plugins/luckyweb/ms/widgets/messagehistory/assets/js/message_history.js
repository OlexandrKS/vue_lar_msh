var MessageHistory = function () {

    var showLoading = function(id) {
        var $message = $('.js-message[data-id='+id+']');
        $message.find('.js-loading_success').hide();
        $message.find('.js-loading_error').hide();
        $message.find('.js-loading_indicator').fadeIn();
    };

    var showSuccess = function(id) {
        var $message = $('.js-message[data-id='+id+']');
        $message.find('.js-loading_indicator').hide();
        $message.find('.js-loading_error').hide();
        $message.find('.js-loading_success').fadeIn();

        setTimeout(function(){
            $message.find('.js-loading_success').fadeOut();
        }, 5000);
    };

    var showError = function(id) {
        var $message = $('.js-message[data-id='+id+']');
        $message.find('.js-loading_indicator').hide();
        $message.find('.js-loading_success').hide();
        $message.find('.js-loading_error').fadeIn();

        setTimeout(function(){
            $message.find('.js-loading_error').fadeOut();
        }, 5000);
    };

    var initMessageSaveButton = function() {
        $('.js-message_list_container').on('click', '.js-save', function(){
            var $message = $(this).closest('.js-message');
            var id = $message.data('id');
            var text = $message.find('.js-message_text').val();
            var datetime = $message.find('.js-datetime').val();
            showLoading(id);

            $.request('onSaveMessage', {
                data: {
                    id: id,
                    text: text,
                    datetime: datetime
                },
                complete: function(data) {
                    if(data.responseJSON.result == 'success') {
                        showSuccess(id);
                    }
                    else if(data.responseJSON.result == 'error') {
                        showError(id);
                    }
                }
            })
        });
    };
    
    var initFormSubmit = function() {
        $('.js-add_message_form').on('submit', function(e){
            var $form = $(this);

            e.preventDefault();
            $form.find('button[type=submit]').prop('disabled', true);
            $form.request('onAddMessage', {
                success: function(data, status, jqXHR) {
                    $('.js-message_list_container').html(data['.js-message_list_container']);
                    $form.find('.fr-view > p').text('');
                    $form.find('[data-control="datepicker"] input').val('');
                },
                complete: function() {
                    $('[data-control="richeditor"]').richEditor();
                    $('[data-control="datepicker"]').datePicker();
                    window.scrollTo(0, $('.js-add_message_form').offset().top - 500);
                    $form.find('button[type=submit]').prop('disabled', false);
                }
            });
        });
    };

    var handleDeleteRequestComplete = function(element, data) {
        if(data.result == 'success') {
            $(element).closest('.js-message').fadeOut()
        }
    };
    
    return {
        init: function () {
            initMessageSaveButton();
            initFormSubmit();
        },
        handleDeleteRequestComplete: handleDeleteRequestComplete
    };
}();

// Initialize when page loads
jQuery(function () {
    MessageHistory.init();
});
