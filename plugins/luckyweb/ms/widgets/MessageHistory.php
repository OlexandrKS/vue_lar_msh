<?php namespace LuckyWeb\MS\Widgets;


use Backend\Classes\WidgetBase;
use Backend\Facades\BackendAuth;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use LuckyWeb\MS\Models\Review;
use LuckyWeb\MS\Models\ReviewMessage;
use October\Rain\Support\Facades\Flash;

class MessageHistory extends WidgetBase
{
    protected $defaultAlias = 'messageHistory';
    protected $reviewId;
    protected $messageList = [];

    /**
     * Behavior constructor
     *
     * @param Controller $controller
     * @param int $recordId
     */
    public function __construct($controller, $recordId)
    {
        parent::__construct($controller);

        if($recordId) {
            $review = Review::find($recordId);
            if ($review) {
                $this->reviewId = $review->id;
                $this->messageList = $review->messages;
            }
        }
    }

    public function widgetDetails()
    {
        return [
            'name'        => 'Message history Widget',
            'description' => 'Used for building message history.'
        ];
    }

    /**
     * Inject CSS and JS assets
     */
    public function loadAssets()
    {
        $this->addCss('/modules/backend/formwidgets/richeditor/assets/css/richeditor.css');
        $this->addCss('css/message_history.css');

        $this->addJs('/modules/backend/formwidgets/richeditor/assets/js/build-min.js');
        $this->addJs('/modules/backend/formwidgets/richeditor/assets/js/richeditor.js');

        $this->addJs('js/message_history.js?20170606');
    }

    public function render()
    {
        return $this->makePartial('message_history', [
            'messages' => $this->messageList
        ]);
    }

    /**
     * Add new message
     * @return array
     */
    public function onAddMessage()
    {
        $message = new ReviewMessage();
        $message->text = post('text');
        $message->review_id = $this->reviewId;
        $message->direction = Input::get('direction',ReviewMessage::OUTPUT_ID);
        $message->created_at = Carbon::parse(Input::get('datetime'));
        $message->backend_user_id = BackendAuth::getUser()->id;
        $message->save();

        if($message) {
            $review = Review::find($this->reviewId);
            if($review) {
                Event::fire('luckyweb.ms.reviewAnswerCreated', [$review, $message]);
            }
        }

        $this->messageList = ReviewMessage::where('review_id', $this->reviewId)->get();

        return ['.js-message_list_container' => $this->makePartial('list', ['messages' => $this->messageList])];
    }

    /**
     * Delete message by given ID
     * @return array
     */
    public function onDeleteMessage()
    {
        $result = 'error';
        try {
            if(ReviewMessage::destroy(intval(post('id')))) {
                $result = 'success';
            }
        }
        catch(Exception $e) {
            Flash::error('Не удалось удалить сообщение.');
        }
        return ['result' => $result];
    }

    public function getMessageTitle($message) {
        if($message->direction == ReviewMessage::INPUT_ID) {
            return 'Клиент';
        }
        if($message->direction == ReviewMessage::OUTPUT_ID) {
            return 'Администратор ('.$message->backendUser->first_name.' '.$message->backendUser->last_name.')';
        }
        if($message->direction == ReviewMessage::FROM_TM_ID) {
            return 'ТМ ('.$message->backendUser->first_name.' '.$message->backendUser->last_name.')';
        }
    }

    public function getMessageClass($direction) {
        if($direction == ReviewMessage::INPUT_ID) {
            return 'input col-sm-offset-6';
        }
        if($direction == ReviewMessage::OUTPUT_ID) {
            return 'output';
        }
        if($direction == ReviewMessage::FROM_TM_ID) {
            return 'output tm';
        }
    }

    /**
     * Save message
     */
    public function onSaveMessage()
    {
        $result = 'error';

        try {
            $message = ReviewMessage::findOrFail(intval(post('id')));
            $message->created_at = Carbon::parse(post('datetime'));
            $message->text = post('text');
            $message->save();
            $result = 'success';
        }
        catch(Exception $e) {
            Log::info($e->getMessage());
            Log::info($e->getTraceAsString());
        }
        return ['result' => $result];
    }
}
