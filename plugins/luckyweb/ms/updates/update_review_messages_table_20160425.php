<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateReviewMessagesTable20160425 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_review_messages', function($table)
        {
            $table->integer('backend_user_id')->unsigned()->nullable()->default(null)->after('mail_id');
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_review_messages'))
        {
            Schema::table('luckyweb_ms_review_messages', function($table)
            {
                $columns = [
                    'backend_user_id'
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('luckyweb_ms_review_messages', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
