<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePagePopupsTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_ms_page_popups', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('popup_name');
            $table->unsignedTinyInteger('type')->index();  // left 1, right 2, center 3, float 4
            $table->string('utm_source')->index()->nullable()->default(null);
            $table->string('utm_medium')->index()->nullable()->default(null);
            $table->string('utm_campaign')->index()->nullable()->default(null);
            $table->string('utm_content')->index()->nullable()->default(null);
            $table->string('float_header')->nullable()->default(null);
            $table->string('left_button_text')->nullable()->default(null);
            $table->string('left_button_color', 7)->nullable()->default(null);
            $table->string('left_button_link')->nullable()->default(null);
            $table->string('right_button_text')->nullable()->default(null);
            $table->string('right_button_color', 7)->nullable()->default(null);
            $table->unsignedInteger('timeout');
            $table->text('message')->nullable()->default(null);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_page_popups');
    }
}
