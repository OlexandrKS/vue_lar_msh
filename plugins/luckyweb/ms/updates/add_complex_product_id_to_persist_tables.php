<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use DB;

class AddComplexProductIdToPersistTables extends Migration
{

    public function up()
    {
        if (Schema::hasTable('luckyweb_ms_products')) {

            Schema::table('luckyweb_ms_images', function ($table) {
                $table->string('product_id')->change();
            });

            Schema::table('luckyweb_ms_products_groups', function ($table) {
                $table->string('product_id')->change();
            });

            // DB::table('luckyweb_ms_images')->update(['product_id' =>
            //     DB::raw("CONCAT(`color_id`, '.',  `good_id`, '.', `package_id`)")]);
            DB::statement(
                "UPDATE `luckyweb_ms_images` SET `product_id` = " .
                "CONCAT(IFNULL(`color_id`, ''), '.', IFNULL(`good_id`, ''), '.', IFNULL(`package_id`, ''))");

            DB::statement("
                UPDATE `luckyweb_ms_products_groups`
                    INNER JOIN `luckyweb_ms_products` ON `luckyweb_ms_products_groups`.`product_id` = `luckyweb_ms_products` .`id`
                SET `luckyweb_ms_products_groups`.`product_id` =
                    CONCAT(IFNULL(`luckyweb_ms_products`.`color_id`, ''), '.',
                        IFNULL(`luckyweb_ms_products`.`good_id`, ''), '.', IFNULL(`luckyweb_ms_products`.`package_id`, ''))
            ");
        }
    }

    public function down()
    {

    }
}
