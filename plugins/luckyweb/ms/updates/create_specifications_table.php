<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSpecificationsTable extends Migration
{
    public function up()
    {
        $this->down();
        Schema::create('luckyweb_ms_specifications', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('id');
            $table->string('name')->nullable()->defaul(null);
            $table->integer('sort')->nullable()->defaul(null);
            $table->boolean('popup')->nullable()->default(null)->index();
            $table->timestamps();
            $table->primary('id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_specifications');
    }
}
