<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreatePageMetasTable extends Migration
{

    public function up()
    {
        $this->down();
        Schema::create('luckyweb_ms_page_metas', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('url')->index();
            $table->text('title')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->text('keywords')->nullable()->default(null);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_page_metas');
    }

}
