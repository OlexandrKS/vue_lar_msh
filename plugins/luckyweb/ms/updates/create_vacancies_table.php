<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateVacanciesTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_ms_vacancies', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('city_id')->unsigned()->nullable()->default(null)->index();
            $table->integer('status_id')->unsigned()->nullable()->default(1)->index();

            $table->string('name');
            $table->text('duties');
            $table->text('conditions');
            $table->text('demands');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_vacancies');
    }
}
