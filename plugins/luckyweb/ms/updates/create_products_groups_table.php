<?php namespace LuckyWeb\CoffeeTown\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateProductsGroupsTable extends Migration
{

    public function up()
    {
        $this->down();
        Schema::create('luckyweb_ms_products_groups', function($table)
        {
            $table->engine = 'InnoDB';
            $table->primary(['product_id', 'group_id']);
            $table->string('product_id');
            $table->integer('group_id')->unsigned();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_products_groups');
    }

}
