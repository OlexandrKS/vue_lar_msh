<?php

namespace Luckyweb\Ms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateMenuTable extends Migration
{
    public function up()
    {
        Schema::create('svg_icons', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->mediumText('content');
        });

        Schema::create('menu_items', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('menu_id');
            $table->unsignedInteger('parent_id')->nullable();
            $table->integer('order')->default(0);
            $table->string('name');
            $table->string('url')->default('#');
            $table->unsignedInteger('icon_id')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('menu_items');
        Schema::dropIfExists('svg_icons');
    }
}
