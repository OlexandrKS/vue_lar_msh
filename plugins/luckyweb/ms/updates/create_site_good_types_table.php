<?php

namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSiteGoodTypesTable extends Migration
{
    public function up()
    {
        $this->down();

        Schema::create('luckyweb_ms_site_good_types', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('type_id')->nullable()->index();
            $table->string('type_name')->nullable();
            $table->integer('width')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_site_good_types');
    }
}
