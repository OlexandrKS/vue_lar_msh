<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateReviewsTable20160425 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_reviews', function($table)
        {
            $table->integer('info_status_id')->unsigned()->default(1)->after('status_id');
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_reviews'))
        {
            Schema::table('luckyweb_ms_reviews', function($table)
            {
                $columns = [
                    'info_status_id'
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('luckyweb_ms_reviews', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
