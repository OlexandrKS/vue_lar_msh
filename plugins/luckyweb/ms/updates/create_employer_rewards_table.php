<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateEmployerRewardsTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_ms_employer_rewards', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('year')->nullable()->default(null)->index();
            $table->string('src')->nullable()->default(null);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_employer_rewards');
    }
}
