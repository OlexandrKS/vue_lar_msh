<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateProductTable20190517 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_products', function($table)
        {
            $table->string('item_number')->nullable()->default(null)->index()->after('installments');
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_products'))
        {
            Schema::table('luckyweb_ms_products', function($table)
            {
                $columns = [
                    'item_number',
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('luckyweb_ms_products', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
