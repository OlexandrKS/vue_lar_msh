<?php

namespace Luckyweb\Ms\Updates;

use Illuminate\Support\Facades\DB;
use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use Carbon\Carbon;

class CreateBannersTable extends Migration
{
    public function up()
    {
        Schema::create('banners', function(Blueprint $table) {
            $table->char('id', 36);
            $table->string('color')->nullable();
            $table->string('image');
            $table->string('type');
            $table->string('name');

            $table->primary('id');
            $table->timestamps();
        });

        DB::table('banners')->insert(
            array(
                'id' => '1',
                'color' => '#3abd15',
                'image' => '3abd15-ha2703@2x.jpg',
                'type' => 'header',
                'name' => 'default header',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            )
        );
        DB::table('banners')->insert(
            array(
                'id' => '2',
                'color' => '',
                'image' => 'image',
                'type' => 'home',
                'name' => 'Home banner 1',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            )
        );
        DB::table('banners')->insert(
            array(
                'id' => '3',
                'color' => '',
                'image' => 'image',
                'type' => 'home',
                'name' => 'Home banner 1',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            )
        );
        DB::table('banners')->insert(
            array(
                'id' => '4',
                'color' => '',
                'image' => 'grid/gr_ban.jpeg',
                'type' => 'grid',
                'name' => 'Grid banner',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            )
        );
    }

    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
