<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateImagesTable extends Migration
{

    public function up()
    {
        Schema::create('luckyweb_ms_images', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('remote_name')->index();
            $table->string('local_name')->index();
            $table->string('remote_time_mdt')->index();
            $table->bigInteger('remote_size')->index();
            $table->boolean('minimized')->default(false)->index();
            $table->bigInteger('good_id')
                ->nullable()->default(null)
                ->unsigned()->index();
            $table->bigInteger('package_id')
                ->nullable()->default(null)
                ->unsigned()->index();
            $table->bigInteger('color_id')
                ->nullable()->default(null)
                ->unsigned()->index();
            $table->bigInteger('category_id')
                ->nullable()->default(null)
                ->unsigned()->index();
            $table->bigInteger('promotion_id')
                ->nullable()->default(null)
                ->unsigned()->index();
            $table->bigInteger('product_id')
                ->nullable()->default(null)
                ->unsigned()->index();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_images');
    }

}
