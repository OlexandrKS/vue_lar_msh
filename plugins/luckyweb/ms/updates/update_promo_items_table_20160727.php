<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdatePromoItemsTable20160727 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_promo_items', function($table)
        {
            $table->string('image_alt')->nullable()->default(null)->after('block_id');
            $table->integer('offset_top')->nullable()->default(null)->after('offset');
            $table->integer('offset_bottom')->nullable()->default(null)->after('offset_top');
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_promo_items'))
        {
            Schema::table('luckyweb_ms_promo_items', function($table)
            {
                $columns = [
                    'image_alt',
                    'offset_top',
                    'offset_bottom'
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('luckyweb_ms_promo_items', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
