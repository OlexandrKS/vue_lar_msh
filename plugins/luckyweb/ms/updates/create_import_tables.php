<?php

namespace LuckyWeb\MS\Updates;

use October\Rain\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class CreateImportTable extends Migration
{

    public function up()
    {
        $this->down();

        // Categories
        Schema::create('luckyweb_ms_import_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->integer('price_from');
            $table->integer(snake_case('NumberOfPhotos'));
            $table->integer('order')->default(0);
            $table->string('slug');
            $table->string(snake_case('PriceDescription'), 1000)->nullable()->default(null);
            $table->integer(snake_case('OwnedSite'))->nullable()->default(0);
            $table->text('banner_week_1')->nullable();
            $table->text('banner_week_2')->nullable();
        });

        // Sub Categories
        Schema::create('luckyweb_ms_import_sub_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->integer('category_id')->nullable()->default(null);
            $table->integer(snake_case('NumberOfPhotos'));
            $table->integer('order')->default(0);
            $table->string('slug');
            $table->integer(snake_case('OwnedSite'))->nullable()->default(0);
        });

        // Goods
        Schema::create('luckyweb_ms_import_goods', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->mediumText(snake_case('Description'))->nullable()->default(null);
            $table->integer('description_type_id')->unsigned()->nullable()->default(null);
            $table->mediumText('description1')->nullable()->default(null);
            $table->mediumText('description2')->nullable()->default(null);
            $table->string(snake_case('GoodsName_1c'))->nullable()->default(null);
            $table->integer(snake_case('FurnisherId'))->unsigned()->nullable()->default(null);
            $table->integer(snake_case('good_type_id'))->unsigned()->nullable()->default(null);
            $table->string(snake_case('OverallDimensions'))->nullable()->default(null);
            $table->decimal(snake_case('PriceTopB'), 10,2)->default(0);
            $table->decimal(snake_case('PriceLowerB'), 10,2)->default(0);
            $table->integer(snake_case('DiscountB'))->nullable()->default(null);
            $table->decimal(snake_case('PriceSpecial'), 10,2)->default(0);
            $table->integer(snake_case('DiscountSpecial'))->nullable()->default(null);
            $table->string(snake_case('ConditionForDiscount'))->nullable()->default(null);
            $table->decimal(snake_case('PricePromo'), 10,2)->default(0);
            $table->integer(snake_case('MixedPromo'))->nullable()->default(null);
            $table->integer(snake_case('DiscountPromo'))->nullable()->default(null);
            $table->boolean(snake_case('OnlyForSetOfPackages'))->nullable()->default(null);
            $table->string(snake_case('category_id'))->nullable()->default(null);
            $table->string(snake_case('SubCategoryId'))->nullable()->default(null);
            $table->string(snake_case('PriceDescription'), 1000)->nullable()->default(null);
            $table->string(snake_case('WeekPromo'))->nullable()->default(null);
            $table->integer(snake_case('OwnedSite'))->nullable()->default(0);
            $table->string('set_id')->nullable()->default(null);
            $table->integer('max_order_item')->nullable()->default(null);
            $table->decimal('delivery_cost', 10, 2)->nullable()->default(null);
            $table->decimal('assembling_cost', 10, 2)->nullable()->default(null);
            $table->string('type_name_for_page')->nullable()->default(null);
            $table->integer('width')->nullable()->default(null);
            $table->integer('depth')->nullable()->default(null);
            $table->integer('height')->nullable()->default(null);
            $table->string('site_type_id')->nullable()->default(null);
            $table->string('site_width')->nullable()->default(null);
            $table->boolean('installments')->default(0);
        });

        // Colors
        Schema::create('luckyweb_ms_import_colors', function($table)
        {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->string('name');
        });

        // ColorsOfGoods
        Schema::create('luckyweb_ms_import_goods_colors', function($table)
        {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->bigInteger('good_id')->unsigned();
            $table->bigInteger('color_id')->unsigned();
            $table->string('title')->nullable()->default(null);
            $table->string('color')->nullable()->default(null);
            $table->integer('number_of_photots')->nullable()->default(null);
            $table->integer(snake_case('PositionInCategory'))->index();
            $table->integer(snake_case('PositionInCategory2'))->index();
            $table->integer(snake_case('PositionInMixedPromo'))->index();
            $table->text(snake_case('LinkToVideo'))->nullable();
            $table->decimal(snake_case('PriceTopSale'), 10,2)->nullable()->default(null);
            $table->decimal(snake_case('PriceLowerSale'), 10,2)->nullable()->default(null);
            $table->decimal(snake_case('DiscountSale'), 10,2)->nullable()->default(null);
            $table->string(snake_case('AlsoInSpecList'))->nullable()->default(null);
            $table->string('slug');
            $table->integer('residual_indicator')->nullable()->default(null);
            $table->boolean('category_invisible')->default(false);
            $table->string('variant_of_design')->nullable()->defaul(null);
        });

        // Furnishers
        Schema::create('luckyweb_ms_import_furnishers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('name');
        });

        // TypesOfGoods
        Schema::create('luckyweb_ms_import_good_types', function($table)
        {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->string('name');
            $table->string('full_name');
            $table->string('category_id')->index();
            $table->string('sub_category_id')->nullable()->default(null)->index();
        });

        // SetOfPackages
        Schema::create('luckyweb_ms_import_packages', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->mediumText(snake_case('Description'))->nullable()->default(null);
            $table->integer('description_type_id')->unsigned()->nullable()->default(null);
            $table->string('description1')->nullable()->default(null);
            $table->string('description2')->nullable()->default(null);
            $table->string(snake_case('GoodsName_1c'))->nullable()->default(null);
            $table->integer(snake_case('FurnisherId'))->unsigned()->nullable()->default(null);
            $table->integer(snake_case('good_type_id'))->unsigned()->nullable()->default(null);
            $table->string(snake_case('OverallDimensions'))->nullable()->default(null);
            $table->decimal(snake_case('PriceTopB'), 10,2)->default(0);
            $table->decimal(snake_case('PriceLowerB'), 10,2)->default(0);
            $table->integer(snake_case('DiscountB'))->nullable()->default(null);
            $table->decimal(snake_case('PriceSpecial'), 10,2)->default(0);
            $table->integer(snake_case('DiscountSpecial'))->nullable()->default(null);
            $table->string(snake_case('ConditionForDiscount'))->nullable()->default(null);
            $table->decimal(snake_case('PricePromo'), 10,2)->default(0);
            $table->integer(snake_case('MixedPromo'))->nullable()->default(null);
            $table->integer(snake_case('DiscountPromo'))->nullable()->default(null);
            $table->string('category_id')->nullable()->default(null);
            $table->string(snake_case('SubCategoryId'))->nullable()->default(null);
            $table->string(snake_case('PriceDescription'), 1000)->nullable()->default(null);
            $table->string(snake_case('WeekPromo'))->nullable()->default(null);
            $table->integer(snake_case('OwnedSite'))->nullable()->default(0);
            $table->string('set_id')->nullable()->default(null);
            $table->integer('max_order_item')->nullable()->default(null);
            $table->decimal('delivery_cost', 10, 2)->nullable()->default(null);
            $table->decimal('assembling_cost', 10, 2)->nullable()->default(null);
            $table->string('type_name_for_page')->nullable()->default(null);
            $table->integer('width')->nullable()->default(null);
            $table->integer('depth')->nullable()->default(null);
            $table->integer('height')->nullable()->default(null);
            $table->string('site_type_id')->nullable()->default(null);
            $table->string('site_width')->nullable()->default(null);
            $table->boolean('installments')->default(0);
        });

        // ColorsOfSetOfPackages
        Schema::create('luckyweb_ms_import_packages_colors', function($table)
        {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->bigInteger('package_id')->unsigned();
            $table->bigInteger('color_id')->unsigned();
            $table->string('title')->nullable()->default(null);
            $table->string('color')->nullable()->default(null);
            $table->integer('number_of_photots')->nullable()->default(null);
            $table->integer(snake_case('PositionInCategory'))->index();
            $table->integer(snake_case('PositionInCategory2'))->index();
            $table->integer(snake_case('PositionInMixedPromo'))->index();
            $table->text(snake_case('LinkToVideo'))->nullable();
            $table->decimal(snake_case('PriceTopSale'), 10,2)->nullable()->default(null);
            $table->decimal(snake_case('PriceLowerSale'), 10,2)->nullable()->default(null);
            $table->decimal(snake_case('DiscountSale'), 10,2)->nullable()->default(null);
            $table->string(snake_case('AlsoInSpecList'))->nullable()->default(null);
            $table->string('slug');
            $table->integer('residual_indicator')->nullable()->default(null);
            $table->boolean('category_invisible')->default(false);
            $table->string('variant_of_design')->nullable()->defaul(null);
        });

        // GoodsOfSetOfPackages
        Schema::create('luckyweb_ms_import_goods_packages', function($table)
        {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('good_id')->unsigned();
            $table->bigInteger('package_id')->unsigned();
            $table->bigInteger('good_color_id')->unsigned();
            $table->bigInteger('package_color_id')->unsigned();
            $table->integer('quantity')->nullable()->default(null);
            //$table->primary(['good_id', 'package_id']);
        });

        // TypesOtherDescription
        Schema::create('luckyweb_ms_import_description_types', function($table)
        {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->integer(snake_case('NumberOfDesc'));
            $table->string('desc_name_1');
            $table->string('desc_name_2');
        });

        // DiscountsOfMonths
        Schema::create('luckyweb_ms_import_promotions', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->nullable()->default(null);
            $table->string('date')->nullable()->default(null);
            $table->integer('number_of_photots')->nullable()->default(null);
            $table->string('banner1')->nullable()->default(null);
            $table->string('banner2')->nullable()->default(null);
            $table->string('category_id')->nullable()->default(null);
            $table->text('video_title')->nullable()->default(null);
            $table->text('link_to_video')->nullable()->default(null);
        });

        Schema::create('luckyweb_ms_import_shops', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('region')->nullable()->default(null)->index();
            $table->string('city')->nullable()->default(null)->index();
            $table->integer('map_number')->nullable()->default(null);
            $table->string('mall_name')->nullable()->default(null);
            $table->string('street_address')->nullable()->default(null);
            $table->string('longlat')->nullable()->default(null);
            $table->string('mode_1')->nullable()->default(null);
            $table->string('mode_2')->nullable()->default(null);
            $table->string('phone')->nullable()->default(null);
            $table->boolean('discount_center')->nullable()->default(null);
            //region;city;map_number;mall_name;street_address;longlat;mode_1;mode_2;phone
        });

        // DescriptionOfFiles
        Schema::create('luckyweb_ms_import_files', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string(snake_case('File'))->nullable()->default(null);
            $table->string(snake_case('Description'))->nullable()->default(null);
            $table->string(snake_case('FileName'))->nullable()->default(null);
            $table->string(snake_case('ShortDescr'))->nullable()->default(null);
            $table->integer(snake_case('OwnedSite'))->nullable()->default(0);
            $table->integer('assignment')->nullable()->default(null);
        });

        // Sets
        Schema::create('luckyweb_ms_import_sets', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('id')->nullable()->default(null);
            $table->string('name')->nullable()->default(null);
        });

        // PositionsInSpecList
        Schema::create('luckyweb_ms_import_spec_positions', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('package_id')->nullable()->default(null);
            $table->integer('package_color_id')->nullable()->default(null);
            $table->integer('good_id')->nullable()->default(null);
            $table->integer('good_color_id')->nullable()->default(null);
            $table->integer('position1')->nullable()->default(null);
            $table->integer('position2')->nullable()->default(null);
        });

        // Specifications
        Schema::create('luckyweb_ms_import_specifications', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer(snake_case('SpecId'))->nullable()->defaul(null);
            $table->string(snake_case('SpecName'))->nullable()->defaul(null);
            $table->integer(snake_case('Sort'))->nullable()->defaul(null);
            $table->integer('popup')->nullable()->defaul(null);
        });

        // ValuesOfSpecifications
        Schema::create('luckyweb_ms_import_specification_values', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer(snake_case('ValueId'))->nullable()->defaul(null);
            $table->string(snake_case('ValueName'))->nullable()->defaul(null);
            $table->integer(snake_case('Sort'))->nullable()->defaul(null);
        });

        // ValuesOfSpecificationsByCategories
        Schema::create('luckyweb_ms_import_specification_values_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer(snake_case('CategoryId'))->nullable()->defaul(null);
            $table->integer(snake_case('SpecId'))->nullable()->defaul(null);
            $table->integer(snake_case('ValueId'))->nullable()->defaul(null);
        });

        // ValuesOfSpecificationsBySets
        Schema::create('luckyweb_ms_import_specification_values_sets', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer(snake_case('ValueId'))->nullable()->defaul(null);
            $table->integer(snake_case('CategoryId'))->nullable()->defaul(null);
            $table->integer(snake_case('SetId'))->nullable()->defaul(null);
        });

        // TogetherCheaper
        Schema::create('luckyweb_ms_import_together_cheeper', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('form_good_id')->nullable()->defaul(null);
            $table->integer('form_package_id')->nullable()->defaul(null);
            $table->integer('supp_good_id')->nullable()->defaul(null);
            $table->integer('supp_package_id')->nullable()->defaul(null);
            $table->integer(snake_case('Saving'))->nullable()->defaul(null);
            $table->integer(snake_case('TheBest'))->nullable()->defaul(null);
        });

        // TypesOfGoodsForSite
        Schema::create('luckyweb_ms_import_site_good_types', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('type_id')->nullable()->defaul(null);
            $table->string('type_name')->nullable()->defaul(null);
            $table->integer('width')->nullable()->defaul(null);
        });



    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_import_categories');
        Schema::dropIfExists('luckyweb_ms_import_sub_categories');
        Schema::dropIfExists('luckyweb_ms_import_goods');
        Schema::dropIfExists('luckyweb_ms_import_colors');
        Schema::dropIfExists('luckyweb_ms_import_goods_colors');
        Schema::dropIfExists('luckyweb_ms_import_furnishers');
        Schema::dropIfExists('luckyweb_ms_import_good_types');
        Schema::dropIfExists('luckyweb_ms_import_packages');
        Schema::dropIfExists('luckyweb_ms_import_packages_colors');
        Schema::dropIfExists('luckyweb_ms_import_goods_packages');
        Schema::dropIfExists('luckyweb_ms_import_description_types');
        Schema::dropIfExists('luckyweb_ms_import_promotions');
        Schema::dropIfExists('luckyweb_ms_import_shops');
        Schema::dropIfExists('luckyweb_ms_import_files');
        Schema::dropIfExists('luckyweb_ms_import_sets');
        Schema::dropIfExists('luckyweb_ms_import_spec_positions');
        Schema::dropIfExists('luckyweb_ms_import_specifications');
        Schema::dropIfExists('luckyweb_ms_import_specification_values');
        Schema::dropIfExists('luckyweb_ms_import_specification_values_categories');
        Schema::dropIfExists('luckyweb_ms_import_specification_values_sets');
        Schema::dropIfExists('luckyweb_ms_import_together_cheeper');
        Schema::dropIfExists('luckyweb_ms_import_site_good_types');
    }

}
