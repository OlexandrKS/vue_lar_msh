<?php namespace FinPlugs\CreditUp\Updates;

use LuckyWeb\MS\Models\ProductGroupType;
use October\Rain\Database\Updates\Seeder;
use Eloquent;
use DB;

class SeedProductGroupTypes extends Seeder
{

    public function run()
    {
        Eloquent::unguard();
        DB::table((new ProductGroupType())->table)->truncate();
        $values = [
            [
                'id' => 1,
                'name' => 'Для отзывов',
            ],
            [
                'id' => 2,
                'name' => 'Для конструтора',
            ],
        ];

        foreach ($values as $value) {
            ProductGroupType::create($value);
        }
    }
}
