<?php namespace Luckyweb\Ms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateFavListItemsTable extends Migration
{
    public function up()
    {
        Schema::create('favorite_list_items', function(Blueprint $table) {
            $table->char('id', 36);
            $table->char('offer_id', 36);
            $table->unsignedInteger('user_id');
            $table->timestamps();
            $table->primary('id');
            $table->index('user_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('favorite_list_items');
    }
}
