<?php namespace Luckyweb\Ms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateBackendFilesTable extends Migration
{
    public function up()
    {
        Schema::create('backend_files', function(Blueprint $table) {
            $table->char('id', 36);
            $table->string('name')->unique();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('backend_files');
    }
}
