<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTogetherCheeperPivotTable extends Migration
{
    public function up()
    {
        $this->down();
        Schema::create('luckyweb_ms_together_cheeper_pivot', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('form_good_id')->nullable()->defaul(null)->index();
            $table->integer('form_package_id')->nullable()->defaul(null)->index();
            $table->integer('supp_good_id')->nullable()->defaul(null)->index();
            $table->integer('supp_package_id')->nullable()->defaul(null)->index();
            $table->integer('supp_category_id')->nullable()->defaul(null)->index();
            $table->decimal('supp_price_lower_b', 10,2)->nullable()->default(null)->index();
            $table->integer('saving')->nullable()->defaul(null)->index();
            $table->integer('the_best')->nullable()->defaul(null)->index();
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_together_cheeper_pivot');
    }
}
