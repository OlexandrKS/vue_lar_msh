<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateVacancyRequestsTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_ms_vacancy_requests', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('vacancy_id')->unsigned()->nullable()->default(null)->index();
            $table->integer('city_id')->unsigned()->nullable()->default(null)->index();

            $table->string('first_name')->nullable()->default(null);
            $table->string('last_name')->nullable()->default(null);
            $table->string('middle_name')->nullable()->default(null);
            $table->date("birth_date")->nullable()->default(null);
            $table->string('citizenship')->nullable()->default(null);
            $table->string('phone')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);
            $table->string('education')->nullable()->default(null);
            $table->string('specialty')->nullable()->default(null);
            $table->text('experience')->nullable()->default(null);
            $table->text('additional_information')->nullable()->default(null);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_vacancy_requests');
    }
}
