<?php namespace LuckyWeb\MS\Updates;

use October\Rain\Database\Updates\Seeder;

use LuckyWeb\MS\Classes\ImportManager;

class ImportSeeder extends Seeder
{
    public function run() {
        ImportManager::import();
    }

}
