<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateSalesManagersTable20160505 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_sales_managers', function($table)
        {
            $table->string('territory')->nullable()->default(null)->after('email');
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_sales_managers'))
        {
            Schema::table('luckyweb_ms_sales_managers', function($table)
            {
                $columns = [
                    'territory'
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('luckyweb_ms_sales_managers', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
