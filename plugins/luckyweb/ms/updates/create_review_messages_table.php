<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateReviewMessagesTable extends Migration
{

    public function up()
    {
        Schema::create('luckyweb_ms_review_messages', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            
            $table->integer('review_id')->unsigned()->nullable()->default(null)->index();
            $table->tinyInteger('direction')->unsigned()->default(1)->index();
            $table->text('text');
            
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_review_messages');
    }

}
