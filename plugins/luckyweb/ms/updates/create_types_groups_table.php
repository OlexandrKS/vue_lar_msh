<?php namespace LuckyWeb\CoffeeTown\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateTypesGroupsTable extends Migration
{

    public function up()
    {
        $this->down();
        Schema::create('luckyweb_ms_types_groups', function($table)
        {
            $table->engine = 'InnoDB';
            $table->primary(['type_id', 'group_id']);
            $table->integer('type_id')->unsigned();
            $table->integer('group_id')->unsigned();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_types_groups');
    }

}
