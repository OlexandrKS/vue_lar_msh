<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdatePromoBlocksTable20160727 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_promo_blocks', function($table)
        {
            $table->boolean('is_show_title')->default(false)->after('subtitle');
            $table->integer('offset_top')->nullable()->default(null)->after('is_show_title');
            $table->integer('offset_bottom')->nullable()->default(null)->after('offset_top');
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_promo_blocks'))
        {
            Schema::table('luckyweb_ms_promo_blocks', function($table)
            {
                $columns = [
                    'is_show_title',
                    'offset_top',
                    'offset_bottom'
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('luckyweb_ms_promo_blocks', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
