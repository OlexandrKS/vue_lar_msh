<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateImagesTable20190124 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_images', function($table)
        {
            $table->string('short_descr')->nullable()->default(null)->after('product_id');
            $table->integer('assignment')->nullable()->default(null)->after('short_descr')->index();
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_images'))
        {
            Schema::table('luckyweb_ms_images', function($table)
            {
                $columns = [
                    'short_descr',
                    'assignment',
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('luckyweb_ms_images', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
