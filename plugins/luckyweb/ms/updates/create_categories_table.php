<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateCategoriesTable extends Migration
{

    public function up()
    {
        $this->down();
        Schema::create('luckyweb_ms_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->integer('price_from');
            $table->integer('order')->default(0);
            $table->string('slug_auto')->index(); // requierd for SEO upgrade from automatic slugs to custom overloaded slugs
            $table->string('slug')->index();
            $table->string(snake_case('PriceDescription'), 1000)->nullable()->default(null);
            $table->text('banner_week_1')->nullable();
            $table->text('banner_week_2')->nullable();
            $table->boolean('active')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_categories');
    }

}
