<?php namespace FinPlugs\CreditUp\Updates;

use LuckyWeb\MS\Models\LastExport;
use October\Rain\Database\Updates\Seeder;
use Eloquent;
use DB;

use LuckyWeb\User\Models\OrderItem;

class SeedLastExports20190117 extends Seeder
{

    public function run()
    {
        Eloquent::unguard();
        $values = [
            [
                'export_type' => OrderItem::class,
                'export_into' => 'Owox',
                'exported_at' => '2019-01-21 00:00:01'
            ],
        ];

        foreach ($values as $value) {
            LastExport::create($value);
        }
    }
}