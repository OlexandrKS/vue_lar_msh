<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateReviewsTable20161124 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_reviews', function($table)
        {
            $table->integer('product_group_id')->unsigned()->nullable()->default(null)->after('product_slug');
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_reviews'))
        {
            Schema::table('luckyweb_ms_reviews', function($table)
            {
                $columns = [
                    'product_group_id'
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('luckyweb_ms_reviews', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
