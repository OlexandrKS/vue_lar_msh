<?php namespace Luckyweb\Ms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateMenuItemsTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_ms_menu_items', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable()->defaul(null);
            $table->integer('order')->default(0)->index();
            $table->string('url')->nullable()->defaul(null);
            $table->integer('type_id')->unsigned()->nullable()->default(1)->index();
            $table->mediumText("right_side")->nullable();
            $table->integer('orientation_id')->unsigned()->nullable()->default(1)->index();
            $table->boolean('other_view')->default(0);
            $table->mediumText("bottom_side")->nullable();
            $table->string("icon")->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_menu_items');
    }
}
