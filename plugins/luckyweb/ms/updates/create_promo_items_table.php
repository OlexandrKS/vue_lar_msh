<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreatePromoItemsTable extends Migration
{

    public function up()
    {
        Schema::create('luckyweb_ms_promo_items', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('block_id')->unsigned()->nullable()->default(null);
            $table->integer('width')->unsigned()->nullable()->default(null);
            $table->integer('offset')->unsigned()->nullable()->default(null);
            $table->integer('sort_order')->unsigned()->nullable()->default(null);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_promo_items');
    }

}
