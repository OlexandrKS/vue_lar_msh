<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateReviewsTable20160523 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_reviews', function($table)
        {
            $table->string('product_slug')->nullable()->default(null)->after('info_status_id');
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_reviews'))
        {
            Schema::table('luckyweb_ms_reviews', function($table)
            {
                $columns = [
                    'product_slug'
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('luckyweb_ms_reviews', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
