<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateReviewsTable extends Migration
{

    public function up()
    {
        $this->down();
        Schema::create('luckyweb_ms_reviews', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('email')->nullable()->default(null)->index();
            $table->string('phone')->nullable()->default(null)->index();
            $table->integer('city_id')->unsigned()->nullable()->default(null)->index();
            $table->boolean('is_client')->default(false);
            $table->string('contract_number')->nullable()->default(null);
            $table->text('text');
            $table->integer('status_id')->unsigned()->default(1)->index();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_reviews');
    }

}
