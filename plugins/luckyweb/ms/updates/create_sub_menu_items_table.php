<?php namespace Luckyweb\Ms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSubMenuItemsTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_ms_sub_menu_items', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable()->defaul(null);
            $table->string('url')->nullable()->defaul(null);
            $table->integer('menu_category_id')->unsigned()->index();
            $table->integer('order')->default(0)->index();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_sub_menu_items');
    }
}
