<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateLastExportsTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_ms_last_exports', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('export_type');
            $table->string('export_into');
            $table->timestamp('exported_at')->nullable()->default(null);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_last_exports');
    }
}
