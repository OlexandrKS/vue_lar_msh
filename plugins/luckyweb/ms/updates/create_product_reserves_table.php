<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductReservesTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_ms_product_reserves', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('name')->nullable()->default(null);
            $table->string('phone')->nullable()->default(null)->index();
            $table->integer('city_id')->unsigned()->nullable()->default(null)->index();
            $table->integer('shop_id')->unsigned()->nullable()->default(null)->index();
            $table->string('time_to_call')->nullable()->default(null);
            $table->text('message');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_product_reserves');
    }
}
