<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateReviewsTable20170108 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_reviews', function($table)
        {
            $table->integer('likes')->nullable()->default(0)->after('product_group_id');
            $table->integer('dislikes')->nullable()->default(0)->after('likes');
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_reviews'))
        {
            Schema::table('luckyweb_ms_reviews', function($table)
            {
                $columns = [
                    'likes',
                    'dislikes'
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('luckyweb_ms_reviews', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
