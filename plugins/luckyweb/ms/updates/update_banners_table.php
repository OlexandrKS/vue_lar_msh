<?php

namespace LuckyWeb\MS\Updates;

use October\Rain\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateBannersTable extends Migration
{
    public function up()
    {
        Schema::table('banners', function(Blueprint $table) {
            if (\DB::connection()->getDriverName() === 'mysql') {
                $table->json('properties')->after('name');
            } else {
                $table->text('properties')->default('')->after('name');
            }
        });

        \DB::table('banners')
            ->update([
                'properties' => '{"head": null, "href": null, "text": null}'
            ]);
    }

    public function down()
    {
        if (Schema::hasTable('banners'))
        {
            Schema::table('banners', function($table)
            {
                $columns = [
                    'properties',
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('banners', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
