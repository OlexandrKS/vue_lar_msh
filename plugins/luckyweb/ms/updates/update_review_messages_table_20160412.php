<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateReviewMessagesTable20160408 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_review_messages', function($table)
        {
            $table->integer('mail_id')->unsigned()->nullable()->default(null)->after('direction');
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_review_messages'))
        {
            Schema::table('luckyweb_ms_review_messages', function($table)
            {
                $columns = [
                    'mail_id'
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('luckyweb_ms_review_messages', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
