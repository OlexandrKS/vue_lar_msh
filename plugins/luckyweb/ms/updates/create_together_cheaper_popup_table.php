<?php namespace LuckyWeb\MS\Updates;

use Illuminate\Support\Facades\DB;
use LuckyWeb\MS\Models\PopupTogetherCheaper;
use October\Rain\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class CreateTogetherCheaperTable extends Migration
{

    public function up()
    {
        Schema::create('luckyweb_ms_together_cheaper_popup', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('timeout');
            $table->string('devices', 255)->nullable()->default(null);
            $table->boolean('active');
            $table->timestamps();
        });

        $model = new PopupTogetherCheaper();
        $model->timeout = 0;
        $model->devices = null;
        $model->active = true;
        $model->save();
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_together_cheaper_popup');
    }

}
