<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductIdsTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_ms_product_ids', function(Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->string('id');
            $table->primary('id');

            $table->bigInteger('numeric_id')->unsigned()->unique();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_product_ids');
    }
}
