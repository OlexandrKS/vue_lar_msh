<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdatePageMetasTable20180716 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_page_metas', function($table)
        {
            $table->text('header')->nullable()->default(null)->after('keywords');
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_page_metas'))
        {
            Schema::table('luckyweb_ms_page_metas', function($table)
            {
                $columns = [
                    'header',
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('luckyweb_ms_page_metas', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
