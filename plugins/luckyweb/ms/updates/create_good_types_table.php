<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateGoodTypesTable extends Migration
{
    public function up()
    {
        $this->down();
        Schema::create('luckyweb_ms_good_types', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('id');
            $table->string('name')->nullable()->default(null)->index();
            $table->string('full_name')->nullable()->default(null)->index();
            $table->integer('category_id')->nullable()->default(null)->index();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_good_types');
    }
}
