<?php

namespace LuckyWeb\MS\Updates;

use October\Rain\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class CreatePageTable extends Migration
{
    public function up()
    {
        if (Schema::hasTable('constructor_page_contents')) {
            Schema::table('constructor_page_contents', function (Blueprint $table) {
                $table->dropForeign('constructor_page_contents_page_id_foreign');
            });
        }

        Schema::dropIfExists('constructor_page');

        Schema::create('constructor_pages', function (Blueprint $table) {
            $table->char('id', 36);
            $table->string('title')->nullable();
            $table->string('name')->nullable();
            $table->string('slug');
            $table->string('breadcrumbs')->nullable();
            $table->string('type', 50);
            $table->text('description')->nullable();
            $table->json('properties');
            $table->timestamps();

            $table->primary('id');
            $table->unique('slug');
        });
    }

    public function down()
    {
        Schema::dropIfExists('constructor_pages');
    }
}
