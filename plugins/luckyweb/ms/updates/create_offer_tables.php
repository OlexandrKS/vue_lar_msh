<?php

namespace LuckyWeb\MS\Updates;

use App\Domain\Enums\Accounting\OrderPaymentMethod;
use App\Domain\Enums\Accounting\OrderStatus;
use Illuminate\Support\Facades\DB;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class CreateOfferTables extends Migration
{
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->char('id', 36);
            $table->string('type', 50);
            $table->string('primary_id');
            $table->json('properties');
            $table->json('prices');
            $table->timestamps();

            $table->primary('id');
            $table->index('primary_id');
            $table->index(['primary_id', 'type']);
        });

        Schema::create('offer_product_pivot', function (Blueprint $table) {
            $table->char('offer_id', 36);
            $table->string('product_id');
            $table->unsignedInteger('quantity')->default(1);

            $table->primary(['offer_id', 'product_id']);
        });

        Schema::create('cart_items', function (Blueprint $table) {
            $table->char('id', 36);
            $table->unsignedInteger('user_id');
            $table->json('offer');

            $table->unsignedInteger('quantity')->default(1);
            $table->json('promotions');
            $table->timestamps();

            $table->primary('id');
            $table->index('user_id');

            // При тестировании не смущаем sqlite
            if ('mysql' === DB::connection()->getDriverName()) {
                $table->char('offer_id', 36)->virtualAs('offer->>"$.id"');
                $table->string('offer_type', 50)->virtualAs('offer->>"$.type"');

                // $table->unique(['used_id', 'offer_id'], 'unique_offer_user');
            }
        });

        Schema::create('orders', function (Blueprint $table) {
            $table->char('id', 36);
            $table->unsignedInteger('user_id');
            $table->unsignedTinyInteger('status')->default(OrderStatus::REQUEST);
            $table->string('payment_method', 50)->default(OrderPaymentMethod::CARD_PAY);
            $table->json('properties');
            $table->json('calculated');
            $table->timestamps();

            $table->primary('id');
            $table->index('user_id');
        });

        Schema::create('order_items', function (Blueprint $table) {
            $table->char('id', 36);
            $table->char('order_id', 36);
            $table->json('offer');
            $table->unsignedInteger('quantity')->default(1);
            $table->json('promotions');

            $table->primary('id');
            $table->index('order_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('order_items');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('offer_product_pivot');
        Schema::dropIfExists('cart_items');
        Schema::dropIfExists('offers');
    }
}
