<?php namespace LuckyWeb\CoffeeTown\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateVacanciesCitiesTable extends Migration
{

    public function up()
    {
        $this->down();
        Schema::create('luckyweb_ms_vacancies_cities', function($table)
        {
            $table->engine = 'InnoDB';
            $table->primary(['vacancy_id', 'city_id']);
            $table->integer('city_id')->unsigned();
            $table->integer('vacancy_id')->unsigned();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_vacancies_cities');
    }

}
