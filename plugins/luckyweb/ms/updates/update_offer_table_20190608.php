<?php

namespace LuckyWeb\MS\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class UpdateOfferTable20190608 extends Migration
{
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->softDeletes();
        });

//        Schema::table('offer_product_pivot', function (Blueprint $table) {
//            $table->foreign('offer_id', 'fk_offer_products_offer_id')
//                ->references('id')->on('offers')
//                ->onDelete('cascade');
//        });

        Schema::table('orders', function (Blueprint $table) {
            $table->softDeletes();

            $table->foreign('user_id', 'fk_orders_user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

//        Schema::table('offer_product_pivot', function (Blueprint $table) {
//            $table->dropForeign('fk_offer_products_offer_id');
//        });

        Schema::table('orders', function (Blueprint $table) {
            $table->dropSoftDeletes();

             $table->dropForeign('fk_orders_user_id');
        });
    }
}
