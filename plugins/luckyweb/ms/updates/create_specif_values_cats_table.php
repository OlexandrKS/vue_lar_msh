<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSpecifValuesCatsTable extends Migration
{
    public function up()
    {
        $this->down();
        Schema::create('luckyweb_ms_specif_values_cats', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('category_id')->index();
            $table->integer('specification_id')->index();
            $table->integer('value_id')->index();
            $table->timestamps();
            $table->primary(['category_id', 'specification_id', 'value_id'], 'primary_'.md5(uniqid()));
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_specif_values_cats');
    }
}
