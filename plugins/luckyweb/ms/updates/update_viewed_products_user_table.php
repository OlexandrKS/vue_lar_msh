<?php

namespace LuckyWeb\MS\Updates;

use October\Rain\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateViewedProductsUserTable extends Migration
{
    public function up()
    {
        Schema::table('luckyweb_ms_viewed_products_user', function (Blueprint $table) {
            $table->integer('status')->after('user_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('luckyweb_ms_viewed_products_user', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }
}
