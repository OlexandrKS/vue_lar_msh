<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateProductPassportsTable20161114 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_product_passports', function($table)
        {
            $table->integer('category_id')->unsigned()->nullable()->default(null)->index()->after('product_name');
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_product_passports'))
        {
            Schema::table('luckyweb_ms_product_passports', function($table)
            {
                $columns = [
                    'category_id',
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('luckyweb_ms_product_passports', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
