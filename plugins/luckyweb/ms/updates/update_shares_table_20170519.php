<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateShareTable20170519 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_shares', function($table)
        {
            $table->integer('sort_order')->nullable()->default(0)->index();
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_shares'))
        {
            Schema::table('luckyweb_ms_shares', function($table)
            {
                $columns = [
                    'sort_order',
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('luckyweb_ms_shares', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
