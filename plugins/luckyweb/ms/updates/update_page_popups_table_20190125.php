<?php namespace LuckyWeb\CoffeeTown\Updates;

use October\Rain\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class UpdatePagePopupTable extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_page_popups', function (Blueprint $table) {
            $table->string('page_show', 255)->nullable()->after('url_hash');
            $table->string('devices', 255)->nullable()->default(null)->after('page_show');
            $table->string('source_traffic', 255)->nullable()->default(null)->after('devices');
            $table->string('variant_footer_popup', 255)->nullable()->after('source_traffic');
            $table->string('text_subscribe', 255)->nullable()->default(null)->after('variant_footer_popup');
            $table->string('modal_size', 25)->nullable()->default(null)->after('text_subscribe');
            $table->boolean('active')->default(false)->after('modal_size');

            $table->dropColumn('url');

        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_page_popups')) {
            Schema::table('luckyweb_ms_page_popups', function ($table) {
                $columns = [
                    'page_show',
                    'devices',
                    'source_traffic',
                    'variant_footer_popup',
                    'text_subscribe',
                    'modal_size',
                    'active'
                ];
                foreach ($columns as $column) {
                    if (Schema::hasColumn('luckyweb_ms_page_popups', $column))
                        $table->dropColumn($column);
                }
                $table->string('url', 2048)->after('conversions');
            });
        }
    }

}
