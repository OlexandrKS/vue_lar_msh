<?php namespace LuckyWeb\MS\Updates;

use October\Rain\Database\Updates\Seeder;

use DB;

use Symfony\Component\Yaml\Yaml;
use LuckyWeb\MS\Models\Settings;


class SettingsSeeder extends Seeder
{
    public function run()
    {
        $import = [
            'ftp' => [
                'host'   => 'v1.mshara.ru',
                'port'  => 21,
                'username' => 'ishop',
                'password'   => 's4tb2VOd0',
                'passive'   => true,
                'path' => 'IShop/',
                'imagePath' => 'IShop/Photos',
            ],
            'updateFlagFile' => 'update.txt',
            'ownedSiteFlags' => [0, 1],
            'csv' => [
                'Categories' => [
                    'fields' => [
                        'CategoryID' => 'id',
                        'CategoryName' => 'name',
                        'SumLower' => 'price_from',
                        'NumberOfPhotos' => snake_case('NumberOfPhotos'),
                        'Order' => 'order',
                        'Slug' => 'slug',
                        'PriceDescription' => snake_case('PriceDescription'),
                        'OwnedSite' => snake_case('OwnedSite'),
                        'BannerWeek1' => 'banner_week_1',
                        'BannerWeek2' => 'banner_week_2',
                    ],
                    'table' => 'categories',
                ],
                'SubCategories' => [
                    'fields' => [
                        'SubCategoryID' => 'id',
                        'SubCategoryName' => 'name',
                        'CategoryID' => 'category_id',
                        'NumberOfPhotos' => snake_case('NumberOfPhotos'),
                        'Order' => 'order',
                        'Slug' => 'slug',
                        'OwnedSite' => snake_case('OwnedSite'),
                    ],
                    'table' => 'sub_categories',
                ],
                'Goods' => [
                    'fields' => [
                        'GoodsID' => 'id',
                        'Description' => snake_case('Description'),
                        'TypeOtherDescription' => 'description_type_id',
                        'OtherDescription1' => 'description1',
                        'OtherDescription2' => 'description2',
                        'GoodsName1C' => snake_case('GoodsName_1c'),
                        'FurnisherID' => snake_case('FurnisherId'),
                        'TypeOfGoodsID' => 'good_type_id',
                        'OverallDimensions' => snake_case('OverallDimensions'),
                        'PriceTopB' => snake_case('PriceTopB'),
                        'PriceLowerB' => snake_case('PriceLowerB'),
                        'DiscountB' => snake_case('DiscountB'),
                        'PriceSpecial' => snake_case('PriceSpecial'),
                        'DiscountSpecial' => snake_case('DiscountSpecial'),
                        'ConditionForDiscount' => snake_case('ConditionForDiscount'),
                        'PricePromo' => snake_case('PricePromo'),
                        'OnlyForSetOfPackages' => snake_case('OnlyForSetOfPackages'),
                        'MixedPromo' => snake_case('MixedPromo'),
                        'DiscountPromo' => snake_case('DiscountPromo'),
                        'CategoryID' => 'category_id',
                        'SubCategoryID' => snake_case('SubCategoryId'),
                        'PriceDescription' => snake_case('PriceDescription'),
                        'OwnedSite' => snake_case('OwnedSite'),
                        'WeekPromo' => snake_case('WeekPromo'),
                        'SetID' => 'set_id',
                        'MaxOrderTerm' => 'max_order_item',
                        'CostOfDelivery' => 'delivery_cost',
                        'CostOfAssembling' => 'assembling_cost',
                        'TypeNameForPage' => 'type_name_for_page',
                        'Width' => 'width',
                        'Depth' => 'depth',
                        'Height' => 'height',
                        'TypeOfGoodsForSiteID' => 'site_type_id', // Код типа товара для сайта (заменяет TypeNameForPage, может быть нулём) (из TypesOfGoodsForSite)
                        'WidthInPixels' => 'site_width', // Ширина изображения в пикселях (если есть - то подменяет собой ширину в пикселях, которая берется из его типа товара для сайта)
                        'Installments' => 'installments',
                    ],
                    'table' => 'goods',
                ],
                'Colors' => [
                    'fields' => [
                        'ColorID' => 'id',
                        'ColorName' => 'name'
                    ],
                    'table' => 'colors'
                ],
                'ColorsOfGoods' => [
                    'fields' => [
                        'GoodsID' => 'good_id',
                        'ColorID' => 'color_id',
                        'Title' => 'title',
                        'TitleColor' => 'color',
                        'NumberOfPhotos' => 'number_of_photots',
                        'PositionInCategory' => snake_case('PositionInCategory'),
                        'PositionInCategory2' => snake_case('PositionInCategory2'),
                        'PositionInMixedPromo' => snake_case('PositionInMixedPromo'),
                        'LinkToVideo' => snake_case('LinkToVideo'),
                        'PriceTopSale' => snake_case('PriceTopSale'),
                        'PriceLowerSale' => snake_case('PriceLowerSale'),
                        'DiscountSale' => snake_case('DiscountSale'),
                        'AlsoInSpecList'=> snake_case('AlsoInSpecList'),
                        'Slug' => 'slug',
                        'ResidualIndicator' => 'residual_indicator',
                        'NotShowInCategory' => 'category_invisible',
                        'VariantOfDesign' => 'variant_of_design',

                    ],
                    'table' => 'goods_colors',
                ],
                'Furnishers' => [
                    'fields' => [
                        'FurnisherID' => 'id',
                        'FurnisherName' => 'name'
                    ],
                    'table' => 'furnishers'
                ],
                'TypesOfGoods' => [
                    'fields' => [
                        'TypeOfGoodsID' => 'id',
                        'TypeOfGoodsName' => 'name',
                        'TypeOfGoodsFName' => 'full_name',
                        'CategoryID' => 'category_id',
                    ],
                    'table' => 'good_types'
                ],
                'SetOfPackages' => [
                    'fields' => [
                        'SetOfPackagesID' => 'id',
                        'Description' => snake_case('Description'),
                        'TypeOtherDescription' => 'description_type_id',
                        'OtherDescription1' => 'description1',
                        'OtherDescription2' => 'description2',
                        'GoodsName1C' => snake_case('GoodsName_1c'),
                        'FurnisherID' => snake_case('FurnisherId'),
                        'TypeOfGoodsID' => 'good_type_id',
                        'OverallDimensions' => snake_case('OverallDimensions'),
                        'PriceTopB' => snake_case('PriceTopB'),
                        'PriceLowerB' => snake_case('PriceLowerB'),
                        'DiscountB' => snake_case('DiscountB'),
                        'PriceSpecial' => snake_case('PriceSpecial'),
                        'DiscountSpecial' => snake_case('DiscountSpecial'),
                        'ConditionForDiscount' => snake_case('ConditionForDiscount'),
                        'PricePromo' => snake_case('PricePromo'),
                        'MixedPromo' => snake_case('MixedPromo'),
                        'DiscountPromo' => snake_case('DiscountPromo'),
                        'CategoryID' => 'category_id',
                        'SubCategoryID' => snake_case('SubCategoryId'),
                        'PriceDescription' => snake_case('PriceDescription'),
                        'OwnedSite' => snake_case('OwnedSite'),
                        'WeekPromo' => snake_case('WeekPromo'),
                        'SetID' => 'set_id',
                        'MaxOrderTerm' => 'max_order_item',
                        'CostOfDelivery' => 'delivery_cost',
                        'CostOfAssembling' => 'assembling_cost',
                        'TypeNameForPage' => 'type_name_for_page',
                        'Width' => 'width',
                        'Depth' => 'depth',
                        'Height' => 'height',
                        'TypeOfGoodsForSiteID' => 'site_type_id', // Код типа товара для сайта (заменяет TypeNameForPage, может быть нулём) (из TypesOfGoodsForSite)
                        'WidthInPixels' => 'site_width', // Ширина изображения в пикселях (если есть - то подменяет собой ширину в пикселях, которая берется из его типа товара для сайта)
                        'Installments' => 'installments',
                    ],
                    'table' => 'packages',
                ],
                'ColorsOfSetOfPackages' => [
                    'fields' => [
                        'SetOfPackagesID' => 'package_id',
                        'ColorID' => 'color_id',
                        'Title' => 'title',
                        'TitleColor' => 'color',
                        'NumberOfPhotos' => 'number_of_photots',
                        'PositionInCategory' => snake_case('PositionInCategory'),
                        'PositionInCategory2' => snake_case('PositionInCategory2'),
                        'PositionInMixedPromo' => snake_case('PositionInMixedPromo'),
                        'LinkToVideo' => snake_case('LinkToVideo'),
                        'PriceTopSale' => snake_case('PriceTopSale'),
                        'PriceLowerSale' => snake_case('PriceLowerSale'),
                        'DiscountSale' => snake_case('DiscountSale'),
                        'AlsoInSpecList'=> snake_case('AlsoInSpecList'),
                        'Slug' => 'slug',
                        'ResidualIndicator' => 'residual_indicator',
                        'NotShowInCategory' => 'category_invisible',
                        'VariantOfDesign' => 'variant_of_design',
                    ],
                    'table' => 'packages_colors',
                ],
                'GoodsOfSetOfPackages' => [
                    'fields' => [
                        'SetOfPackagesID' => 'package_id',
                        'SetOfPackagesColorID' => 'package_color_id',
                        'GoodsID' => 'good_id',
                        'GoodsColorID' => 'good_color_id',
                        'Quantity' => 'quantity',
                    ],
                    'table' => 'goods_packages'
                ],
                'TypesOtherDescription' => [
                    'fields' => [
                        'TypeOtherDescription' => 'id',
                        'NumberOfDesc' => snake_case('NumberOfDesc'),
                        'NameOfDesc1' => 'desc_name_1',
                        'NameOfDesc2' => 'desc_name_2',
                    ],
                    'table' => 'description_types'
                ],
                'DiscountsOfMonths' => [
                    'fields' => [
                        'DiscountsOfMonthsID' => 'id',
                        'DiscountsOfMonthsName' => 'name',
                        'DateOfMonth' => 'date',
                        'NumberOfPhotos' => 'number_of_photots',
                        'Banner1' => 'banner1',
                        'Banner2' => 'banner2',
                        'CategoryID' => 'category_id',
                        'VideoTitle' => 'video_title',
                        'LinkToVideo' => 'link_to_video',
                    ],
                    'table' => 'promotions'
                ],
                'Shops' => [
                    'fields' => [
                        'region' => 'region',
                        'city' => 'city',
                        'map_number' => 'map_number',
                        'mall_name' => 'mall_name',
                        'street_address' => 'street_address',
                        'longlat' => 'longlat',
                        'mode_1' => 'mode_1',
                        'mode_2' => 'mode_2',
                        'phone' => 'phone',
                        'discount_center' => 'discount_center',
                    ],
                    'table' => 'shops'
                ],
                'DescriptionOfFiles' => [
                    'fields' => [
                        'File' => snake_case('File'),
                        'Description' => snake_case('Description'),
                        'FileName' => snake_case('FileName'),
                        'ShortDescr' => snake_case('ShortDescr'),
                        'OwnedSite' => snake_case('OwnedSite'),
                        'Assignment' => 'assignment',
                    ],
                    'table' => 'files'
                ],
                'Sets' => [
                    'fields' => [
                        'SetID' => 'id',
                        'SetName' => 'name'
                    ],
                    'table' => 'sets'
                ],
                'PositionsInSpecList' => [
                    'fields' => [
                        'SetOfPackagesID' => 'package_id',
                        'SetOfPackagesColorID' => 'package_color_id',
                        'GoodsID' => 'good_id',
                        'GoodsColorID' => 'good_color_id',
                        'PositionInSpecList' => 'position1',
                        'PositionInSpecList2' =>'position2',
                    ],
                    'table' => 'spec_positions',
                ],

                'Specifications' => [
                    'fields' => [
                        'SpecID' => snake_case('SpecId'), //  Код техн.характеристики
                        'SpecName' => snake_case('SpecName'), //  Наименование техн.характеристики
                        'Sort' => snake_case('Sort'), //  Сортировка (Число, для внутренней сортировки по возрастанию в нужных местах)
                        'PopUp' => 'popup',
                    ],
                    'table' => 'specifications',
                ],
                'ValuesOfSpecifications' => [
                    'fields' => [
                        'ValueID' => snake_case('ValueId'), // Код значения техн.характеристики
                        'ValueName' => snake_case('ValueName'), // Наименование значения техн.характеристики
                        'Sort' => snake_case('Sort'), // Сортировка (Число, для внутренней сортировки по возрастанию в нужных местах)
                    ],
                    'table' => 'specification_values',
                ],
                'ValuesOfSpecificationsByCategories' => [
                    'fields' => [
                        'CategoryID' => snake_case('CategoryId'), // Код категории (из Categories)
                        'SpecID' => snake_case('SpecId'), // Код техн.характеристики (из Specifications)
                        'ValueID' => snake_case('ValueId'), // Код значения техн.характеристики (из ValuesOfSpecifications)
                    ],
                    'table' => 'specification_values_categories',
                ],
                'ValuesOfSpecificationsBySets' => [
                    'fields' => [
                        'ValueID' => snake_case('ValueId'), // Код значения техн.характеристики (из ValuesOfSpecifications)
                        'CategoryID' => snake_case('CategoryId'), // Код категории (из Categories)
                        'SetID' => snake_case('SetId'), // Код комплекта (из Sets)
                    ],
                    'table' => 'specification_values_sets',
                ],

                'TogetherCheaper' => [
                    'fields' => [
                        'FormGoodsID' => 'form_good_id', // Код образующего товара (из Goods)
                        'FormSetOfPackagesID' => 'form_package_id', // Код образующего НК (из SetOfPackages)
                        'SuppGoodsID' => 'supp_good_id', // Код дополняющего товара (из Goods)
                        'SuppSetOfPackagesID' => 'supp_package_id', // Код дополняющего НК (из SetOfPackages)
                        'Saving' => snake_case('Saving'), // Экономия на товар с образующим типом
                        'TheBest' => snake_case('TheBest'), // "Лучший" вариант для показа на странице товара
                    ],
                    'table' => 'together_cheeper',
                ],

                'TypesOfGoodsForSite' => [ // Типы товаров для сайта
                    'fields' => [
                        'TypeOfGoodsForSiteID' => 'type_id', // Код
                        'TypeOfGoodsForSiteName' => 'type_name', // Наименование
                        'WidthInPixels' => 'width', // Ширина изображения в пикселях для типа
                    ],
                    'table' => 'site_good_types',
                ],

            ],
        ];
        $yaml = Yaml::dump($import, 5);
        //Settings::set('import', $yaml);
        $settings = Settings::instance();
        $settings->import = $yaml;
        $settings->save();
    }

}
