<?php

namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateBpmEventsTable extends Migration
{
    public function up()
    {
        Schema::create('bpm_events', function(Blueprint $table) {
            $table->char('id', 36);
            $table->unsignedTinyInteger('status')->default(0);
            $table->string('type');
            $table->json('user');
            $table->json('analytics');
            $table->json('data');
            $table->timestamps();

            $table->primary('id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('bpm_events');
    }
}
