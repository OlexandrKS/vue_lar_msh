<?php namespace LuckyWeb\Imperial\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateDocumentsTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('luckyweb_ms_documents')) {
            Schema::create('luckyweb_ms_documents', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');

                $table->string('name')->nullable()->default(null);
                $table->string('code')->nullable()->default(null)->index();
                $table->integer('type_id')->unsigned()->nullable()->default(null)->index();

                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_documents');
    }
}
