<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateVacancyCitiesTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_ms_vacancy_cities', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('name')->index();
            $table->string('slug')->index();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_vacancy_cities');
    }
}
