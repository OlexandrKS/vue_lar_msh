<?php namespace LuckyWeb\CoffeeTown\Updates;

use October\Rain\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class UpdatePagePopupTable20190204 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_page_popups', function (Blueprint $table) {
            $table->string('image_url', 255)->nullable()->after('url_hash');
            $table->string('type_button', 255)->nullable()->after('image_url');
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_page_popups')) {
            Schema::table('luckyweb_ms_page_popups', function ($table) {
                $columns = [
                    'url_hash',
                ];
                foreach ($columns as $column) {
                    if (Schema::hasColumn('luckyweb_ms_page_popups', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }

}
