<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSpecificationValuesTable extends Migration
{
    public function up()
    {
        $this->down();
        Schema::create('luckyweb_ms_specification_values', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('id');
            $table->string('name')->nullable()->defaul(null);
            $table->integer('sort')->nullable()->defaul(null);
            $table->timestamps();
            $table->primary('id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_specification_values');
    }
}
