<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateNestedPositionsTable extends Migration
{
    public function up()
    {
        $this->down();
        Schema::create('luckyweb_ms_nested_positions', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('package_product_id');
            $table->string('good_product_id');
            $table->integer('position_row')->default(0);
            $table->integer('position_column')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_nested_positions');
    }
}
