<?php namespace Luckyweb\Ms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSubMenuCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_ms_sub_menu_categories', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable()->defaul(null);
            $table->integer('order')->default(0)->index();
            $table->integer('menu_id')->unsigned()->index();
            $table->string('url')->nullable()->defaul(null);

            $table->integer("column_num")->unsigned()->nullable()->default(1);
            $table->string("icon")->nullable();

            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_sub_menu_categories');
    }
}
