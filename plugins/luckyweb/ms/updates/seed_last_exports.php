<?php namespace FinPlugs\CreditUp\Updates;

use LuckyWeb\MS\Models\LastExport;
use October\Rain\Database\Updates\Seeder;
use Eloquent;
use DB;

use LuckyWeb\User\Models\Order;
use LuckyWeb\User\Models\OrderItem;

class SeedLastExports extends Seeder
{

    public function run()
    {
        Eloquent::unguard();
        $values = [
            [
                'export_type' => OrderItem::class,
                'export_into' => 'Analytics',
                'exported_at' => '2018-12-21 00:00:01'
            ],
        ];

        foreach ($values as $value) {
            LastExport::create($value);
        }
    }
}