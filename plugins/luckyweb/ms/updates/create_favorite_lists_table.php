<?php namespace Luckyweb\Ms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateFavoriteListsTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_ms_favorite_lists', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('list_name')->index();
            $table->bigInteger('user_id')
                ->nullable()->default(null)
                ->unsigned()->index();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_favorite_lists');
    }
}
