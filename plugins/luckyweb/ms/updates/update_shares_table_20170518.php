<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateShareTable20170518 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_shares', function($table)
        {
            $table->integer('type_id')->unsigned()->nullable()->default(1)->index();
            $table->string('link')->nullable()->default(null);
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_shares'))
        {
            Schema::table('luckyweb_ms_shares', function($table)
            {
                $columns = [
                    'type_id',
                    'link'
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('luckyweb_ms_shares', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
