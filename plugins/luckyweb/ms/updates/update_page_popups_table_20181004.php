<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdatePagePopupsTable20181004 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_page_popups', function($table)
        {
            $table->mediumText('html')->after('message')->nullable()->default(null);
            $table->string('url_hash')->after('html')->unique()->nullable()->default(null);
            // change default value for html popups
            $table->unsignedInteger('timeout')->default(0)->change();
            $table->unsignedSmallInteger('conversions')->default(0)->change();
            $table->unsignedSmallInteger('type')->default(3)->change(); // default center
            $table->string('url', 2048)->nullable()->default(null)->change();
            $table->string('left_button_id')->nullable()->default(null)->change();
            $table->string('right_button_id')->nullable()->default(null)->change();
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_page_popups'))
        {
            Schema::table('luckyweb_ms_page_popups', function($table)
            {
                $columns = [
                    'html',
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('luckyweb_ms_page_popups', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
