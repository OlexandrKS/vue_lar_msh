<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateFavoriteListItemsTable20180423 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_favorite_list_items', function($table)
        {
            $table->string('support_id')
                ->nullable()->default(null)
                ->index()->after('count');
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_favorite_list_items'))
        {
            Schema::table('luckyweb_ms_favorite_list_items', function($table)
            {
                $columns = [
                    'support_id',
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('luckyweb_ms_favorite_list_items', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
