<?php

namespace LuckyWeb\MS\Updates;

use October\Rain\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class CreatePageContentsTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('constructor_page_contents');

        Schema::create('constructor_page_component_contents', function (Blueprint $table) {
            $table->char('id', 36);
            $table->char('page_id', 36);
            $table->string('component', 100);
            $table->integer('position')->nullable();
            $table->json('contents');
            $table->timestamps();

            $table->primary('id');
            $table->index('page_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('constructor_page_component_contents');
    }
}
