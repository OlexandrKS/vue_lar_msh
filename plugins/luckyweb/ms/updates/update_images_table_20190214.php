<?php

namespace LuckyWeb\MS\Updates;

use Illuminate\Support\Facades\DB;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class UpdateImagesTable20190214 extends Migration
{
    public function up()
    {
        Schema::table('luckyweb_ms_images', function (Blueprint $table) {
            if (DB::connection()->getDriverName() === 'mysql') {
                $table->json('image_size')->after('assignment');
                $table->json('image_preview_size')->after('image_size');
            } else {
                $table->text('image_size')->default('')->after('assignment');
                $table->text('image_preview_size')->default('')->after('image_size');
            }
        });

        DB::table('luckyweb_ms_images')
            ->update([
                'image_size' => '[0,0]',
                'image_preview_size' => '[0,0]',
            ]);
    }

    public function down()
    {
        Schema::table('luckyweb_ms_images', function (Blueprint $table) {
            $table->dropColumn(['image_size', 'image_preview_size']);
        });
    }
}
