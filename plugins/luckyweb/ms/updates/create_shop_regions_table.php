<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateShopRegionsTable extends Migration
{

    public function up()
    {
        $this->down();
        Schema::create('luckyweb_ms_shop_regions', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->index();
            $table->string('slug')->index();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_shop_regions');
    }

}
