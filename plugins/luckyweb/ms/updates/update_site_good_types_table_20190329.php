<?php

namespace LuckyWeb\MS\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use Schema;

class UpdateSiteGoodTypesTable20190329 extends Migration
{
    public function up()
    {
        Schema::table('luckyweb_ms_site_good_types', function (Blueprint $table) {
            $table->integer('height')->default(0)->after('width');
        });
    }

    public function down()
    {
        Schema::table('luckyweb_ms_site_good_types', function (Blueprint $table) {
            $table->dropColumn(['height']);
        });
    }
}
