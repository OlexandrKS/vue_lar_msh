<?php namespace LuckyWeb\CoffeeTown\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreatePromoPagesBlocksTable extends Migration
{

    public function up()
    {
        $this->down();
        Schema::create('luckyweb_ms_promo_pages_blocks', function($table)
        {
            $table->engine = 'InnoDB';
            $table->primary(['page_id', 'block_id']);
            $table->integer('page_id')->unsigned();
            $table->integer('block_id')->unsigned();
            $table->integer('sort_order')->unsigned()->nullable()->default(null);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_promo_pages_blocks');
    }

}
