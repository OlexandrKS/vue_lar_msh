<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Illuminate\Support\Facades\DB;

class CreateSeoPagesTable extends Migration
{

    public function up()
    {
        Schema::create('luckyweb_ms_seo_pages', function($table)
        {
            $table->increments('id');

            $table->string('name')->index();
            $table->string('url')->index();
        });

        if (DB::connection()->getDriverName() === 'mysql') {
            DB::unprepared(file_get_contents('plugins/luckyweb/ms/updates/luckyweb_ms_seo_pages.sql'));
        }
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_seo_pages');
    }
}
