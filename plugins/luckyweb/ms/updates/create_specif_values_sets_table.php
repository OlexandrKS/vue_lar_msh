<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSpecifValuesSetsTable extends Migration
{
    public function up()
    {
        $this->down();
        Schema::create('luckyweb_ms_specif_values_sets', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('value_id')->index();
            $table->integer('category_id')->index();
            $table->integer('set_id')->index();

            $table->timestamps();
            $table->primary(['value_id', 'category_id', 'set_id'], 'primary_'.md5(uniqid()));
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_specif_values_sets');
    }
}
