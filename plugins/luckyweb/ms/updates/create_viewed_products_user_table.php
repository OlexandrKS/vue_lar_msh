<?php

namespace LuckyWeb\MS\Updates;

use October\Rain\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class CreateViewedProductsUserTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_ms_viewed_products_user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->timestamps();

            $table->index('user_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_viewed_products_user');
    }
}
