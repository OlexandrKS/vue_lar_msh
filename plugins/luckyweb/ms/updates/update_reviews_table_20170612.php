<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateReviewsTable20170612 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_reviews', function($table)
        {
            $table->boolean('is_on_imperial')->default(false)->index()->after('dislikes');
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_reviews'))
        {
            Schema::table('luckyweb_ms_reviews', function($table)
            {
                $columns = [
                    'is_on_imperial',
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('luckyweb_ms_reviews', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
