<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateExchangeFromFrontTable extends Migration
{
    public function up()
    {
        if (!Schema::connection('exchange')->hasTable('exchange_from_front')) {
            Schema::connection('exchange')->create('exchange_from_front', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');

                $table->integer('status_id')->unsigned()->nullable()->default(1)->index();
                $table->string('entity_id')->nullable()->default(null)->index();
                $table->integer('type_id')->unsigned()->nullable()->default(null)->index();
                $table->integer('front_user_id')->unsigned()->nullable()->default(null)->index();
                $table->string('1c_user_id')->nullable()->default(null)->index();
                $table->mediumText('data')->nullable()->default(null);
                $table->timestamp('exported_at')->nullable()->default(null)->index();
                $table->timestamp('imported_at')->nullable()->default(null)->index();

                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::connection('exchange')->dropIfExists('exchange_from_front');
    }
}
