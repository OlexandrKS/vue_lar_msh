<?php namespace LuckyWeb\MS\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class CreateSeoPageLinksTable extends Migration
{

    public function up()
    {
        Schema::create('luckyweb_ms_seo_page_links', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('slug')->index();
            $table->integer('page_id');
            $table->integer('position');
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_seo_page_links');
    }

}
