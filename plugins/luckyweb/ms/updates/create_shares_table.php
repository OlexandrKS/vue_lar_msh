<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSharesTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_ms_shares', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('name')->nullable()->default(null);
            $table->mediumText('text')->nullable()->default(null);;
            $table->integer('status_id')->unsigned()->nullable()->default(1)->index();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_shares');
    }
}
