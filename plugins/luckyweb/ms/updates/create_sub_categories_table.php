<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSubCategoriesTable extends Migration
{
    public function up()
    {
        $this->down();
        Schema::create('luckyweb_ms_sub_categories', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->integer('category_id')->unsigned()->nullable()->default(null)->index();
            $table->integer('order')->default(0);
            $table->string('slug_auto')->index(); // requierd for SEO upgrade from automatic slugs to custom overloaded slugs
            $table->string('slug')->index();
            $table->boolean('active')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_sub_categories');
    }
}
