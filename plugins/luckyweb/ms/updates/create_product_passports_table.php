<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateProductPassportsTable extends Migration
{

    public function up()
    {
        Schema::create('luckyweb_ms_product_passports', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('product_name')->nullable()->default(null);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_product_passports');
    }

}
