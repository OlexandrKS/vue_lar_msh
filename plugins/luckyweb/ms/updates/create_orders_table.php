<?php namespace Luckyweb\Ms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_ms_orders', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('user_id')->unsigned()->nullable()->default(null)->index();
            $table->decimal('total_price', 15, 2)->nullable()->default(null);
            $table->string('payment_method')->nullable()->default(null);
            $table->string('status')->nullable()->default(null);

            $table->timestamps();

            $table->index(['status', 'updated_at']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_orders');
    }
}
