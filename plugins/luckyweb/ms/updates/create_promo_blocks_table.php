<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreatePromoBlocksTable extends Migration
{

    public function up()
    {
        Schema::create('luckyweb_ms_promo_blocks', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            
            $table->string('title')->nullable()->default(null);
            $table->string('subtitle')->nullable()->default(null);
            $table->integer('status_id')->unsigned()->nullable()->default(1);
            
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_promo_blocks');
    }

}
