<?php

namespace LuckyWeb\MS\Updates;

use October\Rain\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class CreateProductsTable extends Migration
{

    public function up()
    {
        $this->down();
        Schema::create('luckyweb_ms_products', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->primary('id');
            $table->string('id');
            $table->integer('category_id')->unsigned()->index(); // good_types
            $table->integer('sub_category_id')->unsigned()->nullable()->default(null)->index(); // good_types
            $table->integer('color_id')->unsigned()->index(); // goods_colors
            $table->integer('good_id')->nullable()->default(null)->unsigned()->index(); // goods_colors
            $table->integer('package_id')->nullable()->default(null)->unsigned()->index(); // packages_colors
            $table->integer('good_type_id')->nullable()->default(null)->unsigned()->index(); // goods_colors
            $table->string('name'); // goods_colors
            $table->string('subname')->nullable()->default(null); // goods_colors
            $table->string('units')->nullable()->default(null);
            $table->mediumText('description')->nullable()->default(null); // goods
            $table->mediumText('description1')->nullable()->default(null); // goods
            $table->mediumText('description2')->nullable()->default(null); // goods
            $table->mediumText('description1_name')->nullable()->default(null); // descrition_types
            $table->mediumText('description2_name')->nullable()->default(null); // descrition_types
            $table->string('dimensions')->nullable()->default(null); // ovarall_dimensions
            $table->decimal('price_top_b', 10,2)->default(0);
            $table->decimal('price_lower_b', 10,2)->default(0);
            $table->integer('discount_b')->nullable()->default(null);
            $table->decimal('price_special', 10,2)->default(0);
            $table->integer('discount_special')->nullable()->default(null);
            $table->integer('discount_promo')->nullable()->default(null);
            $table->string('discount_condition')->nullable()->default(null);
            $table->decimal('price_promo', 10,2)->default(0);
            $table->decimal(snake_case('PriceTopSale'), 10,2)->nullable()->default(null);
            $table->decimal(snake_case('PriceLowerSale'), 10,2)->nullable()->default(null);
            $table->decimal(snake_case('DiscountSale'), 10,2)->nullable()->default(null);
            $table->integer('position_row')->default(0);
            $table->integer('position_column')->default(0);
            $table->boolean('has_mixed_promo')->default(false)->index();
            $table->integer('position_mp_row')->default(0);
            $table->integer('position_mp_column')->default(0);
            $table->string('slug')->index();
            $table->text('video_url')->nullable();
            $table->string(snake_case('PriceDescription'), 1000)->nullable()->default(null);
            $table->boolean(snake_case('WeekPromo'))->nullable()->default(null);
            $table->integer('set_id')->nullable()->default(null)->index();
            $table->boolean(snake_case('AlsoInSpecList'))->default(false)->index();
            $table->integer('max_order_item')->nullable()->default(null)->index();
            $table->integer('residual_indicator')->nullable()->default(null)->index();
            $table->decimal('delivery_cost', 10, 2)->nullable()->default(null);
            $table->decimal('assembling_cost', 10, 2)->nullable()->default(null);
            $table->string('type_name_for_page')->nullable()->default(null);
            $table->integer('width')->nullable()->default(null);
            $table->integer('depth')->nullable()->default(null);
            $table->integer('height')->nullable()->default(null);
            $table->boolean('category_invisible')->default(false)->index();
            $table->string('variant_of_design')->nullable()->defaul(null);
            $table->integer('furnisher_id')->unsigned()->nullable()->default(null);
            $table->integer('site_type_id')->nullable()->default(null)->index();
            $table->integer('site_width')->nullable()->default(null);
            $table->boolean('installments')->default(0);
            $table->timestamps();
        });

        Schema::create('luckyweb_ms_goods_packages', function($table) {
            $table->string('good_product_id');
            $table->string('package_product_id');
            $table->integer('quantity')->nullable()->default(null);
            $table->primary(['good_product_id', 'package_product_id'], 'luckyweb_ms_goods_packages_primary');
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_products');
        Schema::dropIfExists('luckyweb_ms_goods_packages');

    }

}
