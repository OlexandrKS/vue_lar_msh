<?php namespace LuckyWeb\CoffeeTown\Updates;

use October\Rain\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class UpdatePagePopupTable20190408 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_page_popups', function (Blueprint $table) {
            $table->integer('promo_code')->nullable()->default(null)->after('modal_size');
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_page_popups')) {
            Schema::table('luckyweb_ms_page_popups', function ($table) {
                $columns = [
                    'promo_code',
                ];
                foreach ($columns as $column) {
                    if (Schema::hasColumn('luckyweb_ms_page_popups', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }

}
