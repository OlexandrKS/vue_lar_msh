<?php namespace Luckyweb\Ms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateFavoriteListItemsTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_ms_favorite_list_items', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('product_id')
                ->nullable()->default(null);
            $table->integer('count');
            $table->bigInteger('list_id')
                ->nullable()->default(null)
                ->unsigned()->index();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_favorite_list_items');
    }
}
