<?php namespace Luckyweb\Ms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOrderItemsTable extends Migration
{
    public function up()
    {
        Schema::create('luckyweb_ms_order_items', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('order_id')->nullable()->default(null)->index();
            $table->string('product_id')->nullable()->default(null)->index();
            $table->string('name')->nullable()->default(null);
            $table->integer('quantity')->nullable()->default(null);
            $table->decimal('total_price', 15, 2)->nullable()->default(null);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_order_items');
    }
}
