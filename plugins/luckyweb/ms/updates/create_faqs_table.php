<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateFaqsTable extends Migration
{
    
    public function up()
    {
        Schema::create('luckyweb_ms_faqs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            
            $table->text('question')->nullable()->default(null);
            $table->text('answer')->nullable()->default(null);
            $table->integer('status_id')->unsigned()->nullable()->default(1);
            
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_faqs');
    }

}
