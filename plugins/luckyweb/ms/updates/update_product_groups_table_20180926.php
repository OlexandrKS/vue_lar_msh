<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateProductGroupsTable20180926 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_product_groups', function($table)
        {
            $table->integer('page_id')->unsigned()->nullable()->default(null)->index()->after('name');
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_product_groups'))
        {
            Schema::table('luckyweb_ms_product_groups', function($table)
            {
                $columns = [
                    'page_id',
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('luckyweb_ms_product_groups', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
