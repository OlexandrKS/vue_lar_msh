<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateSalesManagersTable extends Migration
{

    public function up()
    {
        Schema::create('luckyweb_ms_sales_managers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('name')->nullable()->default(null);
            $table->string('email')->nullable()->default(null)->index();
            
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_sales_managers');
    }

}
