<?php namespace LuckyWeb\MS\Updates;

use October\Rain\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class CreatePagePopupUrlTable extends Migration
{

    public function up()
    {
        Schema::create('luckyweb_ms_page_popups_url', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->unsignedInteger('page_popup_id');
            $table->timestamps();

            $table->foreign('page_popup_id')->references('id')->on('luckyweb_ms_page_popups')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_page_popups_url');
    }

}
