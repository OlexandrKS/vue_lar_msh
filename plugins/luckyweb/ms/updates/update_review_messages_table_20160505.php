<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateReviewMessagesTable20160505 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_review_messages', function($table)
        {
            $table->boolean('is_publication')->default(false)->after('text');
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_review_messages'))
        {
            Schema::table('luckyweb_ms_review_messages', function($table)
            {
                $columns = [
                    'is_publication'
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('luckyweb_ms_review_messages', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
