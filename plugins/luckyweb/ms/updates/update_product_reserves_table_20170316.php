<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateProductReservesTable20170316 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_product_reserves', function($table)
        {
            $table->string('product_id')->nullable()->default(null)->after('phone');
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_product_reserves'))
        {
            Schema::table('luckyweb_ms_product_reserves', function($table)
            {
                $columns = [
                    'product_id',
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('luckyweb_ms_product_reserves', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
