<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreatePromotionsTable extends Migration
{

    public function up()
    {
        $this->down();
        Schema::create('luckyweb_ms_promotions', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->nullable()->default(null);
            $table->date('date')->index()->nullable()->default(null);
            $table->string('banner_title')->nullable()->default(null);
            $table->string('banner_subtitle')->nullable()->default(null);
            $table->integer('category_id')->unsigned()->nullable()->default(null)->index();
            $table->text('video_title')->nullable()->default(null);
            $table->text('video_url')->nullable()->default(null);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_promotions');
    }

}
