<?php namespace LuckyWeb\MS\Updates;

use Illuminate\Support\Facades\DB;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class FixImagesTableProductId extends Migration
{
    /**
     * Get current column type
     *
     * @return string
     */
    private function getColumnType()
    {
        return DB::connection()->getDoctrineColumn('luckyweb_ms_images', 'product_id')->getType()->getName();
    }

    public function up()
    {
        if (strpos($this->getColumnType(), 'int') !== false) {
            Schema::table('luckyweb_ms_images', function (Blueprint $table) {
                $table->string('product_id')->change();
            });
        }
    }

    public function down()
    {
        if ($this->getColumnType() === 'string') {
            Schema::table('luckyweb_ms_images', function (Blueprint $table) {
                $table->unsignedBigInteger('product_id')->change();
            });
        }
    }
}
