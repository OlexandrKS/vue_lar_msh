<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdatePagePopupsTable20180926 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_page_popups', function($table)
        {
            $table->unsignedTinyInteger('conversions')->default(0)->after('type');
            $table->string('url', 2048)->nullable()->after('conversions');
            $table->string('left_button_id')->nullable()->after('float_header');
            $table->string('right_button_id')->nullable()->after('left_button_link');
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_page_popups'))
        {
            Schema::table('luckyweb_ms_page_popups', function($table)
            {
                $columns = [
                    'conversions',
                    'url',
                    'left_button_id',
                    'right_button_id',
                ];
                foreach ($columns as $column) {
                    if(Schema::hasColumn('luckyweb_ms_page_popups', $column))
                        $table->dropColumn($column);
                }
            });
        }
    }
}
