<?php namespace FinPlugs\CreditUp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateVacanciesTable20170407 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_vacancies', function($table)
        {
            if(Schema::hasColumn('luckyweb_ms_vacancies', 'city_id')) {
                $table->dropColumn('city_id');
            }
        });
    }

    public function down()
    {
        if (Schema::hasTable('luckyweb_ms_vacancies'))
        {
            Schema::table('luckyweb_ms_vacancies', function($table)
            {
                $table->integer('city_id')->unsigned()->nullable()->default(null)->index();
            });
        }
    }
}
