<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateShopCitiesTable extends Migration
{

    public function up()
    {
        $this->down();
        Schema::create('luckyweb_ms_shop_cities', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('region_id')->unsigned()->nullable()->default(null)->index();
            $table->string('name')->index();
            $table->string('slug')->index();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_shop_cities');
    }

}
