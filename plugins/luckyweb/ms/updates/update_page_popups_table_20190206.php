<?php namespace LuckyWeb\CoffeeTown\Updates;

use October\Rain\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class UpdatePagePopupTable20190206 extends Migration
{

    public function up()
    {
        Schema::table('luckyweb_ms_page_popups', function (Blueprint $table) {
            $table->string('image_url', 255)->nullable()->default(null)->after('url_hash')->change();
            $table->string('type_button', 255)->nullable()->default(null)->after('image_url')->change();
        });
    }

    public function down()
    {
    }

}
