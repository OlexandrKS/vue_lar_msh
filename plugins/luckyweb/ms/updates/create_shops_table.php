<?php namespace LuckyWeb\MS\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateShopsTable extends Migration
{

    public function up()
    {
        $this->down();
        Schema::create('luckyweb_ms_shops', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('number')->index();
            $table->integer('city_id')->unsigned()->nullable()->default(null)->index();
            $table->integer('region_id')->unsigned()->nullable()->default(null)->index();
            $table->integer('map_number')->index();
            $table->string('mall_name');
            $table->string('street_address');
            $table->string('longitude');
            $table->string('latitude');
            $table->string('mode_1');
            $table->string('mode_2');
            $table->string('phone');
            $table->boolean('discount_center');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('luckyweb_ms_shops');
    }

}
