<?php

use LuckyWeb\MS\Classes\Markets\GoogleMerchant\GoogleAdwordsManager;
use LuckyWeb\MS\Classes\Markets\GoogleMerchant\GoogleMerchantManager;
use LuckyWeb\MS\Classes\SitemapManager;
use LuckyWeb\MS\Classes\Markets\YandexMarket\YmlManager;
use LuckyWeb\MS\Classes\Markets\WooCommerce\WooManager;
use LuckyWeb\MS\Classes\Markets\MailTorg\TorgManager;
use LuckyWeb\MS\Classes\Markets\SmartBanner\SmartBannerManager;

Route::get('/minimize/{image_id}', 'LuckyWeb\MS\Http\Controllers\ImageController@minimize');

Route::get('sitemap.xml', function () {
    $content = cache()->remember('sitemap.xml', 60, function () {
        $sitemap = new SitemapManager;

        return $sitemap->generate();
    });

    return Response::make($content)
        ->header("Content-Type", "application/xml");
});

// Yandex.Market export
Route::get('export.yml', function () {
    $content = cache()->remember('export.yml', 60, function () {
        return (new YmlManager())->getContent();
    });

    return Response::make($content)
        ->header("Content-Type", "application/xml");
});

// Yandex.Market export without categories
Route::get('export2.yml', function () {
    $content = cache()->remember('export2.yml', 60, function () {
        return (new YmlManager([12], [3]))->getContent();
    });

    return Response::make($content)
        ->header("Content-Type", "application/xml");
});

// Yandex.Market export
Route::get('mail_torg.xml', function () {
    $content = cache()->remember('mail_torg.xml', 60, function () {
        return (new TorgManager())->getContent();
    });

    return Response::make($content)
        ->header("Content-Type", "application/xml");
});

// Yandex Smart Banner
Route::get('smartbanner.xml', function () {
    $content = cache()->remember('smartbanner.xml', 60, function () {
        return (new SmartBannerManager())->getContent();
    });

    return Response::make($content)
        ->header("Content-Type", "application/xml");
});

// Google merchant
Route::get('google_merchant.xml', function () {
    $content = cache()->remember('google_merchant.xml', 60, function () {
        return (new GoogleMerchantManager())->getContent();
    });

    return Response::make($content)
        ->header("Content-Type", "application/xml");
});

// Google merchant CSV
Route::get('google_adwords.csv', function () {
    $content = cache()->remember('google_adwords.xml', 60, function () {
        return (new GoogleAdwordsManager())->getContent();
    });

    return Response::make($content)
        ->header("Content-Type", "text/csv");
});

// Google merchant
Route::get('woo_commerce.xml', function () {
    $content = cache()->remember('woo_commerce.xml', 60, function () {
        return (new WooManager())->getContent();
    });

    return Response::make($content)
        ->header("Content-Type", "application/xml");
});

// Robots
Route::get('robots.txt', function () {
    $content = cache()->remember('robots.txt', 60, function () {
        return view('luckyweb.ms::robots', [
            'host' => Request::getHost(),
            'environment' => App::environment()
        ])->render();
    });

    return Response::make($content)
        ->header('Content-Type', 'text/plain');
});

Route::get('/reviews/products', 'LuckyWeb\MS\Http\Controllers\ReviewsController@getProducts');
