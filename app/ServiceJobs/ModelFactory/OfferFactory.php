<?php

namespace App\ServiceJobs\ModelFactory;

use App\Domain\Entities\Accounting\CartItem;
use App\Domain\Entities\Catalog\Offer;
use App\Domain\Entities\Catalog\OfferProperties\PackageProperties;
use App\Domain\Entities\Catalog\OfferProperties\SingleProperties;
use App\Domain\Entities\Catalog\OfferProperties\TogetherCheapProperties;
use App\Domain\Entities\Catalog\Price;
use App\Domain\Enums\Catalog\OfferType;
use App\Domain\Enums\Catalog\PriceType;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection as SimpleCollection;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\TogetherCheeperPivot;
use October\Rain\Database\Collection;
use Psr\Log\LoggerInterface;

class OfferFactory implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * OfferFactory constructor.
     *
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Offer factory.
     *
     * @param LoggerInterface $logger
     *
     * @throws \Exception
     *
     * @return Offer
     */
    public function handle(LoggerInterface $logger): Offer
    {
        $this->logger = $logger;
        $product = $this->product;

        $offerType = is_null($product->package_id) ? OfferType::SINGLE : OfferType::PACKAGE;

        switch ($offerType) {
            case OfferType::PACKAGE:
                $offer = $this->makePackageOffer($product);
                break;
            case OfferType::SINGLE:
            default:
                $offer = $this->makeSingleOffer($product);
        }

        $this->makeTogetherCheap($offer);

        return $offer;
    }

    /**
     * Single offer factory.
     * Used when product is good.
     *
     * @param Product $product
     *
     * @return Offer
     */
    private function makeSingleOffer(Product $product): Offer
    {
        /** @var Offer $offer */
        $offer = Offer::firstOrNew([
            'type'       => OfferType::SINGLE,
            'primary_id' => $product->getKey(),
        ]);

        $offer->setRelation('primary', $product);
        $offer->setRelation('products', new Collection([$product]));

        $offer->properties = new SingleProperties($offer->properties->toArray());
        $offer->prices = $this->createPricesForOffer($offer);

        $offer->save();
        $offer->products()->sync([
            $product->getKey() => [
                'quantity' => 1, // Single Product attach
            ],
        ]);

        return $offer;
    }

    /**
     * Package offer factory.
     * Used when product is package of goods.
     *
     * @param Product $product
     *
     * @return Offer
     */
    private function makePackageOffer(Product $product): Offer
    {
        /** @var Collection|Product[]|null $packageProducts */
        $packageProducts = $product->getPackageProducts();

        if (0 === $packageProducts->count()) {
            $packageProducts->push($product);
        }

        /** @var Offer $offer */
        $offer = Offer::firstOrNew([
            'type'       => OfferType::PACKAGE,
            'primary_id' => $product->getKey(),
        ]);

        $prices = $this->createPricesForOffer($offer);
        $products = [];

        $offer->setRelation('primary', $product);
        $offer->setRelation('products', $packageProducts);

        /** @var Product $packageProduct */
        foreach ($packageProducts as $packageProduct) {
            $prices = $prices->merge($this->createPricesForOffer($offer, $packageProduct->getKey())->all());

            $products[$packageProduct->getKey()] = [
                'quantity' => data_get($packageProduct, 'set_qunatity', 1),
            ];
        }

        $offer->properties = new PackageProperties($offer->properties->toArray());
        $offer->prices = $prices;

        $offer->save();
        $offer->products()->sync($products);

        return $offer;
    }

    /**
     * Update special "Together cheap" for offer.
     *
     * @param Offer $offer
     *
     * @throws \Exception
     *
     * @return Collection
     */
    private function makeTogetherCheap(Offer $offer): Collection
    {
        $key = OfferType::SINGLE === $offer->type ? 'form_good_id' : 'form_package_id';
        $primaryKey = OfferType::SINGLE === $offer->type ? 'good_id' : 'package_id';

        /** @var Collection|TogetherCheeperPivot[] $togetherCheapCollection */
        $togetherCheapCollection = TogetherCheeperPivot::query()
            ->where($key, $offer->primary->getAttribute($primaryKey))
            ->whereNotNull('supp_category_id')
            ->orderBy('the_best', 'desc')
            ->orderBy('saving', 'desc')
            ->orderBy('supp_price_lower_b', 'asc')
            ->get()
            ->filter(function (TogetherCheeperPivot $cheeperPivot) {
                /** @var Product $supportProduct */
                $supportProduct = $cheeperPivot->getSupportProduct();
                $cheeperPivot->setRelation('support_product', $supportProduct);

                return $supportProduct && 0 == $supportProduct->price_promo && 0 == $supportProduct->price_lower_sale;
            });

        if ($togetherCheapCollection->isEmpty()) {
            // If product hasn't any special cheap - delete already exists
            Offer::where('type', OfferType::TOGETHER_CHEAP)
                ->where('primary_id', $offer->primary->getKey())
                ->delete();

            return new Collection();
        }

        /** @var Collection|Offer[] $collection */
        $collection = Offer::withTrashed()
            ->where('type', OfferType::TOGETHER_CHEAP)
            ->where('primary_id', $offer->primary->getKey())
            ->get();

        // Сохраняем индификаторы офферов для удаления
        $offersForDelete = array_fill_keys($collection->pluck('id')->toArray(), true);
        $responseCollection = new Collection();

        /** @var TogetherCheeperPivot $item */
        foreach ($togetherCheapCollection as $item) {
            /** @var Product $supportProduct */
            $supportProduct = $item->support_product;
            $offerTogether = $collection->first(function (Offer $offer) use ($supportProduct) {
                return $supportProduct->getKey() === $offer->properties->supportProduct;
            }) ?? new Offer([
                    'primary_id' => $offer->primary_id,
                    'type'       => OfferType::TOGETHER_CHEAP,
                ]);

            $offersForDelete[$offerTogether->getKey()] = false;

            $properties = new TogetherCheapProperties([
                'supportProduct'     => $supportProduct->getKey(),
                'saving'             => $item->saving,
                'order'              => $item->the_best,
                'category'           => $item->supp_category_id,
            ]);

            $offerTogether->setRelation('primary', $offer->primary);
            $offerTogether->setRelation('products', new Collection([
                $offer->primary_id        => $offer->primary,
                $supportProduct->getKey() => $supportProduct,
            ]));

            $offerTogether->properties = $properties;
            $offerTogether->prices = $this->createPricesForTogetherCheap($offerTogether);

            $offerTogether->save();

            if ($offerTogether->trashed()) {
                $offerTogether->restore();
            }

            $offerTogether->products()->sync([
                $offer->primary_id        => ['quantity' => 1],
                $supportProduct->getKey() => ['quantity' => 1],
            ]);

            $responseCollection->push($offerTogether);
        }

        $offersForDeleteIds = collect($offersForDelete)->filter(function ($isDeleting) {
            return $isDeleting;
        })
            ->keys();

        try {
            // Удаляем все старые офферы, на которые уже не действует акция
            Offer::whereIn('id', $offersForDeleteIds)
                ->delete();

            // Удаляем из корзины офферы
            CartItem::whereIn('offer_id', $offersForDeleteIds)
                ->delete();
        } catch (\Exception $e) {
            $this->logger->error($e);
        }

        return $responseCollection;
    }

    /**
     * @param Offer $offer
     * @param null  $productId
     * @param bool  $withDeliveryAssembly
     *
     * @return Collection|Price[]
     */
    private function createPricesForOffer(Offer $offer, $productId = null, $withDeliveryAssembly = true): SimpleCollection
    {
        $collection = collect();
        $map = [
            PriceType::TOP     => 'price_top_b',
            PriceType::LOWER   => 'price_lower_b',
            PriceType::SPECIAL => 'price_special',
            PriceType::SALE    => 'price_lower_sale',
            PriceType::PROMO   => 'price_promo',
        ];

        /** @var Product $product */
        $product = is_null($productId) || $productId === $offer->primary_id ?
            $offer->primary :
            $offer->products->find($productId);

        // Получаем текущие цены из продукта
        foreach ($map as $type => $field) {
            if (($value = $product->getAttribute($field)) > 0) {
                $collection->push(new Price([
                    'type'    => $type,
                    'product' => $productId,
                    'value'   => $value,
                ]));
            }
        }

        if ($withDeliveryAssembly) {
            // Отдельно заполняем цены на доставку и сборку так же из продукта
            $collection
                ->push(new Price([
                    'type'    => PriceType::DELIVERY,
                    'product' => $productId,
                    'value'   => $product->getDeliveryCost(),
                ]))
                ->push(new Price([
                    'type'    => PriceType::ASSEMBLING,
                    'product' => $productId,
                    'value'   => $product->getAssemblingCost(),
                ]));
        }

        return $collection;
    }

    /**
     * @param Offer $offer
     *
     * @return Collection|Price[]
     */
    private function createPricesForTogetherCheap(Offer $offer): SimpleCollection
    {
        $map = [
            PriceType::TOP     => function (Offer $offer) {
                return $offer->primary->price_top_b + $offer->support_product->price_top_b;
            },
            PriceType::LOWER   => function (Offer $offer) {
                return $offer->primary->price_lower_b + $offer->support_product->price_lower_b;
            },
            PriceType::SPECIAL => function (Offer $offer) {
                // Ценобразвание для вместе дешевле
                return $offer->primary->price_lower_b + $offer->support_product->price_lower_b - $offer->properties->saving;
            },
//            OfferPriceType::SALE    => 'price_lower_sale',
//            OfferPriceType::PROMO   => 'price_promo',
        ];

        $fromPrices = $this->createPricesForOffer($offer, $offer->primary_id, false);
        $supportPrices = $this->createPricesForOffer($offer, $offer->properties->supportProduct, false);

        $collection = $fromPrices->merge($supportPrices->all());

        foreach ($map as $type => $field) {
            if (is_callable($field)) {
                $collection->push(new Price([
                    'type'  => $type,
                    'value' => call_user_func($field, $offer),
                ]));

                continue;
            }

            $total = new Price([
                'type'    => $type,
                'product' => null,
                'value'   => data_get($offer->primary, $field, 0),
            ]);

            $collection->push($total);
        }

        $collection
            ->push(new Price([
                'type'  => PriceType::DELIVERY,
                'value' => $offer->primary->getDeliveryCost() + $offer->support_product->getDeliveryCost(),
            ]))
            ->push(new Price([
                'type'  => PriceType::ASSEMBLING,
                'value' => $offer->primary->getAssemblingCost() + $offer->support_product->getAssemblingCost(),
            ]));

        return $collection;
    }
}
