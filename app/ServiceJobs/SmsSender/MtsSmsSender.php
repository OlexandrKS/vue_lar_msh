<?php

namespace App\ServiceJobs\SmsSender;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class MtsSmsSender implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * @var string
     */
    protected $phone;

    /**
     * @var string
     */
    protected $message;

    /**
     * MtsSmsSender constructor.
     *
     * @param string $phone
     * @param string $message
     */
    public function __construct(string $phone, string $message)
    {
        $this->phone = $phone;
        $this->message = $message;
    }

    public function handle()
    {
        $data = [
            'msid'     => $this->phone,
            'message'  => $this->message,
            'login'    => config('services.mts.login'),
            'password' => config('services.mts.password'),
            'naming'   => config('services.mts.naming'),
        ];

        $client = new Client();

        $response = $client->post('https://www.mcommunicator.ru/m2m/m2m_api.asmx/SendMessage', [
            'form_params' => $data,
        ]);
    }
}
