<?php

namespace App\ServiceJobs\Importer;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use League\Flysystem\FilesystemInterface;
use LuckyWeb\MS\Models\Image;

class ImageUpload implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * @var Image
     */
    protected $image;

    /**
     * ImageImport constructor.
     *
     * @param Image $image
     */
    public function __construct(Image $image)
    {
        $this->image = $image;
    }

    /**
     * @param Dispatcher $queue
     * @param Factory    $factory
     */
    public function handle(Dispatcher $queue, Factory $factory)
    {
        try {
            /** @var FilesystemInterface $importDriver */
            $importDriver = $this->getImportImagesFilesystem($factory)->getDriver();
            /** @var FilesystemInterface $storageDriver */
            $storageDriver = $this->getImagesFilesystem($factory)->getDriver();

            $stream = $importDriver->readStream($this->image->remote_name);
            $storageDriver->putStream($this->image->local_name, $stream);

            $queue->dispatch(new ImageSizeCalculate($this->image));
        } catch (\Throwable $e) {
            $this->fail($e);
        }
    }

    /**
     * @param Factory $factory
     *
     * @return Filesystem
     */
    private function getImportImagesFilesystem(Factory $factory): Filesystem
    {
        return $factory->disk(
            config('importer.filesystem.import_images')
        );
    }

    /**
     * @param Factory $factory
     *
     * @return Filesystem
     */
    private function getImagesFilesystem(Factory $factory): Filesystem
    {
        return $factory->disk(
            config('importer.filesystem.images')
        );
    }
}
