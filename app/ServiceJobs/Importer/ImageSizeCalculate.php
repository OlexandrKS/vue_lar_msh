<?php

namespace App\ServiceJobs\Importer;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use LuckyWeb\MS\Models\Image;

class ImageSizeCalculate implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * @var Image
     */
    protected $image;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var string
     */
    protected $attribute;

    /**
     * SizeImageJob constructor.
     *
     * @param Image  $image
     * @param string $path
     * @param string $attribute
     */
    public function __construct(Image $image, $path = null, $attribute = 'image_size')
    {
        $this->image = $image;
        $this->path = $path;
        $this->attribute = $attribute;
    }

    /**
     * @param Factory $factory
     *
     * @return bool
     */
    public function handle(Factory $factory)
    {
        $filesystem = $factory->disk('images');

        $path = $this->path ?? $this->image->storage_path;

        try {
            $content = $filesystem->get($path);
            $this->image->setAttribute($this->attribute, getimagesizefromstring($content));
        } catch (\Exception $e) {
            return false;
        }

        return $this->image->save();
    }
}
