<?php

namespace App\Contracts;

use App\ServiceLayer\ModelHydration\Hydrator;
use October\Rain\Database\Collection;
use October\Rain\Database\Model;

interface HydrationFactory
{
    /**
     * @param string        $entity
     * @param array         $data
     * @param Hydrator|null $hydrator
     *
     * @return Collection|Model[]
     */
    public function hydrate(string $entity, array $data, Hydrator $hydrator = null): Collection;
}
