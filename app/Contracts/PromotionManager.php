<?php

namespace App\Contracts;

use App\Domain\Entities\Accounting\Order\Calculation;
use App\ServiceLayer\PriceCalculator\CalculationRequest;
use App\ServiceLayer\Promotions\Promotion;
use Illuminate\Contracts\Auth\Authenticatable;

interface PromotionManager
{
    /**
     * @param string                  $promotionName
     * @param CalculationRequest|null $request
     * @param Authenticatable|null    $user
     * @param array                   $params
     *
     * @return bool
     */
    public function canUse(string $promotionName, CalculationRequest $request = null,
                           Authenticatable $user = null, array $params = []): bool;

    /**
     * @param string               $promotionName
     * @param Calculation|null     $calculation
     * @param Authenticatable|null $user
     * @param array                $params
     *
     * @return bool
     */
    public function apply(string $promotionName, Calculation $calculation = null,
                          Authenticatable $user = null, array $params = []): bool;

    /**
     * @param string $promotionName
     *
     * @return Promotion
     */
    public function getByName(string $promotionName): Promotion;
}
