<?php

namespace App\Contracts;

use App\ServiceLayer\SmsSender\ThrottleException;

interface SmsSender
{
    /**
     * @param string $phone
     * @param string $message
     * @param bool   $throttle
     *
     * @throws ThrottleException
     */
    public function handle(string $phone, string $message, bool $throttle = true);
}
