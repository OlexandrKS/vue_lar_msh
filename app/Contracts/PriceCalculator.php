<?php

namespace App\Contracts;

use App\Domain\Entities\Accounting\Order\Calculation;
use App\ServiceLayer\PriceCalculator\CalculationRequest;

interface PriceCalculator
{
    /**
     * @param CalculationRequest $request
     *
     * @return \App\Domain\Entities\Accounting\Order\Calculation
     */
    public function calculate(CalculationRequest $request): Calculation;
}
