<?php

namespace App\Contracts;

use App\Backend\Entities\Constructor\Page;
use App\Backend\ServiceLayer\Constructor\Component;
use App\Backend\ServiceLayer\Constructor\Page as PageContract;
use Cms\Classes\PageCode as OctoberPage;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

interface ConstructorManager
{
    /**
     * @param Request          $request
     * @param OctoberPage|null $octoberPage
     *
     * @throws ModelNotFoundException
     * @throws \Throwable
     *
     * @return string
     */
    public function render(Request $request, OctoberPage $octoberPage = null): string;

    /**
     * @param string $type
     *
     * @return Component
     */
    public function createComponent(string $type): Component;

    /**
     * @param string $type
     *
     * @return \App\Backend\ServiceLayer\Constructor\Page
     */
    public function createPage(string $type): PageContract;

    /**
     * @param Page $page
     *
     * @return string
     */
    public function renderPage(Page $page): string;
}
