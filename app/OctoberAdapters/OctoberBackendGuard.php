<?php

namespace App\OctoberAdapters;

use Backend\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use October\Rain\Auth\AuthException;

class OctoberBackendGuard extends OctoberGuard
{
    /**
     * {@inheritdoc}
     */
    public function user()
    {
        $user = $this->authManager->getUser();

        if (! is_null($user)) {
            $user = $this->adaptUser($user);
            $this->setUser($user);
        }

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function validate(array $credentials = [])
    {
        try {
            $user = $this->authManager->findUserByCredentials($credentials);

            if ($user) {
                $user = $this->adaptUser($user);
            }

            $this->lastAttempted = $user;

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attempt(array $credentials = [], $remember = false)
    {
        try {
            $user = $this->authManager->authenticate($credentials, $remember);

            if ($user) {
                $user = $this->adaptUser($user);
            }

            $this->lastAttempted = $user;

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param Authenticatable|BackendUserAdapter $user
     * @param bool                               $remember
     */
    public function login(Authenticatable $user, $remember = false)
    {
        try {
            $this->authManager->login($user->getBackendUser(), $remember);
        } catch (AuthException $e) {
        }
    }

    /**
     * {@inheritdoc}
     */
    public function loginUsingId($id, $remember = false)
    {
        if (! is_null($user = $this->authManager->findUserById($id))) {
            $user = $this->adaptUser($user);
            $this->login($user, $remember);

            return $user;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function onceUsingId($id)
    {
        if (! is_null($user = $this->authManager->findUserById($id))) {
            $user = $this->adaptUser($user);
            $this->setUser($user);

            return $user;
        }

        return false;
    }

    /**
     * @param Authenticatable|BackendUserAdapter $user
     *
     * @return $this|OctoberGuard|void
     */
    public function setUser(Authenticatable $user)
    {
        $this->authManager->setUser($user->getBackendUser());
        $this->user = $user->getBackendUser();
        $this->loggedOut = false;

        $this->fireAuthenticatedEvent($user);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'admin_auth';
    }

    /**
     * @param User $user
     *
     * @return BackendUserAdapter
     */
    protected function adaptUser(User $user)
    {
        return new BackendUserAdapter($user);
    }
}
