<?php

namespace App\OctoberAdapters;

use Backend\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Support\Str;

class BackendUserProvider implements UserProvider
{
    /**
     * @var Hasher
     */
    protected $hasher;

    /**
     * {@inheritdoc}
     */
    public function __construct(Hasher $hasher)
    {
        $this->hasher = $hasher;
    }

    /**
     * {@inheritdoc}
     */
    public function retrieveById($identifier)
    {
        $user = User::query()
            ->where('id', $identifier)
            ->first();

        return $this->adapt($user);
    }

    /**
     * {@inheritdoc}
     */
    public function retrieveByToken($identifier, $token)
    {
        $user = $this->retrieveById($identifier);

        if (! $user) {
            return null;
        }

        $rememberToken = $user->getRememberToken();

        return $rememberToken && hash_equals($rememberToken, $token) ? $user : null;
    }

    /**
     * {@inheritdoc}
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        $backendUser = $user->getBackendUser();

        $user->setRememberToken($token);
        $timestamps = $backendUser->timestamps;
        $backendUser->timestamps = false;
        $backendUser->save();
        $backendUser->timestamps = $timestamps;
    }

    /**
     * {@inheritdoc}
     */
    public function retrieveByCredentials(array $credentials)
    {
        if (empty($credentials) ||
            (1 === count($credentials) &&
                array_key_exists('password', $credentials))) {
            return;
        }

        // First we will add each credential element to the query as a where clause.
        // Then we can execute the query and, if we found a user, return it in a
        // Eloquent User "model" that will be utilized by the Guard instances.
        $query = User::query();

        foreach ($credentials as $key => $value) {
            if (! Str::contains($key, 'password')) {
                $query->where($key, $value);
            }
        }

        $user = $query->first();

        return $this->adapt($user);
    }

    /**
     * {@inheritdoc}
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        $plain = $credentials['password'];

        return $this->hasher->check($plain, $user->getAuthPassword());
    }

    /**
     * @param User|null $user
     *
     * @return Authenticatable|null
     */
    protected function adapt(?User $user)
    {
        if ($user) {
            $user = new BackendUserAdapter($user);
        }

        return $user;
    }
}
