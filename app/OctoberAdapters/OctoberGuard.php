<?php

namespace App\OctoberAdapters;

use Illuminate\Auth\SessionGuard;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Session\Session;
use October\Rain\Auth\AuthException;
use October\Rain\Auth\Manager;
use Symfony\Component\HttpFoundation\Request;

class OctoberGuard extends SessionGuard
{
    /**
     * @var Manager
     */
    protected $authManager;

    /**
     * {@inheritdoc}
     */
    public function __construct(string $name, UserProvider $provider, Session $session,
                                Manager $authManager, Request $request = null)
    {
        parent::__construct($name, $provider, $session, $request);

        $this->authManager = $authManager;
    }

    /**
     * {@inheritdoc}
     */
    public function user()
    {
        $user = $this->authManager->getUser();

        if (! is_null($user)) {
            $this->setUser($user);
        }

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function validate(array $credentials = [])
    {
        try {
            $this->lastAttempted = $this->authManager->findUserByCredentials($credentials);

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attempt(array $credentials = [], $remember = false)
    {
        try {
            $this->lastAttempted = $this->authManager->authenticate($credentials, $remember);

            return true;
        } catch (\Exception $e) {
            $this->fireFailedEvent(null, $credentials);

            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function once(array $credentials = [])
    {
        return parent::once($credentials);
    }

    /**
     * {@inheritdoc}
     */
    public function login(Authenticatable $user, $remember = false)
    {
        try {
            $this->authManager->login($user, $remember);
        } catch (AuthException $e) {
        }
    }

    /**
     * {@inheritdoc}
     */
    public function loginUsingId($id, $remember = false)
    {
        if (! is_null($user = $this->authManager->findUserById($id))) {
            $this->login($user, $remember);

            return $user;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function onceUsingId($id)
    {
        if (! is_null($user = $this->authManager->findUserById($id))) {
            $this->setUser($user);

            return $user;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function viaRemember()
    {
        return parent::viaRemember();
    }

    /**
     * {@inheritdoc}
     */
    public function logout()
    {
        $this->authManager->logout();
    }

    /**
     * {@inheritdoc}
     */
    public function setUser(Authenticatable $user)
    {
        $this->authManager->setUser($user);

        parent::setUser($user);
    }

    /**
     * Determine if the current user is authenticated.
     *
     * @return bool
     */
    public function check()
    {
        return $this->authManager->check();
    }

    /**
     * Determine if the current user is a guest.
     *
     * @return bool
     */
    public function guest()
    {
        return ! $this->authManager->check();
    }

    /**
     * Get the ID for the currently authenticated user.
     *
     * @return int|null
     */
    public function id()
    {
        if ($this->loggedOut) {
            return null;
        }

        return $this->user()
            ? $this->user()->getAuthIdentifier()
            : array_get($this->session->get($this->getName()), 0);
    }

    /**
     * Get a unique identifier for the auth session value.
     *
     * @return string
     */
    public function getName()
    {
        return 'user_auth';
    }
}
