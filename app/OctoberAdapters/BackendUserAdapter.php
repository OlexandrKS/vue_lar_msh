<?php

namespace App\OctoberAdapters;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Support\Arrayable;
use October\Rain\Auth\Models\User;
use Tymon\JWTAuth\Contracts\JWTSubject;

class BackendUserAdapter implements Authenticatable, \ArrayAccess, JWTSubject, Arrayable
{
    /**
     * @var User
     */
    protected $user;

    /**
     * BackendUserAdapter constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthIdentifierName()
    {
        return $this->user->getKeyName();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthIdentifier()
    {
        return $this->user->getAuthIdentifier();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthPassword()
    {
        return $this->user->getAuthPassword();
    }

    /**
     * {@inheritdoc}
     */
    public function getRememberToken()
    {
        return $this->user->getRememberToken();
    }

    /**
     * {@inheritdoc}
     */
    public function setRememberToken($value)
    {
        $this->user->setRememberToken($value);
    }

    /**
     * {@inheritdoc}
     */
    public function getRememberTokenName()
    {
        return $this->user->getRememberTokenName();
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset)
    {
        return $this->user->offsetExists($offset);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        return $this->user->offsetGet($offset);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        $this->user->offsetSet($offset, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset)
    {
        $this->user->offsetUnset($offset);
    }

    /**
     * {@inheritdoc}
     */
    public function getJWTIdentifier()
    {
        return $this->getAuthIdentifier();
    }

    /**
     * {@inheritdoc}
     */
    public function getJWTCustomClaims()
    {
        return [
            'user' => [
                'name' => "{$this->user->first_name} {$this->user->last_name}",
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        return $this->user->toArray();
    }

    /**
     * {@inheritdoc}
     */
    public function __get($name)
    {
        return $this->user->__get($name);
    }

    /**
     * {@inheritdoc}
     */
    public function __set($name, $value)
    {
        return $this->user->__set($name, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function __isset($name)
    {
        return $this->user->__isset($name);
    }

    /**
     * {@inheritdoc}
     */
    public function __unset($name)
    {
        $this->user->__unset($name);
    }

    /**
     * {@inheritdoc}
     */
    public function __call($name, $arguments)
    {
        return $this->user->__call($name, $arguments);
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return $this->user->__toString();
    }

    /**
     * @return User
     */
    public function getBackendUser(): User
    {
        return $this->user;
    }
}
