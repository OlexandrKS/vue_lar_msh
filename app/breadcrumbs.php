<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Интернет-Магазин «Мебель Шара»', url('/'));
});

Breadcrumbs::register('page', function ($breadcrumbs, $title, $slug = '/') {
    $breadcrumbs->parent('home');

    $breadcrumbs->push($title, url($slug));
});

Breadcrumbs::register('constructor-category-list', function ($breadcrumbs, $page) {
    $breadcrumbs->parent('home');

    $breadcrumbs->push($page->breadcrumbs ?? $page->title, url($page->slug));
});

Breadcrumbs::register('constructor-category', function ($breadcrumbs, $page) {
    $breadcrumbs->parent('home');

    if ($page->parent_page->breadcrumbs) {
        $breadcrumbs->push($page->parent_page->breadcrumbs ?? $page->parent_page->title, url($page->parent_page->slug));
    }

    $breadcrumbs->push($page->breadcrumbs ?? $page->title, url($page->slug));
});

Breadcrumbs::register('product', function ($breadcrumbs, \LuckyWeb\MS\Models\Product $product) {
    $categoryPage = data_get($product, 'good_type_site.constructor_page');

    if ($categoryPage) {
        $breadcrumbs->parent('constructor-category', $categoryPage);
    } else {
        $breadcrumbs->parent('home');
    }

    $breadcrumbs->push($product->name, $product->url);
});

Breadcrumbs::register('shares', function ($breadcrumbs, $title, $slug = '/') {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Акции', '/shares');

    $breadcrumbs->push($title, url($slug));
});
