<?php

namespace App\Console\Commands;

use App\ServiceLayer\Importer\ImageTransformer;
use Illuminate\Console\Command;
use LuckyWeb\MS\Models\Image;
use Symfony\Component\Console\Helper\ProgressBar;

class ImageMinimizeCommand extends Command
{
    /**
     * @var string the console command name
     */
    protected $name = 'app:images:minimize';

    /**
     * @var string the console command description
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     *
     * @param \App\ServiceLayer\Importer\ImageTransformer $imageTransformer
     */
    public function handle(ImageTransformer $imageTransformer)
    {
        $this->output->writeln('Minimize images');

        $images = Image::query()
            ->where('minimized', false)
            ->get();

        $images->load('product', 'product.good_type_site');

        $progress = new ProgressBar($this->getOutput(), $images->count());

        foreach ($images as $image) {
            $imageTransformer->minimize($image);
            $progress->advance();
        }
    }
}
