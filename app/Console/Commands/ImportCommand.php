<?php

namespace App\Console\Commands;

use App\ServiceLayer\Importer\ImportManager;
use Illuminate\Console\Command;

class ImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:import {mode} --minimize --force --offers --index';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import data for marketplace';

    /**
     * Execute the console command.
     *
     * @param ImportManager $importManager
     *
     * @throws \Throwable
     *
     * @return int
     */
    public function handle(ImportManager $importManager)
    {
        $mode = $this->argument('mode');

        if (! in_array($mode, ['all', 'images', 'data'])) {
            $this->error("Mode '$mode' is not available");

            return 1;
        }

        if (! $importManager->canImport($this->hasOption('force'))) {
            $this->error('Can not import data without force option');

            return 0;
        }

        if ('all' === $mode || 'data' === $mode) {
            $this->info('Start importing data');

            $importManager->importData($this->getOutput());
            $this->line(PHP_EOL);

            $this->info('Update entities');
            $importManager->fillEntities($this->getOutput());
            $this->line(PHP_EOL);
        }

        if ('all' === $mode || 'images' === $mode) {
            $this->info('Start importing images');

            $importManager->importImages($this->getOutput());
            $this->info(PHP_EOL);
        }

        $this->info('Import finished');

        if (('all' === $mode || 'images' === $mode) && $this->hasOption('minimize')) {
            $this->call('app:images:minimize');
        }

        if ($this->hasOption('offers')) {
            $this->call('app:offer:build');
        }

        if ($this->hasOption('index')) {
            $this->call('search:esindex');
        }

        return 0;
    }
}
