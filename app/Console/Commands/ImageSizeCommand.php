<?php

namespace App\Console\Commands;

use App\ServiceJobs\Importer\ImageSizeCalculate;
use Illuminate\Console\Command;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Contracts\Queue\Queue;
use LuckyWeb\MS\Models\Image;
use Symfony\Component\Console\Helper\ProgressBar;

class ImageSizeCommand extends Command
{
    /**
     * @var string the console command name
     */
    protected $name = 'app:images:size';

    /**
     * @var string the console command description
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     *
     * @param Dispatcher $queue
     */
    public function handle(Dispatcher $queue)
    {
        $this->output->writeln('Filling images size');

        $images = Image::query()
            ->whereRaw("json_contains(image_size, '[0, 0]')")
            ->orWhereNull('image_size')
            ->get();

        $progress = new ProgressBar($this->getOutput(), $images->count());

        foreach ($images as $image) {
            $queue->dispatch(new ImageSizeCalculate($image));
            $progress->advance();
        }
    }
}
