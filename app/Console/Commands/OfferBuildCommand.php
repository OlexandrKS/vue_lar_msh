<?php

namespace App\Console\Commands;

use App\ServiceJobs\ModelFactory\OfferFactory;
use Illuminate\Console\Command;
use Illuminate\Contracts\Bus\Dispatcher;
use LuckyWeb\MS\Models\Product;
use Symfony\Component\Console\Helper\ProgressBar;

class OfferBuildCommand extends Command
{
    /**
     * @var string the console command name
     */
    protected $name = 'app:offer:build';

    /**
     * @var string the console command description
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     *
     * @param Dispatcher $bus
     */
    public function handle(Dispatcher $bus)
    {
        // todo laravel
        $products = Product::join('luckyweb_ms_images as i', 'luckyweb_ms_products.id', '=', 'i.product_id')
            ->orderBy('luckyweb_ms_products.id')
            ->select('luckyweb_ms_products.*')
            ->groupBy('luckyweb_ms_products.id')
            ->get();

        $progress = new ProgressBar($this->getOutput(), $products->count());

        foreach ($products as $product) {
            $bus->dispatch(new OfferFactory($product));

            $progress->advance();
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
