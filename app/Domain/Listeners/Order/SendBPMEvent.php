<?php

namespace App\Domain\Listeners\Order;

use App\Domain\Events\Accounting\OrderCreated;
use App\ServiceLayer\BPM\LeadManager;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendBPMEvent implements ShouldQueue
{
    /**
     * @var LeadManager
     */
    private $leadManager;

    /**
     * SendBPMEvent constructor.
     *
     * @param LeadManager $leadManager
     */
    public function __construct(LeadManager $leadManager)
    {
        $this->leadManager = $leadManager;
    }

    /**
     * @param OrderCreated $event
     */
    public function handle(OrderCreated $event)
    {
        $this->leadManager->sendCheckoutEvent($event->order);
    }
}
