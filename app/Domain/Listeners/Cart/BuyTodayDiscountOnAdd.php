<?php

namespace App\Domain\Listeners\Cart;

use App\Domain\Entities\Accounting\Promotions\BuyTodayDiscount;
use App\Domain\Events\Accounting\CartAddedOffer;
use Carbon\Carbon;
use Illuminate\Contracts\Cookie\QueueingFactory;
use Illuminate\Http\Request;

class BuyTodayDiscountOnAdd
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var QueueingFactory
     */
    private $queueingFactory;

    /**
     * BuyTodayDiscountOnAdd constructor.
     *
     * @param Request         $request
     * @param QueueingFactory $queueingFactory
     */
    public function __construct(Request $request, QueueingFactory $queueingFactory)
    {
        $this->request = $request;
        $this->queueingFactory = $queueingFactory;
    }

    /**
     * @param CartAddedOffer $event
     */
    public function handle(CartAddedOffer $event)
    {
        if (! $this->request->hasCookie(BuyTodayDiscount::COOKIE_NAME)) {
            $expired = Carbon::now()->endOfDay()->timestamp;
            $used = false;

            $cookieValue = json_encode(compact('expired', 'used'));

            $this->request->cookies->set(BuyTodayDiscount::COOKIE_NAME, $cookieValue);
            // Создаем куку с временем жизни 11 дней: 1 день активна и 10 дней ждет своего часа.
            $cookie = cookie(BuyTodayDiscount::COOKIE_NAME, $cookieValue, 11 * 24 * 60, null, null, false, false);
            $this->queueingFactory->queue($cookie);
        }
    }
}
