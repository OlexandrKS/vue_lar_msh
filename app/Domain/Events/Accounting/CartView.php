<?php

namespace App\Domain\Events\Accounting;

use App\Domain\Entities\Accounting\CartItem;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Queue\SerializesModels;

class CartView
{
    use SerializesModels;

    /**
     * @var Collection|CartItem[]
     */
    private $items;

    /**
     * @var Authenticatable
     */
    public $user;

    /**
     * CartView constructor.
     *
     * @param Collection      $items
     * @param Authenticatable $user
     */
    public function __construct(Collection $items, Authenticatable $user)
    {
        $this->items = $items;
        $this->user = $user;
    }
}
