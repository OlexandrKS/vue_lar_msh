<?php

namespace App\Domain\Events\Accounting;

use App\Domain\Entities\Accounting\Order;
use Illuminate\Queue\SerializesModels;

class OrderCreated
{
    use SerializesModels;

    /**
     * @var Order
     */
    public $order;

    /**
     * OrderCreated constructor.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }
}
