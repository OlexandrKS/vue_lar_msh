<?php

namespace App\Domain\Events\Accounting;

use App\Domain\Entities\Accounting\CartItem;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Queue\SerializesModels;

class CartDeletedOffer
{
    use SerializesModels;

    /**
     * @var CartItem
     */
    public $cartItem;

    /**
     * @var Authenticatable
     */
    public $user;

    /**
     * {@inheritdoc}
     *
     * @param CartItem             $item
     * @param Authenticatable|null $user
     */
    public function __construct(CartItem $item, Authenticatable $user = null)
    {
        $this->cartItem = $item;
        $this->user = $user;
    }
}
