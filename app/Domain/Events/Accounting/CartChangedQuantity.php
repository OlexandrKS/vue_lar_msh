<?php

namespace App\Domain\Events\Accounting;

use App\Domain\Entities\Accounting\CartItem;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Queue\SerializesModels;

class CartChangedQuantity
{
    use SerializesModels;

    /**
     * @var CartItem
     */
    public $cartItem;

    /**
     * @var Authenticatable|null
     */
    public $user;

    /**
     * {@inheritdoc}
     */
    public function __construct(CartItem $cartItem, Authenticatable $user = null)
    {
        $this->cartItem = $cartItem;
        $this->user = $user;
    }
}
