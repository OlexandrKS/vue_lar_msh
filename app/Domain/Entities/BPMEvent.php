<?php

namespace App\Domain\Entities;

use App\ServiceLayer\Support\ModelUuidKey;
use October\Rain\Database\Model;

/**
 * @property string $id
 * @property int    $status
 * @property string $type
 * @property array  $user
 * @property array  $analytics
 * @property array  $data
 *
 * @mixin \Eloquent
 */
class BPMEvent extends Model
{
    use ModelUuidKey;

    /**
     * @var string the database table used by the model
     */
    public $table = 'bpm_events';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['id'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $casts = [
        'user'      => 'array',
        'analytics' => 'array',
        'data'      => 'array',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
