<?php

namespace App\Domain\Entities\Catalog\OfferProperties;

/**
 * @property string           $supportProduct
 * @property int|float|string $saving
 * @property int              $order
 * @property int              $category
 */
class TogetherCheapProperties extends BaseProperties
{
    /**
     * @var string
     */
    private $supportProduct;

    /**
     * @var float
     */
    private $saving;

    /**
     * @var int
     */
    private $order;

    /**
     * @var int
     */
    private $category;

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        return parent::toArray() +
            [
                'supportProduct' => $this->supportProduct,
                'saving'         => $this->saving,
                'order'          => $this->order,
                'category'       => $this->category,
            ];
    }
}
