<?php

namespace App\Domain\Entities\Catalog\OfferProperties;

use App\ServiceLayer\ValueObject\ValueObject;

/**
 * @property bool $canChangeCount
 */
abstract class BaseProperties extends ValueObject
{
    /**
     * @var bool
     */
    protected $canChangeCount = true;

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'canChangeCount' => $this->canChangeCount,
        ];
    }
}
