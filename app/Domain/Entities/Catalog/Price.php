<?php

namespace App\Domain\Entities\Catalog;

use App\ServiceLayer\ValueObject\ValueObject;

/**
 * Price ValueObject.
 *
 * @property string           $type
 * @property string|null      $product
 * @property int|float|string $value
 */
class Price extends ValueObject
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var string|null
     */
    private $product;

    /**
     * @var float
     */
    private $value;

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        return [
            'type'    => $this->type,
            'product' => $this->product,
            'value'   => $this->value,
        ];
    }
}
