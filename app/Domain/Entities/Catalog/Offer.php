<?php

namespace App\Domain\Entities\Catalog;

use App\Domain\Entities\Accounting\FavoriteListItem;
use App\Domain\Entities\Catalog\OfferProperties\BaseProperties;
use App\Domain\Entities\Catalog\OfferProperties\PackageProperties;
use App\Domain\Entities\Catalog\OfferProperties\SingleProperties;
use App\Domain\Entities\Catalog\OfferProperties\TogetherCheapProperties;
use App\Domain\Enums\Catalog\OfferType;
use App\Domain\Enums\Catalog\PriceType;
use App\ServiceLayer\Support\ModelUuidKey;
use App\ServiceLayer\ValueObject\CastObjects;
use Illuminate\Support\Collection as SimpleCollection;
use LuckyWeb\MS\Models\Product;
use October\Rain\Database\Collection;
use October\Rain\Database\Model;
use October\Rain\Database\Relations\BelongsToMany;
use October\Rain\Database\Traits\SoftDelete;

/**
 * Offer Model.
 *
 * @property-read string $id
 * @property string                                                     $type
 * @property SingleProperties|PackageProperties|TogetherCheapProperties $properties
 * @property string                                                     $primary_id
 * @property-read int $numeric_key
 * @property-read Product|null $support_product
 * @property-read Price $active_price
 * @property-read Price $default_price
 * @property-read int $active_discount
 * @property-read Collection $products_active_price
 * @property-read Product $primary
 * @property-read Collection|Product[] $products
 * @property Collection|Price[] $prices
 *
 * @method BelongsToMany products()
 * @method static        \October\Rain\Database\QueryBuilder togetherCheap(string $primaryId, string $supportProductId)
 *
 * @mixin \Eloquent
 */
class Offer extends Model
{
    use ModelUuidKey;
    use CastObjects;
    use SoftDelete;

    /**
     * @var string the database table used by the model
     */
    public $table = 'offers';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['id'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $casts = [
        'prices'     => 'array',
        'properties' => 'array',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'favorite_list' => [
            FavoriteListItem::class,
            'key'   => 'offer_id',
            'scope' => 'currentUser',
        ],
    ];
    public $belongsTo = [
        'primary' => [
            Product::class,
            'key' => 'primary_id',
        ],
    ];
    public $belongsToMany = [
        'products' => [
            Product::class,
            'table'    => 'offer_product_pivot',
            'key'      => 'offer_id',
            'otherKey' => 'product_id',
            'pivot'    => ['quantity'],
        ],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * Get numeric key for seo.
     *
     * @return int
     */
    public function getNumericKeyAttribute(): int
    {
        // Also, you can use entity ProductIds
        return mb_substr(base_convert(mb_substr(sha1($this->primary_id), 1, 16), 16, 10), 1, 10);
    }

    /**
     * @return Product|null
     */
    public function getSupportProductAttribute() // : ?Product
    {
        if (OfferType::TOGETHER_CHEAP === $this->type) {
            return $this->products->find($this->properties->supportProduct);
        }

        return null;
    }

    /**
     * @param null $productId
     *
     * @return Price|null
     */
    private function activePriceFilter($productId = null)
    {
        if (OfferType::TOGETHER_CHEAP === $this->type) {
            return $this->getPriceByType(PriceType::SPECIAL);
        }

        $priorityOrder = [
            PriceType::SALE  => 30,
            PriceType::PROMO => 20,
            PriceType::LOWER => 0,
        ];

        return $this->prices
            ->where('product', $productId)
            ->sortByDesc(function (Price $price) use ($priorityOrder) {
                return array_get($priorityOrder, $price->type, -100);
            })
            ->first();
    }

    /**
     * @return Price
     */
    public function getActivePriceAttribute(): Price
    {
        $price = $this->activePriceFilter();

        if (is_null($price)) {
            return new Price([
                'type'  => PriceType::LOWER,
                'value' => $this->relationLoaded('primary') ? $this->primary->getActivePrice() : 0,
            ]);
        }

        return $price;
    }

    /**
     * @return Collection|Price
     */
    public function getProductsActivePriceAttribute(): Collection
    {
        $collection = new Collection();

        foreach ($this->products as $product) {
            $price = $this->activePriceFilter($product->getKey());

            if (is_null($price)) {
                $price = $this->primary->getActivePrice();
            }

            $collection->put($product->getKey(), $price);
        }

        return $collection;
    }

    /**
     * @return SimpleCollection
     */
    public function getPricesAttribute()
    {
        if (array_key_exists('prices', $this->objectCasts)) {
            return $this->objectCasts['prices'];
        }

        $collection = collect();
        $prices = array_get($this->attributes, 'prices', '[]');

        foreach ($this->fromJson($prices) as $attributes) {
            $collection->push(new Price($attributes));
        }

        return $this->objectCasts['prices'] = $collection;
    }

    /**
     * @param $prices
     */
    public function setPricesAttribute(SimpleCollection $prices)
    {
        $this->objectCasts['prices'] = $prices;

        $this->attributes['prices'] = $prices->toJson();
    }

    /**
     * @return SingleProperties|PackageProperties|TogetherCheapProperties
     */
    public function getPropertiesAttribute()
    {
        if (array_key_exists('properties', $this->objectCasts)) {
            return $this->objectCasts['properties'];
        }

        switch ($this->type) {
            case OfferType::SINGLE:
                $class = SingleProperties::class;
                break;
            case OfferType::PACKAGE:
                $class = PackageProperties::class;
                break;
            case OfferType::TOGETHER_CHEAP:
                $class = TogetherCheapProperties::class;
                break;
            default:
                throw new \RuntimeException("Cant create properties for offer {$this->type}");
        }

        return $this->objectCasts['properties'] = $class::fromJson(array_get($this->attributes, 'properties', '[]'));
    }

    /**
     * @param BaseProperties $properties
     */
    public function setPropertiesAttribute(BaseProperties $properties)
    {
        $this->objectCasts['properties'] = $properties;

        $this->attributes['properties'] = json_encode($properties->jsonSerialize());
    }

    /**
     * @param string      $type
     * @param string|null $product
     *
     * @return Price|null
     */
    public function getPriceByType(string $type, string $product = null)
    {
        return $this->prices->first(function (Price $price) use ($type, $product) {
            return $price->type === $type && $price->product === $product;
        });
    }

    /**
     * @return Price|null
     */
    public function getDefaultPriceAttribute()
    {
        return $this->getPriceByType(PriceType::LOWER);
    }

    /**
     * @todo move to prices
     *
     * @return float
     */
    public function getActiveDiscountAttribute()
    {
        return $this->primary->getActiveDiscount();
    }

    /**
     * @param \October\Rain\Database\QueryBuilder $query
     *
     * @return \October\Rain\Database\QueryBuilder
     */
    public function scopeStoredPrimary($query)
    {
        return $query->whereIn('type', [
            OfferType::SINGLE,
            OfferType::PACKAGE,
        ]);
    }

    /**
     * @param \October\Rain\Database\QueryBuilder $query
     *
     * @return \October\Rain\Database\QueryBuilder
     */
    public function scopeTogetherCheaper($query)
    {
        return $query->whereType(OfferType::TOGETHER_CHEAP);
    }

    /**
     * @param \October\Rain\Database\QueryBuilder $query
     * @param string                              $primaryId
     * @param string                              $supportProductId
     *
     * @return \October\Rain\Database\QueryBuilder
     */
    public function scopeTogetherCheap($query, $primaryId, $supportProductId)
    {
        return $query->where('type', OfferType::TOGETHER_CHEAP)
            ->where('primary_id', $primaryId)
            ->where('properties->supportProduct', $supportProductId);
    }
}
