<?php

namespace App\Domain\Entities\Accounting\Calculators;

use App\Contracts\PromotionManager;
use App\Domain\Entities\Accounting\Order\Calculation;
use App\Domain\Entities\Accounting\Promotions\PromoCode as PromoCodePromotion;
use App\ServiceLayer\PriceCalculator\CalculationRequest;
use LuckyWeb\User\Models\PromoCodeEvent;

class PromoCode
{
    /**
     * @var PromotionManager
     */
    private $promotionManager;

    /**
     * UseBonuses constructor.
     *
     * @param PromotionManager $promotionManager
     */
    public function __construct(PromotionManager $promotionManager)
    {
        $this->promotionManager = $promotionManager;
    }

    /**
     * @param CalculationRequest $request
     * @param $next
     *
     * @return Calculation
     */
    public function handle(CalculationRequest $request, $next)
    {
        $code = $request->promotion(PromoCodePromotion::name());

        if (is_null($code)) {
            return $next($request);
        }

        /** @var PromoCodeEvent $promoCode */
        $promoCode = PromoCodeEvent::available($request->getUser(), null, $code)
            ->first();

        if ($promoCode) {
            foreach ($request->items() as $item) {
                $promotions = $item->promotions;
                $promotions[PromoCodePromotion::ANY_PROMOTION_USE] = true;
                $promotions[PromoCodePromotion::name()] = $code;
                $item->promotions = $promotions;
            }
        }

        /** @var Calculation $calculated */
        $calculated = $next($request);

        if ($promoCode && $this->promotionManager->canUse(PromoCodePromotion::name(), $request, $request->getUser(), [
                'model' => $promoCode,
                'value' => $calculated->withoutPromotions,
            ])
        ) {
            // Заменяем любую доступную скидку на промокод
            $calculated->promoDiscounts = (float) $promoCode->promo_code->amount;
        }

        return $calculated;
    }
}
