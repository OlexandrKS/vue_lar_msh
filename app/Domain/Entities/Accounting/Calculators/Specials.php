<?php

namespace App\Domain\Entities\Accounting\Calculators;

use App\Domain\Entities\Accounting\CartItem;
use App\Domain\Enums\Catalog\OfferType;
use App\ServiceLayer\PriceCalculator\CalculationRequest;
use App\ServiceLayer\Promotions\Promotion;

class Specials
{
    /**
     * @param CalculationRequest $request
     * @param $next
     *
     * @return mixed
     */
    public function handle(CalculationRequest $request, $next)
    {
        /** @var CartItem $item */
        foreach ($request->items() as $item) {
            // Если у товара скидка больше или равна 50% или это предложения по акции вместе дешевле
            if (OfferType::TOGETHER_CHEAP === $item->offer->type || $item->offer->active_discount >= 50) {
                $promotions = $item->promotions;
                $promotions[Promotion::ANY_PROMOTION_USE] = true;
                $item->promotions = $promotions;
            }
        }

        return $next($request);
    }
}
