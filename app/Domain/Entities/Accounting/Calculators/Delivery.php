<?php

namespace App\Domain\Entities\Accounting\Calculators;

use App\Domain\Entities\Accounting\CartItem;
use App\Domain\Entities\Accounting\Order\Calculation;
use App\Domain\Enums\Accounting\OrderDeliveryType;
use App\Domain\Enums\Catalog\PriceType;
use App\ServiceLayer\PriceCalculator\CalculationRequest;

class Delivery
{
    /**
     * @param CalculationRequest $request
     * @param $next
     *
     * @return Calculation
     */
    public function handle(CalculationRequest $request, $next)
    {
        /** @var Calculation $calculated */
        $calculated = $next($request);

        if (in_array($request->deliveryType(), [OrderDeliveryType::ADDRESS, OrderDeliveryType::PICKUP])) {
            /** @var CartItem $item */
            foreach ($request->items() as $item) {
                $calculated->delivery += data_get($item->offer->getPriceByType(PriceType::DELIVERY), 'value') * $item->quantity;
            }
        }

        return $calculated;
    }
}
