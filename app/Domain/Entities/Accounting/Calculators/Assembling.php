<?php

namespace App\Domain\Entities\Accounting\Calculators;

use App\Domain\Entities\Accounting\CartItem;
use App\Domain\Entities\Accounting\Order\Calculation;
use App\Domain\Enums\Catalog\PriceType;
use App\ServiceLayer\PriceCalculator\CalculationRequest;

class Assembling
{
    /**
     * @param CalculationRequest $request
     * @param $next
     *
     * @return Calculation
     */
    public function handle(CalculationRequest $request, $next)
    {
        /** @var Calculation $calculated */
        $calculated = $next($request);

        if ($request->hasAssembling()) {
            /** @var CartItem $item */
            foreach ($request->items() as $item) {
                $calculated->assembling += data_get($item->offer->getPriceByType(PriceType::ASSEMBLING), 'value') * $item->quantity;
            }
        }

        return $calculated;
    }
}
