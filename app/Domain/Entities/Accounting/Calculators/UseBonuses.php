<?php

namespace App\Domain\Entities\Accounting\Calculators;

use App\Contracts\PromotionManager;
use App\Domain\Entities\Accounting\Order\Calculation;
use App\Domain\Entities\Accounting\Promotions\UserBonuses;
use App\ServiceLayer\PriceCalculator\CalculationRequest;

class UseBonuses
{
    /**
     * @var PromotionManager
     */
    private $promotionManager;

    /**
     * UseBonuses constructor.
     *
     * @param PromotionManager $promotionManager
     */
    public function __construct(PromotionManager $promotionManager)
    {
        $this->promotionManager = $promotionManager;
    }

    /**
     * @param CalculationRequest $request
     * @param $next
     *
     * @return Calculation
     */
    public function handle(CalculationRequest $request, $next)
    {
        // количество бонусов для использования
        $bonuses = (int) array_get($request->promotions(), UserBonuses::name(), 0);
        $usedBonuses = false;

        if ($request->hasPromotion(UserBonuses::name()) &&
            $bonuses > 0 &&
            $this->promotionManager->canUse(UserBonuses::name(), $request, $request->getUser())
        ) {
            foreach ($request->items() as $item) {
                $promotions = $item->promotions;
                $promotions[UserBonuses::ANY_PROMOTION_USE] = true;
                $promotions[UserBonuses::name()] = true;
                $item->promotions = $promotions;
            }

            $usedBonuses = true;
        }

        /** @var Calculation $calculated */
        $calculated = $next($request);

        if ($usedBonuses && $calculated->withoutPromotions > UserBonuses::MIN_BONUSES_PER_ORDER) {
            // максимальная сумма, которую можно оплатить бонусами
            $maxBonuses = $calculated->withoutPromotions / 2;

            // Заменяем любую доступную скидку на бонусы
            $calculated->promoDiscounts = (float) $bonuses > $maxBonuses ? $maxBonuses : $bonuses;
        }

        return $calculated;
    }
}
