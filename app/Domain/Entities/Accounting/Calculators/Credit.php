<?php

namespace App\Domain\Entities\Accounting\Calculators;

use App\Domain\Entities\Accounting\Order\Calculation;
use App\Domain\Enums\Accounting\OrderPaymentMethod;
use App\ServiceLayer\PriceCalculator\CalculationRequest;

class Credit
{
    /**
     * @param CalculationRequest $request
     * @param $next
     *
     * @return Calculation
     */
    public function handle(CalculationRequest $request, $next)
    {
        /** @var Calculation $calculated */
        $calculated = $next($request);

        if (OrderPaymentMethod::CREDIT === $request->paymentMethod()) {
            $calculated->delivery = 0;
            $calculated->assembling = 0;
        }

        return $calculated;
    }
}
