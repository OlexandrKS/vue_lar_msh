<?php

namespace App\Domain\Entities\Accounting\Calculators;

use App\Contracts\PromotionManager;
use App\Domain\Entities\Accounting\Order\Calculation;
use App\Domain\Entities\Accounting\Promotions\FreeDelivery30Plus as FreeDeliveryPromotion;
use App\ServiceLayer\PriceCalculator\CalculationRequest;
use Illuminate\Http\Request;

class FreeDelivery30Plus
{
    /**
     * @var PromotionManager
     */
    private $promotionManager;

    /**
     * @var Request
     */
    private $request;

    /**
     * FreeDelivery constructor.
     *
     * @param PromotionManager $promotionManager
     * @param Request          $request
     */
    public function __construct(PromotionManager $promotionManager, Request $request)
    {
        $this->promotionManager = $promotionManager;
        $this->request = $request;
    }

    /**
     * @param CalculationRequest $request
     * @param $next
     *
     * @return Calculation
     */
    public function handle(CalculationRequest $request, $next)
    {
        if (! $this->request->hasCookie(FreeDeliveryPromotion::COOKIE_NAME) ||
            ! $request->hasPromotion(FreeDeliveryPromotion::name())
        ) {
            return $next($request);
        }

        /** @var Calculation $calculated */
        $calculated = $next($request);

        $canUseFreeDelivery = $this->promotionManager->canUse(
            FreeDeliveryPromotion::name(), $request, $request->getUser(), compact('calculated')
        );

        if ($canUseFreeDelivery) {
            $calculated->delivery = 0;
            $promotions = $calculated->promotions;
            $promotions[FreeDeliveryPromotion::name()] = true;
            $calculated->promotions = $promotions;
        }

        return $calculated;
    }
}
