<?php

namespace App\Domain\Entities\Accounting\Calculators;

use App\Contracts\PromotionManager;
use App\Domain\Entities\Accounting\CartItem;
use App\Domain\Entities\Accounting\Order\Calculation;
use App\Domain\Entities\Accounting\Promotions\BuyTodayDiscount as BuyTodayPromotion;
use App\ServiceLayer\PriceCalculator\CalculationRequest;

class BuyTodayDiscount
{
    // Скидка в 5 процентов
    const DISCOUNT = .05;

    /**
     * @var PromotionManager
     */
    private $promotionManager;

    /**
     * BuyTodayDiscount constructor.
     *
     * @param PromotionManager $promotionManager
     */
    public function __construct(PromotionManager $promotionManager)
    {
        $this->promotionManager = $promotionManager;
    }

    /**
     * @param CalculationRequest $request
     * @param $next
     *
     * @throws \Exception
     *
     * @return mixed
     */
    public function handle(CalculationRequest $request, $next)
    {
        $hasBuyTodayDiscount = false;

        /** @var CartItem $item */
        foreach ($request->items() as $item) {
            if ($request->hasPromotion(BuyTodayPromotion::name()) &&
                ! array_key_exists(BuyTodayPromotion::ANY_PROMOTION_USE, $item->promotions)
            ) {
                $promotions = $item->promotions;
                $promotions[BuyTodayPromotion::ANY_PROMOTION_USE] = true;
                $promotions[BuyTodayPromotion::name()] = true;
                $promotions[BuyTodayPromotion::DISCOUNT_VALUE] = $item->quantity * $item->offer->active_price->value * static::DISCOUNT;
                $item->promotions = $promotions;

                $hasBuyTodayDiscount = true;
            }
        }

        /** @var Calculation $calculated */
        $calculated = $next($request);

        if ($hasBuyTodayDiscount) {
            // округляем до 50 рублей
            $calculated->promoDiscounts = ceil($calculated->promoDiscounts / 50) * 50;
        }

        return $calculated;
    }
}
