<?php

namespace App\Domain\Entities\Accounting\Promotions;

use App\Domain\Entities\Accounting\Order\Calculation;
use App\Domain\Enums\Accounting\OrderPaymentMethod;
use App\ServiceLayer\PriceCalculator\CalculationRequest;
use App\ServiceLayer\Promotions\Promotion;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Cookie\QueueingFactory;
use Illuminate\Http\Request;

class BuyTodayDiscount implements Promotion
{
    const COOKIE_NAME = 'fb_cal';

    /**
     * @var Request
     */
    private $request;

    /**
     * @var QueueingFactory
     */
    private $cookieFactory;

    /**
     * BuyTodayDiscount constructor.
     *
     * @param Request         $request
     * @param QueueingFactory $cookieFactory
     */
    public function __construct(Request $request, QueueingFactory $cookieFactory)
    {
        $this->request = $request;
        $this->cookieFactory = $cookieFactory;
    }

    /**
     * {@inheritdoc}
     */
    public static function name(): string
    {
        return 'buy_today_discount';
    }

    /**
     * {@inheritdoc}
     */
    public function apply(Calculation $calculation = null, Authenticatable $user = null, array $params = []): bool
    {
        if ($this->request->hasCookie(static::COOKIE_NAME)) {
            $cookieValue = json_decode(request()->cookie(BuyTodayDiscount::COOKIE_NAME), true);
            $cookieValue['used'] = true;
            $cookieValue = json_encode($cookieValue);

            // Создаем время жизни куки до конца текущего дня + 1 минута.
            $now = Carbon::now();
            $cookieLife = Carbon::now()
                ->endOfDay()
                ->addMinute()
                ->addHours(3) // Fix timezone
                ->diffInMinutes($now);

            // Позволяем использовать акцию на следующий день.
            $cookie = cookie(BuyTodayDiscount::COOKIE_NAME, $cookieValue, $cookieLife, null, null, false, false);
            $this->cookieFactory->queue($cookie);

            return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function canUse(CalculationRequest $request = null, Authenticatable $user = null, array $params = []): bool
    {
        if (OrderPaymentMethod::CREDIT === $request->paymentMethod()) {
            return false;
        }

        if ($this->request->hasCookie(static::COOKIE_NAME)) {
            $now = Carbon::now()->timestamp;
            $cookieValue = json_decode($this->request->cookie(static::COOKIE_NAME));

            return $now < $cookieValue['expired'] && ! $cookieValue['used'];
        }

        return false;
    }
}
