<?php

namespace App\Domain\Entities\Accounting\Promotions;

use App\Domain\Entities\Accounting\Order\Calculation;
use App\Domain\Enums\Accounting\OrderPaymentMethod;
use App\ServiceLayer\PriceCalculator\CalculationRequest;
use App\ServiceLayer\Promotions\Promotion;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;

class UserBonuses implements Promotion
{
    /**
     * Максимальное количество бонусов, которые можно использовать за один заказ.
     */
    const MAX_BONUSES_PER_ORDER = 5000;
    const MIN_BONUSES_PER_ORDER = 15000;

    /**
     * @var Guard
     */
    private $guard;

    /**
     * {@inheritdoc}
     */
    public function __construct(Guard $guard)
    {
        $this->guard = $guard;
    }

    /**
     * {@inheritdoc}
     */
    public static function name(): string
    {
        return 'user_bonuses';
    }

    /**
     * {@inheritdoc}
     */
    public function apply(Calculation $calculation = null, Authenticatable $user = null, array $params = []): bool
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function canUse(CalculationRequest $request = null, Authenticatable $user = null, array $params = []): bool
    {
        if (OrderPaymentMethod::CREDIT === $request->paymentMethod()) {
            return false;
        }

        return $user &&
            data_get($user, 'extended.bonuses.activated', 0) >= ((int) $request->promotion(static::name()));
    }
}
