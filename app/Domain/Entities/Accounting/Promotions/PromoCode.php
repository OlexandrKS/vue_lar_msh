<?php

namespace App\Domain\Entities\Accounting\Promotions;

use App\Domain\Entities\Accounting\Order\Calculation;
use App\Domain\Enums\Accounting\OrderPaymentMethod;
use App\ServiceLayer\PriceCalculator\CalculationRequest;
use App\ServiceLayer\Promotions\Promotion;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable;
use LuckyWeb\User\Models\PromoCodeEvent;

class PromoCode implements Promotion
{
    /**
     * {@inheritdoc}
     */
    public static function name(): string
    {
        return 'promo_code';
    }

    /**
     * {@inheritdoc}
     */
    public function apply(Calculation $calculation = null, Authenticatable $user = null, array $params = []): bool
    {
        /** @var PromoCodeEvent $model */
        $model = array_get($params, 'model');
        $model->applied_at = Carbon::now()->startOfDay();

        return $model->save();
    }

    /**
     * {@inheritdoc}
     */
    public function canUse(CalculationRequest $request = null, Authenticatable $user = null, array $params = []): bool
    {
        if (OrderPaymentMethod::CREDIT === $request->paymentMethod()) {
            return false;
        }

        /** @var PromoCodeEvent $model */
        $model = array_get($params, 'model');
        $value = array_get($params, 'value', 0);

        if (is_null($model)) {
            return false;
        }

        return $value >= $model->promo_code->available_from;
    }
}
