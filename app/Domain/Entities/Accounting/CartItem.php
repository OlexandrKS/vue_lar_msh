<?php

namespace App\Domain\Entities\Accounting;

use App\Domain\Entities\Catalog\Offer;
use App\Domain\Enums\Catalog\OfferType;
use App\ServiceLayer\Support\ModelUuidKey;
use App\ServiceLayer\ValueObject\CastObjects;
use LuckyWeb\User\Models\User;
use October\Rain\Database\Model;

/**
 * CartItem Model.
 *
 * @property string $id
 * @property int    $user_id
 * @property Offer  $offer
 * @property-read string $offer_id
 * @property-read string $offer_type
 * @property int   $quantity
 * @property array $promotions
 * @property $user User
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @mixin \Eloquent
 */
class CartItem extends Model
{
    use ModelUuidKey;
    use CastObjects;

    /**
     * @var string the database table used by the model
     */
    public $table = 'cart_items';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['id'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array
     */
    protected $casts = [
        'promotions' => 'array',
    ];

    /**
     * @var array
     */
    protected $jsonable = [
        //'promotions',
    ];

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'user'        => User::class,
        'stored_offer'=> [
            Offer::class,
            'key' => 'offer_id',
        ],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * Предложения, данные о которых хранятся в базе и их нужно считывать при подгрузке.
     *
     * @var array
     */
    protected $storedOfferTypes = [
        OfferType::SINGLE,
        OfferType::PACKAGE,
        OfferType::TOGETHER_CHEAP,
    ];

    /**
     * {@inheritdoc}
     */
    protected function beforeSave()
    {
        // Большой грязный хак для виртуальных полей
        // Снести при переезде в postgre
        if ((! array_key_exists('offer', $this->attributes) || '[]' === $this->attributes['offer']) &&
            array_key_exists('offer_tid', $this->attributes) && array_key_exists('offer_type', $this->attributes)
        ) {
            $this->attributes['offer'] = json_encode([
                'id'   => $this->attributes['offer_tid'],
                'type' => $this->attributes['offer_type'],
            ]);

            $this->offsetUnset('offer_tid');
            unset($this->objectCasts['offer']);
            unset($this->relations['stored_offer']);
        }

        // json virtual fields
        $this->offsetUnset('offer_id');
        $this->offsetUnset('offer_type');
    }

    /**
     * Затычка для грязного хака с виртуальными полями.
     *
     * @return string
     */
    public function getOfferIdAttribute(): string
    {
        if ($id = (array_get($this->attributes, 'offer_tid') ?? array_get($this->attributes, 'offer.id'))) {
            return $id;
        }

        return array_get($this->fromJson($this->attributes['offer']), 'id');
    }

    /**
     * Затычка для грязного хака с виртуальными полями.
     *
     * @return string
     */
    public function getOfferTypeAttribute(): string
    {
        if ($type = (array_get($this->attributes, 'offer_type') ?? array_get($this->attributes, 'offer.type'))) {
            return $type;
        }

        return array_get($this->fromJson($this->attributes['offer']), 'type');
    }

    /**
     * @return Offer
     */
    public function getOfferAttribute()
    {
        if (array_key_exists('offer', $this->objectCasts)) {
            return $this->objectCasts['offer'];
        }

        if (in_array($this->offer_type, $this->storedOfferTypes)) {
            $this->loadMissing('stored_offer', 'stored_offer.primary', 'stored_offer.products');

            return $this->objectCasts['offer'] = $this->getRelation('stored_offer');
        }

        return $this->objectCasts['offer'] = hydrate(Offer::class, $this->fromJson($this->attributes['offer']));
    }

    /**
     * @param Offer $offer
     */
    public function setOfferAttribute(Offer $offer)
    {
        $this->objectCasts['offer'] = $offer;

        if (in_array($offer->type, $this->storedOfferTypes)) {
            $this->attributes['offer'] = json_encode([
                $offer->getKeyName() => $offer->getKey(),
                'type'               => $offer->type,
            ]);

            return;
        }

        $this->attributes['offer'] = $offer->toJson();
    }
}
