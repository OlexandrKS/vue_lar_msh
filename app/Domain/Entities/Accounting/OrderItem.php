<?php

namespace App\Domain\Entities\Accounting;

use App\Domain\Entities\Catalog\Offer;
use App\ServiceLayer\Support\ModelUuidKey;
use App\ServiceLayer\ValueObject\CastObjects;
use October\Rain\Database\Model;

/**
 * @property string $id
 * @property Order  $order
 * @property Offer  $offer
 * @property int    $quantity
 * @property array  $promotions
 *
 * @mixin \Eloquent
 */
class OrderItem extends Model
{
    use ModelUuidKey;
    use CastObjects;

    /**
     * @var string the database table used by the model
     */
    public $table = 'order_items';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['id'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array
     */
    protected $casts = [
        'promotions' => 'array',
    ];

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'order' => Order::class,
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @return Offer
     */
    public function getOfferAttribute()
    {
        if (array_key_exists('offer', $this->objectCasts)) {
            return $this->objectCasts['offer'];
        }

        return $this->objectCasts['offer'] = hydrate(Offer::class, $this->fromJson($this->attributes['offer']))->first();
    }

    /**
     * @param Offer $offer
     */
    public function setOfferAttribute(Offer $offer)
    {
        $offer->loadMissing('primary', 'products');

        $this->objectCasts['offer'] = $offer;
        $this->attributes['offer'] = $offer->toJson();
    }
}
