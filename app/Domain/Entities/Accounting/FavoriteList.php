<?php

namespace App\Domain\Entities\Accounting;

use Illuminate\Support\Facades\Auth;
use October\Rain\Database\Collection;

class FavoriteList
{
    /**
     * @return Collection
     */
    public function getItems(): Collection
    {
        return FavoriteListItem::query()
            ->currentUser()
            ->orderBy('created_at')
            ->with('offer.products')
            ->get();
    }

    public function getItemsByUserId($userId): Collection
    {
        return FavoriteListItem::query()
            ->where('user_id', $userId)
            ->orderBy('created_at')
            ->with('offer.products')
            ->get();
    }

    /**
     * @param string $offerId
     *
     * @return FavoriteListItem
     */
    public function addItem(string $offerId): FavoriteListItem
    {
        $item = new FavoriteListItem();
        $item->user_id = Auth::id();
        $item->offer_id = $offerId;
        $item->save();

        return $item;
    }

    /**
     * @param FavoriteListItem $favoriteListItem
     *
     * @throws \Exception
     *
     * @return bool
     */
    public function deleteItem(FavoriteListItem $favoriteListItem): bool
    {
        $favoriteListItem->delete();

        return true;
    }

    public function getPopularOffers(int $maxCount = 10)
    {
        return FavoriteListItem::query()
            ->popularOffers()
            ->with('offer.primary')
            ->take($maxCount)
            ->get();
    }
}
