<?php

namespace App\Domain\Entities\Accounting;

use App\Domain\Entities\Accounting\Order\Calculation;
use App\Domain\Entities\Accounting\Order\Properties;
use App\Domain\Entities\Catalog\Offer;
use App\ServiceLayer\Support\ModelUuidKey;
use App\ServiceLayer\ValueObject\CastObjects;
use LuckyWeb\User\Models\User;
use October\Rain\Database\Collection;
use October\Rain\Database\Model;

/**
 * Order Model.
 *
 * @property string      $id
 * @property User        $user
 * @property int         $status
 * @property string      $payment_method
 * @property Properties  $properties
 * @property Calculation $calculated
 * @property-read Collection|OrderItem[] $items
 * @property-read \Carbon\Carbon $created_at
 * @property-read \Carbon\Carbon $updated_at
 *
 * @mixin \Eloquent
 */
class Order extends Model
{
    use ModelUuidKey;
    use CastObjects;

    /**
     * @var string the database table used by the model
     */
    public $table = 'orders';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['id'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array
     */
    protected $casts = [
        'properties' => Properties::class,
        'calculated' => Calculation::class,
    ];

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'items' => [
            OrderItem::class,
            'key' => 'order_id',
        ],
    ];
    public $belongsTo = [
        'user' => User::class,
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @return Offer
     */
    public function getOfferAttribute(): Offer
    {
        if (array_key_exists('offer', $this->objectCasts)) {
            return $this->objectCasts['offer'];
        }

        return $this->objectCasts['offer'] = hydrate(Offer::class, $this->fromJson($this->attributes['offer']))
            ->first();
    }

    /**
     * @param Offer $offer
     */
    public function setOfferAttribute(Offer $offer)
    {
        $this->objectCasts['offer'] = $offer;
        $this->attributes['offer'] = $offer->toJson();
    }
}
