<?php

namespace App\Domain\Entities\Accounting;

use App\Domain\Entities\Catalog\Offer;
use App\ServiceLayer\Support\ModelUuidKey;
use Illuminate\Database\Query\Builder;
use LuckyWeb\User\Models\User;
use October\Rain\Database\Model;

/**
 * FavoriteList Model.
 *
 * @property string $id
 * @property int    $user_id
 * @property string $offer_id
 * @property $user User
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @mixin \Eloquent
 */
class FavoriteListItem extends Model
{
    use ModelUuidKey;

    /**
     * @var string the database table used by the model
     */
    public $table = 'favorite_list_items';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['id'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array
     */
    protected $casts = [];

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'user'      => User::class,
        'offer'     => Offer::class,
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeCurrentUser($query)
    {
        return $query->where('user_id', auth()->id());
    }

    /**
     * @param Builder $query
     *
     * @return mixed
     */
    public function scopePopularOffers($query)
    {
        return $query->groupBy('offer_id')
            ->selectRaw('offer_id, count(`offer_id`) as count')
            ->groupBy('offer_id')
            ->orderBy('count', 'DESC');
    }
}
