<?php

namespace App\Domain\Entities\Accounting;

use App\Domain\Entities\Catalog\Offer;
use App\Domain\Events\Accounting\CartAddedOffer;
use App\Domain\Events\Accounting\CartChangedQuantity;
use App\Domain\Events\Accounting\CartDeletedOffer;
use App\Domain\Events\Accounting\CartView;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Events\Dispatcher;
use LuckyWeb\User\Models\User;
use October\Rain\Database\Collection;

class Cart
{
    /**
     * @var Authenticatable|User|null
     */
    private $user;

    /**
     * @var Dispatcher
     */
    private $events;

    /**
     * Cart constructor.
     *
     * @param Dispatcher      $events
     * @param Authenticatable $user
     */
    public function __construct(Dispatcher $events, Authenticatable $user = null)
    {
        $this->user = $user;
        $this->events = $events;
    }

    /**
     * @return Authenticatable|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return int|null
     */
    private function getUserId()
    {
        return data_get($this->user, 'id');
    }

    /**
     * @return Collection
     */
    public function getItems(): Collection
    {
        if (! $this->user) {
            return new Collection();
        }

        $collection = $this->getItemsWithoutEvent();
        $this->events->dispatch(new CartView($collection, $this->user));

        return $collection;
    }

    /**
     * @return Collection
     */
    public function getItemsWithoutEvent(): Collection
    {
        return CartItem::query()
            ->where('user_id', $this->user->getAuthIdentifier())
            ->orderBy('created_at')
            ->with('stored_offer',
                'stored_offer.primary',
                'stored_offer.products',
                'stored_offer.primary.good_type_site')
            ->get();
    }

    /**
     * @param Offer $offer
     * @param int   $quantity
     *
     * @return CartItem
     */
    public function addItem(Offer $offer, int $quantity = 0): CartItem
    {
        /** @var CartItem $item */
        $item = CartItem::firstOrNew([
            'offer_id' => $offer->getKey(),
            'user_id'  => $this->getUserId(),
        ], [
            'quantity'   => $quantity,
            'promotions' => [],
        ]);

        $offer->loadMissing('products', 'primary', 'primary.good_type_site');

        $item->offer = $offer;
        $item->quantity = $item->quantity + 1;

        $this->events->dispatch(new CartAddedOffer($item, $this->user));

        if ($this->user) {
            $item->save();
        }

        return $item;
    }

    /**
     * @param CartItem $cartItem
     * @param int      $quantity
     *
     * @return CartItem
     */
    public function changeQuantity(CartItem $cartItem, $quantity = 1): CartItem
    {
        $offer = $cartItem->offer;
        $offer->loadMissing('products', 'primary', 'primary.good_type_site');

        if (! $offer->properties->canChangeCount) {
            throw new \RuntimeException("You can't change quantity for cart item #{$cartItem->getKey()}");
        }

        $cartItem->offer = $offer;
        $cartItem->quantity = (int) $quantity;

        $this->events->dispatch(new CartChangedQuantity($cartItem, $this->user));

        if ($this->user) {
            $cartItem->save();
        }

        return $cartItem;
    }

    /**
     * @param CartItem $cartItem
     *
     * @throws \Exception
     *
     * @return bool
     */
    public function deleteItem(CartItem $cartItem): bool
    {
        $cartItem->delete();

        $this->events->dispatch(new CartDeletedOffer($cartItem, $this->user));

        return true;
    }

    /**
     * @param Collection $collection
     *
     * @return Collection
     */
    public function syncItems(Collection $collection): Collection
    {
        /** @var Collection|CartItem[] $result */
        $result = $this->getItemsWithoutEvent();

        /** @var CartItem $cartItem */
        foreach ($collection as $cartItem) {
            /** @var CartItem|null $dbItem */
            $dbItem = $result->first(function (CartItem $dbItem) use ($cartItem) {
                return $dbItem->offer_id === $cartItem->offer_id;
            });

            if (! $dbItem) {
                $cartItem->user = $this->user;
                $cartItem->exists = false;
                $result->push($cartItem);

                continue;
            }

            $quantity = max($dbItem->quantity, $cartItem->quantity);
            $dbItem->quantity = $quantity;
        }

        foreach ($result as $item) {
            $item->save();
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function erase(): bool
    {
        if ($this->user) {
            return CartItem::query()
                ->where('user_id', $this->user->getKey())
                ->delete();
        }

        return true;
    }
}
