<?php

namespace App\Domain\Entities\Accounting;

use Illuminate\Support\Collection;
use LuckyWeb\MS\Models\ViewedProductsUser;

/**
 * @todo refactor
 */
class ViewedList
{
    /**
     * @throws \Exception
     *
     * @return Collection
     */
    public function getItems(): Collection
    {
        if (\auth()->guest()) {
            return new Collection();
        }

        $userId = \auth()->id();

        $products = ViewedProductsUser::query()
            ->where('user_id', $userId)
            ->orderBy('created_at', 'desc')
            ->with(['product', 'product.primary_offer', 'product.nid', 'product.category', 'product.good_type_site', 'product.images'])
            ->limit(20)
            ->get()
            ->pluck('product');

        return $products;
    }
}
