<?php

namespace App\Domain\Entities\Accounting\Order;

use App\ServiceLayer\ValueObject\ValueObject;

class PaymentData extends ValueObject
{
    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        return [];
    }
}
