<?php

namespace App\Domain\Entities\Accounting\Order;

/**
 * @property int    $firstInstallment
 * @property int    $loanTerms
 * @property string $fullName
 * @property string $surname
 * @property string $phone
 * @property string $passport
 */
class CreditData extends PaymentData
{
    /**
     * @var int
     */
    protected $firstInstallment = 0;

    /**
     * @var int
     */
    protected $loanTerms = 24;

    /**
     * @var string
     */
    protected $fullName;

    /**
     * @var string
     */
    protected $surname;

    /**
     * @var string
     */
    protected $phone;

    /**
     * @var string
     */
    protected $passport;

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        return [
            'firstInstallment' => $this->firstInstallment,
            'loanTerms'        => $this->loanTerms,
            'fullName'         => $this->fullName,
            'surname'          => $this->surname,
            'phone'            => $this->phone,
            'passport'         => $this->passport,
        ];
    }
}
