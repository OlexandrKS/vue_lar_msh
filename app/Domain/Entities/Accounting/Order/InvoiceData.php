<?php

namespace App\Domain\Entities\Accounting\Order;

/**
 * @property string $bankName
 * @property string $bik
 * @property string $inn
 * @property string $city
 * @property string $street
 * @property string $house
 * @property string $apartment
 * @property string $authPerson
 * @property string $deliveryPerson
 */
class InvoiceData extends PaymentData
{
    /**
     * @var string
     */
    protected $bankName = null;

    /**
     * @var string
     */
    protected $bik = null;

    /**
     * @var string
     */
    protected $inn = null;

    /**
     * @var string
     */
    protected $city = null;

    /**
     * @var string
     */
    protected $street = null;

    /**
     * @var string
     */
    protected $house = null;

    /**
     * @var string
     */
    protected $apartment = null;

    /**
     * @var string
     */
    protected $authPerson = null;

    /**
     * @var string
     */
    protected $deliveryPerson = null;

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        return [
            'bankName'       => $this->bankName,
            'inn'            => $this->inn,
            'bik'            => $this->bik,
            'city'           => $this->city,
            'street'         => $this->street,
            'house'          => $this->house,
            'apartment'      => $this->apartment,
            'authPerson'     => $this->authPerson,
            'deliveryPerson' => $this->deliveryPerson,
        ];
    }
}
