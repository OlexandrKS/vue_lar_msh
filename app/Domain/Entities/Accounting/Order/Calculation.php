<?php

namespace App\Domain\Entities\Accounting\Order;

use App\ServiceLayer\ValueObject\ValueObject;

/**
 * @property-read float $amount
 * @property-read float $total
 * @property int|float $assembling
 * @property int|float $delivery
 * @property int|float $withoutPromotions
 * @property int|float $promoDiscounts
 * @property array     $promotions
 */
class Calculation extends ValueObject
{
    /**
     * @var float
     */
    protected $total = 0;

    /**
     * @var float
     */
    protected $withoutPromotions = 0;

    /**
     * @var
     */
    protected $promoDiscounts = 0;

    /**
     * @var float
     */
    protected $assembling = 0;

    /**
     * @var float
     */
    protected $delivery = 0;

    /**
     * @var float
     */
    protected $amount = 0;

    /**
     * @var array
     */
    protected $promotions = [];

    /**
     * Пересчитать общую сумму.
     */
    public function getAmount()
    {
        return $this->amount = $this->total + $this->assembling + $this->delivery - $this->promoDiscounts;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        return [
            'amount'             => $this->getAmount(),
            'total'              => $this->total,
            'withoutPromotions'  => $this->withoutPromotions,
            'promoDiscounts'     => $this->promoDiscounts,
            'assembling'         => $this->assembling,
            'delivery'           => $this->delivery,
        ];
    }
}
