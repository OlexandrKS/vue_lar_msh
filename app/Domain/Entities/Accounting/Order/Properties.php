<?php

namespace App\Domain\Entities\Accounting\Order;

use App\ServiceLayer\ValueObject\ValueObject;

/**
 * @property array                           $promotions
 * @property CreditData|CardData|InvoiceData $paymentData
 * @property DeliveryData                    $deliveryData
 * @property string                          $comment
 */
class Properties extends ValueObject
{
    /**
     * @var array
     */
    protected $promotions = [];

    /**
     * @var PaymentData|CreditData
     */
    protected $paymentData;

    /**
     * @var DeliveryData
     */
    protected $deliveryData;

    /**
     * @var string
     */
    protected $comment = null;

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        return [
            'promotions'  => $this->promotions,
            'paymentData' => $this->paymentData,
            'comment'     => $this->comment,
            'delivery'    => $this->deliveryData,
        ];
    }
}
