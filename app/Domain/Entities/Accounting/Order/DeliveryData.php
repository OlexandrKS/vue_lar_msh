<?php

namespace App\Domain\Entities\Accounting\Order;

use App\ServiceLayer\ValueObject\ValueObject;

/**
 * @property int    $type
 * @property string $address
 */
class DeliveryData extends ValueObject
{
    /**
     * @var int
     */
    protected $type;

    /**
     * @var string
     */
    protected $address;

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        return [
            'type'    => $this->type,
            'address' => $this->address,
        ];
    }
}
