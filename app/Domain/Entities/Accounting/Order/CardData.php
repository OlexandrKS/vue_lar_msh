<?php

namespace App\Domain\Entities\Accounting\Order;

/**
 * @property int $prePayment
 */
class CardData extends PaymentData
{
    /**
     * @var int
     */
    protected $prePayment = 0;

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        return [
            'prePayment' => $this->prePayment,
        ];
    }
}
