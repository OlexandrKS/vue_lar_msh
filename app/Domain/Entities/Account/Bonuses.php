<?php

namespace App\Domain\Entities\Account;

use App\ServiceLayer\ValueObject\ValueObject;

/**
 * @property int $accrued
 * @property int $activated
 * @property int $charged
 * @property int $deactivated
 */
class Bonuses extends ValueObject
{
    /**
     * @var int
     */
    protected $accrued;

    /**
     * @var int
     */
    protected $activated;

    /**
     * @var int
     */
    protected $charged;

    /**
     * @var int
     */
    protected $deactivated;

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        return [
            'accrued'     => $this->accrued,
            'activated'   => $this->activated,
            'charged'     => $this->charged,
            'deactivated' => $this->deactivated,
        ];
    }
}
