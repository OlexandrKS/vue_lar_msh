<?php

namespace App\Domain\Process;

use App\Contracts\PriceCalculator;
use App\Contracts\PromotionManager;
use App\Domain\Entities\Accounting\Cart;
use App\Domain\Entities\Accounting\CartItem;
use App\Domain\Entities\Accounting\Order;
use App\Domain\Entities\Accounting\Order\Calculation;
use App\Domain\Entities\Accounting\Order\Properties;
use App\Domain\Entities\Accounting\OrderItem;
use App\Domain\Enums\Accounting\OrderDeliveryType;
use App\Domain\Enums\Accounting\OrderPaymentMethod;
use App\Domain\Enums\Accounting\OrderStatus;
use App\Domain\Events\Accounting\OrderCreated;
use App\Domain\Process\User\OrderCreation;
use App\ServiceLayer\PriceCalculator\CalculationRequest;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Contracts\Events\Dispatcher;
use LuckyWeb\User\Models\User;
use October\Rain\Database\Collection;

class Checkout
{
    /**
     * @var Dispatcher
     */
    private $events;

    /**
     * @var PriceCalculator
     */
    private $priceCalculator;

    /**
     * @var PromotionManager
     */
    private $promotionManager;

    /**
     * @var Guard|StatefulGuard
     */
    private $guard;

    /**
     * @var OrderCreation
     */
    private $userCreator;

    /**
     * Checkout constructor.
     *
     * @param Dispatcher          $events
     * @param PriceCalculator     $priceCalculator
     * @param PromotionManager    $promotionManager
     * @param Guard|StatefulGuard $guard
     * @param OrderCreation       $userCreator
     */
    public function __construct(Dispatcher $events, PriceCalculator $priceCalculator, PromotionManager $promotionManager,
                                Guard $guard, OrderCreation $userCreator
    ) {
        $this->events = $events;
        $this->priceCalculator = $priceCalculator;
        $this->promotionManager = $promotionManager;
        $this->guard = $guard;
        $this->userCreator = $userCreator;
    }

    /**
     * @param Collection           $items
     * @param array                $promotions
     * @param Authenticatable|null $user
     * @param string               $paymentMethod
     * @param bool                 $hasAssembling
     * @param int                  $deliveryType
     *
     * @return Calculation
     */
    public function calculate(Collection $items, array $promotions = [], Authenticatable $user = null,
                              string $paymentMethod = OrderPaymentMethod::CARD_PAY, bool $hasAssembling = true,
                              int $deliveryType = OrderDeliveryType::ADDRESS): Calculation
    {
        $request = new CalculationRequest($items, $promotions, $user, $paymentMethod, $hasAssembling, $deliveryType);

        return $this->priceCalculator->calculate($request);
    }

    /**
     * @param Collection $items
     * @param array      $accountData
     * @param array      $paymentData
     * @param array      $deliveryData
     * @param array      $promotions
     * @param null       $comment
     *
     * @throws \Throwable
     *
     * @return Order
     */
    public function fromRequestData(Collection $items, array $accountData, array $paymentData, array $deliveryData,
                                    array $promotions = [], $comment = null): Order
    {
        $method = $paymentData['method'];
        $hasAssembling = (bool) array_get($paymentData, 'assembling', true);
        $deliveryType = array_get($deliveryData, 'type', OrderDeliveryType::ADDRESS);

        /** @var User $user */
        $user = $this->processAccountData($accountData);
        $calculated = $this->calculate($items, $promotions, $user, $method, $hasAssembling, $deliveryType);

        foreach (array_keys($calculated->promotions) as $promotion) {
            $this->promotionManager->apply($promotion, $calculated, $user);
        }

        $paymentData = array_except($paymentData, ['method', 'hasAssembling']);

        switch ($method) {
            case 'credit':
                $paymentObject = new Order\CreditData($paymentData);
                break;
            case 'card':
                $paymentObject = new Order\CardData($paymentData);
                break;
            case 'invoice':
                $paymentObject = new Order\InvoiceData($paymentData);
                break;

            default:
                $paymentObject = new Order\PaymentData();
        }

        $order = new Order();
        $order->user = $user;
        $order->status = OrderStatus::REQUEST;
        $order->payment_method = $method;
        $order->calculated = $calculated;
        $order->properties = new Properties([
            'promotions'   => $calculated->promotions,
            'paymentData'  => $paymentObject,
            'deliveryData' => new Order\DeliveryData($deliveryData),
            'comment'      => $comment,
        ]);

        $order->saveOrFail();

        /** @var CartItem $cartItem */
        foreach ($items as $cartItem) {
            $orderItem = new OrderItem();
            $orderItem->order = $order;
            $orderItem->offer = $cartItem->offer;
            $orderItem->quantity = $cartItem->quantity;
            $orderItem->promotions = $cartItem->promotions;

            $orderItem->saveOrFail();
            $order->items->push($orderItem);
        }

        $this->events->dispatch(new OrderCreated($order));

        $cart = new Cart($this->events, $user);
        $cart->erase();

        return $order;
    }

    /**
     * @param array $accountData
     *
     * @return Authenticatable
     */
    private function processAccountData(array $accountData): Authenticatable
    {
        if ($this->guard->check()) {
            return $this->guard->user();
        }

        $phone = array_get($accountData, 'phone');
        $email = array_get($accountData, 'email');

        /** @var User $user */
        $user = User::query()
            ->where('phone', $phone)
            ->orWhere('email', $email)
            ->first();

        if (is_null($user)) {
            $user = $this->userCreator->handle(
                array_get($accountData, 'name'),
                $phone,
                $email
            );
        }

        $this->guard->login($user);

        return $user;
    }
}
