<?php

namespace App\Domain\Process\User;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\StatefulGuard;
use LuckyWeb\User\Models\User;

class ChangePassword
{
    /**
     * @var Guard|StatefulGuard
     */
    protected $guard;

    /**
     * ChangePassword constructor.
     *
     * @param Guard/StatefulGuard $guard
     */
    public function __construct(Guard $guard)
    {
        $this->guard = $guard;
    }

    /**
     * @param array $credentials
     *
     * @return User
     */
    public function handle(array $credentials)
    {
        /** @var User $user */
        $user = User::where('id', $this->guard->id())
            ->firstOrFail();
        $user->password = $credentials['password'];
        $user->password_confirmation = $credentials['password'];
        $user->save();

        $this->guard->login($user, true);

        return $user;
    }
}
