<?php

namespace App\Domain\Process\User;

use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Contracts\Auth\Authenticatable;
use LuckyWeb\User\Models\User;

class PasswordRecovery extends Registration
{
    /**
     * @param array  $credentials
     * @param string $sign
     *
     * @return Authenticatable
     */
    public function handle(array $credentials, string $sign): Authenticatable
    {
        /** @var array $signArray */
        $signArray = $this->cryptService->decrypt($sign);
        $phoneNumber = $signArray['phoneNumber'];

        /** @var User $user */
        $user = User::where('phone', $phoneNumber)
            ->firstOrFail();
        $user->password = $credentials['password'];
        $user->password_confirmation = $credentials['password'];
        $user->save();

        $this->events->dispatch(new PasswordReset($user));

        $this->guard->login($user, true);

        return $user;
    }
}
