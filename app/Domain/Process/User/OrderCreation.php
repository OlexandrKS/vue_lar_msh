<?php

namespace App\Domain\Process\User;

use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Events\Dispatcher;
use LuckyWeb\User\Models\User;

class OrderCreation
{
    /**
     * @var Dispatcher
     */
    private $events;

    /**
     * OrderCreation constructor.
     *
     * @param Dispatcher $events
     */
    public function __construct(Dispatcher $events)
    {
        $this->events = $events;
    }

    /**
     * @param string      $name
     * @param string      $phone
     * @param string|null $email
     *
     * @return Authenticatable
     */
    public function handle(string $name, string $phone, string $email = null): Authenticatable
    {
        list($name, $lastName) = explode_full_name($name);

        $user = new User();
        $user->email = $email;
        $user->phone = $phone;
        $user->name = $name;
        $user->last_name = $lastName;
        $user->save();
        $user->generateCard();
        $user->activation_code = $code = 'activate';
        $user->attemptActivation($code);

        $this->events->dispatch(new Registered($user));

        return $user;
    }
}
