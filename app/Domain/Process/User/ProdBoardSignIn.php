<?php

namespace App\Domain\Process\User;

use App\ServiceLayer\Sentry\Sentry;
use GuzzleHttp\Client;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\URL;
use LuckyWeb\User\Models\User;

class ProdBoardSignIn
{
    /**
     * @var Guard|StatefulGuard
     */
    protected $guard;

    /**
     * ProdBoardSignIn constructor.
     *
     * @param Guard $guard
     */
    public function __construct(Guard $guard)
    {
        $this->guard = $guard;
    }

    /**
     * @return bool|string
     */
    public function handle()
    {
        $user = User::find($this->guard->user()->getKey());
        if ($user) {
            // identity mebelShara9674
            $identity = 'mebelShara'.$user->id;
            $responseToken = $this->clientToken($identity);

            if (null == $responseToken) {
                $client = new Client();

                $response = $client->post('https://api.prodboard.com/api/clients.update', [
                    'headers' => [
                        'api_key' => config('prodboard.api_key'),
                    ],
                    'form_params' => [
                        'identity' => $identity,
                        'email'    => $user->email,
                        'phone'    => $user->phone,
                        'name'     => $user->name,
                    ],
                ]);

                $response = json_decode($response->getBody(), true);

                if ($response['success']) {
                    // Лид ProdBoard
                    $data = [
                        'id'    => $user->id,
                        'name'  => $user->name,
                        'email' => $user->email,
                        'phone' => $user->phone,
                    ];

                    Event::fire('luckyweb.prodboard.register', [$data]);

                    $responseToken = $this->clientToken($identity);
                } else {
                    Sentry::error($this->getResponseException($response));
                }
            }

            return URL::to('/').'/planner#token-'.$responseToken;
        }

        return false;
    }

    protected function clientToken($identity)
    {
        $client = new Client();
        try {
            $response = $client->post('https://api.prodboard.com/api/clients.token', [
                'headers' => [
                    'api_key' => config('prodboard.api_key'),
                ],
                'form_params' => ['identity' => $identity],
            ]);

            return json_decode($response->getBody(), true);
        } catch (\Exception $error) {
            Sentry::error($error);
        }

        return response(null, 500);
    }
}
