<?php

namespace App\Domain\Process\User;

use App\Contracts\SmsSender;
use App\ServiceLayer\Sentry\Sentry;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Contracts\Encryption\Encrypter;
use Illuminate\Contracts\Events\Dispatcher;
use LuckyWeb\User\Models\User;

class Registration
{
    /**
     * @var Encrypter
     */
    protected $cryptService;

    /**
     * @var SmsSender
     */
    protected $smsSender;

    /**
     * @var Guard|StatefulGuard
     */
    protected $guard;

    /**
     * @var Dispatcher
     */
    protected $events;

    /**
     * Registration constructor.
     *
     * @param Encrypter           $cryptService
     * @param SmsSender           $smsSender
     * @param Guard|StatefulGuard $guard
     * @param Dispatcher          $events
     */
    public function __construct(Encrypter $cryptService, SmsSender $smsSender, Guard $guard, Dispatcher $events)
    {
        $this->cryptService = $cryptService;
        $this->smsSender = $smsSender;
        $this->guard = $guard;
        $this->events = $events;
    }

    /**
     * @param string $phoneNumber
     *
     * @throws \App\ServiceLayer\SmsSender\ThrottleException
     *
     * @return string
     */
    public function attempt(string $phoneNumber): string
    {
        $code = $this->codeGenerate();
        $sign = $this->cryptService->encrypt(compact('phoneNumber', 'code'));

        $this->smsSender->handle($phoneNumber, $this->textMessage($code));

        return $sign;
    }

    /**
     * @param string $code
     * @param string $sign
     *
     * @throws \Exception
     *
     * @return bool
     */
    public function validate(string $code, string $sign): bool
    {
        try {
            /** @var array $fields */
            $fields = $this->cryptService->decrypt($sign);

            return $fields['code'] === $code;
        } catch (DecryptException $e) {
            Sentry::captureException($e);

            throw new \Exception('Произошла ошибка, обратитесь к администратору');
        }
    }

    /**
     * @param array  $credentials
     * @param string $sign
     *
     * @return
     */
    public function handle(array $credentials, string $sign): Authenticatable
    {
        /** @var array $signArray */
        $signArray = $this->cryptService->decrypt($sign);
        $activationCode = 1;

        $name = $credentials['name'];
        preg_match('/^([0-9A-Za-zА-Яа-яЁё]+)(.*)(\s)([0-9A-Za-zА-Яа-яЁё]+\s?)$/ui', trim($name), $results);

        $firstName = $results[1];
        $lastName = $results[4];

        $user = new User();
        $user->email = $credentials['email'];
        $user->name = $firstName;
        $user->last_name = $lastName;
        $user->password = $credentials['password'];
        $user->password_confirmation = $credentials['password']; // TODO laravel
        $user->phone = $signArray['phoneNumber'];
        $user->activation_code = $activationCode;
        $user->save();
        $user->generateCard();
        $user->attemptActivation($activationCode);

        $this->events->dispatch(new Registered($user));

        $this->guard->login($user, true);

        return $user;
    }

    /**
     * @return string
     */
    protected function codeGenerate(): string
    {
        try {
            return (string) random_int(1111, 9999);
        } catch (\Exception $e) {
            return 1284; // it's random
        }
    }

    /**
     * @param string $code
     *
     * @return string
     */
    protected function textMessage(string $code): string
    {
        return "CODE: $code";
    }
}
