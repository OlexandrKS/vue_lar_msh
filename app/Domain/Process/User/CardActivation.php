<?php

namespace App\Domain\Process\User;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Encryption\Encrypter;
use Illuminate\Validation\ValidationException;
use LuckyWeb\User\Models\User;

class CardActivation
{
    /**
     * @var Encrypter
     */
    private $cryptService;

    /**
     * @var Guard|StatefulGuard
     */
    private $guard;

    /**
     * @var Repository
     */
    private $config;

    /**
     * CardActivation constructor.
     *
     * @param Encrypter           $cryptService
     * @param Guard|StatefulGuard $guard
     * @param Repository          $config
     */
    public function __construct(Encrypter $cryptService, Guard $guard, Repository $config)
    {
        $this->cryptService = $cryptService;
        $this->guard = $guard;
        $this->config = $config;
    }

    /**
     * @param array  $credentials
     * @param string $sign
     *
     * @return Authenticatable
     */
    public function handle(array $credentials, string $sign): Authenticatable
    {
        $cardNumber = $this->cryptService->decrypt($sign);

        /** @var User $user */
        $user = User::where('card_code', $cardNumber)->firstOrFail();
        $user->password = $credentials['password'];
        $user->password_confirmation = $credentials['password']; // todo laravel
        $user->is_activated = 1;
        $user->activated_at = Carbon::now()->toDateTimeString();
        $user->save();

        $this->guard->login($user);

        return $user;
    }

    /**
     * @param string $cardCode
     * @param string $cardPIN
     *
     * @return string
     */
    public function attempt(string $cardCode, string $cardPIN): string
    {
        // append leading zeros to card number
        $multiplier = $this->config->get('luckyweb.ms::account_card_length') - mb_strlen($cardCode); // TODO laravel
        $cardCode = str_repeat('0', $multiplier > 0 ? $multiplier : 0).$cardCode;

        if ($this->guard->validate([
            'card_pin'  => $cardPIN,
            'card_code' => $cardCode,
        ])) {
            $user = User::where('card_code', $cardCode)
                ->firstOrFail();

            if (0 == $user->is_activated) {
                return $this->cryptService->encrypt($cardCode);
            }

            throw ValidationException::withMessages([
                'card' => ['Эта карта уже активирована!'],
            ]);
        }

        throw ValidationException::withMessages([
            'card' => 'Карта с такими данными не найдена',
        ]);
    }
}
