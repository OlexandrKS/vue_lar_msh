<?php

namespace App\Domain\Process;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\StatefulGuard;
use Jenssegers\Agent\Agent;
use LuckyWeb\MS\Classes\SendPulseManager;
use LuckyWeb\MS\Models\PagePopup;
use LuckyWeb\User\Models\PromoCode;

class Popup
{
    /**
     * @var array
     */
    protected $organic_sources = ['www.google' => ['q='],
        'daum.net/'                            => ['q='],
        'eniro.se/'                            => ['search_word=', 'hitta:'],
        'naver.com/'                           => ['query='],
        'yahoo.com/'                           => ['p='],
        'msn.com/'                             => ['q='],
        'bing.com/'                            => ['q='],
        'aol.com/'                             => ['query=', 'encquery='],
        'lycos.com/'                           => ['query='],
        'ask.com/'                             => ['q='],
        'altavista.com/'                       => ['q='],
        'search.netscape.com/'                 => ['query='],
        'cnn.com/SEARCH/'                      => ['query='],
        'about.com/'                           => ['terms='],
        'mamma.com/'                           => ['query='],
        'alltheweb.com/'                       => ['q='],
        'voila.fr/'                            => ['rdata='],
        'search.virgilio.it/'                  => ['qs='],
        'baidu.com/'                           => ['wd='],
        'alice.com/'                           => ['qs='],
        'yandex.com/'                          => ['text='],
        'najdi.org.mk/'                        => ['q='],
        'aol.com/'                             => ['q='],
        'seznam.cz/'                           => ['q='],
        'search.com/'                          => ['q='],
        'wp.pl/'                               => ['szukai='],
        'online.onetcenter.org/'               => ['qt='],
        'szukacz.pl/'                          => ['q='],
        'yam.com/'                             => ['k='],
        'pchome.com/'                          => ['q='],
        'kvasir.no/'                           => ['q='],
        'sesam.no/'                            => ['q='],
        'ozu.es/'                              => ['q='],
        'terra.com/'                           => ['query='],
        'mynet.com/'                           => ['q='],
        'ekolay.net/'                          => ['q='],
        'rambler.ru/'                          => ['words='],
    ];

    /**
     * @var Guard|StatefulGuard
     */
    private $guard;

    /**
     * @var SendPulseManager
     */
    private $sendPulseManager;

    /**
     * @var Agent
     */
    private $agent;

    /**
     * @var Client
     */
    private $client;

    /**
     * Popup constructor.
     *
     * @param Guard            $guard
     * @param SendPulseManager $sendPulseManager
     * @param Agent            $agent
     * @param Client           $client
     */
    public function __construct(Guard $guard, SendPulseManager $sendPulseManager, Agent $agent, Client $client)
    {
        $this->guard = $guard;
        $this->sendPulseManager = $sendPulseManager;
        $this->agent = $agent;
        $this->client = $client;
    }

    /**
     * @param string $popupIds
     *
     * @return PagePopup|null
     */
    public function handle(string $popupIds)
    {
        parse_str(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY), $utm);
        $url = strtok($_SERVER['HTTP_REFERER'], '?');
        $popupIds = base64_decode($popupIds);
        $popupIds = $popupIds ? json_decode($popupIds) : null;

        $popups = PagePopup::where('active', '=', true)->with(['urls'])->get();
        if ($popups && ! empty($popupIds)) {
            $popups = $popups->whereNotIn('id', $popupIds);
        }
        $inputs = ['utm_source', 'utm_medium', 'utm_campaign', 'utm_content'];
        $current_popup = null;
        foreach ($popups as $popup) {
            if ('all_page' == $popup->page_show) {
                $current_popup = $popup;
                break;
            }
            if ('this_page' == $popup->page_show || 'not_this_page' == $popup->page_show) {
                $is_url = $popup->urls->contains('url', $url);
                if ('this_page' == $popup->page_show && $is_url) {
                    $current_popup = $popup;
                    break;
                } elseif ('not_this_page' == $popup->page_show && ! $is_url) {
                    $current_popup = $popup;
                    break;
                }
            }
        }

        if ($current_popup && 'subscribe' == $current_popup->variant_footer_popup
            && 0 != $current_popup->promo_code && $this->guard->check()) {
            $is_in_books = $this->sendPulseManager->getEmailInfo($this->guard->user()['email']);
            if (! isset($is_in_books->is_error)) {
                $current_popup = null;
            }
        }

        $is_advertising = $is_organic = $is_direct_hit = false;
        if (! empty($current_popup->source_traffic)) {
            foreach ($current_popup->source_traffic as $traffic) {
                switch ($traffic) {
                    case 'advertising':
                        $is_advertising = true;
                        break;
                    case 'organic':
                        $is_organic = true;
                        break;
                    case 'direct_hit':
                        $is_direct_hit = true;
                        break;
                }
            }
        }

        if ($is_advertising && (isset($utm['utm_source'])
                || isset($utm['utm_medium']) || isset($utm['utm_campaign']) || isset($utm['utm_content']))) {
            // if 4 param required
            if ($current_popup) {
                $current_popup = $current_popup->where(function ($query) use ($inputs, $utm) {
                    foreach ($inputs as $value) {
                        $query->where($value, '=', ($utm[$value] ?? ''));
                    }
                })->first();
                if (! $current_popup) {
                    // if 3 param required
                    $current_popup = $current_popup->where(function ($orquery) use ($inputs, $utm) {
                        foreach ($inputs as $value) {
                            if (isset($utm[$value])) {
                                $orquery->orWhere(function ($query) use ($inputs, $value, $utm) {
                                    foreach ($inputs as $input) {
                                        if ($input == $value) {
                                            $query->where($input, '=', '');
                                        } else {
                                            $query->where($input, '=', $utm[$input] ?? '');
                                        }
                                    }

                                    return $query;
                                });
                            }
                        }

                        return $orquery;
                    })->first();
                    if (! $current_popup) {
                        // if 2 param required
                        $current_popup = $current_popup->where(function ($orquery) use ($inputs, $utm) {
                            $index = 0;
                            foreach ($inputs as $value) {
                                ++$index;
                                foreach (array_slice($inputs, $index) as $input) {
                                    $orquery->orWhere(function ($query) use ($inputs, $input, $value, $utm) {
                                        foreach ($inputs as $except) {
                                            if ($except == $value || $except == $input) {
                                                $query->where($except, '=', ($utm[$except] ?? ''));
                                            } else {
                                                $query->where($except, '=', '');
                                            }
                                        }

                                        return $query;
                                    });
                                }
                            }

                            return $orquery;
                        })->first();
                        if (! $current_popup) {
                            // if 1 param required
                            $current_popup = $current_popup->where(function ($orquery) use ($inputs, $utm) {
                                foreach ($inputs as $value) {
                                    if ($utm[$value]) {
                                        $orquery->orWhere(function ($query) use ($inputs, $value, $utm) {
                                            $query->where($value, '=', $utm[$value]);
                                            foreach ($inputs as $input) {
                                                if ($input != $value) {
                                                    $query->where($input, '=', '');
                                                }
                                            }

                                            return $query;
                                        });
                                    }
                                }

                                return $orquery;
                            })->first();
                        }
                    }
                }
            }
        }

        $is_device = false;
        if (! empty($current_popup->devices)) {
            $agent = new Agent();
            foreach ($current_popup->devices as $device) {
                switch ($device) {
                    case 'mobile':
                        if ($agent->isMobile()) {
                            $is_device = true;
                        }
                        break;
                    case 'tablet':
                        if ($agent->isTablet()) {
                            $is_device = true;
                        }
                        break;
                    case 'pc':
                        if ($agent->isDesktop()) {
                            $is_device = true;
                        }
                        break;
                }
            }
        }
        if ((($is_organic && isset($_SERVER['HTTP_REFERER']) && $this->isTrafficOrganic($_SERVER['HTTP_REFERER']))
                || $is_direct_hit) && $is_device) {
            return $current_popup;
        }

        return null;
    }

    /**
     * @param array $condition
     *
     * @return string
     */
    public function emailActivatePromoCode(array $condition)
    {
        $openPopupId = $condition['id'];
        $email = $condition['email'];
        $is_in_books = $this->sendPulseManager->getEmailInfo($email);
        if (isset($is_in_books->is_error) && $is_in_books->is_error && $openPopupId) {
            $popup = PagePopup::find($openPopupId);
            $promo_code = PromoCode::findOrFail($popup->promo_code);

            /** @var PromoCode $promo_code */
            if ($promo_code) {
                $url = url('promo-code', ['code' => $promo_code->code, 'e' => base64_encode($email)]);
                $response = $this->client->post('https://events.sendpulse.com/events/id/806e6f895f674201b599c5e6c512e5f7/7051313', [
                    RequestOptions::JSON => ['email' => $email, 'active_promocode_url' => $url],
                ]);
                $response = json_decode($response->getBody(), true);
                if ($response['result']) {
                    $this->sendPulseManager->addEmailsInBook([['email' => $email]]);

                    return 'На указанный вами электронный адрес '.$email.' был выслан промокод!';
                }
            } else {
                $this->sendPulseManager->addEmailsInBook([['email' => $email]]);

                return 'Спасибо за подписку';
            }
        }

        return 'Вы уже подписаны!!!';
    }

    /**
     * Check if source is organic.
     *
     * @param string $referrer The referrer page
     *
     * @return true if organic, false if not
     */
    private function isTrafficOrganic($referrer)
    {
        foreach ($this->organic_sources as $searchEngine => $queries) {
            if (false !== strpos($referrer, $searchEngine)) {
                foreach ($queries as $query) {
                    if (false !== strpos($referrer, $query)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
