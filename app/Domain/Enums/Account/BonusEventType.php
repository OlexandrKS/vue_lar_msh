<?php

namespace App\Domain\Enums\Account;

final class BonusEventType
{
    const TYPE_ACCRUAL = 1;
    const TYPE_ACTIVATION = 2;
    const TYPE_CHARGE = 3;
    const TYPE_DEACTIVATION = 4;
}
