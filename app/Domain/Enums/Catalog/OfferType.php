<?php

namespace App\Domain\Enums\Catalog;

final class OfferType
{
    /**
     * Единичный товар
     */
    const SINGLE = 'single';

    /**
     * Акция "вместе дешевле".
     */
    const TOGETHER_CHEAP = 'together_cheap';

    /**
     * Набор-комплект
     */
    const PACKAGE = 'package';

    /**
     * Из конструктора кухонь.
     */
    const PRODBOARD = 'prodboard';

    /**
     * @return array
     */
    public static function values()
    {
        return [
            static::SINGLE,
            static::PACKAGE,
            static::TOGETHER_CHEAP,
            static::PRODBOARD,
        ];
    }
}
