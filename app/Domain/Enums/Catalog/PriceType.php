<?php

namespace App\Domain\Enums\Catalog;

final class PriceType
{
    /**
     * Цена продукта без скидки (не используется в расчетах, маркетинговая).
     */
    const TOP = 'top';

    /**
     * Цена продукта с маркетинговой "скидкой".
     */
    const LOWER = 'lower';

    /**
     * Цена продукта по акции "вместе дешевле".
     */
    const SPECIAL = 'special';

    /**
     * Цена продукта по скидке (какой?).
     */
    const SALE = 'sale';

    /**
     * Ежимесячные/ежинедельные промо-скидки.
     */
    const PROMO = 'promo';

    /**
     * Цена на доставку.
     */
    const DELIVERY = 'delivery';

    /**
     * Цена на сборку.
     */
    const ASSEMBLING = 'assembling';
}
