<?php

namespace App\Domain\Enums\Accounting;

final class OrderStatus
{
    /**
     * Заявка.
     */
    const REQUEST = 0;

    /**
     * Подтверждена менеджером
     */
    const APPLIED = 1;

    /**
     * Оплачена.
     */
    const PAID = 2;

    /**
     * В работе: собирается, доставляется.
     */
    const IN_WORK = 3;

    /**
     * Успешно выполнен.
     */
    const COMPLETED = 4;

    /**
     * Отменен.
     */
    const CANCELED = 5;
}
