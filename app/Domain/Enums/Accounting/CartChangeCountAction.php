<?php

namespace App\Domain\Enums\Accounting;

class CartChangeCountAction
{
    const INCREMENTING = 'inc';
    const DECREMENTING = 'dcr';

    /**
     * @return array
     */
    public static function values()
    {
        return [
            self::INCREMENTING,
            self::DECREMENTING,
        ];
    }
}
