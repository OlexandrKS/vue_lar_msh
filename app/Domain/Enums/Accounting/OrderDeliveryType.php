<?php

namespace App\Domain\Enums\Accounting;

final class OrderDeliveryType
{
    /** @var int Доставка по адресу */
    const ADDRESS = 0;

    /** @var int Самовывоз */
    const PICKUP = 1;

    /** @var int Бесплатный самовывоз из Орла */
    const FREE_PICKUP = 2;

    /**
     * @return array
     */
    public static function values()
    {
        return [
            self::ADDRESS,
            self::PICKUP,
            self::FREE_PICKUP,
        ];
    }
}
