<?php

namespace App\Domain\Enums\Accounting;

final class OrderPaymentMethod
{
    /**
     * Оформление в кредит
     */
    const CREDIT = 'credit';

    /**
     * Автоматическая оплата картой.
     */
    const CARD_PAY = 'card';

    /**
     * Оплата через счет.
     */
    const INVOICE_PAY = 'invoice';

    /**
     * @return array
     */
    public static function values()
    {
        return [
            self::CREDIT,
            self::CARD_PAY,
            self::INVOICE_PAY,
        ];
    }
}
