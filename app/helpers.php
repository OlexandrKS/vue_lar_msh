<?php

use App\Contracts\HydrationFactory;
use App\ServiceLayer\ModelHydration\Hydrator;

if (! function_exists('uuid4')) {
    /**
     * @return string
     */
    function uuid4()
    {
        try {
            return Ramsey\Uuid\Uuid::uuid4()->toString();
        } catch (Exception $exception) {
            /*
             * @see http://php.net/manual/ru/function.uniqid.php#94959
             */
            return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),

                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),

                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version number 4
                mt_rand(0, 0x0fff) | 0x4000,

                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,

                // 48 bits for "node"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
            );
        }
    }
}

if (! function_exists('hydrate')) {
    /**
     * @param string        $entity
     * @param array         $data
     * @param Hydrator|null $hydrator
     *
     * @return \October\Rain\Database\Collection|\October\Rain\Database\Model[]
     */
    function hydrate(string $entity, array $data, Hydrator $hydrator = null)
    {
        return app(HydrationFactory::class)->hydrate($entity, $data, $hydrator);
    }
}

if (! function_exists('wasset')) {
    /**
     * @param string $chunkName
     *
     * @throws Exception
     *
     * @return string
     */
    function wasset(string $chunkName): string
    {
        static $meta = null;
        $path = base_path('public/assets/meta.json');

        if (is_null($meta)) {
            if (! file_exists($path)) {
                throw new Exception('The meta file does not exist.');
            }

            $meta = json_decode(file_get_contents($path), true);
        }

        $asset = array_get($meta, $chunkName);

        if (is_null($asset)) {
            new Exception("Unable to locate webpack chunk: {$chunkName}.");
        }

        return asset($asset);
    }
}

if (! function_exists('explode_full_name')) {
    /**
     * @param string $fullName
     *
     * @return array
     */
    function explode_full_name(string $fullName): array
    {
        preg_match('/^(\w+)(.*)(\s)(\w+\s?)$/ui', trim($fullName), $results);

        return [
            array_get($results, 1, ''),
            array_get($results, 4, ''),
        ];
    }
}
