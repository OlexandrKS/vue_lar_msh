<?php

namespace App\Backend\Entities;

use App\ServiceLayer\Support\ModelUuidKey;
use EloquentFilter\Filterable;
use Illuminate\Support\Facades\Storage;
use October\Rain\Database\Model;

/**
 * @mixin \Eloquent
 */
class File extends Model
{
    use ModelUuidKey;
    use Filterable;

    /**
     * @var string the database table used by the model
     */
    public $table = 'backend_files';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['id'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $casts = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @throws \Exception
     *
     * @return mixed
     */
    public function getImageUrlAttribute()
    {
        return cache()->remember("image_url_/{$this->name}", 600, function () {
            return Storage::disk('backend')
                ->url($this->name);
        });
    }
}
