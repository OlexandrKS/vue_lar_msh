<?php

namespace App\Backend\Entities;

use App\Http\ModelFilters\Backend\SvgIconFilter;
use EloquentFilter\Filterable;
use October\Rain\Database\Model;

/**
 * @property-read int $id
 * @property string $name
 * @property string $content
 *
 * @mixin \Eloquent
 */
class SvgIcon extends Model
{
    use Filterable;

    /**
     * @var string the database table used by the model
     */
    public $table = 'svg_icons';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['id'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @return string
     */
    public function modelFilter()
    {
        return SvgIconFilter::class;
    }
}
