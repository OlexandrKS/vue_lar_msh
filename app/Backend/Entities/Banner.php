<?php

namespace App\Backend\Entities;

use App\ServiceLayer\Support\ModelUuidKey;
use EloquentFilter\Filterable;
use Illuminate\Support\Facades\Storage;
use October\Rain\Database\Model;

/**
 * @property string $name
 * @property string $color
 * @property string $image
 * @property array  $properties
 * @property string $type
 * @property-read string $image_url
 *
 * @mixin \Eloquent
 */
class Banner extends Model
{
    use ModelUuidKey;
    use Filterable;

    public const HEADER_BANNER = 'header';
    public const GRID_BANNER = 'grid';
    public const HOME_BANNER = 'home';

    /**
     * @var string the database table used by the model
     */
    public $table = 'banners';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['id'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $casts = [
        'properties' => 'array',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeGridBanners($query)
    {
        return $query->where('type', static::GRID_BANNER);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeHomeBanners($query)
    {
        return $query->where('type', static::HOME_BANNER);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeHeaderBanners($query)
    {
        return $query->where('type', static::HEADER_BANNER)->get();
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public static function getBannerById($id)
    {
        return self::where('id', $id)->first();
    }

    /**
     * @throws \Exception
     *
     * @return mixed
     */
    public function getImageUrlAttribute()
    {
        return cache()->remember("image_url_/{$this->image}", 600, function () {
            return Storage::disk('backend')
                ->url($this->image);
        });
    }
}
