<?php

namespace App\Backend\Entities\Constructor;

use App\ServiceLayer\Support\ModelUuidKey;
use October\Rain\Database\Model;

/**
 * @property string $component
 * @property int    $position
 * @property array  $contents
 * @property Page   $page
 *
 * @mixin \Eloquent
 */
class ComponentContent extends Model
{
    use ModelUuidKey;

    /**
     * @var string the database table used by the model
     */
    public $table = 'constructor_page_component_contents';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['id'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array
     */
    protected $casts = [
        'contents' => 'array',
    ];

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'page' => Page::class,
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @param \Illuminate\Database\Query\Builder $query
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeSorted($query)
    {
        return $query->orderBy('position', 'asc');
    }
}
