<?php

namespace App\Backend\Entities\Constructor;

use App\Backend\Entities\File;
use App\Backend\Enums\Constructor\PageType;
use App\Http\ModelFilters\Backend\Constructor\PageFilter;
use App\ServiceLayer\Sentry\Sentry;
use App\ServiceLayer\Support\ModelUuidKey;
use Carbon\Carbon;
use Cviebrock\LaravelElasticsearch\Facade as Elasticsearch;
use EloquentFilter\Filterable;
use Illuminate\Support\Facades\Storage;
use October\Rain\Database\Collection;
use October\Rain\Database\Model;
use GuzzleHttp;

/**
 * @property string $id
 * @property string $title
 * @property string $name
 * @property string $slug
 * @property string $breadcrumbs
 * @property string $type
 * @property string $description
 * @property array  $properties
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read Collection $components
 * @property-read string $url
 *
 * @mixin \Eloquent
 */
class Page extends Model
{
    use ModelUuidKey;
    use Filterable;

    /**
     * @var string the database table used by the model
     */
    public $table = 'constructor_pages';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'title',
        'name',
        'slug',
        'breadcrumbs',
        'type',
        'description',
        'properties',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'properties' => 'array',
    ];

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'components' => [
            ComponentContent::class,
            'key'   => 'page_id',
            'scope' => 'sorted',
        ],
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @todo laravel
     */
    protected function afterCreate()
    {
        if (config('elasticsearch.enabled') && $this->isNeedIndexing()) {
            $client = $this->configGuzzle();
            try {
                $client->put('/categories/_doc/'.$this->id,[
                    GuzzleHttp\RequestOptions::JSON => $this->toArray()
                ]);
            } catch (\Exception $e) {
                Sentry::captureException($e);
            }
        }
    }

    /**
     * @todo laravel
     */
    protected function afterDelete()
    {
        if (config('elasticsearch.enabled') && $this->isNeedIndexing()) {
            try {
                Elasticsearch::delete([
                    'index' => 'categories',
                    'id'    => $this->id,
                ]);
            } catch (\Exception $e) {
                Sentry::captureException($e);
            }
        }
    }

    /**
     * @todo laravel
     */
    protected function afterUpdate()
    {
        if (config('elasticsearch.enabled') && $this->isNeedIndexing()) {
            //check if page is already created
            $client = $this->configGuzzle();
            try {
                $client->put('/categories/_doc/'.$this->id,[
                    GuzzleHttp\RequestOptions::JSON => $this->toArray()
                ]);
            } catch (\Exception $e) {
                Sentry::captureException($e);
            }
        }
    }

    /**
     * @return bool
     */
    private function isNeedIndexing()
    {
        return in_array($this->type, [
            PageType::CATEGORY_LIST,
            PageType::CATEGORY,
        ]);
    }

    /**
     * @return string
     */
    public function modelFilter()
    {
        return $this->provideFilter(PageFilter::class);
    }

    /**
     * @return Page|null
     */
    public function getParentPageAttribute()
    {
        if (! $this->relationLoaded('parent_page')) {
            $parent = Page::find(array_get($this->properties, 'parent'));

            $this->setRelation('parent_page', $parent);
        }

        return $this->getRelation('parent_page');
    }

    /**
     * @return File|null
     */
    public function getBannerImageAttribute()
    {
        if (! $this->relationLoaded('banner_image')) {
            $bannerImage = File::find(array_get($this->properties, 'bannerImage'));

            $this->setRelation('banner_image', $bannerImage);
        }

        return $this->getRelation('banner_image');
    }

    /**
     * @return File|null
     */
    public function getGridImageAttribute()
    {
        if (! $this->relationLoaded('grid_image')) {
            $gridImage = File::find(array_get($this->properties, 'gridImage'));

            $this->setRelation('grid_image', $gridImage);
        }

        return $this->getRelation('grid_image');
    }

    /**
     * @return File|null
     */
    public function getMenuImageAttribute()
    {
        if (! $this->relationLoaded('menu_image')) {
            $menuImage = File::find(array_get($this->properties, 'menuImage'));

            $this->setRelation('menu_image', $menuImage);
        }

        return $this->getRelation('menu_image');
    }

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getUrlAttribute()
    {
        return url($this->slug);
    }

    /**
     * @throws \Exception
     *
     * @return mixed
     */
    public function getImageUrlAttribute()
    {
        return cache()->remember("image_url_/{$this->image}", 600, function () {
            return Storage::disk('backend')
                ->url($this->image);
        });
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopePromotions($query)
    {
        $currentDay = Carbon::now()->format('Y-m-d');
        return $query->where('type', '=', PageType::PROMOTION)
            ->where('properties->isPublished', true)
            ->where('properties->dateEnd', '>=', $currentDay)
            ->where('properties->dateStart', '<=', $currentDay);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActiveInstallment($query)
    {
        return $query->promotions()
            ->where('properties->promotionType', 'installment');
    }

    /**
     * @return GuzzleHttp\Client
     */
    private function configGuzzle()
    {
        $uri = config('elasticsearch.connections.default.hosts.scheme', 'http') . '://' .
            config('elasticsearch.connections.default.hosts.host', 'localhost') . ':' .
            config('elasticsearch.connections.default.hosts.port', '9200');

        $httpClient = new GuzzleHttp\Client([
            'base_uri' => $uri
        ]);
        return $httpClient;
    }
}
