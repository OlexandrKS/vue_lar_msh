<?php

namespace App\Backend\Entities;

use App\Http\ModelFilters\Backend\MenuFilter;
use EloquentFilter\Filterable;
use October\Rain\Database\Model;

/**
 * @property-read int $id
 * @property string   $name
 * @property string   $url
 * @property int      $menu_id TODO make model
 * @property SvgIcon  $icon
 * @property MenuItem $parent
 * @property-read MenuItem[] $children
 *
 * @mixin \Eloquent
 */
class MenuItem extends Model
{
    use Filterable;
    /**
     * @var string the database table used by the model
     */
    public $table = 'menu_items';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['id'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'children' => [
            MenuItem::class,
            'key' => 'parent_id',
        ],
    ];
    public $belongsTo = [
        'icon'   => SvgIcon::class,
        'parent' => MenuItem::class,
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @todo
     *
     * @return bool
     */
    public function isActiveMenuItem()
    {
        return false;
    }

    /**
     * @return string
     */
    public function modelFilter()
    {
        return MenuFilter::class;
    }
}
