<?php

namespace App\Backend\ServiceLayer\Banner;

use App\Backend\Entities\Banner;
use SplPriorityQueue;

class BannerQueue
{
    private $priorityQueue = null;

    public function __construct()
    {
        $this->priorityQueue = new SplPriorityQueue();
    }

    public function extract()
    {
        if (! $this->priorityQueue->valid()) {
            $this->fill();
        }

        return ! $this->priorityQueue->isEmpty() ? $this->priorityQueue->extract() : null;
    }

    public function getQueue()
    {
        return $this->priorityQueue;
    }

    private function fill()
    {
        $banners = Banner::gridBanners()
            ->get();

        foreach ($banners as $banner) {
            $this->priorityQueue->insert($banner, rand(0, 100));
        }
    }
}
