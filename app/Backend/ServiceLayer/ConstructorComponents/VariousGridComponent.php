<?php

namespace App\Backend\ServiceLayer\ConstructorComponents;

use App\Backend\Entities\Constructor\ComponentContent;
use App\Backend\ServiceLayer\Constructor\Component;
use App\Http\Resources\Catalog\ProductResource;
use App\Http\Resources\Catalog\SiteGoodTypeResource;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\SiteGoodType;

class VariousGridComponent implements Component
{
    /**
     * @param ComponentContent $componentContent
     * @param array            $properties
     *
     * @return string
     */
    public function render(ComponentContent $componentContent, array $properties = []): string
    {
        $sort = request()->input('sort');
        $page = $componentContent->page;
        $productIds = array_get($componentContent->contents, 'productIds');
        $query = Product::whereIn('id', $productIds);

        $sortUrl = [
            'default'    => request()->url(),
            'price_asc'  => request()->url().'?sort=price_asc',
            'price_desc' => request()->url().'?sort=price',
        ];

        switch ($sort) {
            case 'price_asc':
                $query = $query->ascPriceSort();
                $currentSort = 'От дешевых к дорогим';

                break;
            case 'price':
                $query = $query->descPriceSort();
                $currentSort = 'От дорогих к дешевым';

                break;
            default:
                $query = $query->defaultSort();
                $currentSort = 'По популярности';
        }

        $i = 1;
        $str = '';
        $count = count($productIds);

        while ($i <= $count) {
            $str .= $i < $count ? '?,' : '?';
            ++$i;
        }

        $query->orderByRaw('FIELD(id, '.$str.')', $productIds);

        /** @var LengthAwarePaginator $pagination */
        $pagination = $query->where('category_invisible', false)
            ->has('images')
            ->has('primary_offer')
            ->with('images', 'primary_offer', 'nid', 'category', 'reviewGroups')
            ->paginate(array_get($properties, 'perPage', config('grid.tilePerBlock.category')));

        if (request()->has('sort')) {
            $pagination->appends(['sort' => $sort]);
        }

        $demonstrationMode = request()->has('demonstration');
        $columnClass = $this->calculateColumn(4, $demonstrationMode);
        $heightClass = '';

        if ($demonstrationMode) {
            $pagination->appends('demonstration', $demonstrationMode);
        }

        $banner = $pagination->hasPages() || $pagination->count() + 4 > array_get($properties, 'banner.index', $pagination->perPage()) ?
            array_get($properties, 'banner') :
            ['index' => -1];

        $nextTile = [
            'url'  => $pagination->nextPageUrl(),
            'text' => 'На следующую страницу',
        ];
        $imageType = 'preview_mobile_image';

        $goodType = [
            'type_name' => $componentContent->contents['title'],
            'plural'    => $componentContent->contents['title'],
        ];

        $uri = \request()->url();

        return view('constructor.components.grid', array_merge($properties,
            compact('pagination', 'banner', 'columnClass', 'goodType', 'nextTile', 'page',
                'demonstrationMode', 'heightClass', 'uri', 'currentSort', 'sortUrl', 'imageType'
            )
        ));
    }

    /**
     * @param int  $colsCount
     * @param bool $demonstrationMode
     *
     * @return string
     */
    private function calculateColumn($colsCount = null, $demonstrationMode = false): string
    {
        $colsCount = $colsCount ?? 4;

        if ($demonstrationMode) {
            --$colsCount;
        }

        return "cols-$colsCount";
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function searchCategories(Request $request)
    {
        $value = trim($request->json('query'));

        $collection = SiteGoodType::query()
            ->where('type_name', 'like', "%$value%")
            ->get();

        return SiteGoodTypeResource::collection($collection);
    }

    public function getCategories()
    {
        $collection = SiteGoodType::query()->get();

        return SiteGoodTypeResource::collection($collection);
    }

    /**
     * @param Request $request
     *
     * @return SiteGoodTypeResource
     */
    public function getCategory(Request $request)
    {
        $category = SiteGoodType::query()
            ->where('type_id', $request->json('id'))
            ->firstOrFail();

        return new SiteGoodTypeResource($category);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getProducts(Request $request)
    {
        $typeId = $request->json('categoryId');

        $products = Product::query()
            ->where('site_type_id', '=', $typeId)
            ->get();

        return ProductResource::collection($products);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getSelectedProducts(Request $request)
    {
        $selectedIds = $request->json('selectedIds', []);

        $products = Product::query()
            ->whereIn('id', $selectedIds)
            ->get();

        $products = $products->sort(function ($model) use ($selectedIds) {
            return array_search($model->getKey(), $selectedIds);
        })->values();

        return ProductResource::collection($products);
    }
}
