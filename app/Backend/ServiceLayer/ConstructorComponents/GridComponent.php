<?php

namespace App\Backend\ServiceLayer\ConstructorComponents;

use App\Backend\Entities\Constructor\ComponentContent;
use App\Backend\Enums\Constructor\PageType;
use App\Backend\ServiceLayer\Constructor\Component;
use App\Http\Resources\Catalog\ProductResource;
use App\Http\Resources\Catalog\SiteGoodTypeResource;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\SiteGoodType;

class GridComponent implements Component
{
    /**
     * @param ComponentContent $componentContent
     * @param array            $properties
     *
     * @return string
     */
    public function render(ComponentContent $componentContent, array $properties = []): string
    {
        $sort = request()->input('sort');
        $page = $componentContent->page;
        $goodTypeId = array_get($componentContent->contents, 'goodTypeId');

        /** @var SiteGoodType $goodType */
        $goodType = SiteGoodType::where('type_id', $goodTypeId)
            ->firstOrFail();

        $query = Product::grid($goodTypeId);

        $sortUrl = [
            'default'    => request()->url(),
            'price_asc'  => request()->url().'?sort=price_asc',
            'price_desc' => request()->url().'?sort=price',
        ];

        switch ($sort) {
            case 'price_asc':
                $query = $query->ascPriceSort();
                $currentSort = 'От дешевых к дорогим';

                break;
            case 'price':
                $query = $query->descPriceSort();
                $currentSort = 'От дорогих к дешевым';

                break;
            default:
                $query = $query->defaultSort();
                $currentSort = 'По популярности';
        }

        /** @var LengthAwarePaginator $pagination */
        $pagination = $query->with('images', 'primary_offer', 'nid', 'category', 'reviewGroups')
            ->paginate(array_get($properties, 'perPage', config('grid.tilePerBlock.category')));

        if (request()->has('sort')) {
            $pagination->appends(['sort' => request()->input('sort')]);
        }

        $demonstrationMode = request()->has('demonstration');
        $columnClass = $this->calculateColumn($goodType, $demonstrationMode);
        $heightClass = $this->calculateHeightClass($goodType);

        if ($demonstrationMode) {
            $pagination->appends('demonstration', $demonstrationMode);
        }

        $banner = $pagination->hasPages() || $pagination->count() + 4 > array_get($properties, 'banner.index', $pagination->perPage()) ?
            array_get($properties, 'banner') :
            ['index' => -1];

        switch ($componentContent->page->type) {
            case PageType::CATEGORY_LIST:
                $nextTile = [
                    'url'  => url($goodType->constructor_page ? $goodType->constructor_page->slug : '#'),
                    'text' => 'На страницу категории',
                ];
                break;
            case PageType::CATEGORY:
                $nextTile = [
                    'url'  => $pagination->nextPageUrl(),
                    'text' => 'На следующую страницу',
                ];
                break;
            default:
                $nextTile = false;
        }

        $uri = \request()->url();

        return view('constructor.components.grid', array_merge($properties,
            compact('pagination', 'banner', 'columnClass', 'goodType', 'nextTile', 'page',
                'demonstrationMode', 'heightClass', 'uri', 'currentSort', 'sortUrl'
            )
        ));
    }

    /**
     * @param SiteGoodType $goodType
     * @param bool         $demonstrationMode
     *
     * @return string
     */
    private function calculateColumn(SiteGoodType $goodType, $demonstrationMode = false): string
    {
        $colsCount = 4;

        if ($goodType->width > 270) {
            $colsCount = 3;
        }

        if ($demonstrationMode) {
            --$colsCount;
        }

        return "cols-$colsCount";
    }

    /**
     * @param SiteGoodType $goodType
     *
     * @return string
     */
    private function calculateHeightClass(SiteGoodType $goodType): string
    {
        if ($goodType->height < 222 && $goodType->height > 0) {
            return 'product-wide';
        }

        if ($goodType->height >= 300) {
            return 'product-high';
        }

        return '';
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function searchCategories(Request $request)
    {
        $value = trim($request->json('query'));

        $collection = SiteGoodType::query()
            ->where('type_name', 'like', "%$value%")
            ->get();

        return SiteGoodTypeResource::collection($collection);
    }

    /**
     * @param Request $request
     *
     * @return SiteGoodTypeResource
     */
    public function getCategory(Request $request)
    {
        $category = SiteGoodType::query()
            ->where('type_id', $request->json('id'))
            ->firstOrFail();

        return new SiteGoodTypeResource($category);
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getCategories()
    {
        $collection = SiteGoodType::query()->get();

        return SiteGoodTypeResource::collection($collection);
    }

    /**
     * @param Request $request
     *
     * @return ProductResource
     */
    public function getProducts(Request $request)
    {
        $typeId = $request->json('categoryId');

        $products = Product::query()
            ->avaliable()
            ->where('site_type_id', '=', $typeId)
            ->get();

        return new ProductResource($products);
    }
}
