<?php

namespace App\Backend\ServiceLayer\ConstructorComponents;

use App\Backend\Entities\Constructor\ComponentContent;
use App\Backend\Enums\Constructor\PageType;
use App\Backend\ServiceLayer\Constructor\Component;
use App\Http\Resources\Catalog\CategoryResource;
use App\Http\Resources\Catalog\ProductResource;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use LuckyWeb\MS\Models\Category;
use LuckyWeb\MS\Models\Product;

class InstallmentsGridComponent implements Component
{
    /**
     * @param ComponentContent $componentContent
     * @param array            $properties
     *
     * @return string
     */
    public function render(ComponentContent $componentContent, array $properties = []): string
    {
        $sort = request()->input('sort');
        $page = $componentContent->page;
        $promotionType = $componentContent->contents['promotionType'];
        $categoryId = $componentContent->contents['categoryId'];
        switch ($promotionType) {
            case 'installment':
                $query = Product::installments();
                break;
            case 'weekly':
                $query = Product::weekly();
                break;
            case 'monthly':
                $query = Product::available()
                    ->where('luckyweb_ms_products.position_row', '>', 0)  //todo
                    ->where('luckyweb_ms_products.position_column', '>', 0)
                    ->where('category_id', $categoryId);
                break;
        }

        $sortUrl = [
            'default'    => request()->url(),
            'price_asc'  => request()->url().'?sort=price_asc',
            'price_desc' => request()->url().'?sort=price',
        ];

        $currentSort = '';
        switch ($sort) {
            case 'price_asc':
                $query = $query->ascPriceSort();
                $currentSort = 'От дешевых к дорогим';

                break;
            case 'price':
                $query = $query->descPriceSort();
                $currentSort = 'От дорогих к дешевым';

                break;
            default:
                $query = $query->defaultSort();
                $currentSort = 'По популярности';
        }

        /** @var LengthAwarePaginator $pagination */
        $pagination = $query->where('category_invisible', false)
            ->has('images')
            ->has('primary_offer')
            ->with('images', 'primary_offer', 'nid', 'category', 'reviewGroups')
            ->paginate(array_get($properties, 'perPage', config('grid.tilePerBlock.category')));

        $demonstration = request()->url().'?';

        $demonstrationMode = request()->has('demonstration');
        $columnClass = 'cols-4';
        if ($demonstrationMode) {
            $pagination->appends('demonstration', $demonstrationMode);
            $columnClass = 'cols-3';
            $demonstration = '&demonstration=';
        }

        if (request()->has('sort')) {
            $pagination->appends(['sort' => request()->input('sort')]);
            $demonstration = request()->url().'?sort='.$sort;
        }

        switch ($componentContent->page->type) {
                case PageType::PROMOTION:
                $nextTile = [
                    'url'  => $pagination->nextPageUrl(),
                    'text' => 'На следующую страницу',
                ];
                break;
            default:
                $nextTile = false;
        }
        $zoomIn = $demonstration.'&demonstration=';
        $zoomOut = $demonstration;
        $uri = \request()->url();

        return view('page.installments.products', array_merge($properties,
            compact(
                'currentSort', 'sortUrl', 'pagination', 'zoomIn', 'zoomOut', 'columnClass', 'nextTile',
                'page', 'demonstrationMode', 'uri'
            )
        ));
    }

    /**
     * @param Request $request
     *
     * @return ProductResource
     */
    public function getProducts(Request $request)
    {
        $typeId = $request->json('categoryId');

        $products = Product::query()
            ->displayable()
            ->where('site_type_id', '=', $typeId)
            ->where('installments', 1)
            ->get();

        return new ProductResource($products);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getCategories(Request $request)
    {
        $categories = Category::get();

        return CategoryResource::collection($categories);
    }
}
