<?php

namespace App\Backend\ServiceLayer\ConstructorComponents;

use App\Backend\Entities\Constructor\ComponentContent;
use App\Backend\ServiceLayer\Constructor\Component;
use Illuminate\Contracts\View\Factory;

class TextComponent implements Component
{
    /**
     * @var \Parsedown
     */
    private $markdownParser;

    /**
     * @var Factory
     */
    private $viewFactory;

    /**
     * TextComponent constructor.
     *
     * @param Factory $viewFactory
     */
    public function __construct(Factory $viewFactory)
    {
        $this->markdownParser = new \Parsedown();
        $this->viewFactory = $viewFactory;
    }

    /**
     * @param ComponentContent $componentContent
     * @param array            $properties
     *
     * @return string
     */
    public function render(ComponentContent $componentContent, array $properties = []): string
    {
        switch (array_get($componentContent->contents, 'type')) {
            case 'seo':
                $view = 'constructor.components.seoText';
                break;
            default:
            case 'default':
                $view = 'constructor.components.text';
        }

        return $this->viewFactory->make($view, [
            'title' => array_get($componentContent->contents, 'title'),
            'value' => $this->markdownParser->text(array_get($componentContent->contents, 'text')),
        ])->render();
    }
}
