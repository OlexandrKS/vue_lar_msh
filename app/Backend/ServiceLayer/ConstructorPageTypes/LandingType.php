<?php

namespace App\Backend\ServiceLayer\ConstructorPageTypes;

use App\Backend\Entities\Constructor\ComponentContent;
use App\Backend\Entities\Constructor\Page as PageModel;
use App\Backend\ServiceLayer\Constructor\Component;
use App\Backend\ServiceLayer\Constructor\Page;
use App\Contracts\ConstructorManager;

class LandingType implements Page
{
    /**
     * @var ConstructorManager
     */
    private $manager;

    /**
     * LandingType constructor.
     *
     * @param ConstructorManager $manager
     */
    public function __construct(ConstructorManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * {@inheritdoc}
     */
    public function render(PageModel $page): string
    {
        $pageContent = '';

        /** @var ComponentContent $component */
        foreach ($page->components as $component) {
            $component->setRelation('page', $page);

            /** @var Component $componentObject */
            $componentObject = $this->manager->createComponent($component->component);
            $pageContent .= $componentObject->render($component);
        }

        return $pageContent;
    }
}
