<?php

namespace App\Backend\ServiceLayer\ConstructorPageTypes;

use App\Backend\Entities\Constructor\ComponentContent;
use App\Backend\Entities\Constructor\Page;
use App\Backend\ServiceLayer\Constructor\Component;
use App\Backend\ServiceLayer\Constructor\Page as PageContract;
use App\Contracts\ConstructorManager;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

class PromotionType implements PageContract
{
    /**
     * @var ConstructorManager
     */
    private $manager;

    /**
     * LandingType constructor.
     *
     * @param ConstructorManager $manager
     */
    public function __construct(ConstructorManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param Page $page
     *
     * @throws \Throwable
     *
     * @return string
     */
    public function render(Page $page): string
    {
        if (! $page->properties['isPublished']) { //todo
            return response()->redirectTo('/404');
        }

        $componentsContent = [];
        $index = 0;

        $breadcrumbsManager = Breadcrumbs::getFacadeRoot();
        $perPage = config('grid.tilePerBlock.category_list');
        $banner = $page->banner_image->image_url;
        $header = $page->title;

        /** @var ComponentContent $component */
        foreach ($page->components as $component) {
            $component->setRelation('page', $page);
            /** @var Component $componentObject */
            $componentObject = $this->manager->createComponent($component->component);
            $componentsContent[] = $componentObject->render($component,
                compact('perPage')
            );
            ++$index;
        }

        return view('constructor.pages.installments_grid', compact(
            'componentsContent', 'page', 'breadcrumbsManager', 'banner', 'header'
        ))
            ->render();
    }
}
