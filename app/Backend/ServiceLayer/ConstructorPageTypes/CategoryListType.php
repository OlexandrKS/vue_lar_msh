<?php

namespace App\Backend\ServiceLayer\ConstructorPageTypes;

use App\Backend\Entities\Constructor\ComponentContent;
use App\Backend\Entities\Constructor\Page;
use App\Backend\Entities\MenuItem;
use App\Backend\Enums\Menu;
use App\Backend\ServiceLayer\Banner\BannerQueue;
use App\Backend\ServiceLayer\Constructor\Component;
use App\Backend\ServiceLayer\Constructor\Page as PageContract;
use App\Contracts\ConstructorManager;
use App\Http\Resources\Catalog\SiteGoodTypeResource;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;
use LuckyWeb\MS\Models\SiteGoodType;

class CategoryListType implements PageContract
{
    /**
     * @var ConstructorManager
     */
    protected $manager;

    /**
     * @var BannerQueue
     */
    protected $bannerQueue;

    /**
     * CategoryType constructor.
     *
     * @param ConstructorManager $manager
     * @param BannerQueue        $bannerQueue
     */
    public function __construct(ConstructorManager $manager, BannerQueue $bannerQueue)
    {
        $this->manager = $manager;
        $this->bannerQueue = $bannerQueue;
    }

    /**
     * @param Page $page
     *
     * @throws \Throwable
     *
     * @return string
     */
    public function render(Page $page): string
    {
        $componentsContent = [];
        $index = 0;
        $perPage = config('grid.tilePerBlock.category_list');
        $disablePagination = true;
        $breadcrumbsManager = Breadcrumbs::getFacadeRoot();

        /** @var ComponentContent $component */
        foreach ($page->components as $component) {
            $component->setRelation('page', $page);

            /** @var Component $componentObject */
            $componentObject = $this->manager->createComponent($component->component);
            $banner = [
                'index'        => config('grid.banners.category_list')[$index % 2], // четный, нечетный индекс,
                'bannerObject' => $this->bannerQueue->extract(),
            ];

            $componentsContent[] = $componentObject->render($component,
                compact('banner', 'perPage', 'disablePagination')
            );
            ++$index;
        }

        $menuCategories = MenuItem::where('url', $page->slug)
            ->whereNull('parent_id')
            ->where('menu_id', Menu::CATEGORY_LIST)
            ->with('children')
            ->first();

        return view('constructor.pages.category_list', compact(
            'componentsContent', 'page', 'breadcrumbsManager', 'menuCategories'
        ))
            ->render();
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function searchCategories(Request $request)
    {
        $query = $request->json('query');

        $collection = SiteGoodType::query()
            ->where('type_name', 'like', "%$query%")
            ->get();

        return SiteGoodTypeResource::collection($collection);
    }
}
