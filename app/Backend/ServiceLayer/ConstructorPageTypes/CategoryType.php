<?php

namespace App\Backend\ServiceLayer\ConstructorPageTypes;

use App\Backend\Entities\Constructor\ComponentContent;
use App\Backend\Entities\Constructor\Page;
use App\Backend\ServiceLayer\Constructor\Component;
use App\Backend\ServiceLayer\Constructor\Page as PageContract;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;

class CategoryType extends CategoryListType implements PageContract
{
    /**
     * @param Page $page
     *
     * @throws \Throwable
     *
     * @return string
     */
    public function render(Page $page): string
    {
        $componentsContent = [];
        $pageIndex = (request()->input('page', 1) - 1) % 2;
        $banner = [
            'index'        => config('grid.banners.category')[$pageIndex], // четный, нечетный индекс,
            'bannerObject' => $this->bannerQueue->extract(),
        ];
        $breadcrumbsManager = Breadcrumbs::getFacadeRoot();

        /** @var ComponentContent $component */
        foreach ($page->components as $component) {
            $component->setRelation('page', $page);

            /** @var Component $componentObject */
            $componentObject = $this->manager->createComponent($component->component);
            $componentsContent[] = $componentObject->render($component, compact('banner'));
        }

        return view('constructor.pages.category', compact(
            'componentsContent', 'page', 'breadcrumbsManager'
        ))
            ->render();
    }
}
