<?php

namespace App\Backend\ServiceLayer\Constructor;

use App\Backend\Entities\Constructor\Page as PageModel;

interface Page
{
    /**
     * @param \App\Backend\Entities\Constructor\Page $page
     *
     * @return string
     */
    public function render(PageModel $page): string;
}
