<?php

namespace App\Backend\ServiceLayer\Constructor;

use App\Backend\Entities\Constructor\ComponentContent;

interface Component
{
    /**
     * @param ComponentContent $componentContent
     * @param array            $properties
     *
     * @return string
     */
    public function render(ComponentContent $componentContent, array $properties = []): string;
}
