<?php

namespace App\Backend\ServiceLayer\Constructor;

use App\Backend\Entities\Constructor\Page;
use App\Backend\Enums\Constructor\PageType;
use App\Backend\ServiceLayer\Constructor\Page as PageContract;
use App\Backend\ServiceLayer\ConstructorComponents\GridComponent;
use App\Backend\ServiceLayer\ConstructorComponents\InstallmentsGridComponent;
use App\Backend\ServiceLayer\ConstructorComponents\TextComponent;
use App\Backend\ServiceLayer\ConstructorComponents\VariousGridComponent;
use App\Backend\ServiceLayer\ConstructorPageTypes\CategoryListType;
use App\Backend\ServiceLayer\ConstructorPageTypes\CategoryType;
use App\Backend\ServiceLayer\ConstructorPageTypes\LandingType;
use App\Backend\ServiceLayer\ConstructorPageTypes\PromotionType;
use App\Contracts\ConstructorManager;
use Cms\Classes\PageCode as OctoberPage;
use Illuminate\Contracts\Container\Container;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use LuckyWeb\MS\Classes\PageMetaManager;

class Manager implements ConstructorManager
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var array
     */
    private $components = [
        'grid'         => GridComponent::class,
        'text'         => TextComponent::class,
        'variousGrid'  => VariousGridComponent::class,
        'installments' => InstallmentsGridComponent::class,
    ];

    /**
     * @var array
     */
    private $pages = [
        PageType::CATEGORY      => CategoryType::class,
        PageType::CATEGORY_LIST => CategoryListType::class,
        PageType::LANDING       => LandingType::class,
        PageType::PROMOTION     => PromotionType::class,
    ];

    /**
     * Manager constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param Request          $request
     * @param OctoberPage|null $octoberPage
     *
     * @throws ModelNotFoundException
     * @throws \Throwable
     *
     * @return string
     */
    public function render(Request $request, OctoberPage $octoberPage = null): string
    {
        /** @var Page $page */
        $page = Page::query()
            ->where('slug', $request->getPathInfo())
            ->with('components')
            ->firstOrFail();

        if ($octoberPage) {
            $this->setMetaTags($page, $octoberPage);
        }

        return $this->renderPage($page);
    }

    /**
     * @param string $type
     *
     * @return Component
     */
    public function createComponent(string $type): Component
    {
        $class = array_get($this->components, $type);

        if (is_null($class)) {
            throw new \InvalidArgumentException();
        }

        return $this->container->make($class);
    }

    /**
     * @param string $type
     *
     * @return \App\Backend\ServiceLayer\Constructor\Page
     */
    public function createPage(string $type): PageContract
    {
        $class = array_get($this->pages, $type);

        if (is_null($class)) {
            throw new \InvalidArgumentException();
        }

        return $this->container->make($class);
    }

    /**
     * @param Page $page
     *
     * @return string
     */
    public function renderPage(Page $page): string
    {
        return $this->createPage($page->type)
            ->render($page);
    }

    /**
     * @param Page        $page
     * @param OctoberPage $octoberPage
     */
    private function setMetaTags(Page $page, OctoberPage $octoberPage)
    {
        $octoberPage->title = PageMetaManager::getTitle($page->title);
        $octoberPage->description = PageMetaManager::getDescription($page->description);
    }
}
