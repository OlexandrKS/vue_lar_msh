<?php

namespace App\Backend\Enums\Constructor;

final class PageType
{
    const CATEGORY_LIST = 'category_list';
    const CATEGORY = 'category';
    const BLOG = 'blog';
    const LANDING = 'landing';
    const PROMOTION = 'promotion';

    /**
     * @return array
     */
    public static function values()
    {
        return [
            static::PROMOTION,
            static::CATEGORY_LIST,
            static::CATEGORY,
            static::BLOG,
            static::LANDING,
        ];
    }
}
