<?php

namespace App\Backend\Enums;

final class Menu
{
    const GLOBAL = 1;
    const CATEGORY_LIST = 2;
}
