<?php

namespace App\Http\Resources\Catalog;

use App\Domain\Entities\Catalog\Offer;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @mixin Offer
 */
class OfferResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->getKey(),
            'type'              => $this->type,
            'properties'        => $this->properties,
            'active_price'      => $this->active_price,
            'active_discount'   => $this->active_discount,
            'primary'           => new ProductResource($this->whenLoaded('primary')),
            'products'          => ProductResource::collection($this->whenLoaded('products')),
            'prices'            => $this->prices->toArray(),
        ];
    }
}
