<?php

namespace App\Http\Resources\Catalog;

use Illuminate\Http\Resources\Json\Resource;
use LuckyWeb\MS\Models\Category;

/**
 * @mixin Category
 */
class CategoryResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'   => $this->getKey(),
            'name' => $this->name,
            'slug' => $this->slug,
            'url'  => '/catalog/'.$this->slug,
        ];
    }
}
