<?php

namespace App\Http\Resources\Catalog;

use Illuminate\Http\Resources\Json\Resource;
use LuckyWeb\MS\Models\Product;

/**
 * @mixin Product
 */
class ProductResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->getKey(),
            'name'                  => $this->name,
            'subname'               => $this->subname,
            'availability_indicator'=> $this->residual_indicator,
            'nid'                   => new NumericIdResource($this->nid),
            'url'                   => $this->url,
            'category'              => new CategoryResource($this->category),
            'image_preview'         => $this->preview_image->url_preview,
            'image_preview_desktop' => $this->preview_desktop_image->url_preview,
            'image_preview_mobile'  => $this->preview_mobile_image->url_preview,
            'good_type_site'        => new SiteGoodTypeResource($this->good_type_site),
            'together_cheaper'      => OfferResource::collection($this->whenLoaded('together_cheaper')),
            'quantity'              => $this->whenPivotLoaded('offer_product_pivot', function () {
                return (int) $this->pivot->quantity;
            }),
        ];
    }
}
