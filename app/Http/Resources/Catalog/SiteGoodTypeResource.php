<?php

namespace App\Http\Resources\Catalog;

use App\Http\Resources\Backend\ConstructorPage\PageResource;
use Illuminate\Http\Resources\Json\Resource;
use LuckyWeb\MS\Models\SiteGoodType;

/**
 * @mixin SiteGoodType
 */
class SiteGoodTypeResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->getKey(),
            'type_id'          => $this->type_id,
            'type_name'        => $this->type_name,
            'plural'           => $this->plural,
            'constructor_page' => $this->when($this->constructor_page, function () {
                return new PageResource($this->constructor_page);
            }),
        ];
    }
}
