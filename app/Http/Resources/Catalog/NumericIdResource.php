<?php

namespace App\Http\Resources\Catalog;

use Illuminate\Http\Resources\Json\Resource;
use LuckyWeb\MS\Models\ProductId;

/**
 * @mixin ProductId
 */
class NumericIdResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->getKey(),
            'numeric_id'        => $this->numeric_id,
        ];
    }
}
