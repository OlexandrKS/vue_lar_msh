<?php

namespace App\Http\Resources;

use App\Backend\Entities\Banner;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @mixin Banner
 */
class BannerResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'       => $this->name,
            'type'       => $this->type,
            'image'      => $this->image,
            'color'      => $this->color,
            'image_url'  => $this->image_url,
            'properties' => $this->properties,
        ];
    }
}
