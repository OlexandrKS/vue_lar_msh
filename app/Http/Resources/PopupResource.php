<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use LuckyWeb\MS\Models\PagePopup;

/**
 * @mixin PagePopup
 */
class PopupResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                   => $this->getKey(),
            'popup_name'           => $this->popup_name,
            'type'                 => $this->type,
            'float_header'         => $this->float_header,
            'left_button_id'       => $this->left_button_id,
            'left_button_text'     => $this->left_button_text,
            'left_button_color'    => $this->left_button_color,
            'left_button_link'     => $this->left_button_link,
            'right_button_id'      => $this->right_button_id,
            'right_button_text'    => $this->right_button_text,
            'right_button_color'   => $this->right_button_color,
            'timeout'              => $this->timeout,
            'message'              => $this->message,
            'image_url'            => $this->image_url,
            'image_path'           => ($this->image) ? $this->image->getPath() : null,
            'variant_footer_popup' => $this->variant_footer_popup,
            'text_subscribe'       => $this->text_subscribe,
            'modal_size'           => $this->modal_size,
            'promo_code'           => $this->promo_code,
        ];
    }
}
