<?php

namespace App\Http\Resources\Account;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;
use LuckyWeb\User\Models\User;

/**
 * @mixin User
 */
class UserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->getKey(),
            'name'         => $this->name,
            'last_name'    => $this->last_name,
            'other_name'   => $this->other_name,
            'email'        => $this->email,
            'phone'        => $this->phone,
            'card_code'    => $this->card_code,
            'birth_date'   => $this->birth_date ? Carbon::parse($this->birth_date)->toDateTimeString() : null,
            'bonuses'      => $this->bonuses->toArray(),
            'bonus_events' => BonusEventResource::collection($this->whenLoaded('bonus_events')),
            'gender_id'    => $this->gender_id,
        ];
    }
}
