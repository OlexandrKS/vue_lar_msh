<?php

namespace App\Http\Resources\Account;

use Illuminate\Http\Resources\Json\Resource;
use LuckyWeb\User\Models\BonusEvent;

/**
 * @mixin BonusEvent
 */
class BonusEventResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'external_id' => $this->external_id,
            'type_id'     => $this->type_id,
            'description' => $this->description,
            'applied_at'  => $this->applied_at->toDateTimeString(),
            'expired_at'  => $this->expired_at->toDateTimeString(),
        ];
    }
}
