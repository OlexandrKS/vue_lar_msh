<?php

namespace App\Http\Resources\Account;

use Illuminate\Http\Resources\Json\Resource;
use Luckyweb\User\Models\SocialUser;

/**
 * @mixin SocialUser
 */
class SocialUserResource extends Resource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->getKey(),
            'provider_id' => $this->provider_id,
            'social_id'   => $this->social_id,
            'name'        => $this->name,
            'last_name'   => $this->last_name,
            'email'       => $this->email,
        ];
    }
}
