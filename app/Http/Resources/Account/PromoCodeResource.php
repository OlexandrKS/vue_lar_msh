<?php

namespace App\Http\Resources\Account;

use Illuminate\Http\Resources\Json\Resource;
use LuckyWeb\User\Models\PromoCodeEvent;

/**
 * @mixin PromoCodeEvent
 */
class PromoCodeResource extends Resource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'             => $this->getKey(),
            'code'           => $this->code,
            'amount'         => $this->promo_code->amount,
            'available_from' => $this->promo_code->available_from,
            'status'         => $this->promo_code->status,
            'expired_at'     => $this->promo_code->expired_at,
            'applied_at'     => $this->applied_at,
            'created_at'     => $this->created_at,
        ];
    }
}
