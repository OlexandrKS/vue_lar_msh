<?php

namespace App\Http\Resources\Backend\ConstructorPage;

use App\Backend\Entities\Constructor\ComponentContent;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @mixin ComponentContent
 */
class ComponentContentResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'component'     => $this->component,
            'position'      => $this->position,
            'contents'      => $this->contents,
        ];
    }
}
