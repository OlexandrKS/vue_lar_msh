<?php

namespace App\Http\Resources\Backend\ConstructorPage;

use App\Backend\Entities\Constructor\Page;
use App\Backend\Enums\Constructor\PageType;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @mixin Page
 */
class PageResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->getKey(),
            'title'       => $this->title,
            'name'        => $this->name,
            'slug'        => $this->slug,
            'url'         => url($this->slug),
            'breadcrumbs' => $this->breadcrumbs,
            'type'        => $this->type,
            'description' => $this->description,
            'properties'  => $this->properties,
            'components'  => ComponentContentResource::collection($this->whenLoaded('components')),
            'created_at'  => $this->created_at->toDateTimeString(),
            'updated_at'  => $this->updated_at->toDateTimeString(),

            $this->mergeWhen(PageType::PROMOTION === $this->type, [
                'banner_image' => $this->banner_image,
                'grid_image'   => $this->grid_image,
                'menu_image'   => $this->menu_image,
            ]),
        ];
    }
}
