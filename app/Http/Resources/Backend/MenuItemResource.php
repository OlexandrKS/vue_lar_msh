<?php

namespace App\Http\Resources\Backend;

use App\Backend\Entities\MenuItem;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @mixin MenuItem
 */
class MenuItemResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->getKey(),
            'menu_id'   => $this->menu_id,
            'name'      => $this->name,
            'url'       => $this->url,
            'order'     => $this->order,
            'icon'      => new SvgIconResource($this->icon),
            'icon_id'   => $this->icon_id,
            'parent'    => new static($this->whenLoaded('parent')),
            'parent_id' => $this->parent_id,
            'children'  => static::collection($this->whenLoaded('children')),
        ];
    }
}
