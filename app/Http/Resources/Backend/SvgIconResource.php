<?php

namespace App\Http\Resources\Backend;

use App\Backend\Entities\SvgIcon;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @mixin SvgIcon
 */
class SvgIconResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->getKey(),
            'name'    => $this->name,
            'content' => $this->content,
        ];
    }
}
