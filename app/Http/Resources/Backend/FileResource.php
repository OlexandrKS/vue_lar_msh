<?php

namespace App\Http\Resources\Backend;

use App\Backend\Entities\File;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @mixin File
 */
class FileResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->getKey(),
            'name'        => $this->name,
            'created_at'  => $this->created_at->toDateTimeString(),
            'updated_at'  => $this->updated_at->toDateTimeString(),
        ];
    }
}
