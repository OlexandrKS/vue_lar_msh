<?php

namespace App\Http\Resources\Backend;

use App\Backend\Entities\Banner;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @mixin Banner
 */
class BannerResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->getKey(),
            'name'        => $this->name,
            'image'       => $this->image,
            'color'       => $this->color,
            'type'        => $this->type,
            'properties'  => $this->properties,
            'created_at'  => $this->created_at->toDateTimeString(),
            'updated_at'  => $this->updated_at->toDateTimeString(),
        ];
    }
}
