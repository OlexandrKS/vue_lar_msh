<?php

namespace App\Http\Resources\Accounting;

use Illuminate\Http\Resources\Json\Resource;
use LuckyWeb\User\Models\OrderItem;

/**
 * @mixin OrderItem
 */
class UserOrderItemResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->getKey(),
            'good_id'   => $this->good_id,
            'color_id'  => $this->color_id,
            'name'      => $this->name,
            'color'     => $this->color,
            'amount'    => $this->amount,
            'status_id' => $this->status_id,
            'quantity'  => (int) $this->quantity,
        ];
    }
}
