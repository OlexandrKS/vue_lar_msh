<?php

namespace App\Http\Resources\Accounting;

use Illuminate\Http\Resources\Json\Resource;
use LuckyWeb\User\Models\Order;

/**
 * @mixin Order
 */
class UserOrderResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                 => $this->getKey(),
            'status_id'          => $this->status_id,
            'external_id'        => $this->external_id,
            'agreement_date'     => $this->agreement_date,
            'agreement_number'   => $this->agreement_number,
            'delivery_date'      => $this->delivery_date,
            'product_amount'     => $this->product_amount,
            'product_debt'       => $this->product_debt,
            'service_amount'     => $this->service_amount,
            'service_paid'       => $this->service_paid,
            'delivery_amount'    => $this->delivery_amount,
            'delivery_paid'      => $this->delivery_paid,
            'reclamation_amount' => $this->reclamation_amount,
            'reclamation_paid'   => $this->reclamation_paid,
            'total_amount'       => $this->total_amount,
            'total_debt'         => $this->total_debt,
            'total_paid'         => $this->total_paid,
            'extended'           => $this->extended,
            'items'              => $this->whenLoaded('items', UserOrderItemResource::collection($this->items)),
            'applied_at'         => $this->applied_at->toDateTimeString(),
            'created_at'         => $this->created_at->toDateTimeString(),
            'updated_at'         => $this->updated_at->toDateTimeString(),
        ];
    }
}
