<?php

namespace App\Http\Resources\Accounting;

use App\Domain\Entities\Accounting\FavoriteListItem;
use App\Http\Resources\Account\UserResource;
use App\Http\Resources\Catalog\OfferResource;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @mixin FavoriteListItem
 */
class FavoriteListItemResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->getKey(),
            'offer' => new OfferResource($this->offer),
            'user'  => new UserResource($this->whenLoaded('user')),
        ];
    }
}
