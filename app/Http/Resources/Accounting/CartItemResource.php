<?php

namespace App\Http\Resources\Accounting;

use App\Domain\Entities\Accounting\CartItem;
use App\Http\Resources\Catalog\OfferResource;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @mixin CartItem
 */
class CartItemResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->getKey(),
            'offer'             => new OfferResource($this->offer),
            'quantity'          => (int) $this->quantity,
            'promotions'        => $this->promotions,
        ];
    }
}
