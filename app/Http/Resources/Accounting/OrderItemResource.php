<?php

namespace App\Http\Resources\Accounting;

use App\Domain\Entities\Accounting\OrderItem;
use App\Http\Resources\Catalog\OfferResource;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @mixin OrderItem
 */
class OrderItemResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->getKey(),
            'offer'             => new OfferResource($this->offer),
            'quantity'          => (int) $this->quantity,
            'promotions'        => $this->promotions,
        ];
    }
}
