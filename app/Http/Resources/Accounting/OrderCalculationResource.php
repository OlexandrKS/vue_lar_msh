<?php

namespace App\Http\Resources\Accounting;

use App\Domain\Entities\Accounting\Order\Calculation;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @property Calculation $resource
 * @mixin Calculation
 */
class OrderCalculationResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return array_merge($this->resource->toArray(), [
            'promotions' => $this->promotions,
        ]);
    }
}
