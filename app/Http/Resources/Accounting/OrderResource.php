<?php

namespace App\Http\Resources\Accounting;

use App\Domain\Entities\Accounting\Order;
use App\Http\Resources\Account\UserResource;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @mixin Order
 */
class OrderResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'             => $this->getKey(),
            'user'           => new UserResource($this->whenLoaded('user')),
            'status'         => $this->status,
            'payment_method' => $this->payment_method,
            'properties'     => $this->properties->toArray(),
            'calculated'     => $this->calculated->toArray(),
            'items'          => $this->whenLoaded('items', OrderItemResource::collection($this->items)),
            'created_at'     => $this->created_at->toDateTimeString(),
            'updated_at'     => $this->updated_at->toDateTimeString(),
        ];
    }
}
