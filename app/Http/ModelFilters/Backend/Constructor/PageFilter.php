<?php

namespace App\Http\ModelFilters\Backend\Constructor;

use EloquentFilter\ModelFilter;

class PageFilter extends ModelFilter
{
    /**
     * Related Models that have ModelFilters as well as the method on the ModelFilter
     * As [relationMethod => [input_key1, input_key2]].
     *
     * @var array
     */
    public $relations = [];

    /**
     * @param string $value
     *
     * @return PageFilter|\Illuminate\Database\Eloquent\Builder
     */
    public function search(string $value)
    {
        return $this->where('name', 'like', "%$value%")
            ->orWhere('title', 'like', "%$value%");
    }
}
