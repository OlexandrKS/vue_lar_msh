<?php

namespace App\Http\Controllers\Api;

use App\Backend\Entities\Banner;
use App\Http\Resources\BannerResource;

class BannerController extends ApiController
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $collection = Banner::all();

        return BannerResource::collection($collection);
    }

    /**
     * @param string $url
     *
     * @return BannerResource
     */
    public function show($url)
    {
        $url = base64_decode($url);

        $banner = Banner::where('type', 'header')
            ->inRandomOrder()
            ->first();

        return new BannerResource($banner);
    }
}
