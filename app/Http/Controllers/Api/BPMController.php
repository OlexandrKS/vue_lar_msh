<?php

namespace App\Http\Controllers\Api;

use App\ServiceLayer\BPM\LeadManager;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class BPMController extends Controller
{
    /**
     * BPMController constructor.
     */
    public function __construct()
    {
        $this->middleware('web')->only('callibriChatClosed');
        $this->middleware('throttle,2,1')->only('callibriChatClosed');
    }

    /**
     * @param LeadManager $leadManager
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function callibriFeedback(LeadManager $leadManager)
    {
        $leadManager->sendCallibriFeedBack();

        return response(null, 200);
    }

    /**
     * @param Request     $request
     * @param LeadManager $leadManager
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function leftCheckout(Request $request, LeadManager $leadManager)
    {
        $leadManager->sendLeftCheckout($request->json()->all());

        return response(null, 200);
    }
}
