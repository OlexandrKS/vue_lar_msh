<?php

namespace App\Http\Controllers\Api;

use App\Domain\Process\User\ProdBoardSignIn;
use App\ServiceLayer\Sentry\Sentry;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProdBoardController extends ApiController
{
    /**
     * Validate user credentials.
     *
     * @param Request $request
     * @param Guard   $guard
     *
     * @return JsonResponse
     */
    public function signIn(Request $request, Guard $guard)
    {
        try {
            $credentials = array_only($request->json()->all(), ['email', 'password']);
            $result = $guard->validate($credentials);

            return new JsonResponse([
                'success' => $result,
            ]);
        } catch (\Exception $e) {
            Sentry::captureException($e);

            return new JsonResponse([
                'success' => false,
            ]);
        }
    }

    /**
     * @param Request    $request
     * @param Dispatcher $eventDispatcher
     */
    public function sendBasket(Request $request, Dispatcher $eventDispatcher)
    {
        $eventDispatcher->dispatch('luckyweb.prodboard.sendBasket', [$request->all()]);
    }

    /**
     * @param ProdBoardSignIn $prodBoardSignIn
     *
     * @return bool|string
     */
    public function signInProdBoard(ProdBoardSignIn $prodBoardSignIn)
    {
        return $prodBoardSignIn->handle();
    }
}
