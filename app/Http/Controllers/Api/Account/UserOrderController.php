<?php

namespace App\Http\Controllers\Api\Account;

use App\Http\Controllers\Api\ApiController;
use App\Http\Resources\Accounting\UserOrderResource;
use Illuminate\Contracts\Auth\Guard;
use LuckyWeb\User\Models\Order;

class UserOrderController extends ApiController
{
    /**
     * @param Guard $guard
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Guard $guard)
    {
        $orders = Order::where('user_id', $guard->id())
            ->with('items')
            ->get();

        return UserOrderResource::collection($orders);
    }
}
