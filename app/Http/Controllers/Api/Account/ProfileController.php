<?php

namespace App\Http\Controllers\Api\Account;

use App\Domain\Process\User\ChangePassword;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\Account\Profile\ChangePasswordRequest;
use App\Http\Requests\Api\Account\Profile\StoreRequest;
use App\Http\Resources\Account\UserResource;
use Illuminate\Http\Request;
use LuckyWeb\User\Models\User;

class ProfileController extends ApiController
{
    /**
     * @param Request $request
     *
     * @return \App\Http\Resources\Account\UserResource
     */
    public function index(Request $request)
    {
        $user = $request->user();

        if ($request->has('withBonuses')) {
            $user->load('bonus_events');
        }

        return new UserResource($user);
    }

    /**
     * @param StoreRequest $request
     *
     * @throws \Throwable
     *
     * @return UserResource
     */
    public function store(StoreRequest $request)
    {
        /** @var User $user */
        $user = $request->user();
        $data = array_only($request->json()->all(), [
            'name', 'last_name', 'other_name', 'gender_id', 'birth_date',
        ]);

        $user->fill($data);
        $user->saveOrFail();

        return new UserResource($user);
    }

    public function changePassword(ChangePasswordRequest $request, ChangePassword $changePassword)
    {
        $credentials = [
            'password' => $request->json('password'),
        ];

        $user = $changePassword->handle($credentials);

        return new UserResource($user);
    }
}
