<?php

namespace App\Http\Controllers\Api\Account;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\Account\PromoCode\StoreRequest;
use App\Http\Resources\Account\PromoCodeResource;
use Illuminate\Contracts\Auth\Guard;
use LuckyWeb\User\Models\PromoCode;
use LuckyWeb\User\Models\PromoCodeEvent;

class PromoCodeController extends ApiController
{
    /**
     * @param Guard $guard
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Guard $guard)
    {
        $promoCodes = PromoCodeEvent::where('user_id', $guard->user()->getKey())
            ->get();

        return PromoCodeResource::collection($promoCodes);
    }

    /**
     * @param Guard $guard
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function available(Guard $guard)
    {
        $promoCodes = PromoCodeEvent::available($guard->user())
            ->get();

        return PromoCodeResource::collection($promoCodes);
    }

    /**
     * @param StoreRequest $request
     *
     * @throws \Throwable
     *
     * @return PromoCodeResource
     */
    public function store(StoreRequest $request)
    {
        /** @var PromoCode $promoCode */
        $promoCode = PromoCode::where('code', $request->json('code'))
            ->firstOrFail();
        $event = $promoCode->activate($request->user());

        return new PromoCodeResource($event);
    }
}
