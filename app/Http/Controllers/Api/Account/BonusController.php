<?php

namespace App\Http\Controllers\Api\Account;

use App\Domain\Entities\Account\User;
use App\Http\Controllers\Api\ApiController;
use App\Http\Resources\Account\BonusEventResource;
use Illuminate\Http\Request;

class BonusController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        /** @var User $user */
        $user = $request->user();

        return BonusEventResource::collection($user->bonus_events);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // todo bonus activation
    }
}
