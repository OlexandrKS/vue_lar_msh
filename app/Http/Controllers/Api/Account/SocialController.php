<?php

namespace App\Http\Controllers\Api\Account;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\Account\Profile\Social\DestroyRequest;
use App\Http\Resources\Account\SocialUserResource;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use Luckyweb\User\Models\SocialUser;

class SocialController extends ApiController
{
    /**
     * @param Guard $guard
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Guard $guard)
    {
        $socialUsers = SocialUser::where('user_id', $guard->user()->getKey())->get();

        return SocialUserResource::collection($socialUsers);
    }

    public function destroy(DestroyRequest $request)
    {
        try {
            /** @var SocialUser $socialItem */
            $socialItem = SocialUser::findOrFail($request->json('id'));
            $socialItem->delete();
        } catch (ModelNotFoundException $exception) {
        }

        return new Response(null, 204);
    }
}
