<?php

namespace App\Http\Controllers\Api\Catalog;

use App\Http\Controllers\Api\ApiController;
use App\Http\Resources\Catalog\ProductResource;
use LuckyWeb\MS\Models\Product;

class ProductController extends ApiController
{
    /**
     * @param $productId
     *
     * @return ProductResource
     */
    public function togetherCheaper($productId)
    {
        $product = Product::findOrFail($productId);

        $product->load('images', 'good_type_site', 'category', 'nid', 'together_cheaper', 'together_cheaper.products',
            'together_cheaper.products.images', 'together_cheaper.products.good_type_site',
            'together_cheaper.products.category', 'together_cheaper.products.nid');

        return new ProductResource($product);
    }
}
