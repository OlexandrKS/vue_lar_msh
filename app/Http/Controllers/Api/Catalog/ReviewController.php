<?php

namespace App\Http\Controllers\Api\Catalog;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\Review\StoreRequest;
use App\Http\Resources\Catalog\ReviewResource;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\Review;
use LuckyWeb\MS\Models\ReviewMessage;

class ReviewController extends ApiController
{
    /**
     * @param StoreRequest $request
     * @param Dispatcher   $events
     *
     * @throws \Throwable
     *
     * @return ReviewResource
     */
    public function store(StoreRequest $request, Dispatcher $events)
    {
        $review = DB::transaction(function () use ($request, $events) {
            $product = Product::first($request->json('product'));
            $review = new Review();

            if ($product && $product->reviewGroups->first()) {
                $review->product_group_id = $product->reviewGroups->first()->id;
            }

            $review->save();

            $messages = new ReviewMessage();
            $messages->text = $request->json('text');
            $messages->review_id = $review->id;
            $messages->save();

            $events->dispatch('luckyweb.ms.reviewCreated', [$review]);

            return $review;
        });

        return new ReviewResource($review);
    }

    public function onLike($reviewId)
    {
        $likes = Cookie::get('rw_likes', []);

        $review = Review::find(intval($reviewId));

        if (! empty($review)) {
            if (isset($likes[$review->id]) && 1 == $likes[$review->id]) {
                unset($likes[$review->id]);
                $review->decrement('likes');
            } elseif (isset($likes[$review->id]) && -1 == $likes[$review->id]) {
                $likes[$review->id] = 1;
                $review->decrement('dislikes');
                $review->increment('likes');
            } else {
                $likes[$review->id] = 1;
                $review->increment('likes');
            }
            Cookie::queue('rw_likes', $likes, 525600);
        }
        $likes = ['likes' => (int) $review->likes, 'dislikes' => (int) $review->dislikes];

        return response()->json($likes);
    }

    public function onDislike($reviewId)
    {
        $likes = Cookie::get('rw_likes', []);
        $review = Review::find(intval($reviewId));

        if (! empty($review)) {
            if (isset($likes[$review->id]) && -1 == $likes[$review->id]) {
                unset($likes[$review->id]);
                $review->decrement('dislikes');
            } elseif (isset($likes[$review->id]) && 1 == $likes[$review->id]) {
                $likes[$review->id] = -1;
                $review->decrement('likes');
                $review->increment('dislikes');
            } else {
                $likes[$review->id] = -1;
                $review->increment('dislikes');
            }
            Cookie::queue('rw_likes', $likes, 525600);
        }
        $likes = ['likes' => (int) $review->likes, 'dislikes' => (int) $review->dislikes];

        return response()->json($likes);
    }
}
