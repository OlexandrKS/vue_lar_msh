<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use LuckyWeb\MS\Models\Vacancy;
use LuckyWeb\MS\Models\VacancyRequest;

class EmployeeController extends ApiController
{
    /**
     * @param $city_id
     *
     * @return mixed
     */
    public function index($city_id)
    {
        $vacancies = Vacancy::activated()->whereHas('cities', function ($query) use ($city_id) {
            $query->where('id', $city_id);
        })->get();

        return $vacancies;
    }

    /**
     * @param Request $request
     *
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {
            $newVacancy = new VacancyRequest();
            $newVacancy->fill($request->toArray());
            $newVacancy->saveOrFail();

            Event::fire('luckyweb.ms.vacancyRequestCreated', $request);

            return $newVacancy;
        });
    }
}
