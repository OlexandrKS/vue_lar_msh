<?php

namespace App\Http\Controllers\Api\Accounting;

use App\Domain\Entities\Accounting\CartItem;
use App\Domain\Entities\Accounting\Order;
use App\Domain\Enums\Accounting\OrderDeliveryType;
use App\Domain\Enums\Accounting\OrderPaymentMethod;
use App\Domain\Process\Checkout;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\Accounting\Order\CalculationRequest;
use App\Http\Requests\Api\Accounting\Order\CheckUserExistsRequest;
use App\Http\Requests\Api\Accounting\Order\StoreRequest;
use App\Http\Resources\Accounting\OrderCalculationResource;
use App\Http\Resources\Accounting\OrderResource;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Cache\Repository as Cache;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use LuckyWeb\User\Models\User;

class OrderController extends ApiController
{
    /**
     * OrderController constructor.
     */
    public function __construct()
    {
        $this->middleware(['guest', 'throttle:6,1'])->only('checkUserExists');
    }

    /**
     * @param Guard $guard
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Guard $guard)
    {
        $orders = Order::where('user_id', $guard->id())
            ->with('items')
            ->get();

        return OrderResource::collection($orders);
    }

    /**
     * @param StoreRequest $request
     * @param Checkout     $checkout
     *
     * @throws \Throwable
     *
     * @return \App\Http\Resources\Accounting\OrderResource
     */
    public function store(StoreRequest $request, Checkout $checkout)
    {
        $order = DB::transaction(function () use ($checkout, $request) {
            $items = hydrate(CartItem::class, $request->json('items'));
            $accountData = $request->json('account');
            $deliveryData = $request->json('deliveryData');
            $paymentData = $request->json('paymentData');
            $promotions = $request->json('promotions', []);
            $comment = $request->json('comment');

            return $checkout->fromRequestData($items, $accountData, $paymentData, $deliveryData, $promotions, $comment);
        });

        return new OrderResource($order);
    }

    /**
     * @param CalculationRequest $request
     * @param Checkout           $checkout
     *
     * @return OrderCalculationResource
     */
    public function calculate(CalculationRequest $request, Checkout $checkout)
    {
        $items = hydrate(CartItem::class, $request->json('items'));
        $promotions = $request->json('promotions', []);
        $paymentMethod = $request->json('paymentMethod', OrderPaymentMethod::CARD_PAY);
        $hasAssembling = $request->json('assembling', true);
        $deliveryType = $request->json('deliveryType', OrderDeliveryType::ADDRESS);

        return new OrderCalculationResource(
            $checkout->calculate($items, $promotions, $request->user(), $paymentMethod, $hasAssembling, $deliveryType)
        );
    }

    /**
     * @param CheckUserExistsRequest $request
     * @param Cache                  $cache
     *
     * @return JsonResponse
     */
    public function checkUserExists(CheckUserExistsRequest $request, Cache $cache)
    {
        $query = $request->json('query');

        $isExists = $cache->remember('order_check_user_exists_'.md5($query), 2, function () use ($query) {
            return User::query()
                ->where('email', $query)
                ->orWhere('phone', $query)
                ->exists();
        });

        return new JsonResponse([
            'status' => $isExists,
        ]);
    }
}
