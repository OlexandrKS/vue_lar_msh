<?php

namespace App\Http\Controllers\Api\Accounting;

use App\Domain\Entities\Accounting\FavoriteList;
use App\Domain\Entities\Accounting\FavoriteListItem;
use App\Http\Requests\Api\Accounting\FavoriteList\DestroyRequest;
use App\Http\Requests\Api\Accounting\FavoriteList\StoreRequest;
use App\Http\Requests\Api\Accounting\FavoriteList\SyncRequest;
use App\Http\Resources\Accounting\FavoriteListItemResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;

class FavoriteListController
{
    /**
     * @param StoreRequest $request
     * @param FavoriteList $favoriteList
     *
     * @return FavoriteListItemResource
     */
    public function store(StoreRequest $request, FavoriteList $favoriteList)
    {
        $offerId = $request->json('offerId');
        $favoriteListItem = $favoriteList->addItem($offerId);

        return new FavoriteListItemResource($favoriteListItem);
    }

    /**
     * @param DestroyRequest $request
     * @param $offerId
     *
     * @return Response
     */
    public function remove(DestroyRequest $request, $offerId)
    {
        try {
            FavoriteListItem::currentUser()
                ->where('offer_id', $offerId)
                ->delete();
        } catch (ModelNotFoundException $exception) {
        }

        return new Response(null, 204);
    }

    /**
     * @param SyncRequest  $request
     * @param FavoriteList $favList
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(SyncRequest $request, FavoriteList $favList)
    {
        return FavoriteListItemResource::collection($favList->getItems());
    }
}
