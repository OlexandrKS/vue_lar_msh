<?php

namespace App\Http\Controllers\Api\Accounting;

use App\Domain\Entities\Accounting\Cart;
use App\Domain\Entities\Accounting\CartItem;
use App\Domain\Entities\Catalog\Offer;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\Accounting\Cart\DestroyRequest;
use App\Http\Requests\Api\Accounting\Cart\StoreRequest;
use App\Http\Requests\Api\Accounting\Cart\SyncRequest;
use App\Http\Requests\Api\Accounting\Cart\UpdateRequest;
use App\Http\Resources\Accounting\CartItemResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;

class CartController extends ApiController
{
    /**
     * {@inheritdoc}
     */
    public function __construct()
    {
        $this->middleware('auth')->only(['sync']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Cart $cart
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Cart $cart)
    {
        return CartItemResource::collection(
            $cart->getItems()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @param Cart         $cart
     *
     * @return CartItemResource
     */
    public function store(StoreRequest $request, Cart $cart)
    {
        $json = $request->json('offer');

        $offer = array_key_exists('type', $json) ?
            hydrate(Offer::class, $json)->first() :
            Offer::findOrFail(array_get($json, 'id'));
        $quantity = $request->json('quantity', 0);

        $cartItem = $cart->addItem($offer, $quantity);

        return new CartItemResource($cartItem);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param Cart          $cart
     *
     * @return \App\Http\Resources\Accounting\CartItemResource
     */
    public function update(UpdateRequest $request, Cart $cart)
    {
        /** @var CartItem $cartItem */
        $cartItem = CartItem::find($request->json('id'));

        if (is_null($cartItem)) {
            $cartItem = hydrate(CartItem::class, $request->json()->all())->first();
        }

        $updatedCartItem = $cart->changeQuantity($cartItem, $request->json('quantity'));

        return new CartItemResource($updatedCartItem);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyRequest $request
     * @param Cart           $cart
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy(DestroyRequest $request, Cart $cart)
    {
        try {
            /** @var CartItem $cartItem */
            $cartItem = CartItem::findOrFail($request->json('id'));

            $cart->deleteItem($cartItem);
        } catch (ModelNotFoundException $exception) {
        }

        return new Response(null, 204);
    }

    /**
     * @param SyncRequest $request
     * @param Cart        $cart
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function sync(SyncRequest $request, Cart $cart)
    {
        $items = hydrate(CartItem::class, $request->json('items', []));
        $synced = $cart->syncItems($items);

        return CartItemResource::collection($synced);
    }
}
