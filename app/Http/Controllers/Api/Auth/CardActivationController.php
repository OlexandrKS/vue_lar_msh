<?php

namespace App\Http\Controllers\Api\Auth;

use App\Domain\Process\User\CardActivation;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CartActivation\ActivationRequest;
use App\Http\Requests\Api\CartActivation\AttemptRequest;
use App\Http\Resources\Account\UserResource;
use Illuminate\Http\JsonResponse;

class CardActivationController extends Controller
{
    /**
     * @param \App\Http\Requests\Api\CartActivation\ActivationRequest $request
     * @param CardActivation                                          $cardActivation
     *
     * @return JsonResponse
     */
    public function attempt(ActivationRequest $request, CardActivation $cardActivation)
    {
        $cardNumber = $request->json('card_code');
        $cardPIN = $request->json('card_pin');

        $sign = $cardActivation->attempt($cardNumber, $cardPIN);

        return new JsonResponse([
            'data' => [
                'sign' => $sign,
            ],
        ]);
    }

    /**
     * @param AttemptRequest $request
     * @param CardActivation $cardActivation
     *
     * @return UserResource
     */
    public function activate(AttemptRequest $request, CardActivation $cardActivation)
    {
        $credentials = [
            'password' => $request->json('password'),
        ];

        $user = $cardActivation->handle($credentials, $request->json('sign'));

        return new UserResource($user);
    }
}
