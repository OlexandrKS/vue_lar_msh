<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\ServiceLayer\Sentry\Sentry;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Laravel\Socialite\Facades\Socialite;
use Luckyweb\User\Models\SocialUser;
use October\Rain\Support\Facades\Flash;

class SocialAuthController extends Controller
{
    /**
     * @var array
     */
    protected $availableSocialProviders = [
        'facebook', 'google', 'vkontakte',
    ];

    /**
     * Redirect the user to the provider authentication page.
     *
     * @param string $code social provider code
     *
     * @return Response
     */
    public function redirectToProvider($code)
    {
        if (in_array($code, $this->availableSocialProviders)) {
            return Socialite::driver($code)->redirect();
        }

        throw new \RuntimeException();
    }

    /**
     * @param Guard|StatefulGuard $guard
     * @param string              $code  todo refactor
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleProviderCallback(Guard $guard, string $code)
    {
        try {
            if (in_array($code, $this->availableSocialProviders)) {
                /** @var \SocialiteProviders\Manager\OAuth2\User $socialUser */
                $socialUser = Socialite::driver($code)->user();

                if ($guard->guest()) {
                    $social = SocialUser::where('provider_id', $code)
                        ->where('social_id', $socialUser->getId())
                        ->firstOrFail();

                    $userId = $social->user_id;

                    if ($userId) {
                        $guard->loginUsingId($userId, true);

                        return Redirect::to('/account');
                    }

                    Flash::error('К этому профилю соцсети не найден ни один пользователь. Зарегистрируйтесь и привяжите профиль к аккаунту МебельШара');

                    return Redirect::to('/authentication');
                }

                $socialUserId = $socialUser->getId();
                $userId = $guard->id();

                // TODO move to user model
                if ($userId) {
                    $social = new SocialUser();
                    $social->user_id = $userId;
                    $social->social_id = $socialUserId;
                    $social->provider_id = $code;
                    $social->name = $socialUser->name;
                    $social->email = $socialUser->email;
                    $social->last_name = $socialUser->nickname;
                    $social->saveOrFail();

                    Flash::success('Ваш профиль '.$code.' успешно привязан!');

                    return Redirect::to('/account');
                }
            }
        } catch (\Throwable $e) {
            Sentry::captureException($e);
        } finally {
            Flash::error('Что-то пошло не так! Попробуйте войти через другой сервис или форму авторизации/регистрации');

            return Redirect::to('/authentication');
        }
    }
}
