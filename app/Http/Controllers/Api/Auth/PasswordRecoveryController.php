<?php

namespace App\Http\Controllers\Api\Auth;

use App\Domain\Process\User\PasswordRecovery;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\PasswordRecovery\AttemptRequest;
use App\Http\Requests\Api\PasswordRecovery\RecoveryRequest;
use App\Http\Requests\Api\PasswordRecovery\ValidationRequest;
use App\Http\Resources\Account\UserResource;
use App\ServiceLayer\SmsSender\ThrottleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class PasswordRecoveryController extends ApiController
{
    /**
     * @param AttemptRequest   $request
     * @param PasswordRecovery $passwordRecovery
     *
     * @return JsonResponse
     */
    public function attempt(AttemptRequest $request, PasswordRecovery $passwordRecovery)
    {
        $phoneNumber = $request->json('phone');

        try {
            $sign = $passwordRecovery->attempt($phoneNumber);
        } catch (ThrottleException $e) {
            throw ValidationException::withMessages([
                'phone' => ['Следующее сообщение можно отправить через 2 минуты'],
            ]);
        }

        return new JsonResponse([
            'data' => [
                'sign' => $sign,
            ],
        ]);
    }

    /**
     * @param ValidationRequest $request
     * @param PasswordRecovery  $passwordRecovery
     *
     * @return JsonResponse
     */
    public function validateCode(ValidationRequest $request, PasswordRecovery $passwordRecovery)
    {
        try {
            if ($passwordRecovery->validate($request->json('code'), $request->json('sign'))) {
                return new JsonResponse([
                    'data' => [
                        'sign' => $request->json('sign'),
                    ],
                ]);
            }

            throw new \Exception('Введенный код не совпадает');
        } catch (\Exception $e) {
            throw ValidationException::withMessages([
                'code' => [$e->getMessage()],
            ]);
        }
    }

    /**
     * @param RecoveryRequest  $request
     * @param PasswordRecovery $passwordRecovery
     *
     * @return UserResource
     */
    public function recovery(RecoveryRequest $request, PasswordRecovery $passwordRecovery)
    {
        $credentials = [
            'password' => $request->json('password'),
        ];

        $user = $passwordRecovery->handle($credentials, $request->json('sign'));

        return new UserResource($user);
    }
}
