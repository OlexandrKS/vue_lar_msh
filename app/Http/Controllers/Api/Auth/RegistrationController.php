<?php

namespace App\Http\Controllers\Api\Auth;

use App\Domain\Process\User\Registration;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\Registration\AttemptRequest;
use App\Http\Requests\Api\Registration\RegistrationRequest;
use App\Http\Requests\Api\Registration\ValidationRequest;
use App\Http\Resources\Account\UserResource;
use App\ServiceLayer\SmsSender\ThrottleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class RegistrationController extends ApiController
{
    /**
     * @param AttemptRequest $request
     * @param Registration   $registration
     *
     * @return JsonResponse
     */
    public function attempt(AttemptRequest $request, Registration $registration)
    {
        $phoneNumber = $request->json('phone');
        try {
            $sign = $registration->attempt($phoneNumber);
        } catch (ThrottleException $e) {
            $message = 'Следующее сообщение можно отправить через 2 минуты';
            if (420 == $e->getCode()) {
                $message = 'Слишком много отправок сообщений. Отправка заблокирована на сутки';
            }
            throw ValidationException::withMessages([
                'phone' => [$message],
            ]);
        }

        return new JsonResponse([
            'data' => [
                'sign' => $sign,
            ],
        ]);
    }

    /**
     * @param ValidationRequest $request
     * @param Registration      $registration
     *
     * @return JsonResponse
     */
    public function validateCode(ValidationRequest $request, Registration $registration)
    {
        try {
            if ($registration->validate($request->json('code'), $request->json('sign'))) {
                return new JsonResponse([
                    'data' => [
                        'sign' => $request->json('sign'),
                    ],
                ]);
            }

            throw new \Exception('Введенный код не совпадает');
        } catch (\Exception $e) {
            throw ValidationException::withMessages([
                'code' => [$e->getMessage()],
            ]);
        }
    }

    /**
     * @param RegistrationRequest $request
     * @param Registration        $registration
     *
     * @throws \Throwable
     *
     * @return UserResource
     */
    public function register(RegistrationRequest $request, Registration $registration)
    {
        $user = DB::transaction(function () use ($registration, $request) {
            $credentials = [
                'name'     => $request->json('name'),
                'email'    => $request->json('email'),
                'password' => $request->json('password'),
            ];

            return $registration->handle($credentials, $request->json('sign'));
        });

        return new UserResource($user);
    }
}
