<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\Auth\AuthenticationRequest;
use App\Http\Resources\Account\UserResource;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class AuthController extends ApiController
{
    /**
     * @param \App\Http\Requests\Api\Auth\AuthenticationRequest $request
     * @param Guard|StatefulGuard                               $guard
     *
     * @return JsonResponse | UserResource
     */
    public function signIn(AuthenticationRequest $request, Guard $guard)
    {
        $login = $request->json('login');
        $loginField = strpos($login, '@') ? 'email' : 'phone';

        $credentials = [
            $loginField => $login,
            'password'  => $request->json('password'),
        ];

        if ($guard->attempt($credentials, true)) {
            return new UserResource($guard->user());
        }

        throw ValidationException::withMessages([
            'login' => 'Пользователь с такими данными не найден',
        ]);
    }

    /**
     * @param Guard|StatefulGuard $guard
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function logout(Guard $guard)
    {
        $guard->logout();

        return response(null, 205);
    }
}
