<?php

namespace App\Http\Controllers\Api;

use App\Domain\Process\Popup;
use App\Http\Requests\Api\PopUp\SubscribeRequest;
use App\Http\Resources\PopupResource;

class PopupController extends ApiController
{
    /**
     * @param string $popupIds
     * @param Popup  $popup
     *
     * @return PopupResource|null
     */
    public function index(string $popupIds, Popup $popup)
    {
        $popup = $popup->handle($popupIds);

        return ($popup) ? new PopupResource($popup) : null;
    }

    /**
     * @param SubscribeRequest $request
     * @param Popup            $popup
     *
     * @return string
     */
    public function send(SubscribeRequest $request, Popup $popup)
    {
        return $popup->emailActivatePromoCode($request->all());
    }
}
