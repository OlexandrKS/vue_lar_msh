<?php

namespace App\Http\Controllers\BackendApi;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends BackendController
{
    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')
            ->only('logout', 'user');

        $this->middleware('guest')
            ->only('login');
    }

    /**
     * @param Request             $request
     * @param Guard|StatefulGuard $guard
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response|null
     */
    public function login(Request $request, Guard $guard)
    {
        $credentials = $request->all();

        if (! $token = $guard->attempt($credentials)) {
            return response()->json([
                'error' => trans('auth.failed'),
            ], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * @param Guard $guard
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function logout(Guard $guard)
    {
        $guard->logout();

        return response(null, 204);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        try {
            return $this->respondWithToken(auth()->refresh());
        } catch (JWTException $exception) {
            return response()->json([
                'error' => $exception->getMessage(),
            ], 401);
        }
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function user(Request $request)
    {
        return [
            'data' => $request->user()->toArray(),
        ];
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth()->factory()->getTTL() * 60,
        ]);
    }
}
