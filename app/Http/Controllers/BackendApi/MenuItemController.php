<?php

namespace App\Http\Controllers\BackendApi;

use App\Backend\Entities\MenuItem;
use App\Http\Resources\Backend\MenuItemResource;
use Illuminate\Http\Request;

class MenuItemController extends BackendController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $collection = MenuItem::filter($request->all())
            ->with('icon')
            ->get();

        return MenuItemResource::collection($collection);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @throws \Throwable
     *
     * @return MenuItemResource
     */
    public function store(Request $request)
    {
        $item = new MenuItem($request->all());
        $item->saveOrFail();
        $item->load('icon');

        return new MenuItemResource($item);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return MenuItemResource
     */
    public function show($id)
    {
        $item = MenuItem::findOrFail($id);
        $item->load('icon');

        return new MenuItemResource($item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @throws \Throwable
     *
     * @return MenuItemResource
     */
    public function update(Request $request, $id)
    {
        $item = MenuItem::findOrFail($id);
        $item->fill($request->all());
        $item->saveOrFail();
        $item->load('icon');

        return new MenuItemResource($item);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            MenuItem::query()
                ->where('id', $id)
                ->delete();
        } catch (\Throwable | \Exception $e) {
        }

        return response(null, 204);
    }
}
