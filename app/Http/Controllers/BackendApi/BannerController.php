<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22.04.2019
 * Time: 17:39.
 */

namespace App\Http\Controllers\BackendApi;

use App\Backend\Entities\Banner;
use App\Http\Resources\Backend\BannerResource;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Throwable;

class BannerController extends BackendController
{
    /**
     * @param Request $request
     *
     * @return AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $banners = Banner::all();

        return BannerResource::collection($banners);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function show($id)
    {
        $banner = Banner::where('id', '=', $id)
            ->firstOrFail();

        return new BannerResource($banner);
    }

    /**
     * @param Request $request
     *
     * @throws Throwable
     *
     * @return mixed
     */
    public function store(Request $request)
    {
        return DB::transaction(function () use ($request) {
            /** @var Banner $page */
            $banner = new Banner();
            $banner->name = $request->json('name');
            $banner->color = $request->json('color');
            $banner->image = $request->json('image');
            $banner->type = $request->json('type');
            $banner->properties = $request->json('properties');
            $banner->saveOrFail();

            return new BannerResource($banner);
        });
    }

    /**
     * @param Request $request
     * @param $id
     *
     * @throws Throwable
     *
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        return DB::transaction(function () use ($request, $id) {
            /** @var Banner $page */
            $banner = Banner::findOrFail($id);
            $banner->name = $request->json('name');
            $banner->color = $request->json('color');
            $banner->image = $request->json('image');
            $banner->type = $request->json('type');
            $banner->properties = $request->json('properties');
            $banner->saveOrFail();

            return new BannerResource($banner);
        });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try {
            Banner::where('id', $id)
                ->delete();
        } catch (Exception $e) {
        }

        return new Response(null, 204);
    }
}
