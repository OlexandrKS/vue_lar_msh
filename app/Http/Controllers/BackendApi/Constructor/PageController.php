<?php

namespace App\Http\Controllers\BackendApi\Constructor;

use App\Backend\Entities\Constructor\ComponentContent;
use App\Backend\Entities\Constructor\Page;
use App\Contracts\ConstructorManager;
use App\Http\Controllers\BackendApi\BackendController;
use App\Http\Resources\Backend\ConstructorPage\PageResource;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Throwable;

class PageController extends BackendController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $collection = Page::filter($request->all())
            ->with(['components'])
            ->get();

        return PageResource::collection($collection);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return PageResource
     */
    public function show($id)
    {
        $page = Page::where('id', '=', $id)
            ->with(['components' => function ($q) {
                $q->orderBy('position', 'ASC');
            }])->firstOrFail();

        return new PageResource($page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @throws Throwable
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return DB::transaction(function () use ($request) {
            $data = array_except($request->input('page'), ['components']);

            $page = new Page($data);
            $page->saveOrFail();

            if ($contents = $request->input('page.components')) {
                foreach ($contents as $i => $item) {
                    $pageContents = new ComponentContent();
                    $pageContents->page = $page;
                    $pageContents->component = $item['component'];
                    $pageContents->position = $i;
                    $pageContents->contents = $item['contents'];
                    $pageContents->saveOrFail();

                    $page->components->push($pageContents);
                }
            }

            return new PageResource($page);
        });
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @throws Throwable
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return DB::transaction(function () use ($request, $id) {
            ComponentContent::where('page_id', $id)
                ->delete();

            $data = array_except($request->input('page'), ['components']);

            /** @var Page $page */
            $page = Page::findOrFail($id);
            $page->fill($data);
            $page->saveOrFail();

            if ($contents = $request->input('page.components')) {
                foreach ($contents as $i => $item) {
                    $pageContents = new ComponentContent();
                    $pageContents->page = $page;
                    $pageContents->component = $item['component'];
                    $pageContents->position = $i;
                    $pageContents->contents = $item['contents'];
                    $pageContents->saveOrFail();

                    $page->components->push($pageContents);
                }
            }

            return new PageResource($page);
        });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try {
            Page::where('id', $id)
                ->delete();
        } catch (Exception $e) {
        }

        return new Response(null, 204);
    }

    /**
     * @param ConstructorManager $manager
     * @param string             $component
     * @param string             $action
     *
     * @return mixed
     */
    public function component(ConstructorManager $manager, string $component, string $action)
    {
        $componentObject = $manager->createComponent($component);

        if (method_exists($componentObject, $action)) {
            return app()->call([$componentObject, $action]);
        }

        throw new \InvalidArgumentException();
    }

    /**
     * @param ConstructorManager $manager
     * @param string             $type
     * @param string             $action
     *
     * @return mixed
     */
    public function page(ConstructorManager $manager, string $type, string $action)
    {
        $page = $manager->createPage($type);

        if (method_exists($page, $action)) {
            return app()->call([$page, $action]);
        }

        throw new \InvalidArgumentException();
    }
}
