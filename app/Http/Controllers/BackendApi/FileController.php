<?php

namespace App\Http\Controllers\BackendApi;

use App\Backend\Entities\File;
use App\Http\Resources\Backend\FileResource;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Throwable;

class FileController extends BackendController
{
    /**
     * @param Request $request
     *
     * @return
     */
    public function index(Request $request)
    {
        $files = File::all();

        return FileResource::collection($files);
    }

    /**
     * @param $id
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     *
     * @return mixed
     */
    public function show($id)
    {
        $fileModel = File::where('id', '=', $id)
            ->firstOrFail();

        return new FileResource($fileModel);
    }

    /**
     * @param Request $request
     *
     * @throws Throwable
     *
     * @return mixed
     */
    public function store(Request $request)
    {
        return DB::transaction(function () use ($request) {
            /** @var File $page */
            $file = new File();
            $file->name = $request->file('file')->getClientOriginalName();
            $file->saveOrFail();
            $path = $request->file('file')->storeAs('',
                $request->file('file')->getClientOriginalName(), 'backend');

            return new FileResource($file);
        });
    }

    /**
     * @param Request $request
     * @param $id
     *
     * @throws Throwable
     *
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        return DB::transaction(function () use ($request, $id) {
            /** @var File $page */
            $file = File::findOrFail($id);
            $file->name = $request->json('filename');
            $file->saveOrFail();

            return new FileResource($file);
        });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function deleteItems(Request $request)
    {
        $files = $request->json('files');
        try {
            if (File::whereIn('id', array_pluck($files, 'id'))->delete()) {
                Storage::disk('backend')->delete(array_pluck($files, 'name'));
            }
        } catch (Exception $e) {
        }

        return new Response(null, 204);
    }
}
