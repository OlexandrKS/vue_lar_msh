<?php

namespace App\Http\Controllers\BackendApi;

use App\Backend\Entities\SvgIcon;
use App\Http\Resources\Backend\SvgIconResource;
use Illuminate\Http\Request;

class SvgIconController extends BackendController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $collection = SvgIcon::filter($request->all())
            ->get();

        return SvgIconResource::collection($collection);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @throws \Throwable
     *
     * @return SvgIconResource
     */
    public function store(Request $request)
    {
        $icon = new SvgIcon($request->all());
        $icon->saveOrFail();

        return new SvgIconResource($icon);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return SvgIconResource
     */
    public function show($id)
    {
        $icon = SvgIcon::findOrFail($id);

        return new SvgIconResource($icon);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @throws \Throwable
     *
     * @return SvgIconResource
     */
    public function update(Request $request, $id)
    {
        $icon = SvgIcon::findOrFail($id);
        $icon->fill($request->all());
        $icon->saveOrFail();

        return new SvgIconResource($icon);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            SvgIcon::query()
                ->where('id', $id)
                ->delete();
        } catch (\Throwable | \Exception $e) {
        }

        return response(null, 204);
    }
}
