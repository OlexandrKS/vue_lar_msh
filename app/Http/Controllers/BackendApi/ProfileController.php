<?php

namespace App\Http\Controllers\BackendApi;

use Backend\Models\User;
use Illuminate\Http\Request;

class ProfileController extends BackendController
{
    /**
     * @param Request $request
     * @return User
     */
    public function update(Request $request)
    {
        /** @var User $user */
        $user = $request->user()->getBackendUser();
        $credentials = $request->only('password', 'password_confirmation');

        $user->fill($credentials);
        $user->save();

        return $user;
    }
}
