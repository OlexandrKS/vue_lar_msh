<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Config\Repository;

class DefaultGuardSwitch
{
    /**
     * @var Repository
     */
    private $config;

    /**
     * DefaultGuardSwitch constructor.
     *
     * @param Repository $config
     */
    public function __construct(Repository $config)
    {
        $this->config = $config;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @param string|null              $newGuard
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $newGuard = null)
    {
        if (in_array($newGuard, array_keys($this->config->get('auth.guards')))) {
            $this->config->set('auth.defaults.guard', $newGuard);
            app('auth')->setDefaultDriver($newGuard);
        }

        return $next($request);
    }
}
