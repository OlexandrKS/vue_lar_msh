<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class JsonRequest extends FormRequest
{
    /**
     * {@inheritdoc}
     */
    protected function validationData()
    {
        return $this->json()->all();
    }
}
