<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\JsonRequest;
use App\Rules\PhoneOrEmail;

class AuthenticationRequest extends JsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login'    => ['required', new PhoneOrEmail()],
            'password' => 'required|between:4,255',
        ];
    }
}
