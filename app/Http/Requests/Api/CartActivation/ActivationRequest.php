<?php

namespace App\Http\Requests\Api\CartActivation;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;

class ActivationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'card_code' => 'required|digits_between:1,'.config('luckyweb.ms::account_card_length'), // todo laravel
            'card_pin'  => 'required|digits:4',
        ];
    }
}
