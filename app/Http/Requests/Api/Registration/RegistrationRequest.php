<?php

namespace App\Http\Requests\Api\Registration;

use App\Http\Requests\JsonRequest;

class RegistrationRequest extends JsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|between:4,255|confirmed',
            'name'     => 'required',
            'email'    => 'required|unique:users',
            'sign'     => 'required',
        ];
    }
}
