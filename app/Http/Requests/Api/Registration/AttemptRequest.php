<?php

namespace App\Http\Requests\Api\Registration;

use App\Http\Requests\JsonRequest;

class AttemptRequest extends JsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => ['required', 'regex:/^7[0-9]{10}$/', 'unique:users,phone'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function messages()
    {
        return [
            'phone.unique' => 'Аккаунт с таким номером уже существует.',
        ];
    }
}
