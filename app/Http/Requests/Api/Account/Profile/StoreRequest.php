<?php

namespace App\Http\Requests\Api\Account\Profile;

use App\Http\Requests\JsonRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends JsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required|min:3',
            'last_name'  => 'required|min:3',
            'other_name' => 'min:3',
            'gender_id'  => Rule::in([1, 2]),
            'birth_date' => 'date',
        ];
    }
}
