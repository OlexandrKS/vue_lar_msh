<?php

namespace App\Http\Requests\Api\PopUp;

use App\Http\Requests\JsonRequest;

class SubscribeRequest extends JsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'    => 'required',
            'email' => 'required|email',
        ];
    }
}
