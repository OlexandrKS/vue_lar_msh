<?php

namespace App\Http\Requests\Api\Accounting\Order;

use App\Domain\Enums\Accounting\OrderDeliveryType;
use App\Domain\Enums\Accounting\OrderPaymentMethod;
use App\Http\Requests\JsonRequest;
use Illuminate\Validation\Rule;

class CalculationRequest extends JsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'promotions'     => ['array'],
            'items'          => ['array'],
            'paymentMethod'  => [Rule::in(OrderPaymentMethod::values())],
            'hasAssembling'  => ['bool'],
            'deliveryType'   => [Rule::in(OrderDeliveryType::values())],
        ];
    }
}
