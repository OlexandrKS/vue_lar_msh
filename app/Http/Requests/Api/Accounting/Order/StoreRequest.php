<?php

namespace App\Http\Requests\Api\Accounting\Order;

use App\Domain\Enums\Accounting\OrderDeliveryType;
use App\Domain\Enums\Accounting\OrderPaymentMethod;
use App\Http\Requests\JsonRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends JsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'items'                  => ['required', 'array'],
            'account'                => ['required', 'array'],
            'promotions'             => ['array'],
            'paymentData'            => ['required', 'array'],
            'paymentData.method'     => ['required', Rule::in(OrderPaymentMethod::values())],
            'paymentData.assembling' => ['bool'],
            'deliveryData'           => ['required', 'array'],
            'deliveryData.type'      => ['required', Rule::in(OrderDeliveryType::values())],
            'deliveryData.address'   => ['required'],
        ];
    }
}
