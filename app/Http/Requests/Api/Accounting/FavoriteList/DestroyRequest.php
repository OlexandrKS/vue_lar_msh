<?php

namespace App\Http\Requests\Api\Accounting\FavoriteList;

use App\Http\Requests\JsonRequest;

class DestroyRequest extends JsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
