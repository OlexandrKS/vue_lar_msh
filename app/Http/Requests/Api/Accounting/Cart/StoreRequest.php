<?php

namespace App\Http\Requests\Api\Accounting\Cart;

use App\Domain\Enums\Catalog\OfferType;
use App\Http\Requests\JsonRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends JsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantity'   => ['numeric', 'min:0', 'max:10'],
            'offer.id'   => ['required_without:offer.type', 'exists:offers,id'],
            'offer.type' => ['required_without:offer.id', Rule::in(OfferType::values())],
        ];
    }
}
