<?php

namespace App\Http\Requests\Api\Accounting\Cart;

use App\Http\Requests\JsonRequest;

class UpdateRequest extends JsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'       => ['required'],
            'quantity' => ['required', 'numeric', 'min:1', 'max:10'],
        ];
    }
}
