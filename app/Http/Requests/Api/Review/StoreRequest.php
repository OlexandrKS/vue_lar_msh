<?php

namespace App\Http\Requests\Api\Review;

use App\Http\Requests\JsonRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends JsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product'        => [Rule::exists('luckyweb_ms_products', 'id')],
            'contractNumber' => 'required',
            'email'          => ['required', 'email'],
            'name'           => 'required',
            'phone'          => 'required',
            'text'           => 'required',
        ];
    }
}
