<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PhoneOrEmail implements Rule
{
    /**
     * Create a new rule instance.
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return preg_match('/^7[0-9]{10}$/', $value) ||
             preg_match('/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/i', $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Введите правильные данные';
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return 'phone-or-email';
    }
}
