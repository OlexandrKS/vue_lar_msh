<?php

namespace App\ServiceLayer\Promotions;

use App\Contracts\PromotionManager as PromotionManagerContract;
use App\Domain\Entities\Accounting\Order\Calculation;
use App\ServiceLayer\PriceCalculator\CalculationRequest;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Container\Container;
use Illuminate\Support\Arr;

class PromotionManager implements PromotionManagerContract
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var array
     */
    private $promotions;

    /**
     * PromotionManager constructor.
     *
     * @param Container $container
     * @param array     $promotions
     */
    public function __construct(Container $container, array $promotions = [])
    {
        $this->container = $container;
        $this->promotions = $promotions;
    }

    /**
     * @param string                  $promotionName
     * @param CalculationRequest|null $request
     * @param Authenticatable|null    $user
     * @param array                   $params
     *
     * @throws \Exception
     *
     * @return bool
     */
    public function canUse(string $promotionName, CalculationRequest $request = null,
                           Authenticatable $user = null, array $params = []): bool
    {
        /** @var Promotion $promotion */
        $promotion = $this->getByName($promotionName);

        return $promotion->canUse($request, $user, $params);
    }

    /**
     * @param string               $promotionName
     * @param Calculation|null     $calculation
     * @param Authenticatable|null $user
     * @param array                $params
     *
     * @throws \Exception
     *
     * @return bool
     */
    public function apply(string $promotionName, Calculation $calculation = null,
                          Authenticatable $user = null, array $params = []): bool
    {
        /** @var Promotion $promotion */
        $promotion = $this->getByName($promotionName);

        return $promotion->apply($calculation, $user, $params);
    }

    /**
     * @param string $promotionName
     *
     * @throws \Exception
     *
     * @return Promotion
     */
    public function getByName(string $promotionName): Promotion
    {
        $className = $this->findImplementation($promotionName);

        return $this->container->make($className);
    }

    /**
     * @param string $name
     *
     * @throws \Exception
     *
     * @return string
     */
    private function findImplementation(string $name): string
    {
        $class = Arr::first($this->promotions, function ($item) use ($name) {
            return $item::name() === $name;
        });

        if (! $class) {
            throw new \Exception("Promotion with $name not found");
        }

        return $class;
    }
}
