<?php

namespace App\ServiceLayer\Promotions;

use App\Domain\Entities\Accounting\Order\Calculation;
use App\ServiceLayer\PriceCalculator\CalculationRequest;
use Illuminate\Contracts\Auth\Authenticatable;

interface Promotion
{
    const ANY_PROMOTION_USE = 'is_special';
    const DISCOUNT_VALUE = 'discount_value';

    /**
     * техническое название акции.
     *
     * @return string
     */
    public static function name(): string;

    /**
     * @param Calculation|null     $calculation
     * @param Authenticatable|null $user
     * @param array                $params
     *
     * @return bool
     */
    public function apply(Calculation $calculation = null, Authenticatable $user = null, array $params = []): bool;

    /**
     * @param CalculationRequest|null $request
     * @param Authenticatable|null    $user
     * @param array                   $params
     *
     * @return bool
     */
    public function canUse(CalculationRequest $request = null, Authenticatable $user = null, array $params = []): bool;
}
