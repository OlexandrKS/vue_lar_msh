<?php

namespace App\ServiceLayer\Importer;

use App\ServiceJobs\Importer\ImageSizeCalculate;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Contracts\Filesystem\Factory as FilesystemFactory;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Intervention\Image\ImageManager;
use LuckyWeb\MS\Models\Image;

class ImageTransformer
{
    /** @var int Обычная ширина, для мобильные версий */
    const PREVIEW_DEFAULT_WIDTH = 222;

    /** @var int Стандартная ширина изображения для минимизации */
    const MINIMIZED_WIDTH = 1780;

    /** @var bool Ресайзить изображения, для нового формата на странице товара */
    const PRODUCT_DETAIL_IMAGES = false;

    /**
     * @var ImageManager
     */
    private $imageManager;

    /**
     * @var \Illuminate\Contracts\Filesystem\Filesystem
     */
    private $filesystem;

    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * ImageTransformer constructor.
     *
     * @param ImageManager      $imageManager
     * @param FilesystemFactory $factory
     * @param Dispatcher        $dispatcher
     */
    public function __construct(ImageManager $imageManager, FilesystemFactory $factory, Dispatcher $dispatcher)
    {
        $this->imageManager = $imageManager;
        $this->filesystem = $factory->disk('images');
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param Image  $image
     * @param string $path
     * @param bool   $save
     *
     * @return bool
     */
    public function fillImageSize(Image $image, string $path, $save = true): bool
    {
        try {
            $content = $this->filesystem->get($path);
        } catch (FileNotFoundException $e) {
            return false;
        }

        $image->image_size = getimagesizefromstring($content);

        if ($save) {
            $image->save();
        }

        return true;
    }

    /**
     * @param Image $model
     *
     * @return bool
     */
    public function minimize(Image $model): bool
    {
        if ($model->minimized) {
            return true;
        }

        try {
            $content = $this->filesystem->get($model->local_name);
        } catch (FileNotFoundException $e) {
            return false;
        }

        $image = $this->imageManager->make($content);
        $image->backup();

        if ($model->product_id && static::PRODUCT_DETAIL_IMAGES) {
            // TODO get height
        } elseif ($image->width() > static::MINIMIZED_WIDTH + 200) {
            $image->widen(static::MINIMIZED_WIDTH);

            $content = $image->psrResponse()->getBody()->getContents();
            $this->filesystem->put("minimized/{$model->local_name}", $content);

            // Посчитать размер изображения
            $this->dispatcher->dispatch(new ImageSizeCalculate($model));
        }

        if ($model->product_id /* && $model->assignment !== Image::ASSIGNMENT_COMMON */) {
            // reset image object to make preview image
            $image->reset();

            // Получаем ширину изображения и базы и делаем 2х
            $width = data_get($model, 'product.good_type_site.width', static::PREVIEW_DEFAULT_WIDTH) * 2;

            if (Image::ASSIGNMENT_PREVIEW_MOBILE === $model->assigment) {
                $width = static::PREVIEW_DEFAULT_WIDTH * 2;
            }

            $image->widen($width);

            // Fill image preview
            $content = $image->psrResponse()->getBody()->getContents();
            $this->filesystem->put("preview/{$model->local_name}", $content);

            // Посчитать размер изображения
            $this->dispatcher->dispatch(new ImageSizeCalculate($model, $model->storage_path_preview, 'image_preview_size'));
        }

        $image->destroy();
        $model->minimized = true;

        return $model->save();
    }
}
