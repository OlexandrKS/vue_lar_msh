<?php

namespace App\ServiceLayer\Importer;

use App\ServiceJobs\Importer\ImageUpload;
use App\ServiceLayer\ModelFactory\Factory as ModelFactory;
use Carbon\Carbon;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Contracts\Filesystem\Factory as FilesystemFactory;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\ConnectionInterface;
use League\Csv\Reader;
use League\Flysystem\FilesystemInterface;
use LuckyWeb\MS\Models\Image;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

class ImportManager
{
    /**
     * Table prefix.
     */
    const DB_TABLE_PREFIX = 'luckyweb_ms_import';

    /**
     * @var Filesystem
     */
    private $filesystemFactory;

    /**
     * @var ConnectionInterface
     */
    private $db;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $environment;

    /**
     * @var array
     */
    private $config;

    /**
     * @var ProgressBar|null
     */
    private $progressBar;

    /**
     * @var Dispatcher
     */
    private $queue;

    /**
     * @var ModelFactory
     */
    private $modelFactory;

    /**
     * ImportManager constructor.
     *
     * @param FilesystemFactory   $filesystemFactory
     * @param ConnectionInterface $db
     * @param LoggerInterface     $logger
     * @param Application         $application
     * @param Dispatcher          $dispatcher
     * @param ModelFactory        $modelFactory
     * @param array               $config
     */
    public function __construct(FilesystemFactory $filesystemFactory, ConnectionInterface $db, LoggerInterface $logger,
                                Application $application, Dispatcher $dispatcher, ModelFactory $modelFactory, array $config)
    {
        $this->filesystemFactory = $filesystemFactory;
        $this->db = $db;
        $this->logger = $logger;
        $this->config = $config;
        $this->environment = $application->environment();
        $this->queue = $dispatcher;
        $this->modelFactory = $modelFactory;

        ProgressBar::setFormatDefinition('importer', ' %message% -- %current%/%max%');
    }

    /**
     * @return string
     */
    public function getImportTablesPrefix()
    {
        return static::DB_TABLE_PREFIX;
    }

    /**
     * Import data from csv to files.
     *
     * @param OutputInterface|null $output
     *
     * @throws \Throwable
     */
    public function importData(OutputInterface $output = null)
    {
        if (! ini_get('auto_detect_line_endings')) {
            ini_set('auto_detect_line_endings', '1');
        }

        $dataStorage = $this->getImportDataFilesystem();

        $mappingCsv = array_get($this->config, 'mapping', []);
        $ownedSiteFlags = array_get($this->config, 'ownedSiteFlags', []);

        foreach ($mappingCsv as $entityName => $options) {
            try {
                // Read and convert csv
                $raw = $dataStorage->get("$entityName.csv");
                $converted = mb_convert_encoding($raw, 'utf-8', 'windows-1251');
            } catch (FileNotFoundException $e) {
                $this->exceptionOutput($e, $output);

                continue;
            }

            // Csv reader
            $reader = Reader::createFromString($converted);
            $reader->setDelimiter(';');
            $iterator = $reader->fetchAssoc();

            // Truncate old data
            $table = $this->db->table("{$this->getImportTablesPrefix()}_{$options['table']}");
            $table->truncate();

            $mapping = $options['fields'];
            $data = [];

            $this->startProgress($entityName, iterator_count($iterator), $output);

            $iterator->rewind();
            $rowCounter = 0;
            foreach ($iterator as $row) {
                $this->processProgress();
                $rowData = [];

                if (in_array('OwnedSite', array_keys($row)) && ! in_array($row['OwnedSite'], $ownedSiteFlags)) {
                    continue;
                }

                ++$rowCounter;

                foreach ($row as $field => $value) {
                    if ($key = array_get($mapping, $field)) {
                        $rowData[$key] = $value;
                    }
                }

                $data[] = $rowData;

                // Batch insert
                if ($rowCounter > 1000) {
                    $table->insert($data);
                    $data = [];
                    $rowCounter = 0;
                }
            }

            $table->insert($data);
        }
    }

    /**
     * @param OutputInterface|null $output
     */
    public function fillEntities(OutputInterface $output = null)
    {
        $this->modelFactory->fillEntities();
    }

    /**
     * @param OutputInterface|null $output
     *
     * @throws \Throwable
     */
    public function importImages(OutputInterface $output = null)
    {
        /** @var FilesystemInterface $importDriver */
        $importDriver = $this->getImportImagesFilesystem()->getDriver();
        /** @var FilesystemInterface $storageDriver */
        $storageDriver = $this->getImagesFilesystem()->getDriver();

        $remoteFiles = $importDriver->listContents();

        if (! count($remoteFiles)) {
            $this->exceptionOutput(new \Exception('Any images not found'), $output);

            return;
        }

        $count = $this->db->table("{$this->getImportTablesPrefix()}_files")
            ->count();

        $this->startProgress('Images', $count, $output);

        $this->db->table("{$this->getImportTablesPrefix()}_files")
            ->chunkById(1000, function ($filesToUpload) use ($output, $remoteFiles, $importDriver, $storageDriver) {
                foreach ($filesToUpload as $importFile) {
                    $fileRemote = $importFile->file;

                    if ('Thumbs.db' === $fileRemote) {
                        $this->processProgress();
                        continue;
                    }

                    $fileName = $this->getImageLocalName($fileRemote);

                    $key = array_search($fileRemote, array_column($remoteFiles, 'basename'));
                    if (false === $key) {
                        $this->exceptionOutput(new \Exception(
                            "Warning! File $fileRemote not found on FTP server"
                        ), $output);

                        $this->processProgress();
                        continue;
                    }
                    /** @var Image $image */
                    $image = Image::firstOrNew([
                        'remote_name' => $fileRemote,
                    ]);

                    $info = $remoteFiles[$key];
                    $fileSize = $info['size'];
                    $fileLastModifiedCarbon = Carbon::createFromTimestamp($info['timestamp']);

                    $needToUpload = $image->isChanged($info)
                        || $this->isNeedToLoadImage($fileName, $fileSize, $fileLastModifiedCarbon);

                    if ($needToUpload) {
                        $image->image_size = [0, 0];
                        $image->image_preview_size = [0, 0];
                        $image->minimized = false;
                    }

                    $image->remote_time_mdt = $fileLastModifiedCarbon->timestamp;
                    $image->remote_size = $fileSize;
                    $image->local_name = $fileName;
                    $image->short_descr = $importFile->short_descr;
                    $image->assignment = $importFile->assignment;
                    $image->updateProductId();
                    $image->save();

                    if ($needToUpload) {
                        $this->queue->dispatch(new ImageUpload($image));
                    }

                    $this->processProgress();
                }
            });
    }

    /**
     * @param string $originalFileName
     *
     * @return string
     */
    private function getImageLocalName(string $originalFileName): string
    {
        $nameMap = [
            'ТОВ' => 'good',
            'ЦВ'  => 'color',
            'НК'  => 'package',
            'КАТ' => 'category',
            'АКЦ' => 'promotion',
        ];
        $patterns = array_map(function ($patt) { return '/'.$patt.'/iu'; }, array_keys($nameMap));
        $replacements = array_values($nameMap);

        return preg_replace($patterns, $replacements, $originalFileName);
    }

    /**
     * @param string $filename
     * @param int    $size
     * @param Carbon $lastModified
     *
     * @return bool
     */
    private function isNeedToLoadImage(string $filename, int $size, Carbon $lastModified): bool
    {
        $storage = $this->getImagesFilesystem();

        return ! $storage->exists($filename) ||
            $storage->size($filename) != $size;
        //|| Carbon::createFromTimestamp($storage->lastModified($filename))->startOfDay()->notEqualTo($lastModified);
    }

    /**
     * @param string               $entityName
     * @param int                  $count
     * @param OutputInterface|null $output
     */
    private function startProgress(string $entityName, int $count, OutputInterface $output = null)
    {
        if (! $output) {
            return;
        }

        $progressBar = new ProgressBar($output, $count);
        $progressBar->setFormat('importer');
        $progressBar->setMessage("Import: $entityName");

        $this->progressBar = $progressBar;
    }

    /**
     * @param int $step
     */
    private function processProgress($step = 1)
    {
        if (! $this->progressBar) {
            return;
        }

        $this->progressBar->advance($step);
    }

    /**
     * @param \Throwable           $exception
     * @param OutputInterface|null $output
     *
     * @throws \Throwable
     */
    private function exceptionOutput(\Throwable $exception, OutputInterface $output = null)
    {
        $this->logger->error($exception);

        if (! $output) {
            throw $exception;
        }

        $output->writeln('');
        $output->writeln("<error>{$exception->getMessage()}</error>");
        $output->writeln($exception->getTraceAsString(), OutputInterface::VERBOSITY_VERBOSE);
    }

    /**
     * @param string               $message
     * @param OutputInterface|null $output
     */
    private function writeMessage(string $message, OutputInterface $output = null)
    {
        if (! $output) {
            return;
        }

        $output->writeln("<info>$message</info>");
    }

    /**
     * @return Filesystem
     */
    private function getImportDataFilesystem(): Filesystem
    {
        return $this->filesystemFactory->disk(
            array_get($this->config, 'filesystem.import_data')
        );
    }

    /**
     * @return Filesystem
     */
    private function getImportImagesFilesystem(): Filesystem
    {
        return $this->filesystemFactory->disk(
            array_get($this->config, 'filesystem.import_images')
        );
    }

    /**
     * @return Filesystem
     */
    private function getImagesFilesystem(): Filesystem
    {
        return $this->filesystemFactory->disk(
            array_get($this->config, 'filesystem.images')
        );
    }

    /**
     * @return string
     */
    private function getUpdateFlagFileName(): string
    {
        return array_get($this->config, 'updateFlagFile', 'do-not-update');
    }

    /**
     * @param bool $force
     *
     * @return bool
     */
    public function canImport(bool $force = false): bool
    {
        $dataStorage = $this->getImportDataFilesystem();

        return $force || 'production' !== $this->environment || $dataStorage->exists($this->getUpdateFlagFileName());
    }

    public function cleanImportFlagFile()
    {
        if ('production' === $this->environment) {
            $this->getImportDataFilesystem()
                ->delete($this->getUpdateFlagFileName());
        }
    }
}
