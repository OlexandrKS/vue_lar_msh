<?php

namespace App\ServiceLayer\Importer;

use League\Flysystem\Adapter\Ftp;
use League\Flysystem\AdapterInterface;

class FtpCustomAdapter extends Ftp
{
    /**
     * {@inheritdoc}
     */
    protected function normalizeUnixObject($item, $base)
    {
        $item = preg_replace('#\s+#', ' ', trim($item), 7);

        if (9 !== count(explode(' ', $item, 9))) {
            throw new \RuntimeException("Metadata can't be parsed from item '$item' , not enough parts.");
        }

        list($permissions, /* $number */, /* $owner */, /* $group */, $size, $month, $day, $time, $name) = explode(' ', $item, 9);
        $type = $this->detectType($permissions);
        $path = '' === $base ? $name : $base.$this->separator.$name;
        $timestamp = strtotime(implode(' ', [$month, $day, $time]));

        if ('dir' === $type) {
            return compact('type', 'path');
        }

        $permissions = $this->normalizePermissions($permissions);
        $visibility = $permissions & 0044 ? AdapterInterface::VISIBILITY_PUBLIC : AdapterInterface::VISIBILITY_PRIVATE;
        $size = (int) $size;

        return compact('type', 'path', 'visibility', 'size', 'timestamp');
    }

    /**
     * {@inheritdoc}
     */
    protected function listDirectoryContents($directory, $recursive = true)
    {
        $directory = str_replace('*', '\\*', $directory);

        if ($recursive && $this->recurseManually) {
            return $this->listDirectoryContentsRecursive($directory);
        }

        $options = $recursive ? '--full-time -R' : '--full-time';
        $listing = $this->ftpRawlist($options, $directory);

        return $listing ? $this->normalizeListing($listing, $directory) : [];
    }
}
