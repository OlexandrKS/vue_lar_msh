<?php

namespace App\ServiceLayer\PriceCalculator;

use App\Domain\Enums\Accounting\OrderDeliveryType;
use App\Domain\Enums\Accounting\OrderPaymentMethod;
use Illuminate\Contracts\Auth\Authenticatable;
use October\Rain\Database\Collection;

class CalculationRequest
{
    /**
     * @var Collection
     */
    private $items;

    /**
     * @var array
     */
    private $promotions;

    /**
     * @var Authenticatable|null
     */
    private $user;

    /**
     * @var string
     */
    private $paymentMethod;

    /**
     * @var bool
     */
    private $hasAssembling;

    /**
     * @var bool
     */
    private $deliveryType;

    /**
     * Request constructor.
     *
     * @param Collection           $items
     * @param array                $promotions
     * @param Authenticatable|null $user
     * @param string               $paymentMethod
     * @param bool                 $hasAssembling
     * @param int                  $deliveryType
     */
    public function __construct(Collection $items, array $promotions = [], Authenticatable $user = null,
                                string $paymentMethod = OrderPaymentMethod::CARD_PAY, bool $hasAssembling = true,
                                int $deliveryType = OrderDeliveryType::ADDRESS)
    {
        $this->items = $items;
        $this->promotions = $promotions;
        $this->user = $user;
        $this->paymentMethod = $paymentMethod;
        $this->hasAssembling = $hasAssembling;
        $this->deliveryType = $deliveryType;
    }

    /**
     * @return Collection
     */
    public function items(): Collection
    {
        return $this->items;
    }

    /**
     * @return array
     */
    public function promotions(): array
    {
        return $this->promotions;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function hasPromotion(string $name): bool
    {
        return array_key_exists($name, $this->promotions);
    }

    /**
     * @return Authenticatable|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function promotion(string $name)
    {
        if (! $this->hasPromotion($name)) {
            return [];
        }

        return array_get($this->promotions, $name, []);
    }

    /**
     * @return string
     */
    public function paymentMethod(): string
    {
        return $this->paymentMethod;
    }

    /**
     * @return bool
     */
    public function hasAssembling(): bool
    {
        return $this->hasAssembling;
    }

    /**
     * @return int
     */
    public function deliveryType(): int
    {
        return $this->deliveryType;
    }
}
