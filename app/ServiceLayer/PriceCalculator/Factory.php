<?php

namespace App\ServiceLayer\PriceCalculator;

use App\Contracts\PriceCalculator;
use App\Domain\Entities\Accounting\Calculators\Assembling;
use App\Domain\Entities\Accounting\Calculators\BuyTodayDiscount;
use App\Domain\Entities\Accounting\Calculators\Credit;
use App\Domain\Entities\Accounting\Calculators\Delivery;
use App\Domain\Entities\Accounting\Calculators\FreeDelivery30Plus;
use App\Domain\Entities\Accounting\Calculators\PromoCode;
use App\Domain\Entities\Accounting\Calculators\Specials;
use App\Domain\Entities\Accounting\Calculators\UseBonuses;
use App\Domain\Entities\Accounting\CartItem;
use App\Domain\Entities\Accounting\Order\Calculation;
use App\Domain\Enums\Catalog\PriceType;
use App\ServiceLayer\Promotions\Promotion;
use Illuminate\Contracts\Container\Container;
use Illuminate\Pipeline\Pipeline;

class Factory implements PriceCalculator
{
    /**
     * @var Container
     */
    private $container;

    /**
     * После каждого изменения запускать тесты.
     *
     * @var array
     */
    protected $calculators = [
        Credit::class,
        FreeDelivery30Plus::class,
        Delivery::class,
        Assembling::class,
        Specials::class,
        UseBonuses::class,
        PromoCode::class,
        // BuyTodayDiscount::class,
    ];

    /**
     * {@inheritdoc}
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param CalculationRequest $request
     *
     * @return Calculation
     */
    public function calculate(CalculationRequest $request): Calculation
    {
        $pipe = new Pipeline($this->container);

        return $pipe->send($request)
            ->through($this->calculators)
            ->then($this->response());
    }

    /**
     * @return \Closure
     */
    protected function response()
    {
        return function (CalculationRequest $request): Calculation {
            $total = 0;
            $withoutPromotions = 0;
            $promoDiscounts = 0;
            $promotions = [];

            /** @var CartItem $item */
            foreach ($request->items() as $item) {
                $activePromotions = array_except($item->promotions, [
                    Promotion::ANY_PROMOTION_USE,
                    Promotion::DISCOUNT_VALUE,
                ]);
                $activePrice = $item->offer->active_price;

                $total += $activePrice->value * $item->quantity;
                if (PriceType::LOWER === $activePrice->type) {
                    $withoutPromotions += $activePrice->value * $item->quantity;
                }

                $promoDiscounts += array_get($item->promotions, Promotion::DISCOUNT_VALUE, 0) * $item->quantity;

                // Проставляем действующие акции для всего расчета
                foreach (array_keys($activePromotions) as $promotionName) {
                    $promotions[$promotionName] = true;
                }
            }

            return new Calculation(compact(
                'total',
                'withoutPromotions',
                'promoDiscounts',
                'promotions'
            ));
        };
    }
}
