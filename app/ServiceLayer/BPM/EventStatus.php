<?php

namespace App\ServiceLayer\BPM;

final class EventStatus
{
    const FORMED = 0;
    const SENT = 1;
    const ERROR = 100;
}
