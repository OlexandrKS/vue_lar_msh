<?php

namespace App\ServiceLayer\BPM;

final class LeadType
{
    const PRODBOARD_BASKET = 'ProdBoard: Заказ кухни';
    const PRODBOARD_REGISTRATION = 'ProdBoard: Регистрация пользователя';
    const CARD_CHECKOUT = 'Сайт: Заказ из корзины';
    const CREDIT_CHECKOUT = 'Сайт: Заказ из корзины (кредит)';
    const BUY_ONE_CLICK = 'Сайт: Заказ в 1 клик'; // deprecated
    const CALLIBRI_FEEDBACK = 'Сайт: Feedback';
    const CALLIBRI_CHAT_CLOSED = 'Сайт: Чат закрыт'; // deprecated
    const LEFT_CHECKOUT = 'Сайт: Брошенный заказ';
}
