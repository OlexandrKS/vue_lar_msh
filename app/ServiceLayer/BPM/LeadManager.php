<?php

namespace App\ServiceLayer\BPM;

use App\Contracts\PromotionManager;
use App\Domain\Entities\Accounting\CartItem;
use App\Domain\Entities\Accounting\Order;
use App\Domain\Entities\BPMEvent;
use App\Domain\Entities\Catalog\Offer;
use App\Domain\Enums\Accounting\OrderPaymentMethod;
use App\Domain\Enums\Catalog\OfferType;
use App\ServiceLayer\Sentry\Sentry;
use GuzzleHttp\Client;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\User\Models\User;

class LeadManager
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $url;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Guard
     */
    private $guard;

    /**
     * @var PromotionManager
     */
    private $promotionManager;

    /**
     * LeadManager constructor.
     *
     * @param Request          $request
     * @param Guard            $guard
     * @param PromotionManager $promotionManager
     */
    public function __construct(Request $request, Guard $guard, PromotionManager $promotionManager)
    {
        $this->client = new Client([
            'timeout' => 5,
        ]);

        $this->url = config('services.bpm.endpoint');

        $this->request = $request;
        $this->guard = $guard;
        $this->promotionManager = $promotionManager;
    }

    /**
     * @param string $ga
     *
     * @return string
     */
    private function getClientIdGA($ga = null)
    {
        $cid = '';

        if ($ga) {
            list($version, $domainDepth, $cid1, $cid2) = explode('.', $ga);
            $cid = $cid1.'.'.$cid2;
        }

        return $cid;
    }

    /**
     * @param User $user
     *
     * @return array
     */
    private function getUser(User $user): array
    {
        return [
            'id'        => (string) $user->getKey(),
            'eid'       => (string) $user->external_id,
            'card'      => (string) $user->card_code,
            'firstName' => (string) $user->name,
            'lastName'  => (string) $user->last_name,
            'phone'     => (string) $user->phone,
            'email'     => (string) $user->email,
            'bonuses'   => $user->bonuses_safe,
        ];
    }

    /**
     * @return array
     */
    private function getAnalytics(): array
    {
        return [
            'bpmhref' => (string) $this->request->cookie('bpmHref'),
            'bpmref'  => (string) $this->request->cookie('bpmRef'),
            'cid'     => (string) $this->getClientIdGA($this->request->cookie('_ga')),
        ];
    }

    /**
     * @param BPMEvent $event
     * @param array    $requestData
     *
     * @return BPMEvent
     */
    private function fillModel(BPMEvent $event, array $requestData)
    {
        $event->fill(array_only($requestData, ['type', 'analytics', 'user', 'data']));

        return $event;
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    public function sendProdBoardBasketEvent(array $data): bool
    {
        try {
            /** @var User $user */
            $user = User::where('phone', array_get($data, 'account.phone'))
                ->firstOrFail();

            $event = new BPMEvent();
            $requestData = [
                'id'        => $event->getKey(),
                'type'      => LeadType::PRODBOARD_BASKET,
                'analytics' => $this->getAnalytics(),
                'user'      => $this->getUser($user),
                'data'      => [
                    'number' => (string) array_get($data, 'number'),
                    'url'    => (string) array_get($data, 'url'),
                ],
            ];
            $this->fillModel($event, $requestData);
            $event->save();

            $response = $this->client->post($this->url, [
                'json' => $requestData,
            ]);

            $event->status = EventStatus::SENT;
            $event->save();

            return 200 === $response->getStatusCode();
        } catch (\Exception $e) {
            Sentry::captureException($e);
        }

        return false;
    }

    /**
     * @return bool
     */
    public function sendProdBoardRegister(): bool
    {
        try {
            /** @var User $user */
            $user = $this->guard->user();

            $event = new BPMEvent();
            $requestData = [
                'id'        => $event->getKey(),
                'type'      => LeadType::PRODBOARD_REGISTRATION,
                'analytics' => $this->getAnalytics(),
                'user'      => $this->getUser($user),
                'data'      => [
                    'its_object' => true,
                ],
            ];
            $this->fillModel($event, $requestData);
            $event->save();

            $response = $this->client->post($this->url, [
                'json' => $requestData,
            ]);

            $event->status = EventStatus::SENT;
            $event->save();

            return 200 === $response->getStatusCode();
        } catch (\Exception $e) {
            Sentry::captureException($e);
        }

        return false;
    }

    /**
     * @todo get prices from offer
     *
     * @param Offer   $offer
     * @param Product $product
     * @param int     $quantity
     *
     * @return array
     */
    private function transformOffer(Offer $offer, Product $product, $quantity = 1)
    {
        return [
            'id'              => (string) $product->getKey(),
            'name'            => (string) $product->name,
            'color'           => (string) $product->subname,
            'url'             => (string) $product->url,
            'img'             => (string) $product->preview_mobile_image->url_preview,
            'category'        => (string) $product->good_type_site->type_name,
            'basePrice'       => (string) $product->getStrikedPrice(),
            'activePrice'     => (string) $product->getActivePrice(),
            'activeDiscount'  => (string) $product->getActiveDiscount(),
            'deliveryPrice'   => (string) $product->getDeliveryCost(),
            'assemblingPrice' => (string) $product->getAssemblingCost(),
            'quantity'        => (string) $quantity,
        ];
    }

    /**
     * @param Order $order
     *
     * @return bool
     */
    public function sendCheckoutEvent(Order $order): bool
    {
        try {
            $productsData = [];
            $promotions = [];
            $additions = [];
            $togetherCheapDiscount = 0;
            $paymentMethod = $order->payment_method;
            $type = OrderPaymentMethod::CREDIT === $paymentMethod ? LeadType::CREDIT_CHECKOUT : LeadType::CARD_CHECKOUT;
            /** @var User $user */
            $user = $this->guard->user();

            foreach ($order->items as $orderItem) {
                $offer = $orderItem->offer;
                $productsData[] = $this->transformOffer($offer, $offer->primary, $orderItem->quantity);

                if (OfferType::TOGETHER_CHEAP === $offer->type) {
                    $productsData[] = $this->transformOffer($offer, $offer->support_product, $orderItem->quantity);
                    $togetherCheapDiscount += (int) $offer->properties->saving;
                }
            }

            foreach ($order->properties->promotions as $name => $value) {
                $promotion = $this->promotionManager->getByName($name);

                $promotions[] = [
                    'name'  => $promotion::name(),
                    'value' => $value,
                ];
            }

            $deliveryPrice = (string) $order->calculated->delivery;
            $assemblingPrice = (string) $order->calculated->assembling;

            $deliveryType = $order->properties->deliveryData->type;
            $deliveryAddress = $order->properties->deliveryData->address;

            $firstName = $user->name;
            $lastName = $user->last_name;
            $otherName = $user->other_name;
            $phone = $user->phone;
            $passport = '';

            if (LeadType::CREDIT_CHECKOUT === $type) {
                /** @var Order\CreditData $creditData */
                $creditData = $order->properties->paymentData;
                list($firstName, $lastName) = explode_full_name($creditData->fullName);
                $otherName = $creditData->surname;
                $phone = $creditData->phone;
                $passport = $creditData->passport;
            }

            /* TODO refactor */
            switch ($order->payment_method) {
                case OrderPaymentMethod::CARD_PAY:
                    $additions[] = [
                        'name'  => 'Сумма предоплаты',
                        'value' => (string) $order->properties->paymentData->prePayment,
                    ];
                    break;

                case OrderPaymentMethod::CREDIT:
                    $additions[] = [
                        'name'  => 'Первоначальный взнос',
                        'value' => (string) $order->properties->paymentData->firstInstallment,
                    ];
                    $additions[] = [
                        'name'  => 'Скрок кредитования',
                        'value' => (string) $order->properties->paymentData->loanTerms,
                    ];
                    break;

                case OrderPaymentMethod::INVOICE_PAY:
                    $additions[] = [
                        'name'  => 'Наименование организации',
                        'value' => (string) $order->properties->paymentData->name,
                    ];
                    $additions[] = [
                        'name'  => 'КПП',
                        'value' => (string) $order->properties->paymentData->kpp,
                    ];
                    $additions[] = [
                        'name'  => 'ИНН',
                        'value' => (string) $order->properties->paymentData->inn,
                    ];
                    $additions[] = [
                        'name'  => 'Юридический адрес: Город',
                        'value' => (string) $order->properties->paymentData->city,
                    ];
                    $additions[] = [
                        'name'  => 'Юридический адрес: Улица',
                        'value' => (string) $order->properties->paymentData->street,
                    ];
                    $additions[] = [
                        'name'  => 'Юридический адрес: Дом',
                        'value' => (string) $order->properties->paymentData->house,
                    ];
                    $additions[] = [
                        'name'  => 'Юридический адрес: Номер офиса',
                        'value' => (string) $order->properties->paymentData->apartment,
                    ];
                    $additions[] = [
                        'name'  => 'Уполномоченное лицо',
                        'value' => (string) $order->properties->paymentData->authPerson,
                    ];
                    $additions[] = [
                        'name'  => 'Контактное лицо',
                        'value' => (string) $order->properties->paymentData->contactPerson,
                    ];
                    break;
            }

            $event = new BPMEvent();
            $event->id = $order->id;
            $requestData = [
                'id'        => $event->getKey(),
                'type'      => $type,
                'analytics' => $this->getAnalytics(),
                'user'      => $this->getUser($user),
                'data'      => [
                    'order' => [
                        'additions'       => $additions,
                        'paymentMethod'   => $paymentMethod,
                        'promotions'      => $promotions,
                        'products'        => $productsData,
                        'deliveryPrice'   => $deliveryPrice,
                        'assemblingPrice' => $assemblingPrice,
                        'comment'         => (string) $order->properties->comment,
                    ],
                    'delivery' => [
                        'type'    => $deliveryType,
                        'address' => $deliveryAddress,
                    ],
                    'profile' => [
                        'firstName' => $firstName,
                        'lastName'  => $lastName,
                        'otherName' => $otherName,
                        'phone'     => $phone,
                        'email'     => (string) $user->email,
                        'passport'  => $passport,
                    ],
                ],
            ];
            $this->fillModel($event, $requestData);
            $event->save();

            $response = $this->client->post($this->url, [
                'json' => $requestData,
            ]);

            $event->status = EventStatus::SENT;
            $event->save();

            return 200 === $response->getStatusCode();
        } catch (\Exception $e) {
            Sentry::captureException($e);
        }

        return false;
    }

    /**
     * @deprecated
     *
     * @param array $data
     *
     * @return bool
     */
    public function sendQuickOrderEvent(array $data): bool
    {
        try {
            /** @var Product $product */
            $product = Product::where('id', array_get($data, 'productId'))
                ->with('category')
                ->first();

            $productsData = [
                [
                    'id'              => (string) $product->getKey(),
                    'name'            => (string) $product->name,
                    'color'           => (string) $product->subname,
                    'url'             => (string) $product->url,
                    'img'             => (string) $product->getPreviewImage()->getPreviewSource(),
                    'basePrice'       => (string) $product->price_top_b,
                    'activePrice'     => (string) $product->getActivePrice(),
                    'activeDiscount'  => (string) $product->getActiveDiscount(),
                    'deliveryPrice'   => (string) $product->getDeliveryCost(),
                    'assemblingPrice' => (string) $product->getAssemblingCost(),
                    'quantity'        => '1',
                ],
            ];

            $phone = array_get($data, 'phone');
            /** @var User|null $user */
            $user = User::where('phone', $phone)->first();

            if (! $user) {
                $user = $this->guard->check() ? $this->guard->user() : new User();
            }

            $event = new BPMEvent();
            $requestData = [
                'id'        => $event->getKey(),
                'type'      => LeadType::BUY_ONE_CLICK,
                'analytics' => $this->getAnalytics(),
                'user'      => $this->getUser($user),
                'data'      => [
                    'order' => [
                        'products' => $productsData,
                    ],
                ],
            ];
            $this->fillModel($event, $requestData);
            $event->save();

            $response = $this->client->post($this->url, [
                'json' => $requestData,
            ]);

            $event->status = EventStatus::SENT;
            $event->save();

            return 200 === $response->getStatusCode();
        } catch (\Exception $e) {
            Sentry::captureException($e);
        }

        return false;
    }

    /**
     * @return bool
     */
    public function sendCallibriFeedBack(): bool
    {
        try {
            $user = $this->guard->check() ? $this->guard->user() : new User();

            $event = new BPMEvent();
            $requestData = [
                'id'        => $event->getKey(),
                'type'      => LeadType::CALLIBRI_FEEDBACK,
                'analytics' => $this->getAnalytics(),
                'user'      => $this->getUser($user),
                'data'      => [
                    'its_object' => true,
                ],
            ];
            $this->fillModel($event, $requestData);
            $event->save();

            $response = $this->client->post($this->url, [
                'json' => $requestData,
            ]);

            $event->status = EventStatus::SENT;
            $event->save();

            return 200 === $response->getStatusCode();
        } catch (\Exception $e) {
            Sentry::captureException($e);
        }

        return false;
    }

    /**
     * @deprecated
     *
     * @return bool
     */
    public function sendCallibriChatClosed(): bool
    {
        try {
            /** @var User $user */
            $user = $this->guard->check() ? $this->guard->user() : new User();

            $event = new BPMEvent();
            $requestData = [
                'id'        => $event->getKey(),
                'type'      => LeadType::CALLIBRI_CHAT_CLOSED,
                'analytics' => $this->getAnalytics(),
                'user'      => $this->getUser($user),
                'data'      => [
                    'its_object' => true,
                ],
            ];
            $this->fillModel($event, $requestData);
            $event->save();

            $response = $this->client->post($this->url, [
                'json' => $requestData,
            ]);

            $event->status = EventStatus::SENT;
            $event->save();

            return 200 === $response->getStatusCode();
        } catch (\Exception $e) {
            Sentry::captureException($e);
        }

        return false;
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    public function sendLeftCheckout(array $data): bool
    {
        try {
            /** @var User $user */
            $user = $this->guard->check() ? $this->guard->user() : new User();

            list($firstName, $lastName) = explode_full_name(array_get($data, 'profile.name'));
            $profile = [
                'firstName' => $firstName,
                'lastName'  => $lastName,
                'otherName' => '',
                'phone'     => (string) array_get($data, 'profile.phone'),
                'email'     => (string) array_get($data, 'profile.email'),
                'passport'  => '',
            ];

            $productsData = [];
            $cartItems = hydrate(CartItem::class, array_get($data, 'items'));
            $offerIds = $cartItems->pluck('offer.id');
            $offers = Offer::whereIn('id', $offerIds)
                ->with('primary.images', 'primary.good_type_site', 'products.images', 'products.good_type_site')
                ->get();

            /** @var Offer $offer */
            foreach ($offers as $offer) {
                $cartItem = array_first($cartItems, function ($cartItem) use ($offer) {
                    return array_get($cartItem, 'offer.id') === $offer->getKey();
                });
                $quantity = array_get($cartItem, 'quantity', 0);

                $productsData[] = $this->transformOffer($offer, $offer->primary, $quantity);

                if (OfferType::TOGETHER_CHEAP === $offer->type) {
                    $productsData[] = $this->transformOffer($offer, $offer->support_product, $quantity);
                }
            }

            $event = new BPMEvent();
            $requestData = [
                'id'        => $event->getKey(),
                'type'      => LeadType::LEFT_CHECKOUT,
                'analytics' => $this->getAnalytics(),
                'user'      => $this->getUser($user),
                'data'      => [
                    'profile' => $profile,
                    'order'   => [
                        'products' => $productsData,
                    ],
                ],
            ];
            $this->fillModel($event, $requestData);
            $event->save();

            Sentry::captureEvent($requestData); // todo debug

            $response = $this->client->post($this->url, [
                'json' => $requestData,
            ]);

            $event->status = EventStatus::SENT;
            $event->save();

            return 200 === $response->getStatusCode();
        } catch (\Exception $e) {
            Sentry::captureException($e);
        }

        return false;
    }
}
