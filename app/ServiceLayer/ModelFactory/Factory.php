<?php

namespace App\ServiceLayer\ModelFactory;

use Illuminate\Contracts\Bus\Dispatcher;
use LuckyWeb\MS\Models\Category;
use LuckyWeb\MS\Models\Color;
use LuckyWeb\MS\Models\Furnisher;
use LuckyWeb\MS\Models\GoodType;
use LuckyWeb\MS\Models\Image;
use LuckyWeb\MS\Models\NestedPosition;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\Promotion;
use LuckyWeb\MS\Models\Set;
use LuckyWeb\MS\Models\Shop;
use LuckyWeb\MS\Models\SiteGoodType;
use LuckyWeb\MS\Models\Specification;
use LuckyWeb\MS\Models\SpecificationValue;
use LuckyWeb\MS\Models\SubCategory;
use LuckyWeb\MS\Models\TogetherCheeperPivot;
use LuckyWeb\MS\Models\VacancyCity;

class Factory
{
    /**
     * @var Dispatcher
     */
    private $queue;

    /**
     * Factory constructor.
     *
     * @param Dispatcher $queue
     */
    public function __construct(Dispatcher $queue)
    {
        $this->queue = $queue;
    }

    /**
     * Заполнить сущности данными.
     */
    public function fillEntities()
    {
        $this->legacyFill();
    }

    /**
     * Старый метод заполнения данных сущностей.
     */
    protected function legacyFill()
    {
        Category::updateImported();
        SubCategory::updateImported();
        Color::updateImported();
        Product::updateImported();
        Promotion::updateImported();
        Shop::updateImported();
        Set::updateImported();
        GoodType::updateImported();
        NestedPosition::updateImported();
        Image::updateProductRelations();
        Specification::updateImported();
        SpecificationValue::updateImported();
        Furnisher::updateImported();
        SiteGoodType::updateImported();
        TogetherCheeperPivot::updateImported();
        VacancyCity::seedFromShopCities();
    }
}
