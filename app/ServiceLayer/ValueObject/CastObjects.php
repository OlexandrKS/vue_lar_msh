<?php

namespace App\ServiceLayer\ValueObject;

/**
 * @mixin \Eloquent
 */
trait CastObjects
{
    /**
     * @var array
     */
    protected $objectCasts = [];

    /**
     * {@inheritdoc}
     */
    protected function castAttribute($key, $value)
    {
        $type = $this->getCastType($key);

        if (class_exists($type)) {
            if (! array_key_exists($type, $this->objectCasts)) {
                $this->objectCasts[$key] = $type::fromJson($value);
            }

            return $this->objectCasts[$key];
        }

        return parent::castAttribute($key, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function setAttribute($key, $value)
    {
        $type = array_get($this->casts, $key);

        if (class_exists($type)) {
            if ($value instanceof ValueObject) {
                return $this->attributes[$key] = $value->toJson();
            }
        }

        return parent::setAttribute($key, $value);
    }
}
