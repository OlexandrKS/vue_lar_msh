<?php

namespace App\ServiceLayer\ValueObject;

use Illuminate\Contracts\Support\Arrayable;
use Serafim\Properties\Properties;

abstract class ValueObject implements \JsonSerializable, Arrayable, \Serializable
{
    use Properties;

    /**
     * ValueObject constructor.
     *
     * @param array $properties
     */
    public function __construct($properties = [])
    {
        $this->fillProperties($properties);
    }

    /**
     * @param array $properties
     */
    protected function fillProperties(array $properties = [])
    {
        foreach ($properties as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * @param string $json
     *
     * @return ValueObject
     */
    public static function fromJson(string $json)
    {
        return new static(json_decode($json, true));
    }

    /**
     * @param int $options
     *
     * @throws \RuntimeException
     *
     * @return string
     */
    public function toJson($options = 0)
    {
        if (false === ($value = json_encode($this->jsonSerialize(), $options))) {
            throw new \RuntimeException(json_last_error_msg(), json_last_error());
        }

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return $this->toJson();
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized)
    {
        $this->fillProperties(json_decode($serialized, true));
    }
}
