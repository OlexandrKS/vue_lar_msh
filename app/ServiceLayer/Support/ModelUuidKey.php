<?php

namespace App\ServiceLayer\Support;

use October\Rain\Database\Model;

/**
 * @todo laravel
 * @mixin Model
 */
trait ModelUuidKey
{
    /**
     * @todo laravel
     */
    public static function bootModelUuidKey()
    {
        static::extend(function (Model $model) {
            $model->bindEvent('model.beforeCreate', function () use ($model) {
                $model->getKey();
            });
        });
    }

    /**
     * {@inheritdoc}
     */
    public function getKey()
    {
        $key = $this->getAttribute($this->getKeyName());

        if (! $key) {
            $this->attributes[$this->getKeyName()] = $key = uuid4();
        }

        return $key;
    }
}
