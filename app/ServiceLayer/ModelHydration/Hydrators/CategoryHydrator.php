<?php

namespace App\ServiceLayer\ModelHydration\Hydrators;

use App\ServiceLayer\ModelHydration\AbstractHydrator;
use LuckyWeb\MS\Models\Category;

class CategoryHydrator extends AbstractHydrator
{
    /**
     * @var string
     */
    public $entity = Category::class;
}
