<?php

namespace App\ServiceLayer\ModelHydration\Hydrators;

use App\Domain\Entities\Accounting\CartItem;
use App\Domain\Entities\Catalog\Offer;
use App\ServiceLayer\ModelHydration\AbstractHydrator;

class CartItemHydrator extends AbstractHydrator
{
    /**
     * @var string
     */
    public $entity = CartItem::class;

    /**
     * @var array
     */
    public $relations = [
        'offer' => Offer::class,
    ];

    /**
     * @param array $data
     *
     * @return Offer
     */
    public function offer(array $data = [])
    {
        return $this->factory->hydrate(Offer::class, $data)->first();
    }
}
