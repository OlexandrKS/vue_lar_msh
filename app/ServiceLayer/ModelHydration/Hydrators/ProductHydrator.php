<?php

namespace App\ServiceLayer\ModelHydration\Hydrators;

use App\ServiceLayer\ModelHydration\AbstractHydrator;
use LuckyWeb\MS\Models\Category;
use LuckyWeb\MS\Models\Image;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\ProductId;
use LuckyWeb\MS\Models\SiteGoodType;

class ProductHydrator extends AbstractHydrator
{
    /**
     * @var string
     */
    public $entity = Product::class;

    /**
     * @var array
     */
    protected $relations = [
        'nid'            => ProductId::class,
        'category'       => Category::class,
        'good_type_site' => SiteGoodType::class,
        'images'         => Image::class,
    ];

    /**
     * @param array $data
     *
     * @return Category
     */
    public function category(array $data = [])
    {
        return $this->factory->hydrate(Category::class, $data)->first();
    }

    /**
     * @param array $data
     *
     * @return SiteGoodType
     */
    public function goodTypeSite(array $data = [])
    {
        return $this->factory->hydrate(SiteGoodType::class, $data)->first();
    }

    public function nid(array $data = [])
    {
        return $this->factory->hydrate(ProductId::class, $data)->first();
    }
}
