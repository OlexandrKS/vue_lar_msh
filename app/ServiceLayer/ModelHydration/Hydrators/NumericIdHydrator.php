<?php

namespace App\ServiceLayer\ModelHydration\Hydrators;

use App\ServiceLayer\ModelHydration\AbstractHydrator;
use LuckyWeb\MS\Models\ProductId;

class NumericIdHydrator extends AbstractHydrator
{
    /**
     * @var string
     */
    public $entity = ProductId::class;
}
