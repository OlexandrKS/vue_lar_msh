<?php

namespace App\ServiceLayer\ModelHydration\Hydrators;

use App\Domain\Entities\Catalog\Offer;
use App\ServiceLayer\ModelHydration\AbstractHydrator;
use LuckyWeb\MS\Models\Product;

class OfferHydrator extends AbstractHydrator
{
    /**
     * @var string
     */
    public $entity = Offer::class;

    /**
     * @var array
     */
    protected $relations = [
        'primary'  => Product::class,
        'products' => Product::class,
    ];

    /**
     * @param array $data
     *
     * @return Product
     */
    public function primary(array $data = [])
    {
        return $this->factory->hydrate(Product::class, $data)->first();
    }
}
