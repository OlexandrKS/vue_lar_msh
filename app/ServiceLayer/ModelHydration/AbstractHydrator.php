<?php

namespace App\ServiceLayer\ModelHydration;

use App\Contracts\HydrationFactory;
use October\Rain\Database\Collection;
use October\Rain\Database\Model;

/**
 * @property string $entity
 */
abstract class AbstractHydrator implements Hydrator
{
    /**
     * @var Factory
     */
    protected $factory;

    /**
     * @var string
     */
    protected $entity;

    /**
     * @var array
     */
    protected $relations = [];

    /**
     * {@inheritdoc}
     */
    public function __construct(HydrationFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * {@inheritdoc}
     */
    public function hydrate(array $data): Collection
    {
        $relations = array_keys($this->relations);
        $isSingle = array_key_exists((new $this->entity())->getKeyName(), $data);
        $resultCollection = new Collection();

        if ($isSingle) {
            $data = [$data];
        }

        foreach ($data as $entityData) {
            $withoutRelations = array_except($entityData, $relations);
            $dataRaw = [];

            foreach ($withoutRelations as $field => $value) {
                if (is_array($value)) {
                    $value = json_encode($value);
                }

                $dataRaw[$field] = $value;
            }

            /** @var Collection $collection */
            $collection = $this->entity::hydrate([$dataRaw]);
            /** @var Model $entity */
            $entity = $collection->first();

            foreach ($relations as $relationName) {
                if (! array_key_exists($relationName, $entityData)) {
                    continue;
                }

                if (null !== $entityData[$relationName]) {
                    $relationValue = $this->hydrateRelation($relationName, $entityData[$relationName]);
                    $this->setAttributeSafe($entity, $relationName, $relationValue);
                }
            }

            $resultCollection->push($entity);
        }

        return $resultCollection;
    }

    /**
     * @param string $relationName
     * @param array  $data
     *
     * @return Collection|Model
     */
    protected function hydrateRelation(string $relationName, array $data)
    {
        $camelCase = camel_case($relationName);

        return method_exists($this, $camelCase) ?
            call_user_func([$this, $camelCase], $data) :
            $this->factory->hydrate($this->relations[$relationName], $data);
    }

    /**
     * @param Model  $model
     * @param string $name
     * @param mixed  $value
     *
     * @throws \Exception
     */
    protected function setAttributeSafe(Model $model, string $name, $value)
    {
        // todo laravel
        if ($model->hasRelation($name)) {
            $model->setRelation($name, $value);

            return;
        }

        $model->setAttribute($name, $value);
    }
}
