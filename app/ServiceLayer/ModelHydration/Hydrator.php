<?php

namespace App\ServiceLayer\ModelHydration;

use October\Rain\Database\Collection;
use October\Rain\Database\Model;

interface Hydrator
{
    /**
     * @param array $data
     *
     * @return Collection|Model[]
     */
    public function hydrate(array $data): Collection;
}
