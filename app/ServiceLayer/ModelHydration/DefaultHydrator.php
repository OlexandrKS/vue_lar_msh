<?php

namespace App\ServiceLayer\ModelHydration;

class DefaultHydrator extends AbstractHydrator
{
    /**
     * DefaultHydrator constructor.
     *
     * @param Factory $factory
     * @param string  $entity
     */
    public function __construct(Factory $factory, string $entity)
    {
        parent::__construct($factory);

        $this->entity = $entity;
    }
}
