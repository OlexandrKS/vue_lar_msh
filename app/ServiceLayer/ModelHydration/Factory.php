<?php

namespace App\ServiceLayer\ModelHydration;

use App\Contracts\HydrationFactory;
use Illuminate\Contracts\Container\Container;
use October\Rain\Database\Collection;
use October\Rain\Database\Model;

class Factory implements HydrationFactory
{
    /**
     * @var array
     */
    private $config;

    /**
     * @var Container
     */
    private $container;

    /**
     * @var array
     */
    private $hydrators = [];

    /**
     * Factory constructor.
     *
     * @param Container $container
     * @param array     $config
     */
    public function __construct(Container $container, array $config = [])
    {
        $this->config = $config;
        $this->container = $container;
    }

    /**
     * @param string        $entity
     * @param array         $data
     * @param Hydrator|null $hydrator
     *
     * @return Collection|Model[]
     */
    public function hydrate(string $entity, array $data, Hydrator $hydrator = null): Collection
    {
        $hydrator = $hydrator ?? $this->getHydrator($entity);

        return $hydrator
            ->hydrate($data);
    }

    /**
     * @param string $entity
     *
     * @return Hydrator
     */
    private function getHydrator(string $entity): Hydrator
    {
        if (array_key_exists($entity, $this->config)) {
            if (! array_key_exists($entity, $this->hydrators)) {
                $this->hydrators[$entity] = $this->container->make($this->config[$entity]);
            }

            return $this->hydrators[$entity];
        }

        return new DefaultHydrator($this, $entity);
    }
}
