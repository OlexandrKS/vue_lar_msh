<?php

namespace App\ServiceLayer\SmsSender;

use App\Contracts\SmsSender;
use App\ServiceJobs\SmsSender\MtsSmsSender;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Http\Request;

class MtsProvider implements SmsSender
{
    use Throttle;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Dispatcher
     */
    private $queue;

    /**
     * Sender constructor.
     *
     * @param Request    $request
     * @param Dispatcher $queue
     */
    public function __construct(Request $request, Dispatcher $queue)
    {
        $this->request = $request;
        $this->queue = $queue;
    }

    /**
     * @param string $phone
     * @param string $message
     * @param bool   $throttle
     *
     * @throws ThrottleException
     */
    public function handle(string $phone, string $message, bool $throttle = true)
    {
        if ($throttle) {
            $this->throttleByNumber($this->request, $phone);
        }

        $this->queue->dispatch(new SmsEvent($phone));
        $this->queue->dispatch(new MtsSmsSender($phone, $message));
    }
}
