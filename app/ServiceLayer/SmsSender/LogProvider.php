<?php

namespace App\ServiceLayer\SmsSender;

use App\Contracts\SmsSender;
use Illuminate\Http\Request;
use Psr\Log\LoggerInterface;

class LogProvider implements SmsSender
{
    use Throttle;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * LogProvider constructor.
     *
     * @param Request         $request
     * @param LoggerInterface $logger
     */
    public function __construct(Request $request, LoggerInterface $logger)
    {
        $this->request = $request;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(string $phone, string $message, bool $throttle = true)
    {
        if ($throttle) {
            $this->throttleByNumber($this->request, $phone);
        }

        $this->logger->info("SMS $phone: $message");
    }
}
