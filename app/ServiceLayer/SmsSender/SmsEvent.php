<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05.04.2019
 * Time: 16:38.
 */

namespace App\ServiceLayer\SmsSender;

class SmsEvent
{
    public $phone;

    /**
     * SmsEvent constructor.
     *
     * @param Dispatcher $event
     * @param string     $phone
     */
    public function __construct(string $phone)
    {
        $this->phone = $phone;
    }
}
