<?php

namespace App\ServiceLayer\SmsSender;

use Illuminate\Http\Request;

trait Throttle
{
    /**
     * @param Request $request
     * @param string  $phone
     *
     * @throws ThrottleException
     */
    protected function throttleByNumber(Request $request, string $phone)
    {
        $hash = sha1($request->ip().$request->header('user-agent'));
        $key = "sms-throttle.$hash";
        $dayKey = "day-sms.$hash";

        if (cache()->has($key)) {
            cache()->decrement($key);
        }

        cache()->add($key, 2, 2);
        cache()->add($dayKey, 4, 1440);

        if (cache()->get($key) < 1 && cache()->get($dayKey) > 0) {
            throw new ThrottleException();
        } else {
            if (0 != cache()->get($dayKey)) {
                cache()->decrement($dayKey);
            } else {
                throw new ThrottleException('', 420);
            }
        }
    }
}
