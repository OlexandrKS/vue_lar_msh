<?php

namespace App\Providers;

use App\OctoberAdapters\BackendUserProvider;
use App\OctoberAdapters\OctoberGuard;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use LuckyWeb\User\Classes\AuthManager;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::provider('backend', function ($app) {
            return $app->make(BackendUserProvider::class);
        });

        Auth::extend('october', function ($app, $name, array $config) {
            return new OctoberGuard(
                $name, Auth::createUserProvider($config['provider']), $app['session.store'], AuthManager::instance()
            );
        });
    }

    /**
     * {@inheritdoc}
     */
    public function register()
    {
        foreach ([
            'auth' => [\Illuminate\Auth\AuthManager::class, \Illuminate\Contracts\Auth\Factory::class],
            'auth.driver' => [\Illuminate\Contracts\Auth\Guard::class],
        ] as $key => $aliases) {
            foreach ($aliases as $alias) {
                $this->app->alias($key, $alias);
            }
        }
    }
}
