<?php

namespace App\Providers;

use App\Backend\ServiceLayer\Banner\BannerQueue;
use App\Backend\ServiceLayer\Constructor\Manager;
use App\Contracts\ConstructorManager;
use App\Contracts\PriceCalculator;
use App\Contracts\SmsSender;
use App\Domain\Entities\Accounting\Cart;
use App\ServiceLayer\PriceCalculator\Factory as PriceCalculatorFactory;
use App\ServiceLayer\SmsSender\LogProvider;
use App\ServiceLayer\SmsSender\MtsProvider;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\ServiceProvider;
use Intervention\Image\ImageManager;

class FactoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $this->cartFactory();
        $this->priceCalculatorFactory();
        $this->smsSender();
        $this->constructorFactory();
        $this->bannerQuery();
        $this->interventionImage();
    }

    /**
     * Передаем текущего пользователя в корзину.
     */
    private function cartFactory()
    {
        $this->app->when(Cart::class)
            ->needs(Authenticatable::class)
            ->give(function () {
                return $this->app->make('request')->user();
            });
    }

    /**
     * Калькулятор стоимости заказа.
     */
    private function priceCalculatorFactory()
    {
        $this->app->singleton(PriceCalculator::class, PriceCalculatorFactory::class);
    }

    /**
     * Сервис для отправки сообщений.
     */
    private function smsSender()
    {
        $this->app->singleton(
            SmsSender::class,
            'production' === $this->app->environment() ? MtsProvider::class : LogProvider::class
        );
    }

    /**
     * Регистрация зависимостей для конструктора страниц.
     */
    private function constructorFactory()
    {
        $this->app->singleton(ConstructorManager::class, function () {
            return $this->app->make(Manager::class);
        });
    }

    private function bannerQuery()
    {
        $this->app->singleton(BannerQueue::class, function () {
            return new BannerQueue();
        });
    }

    private function interventionImage()
    {
        $this->app->when(ImageManager::class)
            ->needs('data')
            ->give(function () {
                return config('image', [
                    'driver' => 'gd',
                ]);
            });
    }
}
