<?php

namespace App\Providers;

use App\ServiceLayer\Twig\WebpackAssetsExtension;
use Barryvdh\Debugbar\ServiceProvider as DebugbarServiceProvider;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Support\ServiceProvider;
use Sentry\Laravel\ServiceProvider as SentryServiceProvider;
use Serafim\Properties\ArrayCache;
use Serafim\Properties\Bootstrap;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register()
    {
        if (config('app.debug') && 'production' !== $this->app->environment()) {
            $this->app->register(IdeHelperServiceProvider::class);
            $this->app->register(DebugbarServiceProvider::class);

            // @todo laravel
            $twig = $this->app->make('twig.environment');
            $twig->enableDebug();
            $twig->addExtension(new \Twig\Extension\DebugExtension());
        }

        // @todo laravel
        $this->app->make('view')->addExtension('twig', 'twig');
        $this->app->make('twig.environment')->addExtension(new WebpackAssetsExtension());

        if (config('app.sentry', false)) {
            $this->app->register(SentryServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        $cacheDriver = 'production' === $this->app->environment() ?
            $this->app->make('cache.store') :
            new ArrayCache();

        Bootstrap::getInstance()->setCacheDriver($cacheDriver);
    }
}
