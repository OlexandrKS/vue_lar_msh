<?php

namespace App\Providers;

use App\Contracts\HydrationFactory;
use App\Domain\Entities\Accounting\CartItem;
use App\Domain\Entities\Catalog\Offer;
use App\ServiceLayer\ModelHydration\Factory as ModelHydrationFactory;
use App\ServiceLayer\ModelHydration\Hydrators\CartItemHydrator;
use App\ServiceLayer\ModelHydration\Hydrators\CategoryHydrator;
use App\ServiceLayer\ModelHydration\Hydrators\NumericIdHydrator;
use App\ServiceLayer\ModelHydration\Hydrators\OfferHydrator;
use App\ServiceLayer\ModelHydration\Hydrators\ProductHydrator;
use App\ServiceLayer\ModelHydration\Hydrators\SiteGoodTypeHydrator;
use Illuminate\Support\ServiceProvider;
use LuckyWeb\MS\Models\Category;
use LuckyWeb\MS\Models\Product;
use LuckyWeb\MS\Models\ProductId;
use LuckyWeb\MS\Models\SiteGoodType;

class HydrationServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $hydrators = [
        CartItem::class     => CartItemHydrator::class,
        Offer::class        => OfferHydrator::class,
        Product::class      => ProductHydrator::class,
        Category::class     => CategoryHydrator::class,
        SiteGoodType::class => SiteGoodTypeHydrator::class,
        ProductId::class    => NumericIdHydrator::class,
    ];

    /**
     * Register the application services.
     */
    public function register()
    {
        $instance = new ModelHydrationFactory($this->app, $this->hydrators);

        $this->app->instance(HydrationFactory::class, $instance);
    }
}
