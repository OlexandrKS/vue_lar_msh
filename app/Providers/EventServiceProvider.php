<?php

namespace App\Providers;

use App\Domain\Events\Accounting\CartAddedOffer;
use App\Domain\Events\Accounting\CartChangedQuantity;
use App\Domain\Events\Accounting\CartDeletedOffer;
use App\Domain\Events\Accounting\CartView;
use App\Domain\Events\Accounting\OrderCreated;
use App\Domain\Listeners\Cart\BuyTodayDiscountOnAdd;
use App\Domain\Listeners\Order\SendBPMEvent;
use Illuminate\Auth\Events\Attempting;
use Illuminate\Auth\Events\Authenticated;
use Illuminate\Auth\Events\Failed;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        // Auth
        Attempting::class    => [],
        Login::class         => [],
        Authenticated::class => [],
        Failed::class        => [],
        Registered::class    => [],

        // Cart
        CartAddedOffer::class      => [
            // BuyTodayDiscountOnAdd::class,
        ],
        CartView::class            => [],
        CartChangedQuantity::class => [],
        CartDeletedOffer::class    => [],

        // Checkout
        OrderCreated::class => [
            SendBPMEvent::class,
        ],
    ];

    /**
     * Register any events for your application.
     */
    public function boot()
    {
        parent::boot();
    }
}
