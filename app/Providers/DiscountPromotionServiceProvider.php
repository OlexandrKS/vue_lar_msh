<?php

namespace App\Providers;

use App\Contracts\PromotionManager as PromotionManagerContract;
use App\Domain\Entities\Accounting\Promotions\BuyTodayDiscount;
use App\Domain\Entities\Accounting\Promotions\FreeDelivery30Plus;
use App\Domain\Entities\Accounting\Promotions\PromoCode;
use App\Domain\Entities\Accounting\Promotions\UserBonuses;
use App\ServiceLayer\Promotions\PromotionManager;
use Illuminate\Support\ServiceProvider;

class DiscountPromotionServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $promotions = [
        FreeDelivery30Plus::class,
        // BuyTodayDiscount::class,
        UserBonuses::class,
        PromoCode::class,
    ];

    /**
     * Register the application services.
     */
    public function register()
    {
        $manager = new PromotionManager($this->app, $this->promotions);

        $this->app->instance(PromotionManagerContract::class, $manager);
    }
}
