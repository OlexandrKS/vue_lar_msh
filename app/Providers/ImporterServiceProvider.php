<?php

namespace App\Providers;

use App\ServiceLayer\Importer\FtpCustomAdapter;
use App\ServiceLayer\Importer\ImportManager;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ServiceProvider;
use League\Flysystem\Filesystem;

class ImporterServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        Storage::extend('ftpa', function ($app, $config) {
            return new Filesystem(new FtpCustomAdapter($config));
        });
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $this->app->when(ImportManager::class)
            ->needs('$config')
            ->give(function () {
                return config('importer');
            });
    }
}
